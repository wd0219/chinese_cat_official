package com.stylefeng.guns.system;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.stylefeng.guns.base.BaseJunit;
import com.stylefeng.guns.modular.huamao.model.SktAreas;
import com.stylefeng.guns.modular.huamao.service.impl.SktAreasServiceImpl;

public class SktAreasServiceImplTest  extends BaseJunit {

	@Autowired
	
	private SktAreasServiceImpl sktAreasServiceImpl;
	
	@Test
	public void getcityList(){
		List<SktAreas> sktAreas = sktAreasServiceImpl.selectList(null);
		for (SktAreas sktAreas2 : sktAreas) {
			System.out.println(sktAreas2.getAreaName());
		}
	}
}
