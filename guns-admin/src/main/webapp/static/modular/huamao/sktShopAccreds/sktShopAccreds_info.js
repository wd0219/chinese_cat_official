/**
 * 初始化店铺管理详情对话框
 */
var SktShopAccredsInfoDlg = {
    sktShopAccredsInfoData : {}
};

/**
 * 清除数据
 */
SktShopAccredsInfoDlg.clearData = function() {
    this.sktShopAccredsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShopAccredsInfoDlg.set = function(key, val) {
    this.sktShopAccredsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShopAccredsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktShopAccredsInfoDlg.close = function() {
    parent.layer.close(window.parent.SktShopAccreds.layerIndex);
}

/**
 * 收集数据
 */
SktShopAccredsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('accredId')
    .set('shopId')  ;
}

/**
 * 提交添加
 */
SktShopAccredsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktShopAccreds/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktShopAccreds.table.refresh();
        SktShopAccredsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShopAccredsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktShopAccredsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktShopAccreds/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktShopAccreds.table.refresh();
        SktShopAccredsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShopAccredsInfoData);
    ajax.start();
}

$(function() {
	var storeStatus = $('#storeStatus').val();
	switch(storeStatus){
	case '0':
		$('#storeStatus2').val("关闭");
		$('#storeStatus2').attr('disabled',true);
		break;
	case '1':
		$('#storeStatus2').val("已开启");
		$('#storeStatus2').attr('disabled',true);
		break;
		default:
		break;
	}
	
	var shopStatus = $('#shopStatus').val();
	switch(shopStatus){
	case '0':
		$('#shopStatus2').val("已开启");
		$('#shopStatus2').attr('disabled',true);
		break;
	case '1':
		$('#shopStatus2').val("关闭");
		$('#shopStatus2').attr('disabled',true);
		break;
		default:
		break;
	}
});
