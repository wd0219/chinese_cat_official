/**
 * 店铺管理管理初始化
 */
var SktShopAccreds = {
    id: "SktShopAccredsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktShopAccreds.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '账号', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '是否自营', field: 'isSelf', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		var result;
            		switch(value){
            		case 1:
            			result = "自营";
            			break;
            		case 0:
            			result = "非自营";
            			break;
            		}
            		return result;
            	}},
            {title: '店铺名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            {title: '联系电话', field: 'telephone', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺地址', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '所属公司', field: 'companyName', visible: true, align: 'center', valign: 'middle'},
            {title: '商家每日营业额限度', field: 'businessQuota', visible: true, align: 'center', valign: 'middle'},
            {title: '联盟商家状态', field: 'storeStatus', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		var result;
            		switch(value){
            		case 1:
            			result = "已开启";
            			break;
            		case 0:
            			result = "关闭";
            			break;
            		}
            		return result;
            	}},
            {title: '商城店铺状态', field: 'shopStatus', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "已开启";
        			break;
        		case 0:
        			result = "关闭";
        			break;
        		}
        		return result;
        	}},
            {title: '审核时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'op', visible: true,width:90, align: 'center', valign: 'middle',formatter:SktShopAccreds.op}
    ];
};
/*
 * 操作
 * */
SktShopAccreds.op = function () {
	return "<a href='javascript:void(0)' onclick=\"SktShopAccreds.openSktShopAccredsDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktShopAccreds.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
SktShopAccreds.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktShopAccreds.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加店铺管理
 */
SktShopAccreds.openAddSktShopAccreds = function () {
    var index = layer.open({
        type: 2,
        title: '添加店铺管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktShopAccreds/sktShopAccreds_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看店铺管理详情
 */
SktShopAccreds.openSktShopAccredsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '店铺管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktShops/sktShops_update/' + SktShopAccreds.seItem.shopId + '/'+$("#shopType").val()
        });
        this.layerIndex = index;
    }
};

/**
 * 删除店铺管理
 */
SktShopAccreds.delete = function () {
    if (this.check()) {
    	var id = this.seItem.expressId;
        var operation = function(){
        	var ajax = new $ax(Feng.ctxPath + "/sktShopAccreds/delete", function (data) {
            Feng.success("删除成功!");
            SktShopAccreds.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("shopId",id);
        ajax.start();
    };
    	Feng.confirm("是否删除账号 " + SktShopAccreds.seItem.loginName + "?",operation);
    }
};

/**
 * 查询店铺管理列表
 */
SktShopAccreds.search = function () {
    var queryData = {};
    queryData['loginName'] = $("#loginName").val();
    queryData['trueName'] = $("#trueName").val();
    queryData['companyName'] = $("#companyName").val();
    queryData['createTime'] = $("#createTime").val();
    queryData['shopName'] = $("#shopName").val();
    queryData['telephone'] = $("#telephone").val();
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['shopType'] = $("#shopType").val();

    SktShopAccreds.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktShopAccreds.initColumn();
    var obj={};

    var table = new BSTable(SktShopAccreds.id, "/sktShopAccreds/list/"+$("#shopType").val(), defaultColunms);
    table.setPaginationType("server");
    SktShopAccreds.table = table.init();
});
