/**
 * 初始化用户提现详情对话框
 */
var UsersDrawsInfoDlg = {
    usersDrawsInfoData : {}
};

/**
 * 清除数据
 */
UsersDrawsInfoDlg.clearData = function() {
    this.usersDrawsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersDrawsInfoDlg.set = function(key, val) {
    this.usersDrawsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersDrawsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UsersDrawsInfoDlg.close = function() {
    parent.layer.close(window.parent.UsersDraws.layerIndex);
}

/**
 * 收集数据
 */
UsersDrawsInfoDlg.collectData = function() {
    this
    .set('drawId')
    .set('drawNo')
    .set('userId')
    .set('bankCardId')
    .set('type')
    .set('money')
    .set('fee')
    .set('ratio')
    .set('taxratio')
    .set('cashSatus')
    .set('getTime')
    .set("ybeginTime")
    .set("yendTime")
    .set('createTime')
    .set('checkRemark')
    .set('checkStaffId')
    .set('checkTime');
}

/**
 * 提交添加
 */
UsersDrawsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/usersDraws/add", function(data){
	        Feng.success("添加成功!");
	        window.parent.UsersDraws.table.refresh();
	        UsersDrawsInfoDlg.close();
	    },function(data){
	        Feng.error("添加失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.usersDrawsInfoData);
	    ajax.start();
			}else{
				Feng.success("请核实添加字段!");
			}
	}
	
	function isRealNum(val){
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
		if(val === "" || val ==null){
		    return false;
		}
		if(!isNaN(val)){
		    return true;
		}else{
		    return false;
		}
	}

/**
 * 提交修改
 */
UsersDrawsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/usersDraws/update", function(data){
        Feng.success("修改成功!");
        window.parent.UsersDraws.table.refresh();
        UsersDrawsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersDrawsInfoData);
    ajax.start();
}else{
	Feng.success("请核实添加字段!");
}
}
/**
 * 批量拒绝
 */
UsersDrawsInfoDlg.batchEdit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/usersDraws/batchEdit", function(data){
        if(data == "true"){
            Feng.success("拒绝成功!");
        }else{
            Feng.error("未查询到提现记录!");
        }
        window.parent.UsersDraws.table.refresh();
        UsersDrawsInfoDlg.close();
    },function(data){
        Feng.error("拒绝失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersDrawsInfoData);
    ajax.start();
}else{
	Feng.success("请核实添加字段!");
}
}
/**
 * 审核拒绝
 */
UsersDrawsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/usersDraws/updateSatus", function(data){
        Feng.success("success!");
        window.parent.UsersDraws.table.refresh();
        UsersDrawsInfoDlg.close();
    },function(data){
        Feng.error("拒绝失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersDrawsInfoData);
    ajax.start();
}else{
	Feng.success("请核实添加字段!");
}
}
$(function() {

});
