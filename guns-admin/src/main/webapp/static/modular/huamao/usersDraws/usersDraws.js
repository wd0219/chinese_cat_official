/**
 * 用户提现管理初始化
 */
var UsersDraws = {
    id: "UsersDrawsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
UsersDraws.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
        	//{title: '预计到账时间', field: 'getTime', visible: true, align: 'center', valign: 'middle'},
        	{title: '赎回类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:UsersDraws.type},
        	{title: '赎回编号', field: 'drawNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户昵称', field: 'loginName', visible: true, align: 'center', valign: 'middle',formatter:UsersDraws.op},
        	//{title: '自增id', field: 'drawId', visible: false, align: 'center', valign: 'middle'},
            {title: '真实姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            //{title: '银行卡号', field: 'accNo', visible: true, align: 'center', valign: 'middle'},
        	{title: ' 转换华宝数量', field: 'money', visible: true, align: 'center', valign: 'middle'},
        	{title: ' 用户华宝数量', field: 'kaiyuan', visible: true, align: 'center', valign: 'middle'},
        	{title: '手续费+综合服务费', field: 'fee', visible: true, align: 'center', valign: 'middle'},
            //{title: '用户ID', field: 'userId', visible: false, align: 'center', valign: 'middle'},
            //{title: '银行卡表ID', field: 'bankCardId', visible: true, align: 'center', valign: 'middle'},
            {title: '赎回手续费比例  %', field: 'ratio', visible: true, align: 'center', valign: 'middle'},
            {title: '赎回综合服务费率比例  %', field: 'taxratio', visible: true, align: 'center', valign: 'middle'},
            {title: '赎回时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核时间', field: 'checkTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核状态 ', field: 'cashSatus', visible: true, align: 'center', valign: 'middle',formatter:UsersDraws.cashSatus},
            {title: '是否手动处理', field: 'specialType', visible: true, align: 'center', valign: 'middle',formatter:UsersDraws.specialType},
            {title: '操作', field: 'cashSatus', visible: true, align: 'center', valign: 'middle',formatter:UsersDraws.operate},
            //{title: '拒绝理由', field: 'checkRemark', visible: true, align: 'center', valign: 'middle'},
            //{title: '审核员工ID', field: 'checkStaffId', visible: true, align: 'center', valign: 'middle'},
    ];
};
/**
 * 审核状态对应：-2受理失败>-1审核拒绝>0审核中>2受理中>3受理成功>4线下打款成功
 */
UsersDraws.cashSatus = function(value,row,index){
	if(value == -2){
		return "受理失败";
	}else if(value == -1){
		return "申请拒绝："+row.checkRemark;
	}else if(value == 0){
		return "审核中";
	}else if(value == 2){
		return "受理中";
	}else if(value == 3){
		return "受理成功";
	}else if(value == 4){
		return "线下打款成功";
	}
}
/**
 * 是否手动处理对应：0：非手动处理；2：手动处理成功；3：手动处理失败
 */
UsersDraws.specialType = function(value,row,index){
	if(value == 0){
		return "非手动处理";
	}else if(value == 2){
		return "处理成功";
	}else if(value == 3){
		return "处理失败";
	}
}
/**
 * 赎回类型对应：1华宝赎回 2现金赎回 3华宝营业额赎回
 */
UsersDraws.type = function(value,row,index){
	if(value == 1){
		return "华宝赎回";
	}else if(value == 2){
		return "现金赎回";
	}else if(value == 3){
		return "华宝营业额赎回";
	}
}
/**
 * 通过用户昵称查找用户
 */
UsersDraws.op = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"UsersDraws.openUsersDrawsDetail()\">"+value+"</a>"
}
/**
 * 操作的链接
 */
UsersDraws.operate = function (value,row,index) {
	if(value==0){
		return "<a href='javascript:void(0)'onclick=\"UsersDraws.openUsersDrawsPass()\">审核通过</a>&nbsp;&nbsp;<a href='javascript:void(0)'onclick=\"UsersDraws.openUsersDrawsRefuse()\">审核拒绝</a>&nbsp;&nbsp;<a href='javascript:void(0)'onclick=\"UsersDraws.openUsersDrawsMoney()\">线下打款</a>"
	}else if(value==2){
		return "";
	    //return "<a href='javascript:void(0)'onclick=\"UsersDraws.openUsersDrawsInHand()\">订单处理中处理</a>"
	}else if(value==-2){
	    return "";
		// return "<a href='javascript:void(0)'onclick=\"UsersDraws.openUsersDrawsDefeated()\">订单失败处理</a>"
	}else{
	    return "";
    }
}
/**
 * 检查是否选中
 */
UsersDraws.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        UsersDraws.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户提现
 */
UsersDraws.openAddUsersDraws = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户提现',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/usersDraws/usersDraws_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户详情
 */
UsersDraws.openUsersDrawsDetail = function () {
	var UsersDraws2 = {
		    id: "UsersDrawsTable",	//表格id
		    seItem: null,		//选中的条目
		    table: null,
		    layerIndex: -1
		};
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/account?loginName=' + UsersDraws.seItem.loginName	//将昵称传到用户账户记录表查询数据
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户提现
 */
UsersDraws.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/usersDraws/delete", function (data) {
            Feng.success("删除成功!");
            UsersDraws.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("usersDrawsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户提现列表
 */
UsersDraws.search = function () {
    var queryData = {};
    queryData['drawNo'] = $("#drawNo").val();
    queryData['accNo'] = $("#accNo").val();
    queryData['loginName'] = $("#loginName").val();
    queryData['userPhone'] = $("#userPhone").val();
    queryData['cashSatus'] = $("#cashSatus").val();
    queryData['specialType'] = $("#specialType").val();
    queryData['ybeginTime'] = $("#ybeginTime").val();
    queryData['yendTime'] = $("#yendTime").val();
    queryData['cbeginTime'] = $("#cbeginTime").val();
    queryData['cendTime'] = $("#cendTime").val();
    UsersDraws.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = UsersDraws.initColumn();
    var table = new BSTable(UsersDraws.id, "/usersDraws/list", defaultColunms);
    table.setPaginationType("server");
    UsersDraws.table = table.init();
});
/**
 * 审核通过
 */
UsersDraws.openUsersDrawsPass = function(){
    if (this.check()) {
        var userId = UsersDraws.seItem.userId;
        var money = UsersDraws.seItem.money;
        var drawId = UsersDraws.seItem.drawId;
        $.post('/usersDraws/recharge',
            {"userId": userId,"money":money,"drawId":drawId},
            function (req) {
                if(req == "success"){
                    Feng.success("操作成功！");
                    UsersDraws.table.refresh();
                }else{
                    Feng.error(req);

                }
            });
    };
};
/**
 * 审核拒绝
 */
UsersDraws.openUsersDrawsRefuse = function(){
	if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '请输入拒绝理由',
            area: ['400px', '250px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/usersDraws/usersDrawsRefuse?drawId=' + UsersDraws.seItem.drawId	//将昵称传到用户账户记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 订单处理中处理
 */
UsersDraws.openUsersDrawsInHand = function(){
	if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '银行处理中订单',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/usersDraws/usersDrawsInHand?drawId=' + UsersDraws.seItem.drawId	//将昵称传到用户账户记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 订单失败处理
 */
UsersDraws.openUsersDrawsDefeated = function(){
	if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '订单受理失败',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/usersDraws/usersDrawsDefeated?drawId=' + UsersDraws.seItem.drawId	//将昵称传到用户账户记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 线下打款
 */
UsersDraws.openUsersDrawsMoney = function(){
	if (this.check()) {
		var operation = function(){
			var drawId = UsersDraws.seItem.drawId;
	    	var ajax = new $ax(Feng.ctxPath + "/usersDraws/usersDrawsMoney", function (data) {
	            Feng.success("操作成功!");
	            UsersDraws.table.refresh();
	        }, function (data) {
	            Feng.error("操作失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("drawId",drawId);
	        ajax.start();
		};
		Feng.confirm("是否进行线下打款操作?",operation);
	}
};
/**
 * 批量审核拒绝
 */
UsersDraws.batchRefuse = function () {
    var index = layer.open({
        type: 2,
        title: '请输入拒绝理由',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/usersDraws/batchRefuse'
    });
    this.layerIndex = index;
}
//导出Excel
UsersDraws.export= function(){
	$('#'+UsersDraws.id).tableExport({
		fileName:'用户提现管理表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}