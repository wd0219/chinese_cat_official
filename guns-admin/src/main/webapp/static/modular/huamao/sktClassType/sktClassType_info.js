/**
 * 初始化视频类型详情对话框
 */
var SktClassTypeInfoDlg = {
    sktClassTypeInfoData : {}
};

/**
 * 清除数据
 */
SktClassTypeInfoDlg.clearData = function() {
    this.sktClassTypeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktClassTypeInfoDlg.set = function(key, val) {
    this.sktClassTypeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktClassTypeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktClassTypeInfoDlg.close = function() {
    parent.layer.close(window.parent.SktClassType.layerIndex);
}

/**
 * 收集数据
 */
SktClassTypeInfoDlg.collectData = function() {
    this
    .set('typeId')
    .set('typeName')
    .set('creatTime')
    .set('typeUrl');
}

/**
 * 提交添加
 */
SktClassTypeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktClassType/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktClassType.table.refresh();
        SktClassTypeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktClassTypeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktClassTypeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktClassType/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktClassType.table.refresh();
        SktClassTypeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktClassTypeInfoData);
    ajax.start();
}


$(function() {
    var adFile = new $WebUpload("typeUrl");
    adFile.setUploadBarId("progressBar");
    adFile.init();
});
