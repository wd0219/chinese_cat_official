/**
 * 视频类型管理初始化
 */
var SktClassType = {
    id: "SktClassTypeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktClassType.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'typeId', visible: false, align: 'center', valign: 'middle'},
            {title: '类型名称', field: 'typeName', visible: true, align: 'center', valign: 'middle'},
            {title: '类型图片', field: 'typeUrl', visible: true, align: 'center', valign: 'middle',formatter:SktClassType.showClassTypeImg},
            {title: '创建时间', field: 'creatTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'option', visible: true, align: 'center', valign: 'middle',formatter:SktClassType.op}
    ];
};
/*
 * 操作
 * */
SktClassType.op = function (value,row,index) {
    return "<a href='javascript:void(0)'onclick=\"SktClassType.openSktClassTypeDetail()\">修改</a>"
}
/**
 * 检查是否选中
 */
SktClassType.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktClassType.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加视频类型
 */
SktClassType.openAddSktClassType = function () {
    var index = layer.open({
        type: 2,
        title: '添加视频类型',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktClassType/sktClassType_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看视频类型详情
 */
SktClassType.openSktClassTypeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '视频类型详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktClassType/sktClassType_update/' + SktClassType.seItem.typeId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除视频类型
 */
SktClassType.delete = function () {
    if (this.check()) {
        var operation = function(){
            var  typeId=SktClassType.seItem.typeId;
            var ajax = new $ax(Feng.ctxPath + "/sktClassType/delete", function (data) {
                Feng.success("删除成功!");
                SktClassType.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("sktClassTypeId",typeId);
            ajax.start();

        };
        Feng.confirm("是否删除 该条记录?",operation);
    }
};

/**
 * 查询视频类型列表
 */
SktClassType.search = function () {
    var queryData = {};
    queryData['typeName'] = $("#typeName").val();
    SktClassType.table.refresh({query: queryData});
};

//图片显示
SktClassType.showClassTypeImg = function (value,row,index){
    const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
    if(value.indexOf('http')==-1){
        value=imgUrl+value
    }else{
        value=value
    }
    var img = '<img style="width:70;height:30px;"  src="'+value+'" />';
    return img;
}
$(function () {
    var defaultColunms = SktClassType.initColumn();
    var table = new BSTable(SktClassType.id, "/sktClassType/list", defaultColunms);
    table.setPaginationType("server");
    SktClassType.table = table.init();
});
