/**
 * 每日积分转化管理初始化
 */
var SktDateTransform = {
    id: "SktDateTransformTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktDateTransform.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '日期', field: 'date', visible: true, align: 'center', valign: 'middle'},
            {title: '比例（万分单位）', field: 'ratio', visible: true, align: 'center', valign: 'middle'},
            {title: '操作员工', field: 'staffId', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 0:
        			result = "系统";
        			break;
        		}
        		return result;
        	}},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: '#', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktDateTransform.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktDateTransform.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加每日积分转化
 */
SktDateTransform.openAddSktDateTransform = function () {
    var index = layer.open({
        type: 2,
        title: '添加每日积分转化',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktDateTransform/sktDateTransform_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看每日积分转化详情
 */
SktDateTransform.openSktDateTransformDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '每日积分转化详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktDateTransform/sktDateTransform_update/' + SktDateTransform.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除每日积分转化
 */
SktDateTransform.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktDateTransform/delete", function (data) {
            Feng.success("删除成功!");
            SktDateTransform.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktDateTransformId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询每日积分转化列表
 */
SktDateTransform.search = function () {
    var queryData = {};
    queryData['createTime'] = $("#createTime").val().replace(/-/g,"");
    queryData['date'] = $("#date").val();
    SktDateTransform.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktDateTransform.initColumn();
    var table = new BSTable(SktDateTransform.id, "/sktDateTransform/list", defaultColunms);
    table.setPaginationType("server");
    SktDateTransform.table = table.init();
});
