/**
 * 初始化每日积分转化详情对话框
 */
var SktDateTransformInfoDlg = {
    sktDateTransformInfoData : {}
};

/**
 * 清除数据
 */
SktDateTransformInfoDlg.clearData = function() {
    this.sktDateTransformInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktDateTransformInfoDlg.set = function(key, val) {
    this.sktDateTransformInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktDateTransformInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktDateTransformInfoDlg.close = function() {
    parent.layer.close(window.parent.SktDateTransform.layerIndex);
}

/**
 * 收集数据
 */
SktDateTransformInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('date')
    .set('ratio')
    .set('transformNum')
    .set('kaiyuan')
    .set('userNum')
    .set('staffId')
    .set('createTime')
    .set('handleStart')
    .set('handleEnd');
}

/**
 * 提交添加
 */
SktDateTransformInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var date = $("#date").val();
    var ratio = $("#ratio").val();
    
    if(date == null || date ==""){
        var html='<span class="error">时间不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#date').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(!isRealNum(ratio)){
        var html='<span class="error">比例（万分单位）必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#ratio').after(html);
        falg = 1;
    }


    this.clearData();
    this.collectData();
    
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktDateTransform/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktDateTransform.table.refresh();
        SktDateTransformInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    this.sktDateTransformInfoData.date = this.sktDateTransformInfoData.date.replace(/-/g,"");
    ajax.set(this.sktDateTransformInfoData);
    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
	}
	
	function isRealNum(val){
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	if(val === "" || val ==null){
	    return false;
	}
	if(!isNaN(val)){
	    return true;
	}else{
	    return false;
	}
}

/**
 * 提交修改
 */
SktDateTransformInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktDateTransform/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktDateTransform.table.refresh();
        SktDateTransformInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktDateTransformInfoData);
    ajax.start();
}

$(function() {

});
