/**
 * 用户每日对账管理初始化
 */
var CheckDetail = {
    id: "CheckDetailTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
CheckDetail.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户名', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '日期', field: 'checkDate', visible: true, align: 'center', valign: 'middle'},
            {title: '资金对账差异', field: 'zjCheckdiff', visible: true, align: 'center', valign: 'middle'},
            {title: '积分对账差异', field: 'jfCheckdiff', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝对账差异', field: 'kybCheckdiff', visible: true, align: 'center', valign: 'middle'},
            {title: '昨日资金', field: 'zjYesterday', visible: true, align: 'center', valign: 'middle'},
            {title: '今日资金', field: 'zjToday', visible: true, align: 'center', valign: 'middle'},
            {title: '今日业务增量', field: 'zjChange', visible: true, align: 'center', valign: 'middle'},
            {title: '门店订单支付金额', field: 'zjStorepay', visible: true, align: 'center', valign: 'middle'},
            {title: '商城订单支付金额', field: 'zjMallpay', visible: true, align: 'center', valign: 'middle'},
            //{title: '提现申请', field: 'zjTixianapply', visible: true, align: 'center', valign: 'middle'},
            //{title: '提现手续费', field: 'zjTixianfee', visible: true, align: 'center', valign: 'middle'},
            //{title: '提现不通过', field: 'zjTixiannopass', visible: true, align: 'center', valign: 'middle'},
            //{title: '退款', field: 'zjTuikuan', visible: true, align: 'center', valign: 'middle'},
            //{title: '', field: 'zjChongzhi', visible: true, align: 'center', valign: 'middle'},
            //{title: '昨日积分', field: 'jfYesterday', visible: true, align: 'center', valign: 'middle'},
            //{title: '今日积分', field: 'jfToday', visible: true, align: 'center', valign: 'middle'},
            //{title: '今日业务增量', field: 'jfChange', visible: true, align: 'center', valign: 'middle'},
            //{title: '积分转换', field: 'jfZhuanhuan', visible: true, align: 'center', valign: 'middle'},
            //{title: '门店消费获得', field: 'jfStorepay', visible: true, align: 'center', valign: 'middle'},
            //{title: '商城消费获得', field: 'jfMallpay', visible: true, align: 'center', valign: 'middle'},
            //{title: '邀请奖励', field: 'jfInvitereward', visible: true, align: 'center', valign: 'middle'},
            //{title: '其它', field: 'jfOther', visible: true, align: 'center', valign: 'middle'},
            //{title: '昨日华宝', field: 'kybYesterday', visible: true, align: 'center', valign: 'middle'},
            //{title: '今日华宝', field: 'kybToday', visible: true, align: 'center', valign: 'middle'},
            //{title: '今日业务增量', field: 'kybChange', visible: true, align: 'center', valign: 'middle'},
            //{title: '积分转换', field: 'kybZhuanhuan', visible: true, align: 'center', valign: 'middle'},
            //{title: '门店支付', field: 'kybStorepay', visible: true, align: 'center', valign: 'middle'},
            //{title: '门店支付手续费', field: 'kybStorepayfee', visible: true, align: 'center', valign: 'middle'},
            //{title: '商城支付', field: 'kybMallpay', visible: true, align: 'center', valign: 'middle'},
            //{title: '商城支付手续费', field: 'kybMallpayfee', visible: true, align: 'center', valign: 'middle'},
            //{title: '提现', field: 'kybTixianapply', visible: true, align: 'center', valign: 'middle'},
            //{title: '提现手续费', field: 'kybTixianfee', visible: true, align: 'center', valign: 'middle'},
            //{title: '提现不通过', field: 'kybTixiannopass', visible: true, align: 'center', valign: 'middle'},
            //{title: '华宝退款', field: 'kybTuikuan', visible: true, align: 'center', valign: 'middle'},
            //{title: '其它', field: 'kybOther', visible: true, align: 'center', valign: 'middle'}
            {title: '详情', field: '', visible: true, align: 'center', valign: 'middle',formatter:CheckDetail.op}
    ];
};
/*
 * 操作
 * */
CheckDetail.op = function () {
	return "<a href='javascript:void(0)'onclick=\"CheckDetail.openCheckDetailDetail()\">详情</a>"
}

/**
 * 检查是否选中
 */
CheckDetail.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CheckDetail.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户每日对账
 */
CheckDetail.openAddCheckDetail = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户每日对账',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/checkDetail/checkDetail_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户每日对账详情
 */
CheckDetail.openCheckDetailDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户每日对账详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/checkDetail/detail/' + CheckDetail.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户每日对账
 */
CheckDetail.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/checkDetail/delete", function (data) {
            Feng.success("删除成功!");
            CheckDetail.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("checkDetailId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户每日对账列表
 */
CheckDetail.search = function () {
    var queryData = {};
    queryData['loginName'] = $("#loginName").val();
    queryData['beginTime1'] = $("#beginTime").val();
    queryData['endTime1'] = $("#endTime").val();
    CheckDetail.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = CheckDetail.initColumn();
    var table = new BSTable(CheckDetail.id, "/checkDetail/list", defaultColunms);
    table.setPaginationType("server");
    CheckDetail.table = table.init();
});
//导出Excel
CheckDetail.export= function(){
	$('#'+CheckDetail.id).tableExport({
		fileName:'用户每日对账记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}