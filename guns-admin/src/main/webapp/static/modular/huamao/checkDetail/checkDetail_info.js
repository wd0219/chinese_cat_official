/**
 * 初始化用户每日对账详情对话框
 */
var CheckDetailInfoDlg = {
    checkDetailInfoData : {}
};

/**
 * 清除数据
 */
CheckDetailInfoDlg.clearData = function() {
    this.checkDetailInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CheckDetailInfoDlg.set = function(key, val) {
    this.checkDetailInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CheckDetailInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CheckDetailInfoDlg.close = function() {
    parent.layer.close(window.parent.CheckDetail.layerIndex);
}

/**
 * 收集数据
 */
CheckDetailInfoDlg.collectData = function() {
    this
    .set('id')
    .set('userId')
    .set('checkDate')
    .set('zjCheckdiff')
    .set('jfCheckdiff')
    .set('kybCheckdiff')
    .set('zjYesterday')
    .set('zjToday')
    .set('zjChange')
    .set('zjStorepay')
    .set('zjMallpay')
    .set('zjTixianapply')
    .set('zjTixianfee')
    .set('zjTixiannopass')
    .set('zjTuikuan')
    .set('zjChongzhi')
    .set('jfYesterday')
    .set('jfToday')
    .set('jfChange')
    .set('jfZhuanhuan')
    .set('jfStorepay')
    .set('jfMallpay')
    .set('jfInvitereward')
    .set('jfOther')
    .set('kybYesterday')
    .set('kybToday')
    .set('kybChange')
    .set('kybZhuanhuan')
    .set('kybStorepay')
    .set('kybStorepayfee')
    .set('kybMallpay')
    .set('kybMallpayfee')
    .set('kybTixianapply')
    .set('kybTixianfee')
    .set('kybTixiannopass')
    .set('kybTuikuan')
    .set('kybOther');
}

/**
 * 提交添加
 */
CheckDetailInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/checkDetail/add", function(data){
        Feng.success("添加成功!");
        window.parent.CheckDetail.table.refresh();
        CheckDetailInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.checkDetailInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
CheckDetailInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/checkDetail/update", function(data){
        Feng.success("修改成功!");
        window.parent.CheckDetail.table.refresh();
        CheckDetailInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.checkDetailInfoData);
    ajax.start();
}

$(function() {

});
