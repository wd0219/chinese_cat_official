/**
 * 初始化购物券流水详情对话框
 */
var SktShoppingInfoDlg = {
    sktShoppingInfoData : {}
};

/**
 * 清除数据
 */
SktShoppingInfoDlg.clearData = function() {
    this.sktShoppingInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShoppingInfoDlg.set = function(key, val) {
    this.sktShoppingInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShoppingInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktShoppingInfoDlg.close = function() {
    parent.layer.close(window.parent.SktShopping.layerIndex);
}

/**
 * 收集数据
 */
SktShoppingInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('dataId')
    .set('orderNo')
    .set('money')
    .set('remark')
    .set('dataFlag')
    .set('createTime')
    .set('overdueTime')
    .set('status')
    .set('sceneType')
    .set('shoppingType');
}

/**
 * 提交添加
 */
SktShoppingInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktShopping/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktShopping.table.refresh();
        SktShoppingInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShoppingInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktShoppingInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktShopping/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktShopping.table.refresh();
        SktShoppingInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShoppingInfoData);
    ajax.start();
}

$(function() {

});
