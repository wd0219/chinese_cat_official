/**
 * 购物券流水管理初始化
 */
var SktShopping = {
    id: "SktShoppingTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktShopping.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {
            title:'序号',align: 'center', valign: 'middle',width:50,
            formatter: function (value, row, index) {
                return index+1;
            }
        },
            {title: '用户身份', field: 'type', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    if(value == 1){
                        return "会员";
                    }else if(value == 2){
                        return "代理";
                    }else if(value == 3){
                        return "商家";
                    }
                    return "";
                }

            },
            {title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '对应订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '金额', field: 'money', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            // {title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '购物券过期时间', field: 'overdueTime', visible: true, align: 'center', valign: 'middle'},
            {title: '购物券状态', field: 'status', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    if(value == 1){
                        return "未使用";
                    }else if(value == 2){
                        return "已使用";
                    }else if(value == 3){
                        return "已过期";
                    }
                    return "";
                }
            },
            {title: '购物券使用场景', field: 'sceneType', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    if(value == 1){
                        return "通用";
                    }else if(value == 2){
                        return "线上";
                    }else if(value == 3){
                        return "线下";
                    }else if(value == 4){
                        return "无人超市";
                    }
                    return "";
                }
            },
            {title: '流水标志', field: 'shoppingType', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    if(value == -1){
                        return "减少";
                    }else if(value == 1){
                        return "增加";
                    }
                    return "";
                }
            }
    ];
};

/**
 * 检查是否选中
 */
SktShopping.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktShopping.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加购物券流水
 */
SktShopping.openAddSktShopping = function () {
    var index = layer.open({
        type: 2,
        title: '添加购物券流水',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktShopping/sktShopping_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看购物券流水详情
 */
SktShopping.openSktShoppingDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '购物券流水详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktShopping/sktShopping_update/' + SktShopping.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除购物券流水
 */
SktShopping.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktShopping/delete", function (data) {
            Feng.success("删除成功!");
            SktShopping.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktShoppingId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询购物券流水列表
 */
SktShopping.search = function () {
    debugger;
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['type'] = $("#type").val();
    SktShopping.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktShopping.initColumn();
    var table = new BSTable(SktShopping.id, "/sktShopping/list", defaultColunms);
    table.setPaginationType("server");
    SktShopping.table = table.init();
});
