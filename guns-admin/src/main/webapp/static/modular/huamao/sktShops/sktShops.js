/**
 * 商家信息管理初始化
 */
var SktShops = {
    id: "SktShopsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktShops.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '是否自营：1自营0非自营', field: 'isSelf', visible: true, align: 'center', valign: 'middle'},
            {title: '积分赠送比例 默认100 线上线下都使用', field: 'scoreRatio', visible: true, align: 'center', valign: 'middle'},
            {title: '默认运费', field: 'freight', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照所在省id', field: 'provinceId', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照所在市id', field: 'cityId', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照所在区县id', field: 'areaId', visible: true, align: 'center', valign: 'middle'},
            {title: '是否法人 1法人 0不是法人', field: 'isLegal', visible: true, align: 'center', valign: 'middle'},
            {title: '授权书', field: 'authorizationImg', visible: true, align: 'center', valign: 'middle'},
            {title: '法人', field: 'legalPerson', visible: true, align: 'center', valign: 'middle'},
            {title: '法人身份证号', field: 'IDnumber', visible: true, align: 'center', valign: 'middle'},
            {title: '法人手持身份证照片', field: 'legalPersonImg', visible: true, align: 'center', valign: 'middle'},
            {title: '公司名称', field: 'companyName', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照号码', field: 'licenseNo', visible: true, align: 'center', valign: 'middle'},
            {title: '企业类型：0个体户 1公司', field: 'companyType', visible: true, align: 'center', valign: 'middle'},
            {title: '是否三证合一：0未合并 1合并', field: 'licenseMerge', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照', field: 'licenseImg', visible: true, align: 'center', valign: 'middle'},
            {title: '组织机构代码证', field: 'codeImg', visible: true, align: 'center', valign: 'middle'},
            {title: '税务登记证', field: 'taxImg', visible: true, align: 'center', valign: 'middle'},
            {title: '行业ID', field: 'industry', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: 'logo', field: 'logo', visible: true, align: 'center', valign: 'middle'},
            {title: '联系人', field: 'linkman', visible: true, align: 'center', valign: 'middle'},
            {title: '经营电话', field: 'telephone', visible: true, align: 'center', valign: 'middle'},
            {title: '经营地址', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '经度', field: 'lng', visible: true, align: 'center', valign: 'middle'},
            {title: '纬度', field: 'lat', visible: true, align: 'center', valign: 'middle'},
            {title: '促销语', field: 'intro', visible: true, align: 'center', valign: 'middle'},
            {title: '企业内容', field: 'content', visible: true, align: 'center', valign: 'middle'},
            {title: '线下商家状态：1可用 0禁用', field: 'storeStatus', visible: true, align: 'center', valign: 'middle'},
            {title: '线上商家状态：1可用 0禁用', field: 'shopStatus', visible: true, align: 'center', valign: 'middle'},
            {title: '服务营业时间', field: 'startTime', visible: true, align: 'center', valign: 'middle'},
            {title: '服务结束时间', field: 'endTime', visible: true, align: 'center', valign: 'middle'},
            {title: '线下店铺展示图1', field: 'storeImg1', visible: true, align: 'center', valign: 'middle'},
            {title: '线下店铺展示图2', field: 'storeImg2', visible: true, align: 'center', valign: 'middle'},
            {title: '线下店铺展示图3', field: 'storeImg3', visible: true, align: 'center', valign: 'middle'},
            {title: '线上商城展示图1', field: 'shopImg1', visible: true, align: 'center', valign: 'middle'},
            {title: '线上商城展示图2', field: 'shopImg2', visible: true, align: 'center', valign: 'middle'},
            {title: '线上商城展示图3', field: 'shopImg3', visible: true, align: 'center', valign: 'middle'},
            {title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktShops.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktShops.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商家信息
 */
SktShops.openAddSktShops = function () {
    var index = layer.open({
        type: 2,
        title: '添加商家信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktShops/sktShops_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商家信息详情
 */
SktShops.openSktShopsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商家信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktShops/sktShops_update/' + SktShops.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商家信息
 */
SktShops.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktShops/delete", function (data) {
            Feng.success("删除成功!");
            SktShops.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktShopsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商家信息列表
 */
SktShops.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktShops.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktShops.initColumn();
    var table = new BSTable(SktShops.id, "/sktShops/list", defaultColunms);
    table.setPaginationType("client");
    SktShops.table = table.init();
});
