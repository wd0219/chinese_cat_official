/**
 * 初始化商家信息详情对话框
 */
var SktShopsInfoDlg = {
	sktShopsInfoData : {}

};

/**
 * 清除数据
 */
SktShopsInfoDlg.clearData = function() {
	this.sktShopsInfoData = {};
}

/**
 * 设置对话框中的数据
 * 
 * @param key
 *            数据的名称
 * @param val
 *            数据的具体值
 */
SktShopsInfoDlg.set = function(key, val) {
	this.sktShopsInfoData[key] = (typeof val == "undefined") ? $("#" + key)
			.val() : val;
	return this;
}

/**
 * 设置对话框中的数据
 * 
 * @param key
 *            数据的名称
 * @param val
 *            数据的具体值
 */
SktShopsInfoDlg.get = function(key) {
	return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktShopsInfoDlg.close = function() {
	parent.layer.close(window.parent.SktShopAccreds.layerIndex);
}

/**
 * 收集数据
 */
SktShopsInfoDlg.collectData = function() {
	this.set('shopId').set('userId').set('isSelf').set('scoreRatio').set(
			'freight').set('provinceId').set('cityId').set('areaId').set(
			'isLegal').set('authorizationImg').set('legalPerson').set(
			'IDnumber').set('legalPersonImg').set('companyName').set(
			'licenseNo').set('companyType').set('licenseMerge').set(
			'licenseImg').set('codeImg').set('taxImg').set('industry').set(
			'shopName').set('logo').set('linkman').set('telephone').set(
			'address').set('lng').set('lat').set('intro').set('content').set(
			'storeStatus').set('shopStatus').set('startTime').set('endTime')
			.set('storeImg1').set('storeImg2').set('storeImg3').set('shopImg1')
			.set('shopImg2').set('shopImg3').set('dataFlag').set('createTime')
			.set('businessQuota');
}
/**
 * 提交添加
 */
SktShopsInfoDlg.addSubmit = function() {

	this.clearData();
	this.collectData();

	// 提交信息
	var ajax = new $ax(Feng.ctxPath + "/sktShops/add", function(data) {
		Feng.success("添加成功!");
		window.parent.SktShops.table.refresh();
		SktShopsInfoDlg.close();
	}, function(data) {
		Feng.error("添加失败!" + data.responseJSON.message + "!");
	});
	ajax.set(this.sktShopsInfoData);
	ajax.start();
}

/**
 * 提交修改  线下商家
 */
SktShopsInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var shopName = $("#shopName").val();
    var telephone = $("#telephone").val();
    var address = $("#address").val();

    if(shopName == null || shopName ==""){
        var html='<span class="error">店铺名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#shopName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(address == null || address ==""){
        var html='<span class="error">详细地址不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#address').after(html);
        falg = 1;
    }

    if(!isRealNum(telephone)){
        var html='<span class="error">经营电话必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#telephone').after(html);
        falg = 1;
    }

	this.clearData();
	this.collectData();
	// 提交信息
	let
	isSelf = $('input:radio[name=isSelf]:checked').val();
	let
	storeStatus = $('input:radio[name=storeStatus]:checked').val();
	let
	companyType = $('input:radio[name=companyType]:checked').val();
	let
	shopStatus = $('input:radio[name=shopStatus]:checked').val();
	let
	licenseMerge = $('input:radio[name=licenseMerge]:checked').val();
	let
	goods1 = $('#goods1').val();
	
	this.sktShopsInfoData.isSelf = isSelf;
	this.sktShopsInfoData.storeStatus = storeStatus;
	this.sktShopsInfoData.shopStatus = shopStatus;
	this.sktShopsInfoData.companyType = companyType;
	this.sktShopsInfoData.licenseMerge = licenseMerge;
	this.sktShopsInfoData.industry = goods1;
	if(falg == 0){
	var ajax = new $ax(Feng.ctxPath + "/sktShops/update", function(data) {
		Feng.success("修改成功!");
		window.parent.SktShopAccreds.table.refresh();
		SktShopsInfoDlg.close();
	}, function(data) {
		Feng.error("修改失败!" + data.responseJSON.message + "!");
	});
	ajax.set(this.sktShopsInfoData);
	ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
	}
/**
 * 提交修改  线上商家
 */
SktShopsInfoDlg.editSubmit1 = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var shopName = $("#shopName").val();
    var telephone = $("#telephone").val();
    var address = $("#address").val();

    if(shopName == null || shopName ==""){
        var html='<span class="error">店铺名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#shopName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(address == null || address ==""){
        var html='<span class="error">详细地址不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#address').after(html);
        falg = 1;
    }

    if(!isRealNum(telephone)){
        var html='<span class="error">经营电话必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#telephone').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    // 提交信息
    let
        isSelf = $('input:radio[name=isSelf]:checked').val();
    let
        storeStatus = $('input:radio[name=storeStatus]:checked').val();
    let
        companyType = $('input:radio[name=companyType]:checked').val();
    let
        shopStatus = $('input:radio[name=shopStatus]:checked').val();
    let
        licenseMerge = $('input:radio[name=licenseMerge]:checked').val();
    let
        goods1 = $('#goods1').val();

    this.sktShopsInfoData.isSelf = isSelf;
    this.sktShopsInfoData.storeStatus = storeStatus;
    this.sktShopsInfoData.shopStatus = shopStatus;
    this.sktShopsInfoData.companyType = companyType;
    this.sktShopsInfoData.licenseMerge = licenseMerge;
    this.sktShopsInfoData.industry = goods1;
    if(falg == 0){
        var ajax = new $ax(Feng.ctxPath + "/sktShops/update", function(data) {
            Feng.success("修改成功!");
            window.parent.SktShopAccred.table.refresh();
            SktShopsInfoDlg.close();
        }, function(data) {
            Feng.error("修改失败!" + data.responseJSON.message + "!");
        });
        ajax.set(this.sktShopsInfoData);
        ajax.start();
    }else{
        Feng.success("请核实添加字段!");
    }
}

		function isRealNum(val){
		    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
		    if(val === "" || val ==null){
		        return false;
		    }
		    if(!isNaN(val)){
		        return true;
		    }else{
		        return false;
		    }
		}


$(function() {
	/*
	 * // 初始化头像上传 var avatarUp = new $WebUpload("legalPersonImg");
	 * avatarUp.setUploadBarId("progressBar"); avatarUp.init();
	 * UsersInfoDlg.getTuiName();
	 */
	for(let e of document.getElementsByClassName('upload-btn')){
		let id = $(e).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
			uploader = new $WebUpload(id);
		uploader.setUploadBarId(id);
		uploader.init();
	}
    var shopType=$('#shopType').val();
    if (shopType==0){

        $('#shopStatus').parent().hide()
    }
    if (shopType==1){

        $('#storeStatus').parent().hide()
    }
//	$('.uploader').each(function() {
//		let id = $(this).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
//		console.log(id);
//	    let uploader = new window.$WebUploader(id);
//		
//		
//		uploader.setUploadBarId(id);
//		uploader.init();
//	});
});
