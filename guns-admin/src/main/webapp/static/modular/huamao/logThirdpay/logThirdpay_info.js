/**
 * 初始化第三方支付记录详情对话框
 */
var LogThirdpayInfoDlg = {
    logThirdpayInfoData : {}
};

/**
 * 清除数据
 */
LogThirdpayInfoDlg.clearData = function() {
    this.logThirdpayInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogThirdpayInfoDlg.set = function(key, val) {
    this.logThirdpayInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogThirdpayInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogThirdpayInfoDlg.close = function() {
    parent.layer.close(window.parent.LogThirdpay.layerIndex);
}

/**
 * 收集数据
 */
LogThirdpayInfoDlg.collectData = function() {
    this
    .set('id')
    .set('userId')
    .set('orderNo')
    .set('businessType')
    .set('payType')
    .set('ino')
    .set('payMoney')
    .set('outTradeNo')
    .set('request')
    .set('payTypeOrderNo')
    .set('response')
    .set('responseTime')
    .set('payStatus')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogThirdpayInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logThirdpay/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogThirdpay.table.refresh();
        LogThirdpayInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logThirdpayInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogThirdpayInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logThirdpay/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogThirdpay.table.refresh();
        LogThirdpayInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logThirdpayInfoData);
    ajax.start();
}

$(function() {

});
