/**
 * 第三方支付记录管理初始化
 */
var LogThirdpay = {
    id: "LogThirdpayTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogThirdpay.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '支付类型', field: 'payType', visible: true, align: 'center', valign: 'middle',formatter:LogThirdpay.payType},
            {title: '内部业务订单', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '第三方支付流水单号', field: 'outTradeNo', visible: true, align: 'center', valign: 'middle'},
            {title: '业务类型', field: 'businessType', visible: true, align: 'center', valign: 'middle',formatter:LogThirdpay.businessType},
            {title: '支付金额', field: 'payMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '请求数据', field: 'request', visible: true, align: 'center', valign: 'middle'},
            {title: '请求创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '响应数据', field: 'response', visible: true, align: 'center', valign: 'middle'},
            {title: '响应时间', field: 'responseTime', visible: true, align: 'center', valign: 'middle'},
            {title: '支付状态', field: 'payStatus', visible: true, align: 'center', valign: 'middle',formatter:LogThirdpay.payStatus},
            {title: '是否手动补单', field: 'handleStatus', visible: true, align: 'center', valign: 'middle',formatter:LogThirdpay.handleStatus},
            //{title: '商户号', field: 'ino', visible: true, align: 'center', valign: 'middle'},
            //{title: '银行流水号或支付渠道号', field: 'payTypeOrderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '删除标记 0删除 1显示）', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
    ];
};
/**
 * 业务类型对应：BUY_GOODS商城消费>BUY_SCORE申请库存积分>RECHARGE充值>
 * UPGRADE升级>BUY_PLAQUE购买牌匾
 */
LogThirdpay.businessType = function(value,row,index){
	if(value == "BUY_GOODS"){
		return "【商城消费】";
	}else if(value == "BUY_SCORE"){
		return "【申请库存积分】";
	}else if(value == "RECHARGE"){
		return "【充值】";
	}else if(value == "UPGRADE"){
		return "【升级】";
	}else if(value == "BUY_PLAQUE"){
		return "【购买牌匾】";
	}
}
/**
 * 支付类型对应：1微信>2支付宝>3快捷>4网关
 */
LogThirdpay.payType = function(value,row,index){
	if(value == 1){
		return "【微信】";
	}else if(value == 2){
		return "【支付宝】";
	}else if(value == 3){
		return "【快捷】";
	}else if(value == 4){
		return "【网关】";
	}
}
/**
 *支付状态对应：0未请求>1请求中>2接收异步通知中>3支付成功>4处理失败>5重复支付
 */
LogThirdpay.payStatus = function(value,row,index){
	if(value == 0){
		return "【未请求】";
	}else if(value == 1){
		return "【请求中】";
	}else if(value == 2){
		return "【接收异步通知中】";
	}else if(value == 3){
		return "【支付成功】";
	}else if(value == 4){
		return "【处理失败】";
	}else if(value == 5){
		return "【重复支付】";
	}
}
/**
 * 是否手动补单对应：0正常>1手动补单
 */
LogThirdpay.handleStatus = function(value,row,index){
	if(value == 0){
		return "【订单正常】";
	}else if(value == 1){
		return "【手动补单】";
	}
}
/**
 * 检查是否选中
 */
LogThirdpay.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogThirdpay.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加第三方支付记录
 */
LogThirdpay.openAddLogThirdpay = function () {
    var index = layer.open({
        type: 2,
        title: '添加第三方支付记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logThirdpay/logThirdpay_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看第三方支付记录详情
 */
LogThirdpay.openLogThirdpayDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '第三方支付记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logThirdpay/logThirdpay_update/' + LogThirdpay.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除第三方支付记录
 */
LogThirdpay.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logThirdpay/delete", function (data) {
            Feng.success("删除成功!");
            LogThirdpay.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logThirdpayId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询第三方支付记录列表
 */
LogThirdpay.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['orderNo'] = $("#orderNo").val();
    queryData['outTradeNo'] = $("#outTradeNo").val();
    queryData['businessType'] = $("#businessType").val();
    queryData['payType'] = $("#payType").val();
    queryData['payStatus'] = $("#payStatus").val();
    queryData['handleStatus'] = $("#handleStatus").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogThirdpay.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogThirdpay.initColumn();
    var table = new BSTable(LogThirdpay.id, "/logThirdpay/list", defaultColunms);
    table.setPaginationType("server");
    LogThirdpay.table = table.init();
});
//导出Excel
LogThirdpay.export= function(){
	$('#'+LogThirdpay.id).tableExport({
		fileName:'第三方支付记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}