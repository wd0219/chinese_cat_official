/**
 * 用户管理管理初始化
 */
var Users = {
    id: "UsersTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Users.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
         {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '账号/昵称', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '角色', field: 'userType', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	//0普通 1主管 2经理
            	if(value=='0'){
            		return "普通用户";
            	}else if(value=='1'){
            		return "主管";
            	}else if(value=='2'){
            		return "经理";
            	}
            }},
            {title: '真实姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            {title: '推荐人姓名', field: 'tuiName', visible: true, align: 'center', valign: 'middle'},
            {title: '推荐人电话', field: 'tuiPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '是否线下商家', field: 'isStore', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	if(value=='0'){
            		return "否";
            	}else if(value=='1'){
            		return "是";
            	}
            }},
            {title: '是否线上商家', field: 'isShop', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	if(value=='0'){
            		return "否";
            	}else if(value=='1'){
            		return "是";
            	}
            }},
            {title: '是否代理股东', field: 'isAgent', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	//0不是股东 1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
            	var result= '';
            	switch(value){
            	case 1:
            		result = "董事长";
            		break;
            	case 6:
            		result = "监事";
            		break;
            	case 2:
            		result = "总裁";
            		break;
            	case 3:
            		result = "行政总监";
            		break;
            	case 4:
            		result = "财务总监";
            		break;
            	case 5:
            		result = "部门经理";
            		break;
            	case 7:
            		result = "普通股东"
            		break;
            	}
            	return result;
            }},
            {title: '注册时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'userStatus', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	if(value=='0'){
            		return "停用";
            	}else if(value=='1'){
            		return "启用";
            	}
            }},
            {title: '特权账号', field: 'isPrivilege', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	if(value=='0'){
            		return "否";
            	}else if(value=='1'){
            		return "是";
            	}
            }},
        {title: '是否允许提现', field: 'isWithdrawCash', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
                if(value=='2'){
                    return "否";
                }else if(value=='1'){
                    return "是";
                }
            }},
        {title: '是否允许转化积分', field: 'isOperation', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
                if(value=='0'){
                    return "否";
                }else if(value=='1'){
                    return "是";
                }
            }},
            {title: '操作', field: 'op', visible: true, align: 'center', valign: 'middle',formatter:Users.op},
    ];
};
/**
 * 操作列表
 */
Users.op = function(value,row,index){
	return "<a href='javascript:void(0)' onclick=\"Users.openUsersDetail()\">修改</a>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"Users.openUsersTuiDetail("+row.inviteId+")\">推荐列表</a>"
}

/**
 * 检查是否选中
 */
Users.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Users.seItem = selected[0];
        return true;
    }
};
/**
 * 显示推荐人列表
 */
Users.openUsersTuiDetail = function(inviteId){
	if (this.check()) {
		var ajax = new $ax(Feng.ctxPath + "/users/tuiList/"+inviteId, function (data) {
	        $('#'+Users.id).bootstrapTable('load',data); 
	        Feng.success("加载成功!");
	    }, function (data) {
	        Feng.error("加载失败!");
	    });
	    ajax.start();
	}else{
    	return;
    }
}
/**
 * 点击添加用户管理
 */
Users.openAddUsers = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/users/users_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户管理详情
 */
Users.openUsersDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/users/users_update/' + Users.seItem.userId
        });
        this.layerIndex = index;
    }else{
    	return;
    }
};

/**
 * 删除用户管理
 */
Users.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/users/delete", function (data) {
            Feng.success("删除成功!");
            Users.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("usersId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户管理列表
 */
Users.search = function () {
    var queryData = {};
    queryData['loginName'] = $("#loginName").val();
    queryData['userPhone'] = $("#userPhone").val();
    queryData['tuiPhone'] = $("#tuiPhone").val();
    queryData['tuiName'] = $("#tuiName").val();
    queryData['userType'] = $("#userType").val();
    Users.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Users.initColumn();
    var table = new BSTable(Users.id, "/users/list", defaultColunms);
    table.setPaginationType("client");
    Users.table = table.init();
});
