/**
 * 初始化用户管理详情对话框
 */
var UsersInfoDlg = {
    usersInfoData : {}
};

/**
 * 清除数据
 */
UsersInfoDlg.clearData = function() {
    this.usersInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersInfoDlg.set = function(key, val) {
    this.usersInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UsersInfoDlg.close = function() {
    parent.layer.close(window.parent.Users.layerIndex);
}

/**
 * 收集数据
 */
UsersInfoDlg.collectData = function() {
    this
    .set('userId')
    .set('loginName')
    .set('userPhoto')
    .set('userPhone')
    .set('trueName')
    .set('loginPwd')
    .set('userType')
    .set('isStore')
    .set('isShop')
    .set('isAgent')
    .set('tuiPhone')
    .set('inviteId')
    .set('tuiName')
    .set('userStatus')
    .set('isPrivilege')
	.set('isWithdrawCash')
	.set('isOperation')
}

/**
 * 提交添加
 */
UsersInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/users/add", function(data){
    	if(data=="FALSE"){
    		Feng.error("不能提交空数据或者添加失败!" + data.responseJSON.message + "!");
    	}else{
    		Feng.success("添加成功!");
            window.parent.Users.table.refresh();
    	}
        UsersInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UsersInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var loginName = $("#loginName").val();
    var userPhone = $("#userPhone").val();
    
    if(loginName == null || loginName ==""){
        var html='<span class="error">账户/昵称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#loginName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(!isRealNum(userPhone)){
        var html='<span class="error">手机号码必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#userPhone').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();

    var userStatus = $("input:radio[name=userStatus]:checked").val();
    var isPrivilege = $("input:radio[name=isPrivilege]:checked").val();
    var isStore = $("input:radio[name=isStore]:checked").val();
    var isShop = $("input:radio[name=isShop]:checked").val();
    var isWithdrawCash = $("input:radio[name=isWithdrawCash]:checked").val();
    var isOperation = $("input:radio[name=isOperation]:checked").val();
    var loginPwd = $('#loginPwd').val();
    if(loginPwd == ''){
    	 this.usersInfoData.loginPwd = $('#loginOld').val();
    }

    this.usersInfoData.userStatus = userStatus;
    this.usersInfoData.isPrivilege = isPrivilege;
    this.usersInfoData.isStore = isStore;
    this.usersInfoData.isShop = isShop;
    this.usersInfoData.shijianTime = $('#createTime').val();
    this.usersInfoData.isWithdrawCash = isWithdrawCash;
    this.usersInfoData.isOperation = isOperation;
    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/users/update", function(data){
	    	if(data=='REPEAT_PHONE'){
	    		Feng.success("登录名重复!");
	    	}else if(data == 'REPEAT_PHONE'){
	    		Feng.success("电话号重复!");
	    	}else if(data == 'SUCCESS'){
	    		Feng.success("修改成功!");
	            window.parent.Users.table.refresh();
	            UsersInfoDlg.close();
	    	}else{
                Feng.error("修改失败!");
			}
	    },function(data){
	        Feng.error("修改失败!");
	    });
	    ajax.set(this.usersInfoData);
	    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
	}
	
	function isRealNum(val){
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	if(val === "" || val ==null){
	    return false;
	}
	if(!isNaN(val)){
	    return true;
	}else{
	    return false;
	}
}
//获得推荐人id、name
UsersInfoDlg.getTuiName = function(){
	$('#tuiPhone').blur(function(){
		if($(this).val()!=""){
			var ajax = new $ax(Feng.ctxPath + "/users/selectTuiUsersName/"+$(this).val(), function(data){
				$('#tuiName').val(data.trueName);
				$('#inviteId').val(data.userId);
		    },function(data){
		        Feng.error("修改失败!" + data.responseJSON.message + "!");
		    });
		    ajax.start();
		}
	});
}
$(function() {
	// 初始化头像上传
    var avatarUp = new $WebUpload("userPhoto");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.init();
    UsersInfoDlg.getTuiName();
    
    
    
  //0普通 1主管 2经理
	var userType = $('#userType').val();
	switch(userType){
	case "0":
		$('#userType2').val("普通用户");
		$('#userType2').attr('disabled',true);
		break;
	case "1":
		$('#userType2').val("主管");
		$('#userType2').attr('disabled',true);
		break;
	case "2":
		$('#userType2').val("经理");
		$('#userType2').attr('disabled',true);
		break;
	default:
		break;
	}
	
	
	var isAgent = $('#isAgent').val();
	switch(isAgent){
	//0不是股东 1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
	case '0':
		$('#isAgent2').val("不是股东");
		$('#isAgent2').attr('disabled',true);
		break;
	case '1':
		$('#isAgent2').val("董事长");
		$('#isAgent2').attr('disabled',true);
		break;
	case '6':
		$('#isAgent2').val("监事");
		$('#isAgent2').attr('disabled',true);
		break;
	case '2':
		$('#isAgent2').val("总裁");
		$('#isAgent2').attr('disabled',true);
		break;
	case '3':
		$('#isAgent2').val("行政总监");
		$('#isAgent2').attr('disabled',true);
		break;
	case '4':
		$('#isAgent2').val("财务总监");
		$('#isAgent2').attr('disabled',true);
		break;
	case '5':
		$('#isAgent2').val("部门经理");
		$('#isAgent2').attr('disabled',true);
		break;
	case '7':
		$('#isAgent2').val("普通股东");
		$('#isAgent2').attr('disabled',true);
		break;
	}
});
