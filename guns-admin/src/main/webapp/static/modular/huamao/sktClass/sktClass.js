/**
 * 商学院管理初始化
 */
var SktClass = {
    id: "SktClassTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktClass.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '序号', field: 'classId', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row, index) {
                    return index+1;
                }},
            {title: '视频课程名称', field: 'className', visible: true, align: 'center', valign: 'middle'},
            {title: '视频图片', field: 'classImg', visible: true, align: 'center', valign: 'middle',formatter:SktClass.showImg},
            {title: '视频内容', field: 'classUrl', visible: false, align: 'center', valign: 'middle',formatter:SktClass.showvideo},
            {title: '视频存储路径', field: 'fileName', visible: false, align: 'center', valign: 'middle'},
            {title: '视频标题', field: 'classTitle', visible: true, align: 'center', valign: 'middle'},
            {title: '视频点击率(预留字段)', field: 'clickRate', visible: false, align: 'center', valign: 'middle'},
            {title: '视频点赞数(预留字段)', field: 'pointNumber', visible: false, align: 'center', valign: 'middle'},
            {title: '视频评论id', field: 'commentId', visible: false, align: 'center', valign: 'middle'},
            {title: '视频作者', field: 'creatWriter', visible: true, align: 'center', valign: 'middle'},
            {title: '视频介绍', field: 'introduce', visible: true, align: 'center', valign: 'middle'},
            {title: '视频备注(不通过写出原因)', field: 'remark', visible: false, align: 'center', valign: 'middle'},
            {title: '课程状态', field: 'istatus', visible: false, align: 'center', valign: 'middle',formatter:function(value,row,index){
                    //:0未通过,1通过,2不通过
                    if(value=='0'){
                        return "未通过";
                    }else if(value=='1'){
                        return "通过";
                    }else if(value=='2'){
                        return "不通过";
                    }
                }},
            {title: '视频类型', field: 'typeName', visible: true, align: 'center', valign: 'middle'},
            {title: '上传时间', field: 'creatTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核时间', field: 'checkTime', visible: false, align: 'center', valign: 'middle'},
        {title: '操作', field: 'op', visible: true, align: 'center', valign: 'middle',formatter:SktClass.op},
    ];
};

/**
 * 操作列表
 */
SktClass.op = function(){
    return "<a href='javascript:void(0)' onclick=\"SktClass.openSktClassDetail()\">修改</a> &nbsp;&nbsp;  <a href='javascript:void(0)' onclick=\"SktClass.delete()\">删除</a>"
}

//图片显示
SktClass.showImg = function (value,row,index){
    const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
    if(value.indexOf('http')==-1){
        value=imgUrl+value
    }else{
        value=value
    }
    var img = '<img style="width:70;height:30px;"  src="'+value+'" />';
    return img;
}

//图片显示
SktClass.showvideo = function (value,row,index){
       var result =  '<video width="200px" height="200px" controls >'+
             '<source src="'+row.classUrl+'" type="video/ogg">'+
           '<source src="'+row.classUrl+'" type="video/mp4">'+
            '<source src="'+row.classUrl+'" type="video/webm">'+
            '<object data="movie.mp4" width="200px" height="200px">'+
               ' <embed width="200px" height="200px" src="'+row.classUrl+'">'+

            '</object>'+
        '</video>';
       return result;

}
/**
 * 检查是否选中
 */
SktClass.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktClass.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商学院
 */
SktClass.openAddSktClass = function () {
    var index = layer.open({
        type: 2,
        title: '添加商学院',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktClass/sktClass_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商学院详情
 */
SktClass.openSktClassDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商学院详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktClass/sktClass_update/' + SktClass.seItem.classId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商学院
 */
SktClass.delete = function () {
    if (this.check()) {
        var operation=function () {
            var sktClassId=SktClass.seItem.classId;
            var ajax = new $ax(Feng.ctxPath + "/sktClass/delete", function (data) {
                Feng.success("删除成功!");
                SktClass.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("sktClassId",sktClassId);
            ajax.start();
        };
        Feng.confirm("是否删除该条记录?",operation);
    }
};

/**
 * 查询商学院列表
 */
SktClass.search = function () {
    var queryData = {};
    queryData['className'] = $("#className").val();
    queryData['creatWriter'] = $("#creatWriter").val();
    queryData['introduce'] = $("#introduce").val();
    queryData['classTitle'] = $("#classTitle").val();
    queryData['classType'] = $("#classType").val();
    queryData['startCreatTime'] = $("#startCreatTime").val();
    queryData['endCreatTime'] = $("#endCreatTime").val();
    queryData['startCheckTime'] = $("#startCheckTime").val();
    queryData['endCheckTime'] = $("#endCheckTime").val();
    SktClass.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktClass.initColumn();
    var table = new BSTable(SktClass.id, "/sktClass/list", defaultColunms);
    table.setPaginationType("server");
    SktClass.table = table.init();

});
