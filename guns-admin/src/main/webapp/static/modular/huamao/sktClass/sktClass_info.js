/**
 * 初始化商学院详情对话框
 */
var SktClassInfoDlg = {
    sktClassInfoData : {}
};

/**
 * 清除数据
 */
SktClassInfoDlg.clearData = function() {
    this.sktClassInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktClassInfoDlg.set = function(key, val) {
    this.sktClassInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktClassInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktClassInfoDlg.close = function() {
    parent.layer.close(window.parent.SktClass.layerIndex);
}

/**
 * 收集数据
 */
SktClassInfoDlg.collectData = function() {
    this
    .set('classId')
    .set('className')
    .set('classImg')
    .set('classUrl')
    .set('classTitle')
    .set('clickRate')
    .set('pointNumber')
    .set('commentId')
    .set('creatWriter')
    .set('introduce')
    .set('remark')
    .set('istatus')
    .set('classType')
    .set('fileName')
   ;
}

/**
 * 提交添加
 */
SktClassInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;
    var className=$("#className").val();
    if (className==null || className==''){
        var html='<span class="error">视频课程名称!</span>';

        $('#className').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    var creatWriter=$("#creatWriter").val();
    if (creatWriter==null || creatWriter==''){
        var html='<span class="error">视频作者不能为空!</span>';

        $('#creatWriter').after(html);
        falg = 1;
    }
    var introduce=$("#introduce").val();
    if (introduce==null || introduce==''){
        var html='<span class="error">视频介绍不能为空!</span>';

        $('#introduce').after(html);
        falg = 1;
    }
    var classTitle=$("#classTitle").val();
    if (classTitle==null || classTitle==''){
        var html='<span class="error">视频标题不能为空!</span>';

        $('#classTitle').after(html);
        falg = 1;
    }
    var classImg=$("#classImg").val();
   /* if (classImg==null || classImg==''){
        var html='<span class="error">视频图片不能为空!</span>';

        $('#classImg').after(html);
        falg = 1;
    }*/
    var classType=$("#classType").val();
    if (classType==null || classType==''){
        var html='<span class="error">视频类型不能为空!</span>';
        $('#classType').after(html);
       // $('#classTypeSpan').html('视频类型不能为空!');
        falg = 1;
    }

    var fileName=$('#classUrl').val();
    if (fileName==null || fileName==''){
        var html='<span class="error">视频不能为空!</span>';
        $('#fileName').after(html);
        // $('#classTypeSpan').html('视频类型不能为空!');
        falg = 1;
    }

    if (falg == 0){
        this.clearData();
        this.collectData();

        //提交信息
        var ajax = new $ax(Feng.ctxPath + "/sktClass/add", function(data){
            if (data.code=='-1'){
                Feng.error(data.msg);
                return;
            }
            Feng.success("添加成功!");
            window.parent.SktClass.table.refresh();
            SktClassInfoDlg.close();
        },function(data){
            Feng.error("添加失败!" + data.responseJSON.message + "!");
        });
        ajax.set(this.sktClassInfoData);
        ajax.start();
    }else{
        Feng.error("请核实添加字段!");
    }

}

/**
 * 提交修改
 */
SktClassInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktClass/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktClass.table.refresh();
        SktClassInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktClassInfoData);
    ajax.start();
}

$(function() {
    for(let e of document.getElementsByClassName('upload-btn')){
        let id = $(e).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
        uploader = new $WebUpload(id);
        uploader.setUploadBarId(id);
        uploader.init();
    }
    // $('#classType').val($('#classTypeh').val());
    var sktClassTypeId = $("#classTypeh").val();
    $.post('/sktClassType/getAll/',
        {},
        function (req) {
            for(var i in req){
                if(req[i].typeId == sktClassTypeId){
                    $("#classType").append("<option value="+req[i].typeId+" selected>"+req[i].typeName+"</option>");
                }else {
                    $("#classType").append("<option value=" + req[i].typeId + ">" + req[i].typeName + "</option>");
                }
            }
        });
});
