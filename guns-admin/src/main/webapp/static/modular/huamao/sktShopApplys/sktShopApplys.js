/**
 * 商家开店申请管理初始化
 */
var SktShopApplys = {
    id: "SktShopApplysTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktShopApplys.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '申请人', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '法人姓名', field: 'legalPerson', visible: true, align: 'center', valign: 'middle'},
            {title: '工商登记名称', field: 'companyName', visible: true, align: 'center', valign: 'middle'},
            {title: '公司电话', field: 'telephone', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '申请状态', field: 'applyStatus', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "未审核";
        			break;
        		case 2:
        			result = "拒绝 ";
        			break;
        		case 3:
        			result = "已审核";
        			break;
        		}
        		return result;
        		
        	}},
            {title: '申请时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核人', field: 'checkStaffId', visible: true, align: 'center', valign: 'middle'},
            {title: '审核通过时间', field: 'checkTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'applyStatus', visible: true, align: 'center', valign: 'middle',formatter:SktShopApplys.op}
            
    ];
};
/*
 * 操作
 * */
SktShopApplys.op = function (value,ros,index) {
	if(value==1){
	return "<a href='javascript:void(0)' onclick=\"SktShopApplys.openSktShopApplysDetail2()\">处理</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktShopApplys.openSktShopApplysDetail()\">详情</a>"
	}else{
		return "<a href='javascript:void(0)'onclick=\"SktShopApplys.openSktShopApplysDetail()\">详情</a>"
	}
	}
/**
 * 检查是否选中
 */
SktShopApplys.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktShopApplys.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商家开店申请
 */
SktShopApplys.openAddSktShopApplys = function () {
    var index = layer.open({
        type: 2,
        title: '添加商家开店申请',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktShopApplys/sktShopApplys_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商家开店申请详情
 */
SktShopApplys.openSktShopApplysDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商家开店申请详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktShopApplys/detail/' + SktShopApplys.seItem.applyId
        });
        this.layerIndex = index;
    }
};
/**
 * 打开查看商家开店申请详情2 处理
 */
SktShopApplys.openSktShopApplysDetail2 = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商家开店申请详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktShopApplys/detail2/' + SktShopApplys.seItem.applyId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商家开店申请
 */
SktShopApplys.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktShopApplys/delete", function (data) {
            Feng.success("删除成功!");
            SktShopApplys.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktShopApplysId",this.seItem.id);
        ajax.start();
    }
};


/**
 * 查询商家开店申请列表
 */
SktShopApplys.search = function () {
    var queryData = {};
    queryData['userName'] = $("#userName").val();
    queryData['telephone'] = $("#telephone").val();
    queryData['applyStatus'] = $("#applyStatus").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    SktShopApplys.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktShopApplys.initColumn();
    var table = new BSTable(SktShopApplys.id, "/sktShopApplys/list", defaultColunms);
    table.setPaginationType("server");
    SktShopApplys.table = table.init();
 
});
