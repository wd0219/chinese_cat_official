/**
 * 初始化商城商家开店申请详情对话框
 */
var SktShopApplysInfoDlg = {
    sktShopApplysInfoData : {}
};

/**
 * 清除数据
 */
SktShopApplysInfoDlg.clearData = function() {
    this.sktShopApplysInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShopApplysInfoDlg.set = function(key, val) {
    this.sktShopApplysInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShopApplysInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktShopApplysInfoDlg.close = function() {
    parent.layer.close(window.parent.SktShopApplys.layerIndex);
}

/**
 * 收集数据
 */
SktShopApplysInfoDlg.collectData = function() {
    this
    .set('applyId')
    .set('userName')
    .set('legalPerson')
    .set('companyName')
    .set('telephone')
    .set('shopName')
    .set('shopStatus')
    .set('createTime')	
    .set('checkStaffId')
    .set('checkTime')
    .set('applyDesc')
    .set('statusDesc');
}

/**
 * 提交添加
 */
/*SktShopApplysInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
   
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktShopApplys/update", function(data){
        Feng.success("添加成功!");
        window.parent.SktShopApplys.table.refresh();
        SktShopApplysInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShopApplysInfoData);
    ajax.start();
}
*/
/**
 * 提交修改
 */
SktShopApplysInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    var radios = document.getElementsByName("applyStatus");
   // alert(radios);
    for ( var i = 0; i < radios.length; i++) {
	    if (radios[i].checked==true){
	    	this.sktShopApplysInfoData.applyStatus=$(radios[i]).val();
	    }
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktShopApplys/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktShopApplys.table.refresh();
        SktShopApplysInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    this.sktShopApplysInfoData.applyDesc=$('#applyDesc').val();
    ajax.set(this.sktShopApplysInfoData);
    ajax.start();
}

$(function() {
	   
	var applyStatus = $('#applyStatus').val();
	switch(applyStatus){
	case '1':
		$('#applyStatus2').val("未审核");
		$('#applyStatus2').attr('disabled',true);
		break;
	case '2':
		$('#applyStatus2').val("拒绝");
		$('#applyStatus2').attr('disabled',true);
		break;
	case '3':
		$('#applyStatus2').val("已审核");
		$('#applyStatus2').attr('disabled',true);
		break;
		default:
		break;
	}
});
