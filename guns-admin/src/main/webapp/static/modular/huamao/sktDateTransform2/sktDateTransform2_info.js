/**
 * 初始化转换记录详情对话框
 */
var SktDateTransform2InfoDlg = {
    sktDateTransform2InfoData : {}
};

/**
 * 清除数据
 */
SktDateTransform2InfoDlg.clearData = function() {
    this.sktDateTransform2InfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktDateTransform2InfoDlg.set = function(key, val) {
    this.sktDateTransform2InfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktDateTransform2InfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktDateTransform2InfoDlg.close = function() {
    parent.layer.close(window.parent.SktDateTransform2.layerIndex);
}

/**
 * 收集数据
 */
SktDateTransform2InfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('date')
    .set('ratio')
    .set('transformNum')
    .set('kaiyuan')
    .set('userNum')
    .set('staffId')
    .set('createTime')
    .set('handleStart')
    .set('handleEnd');
}

/**
 * 提交添加
 */
SktDateTransform2InfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktDateTransform2/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktDateTransform2.table.refresh();
        SktDateTransform2InfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktDateTransform2InfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktDateTransform2InfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktDateTransform2/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktDateTransform2.table.refresh();
        SktDateTransform2InfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktDateTransform2InfoData);
    ajax.start();
}

$(function() {

});
