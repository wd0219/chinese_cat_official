/**
 * 转换记录管理初始化
 */
var SktDateTransform2 = {
    id: "SktDateTransform2Table",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktDateTransform2.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '转换对象', field: 'type', visible: true, align: 'center', valign: 'middle',
            	 formatter:function(value,row,index){
             		var result;
             		switch(value){
             		case 1:
             			result = "消费者";
             			break;
             		case 2:
             			result = "代理公司 ";
             			break;
             		}
             		return result;
             	}},
            {title: '日期', field: 'date', visible: true, align: 'center', valign: 'middle'},
            {title: '比例（万分单位）', field: 'ratio', visible: true, align: 'center', valign: 'middle'},
            {title: '转换的积分总数', field: 'transformNum', visible: true, align: 'center', valign: 'middle'},
            {title: '转换的华宝总数', field: 'kaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '转换的总人数', field: 'userNum', visible: true, align: 'center', valign: 'middle'},
            {title: '操作员工', field: 'staffId', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 0:
        			result = "系统";
        			break;
        		}
        		return result;
        	}},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktDateTransform2.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktDateTransform2.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加转换记录
 */
SktDateTransform2.openAddSktDateTransform2 = function () {
    var index = layer.open({
        type: 2,
        title: '添加转换记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktDateTransform2/sktDateTransform2_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看转换记录详情
 */
SktDateTransform2.openSktDateTransform2Detail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '转换记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktDateTransform2/sktDateTransform2_update/' + SktDateTransform2.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除转换记录
 */
SktDateTransform2.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktDateTransform2/delete", function (data) {
            Feng.success("删除成功!");
            SktDateTransform2.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktDateTransform2Id",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询转换记录列表
 */
SktDateTransform2.search = function () {
    var queryData = {};
    queryData['type'] = $("#type").val();
    queryData['createTime'] = $("#createTime").val().replace(/-/g,"");
    queryData['date'] = $("#date").val();
    SktDateTransform2.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktDateTransform2.initColumn();
    var table = new BSTable(SktDateTransform2.id, "/sktDateTransform2/list", defaultColunms);
    table.setPaginationType("server");
    SktDateTransform2.table = table.init();
});
