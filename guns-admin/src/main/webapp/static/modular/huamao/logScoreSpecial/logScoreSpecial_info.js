/**
 * 初始化待发特别积分记录详情对话框
 */
var LogScoreSpecialInfoDlg = {
    logScoreSpecialInfoData : {}
};

/**
 * 清除数据
 */
LogScoreSpecialInfoDlg.clearData = function() {
    this.logScoreSpecialInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogScoreSpecialInfoDlg.set = function(key, val) {
    this.logScoreSpecialInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogScoreSpecialInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogScoreSpecialInfoDlg.close = function() {
    parent.layer.close(window.parent.LogScoreSpecial.layerIndex);
}

/**
 * 收集数据
 */
LogScoreSpecialInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preScore')
    .set('scoreType')
    .set('score')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogScoreSpecialInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logScoreSpecial/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogScoreSpecial.table.refresh();
        LogScoreSpecialInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logScoreSpecialInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogScoreSpecialInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logScoreSpecial/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogScoreSpecial.table.refresh();
        LogScoreSpecialInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logScoreSpecialInfoData);
    ajax.start();
}

$(function() {

});
