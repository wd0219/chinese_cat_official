/**
 * 待发特别积分记录管理初始化
 */
var LogScoreSpecial = {
    id: "LogScoreSpecialTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogScoreSpecial.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动数量', field: 'scorep', visible: true, align: 'center', valign: 'middle'},
        	{title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogScoreSpecial.type},
            //{title: '发起用户ID 0为平台', field: 'fromId', visible: true, align: 'center', valign: 'middle'},
            //{title: '目标用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            //{title: '操作前的金额', field: 'preScore', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'scoreType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 类型对应：1特别奖励>31特别奖转
 */
LogScoreSpecial.type = function(value,row,index){
	if(value == 1){
		return "特别奖励";
	}else if(value == 31){
		return "特别奖转";
	}
}
/**
 * 检查是否选中
 */
LogScoreSpecial.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogScoreSpecial.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加待发特别积分记录
 */
LogScoreSpecial.openAddLogScoreSpecial = function () {
    var index = layer.open({
        type: 2,
        title: '添加待发特别积分记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logScoreSpecial/logScoreSpecial_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看待发特别积分记录详情
 */
LogScoreSpecial.openLogScoreSpecialDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '待发特别积分记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logScoreSpecial/logScoreSpecial_update/' + LogScoreSpecial.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除待发特别积分记录
 */
LogScoreSpecial.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logScoreSpecial/delete", function (data) {
            Feng.success("删除成功!");
            LogScoreSpecial.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logScoreSpecialId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询待发特别积分记录列表
 */
LogScoreSpecial.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogScoreSpecial.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogScoreSpecial.initColumn();
    var table = new BSTable(LogScoreSpecial.id, "/logScoreSpecial/list/"+$('#userPhone2').val(), defaultColunms);
    table.setPaginationType("server");
    LogScoreSpecial.table = table.init();
});
//导出Excel
LogScoreSpecial.export= function(){
	$('#'+LogScoreSpecial.id).tableExport({
		fileName:'待发特别积分记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}