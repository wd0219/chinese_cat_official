/**
 * 初始化商家每月营业额详情对话框
 */
var SktSalesMonthInfoDlg = {
    sktSalesMonthInfoData : {}
};

/**
 * 清除数据
 */
SktSalesMonthInfoDlg.clearData = function() {
    this.sktSalesMonthInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktSalesMonthInfoDlg.set = function(key, val) {
    this.sktSalesMonthInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktSalesMonthInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktSalesMonthInfoDlg.close = function() {
    parent.layer.close(window.parent.SktSalesMonth.layerIndex);
}

/**
 * 收集数据
 */
SktSalesMonthInfoDlg.collectData = function() {
    this
    .set('id')
    .set('shopId')
    .set('userId')
    .set('month')
    .set('storeTotal')
    .set('storeCash')
    .set('storeKaiyuan')
    .set('mallTotal')
    .set('mallCash')
    .set('mallKaiyuan')
    .set('profitTotal')
    .set('createTime')
    .set('isProfit');
}

/**
 * 提交添加
 */
SktSalesMonthInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSalesMonth/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktSalesMonth.table.refresh();
        SktSalesMonthInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktSalesMonthInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktSalesMonthInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSalesMonth/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktSalesMonth.table.refresh();
        SktSalesMonthInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktSalesMonthInfoData);
    ajax.start();
}

$(function() {

});
