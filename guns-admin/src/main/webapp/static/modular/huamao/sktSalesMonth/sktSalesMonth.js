/**
 * 商家每月营业额管理初始化
 */
var SktSalesMonth = {
    id: "SktSalesMonthTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktSalesMonth.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户名', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '商家名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '营业月份', field: 'month', visible: true, align: 'center', valign: 'middle'},
            {title: '门店营业总额', field: 'storeTotal', visible: true, align: 'center', valign: 'middle'},
            {title: '门店营业额现金', field: 'storeCash', visible: true, align: 'center', valign: 'middle'},
            {title: '门店营业额华宝', field: 'storeKaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '商城营业总额', field: 'mallTotal', visible: true, align: 'center', valign: 'middle'},
            {title: '商城营业额现金', field: 'mallCash', visible: true, align: 'center', valign: 'middle'},
            {title: '商城营业额华宝', field: 'mallKaiyuan', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktSalesMonth.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktSalesMonth.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商家每月营业额
 */
SktSalesMonth.openAddSktSalesMonth = function () {
    var index = layer.open({
        type: 2,
        title: '添加商家每月营业额',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktSalesMonth/sktSalesMonth_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商家每月营业额详情
 */
SktSalesMonth.openSktSalesMonthDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商家每月营业额详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktSalesMonth/sktSalesMonth_update/' + SktSalesMonth.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商家每月营业额
 */
SktSalesMonth.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktSalesMonth/delete", function (data) {
            Feng.success("删除成功!");
            SktSalesMonth.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktSalesMonthId",this.seItem.id);
        ajax.start();
    }
};
//表格导出
SktSalesMonth.export= function(){
	$('#'+SktSalesMonth.id).tableExport({
		fileName:'线下商家购买牌匾',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}
/**
 * 查询商家每月营业额列表
 */
SktSalesMonth.search = function () {
    var queryData = {};
    queryData['userName'] = $("#userName").val();
    queryData['shopName'] = $("#shopName").val();
    queryData['month'] = $("#month").val();
    SktSalesMonth.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktSalesMonth.initColumn();
    var table = new BSTable(SktSalesMonth.id, "/sktSalesMonth/list", defaultColunms);
    table.setPaginationType("server");
    SktSalesMonth.table = table.init();
});
