/**
 * 友情链接管理初始化
 */
var SktFriendlinks = {
    id: "SktFriendlinksTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktFriendlinks.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '名称', field: 'friendlinkName', visible: true, align: 'center', valign: 'middle'},
            {title: '网址', field: 'friendlinkUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',formatter:SktFriendlinks.op}
    ];
};
/*
 * 操作
 * */
SktFriendlinks.op = function () {
	return "<a href='javascript:void(0)' onclick=\"SktFriendlinks.openSktFriendlinksDetail()\">详情</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktFriendlinks.delete()\">删除</a>"
}

/**
 * 检查是否选中
 */
SktFriendlinks.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktFriendlinks.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加友情链接
 */
SktFriendlinks.openAddSktFriendlinks = function () {
    var index = layer.open({
        type: 2,
        title: '添加友情链接',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktFriendlinks/sktFriendlinks_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看友情链接详情
 */
SktFriendlinks.openSktFriendlinksDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '友情链接详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktFriendlinks/sktFriendlinks_update/' + SktFriendlinks.seItem.friendlinkId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除友情链接
 */
SktFriendlinks.delete = function () {
    if (this.check()) {
    var id = this.seItem.friendlinkId;
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/sktFriendlinks/delete", function (data) {
            Feng.success("删除成功!");
            SktFriendlinks.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktFriendlinksId",id);
        ajax.start();
    };
    	Feng.confirm("是否删除该条记录吗?",operation);
    }
};

/**
 * 查询友情链接列表
 */
SktFriendlinks.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktFriendlinks.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktFriendlinks.initColumn();
    var table = new BSTable(SktFriendlinks.id, "/sktFriendlinks/list", defaultColunms);
    table.setPaginationType("server");
    SktFriendlinks.table = table.init();
});
