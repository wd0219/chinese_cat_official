/**
 * 初始化友情链接详情对话框
 */
var SktFriendlinksInfoDlg = {
    sktFriendlinksInfoData : {}
};

/**
 * 清除数据
 */
SktFriendlinksInfoDlg.clearData = function() {
    this.sktFriendlinksInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktFriendlinksInfoDlg.set = function(key, val) {
    this.sktFriendlinksInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktFriendlinksInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktFriendlinksInfoDlg.close = function() {
    parent.layer.close(window.parent.SktFriendlinks.layerIndex);
}

/**
 * 收集数据
 */
SktFriendlinksInfoDlg.collectData = function() {
    this
    .set('friendlinkId')
    .set('friendlinkIco')
    .set('friendlinkName')
    .set('friendlinkUrl')
    .set('friendlinkSort')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
SktFriendlinksInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;
    
    var friendlinkName = $("#friendlinkName").val();
    var friendlinkUrl = $("#friendlinkUrl").val();

    if(friendlinkName == null || friendlinkName ==""){
        var html='<span class="error">名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#friendlinkName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(friendlinkUrl == null || friendlinkUrl ==""){
        var html='<span class="error">网址不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#friendlinkUrl').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/sktFriendlinks/add", function(data){
	        Feng.success("添加成功!");
	        window.parent.SktFriendlinks.table.refresh();
	        SktFriendlinksInfoDlg.close();
	    },function(data){
	        Feng.error("添加失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.sktFriendlinksInfoData);
	    ajax.start();
		}else{
		    Feng.success("请核实添加字段!");
		}
	}

	function isRealNum(val){
	    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	    if(val === "" || val ==null){
	        return false;
	    }
	    if(!isNaN(val)){
	        return true;
	    }else{
	        return false;
	    }
	}
	
/*	function checkIP()  
	{  
	  obj = document.getElementById("friendlinkUrl").value;  
	//ip地址  
	var exp=/^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$/;  
	var reg = obj.match(exp);  
	if(reg==null)  
	{  
	alert("IP地址不合法！");  
	}  
	else  
	{  
	alert("IP地址合法！");  
	}  
	}  */

/**
 * 提交修改
 */
SktFriendlinksInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var friendlinkName = $("#friendlinkName").val();
    var friendlinkUrl = $("#friendlinkUrl").val();

    if(friendlinkName == null || friendlinkName ==""){
        var html='<span class="error">名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#friendlinkName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(friendlinkUrl == null || friendlinkUrl ==""){
        var html='<span class="error">网址不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#friendlinkUrl').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/sktFriendlinks/update", function(data){
	        Feng.success("修改成功!");
	        window.parent.SktFriendlinks.table.refresh();
	        SktFriendlinksInfoDlg.close();
	    },function(data){
	        Feng.error("修改失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.sktFriendlinksInfoData);
	    ajax.start();
		}else{
		    Feng.success("请核实添加字段!");
		}
	}

$(function() {

});
