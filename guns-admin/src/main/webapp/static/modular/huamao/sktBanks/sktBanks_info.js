/**
 * 初始化银行管理详情对话框
 */
var SktBanksInfoDlg = {
    sktBanksInfoData : {}
};

/**
 * 清除数据
 */
SktBanksInfoDlg.clearData = function() {
    this.sktBanksInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktBanksInfoDlg.set = function(key, val) {
    this.sktBanksInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktBanksInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktBanksInfoDlg.close = function() {
    parent.layer.close(window.parent.SktBanks.layerIndex);
}

/**
 * 收集数据
 */
SktBanksInfoDlg.collectData = function() {
    this
    .set('bankId')
    .set('code')
    .set('bankName')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
SktBanksInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var bankName = $("#bankName").val();

    if(bankName == null || bankName ==""){
        var html='<span class="error">银行简称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#bankName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/sktBanks/add", function(data){
	        Feng.success("添加成功!");
	        window.parent.SktBanks.table.refresh();
	        SktBanksInfoDlg.close();
	    },function(data){
	        Feng.error("添加失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.sktBanksInfoData);
	    ajax.start();
	    }else{
	    	Feng.success("请核实添加字段!");
	    }
}

	function isRealNum(val){
	    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	    if(val === "" || val ==null){
	        return false;
	    }
	    if(!isNaN(val)){
	        return true;
	    }else{
	        return false;
	    }
	}

/**
 * 提交修改
 */
SktBanksInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var bankName = $("#bankName").val();

    if(bankName == null || bankName ==""){
        var html='<span class="error">银行简称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#bankName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktBanks/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktBanks.table.refresh();
        SktBanksInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktBanksInfoData);
    ajax.start();
	}else{
		Feng.success("请核实添加字段!");
	}
}

$(function() {

});
