/**
 * 银行管理管理初始化
 */
var SktBanks = {
    id: "SktBanksTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktBanks.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '银行简称', field: 'bankName', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',formatter:SktBanks.op}
    ];
};
/*
 * 操作
 * */
SktBanks.op = function () {
	return "<a href='javascript:void(0)' onclick=\"SktBanks.openSktBanksDetail()\">详情</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktBanks.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
SktBanks.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktBanks.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加银行管理
 */
SktBanks.openAddSktBanks = function () {
    var index = layer.open({
        type: 2,
        title: '添加银行管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktBanks/sktBanks_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看银行管理详情
 */
SktBanks.openSktBanksDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '银行管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktBanks/sktBanks_update/' + SktBanks.seItem.bankId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除银行管理
 */
SktBanks.delete = function () {
    if (this.check()) {
    	var id = this.seItem.bankId;
    	var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/sktBanks/delete", function (data) {
            Feng.success("删除成功!");
            SktBanks.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktBanksId",id);
        ajax.start();
    };
    Feng.confirm("是否删除该角色 ?",operation);
    };
};

/**
 * 查询银行管理列表
 */
SktBanks.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktBanks.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktBanks.initColumn();
    var table = new BSTable(SktBanks.id, "/sktBanks/list", defaultColunms);
    table.setPaginationType("server");
    SktBanks.table = table.init();
});
