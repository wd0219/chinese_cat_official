/**
 * 用户升级表管理初始化
 */
var UsersUpgrade = {
    id: "UsersUpgradeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

var count = 0;

/**
 * 初始化表格的列
 */
UsersUpgrade.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '帐号/昵称', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '用户Id', field: 'userId', visible: false, align: 'center', valign: 'middle'},
            {title: '升级前角色', field: 'preRole', visible: true, align: 'center', valign: 'middle',formatter:UsersUpgrade.preRole},
            {title: '升级后角色', field: 'afterRole', visible: true, align: 'center', valign: 'middle',formatter:UsersUpgrade.preRole},
            {title: '支付方式', field: 'payType', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	// 1:线下现金支付 2 现金账户支付 3华宝支付 4第三方支付 5混合支付 6华宝营业额
            	switch(value){
            	case 1:
            		return "线下现金支付 ";
            		break;
            	case 2:
            		return "现金账户支付 ";
            		break;
            	case 3:
            		return "华宝支付";
            		break;
            	case 4:
            		return "第三方支付";
            		break;
            	case 5:
            		return "混合支付 ";
            		break;
            	case 6:
            		return "华宝营业额";
            		break;
            	}
            }},
            {title: '订单金额 ', field: 'totalMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '支付金额 ', field: 'realMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '下单时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '升级时间', field: 'upgradeTime', visible: true, align: 'center', valign: 'middle'},
            {title: '状态 ', field: 'status', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	//0未支付 1已付款未处理 2已取消 3已处理 9处理中
            	if(value=='0'){
            		return "未支付";
            	}else if(value=='1'){
            		return "已付款未处理";
            	}else if(value=='2'){
            		return "已取消";
            	}else if(value=='3'){
            		return "已处理";
            	}else if(value=='9'){
            		return "处理中";
            	}
            }},
            {title: '操作', field: 'status', visible: true, align: 'center', valign: 'middle',formatter:UsersUpgrade.op}
    ];
};

UsersUpgrade.preRole  = function(value,row,index){
	//0普通 1主管 2经理
	if(value=='0'){
		return "普通用户";
	}else if(value=='1'){
		return "主管";
	}else if(value=='2'){
		return "经理";
	}
}
/**
 * 操作
 */
UsersUpgrade.op = function(value,row,index){
	//0未支付 1已付款未处理 2已取消 3已处理 9处理中
	switch(value){
	case 0:
	case 2:
	case 3:
		return "";
		break;
	case 1:
	case 9:
		return "<a href='javascript:void(0)' onclick='UsersUpgrade.change("+row.orderId+")'>拒绝</a> &nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick='UsersUpgrade.shengji("+row.orderId+")'>通过</a>";
		break;
	}
	
}

/**
 * 取消
 */
UsersUpgrade.change = function(orderId){
	UsersUpgrade.updateSubmit(orderId,2);
}

/**
 * 升级
 */
UsersUpgrade.shengji = function(orderId){
	UsersUpgrade.updateSubmit(orderId,3);
}

/**
 * 提交修改
 */
UsersUpgrade.updateSubmit = function(orderId,status) {

	if (this.check()) {
	    //提交信息
		var operation = function(){
            if(count == 0) {
                count++;
                var ajax = new $ax(Feng.ctxPath + "/usersUpgrade/updateStatus/" + orderId + "/" + status, function (data) {
                    if (data == 'FINAL_ROLE') {
                        Feng.success("已经是经理！");
                        return;
                    }
                    Feng.success("修改成功!");
                    UsersUpgrade.table.refresh();
                    location.reload();
                }, function (data) {
                    Feng.error("修改失败!" + data.responseJSON.message + "!");
                });
                ajax.set("preRole", UsersUpgrade.seItem.preRole);
                ajax.set("afterRole", UsersUpgrade.seItem.afterRole);

                ajax.start();
            }
	};
	Feng.confirm("是否修改该条记录?",operation);
	}
};
/**
 * 检查是否选中
 */
UsersUpgrade.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        UsersUpgrade.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户升级表
 */
UsersUpgrade.openAddUsersUpgrade = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户升级表',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/usersUpgrade/usersUpgrade_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户升级表详情
 */
UsersUpgrade.openUsersUpgradeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户升级表详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/usersUpgrade/usersUpgrade_update/' + UsersUpgrade.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户升级表
 */
UsersUpgrade.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/usersUpgrade/delete", function (data) {
            Feng.success("删除成功!");
            UsersUpgrade.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("usersUpgradeId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户升级表列表
 */
UsersUpgrade.search = function () {
    var queryData = {};
    queryData['loginName'] = $("#loginName").val();
    queryData['trueName'] = $("#trueName").val();
    queryData['userPhone'] = $("#userPhone").val();
    UsersUpgrade.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = UsersUpgrade.initColumn();
    var table = new BSTable(UsersUpgrade.id, "/usersUpgrade/list", defaultColunms);
    table.setPaginationType("server");
    UsersUpgrade.table = table.init();
});
