/**
 * 初始化用户升级表详情对话框
 */
var UsersUpgradeInfoDlg = {
    usersUpgradeInfoData : {}
};

/**
 * 清除数据
 */
UsersUpgradeInfoDlg.clearData = function() {
    this.usersUpgradeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersUpgradeInfoDlg.set = function(key, val) {
    this.usersUpgradeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersUpgradeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UsersUpgradeInfoDlg.close = function() {
    parent.layer.close(window.parent.UsersUpgrade.layerIndex);
}

/**
 * 收集数据
 */
UsersUpgradeInfoDlg.collectData = function() {
    this
    .set('orderId')
    .set('orderNo')
    .set('userId')
    .set('preRole')
    .set('afterRole')
    .set('payType')
    .set('totalMoney')
    .set('realMoney')
    .set('cash')
    .set('kaiyuan')
    .set('kaiyuanFee')
    .set('orderRemarks')
    .set('createTime')
    .set('upgradeTime')
    .set('dataFlag')
    .set('status')
    .set('batTime');
}

/**
 * 提交添加
 */
UsersUpgradeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersUpgrade/add", function(data){
        Feng.success("添加成功!");
        window.parent.UsersUpgrade.table.refresh();
        UsersUpgradeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersUpgradeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UsersUpgradeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersUpgrade/update", function(data){
        Feng.success("修改成功!");
        window.parent.UsersUpgrade.table.refresh();
        UsersUpgradeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersUpgradeInfoData);
    ajax.start();
}

$(function() {

});
