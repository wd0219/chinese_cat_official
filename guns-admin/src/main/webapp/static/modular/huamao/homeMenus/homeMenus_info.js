/**
 * 初始化前台菜单详情对话框
 */
var HomeMenusInfoDlg = {
    homeMenusInfoData : {}
};

/**
 * 清除数据
 */
HomeMenusInfoDlg.clearData = function() {
    this.homeMenusInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
HomeMenusInfoDlg.set = function(key, val) {
    this.homeMenusInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
HomeMenusInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
HomeMenusInfoDlg.close = function() {
    parent.layer.close(window.parent.HomeMenus.layerIndex);
}

/**
 * 收集数据
 */
HomeMenusInfoDlg.collectData = function() {
    this
    .set('menuId')
    .set('parentId')
    .set('iconClass')
    .set('menuName')
    .set('menuUrl')
    .set('menuOtherUrl')
    .set('menuType')
    .set('isShow')
    .set('menuSort')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
HomeMenusInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var menuName = $("#menuName").val();
    var menuType = $("#menuType").val();
    var menuSort = $("#menuSort").val();
    var menuUrl = $("#menuUrl").val();
    var iconClass = $("#iconClass").val();

    if(menuName == null || menuName ==""){
        var html='<span class="error">菜单名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#menuName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(menuType == null || menuType ==""){
        var html='<span class="error">菜单类型不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#menuType').after(html);
        falg = 1;
    }

    if(!isRealNum(menuSort)){
        var html='<span class="error">菜单排序必须为数字!</span>';
        //$('#menuSort').parent().append(html);
        $('#menuSort').after(html);
        falg = 1;
    }

    if(menuUrl == null || menuUrl ==""){
        var html='<span class="error">菜单Url不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#menuUrl').after(html);
        falg = 1;
    }

    if(iconClass == null || iconClass ==""){
        var html='<span class="error">ico类的名称不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#iconClass').after(html);
        falg = 1;
    }



    this.clearData();
    this.collectData();
    this.homeMenusInfoData['isShow']= $('input[name=isShow]:checked').val();
    if(falg == 0){
        //提交信息
        var ajax = new $ax(Feng.ctxPath + "/homeMenus/add", function(data){
            Feng.success("添加成功!");
            window.parent.HomeMenus.table.refresh();
            HomeMenusInfoDlg.close();
        },function(data){
            Feng.error("添加失败!" + data.responseJSON.message + "!");
        });
        ajax.set(this.homeMenusInfoData);
        ajax.start();
    }else{
        Feng.success("请核实添加字段!");
    }
}

function isRealNum(val){
    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
    if(val === "" || val ==null){
        return false;
    }
    if(!isNaN(val)){
        return true;
    }else{
        return false;
    }
}

/**
 * 提交修改
 */
HomeMenusInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var menuName = $("#menuName").val();
    var menuType = $("#menuType").val();
    var menuSort = $("#menuSort").val();
    var menuUrl = $("#menuUrl").val();
/*    var iconClass = $("#iconClass").val();*/

    if(menuName == null || menuName ==""){
        var html='<span class="error">菜单名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#menuName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(menuType == null || menuType ==""){
        var html='<span class="error">菜单类型不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#menuType').after(html);
        falg = 1;
    }

    if(!isRealNum(menuSort)){
        var html='<span class="error">菜单排序必须为数字!</span>';
        //$('#menuSort').parent().append(html);
        $('#menuSort').after(html);
        falg = 1;
    }

    if(menuUrl == null || menuUrl ==""){
        var html='<span class="error">菜单Url不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#menuUrl').after(html);
        falg = 1;
    }

/*    if(iconClass == null || iconClass ==""){
        var html='<span class="error">ico类的名称不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#iconClass').after(html);
        falg = 1;
    }*/

    this.clearData();
    this.collectData();
    
    let
    isShow = $('input:radio[name=isShow]:checked').val();
    let
	goods1 = $('#goods1').val();
    
    this.homeMenusInfoData.isShow = isShow;
    this.homeMenusInfoData.menuType = goods1;

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/homeMenus/update", function(data){
	        Feng.success("修改成功!");
	        window.parent.HomeMenus.table.refresh();
	        HomeMenusInfoDlg.close();
	    },function(data){
	        Feng.error("修改失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.homeMenusInfoData);
	    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
}

$(function() {
	$('#goods1').val($('#menuType').val());
});
