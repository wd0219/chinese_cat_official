/**
 * 前台菜单管理初始化
 */
var HomeMenus = {
    id: "HomeMenusTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
HomeMenus.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '父ID', field: 'parentId', visible: false, align: 'center', valign: 'middle'},
            {title: 'ico类的名称', field: 'iconClass', visible: false, align: 'center', valign: 'middle'},
            {title: '菜单名称', field: 'menuName', visible: true, align: 'center', valign: 'middle'},
            {title: '菜单Url', field: 'menuUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '关联Url', field: 'menuOtherUrl', visible: false, align: 'center', valign: 'middle'},
            {title: '菜单类型', field: 'menuType', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
               		var result;
               		switch(value){
               		
               		case 1:
               			result = "商家菜单";
               			break;
               		case 0:
               			result = "用户菜单 ";
               			break;
               		}

               		return result;
            }
            
            },
            {title: '是否显示', field: 'isShow', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
               		var result;
               		switch(value){
               		
               		case 1:
               			result = "显示";
               			break;
               		case 0:
               			result = "不显示 ";
               			break;
               		}

               		return result;
            }},
            {title: '菜单排序', field: 'menuSort', visible: true, align: 'center', valign: 'middle'},
            {title: '有效状态', field: 'dataFlag', visible: false, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: false, align: 'center', valign: 'middle'},
            {title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:HomeMenus.open}
            ];
};
HomeMenus.open = function () {
	return "<a href='javascript:void(0)' onclick=\"HomeMenus.openHomeMenusDetail2()\">添加下一级</a>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)'onclick=\"HomeMenus.openHomeMenusDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"HomeMenus.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
HomeMenus.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        HomeMenus.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加前台菜单
 */
HomeMenus.openAddHomeMenus = function () {
    var index = layer.open({
        type: 2,
        title: '添加前台菜单',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/homeMenus/homeMenus_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看前台菜单详情
 */
HomeMenus.openHomeMenusDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '前台菜单详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/homeMenus/homeMenus_update/' + HomeMenus.seItem.menuId
        });
        this.layerIndex = index;
    }
};
/**
 * 打开添加前台子菜单
 */
HomeMenus.openHomeMenusDetail2 = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '添加下一级',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/homeMenus/homeMenusChild_add/' + HomeMenus.seItem.menuId
        });
        this.layerIndex = index;
    }
};
/**
 * 删除前台菜单
 */
HomeMenus.delete = function () {
    if (this.check()) {
    	var id = this.seItem.menuId;
        var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/homeMenus/delete", function (data) {
            Feng.success("删除成功!");
            HomeMenus.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("homeMenusId",id);
        ajax.start();
    };
    	Feng.confirm("是否删除该数据?",operation);
    }
};

/**
 * 查询前台菜单列表
 */
HomeMenus.search = function () {
    var queryData = {};
    queryData['menuType'] = $("#menuType").val();
    HomeMenus.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = HomeMenus.initColumn();
    var table = new BSTable(HomeMenus.id, "/homeMenus/list", defaultColunms);
    table.setPaginationType("server");
    HomeMenus.table = table.init();
});
