/**
 * 商城消息管理初始化
 */
var MessagesList = {
    id: "MessagesListTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};
/**
 * 初始化表格的列
 */
MessagesList.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '自增ID', field: 'id', visible: false, align: 'center', valign: 'middle'},
            {title: '消息类型', field: 'msgType', visible: true, align: 'center', valign: 'middle',formatter:MessagesList.msgType},
            {title: '发送者', field: 'sendName', visible: true, align: 'center', valign: 'middle'},
            {title: '接受者', field: 'receiveName', visible: true, align: 'center', valign: 'middle',formatter:MessagesList.name},
            {title: '消息标题', field: 'msgTitle', visible: false, align: 'center', valign: 'middle'},
            {title: '消息内容', field: 'msgContent', visible: true, align: 'center', valign: 'middle'},
            {title: '阅读状态', field: 'msgStatus', visible: true, align: 'center', valign: 'middle',formatter:MessagesList.msgStatus},
            {title: '存放json数据', field: 'msgJson', visible: false, align: 'center', valign: 'middle'},
            {title: '有效状态', field: 'dataFlag', visible: true, align: 'center', valign: 'middle',formatter:MessagesList.dataFlag},
            {title: '发送时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: '', visible: true, align: 'center', valign: 'middle',formatter:MessagesList.op},
    ];
};
/**
 * 有效状态对应：1有效，-1无效
 */
MessagesList.dataFlag = function(value,row,index){
	if(value == 1){
		return "有效";
	}else if(value == -1){
		return "无效";
	}
}
/**
 * 阅读状态对应：0未读，1已读
 */
MessagesList.msgStatus = function(value,row,index){
	if(value == 0){
		return "未读";
	}else if(value == 1){
		return "已读";
	}
}
/**
 * 消息类型对应：0手工，1系统
 */
MessagesList.msgType = function(value,row,index){
	if(value == 0){
		return "手工发送";
	}else if(value == 1){
		return "系统发送";
	}
}
/**
 * 判断接受者名字是否存在，如果不存在就是给所有商家发送
 */
MessagesList.name = function(value,row,index){
	if(value != null && value != ""){
	return value;
}else{
	return "所有商家";
}
}
/**
 * 操作
 */
MessagesList.op = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"MessagesList.openMessagesDetail()\">查看</a>&nbsp;&nbsp;<a href='javascript:void(0)'onclick=\"MessagesList.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
MessagesList.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        MessagesList.seItem = selected[0];
        return true;
    }
};
/**
 * 查看消息内容详情
 */
MessagesList.openMessagesDetail = function(){
	 if (this.check()) {
	        var index = layer.open({
	            type: 2,
	            title: '内容详情',
	            area: ['100%', '100%'], //宽高
	            fix: false, //不固定
	            maxmin: true,
	            content: Feng.ctxPath + '/messages/messageDetail?id=' + MessagesList.seItem.id//将消息id传过去
	        });
	        this.layerIndex = index;
	    }
};
/**
 * 点击添加商城消息
 */
MessagesList.openAddMessagesList = function () {
    var index = layer.open({
        type: 2,
        title: '添加商城消息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/messages/Messages_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商城消息详情
 */
MessagesList.openMessagesListDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商城消息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/messages/Messages_update/' + MessagesList.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 获取页面的值并写入数据库
 */
MessagesList.submint = function(){
	alert($('input:radio[name=types]:checked').val());
	alert(CKEDITOR.instances.msgContent.getData());
}

/**
 * 通过ID删除发送的消息
 */
MessagesList.delete = function () {
	if (this.check()) {
	      var operation = function (){
	    	  var id = MessagesList.seItem.id;
	    	var ajax = new $ax(Feng.ctxPath + "/messages/deleteById", function (data) {
	            Feng.success("删除成功!");
	            MessagesList.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("id",id);
	        ajax.start();
	       };
	        Feng.confirm("是否删除该条记录?",operation);
	    }
};

/**
 * 查询消息列表
 */
MessagesList.search = function () {
    var queryData = {};
    queryData['msgType'] = $("#msgType").val();
    queryData['msgContent'] = $("#msgContent").val();
    MessagesList.table.refresh({query: queryData});
};
/**
 * 跳转到发送消息页面
 */
MessagesList.sendMessage = function(){
	location.href = Feng.ctxPath + '/messages/sendMessage';
}
$(function () {
    var defaultColunms = MessagesList.initColumn();
    var table = new BSTable(MessagesList.id, "/messages/list", defaultColunms);
    table.setPaginationType("server");
    MessagesList.table = table.init();
});


var HtmlUtil = {
    /*1.用浏览器内部转换器实现html转码*/
    htmlEncode:function (html){
        //1.首先动态创建一个容器标签元素，如DIV
        var temp = document.createElement ("div");
        //2.然后将要转换的字符串设置为这个元素的innerText(ie支持)或者textContent(火狐，google支持)
        (temp.textContent != undefined ) ? (temp.textContent = html) : (temp.innerText = html);
        //3.最后返回这个元素的innerHTML，即得到经过HTML编码转换的字符串了
        var output = temp.innerHTML;
        temp = null;
        return output;
    },
    /*2.用浏览器内部转换器实现html解码*/
    htmlDecode:function (text){
        //1.首先动态创建一个容器标签元素，如DIV
        var temp = document.createElement("div");
        //2.然后将要转换的字符串设置为这个元素的innerHTML(ie，火狐，google都支持)
        temp.innerHTML = text;
        //3.最后返回这个元素的innerText(ie支持)或者textContent(火狐，google支持)，即得到经过HTML解码的字符串了。
        var output = temp.innerText || temp.textContent;
        temp = null;
        return output;
    }
};