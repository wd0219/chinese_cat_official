/**
 * 初始化商城消息详情对话框
 */
var MessagesInfoDlg = {
    messagesInfoData : {}
};

/**
 * 清除数据
 */
MessagesInfoDlg.clearData = function() {
    this.messagesInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MessagesInfoDlg.set = function(key, val) {
    this.messagesInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
MessagesInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
MessagesInfoDlg.close = function() {
    parent.layer.close(window.parent.Messages.layerIndex);
}

/**
 * 收集数据
 */
MessagesInfoDlg.collectData = function() {
    this
    .set('id')
    .set('msgType')
    .set('sendUserId')
    .set('receiveUserId')
    .set('msgTitle')
    .set('msgContent')
    .set('msgStatus')
    .set('msgJson')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
MessagesInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/messages/add", function(data){
        Feng.success("添加成功!");
        window.parent.Messages.table.refresh();
        MessagesInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.messagesInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
MessagesInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/messages/update", function(data){
        Feng.success("修改成功!");
        window.parent.Messages.table.refresh();
        MessagesInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.messagesInfoData);
    ajax.start();
}

$(function() {

});
