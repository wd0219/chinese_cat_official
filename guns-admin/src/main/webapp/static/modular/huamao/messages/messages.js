/**
 * 商城消息管理初始化
 */
var Messages = {
    id: "MessagesTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};
/**
 * 初始化表格的列
 */
Messages.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {
            title: 'id', align: 'center', valign: 'middle', width: 50,
            formatter: function (value, row, index) {
                return index + 1;
            }
        },
        {title: '消息类型', field: 'msgType', visible: true, align: 'center', valign: 'middle'},
        {title: '发送者Id', field: 'sendUserId', visible: true, align: 'center', valign: 'middle'},
        {title: '接受者Id', field: 'receiveUserId', visible: true, align: 'center', valign: 'middle'},
        {title: '消息标题', field: 'msgTitle', visible: true, align: 'center', valign: 'middle'},
        {title: '消息内容', field: 'msgContent', visible: true, align: 'center', valign: 'middle'},
        {title: '阅读状态', field: 'msgStatus', visible: true, align: 'center', valign: 'middle'},
        {title: '存放json数据', field: 'msgJson', visible: true, align: 'center', valign: 'middle'},
        {title: '有效状态', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
        {title: '发送时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Messages.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Messages.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商城消息
 */
Messages.openAddMessages = function () {
    var index = layer.open({
        type: 2,
        title: '添加商城消息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/Messages/Messages_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商城消息详情
 */
Messages.openMessagesDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商城消息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/Messages/Messages_update/' + Messages.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 获取页面的值并写入数据库
 */
Messages.submint = function () {
    //获取富文本框中的内容
    var text = editor.txt.html();
    //获取纯文字
    //var text = editor.txt.text();
   // var html = "<br>aaaaaa<p>bbbb</p>";
    //转码
    var encodeHtml = HtmlUtil.htmlEncode(text);
    //alert("encodeHtml：" + encodeHtml);
    // var decodeHtml = HtmlUtil.htmlDecode(encodeHtml);
    // alert("decodeHtml：" + decodeHtml);
    //alert(text);

    var type = $('input:radio[name=types]:checked').val();
    if (type == 1) {
        //给所有商家发送所以就生成一条sql语句，商家登录时会去查询id=0 或者 id=它自己id的广告信息
        $.post('/messages/addMessages',
            {"msgContent": encodeHtml},
            function (req) {
                if (req == true) {
                    Feng.success("添加成功!");
                    location.reload();
                } else {
                    Feng.error("添加失败!");
                }
            });
    } else if (type == 0) {
        if (list2 == "" || list2 == null || list2 == undefined) {
            Feng.error("请先选择要发送的用户");
        }else{
            //发送给指定用户，将用户id存入集合传到后台
            var usersId = [];
            $.each(list2, function (i, val) {
                usersId.push(val.id);
            });
            $.post('/messages/addMessages',
                {"usersId": usersId, "msgContent": encodeHtml},
                function (req) {
                    if (req == true) {
                        Feng.success("添加成功!");
                        location.reload();
                    } else {
                        Feng.error("添加失败!");
                    }
                });
        }
    }
}
/**
 * 跳转到消息列表页面
 */
Messages.list = function () {
    location.href = Feng.ctxPath + '/messages/getList';
}

/**
 * 点击指定用户时显示div
 */
function showDiv() {
    $("#designatedUser").show();
}

/**
 * 点击店铺时隐藏div
 */
function hideDiv() {
    $("#designatedUser").hide();
}

/**
 * 初始化时隐藏div
 */
$(function () {
    $("#designatedUser").hide();
    //Messages.initEdit();
});
/**
 * 初始化富文本编辑框
 */
// Messages.initEdit = function () {
//     var item = {
//         toolbars: [
//             ['source', 'undo', 'redo', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat                     ', 'simpleupload', 'insertimage', 'insertvideo', 'lineheight', 'inserttable', '|', 'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify'],
//             ['formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', 'fontfamily', 'fontsize']
//         ],
//         imagePathFormat: "upload/image/{yyyy}{mm}{dd}/{time}{rand:6}",
//         autoHeightEnabled: false, //是否自动长高，默认true
//         autoFloatEnabled: false, //是否保持toolbar的位置不动，默认true
//         /*			initialFrameWidth : $(document).width() - 454,
//                     initialFrameHeight : $(document).height() - 155,*/
//         wordCount: true, //是否开启字数统计 默认true
//         maximumWords: 10000, //允许的最大字符数 默认值：10000
//         wordOverFlowMsg: "<span style='color:red'>超出范围了！！！！！！！！</span>", //超出字数限制提示
//         elementPathEnabled: false, //是否启用元素路径
//         padding: 0,
//         saveInterval: 5000000, // 将其设置大点，模拟去掉自动保存功能
//         allowDivTransToP: false
//     };
//     //传参生成实例
//     var ue = UE.getEditor('template_content', item);
//     /*		ue.ready(function(){
//                 UE.getEditor('template_content').setContent("SSSSs");
//             });*/
//     //复写UEDITOR的getActionUrl 方法,定义自己的Action
//     /*UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
//     UE.Editor.prototype.getActionUrl = function(action) {
//         if (action == 'uploadimage' || action == 'uploadfile') {
//              var strFullPath=window.document.location.href;
//              var strPath=window.document.location.pathname;
//              var pos=strFullPath.indexOf(strPath);
//              var prePath=strFullPath.substring(0,pos);
//              var postPath=strPath.substring(0,strPath.substr(1).indexOf('/')+1);
//              var webPath=prePath+postPath;
//              */
//     /**
//      * 本机测试路径
//      * var path=webPath+'/sys/upload.api';
//      */
//     /*
//                      var path=webPath+'/sktSysConfigs/sktSysConfigs_update';
//                      */
//     /**
//      * 服务器环境运行路径
//      * var path=prePath+'/sys/upload.api';
//      */
//     /*
//                      return path;
//                     //   return 'http://localhost:8080/cms/sys/upload.api';
//                 } else {
//                     return this._bkGetActionUrl.call(this, action);
//                 }
//             };
//             // 复写UEDITOR的getContentLength方法 解决富文本编辑器中一张图片或者一个文件只能算一个字符的问题,可跟数据库字符的长度配合使用
//             UE.Editor.prototype._bkGetContentLength = UE.Editor.prototype.getContentLength;
//             UE.Editor.prototype.getContentLength = function(){
//                 return this.getContent().length;
//             }*/
// }
/**
 * 指定发送用户的查询按钮
 */
Messages.seach = function () {
    /**
     * 获得手机号
     */
    var userPhone = $("#userPhone").val();
    /**
     * 到后台查询
     */
    $.post('/messages/getUsers',
        {"userPhone": userPhone},
        function (req) {
            /**
             * 清空
             */
            list.splice(0, list.length);
            list2.splice(0, list2.length);
            $.each(req, function (i, val) {
                /**
                 * 将接收到的集合遍历存入对象中
                 */
                var obj = {
                    name: val.receiveName,
                    id: val.receiveUserId
                }
                /**
                 * push到左框中
                 */
                list.push(obj);
            })
        });
}
var HtmlUtil = {
       /*1.用浏览器内部转换器实现html转码*/
    htmlEncode:function (html){
            //1.首先动态创建一个容器标签元素，如DIV
        var temp = document.createElement ("div");
        //2.然后将要转换的字符串设置为这个元素的innerText(ie支持)或者textContent(火狐，google支持)
        (temp.textContent != undefined ) ? (temp.textContent = html) : (temp.innerText = html);
        //3.最后返回这个元素的innerHTML，即得到经过HTML编码转换的字符串了
               var output = temp.innerHTML;
                temp = null;
                return output;
            },
         /*2.用浏览器内部转换器实现html解码*/
             htmlDecode:function (text){
                 //1.首先动态创建一个容器标签元素，如DIV
        var temp = document.createElement("div");
                //2.然后将要转换的字符串设置为这个元素的innerHTML(ie，火狐，google都支持)
                temp.innerHTML = text;
                 //3.最后返回这个元素的innerText(ie支持)或者textContent(火狐，google支持)，即得到经过HTML解码的字符串了。
                 var output = temp.innerText || temp.textContent;
                 temp = null;
                 return output;
            }
     };