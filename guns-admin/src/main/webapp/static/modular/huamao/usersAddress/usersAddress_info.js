/**
 * 初始化用户地址详情对话框
 */
var UsersAddressInfoDlg = {
    usersAddressInfoData : {}
};

/**
 * 清除数据
 */
UsersAddressInfoDlg.clearData = function() {
    this.usersAddressInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersAddressInfoDlg.set = function(key, val) {
    this.usersAddressInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersAddressInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UsersAddressInfoDlg.close = function() {
    parent.layer.close(window.parent.UsersAddress.layerIndex);
}

/**
 * 收集数据
 */
UsersAddressInfoDlg.collectData = function() {
    this
    .set('addressId')
    .set('userId')
    .set('userName')
    .set('userPhone')
    .set('provinceId')
    .set('cityId')
    .set('areaId')
    .set('userAddress')
    .set('isDefault')
    .set('dataFlag')
    .set('yonghuPhone')
    .set('provinceName')
    .set('cityName')
    .set('areaName')
    .set('createTime');
}

/**
 * 提交添加
 */
UsersAddressInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersAddress/add", function(data){
        Feng.success("添加成功!");
        window.parent.UsersAddress.table.refresh();
        UsersAddressInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersAddressInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UsersAddressInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersAddress/update", function(data){
        Feng.success("修改成功!");
        window.parent.UsersAddress.table.refresh();
        UsersAddressInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersAddressInfoData);
    ajax.start();
}

$(function() {

});
