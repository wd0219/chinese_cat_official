/**
 * 用户地址管理初始化
 */
var UsersAddress = {
    id: "UsersAddressTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
UsersAddress.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户手机', field: 'yonghuPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '用户类型', field: 'userType', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	//0普通 1主管 2经理
            	if(value=='0'){
            		return "普通用户";
            	}else if(value=='1'){
            		return "主管";
            	}else if(value=='2'){
            		return "经理";
            	}
            }},
            {title: '收货人名称', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '收货人手机', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '省id', field: 'provinceId', visible: false, align: 'center', valign: 'middle'},
            {title: '省', field: 'provinceName', visible: true, align: 'center', valign: 'middle'},
            {title: '市id', field: 'cityId', visible: false, align: 'center', valign: 'middle'},
            {title: '市', field: 'cityName', visible: true, align: 'center', valign: 'middle'},
            {title: '区县id', field: 'areaId', visible: false, align: 'center', valign: 'middle'},
            {title: '区县', field: 'areaName', visible: true, align: 'center', valign: 'middle'},
            {title: '详细地址', field: 'userAddress', visible: true, align: 'center', valign: 'middle'},
            {title: '是否默认地址', field: 'isDefault', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	if(value==0){
            		return "否";
            	}else if(value==1){
            		return "是";
            	}
            }},
            {title: '时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
UsersAddress.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        UsersAddress.seItem = selected[0];
        return true;
    }
};

//表格导出
UsersAddress.export= function(){
	$('#'+UsersAddress.id).tableExport({
		fileName:'地址导出',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}
/**
 * 点击添加用户地址
 */
UsersAddress.openAddUsersAddress = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户地址',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/usersAddress/usersAddress_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户地址详情
 */
UsersAddress.openUsersAddressDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户地址详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/usersAddress/usersAddress_update/' + UsersAddress.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户地址
 */
UsersAddress.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/usersAddress/delete", function (data) {
            Feng.success("删除成功!");
            UsersAddress.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("usersAddressId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户地址列表
 */
UsersAddress.search = function () {
    var queryData = {};
    queryData['yonghuPhone'] = $("#yonghuPhone").val();
    queryData['userType'] = $("#userType").val();
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    UsersAddress.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = UsersAddress.initColumn();
    var table = new BSTable(UsersAddress.id, "/usersAddress/list", defaultColunms);
    table.setPaginationType("server");
    UsersAddress.table = table.init();
});
