/**
 * 初始化商城订单审核详情对话框
 */
var SktOrderReviewInfoDlg = {
    sktOrderReviewInfoData : {}
};

/**
 * 清除数据
 */
SktOrderReviewInfoDlg.clearData = function() {
    this.sktOrderReviewInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrderReviewInfoDlg.set = function(key, val) {
    this.sktOrderReviewInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrderReviewInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktOrderReviewInfoDlg.close = function() {
    parent.layer.close(window.parent.SktOrderReview.layerIndex);
}

/**
 * 收集数据
 */
SktOrderReviewInfoDlg.collectData = function() {
    this
    .set('id')
    .set('userId')
    .set('orderunique')
    .set('outTradeNo')
    .set('orderStatus')
    .set('isPay')
    .set('payType')
    .set('payMoney')
    .set('payShopping')
    .set('checkRemark')
    .set('dataFlag')
    .set('createTime')
    .set('staffId')
    .set('auditDatetime');
}

/**
 * 提交添加
 */
SktOrderReviewInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrderReview/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktOrderReview.table.refresh();
        SktOrderReviewInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrderReviewInfoData);
    ajax.start();
}

//拒绝理由
SktOrderReviewInfoDlg.refusedSubmit = function(){
    var checkRemark = $("#checkRemark").val();
    if(checkRemark !== null && checkRemark !== undefined && checkRemark!== ''){
        $("#chesp").html("");
        var ajax = new $ax(Feng.ctxPath + "/sktOrderReview/updatePass", function (data) {
            if("01" == data){
                Feng.success("审核通过!");
                window.parent.SktOrderReview.table.refresh();
                SktOrderReviewInfoDlg.close();
            }else{
                Feng.error("操作失败");
            }
            // SktOrderReview.table.refresh();
        }, function (data) {
            Feng.error("审核失败!" + data.responseJSON.message + "!");
        });
        ajax.set("id",$("#id").val());
        ajax.set("orderStatus",-1);
        ajax.set("checkRemark",checkRemark);
        ajax.start();
}else{
    $("#chesp").html("拒绝理由不能为空");
    }

}

/**
 * 提交修改
 */
SktOrderReviewInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrderReview/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktOrderReview.table.refresh();
        SktOrderReviewInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrderReviewInfoData);
    ajax.start();
}

$(function() {

});
