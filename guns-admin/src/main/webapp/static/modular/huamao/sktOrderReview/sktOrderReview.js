/**
 * 商城订单审核管理初始化
 */
var SktOrderReview = {
    id: "SktOrderReviewTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktOrderReview.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {
            title:'id',align: 'center', valign: 'middle',width:50,
            formatter: function (value, row, index) {
                return index+1;
            }
        },
            // {title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户名', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            // {title: '用户id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '内部流水号', field: 'orderunique', visible: true, align: 'center', valign: 'middle'},
            {title: '第三方订单号', field: 'outTradeNo', visible: true, align: 'center', valign: 'middle'},
            {title: '订单状态', field: 'orderStatus', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    if(value == -1){
                        return "拒绝";
                    }else if(value == 0){
                        return "已付款";
                    }else if(value == 1){
                        return "审核通过";
                    }
                    return "";
                }},
            {title: '是否支付', field: 'isPay', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    if(value == 0){
                        return "未支付";
                    }else if(value == 1){
                        return "已支付";
                    }
                    return "";
                }},
            {title: '支付方式', field: 'payType', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    if(value == 1){
                        return "微信";
                    }else if(value == 2){
                        return "支付宝";
                    }else if(value == 3){
                        return "快捷";
                    }
                    return "";
                }},
            {title: '商城订单总金额', field: 'payMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '购买商品支付的购物券', field: 'payShopping', visible: true, align: 'center', valign: 'middle'},
            {title: '拒绝理由', field: 'checkRemark', visible: true, align: 'center', valign: 'middle'},
            // {title: '订单有效标志', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '下单时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核人', field: 'staffName', visible: true, align: 'center', valign: 'middle'},
            {title: '审核时间', field: 'auditDatetime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'orderStatus', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    if(value == 0){
                        return "<a href='javascript:void(0)' onclick=\"SktOrderReview.approve()\">审核通过</a>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"SktOrderReview. refused()\">审核拒绝</a>";
                    }
                    return "";
                }}
    ];
};

/**
 * 审核通过
 */
SktOrderReview.approve = function () {
    if (this.check()) {
        var operation = function (){
        var ajax = new $ax(Feng.ctxPath + "/sktOrderReview/updatePass", function (data) {
            if("01" == data){
                Feng.success("审核通过!");
                SktOrderReview.table.refresh();
            }else{
                Feng.error("操作失败");
            }
        }, function (data) {
            Feng.error("审核失败!" + data.responseJSON.message + "!");
        });
        ajax.set("id",SktOrderReview.seItem.id);
        ajax.set("orderStatus",1);
        ajax.start();
    };
        Feng.confirm("是否确认通过审核?",operation);
    }
};

/**
 * 审核拒绝
 */
SktOrderReview.refused = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商城订单审核拒绝',
            area: ['800px', '210px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktOrderReview/sktOrderReview_refused/' + SktOrderReview.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 检查是否选中
 */
SktOrderReview.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktOrderReview.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商城订单审核
 */
SktOrderReview.openAddSktOrderReview = function () {
    var index = layer.open({
        type: 2,
        title: '添加商城订单审核',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktOrderReview/sktOrderReview_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商城订单审核详情
 */
SktOrderReview.openSktOrderReviewDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商城订单审核详情',
            area: ['800px', '340px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktOrderReview/sktOrderReview_update/' + SktOrderReview.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商城订单审核
 */
SktOrderReview.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktOrderReview/delete", function (data) {
            Feng.success("删除成功!");
            SktOrderReview.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktOrderReviewId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商城订单审核列表
 */
SktOrderReview.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['loginName'] = $("#loginName").val();
    queryData['orderunique'] = $("#orderunique").val();
    queryData['outTradeNo'] = $("#outTradeNo").val();
    queryData['orderStatus'] = $("#orderStatus").val();
    queryData['isPay'] = $("#isPay").val();
    queryData['payType'] = $("#payType").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['payName'] = $("#payName").val();
    SktOrderReview.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktOrderReview.initColumn();
    var table = new BSTable(SktOrderReview.id, "/sktOrderReview/list", defaultColunms);
    // table.setPaginationType("client");
    table.setPaginationType("server");
    SktOrderReview.table = table.init();
});
