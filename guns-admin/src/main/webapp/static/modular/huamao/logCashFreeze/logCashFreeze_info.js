/**
 * 初始化待发现金记录详情对话框
 */
var LogCashFreezeInfoDlg = {
    logCashFreezeInfoData : {}
};

/**
 * 清除数据
 */
LogCashFreezeInfoDlg.clearData = function() {
    this.logCashFreezeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogCashFreezeInfoDlg.set = function(key, val) {
    this.logCashFreezeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogCashFreezeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogCashFreezeInfoDlg.close = function() {
    parent.layer.close(window.parent.LogCashFreeze.layerIndex);
}

/**
 * 收集数据
 */
LogCashFreezeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preCash')
    .set('cashType')
    .set('cash')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogCashFreezeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logCashFreeze/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogCashFreeze.table.refresh();
        LogCashFreezeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logCashFreezeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogCashFreezeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logCashFreeze/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogCashFreeze.table.refresh();
        LogCashFreezeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logCashFreezeInfoData);
    ajax.start();
}

$(function() {

});
