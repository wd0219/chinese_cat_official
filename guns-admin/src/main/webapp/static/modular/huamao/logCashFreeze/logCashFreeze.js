/**
 * 待发现金记录管理初始化
 */
var LogCashFreeze = {
    id: "LogCashFreezeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogCashFreeze.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前待发现金余额', field: 'preCash', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动金额', field: 'cashp', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后待发现金余额', field: 'acash', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogCashFreeze.type},
            //{title: '发起用户ID 0为平台', field: 'fromId', visible: true, align: 'center', valign: 'middle'},
            //{title: '目标用户ID 0为平台', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'cashType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 资金变动类型对应：1分享奖励>2商城货款>3商家货款>31提现>32购买库存积分
 */
LogCashFreeze.type = function(value,row,index){
	if(value == 1){
		return "分享奖励";
	}else if(value == 2){
		return "商城货款";
	}else if(value == 3){
		return "商家货款";
	}else if(value == 31){
		return "提现";
	}else if(value == 32){
		return "购买库存积分";
	}
}
/**
 * 检查是否选中
 */
LogCashFreeze.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogCashFreeze.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加待发现金记录
 */
LogCashFreeze.openAddLogCashFreeze = function () {
    var index = layer.open({
        type: 2,
        title: '添加待发现金记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logCashFreeze/logCashFreeze_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看待发现金记录详情
 */
LogCashFreeze.openLogCashFreezeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '待发现金记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logCashFreeze/logCashFreeze_update/' + LogCashFreeze.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除待发现金记录
 */
LogCashFreeze.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logCashFreeze/delete", function (data) {
            Feng.success("删除成功!");
            LogCashFreeze.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logCashFreezeId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询待发现金记录列表
 */
LogCashFreeze.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogCashFreeze.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogCashFreeze.initColumn();
    var table = new BSTable(LogCashFreeze.id, "/logCashFreeze/list/"+$('#userPhone2').val(), defaultColunms);
    table.setPaginationType("server");
    LogCashFreeze.table = table.init();
});
//导出Excel
LogCashFreeze.export= function(){
	$('#'+LogCashFreeze.id).tableExport({
		fileName:'待发现金记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}