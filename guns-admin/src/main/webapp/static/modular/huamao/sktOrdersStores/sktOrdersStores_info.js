/**
 * 初始化线下订单管理详情对话框
 */
var SktOrdersStoresInfoDlg = {
    sktOrdersStoresInfoData : {}
};

/**
 * 清除数据
 */
SktOrdersStoresInfoDlg.clearData = function() {
    this.sktOrdersStoresInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrdersStoresInfoDlg.set = function(key, val) {
    this.sktOrdersStoresInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrdersStoresInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktOrdersStoresInfoDlg.close = function() {
    parent.layer.close(window.parent.SktOrdersStores.layerIndex);
}

/**
 * 收集数据
 */
SktOrdersStoresInfoDlg.collectData = function() {
    this
    .set('orderId')
    .set('orderNo')
    .set('shopId')
    .set('shopName')
    .set('userId')
    .set('loginName')
    .set('orderStatus')
    .set('orderType')
    .set('payType')
    .set('totalMoney')
    .set('realMoney')
    .set('cash')
    .set('kaiyuan')
    .set('kaiyuanFee')
    .set('scoreRatio')
    .set('score')
    .set('orderRemarks')
    .set('payMoney')
    .set('createTime')
    .set('checkTime')
    .set('dataFlag');
}

/**
 * 提交添加
 */
SktOrdersStoresInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrdersStores/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktOrdersStores.table.refresh();
        SktOrdersStoresInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrdersStoresInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktOrdersStoresInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrdersStores/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktOrdersStores.table.refresh();
        SktOrdersStoresInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrdersStoresInfoData);
    ajax.start();
}

$(function() {
	$('#payType').val(sktOrdersStoresTransform.payType($('#payType').val()));
	$('#orderType').val(sktOrdersStoresTransform.payType($('#orderType').val()));
	$('#orderStatus').val(sktOrdersStoresTransform.payType($('#orderStatus').val()));
});
