/**
 * 线下订单管理初始化
 */
var SktOrdersStores = {
    id: "SktOrdersStoresTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktOrdersStores.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '订单编号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '门店ID', field: 'shopId', visible: false, align: 'center', valign: 'middle'},
            {title: '用户Id', field: 'userId', visible: false, align: 'center', valign: 'middle'},
            {title: '收货人', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '订单总金额', field: 'totalMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '支付金额', field: 'payMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '支付方式', field: 'payType', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	//1:线下现金支付 2 现金账户支付 3华宝支付 4第三方支付 5混合支付
            	var result = '';
            	switch(value){
            	case 1:
            		result = '线下现金支付';
            		break;
            	case 2:
            		result = '现金账户支付 ';
            		break;
            	case 3:
            		result = '华宝支付 ';
            		break;
            	case 4:
        			result = '第三方支付';
        			break;
            	case 5:
        			result = '混合支付 ';
        			break;
            	}  
            	return result;
            }},
            {title: '订单状态', field: 'orderStatus', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	//订单状态 1:用户下单 2商家取消 3已完成
            	var result = '';
            	switch(value){
            	case 1:
            		result = '用户下单';
            		break;
            	case 2:
            		result = '商家取消';
            		break;
            	case 3:
            		result = '已完成';
            		break;
            	}
            	return result;
            }},
            {title: '订单来源', field: 'orderType', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	//订单来源 1商家补单 2消费者主动下单 3面对面付
            	var result = '';
            	switch(value){
            	case 1:
            		result = '商家补单';
            		break;
            	case 2:
            		result = '消费者主动下单';
            		break;
            	case 3:
            		result = '面对面付';
            		break;
            	}
            	return result;
            }},
            {title: '下单时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'op', visible: true, align: 'center', valign: 'middle',formatter:SktOrdersStores.op},
    ];
};

SktOrdersStores.op = function(){
	return "<a href='javascript:void(0)' onclick=\"SktOrdersStores.openSktOrdersStoresDetail()\">查看</a>"
}
/**
 * 检查是否选中
 */
SktOrdersStores.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktOrdersStores.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加线下订单管理
 */
SktOrdersStores.openAddSktOrdersStores = function () {
    var index = layer.open({
        type: 2,
        title: '添加线下订单管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktOrdersStores/sktOrdersStores_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看线下订单管理详情
 */
SktOrdersStores.openSktOrdersStoresDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '线下订单管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktOrdersStores/sktOrdersStores_update/' + SktOrdersStores.seItem.orderId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除线下订单管理
 */
SktOrdersStores.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktOrdersStores/delete", function (data) {
            Feng.success("删除成功!");
            SktOrdersStores.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktOrdersStoresId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询线下订单管理列表
 */
SktOrdersStores.search = function () {
    var queryData = {};
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['orderNo'] = $("#orderNo").val();
    queryData['shopName'] = $("#shopName").val();
    queryData['userPhone'] = $("#userPhone").val();
    queryData['loginName'] = $("#loginName").val();
    queryData['orderType'] = $("#orderType").val();
    queryData['orderStatus'] = $("#orderStatus").val();
    queryData['payType'] = $("#payType").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    SktOrdersStores.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktOrdersStores.initColumn();
    var table = new BSTable(SktOrdersStores.id, "/sktOrdersStores/list", defaultColunms);
    table.setPaginationType("server");
    SktOrdersStores.table = table.init();
});
