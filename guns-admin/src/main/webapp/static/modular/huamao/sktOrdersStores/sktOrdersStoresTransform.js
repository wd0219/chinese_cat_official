var sktOrdersStoresTransform = {
		//订单来源 1商家补单 2消费者主动下单 3面对面付
		orderType : function(value){
			//订单来源 1商家补单 2消费者主动下单 3面对面付
        	let result = '';
        	switch(value){
        	case '1':
        		result = '商家补单';
        		break;
        	case '2':
        		result = '消费者主动下单';
        		break;
        	case '3':
        		result = '面对面付';
        		break;
        	}
        	return result;
		},
		orderStatus : function(value){
			//订单状态 1:用户下单 2商家取消 3已完成
        	let result = '';
        	switch(value){
        	case '1':
        		result = '用户下单';
        		break;
        	case '2':
        		result = '商家取消';
        		break;
        	case '3':
        		result = '已完成';
        		break;
        	}
        	return result;
		},
		payType : function(value){
			//1:线下现金支付 2 现金账户支付 3华宝支付 4第三方支付 5混合支付
        	let result = '';
        	switch(value){
        	case '1':
        		result = '线下现金支付';
        		break;
        	case '2':
        		result = '现金账户支付 ';
        		break;
        	case '3':
        		result = '华宝支付 ';
        		break;
        	case '4':
    			result = '第三方支付';
    			break;
        	case '5':
    			result = '混合支付 ';
    			break;
        	}  
        	return result;
		}
		

}