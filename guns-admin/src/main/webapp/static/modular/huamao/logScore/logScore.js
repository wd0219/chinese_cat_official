/**
 * 积分记录管理初始化
 */
var LogScore = {
    id: "LogScoreTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogScore.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前余额', field: 'preScore', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动数量', field: 'scorep', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后余额', field: 'ascore', visible: true, align: 'center', valign: 'middle'},
            {title: '类型 ', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogScore.type},
            {title: '支出人', field: 'fromName', visible: true, align: 'center', valign: 'middle',formatter:LogScore.fromName},
            {title: '收入人', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'scoreType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 支出人空为平台
 */
LogScore.fromName = function(value,row,index){
	if(value == 0){
		return "平台";
	}else{
		return value;
	}
}
/**
 * 类型：1商城消费奖励>2商家消费奖励>3购买库存奖励>4消费奖励>5销售奖励>6特别奖励>7升级奖励>
8升级奖励>9代理奖励>10分享代理奖励>11分红>12待发积分转入>13购买库存积分>14手动补发>
15特别奖励>16购买牌匾奖励>17消费奖励>18消费退款>31转化华宝>32手动扣除>33购买自营商品
 *将数字对应成文字
 */
LogScore.type = function(value,row,index){
	if(value == 1){
		return "商城消费奖励";
	}else if(value == 2){
		return "商家消费奖励";
	}else if(value == 3){
		return "购买库存奖励";
	}else if(value == 4){
		return "消费奖励";
	}else if(value == 5){
		return "销售奖励";
	}else if(value == 6){
		return "特别奖励";
	}else if(value == 7){
		return "升级奖励";
	}else if(value == 8){
		return "升级奖励";
	}else if(value == 9){
		return "代理奖励";
	}else if(value == 10){
		return "分享代理奖励";
	}else if(value == 11){
		return "分红";
	}else if(value == 12){
		return "待发积分转入";
	}else if(value == 13){
		return "购买库存积分";
	}else if(value == 14){
		return "手动补发";
	}else if(value == 15){
		return "特别奖励";
	}else if(value == 16){
		return "购买牌匾奖励";
	}else if(value == 17){
		return "消费奖励";
	}else if(value == 18){
		return "消费退款";
	}else if(value == 31){
		return "转化华宝";
	}else if(value == 32){
		return "手动扣除";
	}else if(value == 33){
		return "购买自营商品";
	}else if(value == 36){
		return "转化购物券";
	}
}
/**
 * 检查是否选中
 */
LogScore.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogScore.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加积分记录
 */
LogScore.openAddLogScore = function () {
    var index = layer.open({
        type: 2,
        title: '添加积分记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logScore/logScore_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看积分记录详情
 */
LogScore.openLogScoreDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '积分记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logScore/logScore_update/' + LogScore.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除积分记录
 */
LogScore.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logScore/delete", function (data) {
            Feng.success("删除成功!");
            LogScore.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logScoreId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询积分记录列表
 */
LogScore.search = function () {
	var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogScore.table.refresh({query: queryData});
};

$(function () {
	var defaultColunms = LogScore.initColumn();
    var table = new BSTable(LogScore.id, "/logScore/list/"+$('#userPhone2').val(), defaultColunms);
    table.setPaginationType("server");
    LogScore.table = table.init();
});
//导出Excel
LogScore.export= function(){
	$('#'+LogScore.id).tableExport({
		fileName:'积分记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}
