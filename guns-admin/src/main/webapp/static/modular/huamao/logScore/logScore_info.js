/**
 * 初始化积分记录详情对话框
 */
var LogScoreInfoDlg = {
    logScoreInfoData : {}
};

/**
 * 清除数据
 */
LogScoreInfoDlg.clearData = function() {
    this.logScoreInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogScoreInfoDlg.set = function(key, val) {
    this.logScoreInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogScoreInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogScoreInfoDlg.close = function() {
    parent.layer.close(window.parent.LogScore.layerIndex);
}

/**
 * 收集数据
 */
LogScoreInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preScore')
    .set('scoreType')
    .set('score')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogScoreInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logScore/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogScore.table.refresh();
        LogScoreInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logScoreInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogScoreInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logScore/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogScore.table.refresh();
        LogScoreInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logScoreInfoData);
    ajax.start();
}

$(function() {

});
