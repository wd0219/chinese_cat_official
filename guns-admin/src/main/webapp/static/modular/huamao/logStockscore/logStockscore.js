/**
 * 库存积分记录管理初始化
 */
var LogStockscore = {
    id: "LogStockscoreTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogStockscore.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前数量', field: 'preScore', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动数量', field: 'scorep', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后数量', field: 'ascore', visible: true, align: 'center', valign: 'middle'},
        	{title: '类型 ', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogStockscore.type},
            //{title: '发起用户ID 0为平台', field: 'fromId', visible: true, align: 'center', valign: 'middle'},
            //{title: '目标用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'scoreType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 类型对应：1库存奖励>2消费>3退货返还>4商家消费奖励>5手动添加>6手动减少
 */
LogStockscore.type = function(value,row,index){
	if(value == 1){
		return "库存奖励";
	}else if(value == 2){
		return "消费";
	}else if(value == 3){
		return "退货返还";
	}else if(value == 4){
		return "商家消费奖励";
	}else if(value == 5){
		return "手动添加";
	}else if(value == 6){
		return "手动扣除";
	}
}
/**
 * 检查是否选中
 */
LogStockscore.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogStockscore.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加库存积分记录
 */
LogStockscore.openAddLogStockscore = function () {
    var index = layer.open({
        type: 2,
        title: '添加库存积分记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logStockscore/logStockscore_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看库存积分记录详情
 */
LogStockscore.openLogStockscoreDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '库存积分记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logStockscore/logStockscore_update/' + LogStockscore.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除库存积分记录
 */
LogStockscore.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logStockscore/delete", function (data) {
            Feng.success("删除成功!");
            LogStockscore.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logStockscoreId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询库存积分记录列表
 */
LogStockscore.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogStockscore.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogStockscore.initColumn();
    var table = new BSTable(LogStockscore.id, "/logStockscore/list/"+$('#userPhone2').val()+"?type2="+$('#type2').val(), defaultColunms);
    table.setPaginationType("server");
    LogStockscore.table = table.init();
});
//导出Excel
LogStockscore.export= function(){
	$('#'+LogStockscore.id).tableExport({
		fileName:'库存积分记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}