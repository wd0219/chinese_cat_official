/**
 * 初始化库存积分记录详情对话框
 */
var LogStockscoreInfoDlg = {
    logStockscoreInfoData : {}
};

/**
 * 清除数据
 */
LogStockscoreInfoDlg.clearData = function() {
    this.logStockscoreInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogStockscoreInfoDlg.set = function(key, val) {
    this.logStockscoreInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogStockscoreInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogStockscoreInfoDlg.close = function() {
    parent.layer.close(window.parent.LogStockscore.layerIndex);
}

/**
 * 收集数据
 */
LogStockscoreInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preScore')
    .set('scoreType')
    .set('score')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogStockscoreInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logStockscore/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogStockscore.table.refresh();
        LogStockscoreInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logStockscoreInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogStockscoreInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logStockscore/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogStockscore.table.refresh();
        LogStockscoreInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logStockscoreInfoData);
    ajax.start();
}

$(function() {

});
