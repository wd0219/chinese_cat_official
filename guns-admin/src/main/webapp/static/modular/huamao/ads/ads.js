/**
 * 广告管理管理初始化
 */
var Ads = {
    id: "AdsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Ads.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '广告表自增id', field: 'adId', visible: false, align: 'center', valign: 'middle'},
            {title: '标题', field: 'adName', visible: true, align: 'center', valign: 'middle'},
            {title: '广告位置', field: 'positionName', visible: true, align: 'center', valign: 'middle'},
            {title: '广告网址', field: 'adURL', visible: true, align: 'center', valign: 'middle'},
            {title: '广告开始日期', field: 'adStartDate', visible: true, align: 'center', valign: 'middle'},
            {title: '广告结束日期', field: 'adEndDate', visible: true, align: 'center', valign: 'middle'},
            {title: '图标', field: 'adFile', visible: true, align: 'center', valign: 'middle',formatter:Ads.showAds},
            {title: '点击数', field: 'adClickNum', visible: true, align: 'center', valign: 'middle'},
            {title: '排序号', field: 'adSort', visible: true, align: 'center', valign: 'middle'},
            {title: '前端是否显示', field: 'dataFlag', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
                    //1显示 -1隐藏
                    if(value=='1'){
                        return "启用";
                    }else if(value=='-1'){
                        return "禁用";
                    }
                }
            },
            {title: '操作', field: 'option', visible: true, align: 'center', valign: 'middle',formatter:Ads.op}
    ];
};
/*
 * 操作：给广告添加修改、删除功能
 * */
Ads.op = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"Ads.openAdsDetail()\">修改</a>&nbsp;&nbsp;<a href='javascript:void(0)'onclick=\"Ads.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
Ads.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Ads.seItem = selected[0];
        return true;
    }
};

//图片显示
Ads.showAds = function (value,row,index){
    const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
    if(value.indexOf('http')==-1){
        value=imgUrl+value
    }else{
        value=value
    }
    var img = '<img style="width:70;height:30px;"  src="'+value+'" />';
    return img;
}

/**
 * 点击添加广告管理
 */
Ads.openAddAds = function () {
    var index = layer.open({
        type: 2,
        title: '添加广告管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ads/ads_add'
    });
    this.layerIndex = index;
};

/**
 * 打开修改
 */
Ads.openAdsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '广告管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ads/ads_update/' + Ads.seItem.adId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除广告管理
 */
Ads.delete = function () {
    if (this.check()) {
    	var operation = function(){
    	var adId = Ads.seItem.adId;
        var ajax = new $ax(Feng.ctxPath + "/ads/delete", function (data) {
            Feng.success("删除成功!");
            Ads.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("adsId",adId);
        ajax.start();
    };
        Feng.confirm("是否删除该条记录?",operation);
    }
};

/**
 * 查询广告管理列表
 */
Ads.search = function () {
    var queryData = {};
    queryData['positionType'] = $("#positionType").val();
    queryData['adPositionId'] = $("#adPositionId").val();
    Ads.table.refresh({query: queryData});
};
/**
 * 点击类型异步获取广告位置
 */
$("#positionType").change(function(){
	$("#adPositionId").html("<option value=''>请选择</option>");
	var positionType = $("#positionType").val();
	$.post('/ads/getPosition',
    		{"positionType":positionType},
             function (req) {
    			for(var i in req){
    				$("#adPositionId").append("<option value="+req[i].adPositionId+">"+req[i].positionName+"</option>");
    			}
             });
});
$(function () {
    var defaultColunms = Ads.initColumn();
    var table = new BSTable(Ads.id, "/ads/list", defaultColunms);
    table.setPaginationType("server");
    Ads.table = table.init();
});
