/**
 * 初始化广告管理详情对话框
 */
var AdsInfoDlg = {
    adsInfoData : {}
};
/**
 * 清除数据
 */
AdsInfoDlg.clearData = function() {
    this.adsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AdsInfoDlg.set = function(key, val) {
    this.adsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AdsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AdsInfoDlg.close = function() {
    parent.layer.close(window.parent.Ads.layerIndex);
}

/**
 * 收集数据
 */
AdsInfoDlg.collectData = function() {
    this
    .set('adId')
    .set('adPositionId')
    .set('adFile')
    .set('adName')
    .set('adURL')
    .set('startDate')
    .set('endDate')
    .set('adSort')
    .set('adClickNum')
    .set('positionType')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
AdsInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var adName = $("#adName").val();
    var adSort = $("#adSort").val();
    var positionType = $("#positionType").val();
    var adPositionId = $("#adPositionId").val();
    var adFile = $("#adFile").val();
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();

    if(adName == null || adName ==""){
        var html='<span class="error">广告标题不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#adName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(positionType == null || positionType =="-1"){
        var html='<span class="error">菜单类型不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#positionType').after(html);
        falg = 1;
    }
    if(adSort == null || adSort ==""){
        var html='<span class="error">排序号!</span>';
        //$('#menuType').parent().append(html);
        $('#adSort').after(html);
        falg = 1;
    }

    if(adPositionId == null || adPositionId ==""){
        var html='<span class="error">广告位置不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#adPositionId').after(html);
        falg = 1;
    }

    if(adFile == null || adFile ==""){
        var html='<span class="error">图片不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#adFile').after(html);
        falg = 1;
    }
    if(startDate == null || startDate ==""){
        var html='<span class="error">开始时间不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#startDate').after(html);
        falg = 1;
    }
    if(endDate == null || endDate ==""){
        var html='<span class="error">结束时间不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#endDate').after(html);
        falg = 1;
    }


    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/ads/add", function(data){
        Feng.success("添加成功!");
        window.parent.Ads.table.refresh();
        AdsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.adsInfoData);
    ajax.start();
    }else{
	Feng.error("请核实添加字段!");
    }
}
/**
 * 点击类型异步获取广告位置
 */
$("#positionType").change(function(){
	$("#adPositionId").html("<option value=''>请选择</option>");
	var positionType = $("#positionType").val();
	$.post('/ads/getPosition',
    		{"positionType":positionType},
             function (req) {
    			for(var i in req){
    				$("#adPositionId").append("<option value="+req[i].adPositionId+">"+req[i].positionName+"</option>");
    			}
             });
});
/**
 * 提交修改
 */
AdsInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;
    var adId = $("#adId").val();
    var adName = $("#adName").val();
    var positionType = $("#positionType").val();
    var adPositionId = $("#adPositionId").val();
    var adSort = $("#adSort").val();
    var adFile = $("#adFile").val();
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var dataFlag = $("#dataFlag").val();
    if(adName == null || adName ==""){
        var html='<span class="error">广告标题不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#adName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(positionType == null || positionType =="-1"){
        var html='<span class="error">菜单类型不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#positionType').after(html);
        falg = 1;
    }

    if(adPositionId == null || adPositionId ==""){
        var html='<span class="error">广告位置不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#adPositionId').after(html);
        falg = 1;
    }

    if(adSort == null || adSort ==""){
        var html='<span class="error">排序号!</span>';
        //$('#menuType').parent().append(html);
        $('#adSort').after(html);
        falg = 1;
    }

    if(adFile == null || adFile ==""){
        var html='<span class="error">图片不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#adFile').after(html);
        falg = 1;
    }
    if(startDate == null || startDate ==""){
        var html='<span class="error">开始时间不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#startDate').after(html);
        falg = 1;
    }
    if(endDate == null || endDate ==""){
        var html='<span class="error">结束时间不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#endDate').after(html);
        falg = 1;
    }


    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/ads/update", function(data){
	        Feng.success("修改成功!");
	        AdsInfoDlg.close();
            window.parent.Ads.table.refresh();
        },function(data){
	        Feng.error("修改失败!" + data.responseJSON.message + "!");
	    });
		  ajax.set(this.adsInfoData);
		  ajax.start();
		    }else{
		    	Feng.error("请核实添加字段!");
		}
	}


function isRealNum(val){
    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
    if(val === "" || val ==null){
        return false;
    }
    if(!isNaN(val)){
        return true;
    }else{
        return false;
    }
}

$(function() {
    $("#dataFlag").val($("#dataFlagh").val());
    $("#positionType").val($("#positionTypeh").val());
    $("#startDate").val($("#adStartDateh").val());
    $("#endDate").val($("#adEndDateh").val());
    var positionType = $("#positionType").val();
    $.post('/ads/getPosition',
        {"positionType":positionType},
        function (req) {
            for(var i in req){
                if(req[i].adPositionId == $("#adPositionIdh").val()){
                    $("#adPositionId").append("<option value="+req[i].adPositionId+" selected>"+req[i].positionName+"</option>");
                }else {
                    $("#adPositionId").append("<option value=" + req[i].adPositionId + ">" + req[i].positionName + "</option>");
                }
            }
        });
    //$("#adPositionId").val($("#adPositionIdh").val());
	var adFile = new $WebUpload("adFile");
	adFile.setUploadBarId("progressBar");
	adFile.init();
});

$("#URLType").change(function () {
    if($("#URLType").val() == 0){
        $("#soNamed").hide();
        $("#soName").val("");
        $("#adURLd").hide();
        $("#adURL").val("");

    }
    //1内部链接 2外部链接
    if($("#URLType").val() == 2){
        $("#soNamed").hide();
        $("#soName").val("");
        $("#adURL").val("");
        $("#adURLd").show();
    };
    if($("#URLType").val() == 1){
        $("#soName").val("");
        $("#soNamed").show();
        $("#adURLd").hide();
        $("#adURL").val("");
    };
});

$("#searchType").change(function () {
    if($("#searchType").val() == ""){
        parent.document.getElementById("adURL").value = "";
        parent.document.getElementById("soName").value = "";
        $("#radiod").html("");
        $("#soNamed2").hide();
        $("#soName2").val("");

    }
    //1店铺 2商品 3公告
    if($("#searchType").val() == 1){
        parent.document.getElementById("adURL").value = "";
        parent.document.getElementById("soName").value = "";
        $("#radiod").html("");
        $("#adURLd").hide();
        $("#adURL").val("");
        $("#soName2").val("");
        $("#soNamed2").show();

    };
    if($("#searchType").val() == 2){
        parent.document.getElementById("adURL").value = "";
        parent.document.getElementById("soName").value = "";
        $("#radiod").html("");
        $("#adURLd").hide();
        $("#adURL").val("");
        $("#soName2").val("");
        $("#soNamed2").show();

    };
    if($("#searchType").val() == 3){
        parent.document.getElementById("adURL").value = "";
        parent.document.getElementById("soName").value = "";
        $("#radiod").html("");
        $("#adURLd").hide();
        $("#adURL").val("");
        $("#soName2").val("");
        $("#soNamed2").show();

    };
});
function so2() {
    var soName = $("#soName2").val();
    var searchType = $("#searchType").val();
    $.post('/ads/getSo',
        {"soName": soName, "searchType": searchType},
        function (req) {
            if(req.code == "00"){
                Feng.error(req.msg);
            }
            if(req.code == "01"){
                //1店铺 2商品 3公告
                if(req.type == 1){
                    var shops = req.shops;
                    var hh = "";
                    for(var i= 0;i<shops.length;i++) {
                            hh += "<input type='radio' name='soname' value='"+shops[i].shopId+"'><span id='"+shops[i].shopId+"'>"+shops[i].shopName+"</span><br/><br/>";
                    }
                    $("#radiod").html(hh);
                }else if(req.type == 2){
                    var goods = req.goods;
                    var hh = "";
                    for(var i= 0;i<goods.length;i++) {
                        hh += "<input type='radio' name='soname' value='"+goods[i].goodsId+"'><span id='"+goods[i].goodsId+"'>"+goods[i].goodsName+"</span><br/><br/>";
                    }
                    $("#radiod").html(hh);
                }else if(req.type == 3){
                    var articles = req.articles;
                    var hh = "";
                    for(var i= 0;i<articles.length;i++) {
                        hh += "<input type='radio' name='soname' value='"+articles[i].articleId+"'><span id='"+articles[i].articleId+"'>"+articles[i].articleTitle+"</span><br/><br/>";
                    }
                    $("#radiod").html(hh);
                }
            }
        });
}
function so() {
    var index = layer.open({
        type: 2,
        title: "搜索",
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ads/adsSelect'
    });
    this.layerIndex = index;
}
AdsInfoDlg.adsClose = function () {
    $('#layui-layer1').hide();console.log('xyy')
    $('#layui-layer-shade1').hide()
}

AdsInfoDlg.adsAddSubmit = function (){
    var setype = $("#searchType").val();
    if(setype == ""){
        Feng.error("请选择搜索类型");
        return;
    }
    var rad = $('input:radio[name=soname]:checked').val();
    if(rad == "" || rad == null || rad == undefined){
        Feng.error("请选择名称");
        return;
    }
    if(setype == 1){
        //店铺
        parent.document.getElementById("adURL").value = "shopId:"+rad;
        parent.document.getElementById("soName").value = $("#"+rad).html();
    }else if(setype == 2){
        //商品
        parent.document.getElementById("adURL").value = "goodsId:"+rad;
        parent.document.getElementById("soName").value = $("#"+rad).html();
    }else if(setype == 3){
        //公告
        parent.document.getElementById("adURL").value = "articleId:"+rad;
        parent.document.getElementById("soName").value = $("#"+rad).html();
    }
    // window.parent.location.reload();
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}