/**
 * 商品规格分类管理初始化
 */
var SpecCats = {
    id: "SpecCatsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SpecCats.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '类型名称', field: 'catName', visible: true, align: 'center', valign: 'middle'},
            {title: '商品分类路径', field: 'goodsCatPath', visible: true, align: 'center', valign: 'middle'},
            {title: '是否允许上传图片', field: 'isAllowImg', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
               		var result;
               		switch(value){
               		case 1:
               			result = "允许";
               			break;
               		case 0:
               			result = "不允许 ";
               			break;
               		}

               		return result;
            }},
            {title: '是否显示', field: 'isShow', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
               		var result;
               		switch(value){
               		case 1:
               			result = "显示";
               			break;
               		case 2:
               			result = "不显示 ";
               			break;
               		}

               		return result;
            }},
           
            {title: '最后一级商品分类ID', field: 'goodsCatId', visible: false, align: 'center', valign: 'middle'},
            {title: '排序号', field: 'catSort', visible: true, align: 'center', valign: 'middle'},
            {title: '有效状态：1有效-1无效', field: 'dataFlag', visible: false, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: false, align: 'center', valign: 'middle'},
        	{title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:SpecCats.open}
            
       ];
};
SpecCats.open = function () {
	return "<a href='javascript:void(0)' onclick=\"SpecCats.openSpecCatsDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SpecCats.delete2()\">删除</a>"
}
/**
 * 检查是否选中
 */
SpecCats.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SpecCats.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品规格分类
 */
SpecCats.openAddSpecCats = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品规格分类',
        area:  ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/specCats/specCats_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品规格分类详情
 */
SpecCats.openSpecCatsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品规格分类详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/specCats/specCats_update/' + SpecCats.seItem.catId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品规格分类
 */
SpecCats.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/specCats/delete", function (data) {
            Feng.success("删除成功!");
            SpecCats.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("specCatsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 删除商品规格分类
 */
SpecCats.delete2 = function () {
    if (this.check()) {
      var operation = function(){
    	  var catId=SpecCats.seItem.catId;
	      var ajax = new $ax(Feng.ctxPath + "/specCats/delete", function (data) {
	            Feng.success("删除成功!");
	            SpecCats.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("specCatsId",catId);
	        ajax.start();
	    };
	    Feng.confirm("是否删除 该条记录?",operation);
    }
};
/**
 * 查询商品规格分类列表
 */
SpecCats.search = function () {
    var queryData = {};
    queryData['catName'] = $("#catName").val();
    queryData['goods1'] = $("#goods1").val();
    queryData['goods2'] = $("#goods2").val();
    queryData['goods3'] = $("#goods3").val();
    SpecCats.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SpecCats.initColumn();
    var table = new BSTable(SpecCats.id, "/specCats/list", defaultColunms);
    table.setPaginationType("server");
    SpecCats.table = table.init();
});
