/**
 * 初始化商品规格分类详情对话框
 */
var SpecCatsInfoDlg = {
    specCatsInfoData : {}
};

/**
 * 清除数据
 */
SpecCatsInfoDlg.clearData = function() {
    this.specCatsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SpecCatsInfoDlg.set = function(key, val) {
    this.specCatsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SpecCatsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SpecCatsInfoDlg.close = function() {
    parent.layer.close(window.parent.SpecCats.layerIndex);
}

/**
 * 收集数据
 */
SpecCatsInfoDlg.collectData = function() {
    this
    .set('catId')
    .set('goodsCatId')
    .set('goodsCatPath')
    .set('catName')
    .set('isAllowImg')
    .set('isShow')
    .set('catSort')
    .set('dataFlag')
    .set('createTime')
     .set("goods1")
    .set("goods2")
    .set("goods3");
}

/**
 * 提交添加
 */
SpecCatsInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var catName = $("#catName").val();
    var goods1 = $("#goods1").val();

    if(catName == null || catName ==""){
        var html='<span class="error">规格名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#catName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(goods1 == null || goods1 ==""){
        var html='<span class="error">所属分类不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#goods1').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    var radios = document.getElementsByName("isShow");
    // alert(radios);
     for ( var i = 0; i < radios.length; i++) {
 	    if (radios[i].checked==true){
 	    	this.specCatsInfoData.isShow=$(radios[i]).val();
 	    }
     }
     var radios2 = document.getElementsByName("isAllowImg");
     // alert(radios);
      for ( var i = 0; i < radios2.length; i++) {
  	    if (radios2[i].checked==true){
  	    	this.specCatsInfoData.isAllowImg=$(radios2[i]).val();
  	    }
      } 
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/specCats/add", function(data){
        Feng.success("添加成功!");
        window.parent.SpecCats.table.refresh();
        SpecCatsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.specCatsInfoData);
    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
}

	function isRealNum(val){
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	if(val === "" || val ==null){
	    return false;
	}
	if(!isNaN(val)){
	    return true;
	}else{
	    return false;
	}
}

/**
 * 提交修改
 */
SpecCatsInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var catName = $("#catName").val();
    var goods1 = $("#goods1").val();

    if(catName == null || catName ==""){
        var html='<span class="error">规格名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#catName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(goods1 == null || goods1 ==""){
        var html='<span class="error">所属分类不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#goods1').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    let isShow = $('input:radio[name=isShow]:checked').val();
    this.specCatsInfoData.isShow = isShow ;
    let isAllowImg = $('input:radio[name=isAllowImg]:checked').val();
    this.specCatsInfoData.isShow = isAllowImg ;
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/specCats/update", function(data){
        Feng.success("修改成功!");
        window.parent.SpecCats.table.refresh();
        SpecCatsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.specCatsInfoData);
    ajax.start();
}else{
    Feng.success("请核实添加字段!");
}
}

$(function() {
	if($('#goodsCatId3').val()!='0'){
		$('#goods1').val($('#goodsCatId3').val());
		if($('#goods1').val()!=0){
			$('#goods1').val($('#goodsCatId3').val());
			$('#goods1').trigger("change");
		}
		if($('#goodsCatId2').val()!=0){
			$('#goods2').val($('#goodsCatId2').val());
			$('#goods2').trigger("change");
		}
		if($('#goodsCatId1').val()!=0){
			$('#goods3').val($('#goodsCatId1').val());
			$('#goods3').trigger("change");
		}
	}else{
		if($('#goodsCatId2').val()!='0'){
			$('#goods1').val($('#goodsCatId2').val());
			if($('#goods1').val()!=0){
				$('#goods1').val($('#goodsCatId2').val());
				$('#goods1').trigger("change");
			}
			if($('#goodsCatId3').val()!=0){
				$('#goods2').val($('#goodsCatId3').val());
				$('#goods2').trigger("change");
			}
		}else{
			$('#goods1').val($('#goodsCatId1').val());
			if($('#goods1').val()!=0){
				$('#goods1').val($('#goodsCatId1').val());
				$('#goods1').trigger("change");
			}
		}
		
	}
});
