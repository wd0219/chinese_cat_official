var proData;
$.ajax({

    type:"get",

    url:"/goodsCats/selectByParentId?id=0", //必选。规定吧请求发送到那个URL。

    data:null,//可选。映射或字符串值。规定连同请求发送到服务器的数据。
    async:false,
    success:function(result){
    	proData = result;
  	 
    }, //可选，请求成功时执行的回调函数。

    datatype:"json"//可选。规定预期的服务器响应的数据类型。默认执行职能判断(xml,json,script 或html)

  });

 cityData = [],
 distData = [];
 var proSelect = document.getElementById("goods1"),
 citySelect = document.getElementById("goods2"),
 districtSelect = document.getElementById("goods3");
 var curPro = "",
 curCity = "";

 $(function() {
	  //load city.json
	 
		 
	  var sb =[];
	  $.each(proData,
	    function(i, val) {
	     
	        sb.push("<option value='" + val.catId + "'>" + val.catName + "</option>");
	      
	    });
	  $("#goodscats1").after(sb.join(""));
 });
 //一级变化时处理二级
 $("#goods1").change(function(){
 //function dogoodscat1() {
	 var goods2Data;
	 var goodscats1=$("#goods1").val();
	  var city = $("#goods2");
	  var county = $("#goods3");
	  if (city.children().length > 1) {
			 
		    city.empty();
		  }
		  if (county.children().length > 1) {
			  
		    county.empty();
		  }
		  if ($("#goodscats2").length === 0) {
		    city.append("<option id='goodscats2' value='-1'>请选择</option>");
		  }
		  if ($("#goodscats3").length === 0) {
		    county.append("<option id='goodscats3' value='-1'>请选择</option>");
		  }
	  var sb = [];
	  $.ajax({

		    type:"get",

		    url:"/goodsCats/selectByParentId?id="+goodscats1, //必选。规定吧请求发送到那个URL。

		    data:null,//可选。映射或字符串值。规定连同请求发送到服务器的数据。
		    async:false,
		    success:function(result){
		    	goods2Data = result;
		  	 
		    }, //可选，请求成功时执行的回调函数。

		    datatype:"json"//可选。规定预期的服务器响应的数据类型。默认执行职能判断(xml,json,script 或html)

		  });
	  $.each(goods2Data,
	    function(i, val) {
	     
	        sb.push("<option value='" + val.catId + "'>" + val.catName + "</option>");
	      
	    });
	  $("#goodscats2").after(sb.join(""));
	});
 //二级变化时处理三级
 $("#goods2").change(function(){
// function dogoodscat2() {
	 var goods2Data;
	 var goodscats1=$("#goods2").val();
	 
	
	 var county = $("#goods3");
	  if (county.children().length > 1) {
	    county.empty();
	  }
	  if ($("#goodscats3").length === 0) {
	    county.append("<option id='goodscats3' value='-1'>请选择</option>");
	  }
	  var sb =[];
	  $.ajax({

		    type:"get",

		    url:"/goodsCats/selectByParentId?id="+goodscats1, //必选。规定吧请求发送到那个URL。

		    data:null,//可选。映射或字符串值。规定连同请求发送到服务器的数据。
		    async:false,
		    success:function(result){
		    	goods2Data = result;
		  	 
		    }, //可选，请求成功时执行的回调函数。

		    datatype:"json"//可选。规定预期的服务器响应的数据类型。默认执行职能判断(xml,json,script 或html)

		  });
	  $.each(goods2Data,
	    function(i, val) {
	     
	        sb.push("<option value='" + val.catId + "'>" + val.catName + "</option>");
	      
	    });
	  $("#goodscats3").after(sb.join(""));
	});
//三级变化时处理商品
 $("#goods3").change(function(){
// function dogoodscat2() {
	 var goods2Data;
	 var goodscats1=$("#goods3").val();
	 
	
	 var county = $("#goods");
	  if (county.children().length > 1) {
	    county.empty();
	  }
	  if ($("#goodscats4").length === 0) {
	    county.append("<option id='goodscats4' value='-1'>请选择</option>");
	  }
	  var sb =[];
	  $.ajax({

		    type:"get",

		    url:"/goods/selectGoodsByGoodsCatId?id="+goodscats1, //必选。规定吧请求发送到那个URL。

		    data:null,//可选。映射或字符串值。规定连同请求发送到服务器的数据。
		    async:false,
		    success:function(result){
		    	goods2Data = result;
		  	 
		    }, //可选，请求成功时执行的回调函数。

		    datatype:"json"//可选。规定预期的服务器响应的数据类型。默认执行职能判断(xml,json,script 或html)

		  });
	  $.each(goods2Data,
	    function(i, val) {
	     
	        sb.push("<option value='" + val.goodsId + "'>" + val.goodsName + "</option>");
	      
	    });
	  $("#goodscats4").after(sb.join(""));
	});
