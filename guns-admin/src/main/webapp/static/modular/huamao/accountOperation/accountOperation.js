/**
 * 手动补单记录管理初始化
 */
var AccountOperation = {
    id: "AccountOperationTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AccountOperation.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户手机', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '登录名', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '订单编号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '操作对象', field: 'object', visible: true, align: 'center', valign: 'middle',formatter:AccountOperation.object},
            {title: '变化数量', field: 'nump', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            {title: '时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 操作对象 1积分 2现金 3华宝 4库存积分
 */
AccountOperation.object = function (value,row,index) {
	if(value == 1){
		return "积分";
	}else if(value == 2){
		return "现金";
	}else if(value == 3){
		return "华宝";
	}else if(value == 4){
		return "库存积分";
	}
}
/**
 * 检查是否选中
 */
AccountOperation.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AccountOperation.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加手动补单记录
 */
AccountOperation.openAddAccountOperation = function () {
    var index = layer.open({
        type: 2,
        title: '添加手动补单记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accountOperation/accountOperation_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看手动补单记录详情
 */
AccountOperation.openAccountOperationDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '手动补单记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accountOperation/accountOperation_update/' + AccountOperation.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除手动补单记录
 */
AccountOperation.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/accountOperation/delete", function (data) {
            Feng.success("删除成功!");
            AccountOperation.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("accountOperationId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询手动补单记录列表
 */
AccountOperation.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['loginName'] = $("#loginName").val();
    queryData['orderNo'] = $("#orderNo").val();
    queryData['object'] = $("#object").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    AccountOperation.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AccountOperation.initColumn();
    var table = new BSTable(AccountOperation.id, "/accountOperation/list", defaultColunms);
    table.setPaginationType("server");
    AccountOperation.table = table.init();
});
//导出Excel
AccountOperation.export= function(){
	$('#'+AccountOperation.id).tableExport({
		fileName:'手动补单记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}