/**
 * 初始化手动补单记录详情对话框
 */
var AccountOperationInfoDlg = {
    accountOperationInfoData : {}
};

/**
 * 清除数据
 */
AccountOperationInfoDlg.clearData = function() {
    this.accountOperationInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccountOperationInfoDlg.set = function(key, val) {
    this.accountOperationInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccountOperationInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AccountOperationInfoDlg.close = function() {
    parent.layer.close(window.parent.AccountOperation.layerIndex);
}

/**
 * 收集数据
 */
AccountOperationInfoDlg.collectData = function() {
    this
    .set('id')
    .set('orderNo')
    .set('userId')
    .set('object')
    .set('type')
    .set('num')
    .set('remark')
    .set('dataFlag')
    .set('staffId')
    .set('createTime');
}

/**
 * 提交添加
 */
AccountOperationInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accountOperation/add", function(data){
        Feng.success("添加成功!");
        window.parent.AccountOperation.table.refresh();
        AccountOperationInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accountOperationInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AccountOperationInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accountOperation/update", function(data){
        Feng.success("修改成功!");
        window.parent.AccountOperation.table.refresh();
        AccountOperationInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accountOperationInfoData);
    ajax.start();
}

$(function() {

});
