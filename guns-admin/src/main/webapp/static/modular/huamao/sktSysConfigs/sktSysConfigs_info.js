/**
 * 初始化基础设置详情对话框
 */
var SktSysConfigsInfoDlg = {
    sktSysConfigsInfoData : {}
};

/**
 * 清除数据
 */
SktSysConfigsInfoDlg.clearData = function() {
    this.sktSysConfigsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktSysConfigsInfoDlg.set = function(key, val) {
    this.sktSysConfigsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktSysConfigsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktSysConfigsInfoDlg.close = function() {
    parent.layer.close(window.parent.SktSysConfigs.layerIndex);
}

/**
 * 收集数据
 */
SktSysConfigsInfoDlg.collectData = function() {
    this
    .set('configId')
    .set('fieldName')
    .set('fieldCode')
    .set('fieldValue')
    .set('type')
    .set('page')
    .set('value');
}

/**
 * 提交添加
 */
SktSysConfigsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktSysConfigs.table.refresh();
        SktSysConfigsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktSysConfigsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktSysConfigsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktSysConfigs.table.refresh();
        SktSysConfigsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktSysConfigsInfoData);
    ajax.start();
}

$(function() {

});
