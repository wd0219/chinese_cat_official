/**
 * 基础设置管理初始化
 */
var SktSysConfigs = {
    id: "SktSysConfigsTable",	// 表格id
    seItem: null,		// 选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktSysConfigs.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '自增ID', field: 'configId', visible: true, align: 'center', valign: 'middle'},
            {title: '字段名称', field: 'fieldName', visible: true, align: 'center', valign: 'middle'},
            {title: '字段代码', field: 'fieldCode', visible: true, align: 'center', valign: 'middle'},
            {title: '字段值', field: 'fieldValue', visible: true, align: 'center', valign: 'middle'},
            {title: '类型1文本框 2单选 3 富文本 4 特殊下拉', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '页码', field: 'page', visible: true, align: 'center', valign: 'middle'},
            {title: '选项值 以“,”间隔', field: 'value', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktSysConfigs.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktSysConfigs.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加基础设置
 */
SktSysConfigs.openAddSktSysConfigs = function () {
    var index = layer.open({
        type: 2,
        title: '添加基础设置',
        area: ['800px', '420px'], // 宽高
        fix: false, // 不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktSysConfigs/sktSysConfigs_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看基础设置详情
 */
SktSysConfigs.openSktSysConfigsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '基础设置详情',
            area: ['800px', '420px'], // 宽高
            fix: false, // 不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktSysConfigs/sktSysConfigs_update/' + SktSysConfigs.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除基础设置
 */
SktSysConfigs.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/delete", function (data) {
            Feng.success("删除成功!");
            SktSysConfigs.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktSysConfigsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询基础设置列表
 */
SktSysConfigs.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktSysConfigs.table.refresh({query: queryData});
};

/**
 * 提交添加
 */
SktSysConfigs.editSubmit = function() {
	var newData = [];
	$('#content div').each(function(i,val){
		var obj = new Object();
		obj.configId = $(this).attr('id');
		var val = $(this).find("input");
		if(val.length<2){
			obj.fieldValue = $(val).val();
		}else{
			obj.fieldValue = $(val[0]).is(':checked')?'1':'0';
		}
		newData.push(obj);
	});
	
	$.ajax({
		url: Feng.ctxPath + "/sktSysConfigs/updateJc",
		type: "POST",
		contentType : 'application/json;charset=utf-8', // 设置请求头信息
		dataType:"json",
		data: JSON.stringify(newData),
		success: function(data){
			Feng.success("保存成功!");
			SktSysConfigs.loadData();
		},
		error: function(res){
			Feng.error("保存失败!" + data.responseJSON.message + "!");
		}
	});
}
// 需要重置数据
var oldData= "";
/**
 * 重置
 */
SktSysConfigs.reset = function(){
	$('#content').html("");
	SktSysConfigs.showData(oldData);
	SktSysConfigs.showColor();
}


$(function () {
	SktSysConfigs.loadData();
// var defaultColunms = SktSysConfigs.initColumn();
// var table = new BSTable(SktSysConfigs.id, "/sktSysConfigs/list",
// defaultColunms);
// table.setPaginationType("client");
// SktSysConfigs.table = table.init();
});
//加载数据
SktSysConfigs.loadData = function(){
	 var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/list", function (data) {
		 oldData = data;
		 SktSysConfigs.showData(data);
		 SktSysConfigs.showColor();
     }, function (data) {
         Feng.error("删除失败!" + data.responseJSON.message + "!");
     });
     ajax.set("page",$('#page').val());
     ajax.start();
}

/**
 * 改变颜色
 */
SktSysConfigs.showColor = function(){
	$('#watermarkColor').colpick({
		layout:'hex',
		submit:0,
		colorScheme:'dark',
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css('border-color','#'+hex);
			if(!bySetColor) $(el).val(hex);
		}
	}).keyup(function(){
		$(this).colpickSetColor(this.value);
	});
}

/*
 * 显示数据
 */
SktSysConfigs.showData = function(data){
	// alert(data);
	if(data){
		var html = [];
		$(data).each(function(i,val){
			switch(val.fieldCode){
			case 'shopLogo':
			case 'userLogo':
			case 'shopBanner':
			case 'mallLogo':
			case 'goodsLogo':
			case 'watermarkFile':
				html.push("<div>");
				html.push("<span>"+val.fieldName+"</span>");
                const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
                var photo = val.fieldValue;
                if(photo.indexOf('http')==-1){
                    photo=imgUrl+photo
                }else{
                    photo=photo
                }
				html.push("<img src='"+photo+"' id='showPic' style='width:60px;height:60px;'>");
				html.push("<input type='file' id='"+val.fieldCode+"' onchange='SktSysConfigs.showPic(this)'>")
				html.push("</div>");
				break;
			default:
				html.push("<div id='"+val.configId+"'><span>"+val.fieldName+"</span><input type='text' id="+val.fieldCode+" name='"+val.fieldName+"' value="+(typeof val.fieldValue !='undefined'?val.fieldValue:'')+"></div>");
			}
		});
		$('#content').html(html.join(''));
	}
}
/**
 * 上传图片
 */
SktSysConfigs.showPic = function(picId){
	var formData = new FormData();
	formData.append('file',$('#'+picId.id)[0].files[0]);
	formData.append('servicerid',$('#'+picId.id).val());
    $.ajax({
      type : "POST",
      url : Feng.ctxPath + "/messages/aliyunUpload",
      data: formData,
      dataType: 'JSON',
      cache: false,
      processData: false,
      contentType: false,
      success : function(data) {
          SktSysConfigs.updateData(picId,data.data);
    	  SktSysConfigs.loadData();
      },
      error:function(){
          alert("上传失败");
      }
    });

}

SktSysConfigs.updateData = function(picId,imgs){
    console.log(imgs[0]);
    console.log(picId.id);
    var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/upload/"+picId.id, function (data) {
        console.log(data);
    }, function (data) {
        Feng.error("删除失败!" + data.responseJSON.message + "!");
    });
    ajax.set("imgs",imgs[0]);
    ajax.start();
}
