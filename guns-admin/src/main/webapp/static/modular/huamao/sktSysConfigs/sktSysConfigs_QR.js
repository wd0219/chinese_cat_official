/**
 * 基础设置管理初始化
 */
var SktSysConfigs = {
    id: "SktSysConfigsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktSysConfigs.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '字段名称', field: 'fieldName', visible: true, align: 'center', valign: 'middle'},
            {title: '字段代码', field: 'fieldCode', visible: true, align: 'center', valign: 'middle'},
            {title: '字段值', field: 'fieldValue', visible: true, align: 'center', valign: 'middle'},
            {title: '类型1文本框 2单选 3 富文本 4 特殊下拉', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '页码', field: 'page', visible: true, align: 'center', valign: 'middle'},
            {title: '选项值 以“,”间隔', field: 'value', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktSysConfigs.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktSysConfigs.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加基础设置
 */
SktSysConfigs.openAddSktSysConfigs = function () {
    var index = layer.open({
        type: 2,
        title: '添加基础设置',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktSysConfigs/sktSysConfigs_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看基础设置详情
 */
SktSysConfigs.openSktSysConfigsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '基础设置详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktSysConfigs/sktSysConfigs_update/' + SktSysConfigs.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除基础设置
 */
SktSysConfigs.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/delete", function (data) {
            Feng.success("删除成功!");
            SktSysConfigs.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktSysConfigsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询基础设置列表
 */
SktSysConfigs.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktSysConfigs.table.refresh({query: queryData});
};

/**
 * 提交添加
 */
SktSysConfigs.editSubmit = function() {
	debugger;
	var newData = [];
    var obj = new Object();
    obj.fieldValue = $('#alipayQR').val();
    obj.configId=$('#alipayQRId').val();
    newData.push(obj);
	$.ajax({
		url: Feng.ctxPath + "/sktSysConfigs/updateJc",
		type: "POST",
		contentType : 'application/json;charset=utf-8', //设置请求头信息
		dataType:"json",
		data: JSON.stringify(newData),
		success: function(data){
			Feng.success("保存成功!");
			SktSysConfigs.loadData();
		},
		error: function(res){
			Feng.error("保存失败!" + data.responseJSON.message + "!");
		}
	});
}/**
 * 提交添加
 */
SktSysConfigs.editSubmit2 = function() {
	debugger;
	var newData = [];
    var obj = new Object();
    obj.fieldValue = $('#weChatQR').val();
    obj.configId=$('#weChatQRId').val();
    newData.push(obj);
	$.ajax({
		url: Feng.ctxPath + "/sktSysConfigs/updateJc",
		type: "POST",
		contentType : 'application/json;charset=utf-8', //设置请求头信息
		dataType:"json",
		data: JSON.stringify(newData),
		success: function(data){
			Feng.success("保存成功!");
			SktSysConfigs.loadData();
		},
		error: function(res){
			Feng.error("保存失败!" + data.responseJSON.message + "!");
		}
	});
}
//需要重置数据
var oldData= "";

SktSysConfigs.reset = function(){
	$('#content').html("");
	SktSysConfigs.showData(oldData);
}

//加载数据
// SktSysConfigs.loadData = function(){
// 	 var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/list", function (data) {
// 		 oldData = data;
// 		 SktSysConfigs.showData(data);
//      }, function (data) {
//          Feng.error("删除失败!" + data.responseJSON.message + "!");
//      });
//      ajax.set("page",$('#page').val());
//      ajax.start();
// }
$(function () {
	// SktSysConfigs.loadData();
    for(let e of document.getElementsByClassName('upload-btn')){
        let id = $(e).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
        uploader = new $WebUpload(id);
        uploader.setUploadBarId(id);
        uploader.init();
    }
})



