/**
 * 基础设置管理初始化
 */
var SktSysConfigs = {
    id: "SktSysConfigsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktSysConfigs.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '自增ID', field: 'configId', visible: true, align: 'center', valign: 'middle'},
            {title: '字段名称', field: 'fieldName', visible: true, align: 'center', valign: 'middle'},
            {title: '字段代码', field: 'fieldCode', visible: true, align: 'center', valign: 'middle'},
            {title: '字段值', field: 'fieldValue', visible: true, align: 'center', valign: 'middle'},
            {title: '类型1文本框 2单选 3 富文本 4 特殊下拉', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '页码', field: 'page', visible: true, align: 'center', valign: 'middle'},
            {title: '选项值 以“,”间隔', field: 'value', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktSysConfigs.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktSysConfigs.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加基础设置
 */
SktSysConfigs.openAddSktSysConfigs = function () {
    var index = layer.open({
        type: 2,
        title: '添加基础设置',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktSysConfigs/sktSysConfigs_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看基础设置详情
 */
SktSysConfigs.openSktSysConfigsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '基础设置详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktSysConfigs/sktSysConfigs_update/' + SktSysConfigs.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除基础设置
 */
SktSysConfigs.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/delete", function (data) {
            Feng.success("删除成功!");
            SktSysConfigs.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktSysConfigsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询基础设置列表
 */
SktSysConfigs.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktSysConfigs.table.refresh({query: queryData});
};

/**
 * 提交添加
 */
SktSysConfigs.editSubmit = function() {
	var newData = [];
	$('#content div').each(function(i,val){
		var obj = new Object();
		obj.configId = $(this).attr('id');
		var val = $(this).find("input");
		if(val.length<2){
			obj.fieldValue = $(val).val();
		}else{
			obj.fieldValue = $(val[0]).is(':checked')?'1':'0';
		}
		newData.push(obj);
	});
	
	$.ajax({
		url: Feng.ctxPath + "/sktSysConfigs/updateJc",
		type: "POST",
		contentType : 'application/json;charset=utf-8', //设置请求头信息
		dataType:"json",
		data: JSON.stringify(newData),
		success: function(data){
			Feng.success("保存成功!");
			SktSysConfigs.loadData();
		},
		error: function(res){
			Feng.error("保存失败!" + data.responseJSON.message + "!");
		}
	});
}
//需要重置数据
var oldData= "";

SktSysConfigs.reset = function(){
	$('#content').html("");
	SktSysConfigs.showData(oldData);
}

//加载数据
SktSysConfigs.loadData = function(){
	 var ajax = new $ax(Feng.ctxPath + "/sktSysConfigs/list", function (data) {
		 oldData = data;
		 SktSysConfigs.showData(data);
     }, function (data) {
         Feng.error("删除失败!" + data.responseJSON.message + "!");
     });
     ajax.set("page",$('#page').val());
     ajax.start();
}
$(function () {
	SktSysConfigs.loadData();
//    var defaultColunms = SktSysConfigs.initColumn();
//    var table = new BSTable(SktSysConfigs.id, "/sktSysConfigs/list", defaultColunms);
//    table.setPaginationType("client");
//    SktSysConfigs.table = table.init();
});

SktSysConfigs.showData = function(data){
	//alert(data);
	if(data){
		var html = [];
		$(data).each(function(i,val){
			if(val.fieldCode=='mailAuth'){
				html.push("<div id='"+val.configId+"'><span>"+val.fieldName+"</span>");
				if(val.fieldValue == '1'){
					html.push("<input type='radio' name='"+val.fieldName+"' checked='checked'>是");
					html.push("<input type='radio' name='"+val.fieldName+"' >否");
				}else{
					html.push("<input type='radio' name='"+val.fieldName+"' >是");
					html.push("<input type='radio' name='"+val.fieldName+"' checked='checked'>否");
				}
				html.push("</div>");
			} else if(val.fieldCode=='upgradeProfitFlag'){
				html.push("<div id='"+val.configId+"'><span>"+val.fieldName+"</span>");
				if(val.fieldValue == '1'){
					html.push("<input type='radio' name='"+val.fieldName+"' checked='checked'>是");
					html.push("<input type='radio' name='"+val.fieldName+"' >否");
				}else{
					html.push("<input type='radio' name='"+val.fieldName+"' >是");
					html.push("<input type='radio' name='"+val.fieldName+"' checked='checked'>否");
				}
				html.push("</div>");
			}else{
				html.push("<div id='"+val.configId+"'><span>"+val.fieldName+"</span><input type='text' id="+val.fieldCode+" name='"+val.fieldName+"' value="+(typeof val.fieldValue !='undefined'?val.fieldValue:'')+"></div>");
			}
		});
		$('#content').html(html.join(''));
	}
}

