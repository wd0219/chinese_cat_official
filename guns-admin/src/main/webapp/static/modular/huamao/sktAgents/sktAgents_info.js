/**
 * 初始化代理股东管理详情对话框
 */
var SktAgentsInfoDlg = {
    sktAgentsInfoData : {}
};

/**
 * 清除数据
 */
SktAgentsInfoDlg.clearData = function() {
    this.sktAgentsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAgentsInfoDlg.set = function(key, val) {
    this.sktAgentsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAgentsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktAgentsInfoDlg.close = function() {
    parent.layer.close(window.parent.SktAgents.layerIndex);
}

/**
 * 收集数据
 */
SktAgentsInfoDlg.collectData = function() {
    this
    .set('agentId')
    .set('agentName')
    .set('alevel')
    .set('provinceId')
    .set('cityId')
    .set('areaId')
    .set('agencyFee')
    .set('stockNum')
    .set('totalNum')
    .set('storeNum')
    .set('totalStoreNum')
    .set('totalStockNum')
    .set('LPsurplusStockNum')
    .set('GPsurplusStockNum')
    .set('oneRatio')
    .set('twoRatio')
    .set('threeRatio')
    .set('phone')
    .set('loginName')
    .set('loginSecret')
    .set('loginPwd')
    .set('payPwd')
    .set('agentStatus')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
SktAgentsInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var agentName = $("#agentName").val();
    var totalNum = $("#totalNum").val();
    
    if(agentName == null || agentName ==""){
        var html='<span class="error">代理名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#agentName').after(html);
        falg = 1;
    }else { 
        var html = '';
    }

    if(!isRealNum(totalNum)){
        var html='<span class="error">代理公司总人数必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#totalNum').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
	let ss = SktAgentsInfoDlg.yanzheng();
	if(ss){
		return ;
	}
    var alevel = $('#alevel').val();
    var province = $('#province').val();
    var citys = $('#citys').val();
    var county = $('#county').val();
    
    if(alevel == 10){
    	if(province == ''){
    		Feng.success("请选择省!");
        	return;
    	}
    	this.sktAgentsInfoData.provinceId = province;
    	this.sktAgentsInfoData.cityId = 0;
    	this.sktAgentsInfoData.areaId = 0;
    }else if(alevel == 11){
    	if(province == '' || citys == ""){
    		Feng.success("请选择省,市!");
        	return;
    	}
    	this.sktAgentsInfoData.provinceId = province;
    	this.sktAgentsInfoData.cityId = citys;
    	this.sktAgentsInfoData.areaId = 0;
    }else if(alevel == 12){
    	if(province == '' || citys == "" || county == ''){
    		Feng.success("请选择省,市,县!");
        	return;
    	}
    	this.sktAgentsInfoData.provinceId = province;
    	this.sktAgentsInfoData.cityId = citys;
    	this.sktAgentsInfoData.areaId = county;
    }else {
    	Feng.success("请选择代理级别!");
    	return;
    }
    
    let agentStatus = $("input:radio[name=agentStatus]:checked").val();
    this.sktAgentsInfoData.agentStatus = agentStatus;
    
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktAgents/add", function(data){
    	if(data=='REAPT'){
    		Feng.success("添加失败,已经有该代理!");
    	}else{
    		 Feng.success("添加成功!");
    	}
        window.parent.SktAgents.table.refresh();
        SktAgentsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAgentsInfoData);
    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
	}
	
	function isRealNum(val){
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	if(val === "" || val ==null){
	    return false;
	}
	if(!isNaN(val)){
	    return true;
	}else{
	    return false;
	}
}
/**
 * 提交修改
 */
SktAgentsInfoDlg.editSubmit = function() {
	
    this.clearData();
    this.collectData();
    
    let ss = SktAgentsInfoDlg.yanzheng();
	if(ss){
		return ;
	}
    let agentStatus = $("input:radio[name=agentStatus]:checked").val();
    this.sktAgentsInfoData.agentStatus = agentStatus;
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktAgents/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktAgents.table.refresh();
        SktAgentsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAgentsInfoData);
    ajax.start();
	}else{
    Feng.success("请核实添加字段!");
	}
}


$(function() {
	if($('#agentId').val()){
		$('#alevel').val($('#alevel2').val());
		if($('#provinceId').val()!=0){
			$('#province').val($('#provinceId').val());
			$('#province').trigger("change");
		}
		if($('#cityId').val()!=0){
			$('#citys').val($('#cityId').val());
			$('#citys').trigger("change");
		}
		if($('#areaId').val()!=0){
			$('#county').val($('#areaId').val());
			$('#county').trigger("change");
		}
	}
	$('#agentStatus').val()=='1'? $('#isAgentStatus').attr('checked', true):$('#isNotAgentStatus').attr('checked', true);
});

SktAgentsInfoDlg.yanzheng = function(){
	if($('#agentName').val() == '' || $('#agentName').val() == 'undefined'){
		Feng.success("用户名不能为空");
		return 1;
	}
	if($('#alevel').val() == '' || $('#alevel').val() == 'undefined'){
		Feng.success("代理级别不能为空");
		return 1;
	}
	if($('#agencyFee').val() == '' || $('#agencyFee').val() == 'undefined'){
		Feng.success("每股缴纳的代理费不能为空");
		return 1;
	}
	if($('#totalNum').val() == '' || $('#totalNum').val() == 'undefined'){
		Feng.success("代理公司股东总人数不能为空");
		return 1;
	}
	if($('#totalStoreNum').val() == '' || $('#totalStoreNum').val() == 'undefined'){
		Feng.success("完成目标辖区商家数量不能为空");
		return 1;
	}
	if($('#oneRatio').val() == '' || $('#oneRatio').val() == 'undefined'){
		Feng.success("成为股东的分红系数不能为空");
		return 1;
	}
	if($('#twoRatio').val() == '' || $('#twoRatio').val() == 'undefined'){
		Feng.success("完成70%(落地)的分红系数不能为空");
		return 1;
	}
	if($('#threeRatio').val() == '' || $('#threeRatio').val() == 'undefined'){
		Feng.success("全部完成的分红系数不能为空");
		return 1;
	}
	if($('#loginPwd').val() == '' || $('#loginPwd').val() == 'undefined'){
		Feng.success("密码不能为空");
		return 1;
	}
	
	if(parseInt($('#oneRatio').val())>= 1 || 
		parseInt($('#twoRatio').val()) >= 1 || 
		parseInt($('#threeRatio').val()) >= 1 ||
		parseFloat($('#oneRatio').val()) > parseFloat($('#twoRatio').val()) ||
		parseFloat($('#twoRatio').val()) > parseFloat($('#threeRatio').val())
	){
		Feng.success("请设置合理的分红系数！");
		return 1;
	}
}
