/**
 * 代理股东管理管理初始化
 */
var SktAgents = {
    id: "SktAgentsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktAgents.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '代理名称', field: 'agentName', visible: true, align: 'center', valign: 'middle'},
            {title: '代理级别', field: 'alevel', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	let result = "";
            	switch(value){
            	case 10:
            		result = "省级";
            		break;
            	case 11:
            		result = "市级";
            		break;
            	case 12:
            		result = "区县";
            		break;
            	}
            	return result;
            }},
            {title: '代理地址', field: 'areas', visible: true, align: 'center', valign: 'middle'},
            {title: '每股缴纳的代理费', field: 'agencyFee', visible: true, align: 'center', valign: 'middle'},
            {title: '公司总股份数', field: 'totalStockNum', visible: true, align: 'center', valign: 'middle'},
            {title: '股东总人数', field: 'totalNum', visible: true, align: 'center', valign: 'middle'},
            {title: '任务辖区商家数量', field: 'totalStoreNum', visible: true, align: 'center', valign: 'middle'},
            {title: '股东完成度', field: 'gdNum', visible: true, align: 'center', valign: 'middle'},
            {title: '辖区商家完成度', field: 'gxNum', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号码', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '登录名', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '账号状态', field: 'agentStatus', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	if(value==1){
            		return "启用"
            	}else{
            		return "停用"
            	}
            	
            }},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'op', visible: true,width:150, align: 'center', valign: 'middle',formatter:SktAgents.op}
    ];
};
/**
 * 操作
 */
SktAgents.op = function(){
	return "<a href='javascript:void(0)' onclick=\"SktAgents.openSktAgentsDetail()\">修改</a>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"SktAgents.openStockholderDetail()\">股东详情</a>"
}

/**
 * 检查是否选中
 */
SktAgents.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktAgents.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理股东管理
 */
SktAgents.openAddSktAgents = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理股东管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/sktAgents/sktAgents_add'
    });
    this.layerIndex = index;
};
/**
 * 跳转股东详情
 */
SktAgents.openStockholderDetail = function(){
	if (this.check()) {
		window.location.href = Feng.ctxPath + '/sktAgentsStockholder?agentId=' +this.seItem.agentId
	}
}


/**
 * 打开查看代理股东管理详情
 */
SktAgents.openSktAgentsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理股东管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/sktAgents/sktAgents_update/' + SktAgents.seItem.agentId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代理股东管理
 */
SktAgents.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktAgents/delete", function (data) {
            Feng.success("删除成功!");
            SktAgents.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktAgentsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代理股东管理列表
 */
SktAgents.search = function () {
    var queryData = {};
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['alevel'] = $("#alevel").val();
    queryData['agentStatus'] = $("#agentStatus").val();
    queryData['agentId'] = '';
    SktAgents.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktAgents.initColumn();
    var table = new BSTable(SktAgents.id, "/sktAgents/list", defaultColunms);
    table.setPaginationType("server");
    var obj = {};
    obj['agentId'] = $('#agentId').val();
    table.setQueryParams(obj);
    SktAgents.table = table.init();
});
