/**
 * 代付明细管理初始化
 */
var LogDaifu = {
    id: "LogDaifuTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogDaifu.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '日期', field: 'date', visible: true, align: 'center', valign: 'middle'},
            {title: '批次', field: 'batch', visible: true, align: 'center', valign: 'middle'},
            //{title: '代付公司id', field: 'daifuId', visible: true, align: 'center', valign: 'middle'},
            {title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogDaifu.type},
            {title: '提现内部业务单号', field: 'drawNo', visible: true, align: 'center', valign: 'middle'},
            {title: '提现第三方流水号', field: 'outTradeNo', visible: true, align: 'center', valign: 'middle'},
            {title: '请求数据', field: 'request', visible: true, align: 'center', valign: 'middle'},
            {title: '响应数据', field: 'response', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle',formatter:LogDaifu.status},
            {title: '备注', field: 'remarks', visible: true, align: 'center', valign: 'middle'},
            //{title: '响应时间', field: 'responseTime', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 类型对应：1用户>2代理公司
 */
LogDaifu.type = function(value,row,index){
	if(value == 1){
		return "用户";
	}else if(value == 2){
		return "代理公司";
	}
}
/**
 * 状态对应：-2受理失败>1请求前>2受理中>3受理成功
 */
LogDaifu.status = function(value,row,index){
	if(value == -2){
		return "受理失败";
	}else if(value == 1){
		return "请求前";
	}else if(value == 2){
		return "受理中";
	}else if(value == 3){
		return "受理成功";
	}
}
/**
 * 检查是否选中
 */
LogDaifu.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogDaifu.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代付明细
 */
LogDaifu.openAddLogDaifu = function () {
    var index = layer.open({
        type: 2,
        title: '添加代付明细',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logDaifu/logDaifu_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代付明细详情
 */
LogDaifu.openLogDaifuDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代付明细详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logDaifu/logDaifu_update/' + LogDaifu.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代付明细
 */
LogDaifu.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logDaifu/delete", function (data) {
            Feng.success("删除成功!");
            LogDaifu.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logDaifuId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代付明细列表
 */
LogDaifu.search = function () {
    var queryData = {};
    queryData['batch'] = $("#batch").val();
    queryData['type'] = $("#type").val();
    queryData['drawNo'] = $("#drawNo").val();
    queryData['outTradeNo'] = $("#outTradeNo").val();
    queryData['status'] = $("#status").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogDaifu.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogDaifu.initColumn();
    var table = new BSTable(LogDaifu.id, "/logDaifu/list", defaultColunms);
    table.setPaginationType("server");
    LogDaifu.table = table.init();
});
//导出Excel
LogDaifu.export= function(){
	$('#'+LogDaifu.id).tableExport({
		fileName:'代付明细记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}