/**
 * 初始化代付明细详情对话框
 */
var LogDaifuInfoDlg = {
    logDaifuInfoData : {}
};

/**
 * 清除数据
 */
LogDaifuInfoDlg.clearData = function() {
    this.logDaifuInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogDaifuInfoDlg.set = function(key, val) {
    this.logDaifuInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogDaifuInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogDaifuInfoDlg.close = function() {
    parent.layer.close(window.parent.LogDaifu.layerIndex);
}

/**
 * 收集数据
 */
LogDaifuInfoDlg.collectData = function() {
    this
    .set('id')
    .set('date')
    .set('batch')
    .set('daifuId')
    .set('type')
    .set('drawNo')
    .set('outTradeNo')
    .set('request')
    .set('response')
    .set('status')
    .set('remarks')
    .set('responseTime')
    .set('createTime');
}

/**
 * 提交添加
 */
LogDaifuInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logDaifu/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogDaifu.table.refresh();
        LogDaifuInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logDaifuInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogDaifuInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logDaifu/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogDaifu.table.refresh();
        LogDaifuInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logDaifuInfoData);
    ajax.start();
}

$(function() {

});
