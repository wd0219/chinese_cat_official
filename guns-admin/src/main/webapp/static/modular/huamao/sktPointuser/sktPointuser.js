/**
 * 视屏点赞用户表管理初始化
 */
var SktPointuser = {
    id: "SktPointuserTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktPointuser.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '点赞视频Id', field: 'classId', visible: true, align: 'center', valign: 'middle'},
            {title: '点赞用户Id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '点赞时间', field: 'creatTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktPointuser.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktPointuser.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加视屏点赞用户表
 */
SktPointuser.openAddSktPointuser = function () {
    var index = layer.open({
        type: 2,
        title: '添加视屏点赞用户表',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktPointuser/sktPointuser_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看视屏点赞用户表详情
 */
SktPointuser.openSktPointuserDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '视屏点赞用户表详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktPointuser/sktPointuser_update/' + SktPointuser.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除视屏点赞用户表
 */
SktPointuser.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktPointuser/delete", function (data) {
            Feng.success("删除成功!");
            SktPointuser.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktPointuserId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询视屏点赞用户表列表
 */
SktPointuser.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktPointuser.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktPointuser.initColumn();
    var table = new BSTable(SktPointuser.id, "/sktPointuser/list", defaultColunms);
    table.setPaginationType("client");
    SktPointuser.table = table.init();
});
