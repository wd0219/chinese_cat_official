/**
 * 初始化视屏点赞用户表详情对话框
 */
var SktPointuserInfoDlg = {
    sktPointuserInfoData : {}
};

/**
 * 清除数据
 */
SktPointuserInfoDlg.clearData = function() {
    this.sktPointuserInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktPointuserInfoDlg.set = function(key, val) {
    this.sktPointuserInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktPointuserInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktPointuserInfoDlg.close = function() {
    parent.layer.close(window.parent.SktPointuser.layerIndex);
}

/**
 * 收集数据
 */
SktPointuserInfoDlg.collectData = function() {
    this
    .set('id')
    .set('classId')
    .set('userId')
    .set('creatTime');
}

/**
 * 提交添加
 */
SktPointuserInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktPointuser/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktPointuser.table.refresh();
        SktPointuserInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktPointuserInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktPointuserInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktPointuser/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktPointuser.table.refresh();
        SktPointuserInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktPointuserInfoData);
    ajax.start();
}

$(function() {

});
