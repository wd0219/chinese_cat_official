/**
 * 初始化个人认证详情对话框
 */
var UsersRealnameInfoDlg = {
    usersRealnameInfoData : {}
};

/**
 * 清除数据
 */
UsersRealnameInfoDlg.clearData = function() {
    this.usersRealnameInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersRealnameInfoDlg.set = function(key, val) {
    this.usersRealnameInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersRealnameInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UsersRealnameInfoDlg.close = function() {
    parent.layer.close(window.parent.UsersRealname.layerIndex);
}

/**
 * 收集数据
 */
UsersRealnameInfoDlg.collectData = function() {
    this
    .set('realId')
    .set('userId')
    .set('phone')
    .set('trueName')
    .set('cardType')
    .set('cardID')
    .set('cardUrl')
    .set('cardBackUrl')
    .set('handCardUrl')
    .set('auditStatus')
    .set('cardAddress')
    .set('staffId')
    .set('auditDatetime')
    .set('auditRemark')
    .set('addDatetime')
    .set('optTerminal')
    .set('optIP')
}

/**
 * 提交添加
 */
UsersRealnameInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersRealname/add", function(data){
        Feng.success("添加成功!");
        window.parent.UsersRealname.table.refresh();
        UsersRealnameInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersRealnameInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UsersRealnameInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    
    this.usersRealnameInfoData.auditStatus = $("input:radio[name=auditStatus]:checked").val();
    
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersRealname/update", function(data){
        Feng.success("修改成功!");
        window.parent.UsersRealname.table.refresh();
        UsersRealnameInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set("realId",this.usersRealnameInfoData.realId);
    ajax.set("auditStatus",this.usersRealnameInfoData.auditStatus);
    ajax.set("auditRemark",this.usersRealnameInfoData.auditRemark);
    ajax.start();
}

$(function() {
	//0普通 1主管 2经理
	var userType = $('#userType').val();
	switch(userType){
	case "0":
		$('#userType2').val("普通用户");
		$('#userType2').attr('disabled',true);
		break;
	case "1":
		$('#userType2').val("主管");
		$('#userType2').attr('disabled',true);
		break;
	case "2":
		$('#userType2').val("经理");
		$('#userType2').attr('disabled',true);
		break;
	default:
		break;
	}
	
	var auditStatus3 = $('#auditStatus3').val();
	switch(auditStatus3){//1 审核通过 2 审核不通过
	case "1":
		$('#auditStatus3').val("审核通过");
		break;
	case "2":
		$('#auditStatus3').val("审核不通过");
		break;
	default:
		break;
	}
});
