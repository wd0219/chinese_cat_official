/**
 * 个人认证管理初始化
 */
var UsersRealname = {
    id: "UsersRealnameTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
UsersRealname.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
           /* {title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},*/
            {title: '账号', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号', field: 'cardID', visible: true, align: 'center', valign: 'middle'},
            {title: '提交时间', field: 'addDatetime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核状态', field: 'auditStatus', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		let result = "";
            		switch(value){
            		case 0:
            			result =  "审核中";
            			break;
            		case 1:
            			result =  "审核通过";
            			break;
            		case 2:
            			result =  "审核不通过";
            			break;
            		}
            		return result;
            	}
            },
            {title: '审核时间', field: 'auditDatetime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'auditStatus', visible: true, align: 'center', valign: 'middle',formatter:UsersRealname.op},
    ];
};

/**
 * 操作
 */
UsersRealname.op = function(value,row,index){
	if(value==0){
		return "<a href='javascript:void(0)' onclick=\"UsersRealname.checkInfo()\">审核</a>";
	}else{
		return "<a href='javascript:void(0)' onclick=\"UsersRealname.openUsersRealnameDetail()\">查看</a>";
	}
}
/**
 * 审核
 */
UsersRealname.checkInfo = function(){
	UsersRealname.openUsersRealnameUpdate();
}

/**
 * 检查是否选中
 */
UsersRealname.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        UsersRealname.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加个人认证
 */
UsersRealname.openAddUsersRealname = function () {
    var index = layer.open({
        type: 2,
        title: '添加个人认证',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/usersRealname/usersRealname_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看个人认证详情
 */
UsersRealname.openUsersRealnameDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '个人认证详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/usersRealname/usersRealname_XiangQing/' + UsersRealname.seItem.realId
        });
        this.layerIndex = index;
    }
};
/**
 * 打开查看个人认证详情
 */
UsersRealname.openUsersRealnameUpdate = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '个人认证详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/usersRealname/usersRealname_Update/' + UsersRealname.seItem.realId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除个人认证
 */
UsersRealname.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/usersRealname/delete", function (data) {
            Feng.success("删除成功!");
            UsersRealname.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("usersRealnameId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询个人认证列表
 */
UsersRealname.search = function () {
    var queryData = {};
    queryData['phone'] = $("#phone").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    UsersRealname.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = UsersRealname.initColumn();
    var table = new BSTable(UsersRealname.id, "/usersRealname/list", defaultColunms);
    table.setPaginationType("server");
    UsersRealname.table = table.init();
});
