/**
 * 初始化会员登录日志详情对话框
 */
var SktLogUsersLoginsInfoDlg = {
    sktLogUsersLoginsInfoData : {}
};

/**
 * 清除数据
 */
SktLogUsersLoginsInfoDlg.clearData = function() {
    this.sktLogUsersLoginsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktLogUsersLoginsInfoDlg.set = function(key, val) {
    this.sktLogUsersLoginsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktLogUsersLoginsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktLogUsersLoginsInfoDlg.close = function() {
    parent.layer.close(window.parent.SktLogUsersLogins.layerIndex);
}

/**
 * 收集数据
 */
SktLogUsersLoginsInfoDlg.collectData = function() {
    this
    .set('loginId')
    .set('userId')
    .set('loginTime')
    .set('loginIp')
    .set('loginTer')
    .set('loginRemark');
}

/**
 * 提交添加
 */
SktLogUsersLoginsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktLogUsersLogins/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktLogUsersLogins.table.refresh();
        SktLogUsersLoginsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktLogUsersLoginsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktLogUsersLoginsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktLogUsersLogins/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktLogUsersLogins.table.refresh();
        SktLogUsersLoginsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktLogUsersLoginsInfoData);
    ajax.start();
}

$(function() {

});
