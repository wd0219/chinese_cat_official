/**
 * 会员登录日志管理初始化
 */
var SktLogUsersLogins = {
    id: "SktLogUsersLoginsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktLogUsersLogins.initColumn = function () {
    return [
			
            {field: 'selectItem', radio: true},
            {
            	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '用户昵称', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '登陆时间', field: 'loginTime', visible: true, align: 'center', valign: 'middle'},
            {title: '登陆IP', field: 'loginIp', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
SktLogUsersLogins.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktLogUsersLogins.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加会员登录日志
 */
SktLogUsersLogins.openAddSktLogUsersLogins = function () {
    var index = layer.open({
        type: 2,
        title: '添加会员登录日志',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktLogUsersLogins/sktLogUsersLogins_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看会员登录日志详情
 */
SktLogUsersLogins.openSktLogUsersLoginsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '会员登录日志详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktLogUsersLogins/sktLogUsersLogins_update/' + SktLogUsersLogins.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除会员登录日志
 */
SktLogUsersLogins.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktLogUsersLogins/delete", function (data) {
            Feng.success("删除成功!");
            SktLogUsersLogins.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktLogUsersLoginsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询会员登录日志列表
 */
SktLogUsersLogins.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['loginTime'] = $("#loginTime").val();
    queryData['createTime'] = $("#createTime").val();
    queryData['checkTime'] = $("#checkTime").val();
    SktLogUsersLogins.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktLogUsersLogins.initColumn();
    var table = new BSTable(SktLogUsersLogins.id, "/sktLogUsersLogins/list", defaultColunms);
    table.setPaginationType("server");
    SktLogUsersLogins.table = table.init();
});
