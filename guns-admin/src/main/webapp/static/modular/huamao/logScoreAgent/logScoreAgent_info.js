/**
 * 初始化代理公司积分记录详情对话框
 */
var LogScoreAgentInfoDlg = {
    logScoreAgentInfoData : {}
};

/**
 * 清除数据
 */
LogScoreAgentInfoDlg.clearData = function() {
    this.logScoreAgentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogScoreAgentInfoDlg.set = function(key, val) {
    this.logScoreAgentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogScoreAgentInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogScoreAgentInfoDlg.close = function() {
    parent.layer.close(window.parent.LogScoreAgent.layerIndex);
}

/**
 * 收集数据
 */
LogScoreAgentInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('agentId')
    .set('orderNo')
    .set('preScore')
    .set('scoreType')
    .set('score')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogScoreAgentInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logScoreAgent/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogScoreAgent.table.refresh();
        LogScoreAgentInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logScoreAgentInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogScoreAgentInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logScoreAgent/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogScoreAgent.table.refresh();
        LogScoreAgentInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logScoreAgentInfoData);
    ajax.start();
}

$(function() {

});
