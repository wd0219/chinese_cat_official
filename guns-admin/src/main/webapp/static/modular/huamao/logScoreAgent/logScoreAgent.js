/**
 * 代理公司积分记录管理初始化
 */
var LogScoreAgent = {
    id: "LogScoreAgentTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogScoreAgent.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前数量', field: 'preScore', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动数量', field: 'scorep', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后数量', field: 'ascore', visible: true, align: 'center', valign: 'middle'},
        	{title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogScoreAgent.type},
            //{title: '目标代理公司ID', field: 'agentId', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'scoreType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 类型对应：1运营积分>2待发运营积分>3代理奖励>31转化华宝>32待发积分转入
 */
LogScoreAgent.type = function(value,row,index){
	if(value == 1){
		return "运营积分";
	}else if(value == 2){
		return "待发运营积分";
	}else if(value == 3){
		return "代理奖励";
	}else if(value == 31){
		return "转化华宝";
	}else if(value == 32){
		return "待发积分转入";
	}
}
/**
 * 检查是否选中
 */
LogScoreAgent.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogScoreAgent.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理公司积分记录
 */
LogScoreAgent.openAddLogScoreAgent = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理公司积分记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logScoreAgent/logScoreAgent_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代理公司积分记录详情
 */
LogScoreAgent.openLogScoreAgentDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理公司积分记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logScoreAgent/logScoreAgent_update/' + LogScoreAgent.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代理公司积分记录
 */
LogScoreAgent.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logScoreAgent/delete", function (data) {
            Feng.success("删除成功!");
            LogScoreAgent.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logScoreAgentId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代理公司积分记录列表
 */
LogScoreAgent.search = function () {
    var queryData = {};
    queryData['phone'] = $("#phone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogScoreAgent.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogScoreAgent.initColumn();
    var table = new BSTable(LogScoreAgent.id, "/logScoreAgent/list", defaultColunms);
    table.setPaginationType("server");
    LogScoreAgent.table = table.init();
});
//导出Excel
LogScoreAgent.export= function(){
	$('#'+LogScoreAgent.id).tableExport({
		fileName:'代理公司积分记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}