/**
 * 初始化代理公司登录日志详情对话框
 */
var SktLogAgentsLoginsInfoDlg = {
    sktLogAgentsLoginsInfoData : {}
};

/**
 * 清除数据
 */
SktLogAgentsLoginsInfoDlg.clearData = function() {
    this.sktLogAgentsLoginsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktLogAgentsLoginsInfoDlg.set = function(key, val) {
    this.sktLogAgentsLoginsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktLogAgentsLoginsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktLogAgentsLoginsInfoDlg.close = function() {
    parent.layer.close(window.parent.SktLogAgentsLogins.layerIndex);
}

/**
 * 收集数据
 */
SktLogAgentsLoginsInfoDlg.collectData = function() {
    this
    .set('loginId')
    .set('agentId')
    .set('loginTime')
    .set('loginIp');
}

/**
 * 提交添加
 */
SktLogAgentsLoginsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktLogAgentsLogins/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktLogAgentsLogins.table.refresh();
        SktLogAgentsLoginsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktLogAgentsLoginsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktLogAgentsLoginsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktLogAgentsLogins/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktLogAgentsLogins.table.refresh();
        SktLogAgentsLoginsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktLogAgentsLoginsInfoData);
    ajax.start();
}

$(function() {

});
