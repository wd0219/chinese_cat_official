/**
 * 代理公司登录日志管理初始化
 */
var SktLogAgentsLogins = {
    id: "SktLogAgentsLoginsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktLogAgentsLogins.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '代理商名称', field: 'agentName', visible: true, align: 'center', valign: 'middle'},
            {title: '登陆时间', field: 'loginTime', visible: true, align: 'center', valign: 'middle'},
            {title: '登陆IP', field: 'loginIp', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktLogAgentsLogins.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktLogAgentsLogins.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理公司登录日志
 */
SktLogAgentsLogins.openAddSktLogAgentsLogins = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理公司登录日志',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktLogAgentsLogins/sktLogAgentsLogins_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代理公司登录日志详情
 */
SktLogAgentsLogins.openSktLogAgentsLoginsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理公司登录日志详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktLogAgentsLogins/sktLogAgentsLogins_update/' + SktLogAgentsLogins.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代理公司登录日志
 */
SktLogAgentsLogins.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktLogAgentsLogins/delete", function (data) {
            Feng.success("删除成功!");
            SktLogAgentsLogins.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktLogAgentsLoginsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代理公司登录日志列表
 */
SktLogAgentsLogins.search = function () {
    var queryData = {};
    queryData['loginTime'] = $("#loginTime").val();
    queryData['createTime'] = $("#createTime").val();
    queryData['checkTime'] = $("#checkTime").val();
    SktLogAgentsLogins.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktLogAgentsLogins.initColumn();
    var table = new BSTable(SktLogAgentsLogins.id, "/sktLogAgentsLogins/list", defaultColunms);
    table.setPaginationType("server");
    SktLogAgentsLogins.table = table.init();
});
