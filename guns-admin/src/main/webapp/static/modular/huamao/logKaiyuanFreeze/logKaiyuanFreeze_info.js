/**
 * 初始化待发华宝货款记录详情对话框
 */
var LogKaiyuanFreezeInfoDlg = {
    logKaiyuanFreezeInfoData : {}
};

/**
 * 清除数据
 */
LogKaiyuanFreezeInfoDlg.clearData = function() {
    this.logKaiyuanFreezeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogKaiyuanFreezeInfoDlg.set = function(key, val) {
    this.logKaiyuanFreezeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogKaiyuanFreezeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogKaiyuanFreezeInfoDlg.close = function() {
    parent.layer.close(window.parent.LogKaiyuanFreeze.layerIndex);
}

/**
 * 收集数据
 */
LogKaiyuanFreezeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preKaiyuan')
    .set('kaiyuanType')
    .set('kaiyuan')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogKaiyuanFreezeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logKaiyuanFreeze/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogKaiyuanFreeze.table.refresh();
        LogKaiyuanFreezeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logKaiyuanFreezeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogKaiyuanFreezeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logKaiyuanFreeze/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogKaiyuanFreeze.table.refresh();
        LogKaiyuanFreezeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logKaiyuanFreezeInfoData);
    ajax.start();
}

$(function() {

});
