/**
 * 待发华宝货款记录管理初始化
 */
var LogKaiyuanFreeze = {
    id: "LogKaiyuanFreezeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogKaiyuanFreeze.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前华宝余额', field: 'preKaiyuan', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动金额', field: 'kaiyuanp', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后华宝余额', field: 'akaiyuan', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogKaiyuanFreeze.type},
            //{title: '发起用户ID 0为平台', field: 'fromId', visible: true, align: 'center', valign: 'middle'},
            //{title: '目标用户ID 0为平台', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'kaiyuanType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 资金变动类型对应：1商城货款>2商家货款>31用户升级支付>33购买库存支出>34购买库存手续费
 */
LogKaiyuanFreeze.type = function(value,row,index){
	if(value == 1){
		return "商城货款";
	}else if(value == 2){
		return "商家货款";
	}else if(value == 31){
		return "用户升级支付";
	}else if(value == 33){
		return "购买库存支出";
	}else if(value == 34){
		return "购买库存手续费";
	}
}
/**
 * 检查是否选中
 */
LogKaiyuanFreeze.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogKaiyuanFreeze.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加待发华宝货款记录
 */
LogKaiyuanFreeze.openAddLogKaiyuanFreeze = function () {
    var index = layer.open({
        type: 2,
        title: '添加待发华宝货款记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logKaiyuanFreeze/logKaiyuanFreeze_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看待发华宝货款记录详情
 */
LogKaiyuanFreeze.openLogKaiyuanFreezeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '待发华宝货款记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logKaiyuanFreeze/logKaiyuanFreeze_update/' + LogKaiyuanFreeze.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除待发华宝货款记录
 */
LogKaiyuanFreeze.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logKaiyuanFreeze/delete", function (data) {
            Feng.success("删除成功!");
            LogKaiyuanFreeze.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logKaiyuanFreezeId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询待发华宝货款记录列表
 */
LogKaiyuanFreeze.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogKaiyuanFreeze.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogKaiyuanFreeze.initColumn();
    var table = new BSTable(LogKaiyuanFreeze.id, "/logKaiyuanFreeze/list/"+$('#userPhone2').val(), defaultColunms);
    table.setPaginationType("server");
    LogKaiyuanFreeze.table = table.init();
});
//导出Excel
LogKaiyuanFreeze.export= function(){
	$('#'+LogKaiyuanFreeze.id).tableExport({
		fileName:'待发华宝货款记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}