/**
 * 导航管理管理初始化
 */
var Navs = {
    id: "NavsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Navs.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '导航类型', field: 'navType', visible: true, align: 'center', valign: 'middle',formatter:Navs.navType},
            {title: '导航名称', field: 'navTitle', visible: true, align: 'center', valign: 'middle'},
            {title: '导航连接', field: 'navUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '是否显示', field: 'isShow', visible: true, align: 'center', valign: 'middle',formatter:Navs.isShow},
            {title: '打开方式', field: 'isOpen', visible: true, align: 'center', valign: 'middle',formatter:Navs.isOpen},
            {title: '排序号', field: 'navSort', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'createTime', visible: true, align: 'center', valign: 'middle',formatter:Navs.op}
    ];
};
/**
 * 导航类型对应：0顶部，1底部
 */
Navs.navType = function(value,row,index){
	if(value == 0){
		return "顶部";
	}else if(value == 1){
		return "底部";
	}
}
/**
 * 是否显示对应：0隐藏，1显示
 */
Navs.isShow = function(value,row,index){
	if(value == 0){
		return "隐藏";
	}else if(value == 1){
		return "显示";
	}
}
/**
 * 打开方式对应：0页面跳转，1新窗口打开
 */
Navs.isOpen = function(value,row,index){
	if(value == 0){
		return "页面跳转";
	}else if(value == 1){
		return "新窗口打开";
	}
}
/*
 * 操作：给导航添加修改、删除功能
 * */
Navs.op = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"Navs.openNavsDetail()\">修改</a>&nbsp;&nbsp;<a href='javascript:void(0)'onclick=\"Navs.delete()\">删除</a>"
};
/**
 * 检查是否选中
 */
Navs.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Navs.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加导航管理
 */
Navs.openAddNavs = function () {
    var index = layer.open({
        type: 2,
        title: '添加导航管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/navs/navs_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看导航管理详情
 */
Navs.openNavsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '导航管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/navs/navs_update/' + Navs.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除导航管理
 */
Navs.delete = function () {
    if (this.check()) {
    	var operation = function (){
    		var id = Navs.seItem.id;
        var ajax = new $ax(Feng.ctxPath + "/navs/delete", function (data) {
            Feng.success("删除成功!");
            Navs.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("navsId",id);
        ajax.start();
    };
    	Feng.confirm("是否删除该条记录?",operation);
    }
};

/**
 * 查询导航管理列表
 */
Navs.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Navs.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Navs.initColumn();
    var table = new BSTable(Navs.id, "/navs/list", defaultColunms);
    table.setPaginationType("server");
    Navs.table = table.init();
});
