/**
 * 初始化导航管理详情对话框
 */
var NavsInfoDlg = {
    navsInfoData : {}
};

/**
 * 清除数据
 */
NavsInfoDlg.clearData = function() {
    this.navsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
NavsInfoDlg.set = function(key, val) {
    this.navsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
NavsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
NavsInfoDlg.close = function() {
    parent.layer.close(window.parent.Navs.layerIndex);
}

/**
 * 收集数据
 */
NavsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('navType')
    .set('navTitle')
    .set('navUrl')
    .set('isShow')
    .set('isOpen')
    .set('navSort')
    .set('createTime');
}

/**
 * 提交添加
 */
NavsInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    this.clearData();
    this.collectData();
    
  //因为js不能主动获取单选框的的值所以在这里进行设置
    this.navsInfoData.isShow = $('input:radio[name=isShow]:checked').val();
    this.navsInfoData.isOpen = $('input:radio[name=isOpen]:checked').val();
    
    var navSort = $("#navSort").val();
    var navTitle = $("#navTitle").val();
    var nav = $("#nav").val();
    var navUrl = $("#navUrl").val();
	
    if(navTitle == null || navTitle ==""){
        var html='<span class="error">导航名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#navTitle').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(nav == null || nav ==""){
        var html='<span class="error">链接来自不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#nav').after(html);
        falg = 1;
    }

    if(!isRealNum(navSort)){
        var html='<span class="error">导航排序必须为数字!</span>';
        //$('#menuSort').parent().append(html);
        $('#navSort').after(html);
        falg = 1;
    }

    if(navUrl == null || navUrl ==""){
        var html='<span class="error">导航链接不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#navUrl').after(html);
        falg = 1;
    }

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/navs/add", function(data){
        Feng.success("添加成功!");
        window.parent.Navs.table.refresh();
        NavsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.navsInfoData);
    ajax.start();
    }else{
    Feng.success("请核实添加字段!");
	}
}

/**
 * 提交修改
 */
NavsInfoDlg.editSubmit = function() {
	
    $('.col-sm-9 span').html("");
    var falg = 0;

    this.clearData();
    this.collectData();
    
    //因为js不能主动获取单选框的的值所以在这里进行设置
    this.navsInfoData.isShow = $('input:radio[name=isShow]:checked').val();
    this.navsInfoData.isOpen = $('input:radio[name=isOpen]:checked').val();
    
    //提交信息
    $('.col-sm-9 span').html("");
    var falg = 0;

    var navSort = $("#navSort").val();
    var navTitle = $("#navTitle").val();
    var nav = $("#nav").val();
    var navUrl = $("#navUrl").val();

    if(navTitle == null || navTitle ==""){
        var html='<span class="error">导航名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#navTitle').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(nav == null || nav ==""){
        var html='<span class="error">链接来自不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#nav').after(html);
        falg = 1;
    }

    if(!isRealNum(navSort)){
        var html='<span class="error">导航排序必须为数字!</span>';
        //$('#menuSort').parent().append(html);
        $('#navSort').after(html);
        falg = 1;
    }

    if(navUrl == null || navUrl ==""){
        var html='<span class="error">导航Url不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#navUrl').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    
/*    let
    isShow = $('input:radio[name=isShow]:checked').val();
*/    
    let
	goods1 = $('#goods1').val();
    
    let
    isShow = $('input:radio[name=isShow]:checked').val();
    let
    isOpen = $('input:radio[name=isOpen]:checked').val();
    
    this.navsInfoData.isShow = isShow;
    this.navsInfoData.isOpen = isOpen;
    
    //提交信息
    if(falg == 0){
        var ajax = new $ax(Feng.ctxPath + "/navs/update", function(data){
            Feng.success("修改成功!");
            window.parent.Navs.table.refresh();
            NavsInfoDlg.close();
        },function(data){
            Feng.error("修改失败!" + data.responseJSON.message + "!");
        });
        ajax.set(this.navsInfoData);
        ajax.start();
    }else{
	    Feng.success("请核实添加字段!");
	}
}
	function isRealNum(val){
	    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	    if(val === "" || val ==null){
	        return false;
	    }
	    if(!isNaN(val)){
	        return true;
	    }else{
	        return false;
	    }
	}


$(function() {
	//导航位置回显选中
	if($("#navTypeS").val() == 0){
		$("#navType").val(0);
	}else{
		$("#navType").val(1);
	}
	//是否显示回显选中
	if($("#showC").val() == 0){
		$("#hidden").attr("checked","checked");
	}else{
		$("#show").attr("checked","checked");
	};
	//打开方式回显选中
	if($("#openC").val() == 0){
		$("#skip").attr("checked","checked");
	}else{
		$("#new").attr("checked","checked");
	};
});
