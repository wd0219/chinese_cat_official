$(function(){
	initSystem();
});

var initSystem = function() {
	sktOrderRefundsToOrdersDetail.init();
};

var sktOrderRefundsToOrdersDetail = {
	init:function(){
		this.sktOrderOrdersDetail();
		this.sktOrderGoodsLoad();
	},
	sktOrderOrdersDetail : function(){
		var sktOrderRefundsId = $("#sktOrderRefundsId").val();
		$.post("/sktOrderRefunds/sktOrderRefunds_selectOrdersDetail",{
			sktOrderRefundsId:sktOrderRefundsId
		},function(data){
			var sktOrderRefunds = data;
			sktOrderRefundsToOrdersDetail.logInit(sktOrderRefunds);
			sktOrderRefundsToOrdersDetail.orderInfoLoad(sktOrderRefunds);
			sktOrderRefundsToOrdersDetail.invoiceInfoLoad(sktOrderRefunds);
			sktOrderRefundsToOrdersDetail.consigneeInfoLoad(sktOrderRefunds);
			//sktOrderRefundsToOrdersDetail.goodsInfoLoad(sktOrderRefunds);
			sktOrderRefundsToOrdersDetail.bottomInfoLoad(sktOrderRefunds);
		});
	},
	/**
	 * 日志信息
	 */
	logInit : function(sktOrderRefunds){
		var html = [];
		html.push("<div style='margin-left:50px;'>");
		html.push("<div><label>下单成功，等待用户支付:</label>"+sktOrderRefunds.createTime+"</div>");
		html.push("<div><label>订单已支付，下单成功:</label>"+sktOrderRefunds.paymentTime+"</div>");
		if(sktOrderRefunds.deliveryTime != undefined)
			html.push("<div>"+sktOrderRefunds.deliveryTime+"<label>商家已发货,快递号为：</label>"+sktOrderRefunds.expressNo+"</div>");
		if(sktOrderRefunds.refundTime != undefined)
			html.push("<div>"+sktOrderRefunds.refundTime+"<label>用户退款原因：</label>"+sktOrderRefunds.refundOtherReson+"</div>");
		html.push("</div>");
		$('#logInit').html(html.join(''));
	},
	/**
	 * 订单信息
	 * 
	 */
	orderInfoLoad : function(sktOrderRefunds){
		var html = [];
		html.push("<div style='margin-left:50px;'>");
		if(sktOrderRefunds.orderId != undefined){
			html.push("<div><label>订单编号:</label>"+sktOrderRefunds.orderId+"</div>");
		}else{
			html.push("<div><label>订单编号:</label></div>");
		}
		
		if(sktOrderRefunds.createTime != undefined){
			html.push("<div><label>下单时间:</label>"+sktOrderRefunds.createTime+"</div>");
		}else{
			html.push("<div><label>下单时间:</label></div>");
		}
		
		if(sktOrderRefunds.payType != undefined){
			html.push("<div><label>支付方式:</label>"+sktOrderRefunds.payType+"</div>");
		}else{
			html.push("<div><label>支付方式:</label></div>");
		}
		
		if(sktOrderRefunds.expressName != undefined){
			html.push("<div><label>快递公司:</label>"+sktOrderRefunds.expressName+"</div>");
		}else{
			html.push("<div><label>快递公司:</label></div>");
		}
		
		if(sktOrderRefunds.expressNo != undefined){
			html.push("<div><label>  快递号:</label>"+sktOrderRefunds.expressNo+"</div>");
		}else{
			html.push("<div><label>  快递号:</label></div>");
		}
		
		html.push("<div><label>买家留言:</label>");
		html.push("</div>");
		$('#orderInfoLoad').html(html.join(''));
	},
	/**
	 * 发票信息
	 */
	invoiceInfoLoad : function(sktOrderRefunds){
		var html = [];
		html.push("<div style='margin-left:50px;'>");
		if(sktOrderRefunds.isInvoice != undefined){
			html.push("<div><label>是否需要发票:</label>"+sktOrderRefunds.isInvoice+"</div>");
		}else{
			html.push("<div><label>是否需要发票:</label></div>");
		}
		
		if(sktOrderRefunds.invoiceClient != undefined){
			html.push("<div><label>发票抬头:</label>"+sktOrderRefunds.invoiceClient+"</div>");
		}else{
			html.push("<div><label>发票抬头:</label>/div>");
		}
		
		html.push("</div>");
		$('#invoiceInfoLoad').html(html.join(''));
	},
	/**
	 * 收货人信息
	 */
	consigneeInfoLoad : function(sktOrderRefunds){
		var html = [];
		html.push("<div style='margin-left:50px;'>");
		if(sktOrderRefunds.userName != undefined){
			html.push("<div><label>收货人:</label>"+sktOrderRefunds.userName+"</div>");
		}else{
			html.push("<div><label>收货人:</label></div>");
		}
		
		if(sktOrderRefunds.userAddress != undefined){
			html.push("<div><label>收货人地址:</label>"+sktOrderRefunds.userAddress+"</div>");
		}else{
			html.push("<div><label>收货人地址:</label></div>");
		}
		
		if(sktOrderRefunds.userPhone != undefined){
			html.push("<div><label>联系方式:</label>"+sktOrderRefunds.userPhone+"</div>");
		}else{
			html.push("<div><label>联系方式:</label></div>");
		}
		html.push("</div>");
		$('#consigneeInfoLoad').html(html.join(''));
	},
	
	/**
	 * 商品数据加载
	 */
	sktOrderGoodsLoad:function(){
		var orderId = $("#orderId").val();
		$.post("/sktOrders/sktOrdersGoods_load",{
			sktOrdersId:orderId
		},function(result){
			$.each(result, function(i,val){
				var goodsPic = val.goodsImg;
                const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
                if(goodsPic.indexOf('http')==-1){
                    goodsPic=imgUrl+goodsPic
                }else{
                    goodsPic=goodsPic
                }
				var goodsId = val.goodsId;
				$("#goodsHead").after("<div class='goods-item' id='goodsItem"+i+"'></div>");
				$("#goodsItem"+i+"").append("<div class='shop'>"+val.shopName+"</div>");
				$("#goodsItem"+i+"").append("<div class='goods-list' id='goodsList"+i+"' style='overflow: hidden;'>");
				$("#goodsList"+i+"").append("<div class='item' id='item"+i+"'></div>");
				
				$("#item"+i+"").append("<div class='goods' id='goods"+i+"'></div>");
				$("#item"+i+"").append("<div class='price'>¥"+val.goodsPrice+"</div>");
				$("#item"+i+"").append("<div class='num'>"+val.goodsNum+"</div>");
				$("#item"+i+"").append("<div class='t-price'>¥"+val.goodsPrice * val.goodsNum+"</div>");
				$("#item"+i+"").append("<div class='f-clear'></div>");
				
				$("#goods"+i+"").append("<div class='img' id='img"+i+"'></div>");
				$("#goods"+i+"").append("<div class='name'>"+val.goodsName+"</div>");
				//$("#goods"+i+"").append("<div class='spec'>内存：2G<br>颜色：铂金色</div>");
				
				$("#img"+i+"").append("<a href='http://test.izzht.cn/home/goods/detail?id="+val.goodsId+"' target='_blank'>" +
						"<img src='"+goodsPic+"' width='80' height='80' title='"+val.goodsName+"'> </a>");
				
				sktOrderRefundsToOrdersDetail.sktOrderGoodsParameterLoad(goodsId,i);
				
			});
		})
	},
	/**
	 * 商品参数加载
	 */
	sktOrderGoodsParameterLoad:function(goodsId,i){
		$.post("/sktOrders/sktOrdersGoodsParameter_load",{
			goodsId:goodsId
		},function(result){
			var temp="";
			$.each(result,function(i,data){
				temp = temp + data.attrName +":"+data.attrVal+"<br>";
			});
			$("#goods"+i+"").append("<div class='spec'>"+temp+"</div>")
		})
	},
	
	
	/**
	 * 商品清单
	 */
/*	goodsInfoLoad : function(sktOrderRefunds){
		let orderId = sktOrderRefunds.orderId;
		if(!orderId) return;
		$.post("/sktOrderRefunds/sktOrderRefunds_selectGoodsDetail",{'orderId':sktOrderRefunds.orderId},function(data){
			sktOrderRefundsToOrdersDetail.addInfoLoad(data);
		});
	},*/
	/**
	 * 商品清单
	 */
	/*addInfoLoad : function(goods){
		let html = [];
		html.push("<div style=\"background-color:#ddd\"><span>商品</span> <span>单价</span> <span>数量</span> <span>总价</span></div>");
		$.each(goods,function(i,val){
			html.push("<div>"+val.shopName+"</div>");
			html.push("<div>");
			html.push("<img src='"+val.goodsImg+"' style=\"width:100px;heigth:100px;\" />");
			html.push("<span>"+val.goodsName+"</span>&nbsp;&nbsp;&nbsp;");
			html.push("<span>"+val.goodsNum+"</span>&nbsp;&nbsp;&nbsp;");
			html.push("<span>"+val.goodsPrice+"</span>");
			html.push("</div>");
		})
		$('#goodsInfoLoad').html(html.join(''));
	},*/
	/**
	 * 底部信息
	 */
	bottomInfoLoad : function(sktOrderRefunds){
		$("#goodsSummary").html("");
		$("#goodsSummary").append("<div class='summary' id='goodsMoney'></div>");
		$("#goodsMoney").append("商品总金额：¥<span>"+sktOrderRefunds.goodsMoney+"</span>");
		
		$("#goodsSummary").append("<div class='summary' id='deliverMoney'></div>");
		$("#deliverMoney").append("运费：¥<span>"+sktOrderRefunds.deliverMoney+"</span>");
		
		$("#goodsSummary").append("<div class='summary' id='totalMoney'></div>");
		$("#totalMoney").append("应付总金额：¥<span>"+sktOrderRefunds.totalMoney+"</span>");
		
		$("#goodsSummary").append("<div class='summary' id='realMoney'></div>");
		$("#realMoney").append("实付总金额：¥<span>"+sktOrderRefunds.realMoney+"</span>");
		
		$("#goodsSummary").append("<div class='summary' id='score'></div>");
		//$("#score").append("可获得积分：<span class='orderScore'>"+skt_order.score+"</span>");
		$("#score").append("可获得积分：<span class='orderScore'>"+sktOrderRefunds.goodsMoney * 100+".000</span>个");
	},
	/**
	 * 关闭此对话框
	 */
	close : function(){
		 parent.layer.close(window.parent.SktOrderRefunds.layerIndex);
	}
		
}