/**
 * 退款订单管理初始化
 */
var SktOrderRefunds = {
    id: "SktOrderRefundsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktOrderRefunds.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '订单编号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '申请人', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号 ', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '实收金额 ', field: 'realMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '申请退款金额 ', field: 'backMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '申请时间', field: 'refundTime', visible: true, align: 'center', valign: 'middle'},
            {title: '退款状态', field: 'isRefund', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		var result = "";
            		if(value == 1){
            			result = "已退款";
            		}else{
            			result = "未退款";
            		}
            		return result;
            	}},
            {title: '退款备注', field: 'refundRemark', visible: true, align: 'center', valign: 'middle'},
            {title: '处理状态', field: 'refundStatus', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		var result = "";
            		switch(value){
            		case -1:
            			result = "商家不同意";
            			break;
            		case 0:
            			result = "用户申请退款 ";
            			break;
            		case 1:
            			result = "商家同意退款 ";
            			break;
            		case 2:
            			result = "平台已退款 ";
            			break;
            			
            		}
            		return result;
            	}},
            {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		/**
            		 * row代表一行数据
            		 */
            		var result = "";
            		var aa = row.isRefund;
            		if(row.isRefund == 0 && row.refundStatus == 1){
            			result += "<a href='javascript:void(0)' onclick=\"SktOrderRefunds.toRefund()\">审核</a> "
            			result += "<a href='javascript:void(0)' onclick=\"SktOrderRefunds.toRefundReject()\">拒绝</a> "
            		}
            		result +="<a href='javascript:void(0)' onclick=\"SktOrderRefunds.openSktOrdersDetail()\">详情</a>";
            		return result;
            	}}
          
    ];
};

/**
 * 打开审核退款订单详情
 */
SktOrderRefunds.toRefundReject = function () {
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '审核订单',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/sktOrderRefunds/sktOrderRefunds_toRefundReject/' + SktOrderRefunds.seItem.id
		});
		this.layerIndex = index;
	}
};
/**
 * 打开审核退款订单详情
 */
SktOrderRefunds.toRefund = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '拒绝订单',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktOrderRefunds/sktOrderRefunds_toRefund/' + SktOrderRefunds.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 打开退款订单详情
 */
SktOrderRefunds.openSktOrdersDetail = function(){
	if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '退款订单详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktOrderRefunds/sktOrderRefunds_toOrdersDetail/' + SktOrderRefunds.seItem.id
        });
        this.layerIndex = index;
    }
}
/**
 * 检查是否选中
 */
SktOrderRefunds.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktOrderRefunds.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加退款订单
 */
SktOrderRefunds.openAddSktOrderRefunds = function () {
    var index = layer.open({
        type: 2,
        title: '添加退款订单',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktOrderRefunds/sktOrderRefunds_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看退款订单详情
 */
SktOrderRefunds.openSktOrderRefundsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '退款订单详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktOrderRefunds/sktOrderRefunds_update/' + SktOrderRefunds.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除退款订单
 */
SktOrderRefunds.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktOrderRefunds/delete", function (data) {
            Feng.success("删除成功!");
            SktOrderRefunds.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktOrderRefundsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询退款订单列表
 */
SktOrderRefunds.search = function () {
	var queryData = {};
    queryData['provinceId'] = $("#province").val();
    queryData['cityId'] = $("#citys").val();
    queryData['areaId'] = $("#county").val();
    
    queryData['orderNo'] = $("#orderNo").val();
    queryData['shopName'] = $("#shopName").val();
    
    queryData['isRefund'] = $("#isRefund").val();
    queryData['refundStatus'] = $("#refundStatus").val();
    
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    SktOrderRefunds.table.refresh({query: queryData});
};

/**
 * 清空查询订单条件
 */
SktOrderRefunds.refresh = function () {
	$("#province").val("");
	$("#citys").val("");
	$("#county").val("");
	
	$("#orderNo").val("");
	$("#shopName").val("");
	
	$("#isRefund").val("");
	$("#refundStatus").val("");
	
	$("#beginTime").val("");
	$("#endTime").val("");
	
};

/**
 * 下拉菜单实现
 */
SktOrderRefunds.selectCon = function(pid){
	/**
	 * 当pid的值为60时，表示退款状态，操作isRefund的option
	 * 当pid的值为63时，表示处理状态，操作refundStatus的option
	 */
	$.post("/dict/namelist",{
		pid:pid
	},function(result){
		 $.each(result, function(i,val){
    		 if(pid == 60){
    			 $("#isRefund").append("<option value='" + val.num + "'>" + val.name + "</option>");
    		 }
    		 if(pid == 63){
    			 $("#refundStatus").append("<option value='" + val.num + "'>" + val.name + "</option>");
    		 }
    	 });
	});
}
$(function () {
    var defaultColunms = SktOrderRefunds.initColumn();
    var table = new BSTable(SktOrderRefunds.id, "/sktOrderRefunds/alllist", defaultColunms);
    table.setPaginationType("server");
    SktOrderRefunds.table = table.init();
    SktOrderRefunds.selectCon(60);
    SktOrderRefunds.selectCon(63);
});
