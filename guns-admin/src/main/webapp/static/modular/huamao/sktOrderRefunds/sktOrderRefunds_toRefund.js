/**
 * @author caody
 * 调用页面初始化方法
 */
$(function() {
	initSystem();
});
/**
 * 初始化方法的，调用sktOrderRefundsToRefund对象的加载方法
 */
var initSystem = function() {
	sktOrderRefundsToRefund.init();
};
/**
 * 创建sktOrderRefundsToRefund对象
 */
var sktOrderRefundsToRefund = {
	/**
	 * 平台审核通过是TYPE的值是2
	 */	
	TYPE:2,	
		
	init : function() {
		this.sktOrderRefunds();
	},
	/**
	 * 页面加载数据方法
	 */
	sktOrderRefunds:function(){
		var sktOrderRefundsId = $("#sktOrderRefundsId").val();
		$.post("/sktOrderRefunds/sktOrderRefundsToRefund_load",{
			sktOrderRefundsId:sktOrderRefundsId
		},function(data){
			var sktOrderRefunds = data;
			sktOrderRefundsToRefund.parameterInit(sktOrderRefunds);
			sktOrderRefundsToRefund.orderInfoLoad(sktOrderRefunds);
			sktOrderRefundsToRefund.refundsInfoLoad(sktOrderRefunds);
		});
	},
	/**
	 * 审核参数设置
	 */
	parameterInit:function(sktOrderRefunds){
		$("#backMoney").val(sktOrderRefunds.backMoney);
		$("#refundTo").val(sktOrderRefunds.refundTo);
		$("#orderId").val(sktOrderRefunds.orderId);
		$("#orderNo").val(sktOrderRefunds.orderNo);
	},
	/**
	 * 订单信息加载
	 */
	orderInfoLoad:function(sktOrderRefunds){
		$("#orderInfo").append("<div class='form-group' id='orderNo'></div>");
		$("#orderNo").append("<label for='name'>订单编号:</label>"+sktOrderRefunds.orderNo+"");
		
		$("#orderInfo").append("<div class='form-group' id='payType'></div>");
		$("#payType").append("<label for='name'>支付方式:</label>"+sktOrderRefunds.payType+"");
		
		$("#orderInfo").append("<div class='form-group' id='totalMoney'></div>");
		$("#totalMoney").append("<label for='name'>订单总金额:</label>¥"+sktOrderRefunds.totalMoney+"" +
				"【商品总金额:¥"+sktOrderRefunds.goodsMoney+"，运费:¥"+sktOrderRefunds.deliverMoney+"】");
		
		$("#orderInfo").append("<div class='form-group' id='realMoney'></div>");
		$("#realMoney").append("<label for='name'>实收总金额:</label>¥"+sktOrderRefunds.realMoney+"");
	},
	
	/**
	 * 退款信息加载
	 */
	refundsInfoLoad:function(sktOrderRefunds){
		$("#refundsInfo").append("<div class='form-group' id='refundReson'></div>");
		$("#refundReson").append("<label for='name'>退款原因:</label>"+sktOrderRefunds.refundReson+"");
		
		$("#refundsInfo").append("<div class='form-group' id='backMoney'></div>");
		$("#backMoney").append("<label for='name'>申请退款:</label>"+sktOrderRefunds.backMoney+"");
		
		$("#refundsInfo").append("<div class='form-group' id='textarea'></div>");
		$("#textarea").append("<label for='name' id='operatingNotes' style='display:block;'>操作备注:</label>");
		$("#operatingNotes").after("<textarea id='content' style='width:30%;height:60px;' placeholder='操作备注，用户可见' maxlength='200'></textarea>")
	},
	
	/**
	 * 关闭此对话框
	 */
	close:function() {
	    parent.layer.close(window.parent.SktOrderRefunds.layerIndex);
	},
	/**
	 * 审核确认功能实现
	 */
	submit:function(type){
		var content = $("#content").val();
		var id = $("#sktOrderRefundsId").val();
		var backMoney = $("#backMoney").val();
		var refundTo = $("#refundTo").val();
		var orderId = $("#orderId").val();
		var orderNo = $("#orderNo").val();
		$.post("/sktOrderRefunds/sktOrderRefundsToRefund_submit",{
			refundRemark:content,
			refundStatus:type,
			id:id,
			backMoney:backMoney,
			refundTo:refundTo,
			orderId:orderId,
			orderNo:orderNo
		},function(result){
			if(result){
				Feng.success("提交成功!");
				$("#content").val("");
				sktOrderRefundsToRefund.close();
			}else{
				Feng.success("提交失败!请重新操作");
				$("#content").val("");
				sktOrderRefundsToRefund.close();
			}
			parent.location.reload(); // 父页面刷新
				
		});
	}
	

};