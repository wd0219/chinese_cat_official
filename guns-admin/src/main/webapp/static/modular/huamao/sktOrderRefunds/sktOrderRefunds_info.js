/**
 * 初始化退款订单详情对话框
 */
var SktOrderRefundsInfoDlg = {
    sktOrderRefundsInfoData : {}
};

/**
 * 清除数据
 */
SktOrderRefundsInfoDlg.clearData = function() {
    this.sktOrderRefundsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrderRefundsInfoDlg.set = function(key, val) {
    this.sktOrderRefundsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrderRefundsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktOrderRefundsInfoDlg.close = function() {
    parent.layer.close(window.parent.SktOrderRefunds.layerIndex);
}

/**
 * 收集数据
 */
SktOrderRefundsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('orderId')
    .set('refundTo')
    .set('refundReson')
    .set('refundOtherReson')
    .set('expressId')
    .set('expressNo')
    .set('backMoney')
    .set('refundTradeNo')
    .set('refundRemark')
    .set('refundTime')
    .set('shopRejectReason')
    .set('refundStatus')
    .set('createTime');
}

/**
 * 提交添加
 */
SktOrderRefundsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrderRefunds/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktOrderRefunds.table.refresh();
        SktOrderRefundsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrderRefundsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktOrderRefundsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrderRefunds/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktOrderRefunds.table.refresh();
        SktOrderRefundsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrderRefundsInfoData);
    ajax.start();
}

$(function() {

});
