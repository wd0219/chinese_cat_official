/**
 * 初始化现金记录详情对话框
 */
var LogCashInfoDlg = {
    logCashInfoData : {}
};

/**
 * 清除数据
 */
LogCashInfoDlg.clearData = function() {
    this.logCashInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogCashInfoDlg.set = function(key, val) {
    this.logCashInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogCashInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogCashInfoDlg.close = function() {
    parent.layer.close(window.parent.LogCash.layerIndex);
}

/**
 * 收集数据
 */
LogCashInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preCash')
    .set('cashType')
    .set('cash')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogCashInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logCash/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogCash.table.refresh();
        LogCashInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logCashInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogCashInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logCash/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogCash.table.refresh();
        LogCashInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logCashInfoData);
    ajax.start();
}

$(function() {

});
