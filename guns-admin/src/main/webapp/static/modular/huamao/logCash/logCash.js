/**
 * 现金记录管理初始化
 */
var LogCash = {
    id: "LogCashTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogCash.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前现金余额', field: 'preCash', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动金额', field: 'cashp', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后现金余额', field: 'acash', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogCash.type},
            //{title: '发起用户ID 0为平台', field: 'fromId', visible: true, align: 'center', valign: 'middle'},
            //{title: '目标用户ID 0为平台', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'cashType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 资金变动类型对应：1充值>2分享奖励>3商城货款>4待发现金转入>5提现驳回退还>6手动补发>7退款>
8退换手续费>9线下商家货款>10取消升级退换>11取消工单退换>12支付工单手续费>13重复支付退换>
31提现>32购买库存积分>33商城消费>34手动扣除>35用户升级>36提现手续费>37商家消费>38购买牌匾
 */
LogCash.type = function(value,row,index){
	if(value == 1){
		return "充值";
	}else if(value == 2){
		return "分享奖励";
	}else if(value == 3){
		return "商城货款";
	}else if(value == 4){
		return "待发现金转入";
	}else if(value == 5){
		return "提现驳回退还";
	}else if(value == 6){
		return "手动补发";
	}else if(value == 7){
		return "退款";
	}else if(value == 8){
		return "退换手续费";
	}else if(value == 9){
		return "线下商家货款";
	}else if(value == 10){
		return "取消升级退换";
	}else if(value == 11){
		return "取消工单退换";
	}else if(value == 12){
		return "支付工单手续费";
	}else if(value == 13){
		return "重复支付退换";
	}else if(value == 31){
		return "提现";
	}else if(value == 32){
		return "购买库存积分";
	}else if(value == 33){
		return "商城消费";
	}else if(value == 34){
		return "手动扣除";
	}else if(value == 35){
		return "用户升级";
	}else if(value == 36){
		return "提现手续费";
	}else if(value == 37){
		return "商家消费";
	}else if(value == 38){
		return "购买牌匾";
	}else if(value == 39){
		return "转化猫币";
	}
}
/**
 * 检查是否选中
 */
LogCash.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogCash.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加现金记录
 */
LogCash.openAddLogCash = function () {
    var index = layer.open({
        type: 2,
        title: '添加现金记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logCash/logCash_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看现金记录详情
 */
LogCash.openLogCashDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '现金记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logCash/logCash_update/' + LogCash.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除现金记录
 */
LogCash.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logCash/delete", function (data) {
            Feng.success("删除成功!");
            LogCash.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logCashId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询现金记录列表
 */
LogCash.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogCash.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogCash.initColumn();
    var table = new BSTable(LogCash.id, "/logCash/list/"+$('#userPhone2').val(), defaultColunms);
    table.setPaginationType("server");
    LogCash.table = table.init();
});
//导出Excel
LogCash.export= function(){
	$('#'+LogCash.id).tableExport({
		fileName:'现金记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}