/**
 * 商品管理初始化
 */
var Goods = {
    id: "GoodsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Goods.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '自增ID', field: 'goodsId', visible: false, align: 'center', valign: 'middle'},
        {title: '商品图片', field: 'goodsImg', visible: true, align: 'center', valign: 'middle', formatter:Goods.unq},
        {title: '商品名称', field: 'goodsName', visible: true, align: 'center', valign: 'middle'},
        {title: '商品编号', field: 'goodsSn', visible: true, align: 'center', valign: 'middle'},
        {title: '门店价', field: 'shopPrice', visible: true, align: 'center', valign: 'middle'},
        {title: '所属店铺', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
        {title: '商品分类路径', field: 'goodsCatIdPath', visible: true, align: 'center', valign: 'middle'},
        {title: '总销售量', field: 'saleNum', visible: true, align: 'center', valign: 'middle'},   
        {title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:Goods.open}
        
        /*{title: '自增ID', field: 'goodsId', visible: true, align: 'center', valign: 'middle'},
        {title: '商品货号', field: 'productNo', visible: true, align: 'center', valign: 'middle'},
        {title: '积分赠送比例', field: 'scoreRatio', visible: true, align: 'center', valign: 'middle'},
            {title: '市场价', field: 'marketPrice', visible: true, align: 'center', valign: 'middle'},
        
            {title: '自营店铺的商品支付的积分', field: 'payScore', visible: true, align: 'center', valign: 'middle'},
            {title: '预警库存', field: 'warnStock', visible: true, align: 'center', valign: 'middle'},
            {title: '商品总库存', field: 'goodsStock', visible: true, align: 'center', valign: 'middle'},
            {title: '单位', field: 'goodsUnit', visible: true, align: 'center', valign: 'middle'},
            {title: '促销信息', field: 'goodsTips', visible: true, align: 'center', valign: 'middle'},
            {title: '是否上架 0：不上架 1：上架', field: 'isSale', visible: true, align: 'center', valign: 'middle'},
            {title: '是否精品 0：否 1：是', field: 'isBest', visible: true, align: 'center', valign: 'middle'},
            {title: '是否热销产品 0：否 1：是', field: 'isHot', visible: true, align: 'center', valign: 'middle'},
            {title: '是否新品 0：否 1：是', field: 'isNew', visible: true, align: 'center', valign: 'middle'},
            {title: '是否推荐 0：否 1：是', field: 'isRecom', visible: true, align: 'center', valign: 'middle'},
            {title: '商品分类ID路径', field: 'goodsCatIdPath', visible: true, align: 'center', valign: 'middle'},
            {title: '最后一级商品分类ID', field: 'goodsCatId', visible: true, align: 'center', valign: 'middle'},
            {title: '门店商品分类第一级ID', field: 'shopCatId1', visible: true, align: 'center', valign: 'middle'},
            {title: '门店商品第二级分类ID', field: 'shopCatId2', visible: true, align: 'center', valign: 'middle'},
            {title: '品牌ID', field: 'brandId', visible: true, align: 'center', valign: 'middle'},
            {title: '商品描述', field: 'goodsDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '商品状态 -1：违规 0：未审核 1：已审核', field: 'goodsStatus', visible: true, align: 'center', valign: 'middle'},
         
            {title: '上架时间', field: 'saleTime', visible: true, align: 'center', valign: 'middle'},
            {title: '访问数', field: 'visitNum', visible: true, align: 'center', valign: 'middle'},
            {title: '评价数', field: 'appraiseNum', visible: true, align: 'center', valign: 'middle'},
            {title: '是否有规格 0：没有 1：有', field: 'isSpec', visible: true, align: 'center', valign: 'middle'},
            {title: '商品相册', field: 'gallery', visible: true, align: 'center', valign: 'middle'},
            {title: '商品SEO关键字', field: 'goodsSeoKeywords', visible: true, align: 'center', valign: 'middle'},
            {title: '状态说明(一般用于说明拒绝原因)', field: 'illegalRemarks', visible: true, align: 'center', valign: 'middle'},
            {title: '删除标志 -1：删除 1：有效', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '商品类型', field: 'goodsType', visible: true, align: 'center', valign: 'middle'},
            {title: '是否分销商品', field: 'isDistribut', visible: true, align: 'center', valign: 'middle'},
            {title: '分销佣金', field: 'commission', visible: true, align: 'center', valign: 'middle'},
            {title: '是否包邮', field: 'isFreeShipping', visible: true, align: 'center', valign: 'middle'}*/
    ];
};

Goods.open = function () {
	return "<a href='javascript:void(0)' onclick=\"Goods.showGoods()\">查看</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"Goods.openGoodsDetail2()\">审核通过</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"Goods.delete2()\">删除</a>"
}


/**
 * 检查是否选中
 */
Goods.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Goods.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品
 */
Goods.openAddGoods = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/goods/goods_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品详情
 */
Goods.openGoodsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goods/goods_update/' + Goods.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 打开审核通过
 */
Goods.openGoodsDetail3 = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '是否审核通过',
            area:['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goods/goods_update1/' + Goods.seItem.goodsId
        });
        this.layerIndex = index;
    }
};

//图片展示
Goods.unq = function(value,row,index){
    var image = value;
    const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
    if(image.indexOf('http')==-1){
        image=imgUrl+image
    }else{
        image=image
    }
    var img = '<a class = "view"  href="javascript:void(0)"><img style="width:70px;height:30px;"  src="'+image+'" /></a>';
    return img;
};

/**
 * 查看商品详情
 */
Goods.showGoods = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '查看商品详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goods/showGoods/' + Goods.seItem.goodsId
        });
        this.layerIndex = index;
    }
};
/**
 * 审核商品
 */
Goods.openGoodsDetail2 = function () {
    if (this.check()) {
        var operation = function(){
            var goodsId = Goods.seItem.goodsId;
            var ajax = new $ax(Feng.ctxPath + "/goods/update/", function (data) {
                Feng.success("操作成功!");
                Goods.table.refresh();
            }, function (data) {
                Feng.error("操作失败!" + data.responseJSON.message + "!");
            });
            ajax.set("goodsId",goodsId);
            ajax.set("goodsStatus",1) ;
            ajax.set("isSale",1)
            ajax.start();
        };

        Feng.confirm("是否审核通过该件商品?",operation);
    }
};
/**
 * 删除商品
 */
Goods.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/goods/delete", function (data) {
            Feng.success("删除成功!");
            Goods.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("goodsId",this.seItem.goodsId);
        ajax.start();
    }
};

/**
 * 删除商品
 */
Goods.delete2 = function () {
    if (this.check()) {
    	var operation = function(){
	    	 var goodsId = Goods.seItem.goodsId;
	        var ajax = new $ax(Feng.ctxPath + "/goods/delete", function (data) {
	            Feng.success("删除成功!");
	            Goods.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("goodsId",goodsId);
	        ajax.start();
	    };
	    Feng.confirm("是否删除该条记录?",operation);
    }
};
/**
 * 查询商品列表
 */
Goods.search = function () {
    var queryData = {};
    queryData['goodsName'] = $("#goodsName").val();
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['shopName'] = $("#shopName").val();
    queryData['goods1'] = $("#goods1").val();
    queryData['goods2'] = $("#goods2").val();
    queryData['goods3'] = $("#goods3").val();
    queryData['goodsStatus'] = -1;
    queryData['isSale'] = 0;
    Goods.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Goods.initColumn();
    var table = new BSTable(Goods.id, "/goods/list3", defaultColunms);
    table.setPaginationType("server");
    Goods.table = table.init();
});
