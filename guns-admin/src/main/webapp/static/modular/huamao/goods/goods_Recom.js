/**
 * 商品管理初始化
 */
var Goods = {
    id: "GoodsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

var Goods1 = {
	    id: "GoodsTable1",	//表格id
	    seItem: null,		//选中的条目
	    table: null,
	    layerIndex: -1
	};
/**
 * 初始化表格的列
 */
Goods.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '自增ID', field: 'goodsId', visible: false, align: 'center', valign: 'middle'},
        {title: '商品图片', field: 'goodsImg', visible: false, align: 'center', valign: 'middle'},
        {title: '商品名称', field: 'goodsName', visible: false, align: 'center', valign: 'middle'},
        {title: '商品编号', field: 'goodsSn', visible: false, align: 'center', valign: 'middle'},
        {title: '门店价', field: 'shopPrice', visible: false, align: 'center', valign: 'middle'},
        {title: '所属店铺', field: 'shopName', visible: false, align: 'center', valign: 'middle'},
        {title: '商品分类路径', field: 'goodsCatIdPath', visible: true, align: 'center', valign: 'middle'},
        {title: '总销售量', field: 'saleNum', visible: false, align: 'center', valign: 'middle'},  
        {title: '操作', field:'op', visible: false, align: 'center', valign: 'middle',formatter:Goods.op}
        
        /*{title: '自增ID', field: 'goodsId', visible: true, align: 'center', valign: 'middle'},
        {title: '商品货号', field: 'productNo', visible: true, align: 'center', valign: 'middle'},
        {title: '积分赠送比例', field: 'scoreRatio', visible: true, align: 'center', valign: 'middle'},
            {title: '市场价', field: 'marketPrice', visible: true, align: 'center', valign: 'middle'},
        
            {title: '自营店铺的商品支付的积分', field: 'payScore', visible: true, align: 'center', valign: 'middle'},
            {title: '预警库存', field: 'warnStock', visible: true, align: 'center', valign: 'middle'},
            {title: '商品总库存', field: 'goodsStock', visible: true, align: 'center', valign: 'middle'},
            {title: '单位', field: 'goodsUnit', visible: true, align: 'center', valign: 'middle'},
            {title: '促销信息', field: 'goodsTips', visible: true, align: 'center', valign: 'middle'},
            {title: '是否上架 0：不上架 1：上架', field: 'isSale', visible: true, align: 'center', valign: 'middle'},
            {title: '是否精品 0：否 1：是', field: 'isBest', visible: true, align: 'center', valign: 'middle'},
            {title: '是否热销产品 0：否 1：是', field: 'isHot', visible: true, align: 'center', valign: 'middle'},
            {title: '是否新品 0：否 1：是', field: 'isNew', visible: true, align: 'center', valign: 'middle'},
            {title: '是否推荐 0：否 1：是', field: 'isRecom', visible: true, align: 'center', valign: 'middle'},
            {title: '商品分类ID路径', field: 'goodsCatIdPath', visible: true, align: 'center', valign: 'middle'},
            {title: '最后一级商品分类ID', field: 'goodsCatId', visible: true, align: 'center', valign: 'middle'},
            {title: '门店商品分类第一级ID', field: 'shopCatId1', visible: true, align: 'center', valign: 'middle'},
            {title: '门店商品第二级分类ID', field: 'shopCatId2', visible: true, align: 'center', valign: 'middle'},
            {title: '品牌ID', field: 'brandId', visible: true, align: 'center', valign: 'middle'},
            {title: '商品描述', field: 'goodsDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '商品状态 -1：违规 0：未审核 1：已审核', field: 'goodsStatus', visible: true, align: 'center', valign: 'middle'},
         
            {title: '上架时间', field: 'saleTime', visible: true, align: 'center', valign: 'middle'},
            {title: '访问数', field: 'visitNum', visible: true, align: 'center', valign: 'middle'},
            {title: '评价数', field: 'appraiseNum', visible: true, align: 'center', valign: 'middle'},
            {title: '是否有规格 0：没有 1：有', field: 'isSpec', visible: true, align: 'center', valign: 'middle'},
            {title: '商品相册', field: 'gallery', visible: true, align: 'center', valign: 'middle'},
            {title: '商品SEO关键字', field: 'goodsSeoKeywords', visible: true, align: 'center', valign: 'middle'},
            {title: '状态说明(一般用于说明拒绝原因)', field: 'illegalRemarks', visible: true, align: 'center', valign: 'middle'},
            {title: '删除标志 -1：删除 1：有效', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '商品类型', field: 'goodsType', visible: true, align: 'center', valign: 'middle'},
            {title: '是否分销商品', field: 'isDistribut', visible: true, align: 'center', valign: 'middle'},
            {title: '分销佣金', field: 'commission', visible: true, align: 'center', valign: 'middle'},
            {title: '是否包邮', field: 'isFreeShipping', visible: true, align: 'center', valign: 'middle'}*/
    ];
};


/**
 * 操作
 * */

/**
 * 检查是否选中
 */
Goods.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Goods.seItem = selected[0];
        return true;
    }
};

Goods.op = function () {
	return "<a href='javascript:void(0)' onclick=\"#\">处理</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"Goods.openGoodsDetail2()\">违规下架</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"Goods.delete2()\">删除</a>"
}

/**
 * 点击添加商品
 */
Goods.openAddGoods = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/goods/goods_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品详情
 */
Goods.openGoodsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goods/goods_update/' + Goods.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 打开违规下架
 */
Goods.openGoodsDetail2 = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品违规原因',
            area:['100%', '100%'],//宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goods/goods_update/' + Goods.seItem.goodsId
        });
        this.layerIndex = index;
    }
};
/**
 * 删除商品
 */
Goods.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/goods/delete", function (data) {
            Feng.success("删除成功!");
            Goods.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("goodsId",this.seItem.goodsId);
        ajax.start();
    }
};

/**
 * 删除商品
 */
Goods.delete2 = function () {
    if (this.check()) {

        var operation = function(){
        	 var goodsId = Goods.seItem.goodsId;
        	var ajax = new $ax(Feng.ctxPath + "/goods/delete", function (data) {
                Feng.success("删除成功!");
                Goods.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("goodsId",goodsId);
            ajax.start();
        };

        Feng.confirm("是否删除该条记录?",operation);
    }
};
/**
 * 查询商品列表左侧
 */
Goods.search = function () {
	
   /* var queryData = {};
    queryData['goodsName'] = $("#goodsName").val();
   
    queryData['shopName'] = $("#shopName").val();
    queryData['goods1'] = $("#goods1").val();
    queryData['goods2'] = $("#goods2").val();
    queryData['goods3'] = $("#goods3").val();
    queryData['isSale'] = 1;
    queryData['goodsStatus'] = 1;
    Goods.table.refresh({query: queryData});*/
	var sb = [];
	var goodsName=$("#goodsName").val();
	var shopName=$("#shopName").val();
	var goods1=$("#goods1").val();
	var goods2=$("#goods2").val();
	var goods3=$("#goods3").val();
    if((goodsName == '' || typeof goodsName == 'undefined') && (shopName == '' || typeof shopName == 'undefined')){
        if(goods1 == '' || typeof goods1 == 'undefined'){
            Feng.success("请选择商品分类!");
            return ;
        }
    }

	var isSale=1;
	var goodsStatus=1;
	var isBest;
	var isHot;
	var isNew;
	var isRecom;
	var obj={
			goodsName:goodsName,
			shopName:shopName,
			goods1:goods1,
			goods2:goods2,
			goods3:goods3,
			isSale:isSale,
			goodsStatus:goodsStatus,
	}

	$.post("/recommends/selectNotRecomGoods",obj,function(result){
		list.splice(0,list.length);
		$.each(result, function(i,val){
			
			var obj1 = {
					name: val.goodsName,
					id: val.goodsId,
					isRecommend: '否',
                	dataSort:'',
			}
			list.push(obj1);
//			sb.push("<input type='checkbox' value='" + val.goodsId + "'>[" + val.shopName + "]"+val.goodsName+"/><br>");
		})
//		 $("#goodslist").after(sb.join(""));
	});
};


$(function () {
/*    var defaultColunms = Goods.initColumn();
    var table = new BSTable(Goods.goodsId, "/goods/list", defaultColunms);
    table.setPaginationType("client");
    Goods.table = table.init();*/
});

//右侧搜索
$("#style").change(function(){
	var goods1=$("#goods11").val();
	var goods2=$("#goods21").val();
	var goods3=$("#goods31").val();
	var style=$("#style").val();
	var isSale=1;
	var goodsStatus=1;
	var isBest;
	var isHot;
	var isNew;
	var isRecom;
	var obj={
//			goodsName:goodsName,
//			shopName:shopName,
			goods1:goods1,
			goods2:goods2,
			goods3:goods3,
			isSale:isSale,
			goodsStatus:goodsStatus,
        	style:style
	}

	$.post("/recommends/selectRecomGoods",obj,function(result){
		list2.splice(0,list2.length);
		$.each(result, function(i,val){
			var obj1 = {
					name: val.goodsName,
					id: val.goodsId,
					isRecommend: '是',
               		dataSort:val.dataSort
			}
			list2.push(obj1);
		})
		
	});
});
// 	var obje={
// //			goodsName:goodsName,
// //			shopName:shopName,
// 			goods1:goods1,
// 			goods2:goods2,
// 			goods3:goods3,
// 			isSale:isSale,
// 			goodsStatus:goodsStatus,
// 	}
//
// 	$.post("/recommends/selectRecomGoods",obje,function(result){
// 		list.splice(0,list.length);
// 		$.each(result, function(i,val){
// 			var obj = {
// 					name: val.goodsName,
// 					id: val.goodsId,
// 					isRecommend: '否'
//
//
// 			}
// 			list.push(obj);
// 		})
//
// 	});
	

/**
 * 更改推荐
 */
Goods.update = function () {
//	$.post("/goods/list",{
//		
//	}
}
