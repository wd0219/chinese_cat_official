/**
 * 工单列表管理初始化
 */
var SktWorkorder = {
    id: "SktWorkorderTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktWorkorder.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '申请人', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "修改推荐关系";
        			break;
        		case 2:
        			result = "修改手机号码";
        			break;
        		}
        		return result;
        	}},
            {title: '修改前的值', field: 'preValue', visible: true, align: 'center', valign: 'middle'},
            {title: '修改后的值', field: 'afterValue', visible: true, align: 'center', valign: 'middle'},
            {title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '状态 ', field: 'status', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 0:
        			result = "未支付";
        			break;
        		case 1:
        			result = "已付款";
        			break;
        		case 2:
        			result = "审核拒绝";
        			break;
        		case 3:
        			result = "审核通关";
        			break;
        		}
        		return result;
        	}},
            {title: '支付方式', field: 'payType', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "线下现金支付";
        			break;
        		case 2:
        			result = "现金账户支付";
        			break;
        		case 3:
        			result = "华宝支付";
        			break;
        		case 4:
        			result = "第三方支付";
        			break;
        		case 5:
        			result = "混合支付";
        			break;
        		case 6:
        			result = "华宝营业额";
        			break;
        		}
        		return result;
        	}},
            {title: '申请时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核时间', field: 'checkTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核人', field: 'checkStaffId', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'status', visible: true, align: 'center', valign: 'middle',formatter:SktWorkorder.op}
    ];
};
/*
 * 操作
 * */
SktWorkorder.op = function (value,ros,index) {
	if(value==1){
		return "<a href='javascript:void(0)' onclick=\"SktWorkorder.openSktWorkorderDetail2()\">处理</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktWorkorder.openSktWorkorderDetail()\">详情</a>"
	}else{
		return "<a href='javascript:void(0)'onclick=\"SktWorkorder.openSktWorkorderDetail()\">详情</a>"
	}
	
}
/**
 * 检查是否选中
 */
SktWorkorder.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktWorkorder.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加工单列表
 */
SktWorkorder.openAddSktWorkorder = function () {
    var index = layer.open({
        type: 2,
        title: '添加工单列表',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktWorkorder/sktWorkorder_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看工单列表详情
 */
SktWorkorder.openSktWorkorderDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '工单列表详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktWorkorder/detail/' + SktWorkorder.seItem.userId
        });
        this.layerIndex = index;
    }
};
/**
 * 打开查看工单列表详情
 */
SktWorkorder.openSktWorkorderDetail2 = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '工单列表详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktWorkorder/detail2/' + SktWorkorder.seItem.userId
        });
        this.layerIndex = index;
    }
};
/**
 * 删除工单列表
 */
SktWorkorder.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktWorkorder/delete", function (data) {
            Feng.success("删除成功!");
            SktWorkorder.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktWorkorderId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询工单列表列表
 */
SktWorkorder.search = function () {
    var queryData = {};
    queryData['userName'] = $("#userName").val();
    queryData['createTime'] = $("#createTime").val();
    queryData['checkTime'] = $("#checkTime").val();
    queryData['status'] = $("#status").val();
    queryData['type'] = $("#type").val();
    SktWorkorder.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktWorkorder.initColumn();
    var table = new BSTable(SktWorkorder.id, "/sktWorkorder/list", defaultColunms);
    table.setPaginationType("server");
    SktWorkorder.table = table.init();
});
