/**
 * 初始化工单列表详情对话框
 */
var SktWorkorderInfoDlg = {
    sktWorkorderInfoData : {}
};

/**
 * 清除数据
 */
SktWorkorderInfoDlg.clearData = function() {
    this.sktWorkorderInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktWorkorderInfoDlg.set = function(key, val) {
    this.sktWorkorderInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktWorkorderInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktWorkorderInfoDlg.close = function() {
    parent.layer.close(window.parent.SktWorkorder.layerIndex);
}

/**
 * 收集数据
 */
SktWorkorderInfoDlg.collectData = function() {
    this
    .set('orderId')
    .set('orderNo')
    .set('userId')
    .set('type')
    .set('preValue')
    .set('afterValue')
    .set('orderRemarks')
    .set('imgs')
    .set('payType')
    .set('totalMoney')
    .set('realMoney')
    .set('cash')
    .set('kaiyuan')
    .set('kaiyuanFee')
    .set('remarks')
    .set('createTime')
    .set('checkStaffId')
    .set('checkTime')
    .set('status')
    .set('dataFlag');
}

/**
 * 提交添加
 */
SktWorkorderInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktWorkorder/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktWorkorder.table.refresh();
        SktWorkorderInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktWorkorderInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktWorkorderInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktWorkorder/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktWorkorder.table.refresh();
        SktWorkorderInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktWorkorderInfoData);
    ajax.start();
}

$(function() {

});
