/**
 * 管理初始化
 */
var SktStaffs = {
    id: "SktStaffsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktStaffs.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '申请人', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '申请地区', field: 'areas', visible: true, align: 'center', valign: 'middle'},
            {title: '申请行业', field: 'hyName', visible: true, align: 'center', valign: 'middle'},
            {title: '法人', field: 'legalPerson', visible: true, align: 'center', valign: 'middle'},
            {title: '公司名称', field: 'companyName', visible: true, align: 'center', valign: 'middle'},
            {title: '企业类型', field: 'companyType', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		var result;
            		switch(value){
            		case 0:
            			result = "个体户";
            			break;
            		case 1:
            			result = "公司 ";
            			break;
            		}
            		return result;
            	}},
            {title: '经营地址', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '经营电话', field: 'telephone', visible: true, align: 'center', valign: 'middle'},
            {title: '申请时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'shopStatus', visible: true, align: 'center', valign: 'middle',
        	formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case -1:
        			result = "拒绝";
        			break;
        		case 0:
        			result = "未审核";
        			break;
        		case 1:
        			result = "已审核";
        			break;
                case 3:
                    result = "已审核";
                    break;
                }

        		return result;
        		
        	}},
            {title: '审核人', field: 'checkStaffName', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'shopStatus', visible: true, align: 'center', valign: 'middle',formatter:SktStaffs.op}
    ];
};
/*
 * 操作
 * */
SktStaffs.op = function (value,ros,index) {
	if(value==0){
	return "<a href='javascript:void(0)' onclick=\"SktStaffs.openSktStaffsDetail2()\">处理</a>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"SktStaffs.openSktStaffsDetail()\">详情</a>"
	}else{
	return "<a href='javascript:void(0)' onclick=\"SktStaffs.openSktStaffsDetail()\">详情</a>"
	}
}
/**
 * 检查是否选中
 */
SktStaffs.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktStaffs.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加联盟商家申请
 */
SktStaffs.openAddSktStaffs = function () {
    var index = layer.open({
        type: 2,
        title: '添加联盟商家申请',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktStaffs/sktStaffs_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看联盟商家申请详情
 */
SktStaffs.openSktStaffsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '联盟商家申请详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktStaffs/sktStaffs_update/' + SktStaffs.seItem.applyId
        });
        this.layerIndex = index;
    }
};
/**
 * 打开查看联盟商家申请详情
 */
SktStaffs.openSktStaffsDetail2 = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '联盟商家申请详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktStaffs/detail2/' + SktStaffs.seItem.applyId
        });
        this.layerIndex = index;
    }
};
/**
 * 删除联盟商家申请
 */
SktStaffs.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktStaffs/delete", function (data) {
            Feng.success("删除成功!");
            SktStaffs.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktStaffsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询联盟商家申请列表
 */
SktStaffs.search = function () {
    var queryData = {};
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['userName'] = $("#userName").val();
    queryData['companyName'] = $("#companyName").val();
    queryData['shopStatus'] = $("#shopStatus").val();
    queryData['companyType'] = $("#companyType").val();
    queryData['createTime'] = $("#createTime").val();
    queryData['checkTime'] = $("#checkTime").val();
    SktStaffs.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktStaffs.initColumn();
    var table = new BSTable(SktStaffs.id, "/sktStaffs/list", defaultColunms);
    table.setPaginationType("server");
    SktStaffs.table = table.init();
});
