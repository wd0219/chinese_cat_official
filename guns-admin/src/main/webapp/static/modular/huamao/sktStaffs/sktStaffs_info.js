/**
 * 初始化联盟商家申请详情对话框
 */
var SktStaffsInfoDlg = {
    sktStaffsInfoData : {}
};

/**
 * 清除数据
 */
SktStaffsInfoDlg.clearData = function() {
    this.sktStaffsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktStaffsInfoDlg.set = function(key, val) {
    this.sktStaffsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktStaffsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktStaffsInfoDlg.close = function() {
    parent.layer.close(window.parent.SktStaffs.layerIndex);
}

/**
 * 收集数据
 */
SktStaffsInfoDlg.collectData = function() {
    this
    .set('applyId')
    .set('staffId')
    .set('loginName')
    .set('loginPwd')
    .set('secretKey')
    .set('staffName')
    .set('staffNo')
    .set('staffPhoto')
    .set('staffRoleId')
    .set('workStatus')
    .set('staffStatus')
    .set('dataFlag')
    .set('createTime')
    .set('lastTime')
    .set('logo')
    .set('IDnumber')
    .set('legalPersonImg').set('companyName').set('licenseNo').set('companyType').set('licenseImg').set('shopName').set('address')
    .set('telephone')
    .set('applyStatus')
    .set('shopStatus')
    .set('statusDesc')
    .set('legalPerson');
}

/**
 * 提交添加
 */
SktStaffsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktStaffs/add", function(data){
        window.parent.SktStaffs.table.refresh();
        SktStaffsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktStaffsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktStaffsInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var legalPerson = $("#legalPerson").val();
    var IDnumber = $("#IDnumber").val();
    var licenseNo = $("#licenseNo").val();
    var legalPersonImg = $("#legalPersonImg").val();
    var companyName = $("#companyName").val();
    var licenseImg = $("#licenseImg").val();
    var shopName = $("#shopName").val();
    var address = $("#address").val();
    var telephone = $("#telephone").val();
    var logo = $("#logo").val();

    if(legalPerson == null || legalPerson ==""){
        var html='<span class="error">法人姓名不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#legalPerson').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(!isRealNum(IDnumber)){
        var html='<span class="error">法人证件号必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#IDnumber').after(html);
        falg = 1;
    }

    if(companyName == null || companyName ==""){
        var html='<span class="error">公司名称不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#companyName').after(html);
        falg = 1;
    }
    
    if(telephone == null || telephone ==""){
        var html='<span class="error">经营电话不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#telephone').after(html);
        falg = 1;
    }
    
    if(licenseImg == null || licenseImg ==""){
        var html='<span class="error">营业执照不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#licenseImg').after(html);
        falg = 1;
    }
    
    if(legalPersonImg == null || legalPersonImg ==""){
        var html='<span class="error">法人照片不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#legalPersonImg').after(html);
        falg = 1;
    }
    
    if(address == null || address ==""){
        var html='<span class="error">经营地址不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#address').after(html);
        falg = 1;
    }
    
    if(logo == null || logo ==""){
        var html='<span class="error">logo不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#logo').after(html);
        falg = 1;
    }
    
    if(shopName == null || shopName ==""){
        var html='<span class="error">店铺名称不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#shopName').after(html);
        falg = 1;
    }
    
    
    if(licenseNo == null || licenseNo ==""){
        var html='<span class="error">营业执照号码不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#licenseNo').after(html);
        falg = 1;
    }


    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktStaffs/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktStaffs.table.refresh();
        SktStaffsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktStaffsInfoData);
    ajax.start();
}else{
    Feng.success("请核实添加字段!");
}
}

function isRealNum(val){
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	if(val === "" || val ==null){
	    return false;
	}
	if(!isNaN(val)){
	    return true;
	}else{
	    return false;
	}
}

/**
 * 提交修改
 */
SktStaffsInfoDlg.editSubmit2 = function() {


    this.clearData();
    this.collectData();
    
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktStaffs/update2", function(data){
    	if(data=='2'){
    		Feng.success("该地区没有代理公司，请先添加代理公司!");
    	}else if(data==0){
    		 Feng.success("已有商铺或者修改失败!");
    	}else{
    		Feng.success("修改成功!");
            window.parent.SktStaffs.table.refresh();
            SktStaffsInfoDlg.close();
    	}
    },function(data){
        Feng.error("修改失败!");
    });
    this.sktStaffsInfoData['shopStatus']=$("input:radio[name=shopStatus]:checked").val();
    this.sktStaffsInfoData['statusDesc']=$("#statusDesc").val();
    ajax.set(this.sktStaffsInfoData);
    ajax.start();
	}


$(function() {
	var companyType = $('#companyType').val();
	switch(companyType){
	case '0':
		$('#companyType2').val("个体户");
		$('#companyType2').attr('disabled',true);
		break;
	case '1':
		$('#companyType2').val("公司");
		$('#companyType2').attr('disabled',true);
		break;
		default:
		break;
	}
	
	var shopStatus = $('#shopStatus').val();
	switch(shopStatus){
	case '1':
		$('#shopStatus2').val("已审核");
		$('#shopStatus2').attr('disabled',true);
		break;
	case '-1':
		$('#shopStatus2').val("拒绝");
		$('#shopStatus2').attr('disabled',true);
		break;
	case '0':
        $('#shopStatus2').val("未审核");
        $('#shopStatus2').attr('disabled',true);
        break;
	case '3':
		$('#shopStatus2').val("已审核");
		$('#shopStatus2').attr('disabled',true);
		break;
		default:
		break;
	}
	
	var licenseMerge = $('#licenseMerge').val();
	switch(licenseMerge){
	case '0':
		$('#licenseMerge2').val("未合并");
		$('#licenseMerge2').attr('disabled',true);
		break;
	case '1':
		$('#licenseMerge2').val("合并");
		$('#licenseMerge2').attr('disabled',true);
		break;
		default:
		break;
	}
	
	var isLegal = $('#isLegal').val();
	switch(isLegal){
	case '0':
		$('#isLegal2').val("不是");
		$('#isLegal2').attr('disabled',true);
		break;
	case '1':
		$('#isLegal2').val("是");
		$('#isLegal2').attr('disabled',true);
		break;
		default:
		break;
	}
	
	for(let e of document.getElementsByClassName('upload-btn')){
		let id = $(e).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
			uploader = new $WebUpload(id);
		uploader.setUploadBarId(id);
		uploader.init();
	}
});
	
