/**
 * 初始化华宝记录详情对话框
 */
var LogKaiyuanInfoDlg = {
    logKaiyuanInfoData : {}
};

/**
 * 清除数据
 */
LogKaiyuanInfoDlg.clearData = function() {
    this.logKaiyuanInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogKaiyuanInfoDlg.set = function(key, val) {
    this.logKaiyuanInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogKaiyuanInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogKaiyuanInfoDlg.close = function() {
    parent.layer.close(window.parent.LogKaiyuan.layerIndex);
}

/**
 * 收集数据
 */
LogKaiyuanInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preKaiyuan')
    .set('kaiyuanType')
    .set('kaiyuan')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogKaiyuanInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logKaiyuan/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogKaiyuan.table.refresh();
        LogKaiyuanInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logKaiyuanInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogKaiyuanInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logKaiyuan/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogKaiyuan.table.refresh();
        LogKaiyuanInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logKaiyuanInfoData);
    ajax.start();
}

$(function() {

});
