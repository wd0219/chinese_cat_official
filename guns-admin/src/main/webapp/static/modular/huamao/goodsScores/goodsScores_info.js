/**
 * 初始化商品评分详情对话框
 */
var GoodsScoresInfoDlg = {
    goodsScoresInfoData : {}
};

/**
 * 清除数据
 */
GoodsScoresInfoDlg.clearData = function() {
    this.goodsScoresInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsScoresInfoDlg.set = function(key, val) {
    this.goodsScoresInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsScoresInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
GoodsScoresInfoDlg.close = function() {
    parent.layer.close(window.parent.GoodsScores.layerIndex);
}

/**
 * 收集数据
 */
GoodsScoresInfoDlg.collectData = function() {
    this
    .set('scoreId')
    .set('goodsId')
    .set('shopId')
    .set('totalScore')
    .set('totalUsers')
    .set('goodsScore')
    .set('goodsUsers')
    .set('serviceScore')
    .set('serviceUsers')
    .set('timeScore')
    .set('timeUsers');
}

/**
 * 提交添加
 */
GoodsScoresInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsScores/add", function(data){
        Feng.success("添加成功!");
        window.parent.GoodsScores.table.refresh();
        GoodsScoresInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsScoresInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
GoodsScoresInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsScores/update", function(data){
        Feng.success("修改成功!");
        window.parent.GoodsScores.table.refresh();
        GoodsScoresInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsScoresInfoData);
    ajax.start();
}

$(function() {

});
