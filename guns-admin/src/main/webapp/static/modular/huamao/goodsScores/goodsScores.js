/**
 * 商品评分管理初始化
 */
var GoodsScores = {
    id: "GoodsScoresTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
GoodsScores.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '商品Id', field: 'goodsId', visible: true, align: 'center', valign: 'middle'},
            {title: '门店ID', field: 'shopId', visible: true, align: 'center', valign: 'middle'},
            {title: '总评分', field: 'totalScore', visible: true, align: 'center', valign: 'middle'},
            {title: '总评评分用户数', field: 'totalUsers', visible: true, align: 'center', valign: 'middle'},
            {title: '商品评分', field: 'goodsScore', visible: true, align: 'center', valign: 'middle'},
            {title: '商品评分用户数', field: 'goodsUsers', visible: true, align: 'center', valign: 'middle'},
            {title: '服务评分', field: 'serviceScore', visible: true, align: 'center', valign: 'middle'},
            {title: '服务评分用户数', field: 'serviceUsers', visible: true, align: 'center', valign: 'middle'},
            {title: '时效评分', field: 'timeScore', visible: true, align: 'center', valign: 'middle'},
            {title: '时效评分用户数', field: 'timeUsers', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
GoodsScores.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        GoodsScores.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品评分
 */
GoodsScores.openAddGoodsScores = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品评分',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/goodsScores/goodsScores_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品评分详情
 */
GoodsScores.openGoodsScoresDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品评分详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goodsScores/goodsScores_update/' + GoodsScores.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品评分
 */
GoodsScores.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/goodsScores/delete", function (data) {
            Feng.success("删除成功!");
            GoodsScores.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("goodsScoresId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商品评分列表
 */
GoodsScores.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    GoodsScores.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = GoodsScores.initColumn();
    var table = new BSTable(GoodsScores.id, "/goodsScores/list", defaultColunms);
    table.setPaginationType("client");
    GoodsScores.table = table.init();
});
