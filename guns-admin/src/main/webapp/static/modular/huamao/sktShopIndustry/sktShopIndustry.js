/**
 * 行业管理初始化
 */
var SktShopIndustry = {
    id: "SktShopIndustryTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktShopIndustry.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '行业名称', field: 'hyName', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "显示";	
        			break;
        		case 0:
        			result = "隐藏";
        			break;
        		}
        		return result;
        	}},
            {title: '是否常用', field: 'hot', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
            		var result;
            		switch(value){
            		case 1:
            			result = "常用";
            			break;
            		case 2:
            			result = "不常用";
            			break;
            		}
            		return result;
            	}},
            {title: '首字母', field: 'initial', visible: true, align: 'center', valign: 'middle'},
            {title: '图标', field: 'icon', visible: true, align: 'center', valign: 'middle', formatter:SktShopIndustry.showIcon},
            {title: '排序', field: 'sort', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',formatter:SktShopIndustry.op}
    ];
};
/*
 * 操作
 * */
SktShopIndustry.op = function () {
	return "<a href='javascript:void(0)' onclick=\"SktShopIndustry.openSktShopIndustryDetail()\">修改</a>"
}
/**
 * 检查是否选中
 */
SktShopIndustry.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktShopIndustry.seItem = selected[0];
        return true;
    }
};

//展示图标
SktShopIndustry.showIcon = function(value,row,index){
    const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
    var s;
    if(row.icon!=null){
        var url = row.icon;
        if(url.indexOf('http')==-1){
            url=imgUrl+url
        }else{
            url=url
        }
        //var i = row.companyImage.indexOf("webapps");
        //var url = row.companyImage.substring(i+7,row.companyImage.length);
        //var url = 'F:\idnex.jpg';
        s = '<a class = "view"  href="javascript:void(0)"><img style="width:100;height:40px;"  src='+url+' /></a>';
    }
    return s;

}

/**
 * 点击添加行业管理
 */
SktShopIndustry.openAddSktShopIndustry = function () {
    var index = layer.open({
        type: 2,
        title: '添加行业管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktShopIndustry/sktShopIndustry_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看行业管理详情
 */
SktShopIndustry.openSktShopIndustryDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '行业管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktShopIndustry/sktShopIndustry_update/' + SktShopIndustry.seItem.hyId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除行业管理
 */
SktShopIndustry.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktShopIndustry/delete", function (data) {
            Feng.success("删除成功!");
            SktShopIndustry.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktShopIndustryId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询行业管理列表
 */
SktShopIndustry.search = function () {
    var queryData = {};
    queryData['hot'] = $("#hot").val();
    SktShopIndustry.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktShopIndustry.initColumn();
    var table = new BSTable(SktShopIndustry.id, "/sktShopIndustry/list", defaultColunms);
    table.setPaginationType("server");
    SktShopIndustry.table = table.init();
});
