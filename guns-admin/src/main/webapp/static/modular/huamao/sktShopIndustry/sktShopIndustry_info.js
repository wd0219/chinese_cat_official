/**
 * 初始化行业管理详情对话框
 */
var SktShopIndustryInfoDlg = {
    sktShopIndustryInfoData : {}
};

/**
 * 清除数据
 */
SktShopIndustryInfoDlg.clearData = function() {
    this.sktShopIndustryInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShopIndustryInfoDlg.set = function(key, val) {
    this.sktShopIndustryInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShopIndustryInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktShopIndustryInfoDlg.close = function() {
    parent.layer.close(window.parent.SktShopIndustry.layerIndex);
}

/**
 * 收集数据
 */
SktShopIndustryInfoDlg.collectData = function() {
    this
    .set('hyId')
    .set('hyName')
    .set('status')
    .set('hot')
    .set('initial')
    .set('icon')
    .set('sort');
}

/**
 * 提交添加
 */
SktShopIndustryInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var hyName = $("#hyName").val();
    var icon = $("#icon").val();
    var initial = $("#initial").val();
    var sort = $("#sort").val();

    if(hyName == null || hyName ==""){
        var html='<span class="error">行业名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#hyName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(icon == null || icon ==""){
        var html='<span class="error">图片不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#icon').after(html);
        falg = 1;
    }

    if(!isRealNum(sort)){
        var html='<span class="error">排序必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#sort').after(html);
        falg = 1;
    }

    if(initial == null || initial ==""){
        var html='<span class="error">首字母不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#initial').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    
    let
    status = $('input:radio[name=status]:checked').val();
    let
    hot = $('input:radio[name=hot]:checked').val();
    
    this.sktShopIndustryInfoData.status = status;
    this.sktShopIndustryInfoData.hot = hot;
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktShopIndustry/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktShopIndustry.table.refresh();
        SktShopIndustryInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShopIndustryInfoData);
    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
	}

	function isRealNum(val){
	    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	    if(val === "" || val ==null){
	        return false;
	    }
	    if(!isNaN(val)){
	        return true;
	    }else{
	        return false;
	    }
	}

/**
 * 提交修改
 */
SktShopIndustryInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var hyName = $("#hyName").val();
    var icon = $("#icon").val();
    var initial = $("#initial").val();
    var sort = $("#sort").val();

    if(hyName == null || hyName ==""){
        var html='<span class="error">行业名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#hyName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(icon == null || icon ==""){
        var html='<span class="error">图片不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#icon').after(html);
        falg = 1;
    }

    if(!isRealNum(sort)){
        var html='<span class="error">排序必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#sort').after(html);
        falg = 1;
    }

    if(initial == null || initial ==""){
        var html='<span class="error">首字母不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#initial').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    
    let
    status = $('input:radio[name=status]:checked').val();
    let
    hot = $('input:radio[name=hot]:checked').val();
    
    
    
    this.sktShopIndustryInfoData.status = status;
    this.sktShopIndustryInfoData.hot = hot;

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktShopIndustry/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktShopIndustry.table.refresh();
        SktShopIndustryInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShopIndustryInfoData);
    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
}

$(function() {
	for(let e of document.getElementsByClassName('upload-btn')){
		let id = $(e).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
			uploader = new $WebUpload(id);
		uploader.setUploadBarId(id);
		uploader.init();
	}
}
);
