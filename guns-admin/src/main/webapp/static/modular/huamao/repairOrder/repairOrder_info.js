/**
 * 初始化第三方支付补单记录详情对话框
 */
var RepairOrderInfoDlg = {
    repairOrderInfoData : {}
};

/**
 * 清除数据
 */
RepairOrderInfoDlg.clearData = function() {
    this.repairOrderInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RepairOrderInfoDlg.set = function(key, val) {
    this.repairOrderInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RepairOrderInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RepairOrderInfoDlg.close = function() {
    parent.layer.close(window.parent.RepairOrder.layerIndex);
}

/**
 * 收集数据
 */
RepairOrderInfoDlg.collectData = function() {
    this
    .set('id')
    .set('userId')
    .set('orderNo')
    .set('outTradeNo')
    .set('businessType')
    .set('payType')
    .set('createTime')
    .set('operateId');
}

/**
 * 提交添加
 */
RepairOrderInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/repairOrder/add", function(data){
        Feng.success("添加成功!");
        window.parent.RepairOrder.table.refresh();
        RepairOrderInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.repairOrderInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
RepairOrderInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/repairOrder/update", function(data){
        Feng.success("修改成功!");
        window.parent.RepairOrder.table.refresh();
        RepairOrderInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.repairOrderInfoData);
    ajax.start();
}

$(function() {

});
