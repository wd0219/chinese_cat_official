/**
 * 第三方支付补单记录管理初始化
 */
var RepairOrder = {
    id: "RepairOrderTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
RepairOrder.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '内部业务订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '第三方流水号', field: 'outTradeNo', visible: true, align: 'center', valign: 'middle'},
            {title: '业务类型', field: 'businessType', visible: true, align: 'center', valign: 'middle',formatter:RepairOrder.businessType},
            {title: '支付类型', field: 'payType', visible: true, align: 'center', valign: 'middle',formatter:RepairOrder.payType},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作人ID', field: 'operateId', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 业务类型 BUY_GOODS:商城消费 BUY_SCORE:购买积分
 * RECHARGE:充值 UPGRADE:升级 BUY_PLAQUE：购买牌匾 WORK_ORDER：工单
 */
RepairOrder.businessType = function(value,row,index){
	if(value == "BUY_GOODS"){
		return "商城消费";
	}else if(value == "BUY_SCORE"){
		return "购买积分";
	}else if(value == "RECHARGE"){
		return "充值";
	}else if(value == "UPGRADE"){
		return "升级";
	}else if(value == "BUY_PLAQUE"){
		return "购买牌匾";
	}else if(value == "WORK_ORDER"){
		return "工单";
	}
}
/**
 * 支付类型（1：微信；2：支付宝；3：快捷；4：网关）
 */
RepairOrder.payType = function(value,row,index){
	if(value == 1){
		return "微信";
	}else if(value == 2){
		return "支付宝";
	}else if(value == 3){
		return "快捷";
	}else if(value == 4){
		return "网关";
	}
}
/**
 * 检查是否选中
 */
RepairOrder.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        RepairOrder.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加第三方支付补单记录
 */
RepairOrder.openAddRepairOrder = function () {
    var index = layer.open({
        type: 2,
        title: '添加第三方支付补单记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/repairOrder/repairOrder_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看第三方支付补单记录详情
 */
RepairOrder.openRepairOrderDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '第三方支付补单记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/repairOrder/repairOrder_update/' + RepairOrder.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除第三方支付补单记录
 */
RepairOrder.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/repairOrder/delete", function (data) {
            Feng.success("删除成功!");
            RepairOrder.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("repairOrderId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询第三方支付补单记录列表
 */
RepairOrder.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['orderNo'] = $("#orderNo").val();
    queryData['userPhone'] = $("#userPhone").val();
    queryData['businessType'] = $("#businessType").val();
    RepairOrder.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = RepairOrder.initColumn();
    var table = new BSTable(RepairOrder.id, "/repairOrder/list", defaultColunms);
    table.setPaginationType("client");
    RepairOrder.table = table.init();
});
//导出Excel
RepairOrder.export= function(){
	$('#'+RepairOrder.id).tableExport({
		fileName:'第三方支付补单记录',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}
