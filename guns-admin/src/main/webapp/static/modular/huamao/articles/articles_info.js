/**
 * 初始化文章管理详情对话框
 */
var ArticlesInfoDlg = {
    articlesInfoData : {}
};

/**
 * 清除数据
 */
ArticlesInfoDlg.clearData = function() {
    this.articlesInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ArticlesInfoDlg.set = function(key, val) {
    this.articlesInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ArticlesInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ArticlesInfoDlg.close = function() {
    parent.layer.close(window.parent.Articles.layerIndex);
}

/**
 * 收集数据		
 */
ArticlesInfoDlg.collectData = function() {
//	 var queryData = {};
//	    queryData['articleId'] = $("#articleId").val();
//	    queryData['catId'] = $("#catId").val();
//	    queryData['articleTitle'] = $("#articleTitle").val();
//	    queryData['isShow'] = $("#isShow").val();
//	    queryData['articleContent'] = CKEDITOR.instances.articleContent.getData();
//	    queryData['articleKey'] = $("#articleKey").val();
//	    queryData['staffId'] = $("#staffId").val();
//	    queryData['status'] = $("#status").val();
//	    queryData['dataFlag'] = $("#dataFlag").val();
//	    queryData['createTime'] = $("#createTime").val();
//	    queryData['solve'] = $("#solve").val();
//	    queryData['unsolve'] = $("#unsolve").val();
//	    queryData['article1'] = $("#article1").val();
//	    queryData['article2'] = $("#article2").val();
//	    queryData['article3'] = $("#article3").val();
//	    Articles.table.refresh({query: queryData});
    //获取富文本框中的内容
    var text2 = editor.txt.html();

    //转码
    var articleContent2 = HtmlUtil.htmlEncode(text2);
	$("#articleContent").html(articleContent2);
    this
    .set('articleId')
    .set('catId')
    .set('articleTitle')
    .set('isShow')
    .set('articleContent')
    .set('articleKey')
    .set('staffId')
    .set('status')
    .set('dataFlag')
    .set('createTime')
    .set('solve')
    .set('unsolve')
    .set('article1')
    .set('article2')
    .set('article3')
    ;


}

/**
 * 提交添加
 */
ArticlesInfoDlg.addSubmit = function() {
//	alert(CKEDITOR.instances.articleContent.getData());
	 $('.col-sm-9 span').html("");
	    var falg = 0;

	    var article1 = $("#article1").val();
	    var articleTitle = $("#articleTitle").val();
	    var articleKey = $("#articleKey").val();
    //获取富文本框中的内容
    var text22 = editor.txt.html();

    //转码
    var articleContent = HtmlUtil.htmlEncode(text22);

	    if(article1 == null || article1 ==""){
	        var html='<span class="error">所属分类不能为空!</span>';
	        //$('#menuName'.parent()).append(html);
	        $('#article1').after(html);
	        falg = 1;
	    }else {
	        var html = '';
	    }

	    if(articleTitle == null || articleTitle ==""){
	        var html='<span class="error">文章标题不能为空!</span>';
	        //$('#menuName'.parent()).append(html);
	        $('#articleTitle').after(html);
	        falg = 1;
	    }else {
	        var html = '';
	    }

	    if(articleKey == null || articleKey ==""){
	        var html='<span class="error">关键字不能为空!</span>';
	        //$('#menuName'.parent()).append(html);
	        $('#articleKey').after(html);
	        falg = 1;
	    }else {
	        var html = '';
	    }

	    if(articleContent == null || articleContent ==""){
	        var html='<span class="error">文章内容不能为空!</span>';
	        //$('#menuName'.parent()).append(html);
	        $('#articleContent').after(html);
	        falg = 1;
	    }else {
	        var html = '';
	    }


    this.clearData();
    this.collectData();
//获取富文本框中的内容
    var text2 = editor.txt.html();

    //转码
    var articleContent2 = HtmlUtil.htmlEncode(text2);
    this.articlesInfoData.articleContent = articleContent2;
    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/articles/add", function(data){
	        Feng.success("添加成功!");
	        window.parent.Articles.table.refresh();
	        ArticlesInfoDlg.close();
	    },function(data){
	        Feng.error("添加失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.articlesInfoData);
	    ajax.start();
		}else{
		    Feng.success("请核实添加字段!");
		}
	}

	function isRealNum(val){
		// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
		if(val === "" || val ==null){
		    return false;
		}
		if(!isNaN(val)){
		    return true;
		}else{
		    return false;
		}
	}

/**
 * 提交修改
 */
ArticlesInfoDlg.editSubmit = function() {
	
	 $('.col-sm-9 span').html("");
	    var falg = 0;

	    var article1 = $("#article1").val();
	    var articleTitle = $("#articleTitle").val();
	    var articleKey = $("#articleKey").val();
	    //var articleContent = $("#articleContent").val();
		//获取富文本框中的内容
		var text = editor.txt.html();

		//转码
		var articleContent = HtmlUtil.htmlEncode(text);

	    if(article1 == null || article1 ==""){
	        var html='<span class="error">所属分类不能为空!</span>';
	        //$('#menuName'.parent()).append(html);
	        $('#article1').after(html);
	        falg = 1;
	    }else {
	        var html = '';
	    }

	    if(articleTitle == null || articleTitle ==""){
	        var html='<span class="error">文章标题不能为空!</span>';
	        //$('#menuName'.parent()).append(html);
	        $('#articleTitle').after(html);
	        falg = 1;
	    }else {
	        var html = '';
	    }
	    
	    if(articleKey == null || articleKey ==""){
	        var html='<span class="error">关键字不能为空!</span>';
	        //$('#menuName'.parent()).append(html);
	        $('#articleKey').after(html);
	        falg = 1;
	    }else {
	        var html = '';
	    }
	    
	    if(articleContent == null || articleContent ==""){
	        var html='<span class="error">文章内容不能为空!</span>';
	        //$('#menuName'.parent()).append(html);
	        $('#articleContent').after(html);
	        falg = 1;
	    }else {
            var html = '';
	    }

    this.clearData();
    this.collectData();
    let isShow = $('input:radio[name=isShow]:checked').val();
    this.articlesInfoData.isShow=isShow;

    //获取富文本框中的内容
    var text2 = editor.txt.html();

    //转码
    var articleContent2 = HtmlUtil.htmlEncode(text2);
    this.articlesInfoData.articleContent = articleContent2;

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/articles/update", function(data){
	        Feng.success("修改成功!");
	        window.parent.Articles.table.refresh();
	        ArticlesInfoDlg.close();
	    },function(data){
	        Feng.error("修改失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.articlesInfoData);
	    ajax.start();
		}else{
		    Feng.success("请核实添加字段!");
		}
	}



var articleCatsData;

$.ajax({
	type:"get",

    url:"/articleCats/selectByParentId?parentId=0", //必选。规定吧请求发送到那个URL。

    data:null,//可选。映射或字符串值。规定连同请求发送到服务器的数据。
    async:false,
    success:function(result){
    	articleCatsData = result;
  	 
    }, //可选，请求成功时执行的回调函数。

    datatype:"json"//可选。规定预期的服务器响应的数据类型。默认执行职能判断(xml,json,script 或html)

  });

 $(function() {
	var sb =[];
	  $.each(articleCatsData,
	    function(i, val) {
	        sb.push("<option id= '" + val.catId + "' value='" + val.catId + "'>" + val.catName + "</option>");
	    });
	  $("#articleCats1").after(sb.join(""));
	 
	  
	  
 });
//一级变化时处理二级
 $("#article1").change(function(){
	 if($("#article1").val()==""){
		  return;
	  } 
 //function dogoodscat1() {
	 var articleCatsData2;
	 var article1=$("#article1").val();
	 
	  var article2 = $("#article2");
	  var article3 = $("#article3");
	  if (article2.children().length > 1) {
			 
		  article2.empty();
		  }
		  if (article3.children().length > 1) {
			  
			  article3.empty();
		  }
		  if ($("#articleCats2").length === 0) {
			  article2.append("<option id='articleCats2' value=''>请选择</option>");
		  }
		  if ($("#articleCats3").length === 0) {
			  article3.append("<option id='articleCats3' value=''>请选择</option>");
		  }
	  var sb = [];
	 
	  $.ajax({
            type:"get",

		    url:"/articleCats/selectByParentId?parentId="+article1, //必选。规定吧请求发送到那个URL。

		    data:null,//可选。映射或字符串值。规定连同请求发送到服务器的数据。
		    async:false,
		    success:function(result){
		    	articleCatsData2 = result;
		  	 
		    }, //可选，请求成功时执行的回调函数。

		    datatype:"json"//可选。规定预期的服务器响应的数据类型。默认执行职能判断(xml,json,script 或html)

		  });
	  $.each(articleCatsData2,
	    function(i, val) {
	     
	        sb.push("<option value='" + val.catId + "'>" + val.catName + "</option>");
	      
	    });
	  $("#articleCats2").after(sb.join(""));
	});
 //二级变化时处理三级
 $("#article2").change(function(){
	 if($("#article2").val()==""){
		  return;
	  } 
	 var articleCatsData3;
	 var article2=$("#article2").val();
	 var article3 = $("#article3");
	  if (article3.children().length > 1) {
		  article3.empty();
	  }
	  if ($("#articleCats3").length === 0) {
		  article3.append("<option id='articleCats3' value=''>请选择</option>");
	  }
	  var sb =[];
	  
	  $.ajax({

		    type:"get",

		    url:"/articleCats/selectByParentId?parentId="+article2, //必选。规定吧请求发送到那个URL。

		    data:null,//可选。映射或字符串值。规定连同请求发送到服务器的数据。
		    async:false,
		    success:function(result){
		    	articleCatsData3 = result;
		  	 
		    }, //可选，请求成功时执行的回调函数。

		    datatype:"json"//可选。规定预期的服务器响应的数据类型。默认执行职能判断(xml,json,script 或html)

		  });
	  $.each(articleCatsData3,
	    function(i, val) {
	     
	        sb.push("<option value='" + val.catId + "'>" + val.catName + "</option>");
	      
	    });
	  $("#articleCats3").after(sb.join(""));
	});

$(function(){
//	if($('#parentId').val()!=0){
//		$('#parentId').val($('#article1').val());
//		$('#catId').val($('#article2').val());
//	}
	if($('#parentId').val()!='0'){
		$('#article1').val($('#parentId').val());
		if($('#article1').val()!=0){
			$('#article1').val($('#parentId').val());
			$('#article1').trigger("change");
		}
		if($('#catId').val()!=0){
			$('#article2').val($('#catId').val());
			$('#article2').trigger("change");
		}
//		if($('#areaId').val()!=0){
//			$('#county').val($('#areaId').val());
//			$('#county').trigger("change");
//		}
	}else{
		if($('#catId').val()!=0){
			$('#article1').val($('#catId').val());
			$('#article1').trigger("change");
		}
		
		
	}
//	if($('#parentId').val()){
//		$('#parentId').val($('#article1').val());
//		if($('#articleCats1').val()!=0){
//			$('#province').val($('#provinceId').val());
//			$('#province').trigger("change");
//		}
//		if($('#cityId').val()!=0){
//			$('#citys').val($('#cityId').val());
//			$('#citys').trigger("change");
//		}
//		if($('#areaId').val()!=0){
//			$('#county').val($('#areaId').val());
//			$('#county').trigger("change");
//		}
//	}
	 for(let e of document.getElementsByClassName('upload-btn')){
			let id = $(e).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
			uploader = new $WebUpload(id);
			uploader.setUploadBarId(id);
			uploader.init();
		}
	// 初始化头像上传
 /*var avatarUp = new $WebUpload("brandImg");
 avatarUp.setUploadBarId("progressBar");
 avatarUp.init();*/
//  BrandsInfoDlg.getBrandImg();
})

var HtmlUtil = {
    /*1.用浏览器内部转换器实现html转码*/
    htmlEncode:function (html){
        //1.首先动态创建一个容器标签元素，如DIV
        var temp = document.createElement ("div");
        //2.然后将要转换的字符串设置为这个元素的innerText(ie支持)或者textContent(火狐，google支持)
        (temp.textContent != undefined ) ? (temp.textContent = html) : (temp.innerText = html);
        //3.最后返回这个元素的innerHTML，即得到经过HTML编码转换的字符串了
        var output = temp.innerHTML;
        temp = null;
        return output;
    },
    /*2.用浏览器内部转换器实现html解码*/
    htmlDecode:function (text){
        //1.首先动态创建一个容器标签元素，如DIV
        var temp = document.createElement("div");
        //2.然后将要转换的字符串设置为这个元素的innerHTML(ie，火狐，google都支持)
        temp.innerHTML = text;
        //3.最后返回这个元素的innerText(ie支持)或者textContent(火狐，google支持)，即得到经过HTML解码的字符串了。
        var output = temp.innerText || temp.textContent;
        temp = null;
        return output;
    }
};

