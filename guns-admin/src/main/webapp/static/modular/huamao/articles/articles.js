/**
 * 文章管理管理初始化
 */
var Articles = {
    id: "ArticlesTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Articles.initColumn = function () {
    return [
	        {field: 'selectItem', radio: true},
	        {
            	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
	        {title: '文章标题', field: 'articleTitle', visible: true, align: 'center', valign: 'middle'},
	        {title: '分类', field: 'catName', visible: true, align: 'center', valign: 'middle'},
            {title: '是否显示', field: 'isShow', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
               		var result;
               		switch(value){
               		case 1:
               			result = "显示";
               			break;
               		case 0:
               			result = "不显示 ";
               			break;
               		}

               		return result;
            }},
            {title: '创建者', field: 'account', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:Articles.open}
            
            
           /* {title: '自增ID', field: 'articleId', visible: true, align: 'center', valign: 'middle'},
            {title: '文章内容', field: 'articleContent', visible: true, align: 'center', valign: 'middle'},
            {title: '关键字', field: 'articleKey', visible: true, align: 'center', valign: 'middle'},       
            {title: '状态 0未读 1已读', field: 'status', visible: true, align: 'center', valign: 'middle'},
            {title: '有效状态', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '觉得文章有帮助的次数', field: 'solve', visible: true, align: 'center', valign: 'middle'},
            {title: '觉得文章没帮助的次数', field: 'unsolve', visible: true, align: 'center', valign: 'middle'}
    */];
};

Articles.open = function () {
	return "<a href='javascript:void(0)' onclick=\"Articles.openArticlesDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"Articles.delete2()\">删除</a>"
}
/**
 * 检查是否选中
 */
Articles.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Articles.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加文章管理
 */
Articles.openAddArticles = function () {
	
    var index = layer.open({
        type: 2,
        title: '添加文章管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/articles/articles_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看文章管理详情
 */
Articles.openArticlesDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '文章管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/articles/articles_update/' + Articles.seItem.articleId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除文章管理
 */
Articles.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/articles/delete", function (data) {
            Feng.success("删除成功!");
            Articles.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("articlesId",this.seItem.id);
        ajax.start();
    }
};


/**
 * 删除文章管理
 */
Articles.delete2 = function () {
    if (this.check()) {
	   	 var operation = function(){
	   		 var articlesId=Articles.seItem.articleId;
	   		 var ajax = new $ax(Feng.ctxPath + "/articles/delete", function (data) {
	            Feng.success("删除成功!");
	            Articles.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("articlesId",articlesId);
	        ajax.start();
	    };
	    Feng.confirm("是否删除 该条记录?",operation);
    }
};
/**
 * 查询文章管理列表
 */
Articles.search = function () {
    var queryData = {};
    queryData['articleTitle'] = $("#articleTitle").val();
    Articles.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Articles.initColumn();
    var table = new BSTable(Articles.id, "/articles/list", defaultColunms);
    table.setPaginationType("server");
    Articles.table = table.init();
});
