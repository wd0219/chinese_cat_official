/**
 * 管理初始化
 */
var SktFavorites = {
    id: "SktFavoritesTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktFavorites.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '收藏类型', field: 'favoriteType', visible: true, align: 'center', valign: 'middle'},
            {title: '对象ID', field: 'targetId', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktFavorites.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktFavorites.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
SktFavorites.openAddSktFavorites = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktFavorites/sktFavorites_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
SktFavorites.openSktFavoritesDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktFavorites/sktFavorites_update/' + SktFavorites.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
SktFavorites.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktFavorites/delete", function (data) {
            Feng.success("删除成功!");
            SktFavorites.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktFavoritesId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
SktFavorites.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktFavorites.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktFavorites.initColumn();
    var table = new BSTable(SktFavorites.id, "/sktFavorites/list", defaultColunms);
    table.setPaginationType("client");
    SktFavorites.table = table.init();
});
