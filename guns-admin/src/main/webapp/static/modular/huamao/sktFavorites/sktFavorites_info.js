/**
 * 初始化详情对话框
 */
var SktFavoritesInfoDlg = {
    sktFavoritesInfoData : {}
};

/**
 * 清除数据
 */
SktFavoritesInfoDlg.clearData = function() {
    this.sktFavoritesInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktFavoritesInfoDlg.set = function(key, val) {
    this.sktFavoritesInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktFavoritesInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktFavoritesInfoDlg.close = function() {
    parent.layer.close(window.parent.SktFavorites.layerIndex);
}

/**
 * 收集数据
 */
SktFavoritesInfoDlg.collectData = function() {
    this
    .set('favoriteId')
    .set('userId')
    .set('favoriteType')
    .set('targetId')
    .set('createTime');
}

/**
 * 提交添加
 */
SktFavoritesInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktFavorites/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktFavorites.table.refresh();
        SktFavoritesInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktFavoritesInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktFavoritesInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktFavorites/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktFavorites.table.refresh();
        SktFavoritesInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktFavoritesInfoData);
    ajax.start();
}

$(function() {

});
