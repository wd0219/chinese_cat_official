/**
 * 代理公司华宝记录管理初始化
 */
var LogKaiyuanAgent = {
    id: "LogKaiyuanAgentTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogKaiyuanAgent.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前华宝余额', field: 'preKaiyuan', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动金额', field: 'kaiyuanp', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后华宝余额', field: 'akaiyuan', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogKaiyuanAgent.type},
            //{title: '目标代理公司ID', field: 'agentId', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'kaiyuanType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 资金变动类型对应：1转换获得>2提现驳回>3提现服务费退回>32提现>33提现服务费
 */
LogKaiyuanAgent.type = function(value,row,index){
	 if(value == 1){
		 return "转换获得";
	 }else if(value == 2){
		return "提现驳回"; 
	 }else if(value == 3){
		return "提现服务费退回"; 
	 }else if(value == 32){
		return "提现"; 
	 }else if(value == 33){
		return "提现服务费"; 
	 }
}
/**
 * 检查是否选中
 */
LogKaiyuanAgent.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogKaiyuanAgent.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理公司华宝记录
 */
LogKaiyuanAgent.openAddLogKaiyuanAgent = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理公司华宝记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logKaiyuanAgent/logKaiyuanAgent_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代理公司华宝记录详情
 */
LogKaiyuanAgent.openLogKaiyuanAgentDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理公司华宝记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logKaiyuanAgent/logKaiyuanAgent_update/' + LogKaiyuanAgent.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代理公司华宝记录
 */
LogKaiyuanAgent.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logKaiyuanAgent/delete", function (data) {
            Feng.success("删除成功!");
            LogKaiyuanAgent.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logKaiyuanAgentId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代理公司华宝记录列表
 */
LogKaiyuanAgent.search = function () {
    var queryData = {};
    queryData['phone'] = $("#phone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogKaiyuanAgent.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogKaiyuanAgent.initColumn();
    var table = new BSTable(LogKaiyuanAgent.id, "/logKaiyuanAgent/list", defaultColunms);
    table.setPaginationType("server");
    LogKaiyuanAgent.table = table.init();
});
//导出Excel
LogKaiyuanAgent.export= function(){
	$('#'+LogKaiyuanAgent.id).tableExport({
		fileName:'代理公司华宝记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}