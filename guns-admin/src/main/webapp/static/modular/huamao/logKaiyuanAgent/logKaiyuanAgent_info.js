/**
 * 初始化代理公司华宝记录详情对话框
 */
var LogKaiyuanAgentInfoDlg = {
    logKaiyuanAgentInfoData : {}
};

/**
 * 清除数据
 */
LogKaiyuanAgentInfoDlg.clearData = function() {
    this.logKaiyuanAgentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogKaiyuanAgentInfoDlg.set = function(key, val) {
    this.logKaiyuanAgentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogKaiyuanAgentInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogKaiyuanAgentInfoDlg.close = function() {
    parent.layer.close(window.parent.LogKaiyuanAgent.layerIndex);
}

/**
 * 收集数据
 */
LogKaiyuanAgentInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('agentId')
    .set('orderNo')
    .set('preKaiyuan')
    .set('kaiyuanType')
    .set('kaiyuan')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogKaiyuanAgentInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logKaiyuanAgent/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogKaiyuanAgent.table.refresh();
        LogKaiyuanAgentInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logKaiyuanAgentInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogKaiyuanAgentInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logKaiyuanAgent/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogKaiyuanAgent.table.refresh();
        LogKaiyuanAgentInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logKaiyuanAgentInfoData);
    ajax.start();
}

$(function() {

});
