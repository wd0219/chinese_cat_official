/**
 * 用户账户管理初始化
 */
var Account = {
    id: "AccountTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Account.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户手机', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '用户类型', field: 'userType', visible: true, align: 'center', valign: 'middle',formatter:Account.userType},
            {title: '积分', field: 'score', visible: true, align: 'center', valign: 'middle',formatter:Account.op},
            {title: '待发积分', field: 'freezeScore', visible: true, align: 'center', valign: 'middle',formatter:Account.freeze},
            {title: '累计获得积分', field: 'totalScore', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝', field: 'kaiyuan', visible: true, align: 'center', valign: 'middle',formatter:Account.huabao},
            {title: '累计获得华宝', field: 'totalKaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '现金', field: 'cash', visible: true, align: 'center', valign: 'middle',formatter:Account.cash},
            {title: '待发现金', field: 'freezeCash', visible: true, align: 'center', valign: 'middle',formatter:Account.freezeCash},
            {title: '累计获得现金', field: 'totalCash', visible: true, align: 'center', valign: 'middle'},
            {title: '线上消费额', field: 'onlineExpenditure', visible: true, align: 'center', valign: 'middle'},
            {title: '线下消费额', field: 'offlineExpenditure', visible: true, align: 'center', valign: 'middle'},
            {title: '用户发展的主管或经理数量', field: 'inviteNum', visible: true, align: 'center', valign: 'middle'},
            {title: '发展1个商家待发奖励的积分', field: 'one', visible: true, align: 'center', valign: 'middle',formatter:Account.special},
            {title: '发展2个商家待发奖励的积分', field: 'two', visible: true, align: 'center', valign: 'middle',formatter:Account.special},
            {title: '发展3个商家待发奖励的积分', field: 'three', visible: true, align: 'center', valign: 'middle',formatter:Account.special},
            {title: '发展4个商家待发奖励的积分', field: 'four', visible: true, align: 'center', valign: 'middle',formatter:Account.special},
            {title: '发展5个及其以上商家待发奖励的积分', field: 'five', visible: true, align: 'center', valign: 'middle',formatter:Account.special},
    ];
};
/**
 * 用户类型 0普通 1主管 2经理
 */
Account.userType = function (value,row,index) {
	if(value == 0){
		return "普通";
	}else if(value == 1){
		return "主管";
	}else if(value == 2){
		return "经理";
	}
}
/*
 * 操作：给积分添加一个查看详情功能
 * */
Account.op = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"Account.openAccountDetail()\">"+value+"</a>"
}
/**
 * 代发积分详情
 */
Account.freeze = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"Account.openFreezeDetail()\">"+value+"</a>"
}
/**
 * 华宝详情
 */
Account.huabao = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"Account.openHuabaoDetail()\">"+value+"</a>"
}
/**
 * 现金详情
 */
Account.cash = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"Account.openCashDetail()\">"+value+"</a>"
}
/**
 * 待发现金详情
 */
Account.freezeCash = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"Account.openFreezeCashDetail()\">"+value+"</a>"
}
/**
 * 特别奖详情
 */
Account.special = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"Account.openSpecialDetail()\">"+value+"</a>"
}
/**
 * 检查是否选中
 */
Account.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Account.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户账户
 */
Account.openAddAccount = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户账户',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/account/account_add'
    });
    this.layerIndex = index;
};

/**
 * 查看用户积分账户详情
 */
Account.openAccountDetail = function () {
	var Account2 = {
		    id: "AccountTable",	//表格id
		    seItem: null,		//选中的条目
		    table: null,
		    layerIndex: -1
		};
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户账户详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logScore?userPhone=' + Account.seItem.userPhone	//将手机号传到积分记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 查看用户待发积分账户详情
 */
Account.openFreezeDetail = function () {
	var Account3 = {
		    id: "AccountTable",	//表格id
		    seItem: null,		//选中的条目
		    table: null,
		    layerIndex: -1
		};
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户账户详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logScoreFreeze?userPhone=' + Account.seItem.userPhone	//将手机号传到待发积分记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 查看用户华宝账户详情
 */
Account.openHuabaoDetail = function () {
	var Account4 = {
			id: "AccountTable",	//表格id
			seItem: null,		//选中的条目
			table: null,
			layerIndex: -1
	};
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '用户账户详情',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/logKaiyuan?userPhone=' + Account.seItem.userPhone	//将手机号传到待发积分记录表查询数据
		});
		this.layerIndex = index;
	}
};
/**
 * 查看用户现金账户详情
 */
Account.openCashDetail = function () {
	var Account5 = {
			id: "AccountTable",	//表格id
			seItem: null,		//选中的条目
			table: null,
			layerIndex: -1
	};
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '用户账户详情',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/logCash?userPhone=' + Account.seItem.userPhone	//将手机号传到待发积分记录表查询数据
		});
		this.layerIndex = index;
	}
};
/**
 * 查看用户待发现金账户详情
 */
Account.openFreezeCashDetail = function () {
	var Account6 = {
			id: "AccountTable",	//表格id
			seItem: null,		//选中的条目
			table: null,
			layerIndex: -1
	};
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '用户账户详情',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/logCashFreeze?userPhone=' + Account.seItem.userPhone	//将手机号传到待发积分记录表查询数据
		});
		this.layerIndex = index;
	}
};
/**
 * 查看用户特别奖励账户详情
 */
Account.openSpecialDetail = function () {
	var Account6 = {
			id: "AccountTable",	//表格id
			seItem: null,		//选中的条目
			table: null,
			layerIndex: -1
	};
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '用户账户详情',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/logScoreSpecial?userPhone=' + Account.seItem.userPhone	//将手机号传到待发积分记录表查询数据
		});
		this.layerIndex = index;
	}
};

/**
 * 删除用户账户
 */
Account.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/account/delete", function (data) {
            Feng.success("删除成功!");
            Account.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("accountId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户账户列表
 */
Account.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['userType'] = $("#userType").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['loginName'] = $("#loginName").val();
    Account.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Account.initColumn();
    var table = new BSTable(Account.id, "/account/list/"+$('#loginName2').val(), defaultColunms);
    table.setPaginationType("server");
    Account.table = table.init();
});

//导出Excel
Account.export= function(){
	$('#'+Account.id).tableExport({
		fileName:'用户账户',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}