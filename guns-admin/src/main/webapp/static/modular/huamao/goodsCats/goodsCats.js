/**
 * 商品分类管理初始化
 */
var GoodsCats = {
    id: "GoodsCatsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
GoodsCats.initColumn = function () {
    return [
            {field: 'selectItem', radio: true},
            // {
            	// title:'id',visible: false,align: 'center', valign: 'middle',width:50,
			//     formatter: function (value, row, index) {
			//         return index+1;
			//     }
			// },
          /*  {title: '自增Id', field: 'catId', visible: false, align: 'center', valign: 'middle'},
            {title: '父ID', field: 'parentId', visible: false, align: 'center', valign: 'middle'},*/
            {title: '分类名称', field: 'catName', visible: true, align: 'center', valign: 'middle'},
            {title: '推荐楼层', field: 'isFloorName', visible: true, align: 'center', valign: 'middle'},
         /*  {title: '分类图片', field: 'catImg', visible: false, align: 'center', valign: 'middle'},*/
            {title: '是否显示', field: 'isShowName', visible: true, align: 'center', valign: 'middle'},
            
            {title: '排序号', field: 'catSort', visible: true, align: 'center', valign: 'middle'},
           /* {title: '删除标志', field: 'dataFlag', visible: false, align: 'center', valign: 'middle'},
            {title: '建立时间', field: 'createTime', visible: false, align: 'center', valign: 'middle'},*/
            {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',formatter:GoodsCats.op}
    ];
};
GoodsCats.op = function () {
	return "<a href='javascript:void(0)' onclick=\"GoodsCats.openAddGoodschildCats()\">增加子分类</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"GoodsCats.openGoodsCatsDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"GoodsCats.delete2()\">删除</a>"
}
/**
 * 检查是否选中
 */
GoodsCats.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        GoodsCats.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品分类
 */
GoodsCats.openAddGoodsCats = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品分类',
        area:['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/goodsCats/goodsCats_add'
    });
    this.layerIndex = index;
};
/**
 * 点击添加商品分类(子类)
 */
GoodsCats.openAddGoodschildCats = function () {
	if (this.check()) {
	    var index = layer.open({
	        type: 2,
	        title: '添加商品分类子类',
	        area: ['100%', '100%'], //宽高
	        fix: false, //不固定
	        maxmin: true,
	        content: Feng.ctxPath + '/goodsCats/goodsCatsChild_add/'+ GoodsCats.seItem.id
	    });
	    this.layerIndex = index;
	}
};

/**
 * 打开查看商品分类详情
 */
GoodsCats.openGoodsCatsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品分类详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goodsCats/goodsCats_update/' + GoodsCats.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品分类
 */
GoodsCats.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/goodsCats/delete", function (data) {
            Feng.success("删除成功!");
            GoodsCats.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("goodsCatsId",this.seItem.id);
        ajax.start();
    }
};
/**
 * 删除商品分类
 */
GoodsCats.delete2 = function () {
    if (this.check()) {
    	var goodsCatsId=this.seItem.id;
        var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/goodsCats/delete", function (data) {
            Feng.success("删除成功!");
            GoodsCats.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("goodsCatsId",goodsCatsId);
        ajax.start();
    };
    	  Feng.confirm("是否删除该条记录?",operation);
    }
};
/**
 * 查询商品分类列表
 */
GoodsCats.search = function () {
    var queryData = {};
    queryData['catName'] = $("#catName").val();
    GoodsCats.table.refresh({query: queryData});
};

$(function () {
   /* var defaultColunms = GoodsCats.initColumn();
    var table = new BSTable(GoodsCats.id, "/goodsCats/list", defaultColunms);
    table.setPaginationType("client");
   GoodsCats.table = table.init();*/
	var defaultColunms = GoodsCats.initColumn();
	var table = new BSTreeTable(GoodsCats.id, "/goodsCats/list", defaultColunms);
	table.setExpandColumn(0);
	table.setIdField("catId");
	table.setCodeField("catId");
	table.setParentCodeField("parentId");
	table.setExpandAll(false);
	table.init();
	GoodsCats.table = table;
});


/*$(function () {
    var $table = $("#table");
    $table.bootstrapTable({
    	url:'goodsCats/list1',
       // url:'${ctxPath}/static/json/test.json',
        striped:true,
        sidePagenation:'server',
        idField:'id',
        columns:[
            {
                field: 'ck',
                checkbox: true
            },{
                field:'catName',
                title:'分类名称'
            },{
                field:'isFloor',
                title:'推荐楼层'
            },{
                field:'isShow',
                title:'是否显示'
            },{
                field:'catSort',
                title:'排序号'
            },{
            	field:'op',  
                title:'操作',
                align:'center',  
                formatter:function (value,row,index) {  
                    var a = "<a href='#' onclick='method()'>修改</a>";  
                    var b = "<a href='#' onclick='method()'>删除</a>";  
                    return a+b; 
                 }
            }
        ],
        treeShowField: 'catName',
        parentIdField: 'pid',
        onLoadSuccess: function(data) {
            $table.treegrid({
                initialState: 'collapsed',//收缩
                treeColumn: 1,//指明第几列数据改为树形
                expanderExpandedClass: 'glyphicon glyphicon-triangle-bottom',
                expanderCollapsedClass: 'glyphicon glyphicon-triangle-right',
                onChange: function() {
                    $table.bootstrapTable('resetWidth');
                }
            });
        }
    });
})
*/
