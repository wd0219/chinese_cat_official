/**
 * 初始化商品分类详情对话框
 */
var GoodsCatsInfoDlg = {
    goodsCatsInfoData : {}
};

/**
 * 清除数据
 */
GoodsCatsInfoDlg.clearData = function() {
    this.goodsCatsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsCatsInfoDlg.set = function(key, val) {
    this.goodsCatsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsCatsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
GoodsCatsInfoDlg.close = function() {
    parent.layer.close(window.parent.GoodsCats.layerIndex);
}

/**
 * 收集数据
 */
GoodsCatsInfoDlg.collectData = function() {
    this
    .set('catId')
    .set('parentId')
    .set('catName')
    .set('catImg')
    .set('isShow')
    .set('isFloor')
    .set('catSort')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
GoodsCatsInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var catName = $("#catName").val();
    var catImg = $("#catImg").val();
    var catSort = $("#catSort").val();

    if(catName == null || catName ==""){
        var html='<span class="error">分类名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#catName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(catImg == null || catImg ==""){
        var html='<span class="error">分类图片不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#catImg').after(html);
        falg = 1;
    }

    if(!isRealNum(catSort)){
        var html='<span class="error">排序必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#catSort').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    //console.log($('input[name=isShow]:checked').val());
    this.goodsCatsInfoData['isShow']=$('input[name=isShow]:checked').val();
    this.goodsCatsInfoData['isFloor']=$('input[name=isFloor]:checked').val();
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/goodsCats/add", function(data){
        Feng.success("添加成功!");
        window.parent.GoodsCats.table.refresh();
        GoodsCatsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsCatsInfoData);
    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
}

function isRealNum(val){
    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
    if(val === "" || val ==null){
        return false;
    }
    if(!isNaN(val)){
        return true;
    }else{
        return false;
    }
}

/**
 * 提交修改
 */
GoodsCatsInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var catName = $("#catName").val();
    var catImg = $("#catImg").val();
    var catSort = $("#catSort").val();

    if(catName == null || catName ==""){
        var html='<span class="error">分类名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#catName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(catImg == null || catImg ==""){
        var html='<span class="error">分类图片不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#catImg').after(html);
        falg = 1;
    }

    if(!isRealNum(catSort)){
        var html='<span class="error">排序必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#catSort').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    let isShow = $('input:radio[name=isShow]:checked').val();
    this.goodsCatsInfoData.isShow = isShow ;
    let isFloor = $('input:radio[name=isFloor]:checked').val();
    this.goodsCatsInfoData.isFloor = isFloor ;
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/goodsCats/update", function(data){
        Feng.success("修改成功!");
        window.parent.GoodsCats.table.refresh();
        GoodsCatsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsCatsInfoData);
    ajax.start();
}else{
    Feng.success("请核实添加字段!");
}
}

$(function() {
	for(let e of document.getElementsByClassName('upload-btn')){
		let id = $(e).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
		uploader = new $WebUpload(id);
		uploader.setUploadBarId(id);
		uploader.init();
	}
	// 初始化头像上传
    var avatarUp = new $WebUpload("catImg");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.init();
    GoodsCatsInfoDlg.getCatImg();
    
});
