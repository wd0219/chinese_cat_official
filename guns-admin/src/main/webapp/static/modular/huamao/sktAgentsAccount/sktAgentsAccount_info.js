/**
 * 初始化代理公司账户详情对话框
 */
var SktAgentsAccountInfoDlg = {
    sktAgentsAccountInfoData : {}
};

/**
 * 清除数据
 */
SktAgentsAccountInfoDlg.clearData = function() {
    this.sktAgentsAccountInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAgentsAccountInfoDlg.set = function(key, val) {
    this.sktAgentsAccountInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAgentsAccountInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktAgentsAccountInfoDlg.close = function() {
    parent.layer.close(window.parent.SktAgentsAccount.layerIndex);
}

/**
 * 收集数据
 */
SktAgentsAccountInfoDlg.collectData = function() {
    this
    .set('accountId')
    .set('agentId')
    .set('score')
    .set('freezeScore')
    .set('totalScore')
    .set('kaiyuan')
    .set('totalKaiyuan')
    .set('runFlag')
    .set('runTime')
    .set('oneRatio')
    .set('twoRatio')
    .set('threeRatio')
    .set('createTime');
}

/**
 * 提交添加
 */
SktAgentsAccountInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if($('#oneRatio').val() == '' || $('#oneRatio').val() == 'undefined'){
		Feng.success("成为股东的分红系数不能为空");
		return ;
	}
	if($('#twoRatio').val() == '' || $('#twoRatio').val() == 'undefined'){
		Feng.success("完成70%(落地)的分红系数不能为空");
		return ;
	}
	if($('#threeRatio').val() == '' || $('#threeRatio').val() == 'undefined'){
		Feng.success("全部完成的分红系数不能为空");
		return ;
	}
	if(parseInt($('#oneRatio').val())>= 1 || 
			parseInt($('#twoRatio').val()) >= 1 || 
			parseInt($('#threeRatio').val()) >= 1 ||
			parseFloat($('#oneRatio').val()) > parseFloat($('#twoRatio').val()) ||
			parseFloat($('#twoRatio').val()) > parseFloat($('#threeRatio').val())
		){
			Feng.success("请设置合理的分红系数！");
			return ;
		}
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktAgentsAccount/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktAgentsAccount.table.refresh();
        SktAgentsAccountInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAgentsAccountInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktAgentsAccountInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktAgentsAccount/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktAgentsAccount.table.refresh();
        SktAgentsAccountInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAgentsAccountInfoData);
    ajax.start();
}

$(function() {

});
