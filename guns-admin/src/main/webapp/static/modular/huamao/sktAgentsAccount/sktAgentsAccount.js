/**
 * 代理公司账户管理初始化
 */
var SktAgentsAccount = {
    id: "SktAgentsAccountTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktAgentsAccount.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '代理公司ID', field: 'agentId', visible: false, align: 'center', valign: 'middle'},
            {title: '代理公司名称', field: 'agentName', visible: true, align: 'center', valign: 'middle',formatter:SktAgentsAccount.agentName},
            {title: '积分', field: 'score', visible: true, align: 'center', valign: 'middle'},
            {title: '待发积分', field: 'freezeScore', visible: true, align: 'center', valign: 'middle'},
            {title: '累计获得积分', field: 'totalScore', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝', field: 'kaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '累计获得华宝', field: 'totalKaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '成为股东分红系数', field: 'oneRatio', visible: true, align: 'center', valign: 'middle'},
            {title: '完成70%的分红系数', field: 'twoRatio', visible: true, align: 'center', valign: 'middle'},
            {title: '全部完成的分红系数', field: 'threeRatio', visible: true, align: 'center', valign: 'middle'},
            {title: '运营账号状态', field: 'runFlag', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	if(value=='1')
            		return '开启';
            	else{
            		return '关闭';
            	}
            }},
            {title: '运营账号开启时间', field: 'runTime', visible: true, align: 'center', valign: 'middle'},
            {title: '运营账号状态操作', field: 'runFlag', visible: true, align: 'center', valign: 'middle',formatter:SktAgentsAccount.runFlag},
            {title: '设置分红系数', field: 'op', visible: true, align: 'center', valign: 'middle',formatter:SktAgentsAccount.op}
    ];
};
/**
 * 运营账号状态操作
 */
SktAgentsAccount.runFlag = function(value, row, index){
	if(value==1){
		return "<a href='javascript:void(0)' onclick=\"SktAgentsAccount.openOrClose(0)\">关闭</a>";
	}else{
		return "<a href='javascript:void(0)' onclick=\"SktAgentsAccount.openOrClose(1)\">开启</a>";
	}
}
/**
 * 更改状态开启关闭
 */
SktAgentsAccount.openOrClose = function(runFlag){
	if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '系统提示',
            area: ['430px', '240px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktAgentsAccount/sktAgentsAccount_openOrClose/' + SktAgentsAccount.seItem.accountId+"/"+runFlag
        });
        this.layerIndex = index;
    }
}

/**
 * 操作
 */
SktAgentsAccount.op = function(){
	return "<a href='javascript:void(0)' onclick=\"SktAgentsAccount.ratio()\">设置分红系数</a>";
}
/**
 * 设置分红系数
 */
SktAgentsAccount.ratio = function(){
	if (this.check()) {
	 var index = layer.open({
	        type: 2,
	        title: '添加代理公司账户',
	        area: ['100%', '100%'], //宽高
	        fix: false, //不固定
	        maxmin: false,
	        content: Feng.ctxPath + '/sktAgentsAccount/sktAgentsAccount_setRatio/'+SktAgentsAccount.seItem.agentId
	    });
	    this.layerIndex = index;
	}
}

/**
 * 代理公司名称
 */
SktAgentsAccount.agentName = function(value,row,index){
	return "<a href='javascript:void(0)' onclick=\"SktAgentsAccount.guanliye()\">"+value+"</a>";
}

SktAgentsAccount.guanliye = function(){
	if (this.check()) {
		window.location.href = Feng.ctxPath + '/sktAgents?agentId=' +this.seItem.agentId
	}
}

/**
 * 检查是否选中
 */
SktAgentsAccount.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktAgentsAccount.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理公司账户
 */
SktAgentsAccount.openAddSktAgentsAccount = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理公司账户',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/sktAgentsAccount/sktAgentsAccount_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代理公司账户详情
 */
SktAgentsAccount.openSktAgentsAccountDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理公司账户详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/sktAgentsAccount/sktAgentsAccount_update/' + SktAgentsAccount.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代理公司账户
 */
SktAgentsAccount.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktAgentsAccount/delete", function (data) {
            Feng.success("删除成功!");
            SktAgentsAccount.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktAgentsAccountId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代理公司账户列表
 */
SktAgentsAccount.search = function () {
    var queryData = {};
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['runFlag'] = $("#runFlag").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    SktAgentsAccount.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktAgentsAccount.initColumn();
    var table = new BSTable(SktAgentsAccount.id, "/sktAgentsAccount/list", defaultColunms);
    table.setPaginationType("server");
    SktAgentsAccount.table = table.init();
});
