/**
 * 初始化详情对话框
 */
var SktLogOperatesInfoDlg = {
    sktLogOperatesInfoData : {}
};

/**
 * 清除数据
 */
SktLogOperatesInfoDlg.clearData = function() {
    this.sktLogOperatesInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktLogOperatesInfoDlg.set = function(key, val) {
    this.sktLogOperatesInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktLogOperatesInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktLogOperatesInfoDlg.close = function() {
    parent.layer.close(window.parent.SktLogOperates.layerIndex);
}

/**
 * 收集数据
 */
SktLogOperatesInfoDlg.collectData = function() {
    this
    .set('operateId')
    .set('staffId')
    .set('operateTime')
    .set('menuId')
    .set('operateDesc')
    .set('operateUrl')
    .set('content')
    .set('operateIP');
}

/**
 * 提交添加
 */
SktLogOperatesInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktLogOperates/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktLogOperates.table.refresh();
        SktLogOperatesInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktLogOperatesInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktLogOperatesInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktLogOperates/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktLogOperates.table.refresh();
        SktLogOperatesInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktLogOperatesInfoData);
    ajax.start();
}

$(function() {

});
