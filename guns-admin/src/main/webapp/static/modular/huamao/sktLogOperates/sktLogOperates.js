/**
 * 管理初始化
 */
var SktLogOperates = {
    id: "SktLogOperatesTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktLogOperates.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '职员', field: 'staffId', visible: true, align: 'center', valign: 'middle'},
            {title: '操作功能', field: 'operateDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '操作链接地址', field: 'operateUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '操作IP', field: 'operateIP', visible: true, align: 'center', valign: 'middle'},
            {title: '操作时间', field: 'operateTime', visible: true, align: 'center', valign: 'middle'},
            {title: '请求内容', field: 'content', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktLogOperates.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktLogOperates.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
SktLogOperates.openAddSktLogOperates = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktLogOperates/sktLogOperates_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
SktLogOperates.openSktLogOperatesDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktLogOperates/sktLogOperates_update/' + SktLogOperates.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
SktLogOperates.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktLogOperates/delete", function (data) {
            Feng.success("删除成功!");
            SktLogOperates.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktLogOperatesId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
SktLogOperates.search = function () {
    var queryData = {};
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    SktLogOperates.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktLogOperates.initColumn();
    var table = new BSTable(SktLogOperates.id, "/sktLogOperates/list", defaultColunms);
    table.setPaginationType("server");
    SktLogOperates.table = table.init();
});
