/**
 * 初始化代发积分流水表详情对话框
 */
var LogScoreFreezeInfoDlg = {
    logScoreFreezeInfoData : {}
};

/**
 * 清除数据
 */
LogScoreFreezeInfoDlg.clearData = function() {
    this.logScoreFreezeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogScoreFreezeInfoDlg.set = function(key, val) {
    this.logScoreFreezeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogScoreFreezeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogScoreFreezeInfoDlg.close = function() {
    parent.layer.close(window.parent.LogScoreFreeze.layerIndex);
}

/**
 * 收集数据
 */
LogScoreFreezeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preScore')
    .set('scoreType')
    .set('score')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogScoreFreezeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logScoreFreeze/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogScoreFreeze.table.refresh();
        LogScoreFreezeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logScoreFreezeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogScoreFreezeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logScoreFreeze/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogScoreFreeze.table.refresh();
        LogScoreFreezeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logScoreFreezeInfoData);
    ajax.start();
}

$(function() {

});
