/**
 * 代发积分流水表管理初始化
 */
var LogScoreFreeze = {
    id: "LogScoreFreezeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogScoreFreeze.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前数量', field: 'preScore', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动数量', field: 'scorep', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后数量', field: 'ascore', visible: true, align: 'center', valign: 'middle'},
        	{title: '类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogScoreFreeze.type},
            //{title: '支出人', field: 'fromId', visible: true, align: 'center', valign: 'middle'},
            //{title: '收入人', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'scoreType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 类型对应：1商城消费奖励>2商家消费奖励>3申请库存>4消费奖励>5销售奖励>6特别奖励>7升级奖励>
8分享升级奖励>9代理奖励>10分享代理奖励>14手动补发>15代发分红>16代发分红>17代发分红>
18购买牌匾奖励>19消费奖励>31代发积分转出>32手动扣除>33代发积分转出>34代发积分转出
 */
LogScoreFreeze.type = function(value,row,index){
	if(value == 1){
		return "商城消费奖励";
	}else if(value == 2){
		return "商家消费奖励";
	}else if(value == 3){
		return "申请库存";
	}else if(value == 4){
		return "消费奖励";
	}else if(value == 5){
		return "销售奖励";
	}else if(value == 6){
		return "特别奖励";
	}else if(value == 7){
		return "升级奖励";
	}else if(value == 8){
		return "分享升级奖励";
	}else if(value == 9){
		return "代理奖励";
	}else if(value == 10){
		return "分享代理奖励";
	}else if(value == 14){
		return "手动补发";
	}else if(value == 15){
		return "代发分红";
	}else if(value == 16){
		return "代发分红";
	}else if(value == 17){
		return "代发分红";
	}else if(value == 18){
		return "购买牌匾奖励";
	}else if(value == 19){
		return "消费奖励";
	}else if(value == 31){
		return "代发积分转出";
	}else if(value == 32){
		return "手动扣除";
	}else if(value == 33){
		return "代发积分转出";
	}else if(value == 34){
		return "代发积分转出";
	}
}
/**
 * 检查是否选中
 */
LogScoreFreeze.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogScoreFreeze.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代发积分流水表
 */
LogScoreFreeze.openAddLogScoreFreeze = function () {
    var index = layer.open({
        type: 2,
        title: '添加代发积分流水表',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logScoreFreeze/logScoreFreeze_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代发积分流水表详情
 */
LogScoreFreeze.openLogScoreFreezeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代发积分流水表详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logScoreFreeze/logScoreFreeze_update/' + LogScoreFreeze.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代发积分流水表
 */
LogScoreFreeze.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logScoreFreeze/delete", function (data) {
            Feng.success("删除成功!");
            LogScoreFreeze.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logScoreFreezeId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代发积分流水表列表
 */
LogScoreFreeze.search = function () {
	var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogScoreFreeze.table.refresh({query: queryData});
};

$(function () {
	var defaultColunms = LogScoreFreeze.initColumn();
    var table = new BSTable(LogScoreFreeze.id, "/logScoreFreeze/list/"+$('#userPhone2').val(), defaultColunms);
    table.setPaginationType("server");
    LogScoreFreeze.table = table.init();
});
//导出Excel
LogScoreFreeze.export= function(){
	$('#'+LogScoreFreeze.id).tableExport({
		fileName:'待发积分流水表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}