/**
 * 初始化快递管理详情对话框
 */
var SktExpressInfoDlg = {
    sktExpressInfoData : {}
};

/**
 * 清除数据
 */
SktExpressInfoDlg.clearData = function() {
    this.sktExpressInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktExpressInfoDlg.set = function(key, val) {
    this.sktExpressInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktExpressInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktExpressInfoDlg.close = function() {
    parent.layer.close(window.parent.SktExpress.layerIndex);
}

/**
 * 收集数据
 */
SktExpressInfoDlg.collectData = function() {
    this
    .set('expressId')
    .set('expressName')
    .set('dataFlag')
    .set('expressCode');
}

/**
 * 提交添加
 */
SktExpressInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var expressName = $("#expressName").val();

    if(expressName == null || expressName ==""){
        var html='<span class="error">快递名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#expressName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/sktExpress/add", function(data){
	        Feng.success("添加成功!");
	        window.parent.SktExpress.table.refresh();
	        SktExpressInfoDlg.close();
	    },function(data){
	        Feng.error("添加失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.sktExpressInfoData);
	    ajax.start();
		}else{
		    Feng.success("请核实添加字段!");
		}
	}

	function isRealNum(val){
	    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	    if(val === "" || val ==null){
	        return false;
	    }
	    if(!isNaN(val)){
	        return true;
	    }else{
	        return false;
	    }
	}
/**
 * 提交修改
 */
SktExpressInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var expressName = $("#expressName").val();

    if(expressName == null || expressName ==""){
        var html='<span class="error">快递名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#expressName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
	    var ajax = new $ax(Feng.ctxPath + "/sktExpress/update", function(data){
	        Feng.success("修改成功!");
	        window.parent.SktExpress.table.refresh();
	        SktExpressInfoDlg.close();
	    },function(data){
	        Feng.error("修改失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.sktExpressInfoData);
	    ajax.start();
		}else{
		    Feng.success("请核实添加字段!");
		}
	}

$(function() {

});
