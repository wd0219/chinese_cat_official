/**
 * 快递管理管理初始化
 */
var SktExpress = {
    id: "SktExpressTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktExpress.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '快递名称', field: 'expressName', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',formatter:SktExpress.op}
        ];
};
/*
 * 操作
 * */
SktExpress.op = function () {
	return "<a href='javascript:void(0)' onclick=\"SktExpress.openSktExpressDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktExpress.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
SktExpress.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktExpress.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加快递管理
 */
SktExpress.openAddSktExpress = function () {
    var index = layer.open({
        type: 2,
        title: '添加快递管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktExpress/sktExpress_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看快递管理详情
 */
SktExpress.openSktExpressDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '快递管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktExpress/sktExpress_update/' + SktExpress.seItem.expressId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除快递管理
 */
SktExpress.delete = function () {
    if (this.check()) {
    	var id = this.seItem.expressId;
    	var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/sktExpress/delete", function (data) {
            Feng.success("删除成功!");
            SktExpress.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktExpressId",id);
        ajax.start();
    };
    Feng.confirm("是否删除角色 ?",operation);
    };
};

/**
 * 查询快递管理列表
 */
SktExpress.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktExpress.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktExpress.initColumn();
    var table = new BSTable(SktExpress.id, "/sktExpress/list", defaultColunms);
    table.setPaginationType("client");
    SktExpress.table = table.init();
});
