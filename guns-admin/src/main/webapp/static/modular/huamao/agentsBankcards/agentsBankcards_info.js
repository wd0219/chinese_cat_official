/**
 * 初始化代理公司银行卡详情对话框
 */
var AgentsBankcardsInfoDlg = {
    agentsBankcardsInfoData : {}
};

/**
 * 清除数据
 */
AgentsBankcardsInfoDlg.clearData = function() {
    this.agentsBankcardsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AgentsBankcardsInfoDlg.set = function(key, val) {
    this.agentsBankcardsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AgentsBankcardsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AgentsBankcardsInfoDlg.close = function() {
    parent.layer.close(window.parent.AgentsBankcards.layerIndex);
}

/**
 * 收集数据
 */
AgentsBankcardsInfoDlg.collectData = function() {
    this
    .set('bankCardId')
    .set('agentId')
    .set('provinceId')
    .set('cityId')
    .set('areaId')
    .set('bankId')
    .set('accArea')
    .set('accNo')
    .set('accUser')
    .set('IDnumber')
    .set('phone')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
AgentsBankcardsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/agentsBankcards/add", function(data){
        Feng.success("添加成功!");
        window.parent.AgentsBankcards.table.refresh();
        AgentsBankcardsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.agentsBankcardsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AgentsBankcardsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/agentsBankcards/update", function(data){
        Feng.success("修改成功!");
        window.parent.AgentsBankcards.table.refresh();
        AgentsBankcardsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.agentsBankcardsInfoData);
    ajax.start();
}

$(function() {

});
