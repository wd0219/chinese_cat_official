/**
 * 代理公司银行卡管理初始化
 */
var AgentsBankcards = {
    id: "AgentsBankcardsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AgentsBankcards.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '代理公司ID', field: 'agentId', visible: true, align: 'center', valign: 'middle'},
            //{title: '省id', field: 'provinceId', visible: true, align: 'center', valign: 'middle'},
            //{title: '市id', field: 'cityId', visible: true, align: 'center', valign: 'middle'},
            //{title: '区县id', field: 'areaId', visible: true, align: 'center', valign: 'middle'},
            //{title: '银行ID', field: 'bankId', visible: true, align: 'center', valign: 'middle'},
            {title: '代理公司名称', field: 'agentName', visible: true, align: 'center', valign: 'middle'},
            {title: '银行名称', field: 'bankName', visible: true, align: 'center', valign: 'middle'},
            {title: '开户行', field: 'accArea', visible: true, align: 'center', valign: 'middle'},
            {title: '银行卡号', field: 'accNo', visible: true, align: 'center', valign: 'middle'},
            {title: '持卡人', field: 'accUser', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号', field: 'number', visible: true, align: 'center', valign: 'middle'},
            //{title: '身份证号', field: 'IDnumber', visible: true, align: 'center', valign: 'middle'},
            //{title: '银行预留电话号码', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效标志', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '绑卡时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: '', visible: true, align: 'center', valign: 'middle',formatter:AgentsBankcards.deleteById}
    ];
};
/*
 * 操作：删除本条数据
 * */
AgentsBankcards.deleteById = function () {
	return "<a href='javascript:void(0)'onclick=\"AgentsBankcards.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
AgentsBankcards.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AgentsBankcards.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理公司银行卡
 */
AgentsBankcards.openAddAgentsBankcards = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理公司银行卡',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/agentsBankcards/agentsBankcards_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代理公司银行卡详情
 */
AgentsBankcards.openAgentsBankcardsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理公司银行卡详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/agentsBankcards/agentsBankcards_update/' + AgentsBankcards.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 通过ID删除代理公司银行卡
 */
AgentsBankcards.delete = function () {
	if (this.check()) {
	      var operation = function (){
	    	  var bankCardId = AgentsBankcards.seItem.bankCardId;
	    	var ajax = new $ax(Feng.ctxPath + "/agentsBankcards/deleteById", function (data) {
	            Feng.success("删除成功!");
	            AgentsBankcards.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("bankCardId",bankCardId);
	        ajax.start();
	       };
	        Feng.confirm("是否删除该条记录?",operation);
	    }
};

/**
 * 查询代理公司银行卡列表
 */
AgentsBankcards.search = function () {
    var queryData = {};
    queryData['accNo'] = $("#accNo").val();
    queryData['agentName'] = $("#agentName").val();
    AgentsBankcards.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AgentsBankcards.initColumn();
    var table = new BSTable(AgentsBankcards.id, "/agentsBankcards/list", defaultColunms);
    table.setPaginationType("server");
    AgentsBankcards.table = table.init();
});
//导出Excel
AgentsBankcards.export= function(){
	$('#'+AgentsBankcards.id).tableExport({
		fileName:'代理公司银行卡记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}