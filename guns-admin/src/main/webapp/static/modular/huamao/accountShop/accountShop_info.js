/**
 * 初始化商家账户详情对话框
 */
var AccountShopInfoDlg = {
    accountShopInfoData : {}
};

/**
 * 清除数据
 */
AccountShopInfoDlg.clearData = function() {
    this.accountShopInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccountShopInfoDlg.set = function(key, val) {
    this.accountShopInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccountShopInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AccountShopInfoDlg.close = function() {
    parent.layer.close(window.parent.AccountShop.layerIndex);
}

/**
 * 收集数据
 */
AccountShopInfoDlg.collectData = function() {
    this
    .set('accountId')
    .set('userId')
    .set('shopId')
    .set('onlineTurnover')
    .set('offlineTurnover')
    .set('stockScore')
    .set('freezeStockScore')
    .set('totalStockScore')
    .set('totalGiveStockScore')
    .set('freezeKaiyuan')
    .set('kaiyuanTurnover')
    .set('totalKaiyuanTurnover')
    .set('createTime');
}

/**
 * 提交添加
 */
AccountShopInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accountShop/add", function(data){
        Feng.success("添加成功!");
        window.parent.AccountShop.table.refresh();
        AccountShopInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accountShopInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AccountShopInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accountShop/update", function(data){
        Feng.success("修改成功!");
        window.parent.AccountShop.table.refresh();
        AccountShopInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accountShopInfoData);
    ajax.start();
}

$(function() {

});
