/**
 * 商家账户管理初始化
 */
var AccountShop = {
    id: "AccountShopTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AccountShop.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户手机', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '用户类型', field: 'userType', visible: true, align: 'center', valign: 'middle',formatter:AccountShop.userType},
            {title: '线上商城营业额', field: 'onlineTurnover', visible: true, align: 'center', valign: 'middle'},
            {title: '线下商家营业额', field: 'offlineTurnover', visible: true, align: 'center', valign: 'middle'},
            {title: '库存积分', field: 'stockScore', visible: true, align: 'center', valign: 'middle',formatter:AccountShop.stockScore},
            {title: '冻结的库存积分', field: 'freezeStockScore', visible: true, align: 'center', valign: 'middle',formatter:AccountShop.freezeStockScore},
            {title: '累计充值库存积分', field: 'totalStockScore', visible: true, align: 'center', valign: 'middle'},
            {title: '累计分发库存积分', field: 'totalGiveStockScore', visible: true, align: 'center', valign: 'middle'},
            {title: '待发华宝营业额', field: 'freezeKaiyuan', visible: true, align: 'center', valign: 'middle',formatter:AccountShop.freezeKaiyuan},
            {title: '门店获得华宝支付的营业额', field: 'kaiyuanTurnover', visible: true, align: 'center', valign: 'middle',formatter:AccountShop.kaiyuanTurnover},
            {title: '门店累计获得华宝支付的营业额', field: 'totalKaiyuanTurnover', visible: true, align: 'center', valign: 'middle'},
            {title: '门店累计获得优惠券总额', field: 'totalShopping', visible: true, align: 'center', valign: 'middle'},
    ];
};
/**
 * 用户类型 0普通 1主管 2经理
 */
AccountShop.userType = function (value,row,index) {
	if(value == 0){
		return "普通";
	}else if(value == 1){
		return "主管";
	}else if(value == 2){
		return "经理";
	}
}
/*
 * 操作：给库存积分添加一个查看详情功能
 * */
AccountShop.stockScore = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"AccountShop.openStockScoreDetail()\">"+value+"</a>"
}
/*
 * 冻结库存积分查询详情功能
 */
AccountShop.freezeStockScore = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"AccountShop.openFreezeStockScoreDetail()\">"+value+"</a>"
}
/*
 * 待发华宝营业额查询详情功能
 */
AccountShop.freezeKaiyuan = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"AccountShop.openKaiyuanFreezeDetail()\">"+value+"</a>"
}
/*
 * 门店获得华宝支付查询详情功能
 */
AccountShop.kaiyuanTurnover = function (value,row,index) {
	return "<a href='javascript:void(0)'onclick=\"AccountShop.openKaiyuanTurnoverDetail()\">"+value+"</a>"
}

/**
 * 检查是否选中
 */
AccountShop.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AccountShop.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商家账户
 */
AccountShop.openAddAccountShop = function () {
    var index = layer.open({
        type: 2,
        title: '添加商家账户',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accountShop/accountShop_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商家库存积分账户详情
 */
AccountShop.openStockScoreDetail = function () {
	var AccountShop2 = {
		    id: "AccountShopTable",	//表格id
		    seItem: null,		//选中的条目
		    table: null,
		    layerIndex: -1
		};
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商家账户详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logStockscore?userPhone=' + AccountShop.seItem.userPhone	//将手机号传到积分记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 打开查看商家冻结库存积分账户详情
 */
AccountShop.openFreezeStockScoreDetail = function () {
	var AccountShop3 = {
			id: "AccountShopTable",	//表格id
			seItem: null,		//选中的条目
			table: null,
			layerIndex: -1
	};
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '商家账户详情',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/logStockscore?userPhone=' + AccountShop.seItem.userPhone + '&type=2'	//将手机号和类型传到积分记录表查询数据
		});
		this.layerIndex = index;
	}
};
/**
 * 打开查看商家待发华宝营业额账户详情
 */
AccountShop.openKaiyuanFreezeDetail = function () {
	var AccountShop4 = {
			id: "AccountShopTable",	//表格id
			seItem: null,		//选中的条目
			table: null,
			layerIndex: -1
	};
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '商家账户详情',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/logKaiyuanFreeze?userPhone=' + AccountShop.seItem.userPhone	//将手机号传到积分记录表查询数据
		});
		this.layerIndex = index;
	}
};
/**
 * 打开查看商家门店获得华宝支付账户详情
 */
AccountShop.openKaiyuanTurnoverDetail = function () {
	var AccountShop5 = {
			id: "AccountShopTable",	//表格id
			seItem: null,		//选中的条目
			table: null,
			layerIndex: -1
	};
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '商家账户详情',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/logKaiyuanTurnover?userPhone=' + AccountShop.seItem.userPhone	//将手机号传到积分记录表查询数据
		});
		this.layerIndex = index;
	}
};

/**
 * 删除商家账户
 */
AccountShop.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/accountShop/delete", function (data) {
            Feng.success("删除成功!");
            AccountShop.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("accountShopId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商家账户列表
 */
AccountShop.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['userType'] = $("#userType").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    AccountShop.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AccountShop.initColumn();
    var table = new BSTable(AccountShop.id, "/accountShop/list", defaultColunms);
    table.setPaginationType("server");
    AccountShop.table = table.init();
});
//导出Excel
AccountShop.export= function(){
	$('#'+AccountShop.id).tableExport({
		fileName:'商家账户记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}