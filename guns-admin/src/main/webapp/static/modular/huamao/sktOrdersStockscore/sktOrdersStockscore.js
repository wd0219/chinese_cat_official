/**
 * 购买积分管理初始化
 */
var SktOrdersStockscore = {
    id: "SktOrdersStockscoreTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktOrdersStockscore.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '订单编号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '用户Id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号码', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '申请积分数', field: 'score', visible: true, align: 'center', valign: 'middle'},
            {title: '订单金额 ', field: 'totalMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '支付方式 ', field: 'payType', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "线下现金支付";
        			break;
        		case 2:
        			result = "现金账户支付";
        			break;
        		case 3:
        			result = "华宝支付";
        			break;
        		case 4:
        			result = "第三方支付";
        			break;
        		case 5:
        			result = "混合支付";
        			break;
        		case 6:
        			result = "华宝营业额";
        			break;
        		}
        		return result;
        	}},
            {title: '第三方支付', field: 'realMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '现金账户', field: 'cash', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝', field: 'kaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝费用', field: 'kaiyuanFee', visible: true, align: 'center', valign: 'middle'},
            {title: '订单备注', field: 'orderRemarks', visible: true, align: 'center', valign: 'middle'},
            {title: '订单状态', field: 'orderStatus', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "下单";
        			break;
        		case 2:
        			result = "平台取消";
        			break;
        		case 3:
        			result = "已完成";
        			break;
        		}
        		return result;
        	}},
            {title: '下单时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: '', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktOrdersStockscore.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktOrdersStockscore.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加购买积分
 */
SktOrdersStockscore.openAddSktOrdersStockscore = function () {
    var index = layer.open({
        type: 2,
        title: '添加购买积分',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktOrdersStockscore/sktOrdersStockscore_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看购买积分详情
 */
SktOrdersStockscore.openSktOrdersStockscoreDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '购买积分详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktOrdersStockscore/sktOrdersStockscore_update/' + SktOrdersStockscore.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除购买积分
 */
SktOrdersStockscore.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktOrdersStockscore/delete", function (data) {
            Feng.success("删除成功!");
            SktOrdersStockscore.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktOrdersStockscoreId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询购买积分列表
 */
SktOrdersStockscore.search = function () {
    var queryData = {};
    queryData['orderNo'] = $("#orderNo").val();
    queryData['userPhone'] = $("#userPhone").val();
    SktOrdersStockscore.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktOrdersStockscore.initColumn();
    var table = new BSTable(SktOrdersStockscore.id, "/sktOrdersStockscore/list", defaultColunms);
    table.setPaginationType("server");
    SktOrdersStockscore.table = table.init();
});
