/**
 * 初始化购买积分详情对话框
 */
var SktOrdersStockscoreInfoDlg = {
    sktOrdersStockscoreInfoData : {}
};

/**
 * 清除数据
 */
SktOrdersStockscoreInfoDlg.clearData = function() {
    this.sktOrdersStockscoreInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrdersStockscoreInfoDlg.set = function(key, val) {
    this.sktOrdersStockscoreInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrdersStockscoreInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktOrdersStockscoreInfoDlg.close = function() {
    parent.layer.close(window.parent.SktOrdersStockscore.layerIndex);
}

/**
 * 收集数据
 */
SktOrdersStockscoreInfoDlg.collectData = function() {
    this
    .set('orderId')
    .set('orderNo')
    .set('userId')
    .set('orderStatus')
    .set('payType')
    .set('totalMoney')
    .set('realMoney')
    .set('cash')
    .set('kaiyuan')
    .set('kaiyuanFee')
    .set('score')
    .set('orderRemarks')
    .set('createTime')
    .set('checkTime')
    .set('dataFlag');
}

/**
 * 提交添加
 */
SktOrdersStockscoreInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrdersStockscore/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktOrdersStockscore.table.refresh();
        SktOrdersStockscoreInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrdersStockscoreInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktOrdersStockscoreInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrdersStockscore/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktOrdersStockscore.table.refresh();
        SktOrdersStockscoreInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrdersStockscoreInfoData);
    ajax.start();
}

$(function() {

});
