/**
 * 初始化商品属性详情对话框
 */
var AttributesInfoDlg = {
    attributesInfoData : {}
};

/**
 * 清除数据
 */
AttributesInfoDlg.clearData = function() {
    this.attributesInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AttributesInfoDlg.set = function(key, val) {
    this.attributesInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AttributesInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AttributesInfoDlg.close = function() {
    parent.layer.close(window.parent.Attributes.layerIndex);
}

/**
 * 收集数据
 */
AttributesInfoDlg.collectData = function() {
    this
    .set('attrId')
    .set('goodsCatId')
    .set('goodsCatPath')
    .set('attrName')
    .set('attrType')
    .set('attrVal')
    .set('attrSort')
    .set('isShow')
    .set('dataFlag')
    .set('createTime')
    .set("goods1")
    .set("goods2")
    .set("goods3");
}

/**
 * 提交添加
 */
AttributesInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var goods1 = $("#goods1").val();
    var attrName = $("#attrName").val();
    var attrType = $("#attrType").val();
    var attrSort = $("#attrSort").val();

    if(goods1 == null || goods1 ==""){
        var html='<span class="error">所属分类不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#goods1').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(attrName == null || attrName ==""){
        var html='<span class="error">属性名称不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#attrName').after(html);
        falg = 1;
    }

    if(!isRealNum(attrSort)){
        var html='<span class="error">排序号必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#attrSort').after(html);
        falg = 1;
    }

    if(attrType == null || attrType ==""){
        var html='<span class="error">属性类型不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#attrType').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    var radios = document.getElementsByName("isShow");
    // alert(radios);
     for ( var i = 0; i < radios.length; i++) {
 	    if (radios[i].checked==true){
 	    	this.attributesInfoData.isShow=$(radios[i]).val();
 	    }
     }
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/attributes/add", function(data){
        Feng.success("添加成功!");
        window.parent.Attributes.table.refresh();
        AttributesInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.attributesInfoData);
    ajax.start();
    }else{
    Feng.success("请核实添加字段!");
    }
}

function isRealNum(val){
    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
    if(val === "" || val ==null){
        return false;
    }
    if(!isNaN(val)){
        return true;
    }else{
        return false;
    }
}

/**
 * 提交修改
 */
AttributesInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var goods1 = $("#goods1").val();
    var attrName = $("#attrName").val();
    var attrType = $("#attrType").val();
    var attrSort = $("#attrSort").val();

    if(goods1 == null || goods1 ==""){
        var html='<span class="error">所属分类不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#goods1').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(attrName == null || attrName ==""){
        var html='<span class="error">属性名称不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#attrName').after(html);
        falg = 1;
    }

    if(!isRealNum(attrSort)){
        var html='<span class="error">排序号必须为数字切不能为空!</span>';
        //$('#menuSort').parent().append(html);
        $('#attrSort').after(html);
        falg = 1;
    }

    if(attrType == null || attrType ==""){
        var html='<span class="error">属性类型不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#attrType').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    
    let isShow = $('input:radio[name=isShow]:checked').val();
    
    this.attributesInfoData.isShow = isShow ;

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/attributes/update", function(data){
        Feng.success("修改成功!");
        window.parent.Attributes.table.refresh();
        AttributesInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.attributesInfoData);
    ajax.start();
    }else{
    Feng.success("请核实添加字段!");
}
}

$(function() {
	$('#attrType').val($('#attrType1').val());
    if($('#goodsCatId3').val()!='0'){
        $('#goods1').val($('#goodsCatId3').val());
        if($('#goods1').val()!=0){
            $('#goods1').val($('#goodsCatId3').val());
            $('#goods1').trigger("change");
        }
        if($('#goodsCatId2').val()!=0){
            $('#goods2').val($('#goodsCatId2').val());
            $('#goods2').trigger("change");
        }
        if($('#goodsCatId1').val()!=0){
            $('#goods3').val($('#goodsCatId1').val());
            $('#goods3').trigger("change");
        }
    }else{
        if($('#goodsCatId2').val()!='0'){
            $('#goods1').val($('#goodsCatId2').val());
            if($('#goods1').val()!=0){
                $('#goods1').val($('#goodsCatId2').val());
                $('#goods1').trigger("change");
            }
            if($('#goodsCatId3').val()!=0){
                $('#goods2').val($('#goodsCatId3').val());
                $('#goods2').trigger("change");
            }
        }else{
            $('#goods1').val($('#goodsCatId1').val());
            if($('#goods1').val()!=0){
                $('#goods1').val($('#goodsCatId1').val());
                $('#goods1').trigger("change");
            }
        }

    }
});
