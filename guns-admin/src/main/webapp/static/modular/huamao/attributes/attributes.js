/**
 * 商品属性管理初始化
 */
var Attributes = {
    id: "AttributesTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Attributes.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
        	{title: '属性名称', field: 'attrName', visible: true, align: 'center', valign: 'middle'},
        	{title: '所属商品分类', field: 'goodsCatPath', visible: true, align: 'center', valign: 'middle'},
        	{title: '属性类型' , field: 'attrType', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
               		var result;
               		switch(value){
               		case 2:
               			result = "下拉框";
               			break;
               		case 1:
               			result = "多选框";
               			break;
               		case 0:
               			result = "输入框 ";
               			break;
               		}

               		return result;
            }},
        	{title: '属性值', field: 'attrVal', visible: true, align: 'center', valign: 'middle'},
        	{title: '是否显示 ', field: 'isShow', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
               		var result;
               		switch(value){
               		case 1:
               			result = "显示";
               			break;
               		case 0:
               			result = "不显示 ";
               			break;
               		}

               		return result;
            }},
        	{title: '排序号', field: 'attrSort', visible: true, align: 'center', valign: 'middle'},
        	{title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:Attributes.open}
        	 
        	 
        	
           /* {title: '最后一级商品分类ID', field: 'goodsCatId', visible: true, align: 'center', valign: 'middle'},
            
          
           
           
         
           
            {title: '有效状态 1：有效 -1：无效', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}*/
    ];
};
Attributes.open = function () {
	return "<a href='javascript:void(0)' onclick=\"Attributes.openAttributesDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"Attributes.delete2()\">删除</a>"
}
/**
 * 检查是否选中
 */
Attributes.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Attributes.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品属性
 */
Attributes.openAddAttributes = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品属性',
        area:['100%', '100%'],  //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/attributes/attributes_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品属性详情
 */
Attributes.openAttributesDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品属性详情',
            area: ['100%', '100%'],  //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/attributes/attributes_update/' + Attributes.seItem.attrId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品属性
 */
Attributes.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/attributes/delete", function (data) {
            Feng.success("删除成功!");
            Attributes.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("attributesId",this.seItem.id);
        ajax.start();
    }
};
/**
 * 删除商品属性
 */
Attributes.delete2 = function () {
    if (this.check()) {

        var operation = function(){
        	 var attributesId = Attributes.seItem.attrId;
        	var ajax = new $ax(Feng.ctxPath + "/attributes/delete", function (data) {
                Feng.success("删除成功!");
                Attributes.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("attributesId",attributesId);
            ajax.start();
        };

        Feng.confirm("是否删除 该条记录?",operation);
    }
};
/**
 * 查询商品属性列表
 */
Attributes.search = function () {
    var queryData = {};
    queryData['attrName'] = $("#attrName").val();
    queryData['goods1'] = $("#goods1").val();
    queryData['goods2'] = $("#goods2").val();
    queryData['goods3'] = $("#goods3").val();
    Attributes.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Attributes.initColumn();
    var table = new BSTable(Attributes.id, "/attributes/list", defaultColunms);
    table.setPaginationType("server");
    Attributes.table = table.init();
});
