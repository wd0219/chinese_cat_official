/**
 * 第三方支付记录管理初始化
 */
var ThirdSupplement = {
    id: "ThirdSupplementTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ThirdSupplement.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '用户手机号码', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '支付类型', field: 'payType', visible: true, align: 'center', valign: 'middle'},
            {title: '内部业务订单', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '第三方支付流水单号', field: 'outTradeNo', visible: true, align: 'center', valign: 'middle'},
            {title: '业务类型', field: 'businessType', visible: true, align: 'center', valign: 'middle'},
            {title: '支付金额', field: 'payMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '请求数据', field: 'request', visible: true, align: 'center', valign: 'middle'},
            {title: '请求创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '响应数据', field: 'response', visible: true, align: 'center', valign: 'middle'},
            {title: '响应时间', field: 'responseTime', visible: true, align: 'center', valign: 'middle'},
            {title: '支付状态', field: 'payStatus', visible: true, align: 'center', valign: 'middle'},
            {title: '是否手动补单', field: 'handleStatus', visible: true, align: 'center', valign: 'middle'},
            //{title: '商户号', field: 'ino', visible: true, align: 'center', valign: 'middle'},
            //{title: '银行流水号或支付渠道号', field: 'payTypeOrderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '删除标记 0删除 1显示）', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
ThirdSupplement.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ThirdSupplement.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加第三方支付记录
 */
ThirdSupplement.openAddThirdSupplement = function () {
    var index = layer.open({
        type: 2,
        title: '添加第三方支付记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/ThirdSupplement/ThirdSupplement_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看第三方支付记录详情
 */
ThirdSupplement.openThirdSupplementDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '第三方支付记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/ThirdSupplement/ThirdSupplement_update/' + ThirdSupplement.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除第三方支付记录
 */
ThirdSupplement.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/ThirdSupplement/delete", function (data) {
            Feng.success("删除成功!");
            ThirdSupplement.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("ThirdSupplementId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询第三方支付记录列表
 */
ThirdSupplement.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['orderNo'] = $("#orderNo").val();
    queryData['outTradeNo'] = $("#outTradeNo").val();
    queryData['businessType'] = $("#businessType").val();
    queryData['payType'] = $("#payType").val();
    queryData['payStatus'] = $("#payStatus").val();
    queryData['handleStatus'] = $("#handleStatus").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    ThirdSupplement.table.refresh({query: queryData});
};
/**
 * 补单
 */
ThirdSupplement.supplement = function(){
    var index = layer.open({
        type: 2,
        title: '补单',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/thirdSupplement/supplement'
    });
    this.layerIndex = index;
}
$(function () {
    var defaultColunms = ThirdSupplement.initColumn();
    var table = new BSTable(ThirdSupplement.id, "/ThirdSupplement/list", defaultColunms);
    table.setPaginationType("client");
    ThirdSupplement.table = table.init();
});
//导出Excel
ThirdSupplement.export= function(){
	$('#'+ThirdSupplement.id).tableExport({
		fileName:'第三方支付记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}