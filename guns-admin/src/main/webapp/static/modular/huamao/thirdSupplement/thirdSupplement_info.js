/**
 * 初始化用户提现详情对话框
 */
var ThirdSupplementInfoDlg = {
    ThirdSupplementInfoData : {}
};

/**
 * 清除数据
 */
ThirdSupplementInfoDlg.clearData = function() {
    this.ThirdSupplementInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ThirdSupplementInfoDlg.set = function(key, val) {
    this.ThirdSupplementInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ThirdSupplementInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ThirdSupplementInfoDlg.close = function() {
    parent.layer.close(window.parent.ThirdSupplement.layerIndex);
}

/**
 * 收集数据
 */
ThirdSupplementInfoDlg.collectData = function() {
    this
    .set('drawId')
    .set('drawNo')
    .set('userId')
    .set('bankCardId')
    .set('type')
    .set('money')
    .set('fee')
    .set('ratio')
    .set('taxratio')
    .set('cashSatus')
    .set('getTime')
    .set("ybeginTime")
    .set("yendTime")
    .set('createTime')
    .set('checkRemark')
    .set('checkStaffId')
    .set('checkTime');
}

/**
 * 提交添加
 */
ThirdSupplementInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ThirdSupplement/add", function(data){
        Feng.success("添加成功!");
        window.parent.ThirdSupplement.table.refresh();
        ThirdSupplementInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ThirdSupplementInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ThirdSupplementInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ThirdSupplement/update", function(data){
        Feng.success("修改成功!");
        window.parent.ThirdSupplement.table.refresh();
        ThirdSupplementInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ThirdSupplementInfoData);
    ajax.start();
}
/**
 * 批量拒绝
 */
ThirdSupplementInfoDlg.batchEdit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ThirdSupplement/batchEdit", function(data){
        if(data == "true"){
            Feng.success("拒绝成功!");
        }else{
            Feng.error("未查询到提现记录!");
        }
        window.parent.ThirdSupplement.table.refresh();
        ThirdSupplementInfoDlg.close();
    },function(data){
        Feng.error("拒绝失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ThirdSupplementInfoData);
    ajax.start();
}
/**
 * 审核拒绝
 */
ThirdSupplementInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/ThirdSupplement/updateSatus", function(data){
        Feng.success("success!");
        window.parent.ThirdSupplement.table.refresh();
        ThirdSupplementInfoDlg.close();
    },function(data){
        Feng.error("拒绝失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.ThirdSupplementInfoData);
    ajax.start();
}
$(function() {

});
