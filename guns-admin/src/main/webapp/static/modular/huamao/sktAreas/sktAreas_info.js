/**
 * 初始化地区管理详情对话框
 */
var SktAreasInfoDlg = {
    sktAreasInfoData : {}
};

/**
 * 清除数据
 */
SktAreasInfoDlg.clearData = function() {
    this.sktAreasInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAreasInfoDlg.set = function(key, val) {
    this.sktAreasInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAreasInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktAreasInfoDlg.close = function() {
    parent.layer.close(window.parent.SktAreas.layerIndex);
}

/**
 * 收集数据
 */
SktAreasInfoDlg.collectData = function() {
    this
    .set('areaId')
    .set('parentId')
    .set('areaName')
    .set('isShow')
    .set('areaSort')
    .set('areaKey')
    .set('areaType')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
SktAreasInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var areaName = $("#areaName").val();
    var areaKey = $("#areaKey").val();
    var areaSort = $("#areaSort").val();
    var isShow = $("#isShow").val();

    if(areaName == null || areaName ==""){
        var html='<span class="error">地区名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#areaName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(areaKey == null || areaKey ==""){
        var html='<span class="error">地区首字母不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#areaKey').after(html);
        falg = 1;
    }

    if(!isRealNum(areaSort)){
        var html='<span class="error">排序号必须为数字默认为0!</span>';
        //$('#menuSort').parent().append(html);
        $('#areaSort').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktAreas/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktAreas.table.refresh();
        SktAreasInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAreasInfoData);
    ajax.start();
}else{
    Feng.success("请核实添加字段!");
	}
}
	function isRealNum(val){
	    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	    if(val === "" || val ==null){
	        return false;
	    }
	    if(!isNaN(val)){
	        return true;
	    }else{
	        return false;
	    }
	}

/**
 * 提交修改
 */
SktAreasInfoDlg.editSubmit = function() {
	$('.col-sm-9 span').html("");
    var falg = 0;

    var areaName = $("#areaName").val();
    var areaKey = $("#areaKey").val();
    var areaSort = $("#areaSort").val();

    if(areaName == null || areaName ==""){
        var html='<span class="error">地区名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#areaName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(areaKey == null || areaKey ==""){
        var html='<span class="error">地区首字母不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#areaKey').after(html);
        falg = 1;
    }

    if(!isRealNum(areaSort)){
        var html='<span class="error">排序号必须为数字默认为0!</span>';
        //$('#menuSort').parent().append(html);
        $('#areaSort').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    let isShow = $('input:radio[name=isShow]:checked').val();
    
    this.sktAreasInfoData.isShow = isShow ;
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktAreas/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktAreas.table.refresh();
        SktAreasInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAreasInfoData);
    ajax.start();
}else{
    Feng.success("请核实添加字段!");
}
}

$(function() {

});
