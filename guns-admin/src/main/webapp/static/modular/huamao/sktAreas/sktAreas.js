/**
 * 地区管理管理初始化
 */
var SktAreas = {
    id: "SktAreasTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktAreas.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '父ID', field: 'parentId', visible: true, align: 'center', valign: 'middle'},
            {title: '地区名称', field: 'areaName', visible: true, align: 'center', valign: 'middle'},
            {title: '是否显示', field: 'isShow', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
               		var result;
               		switch(value){
               		case 1:
               			result = "显示";
               			break;
               		case 0:
               			result = "不显示 ";
               			break;
               		}

               		return result;
            }},
            {title: '排序号', field: 'areaSort', visible: true, align: 'center', valign: 'middle'},
            {title: '地区首字母', field: 'areaKey', visible: true, align: 'center', valign: 'middle'},
            {title: '级别标志', field: 'areaType', visible: false, align: 'center', valign: 'middle'},
            {title: '删除标志', field: 'dataFlag', visible: false, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: false, align: 'center', valign: 'middle'},
            {title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:SktAreas.open}
    ];
};
SktAreas.open = function () {
	return "<a href='javascript:void(0)' onclick=\"SktAreas.openSktAreasAddChild()\">添加下一级</a>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"SktAreas.search2()\">查看</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktAreas.openSktAreasDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktAreas.delete2()\">删除</a>"
}
/**
 * 检查是否选中
 */
SktAreas.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktAreas.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加地区管理
 */
SktAreas.openAddSktAreas = function () {
	
    var index = layer.open({
        type: 2,
        title: '添加地区管理',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktAreas/sktAreas_add/'
    });
    this.layerIndex = index;
};
/**
 * 添加子类地区
 */
SktAreas.openSktAreasAddChild = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '添加地区',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktAreas/sktAreas_addChild/' + SktAreas.seItem.areaId
        });
        this.layerIndex = index;
    }
};
/**
 * 打开查看地区管理详情
 */
//$('#'+BrandsInfoDlg.id).bootstrapTable("load",data);
//$('#fanhui').show(); 
SktAreas.openSktAreasDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '地区管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktAreas/sktAreas_update/' + SktAreas.seItem.areaId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除地区管理
 */
SktAreas.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktAreas/delete", function (data) {
            Feng.success("删除成功!");
            SktAreas.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktAreasId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 删除地区管理
 */
SktAreas.delete2 = function () {
    if (this.check()) {
    	var areaId=this.seItem.areaId;
    	var operation=function(){
        var ajax = new $ax(Feng.ctxPath + "/sktAreas/delete", function (data) {
            Feng.success("删除成功!");
            SktAreas.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktAreasId",areaId);
        ajax.start();
    };
    	  Feng.confirm("是否删除该条记录?",operation);
    }
};
/**
 * 查询地区管理列表
 */
SktAreas.search = function () {
    var queryData = {};
    queryData['areaName'] = $("#areaName").val();
  //  queryData['areaId'] = 0;
    SktAreas.table.refresh({query: queryData});
};

SktAreas.search2 = function(){
	if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktAreas/list1", function (data) {
        	console.log(data);
        	$('#' + SktAreas.id).bootstrapTable("load",data);
            //Feng.success("删除成功!");
            //SktAreas.table.refresh();
        }, function (data) {
            Feng.error("查找失败!" + data.responseJSON.message + "!");
        });
        ajax.set("areaId",this.seItem.areaId);
        ajax.start();
    }
}

SktAreas.goback=function(){
	var selected = $('#' + this.id).bootstrapTable('getData');
	var a = selected[0].parentId;
	var ajax = new $ax(Feng.ctxPath + "/sktAreas/list2", function (data) {
    	console.log(data);
    	$('#' + SktAreas.id).bootstrapTable("load",data);
        //Feng.success("删除成功!");
        //SktAreas.table.refresh();
    }, function (data) {
        Feng.error("查找失败!" + data.responseJSON.message + "!");
    });
	
	ajax.set("areaId",a);
    ajax.start();
	//window.history.go(-1);
}

$(function () {
	//$('#fanhui').hide();
	//window.location.href="/sktAreas/";
    var defaultColunms = SktAreas.initColumn();
    var table = new BSTable(SktAreas.id, "/sktAreas/list1", defaultColunms);
    table.setPaginationType("client");
    var queryData = {};
    queryData['areaId'] = 0;
    table.setQueryParams(queryData);
    SktAreas.table = table.init();
	/*var defaultColunms = SktAreas.initColumn();
	var table = new BSTreeTable(SktAreas.id, "/sktAreas/list", defaultColunms);
	table.setExpandColumn(0);
	table.setIdField("areaId");
	table.setCodeField("areaId");
	table.setParentCodeField("parentId");
	table.setExpandAll(false);
	table.init();
	GoodsCats.table = table;*/
});
