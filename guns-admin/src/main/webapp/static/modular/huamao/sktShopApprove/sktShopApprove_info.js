/**
 * 初始化详情对话框
 */
var SktShopApproveInfoDlg = {
    sktShopApproveInfoData : {}
};

/**
 * 清除数据
 */
SktShopApproveInfoDlg.clearData = function() {
    this.sktShopApproveInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShopApproveInfoDlg.set = function(key, val) {
    this.sktShopApproveInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktShopApproveInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktShopApproveInfoDlg.close = function() {
    parent.layer.close(window.parent.SktShopApprove.layerIndex);
}

/**
 * 收集数据
 */
SktShopApproveInfoDlg.collectData = function() {
    this
    .set('applyId')
    .set('userId')
    .set('provinceId')
    .set('cityId')
    .set('areaId')
    .set('isLegal')
    .set('legalPerson')
    .set('IDnumber')
    .set('legalPersonImg')
    .set('companyName')
    .set('licenseNo')
    .set('companyType')
    .set('licenseMerge')
    .set('licenseImg')
    .set('codeImg')
    .set('taxImg')
    .set('authorizationImg')
    .set('industry')
    .set('shopName')
    .set('address')
    .set('telephone')
    .set('logo')
    .set('shopStatus')
    .set('statusDesc')
    .set('createTime')
    .set('checkTime')
    .set('checkStaffId');
}

/**
 * 提交添加
 */
SktShopApproveInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktShopApprove/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktShopApprove.table.refresh();
        SktShopApproveInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShopApproveInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktShopApproveInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktShopApprove/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktShopApprove.table.refresh();
        SktShopApproveInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktShopApproveInfoData);
    ajax.start();
}

$(function() {

});
