/**
 * 管理初始化
 */
var SktShopApprove = {
    id: "SktShopApproveTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktShopApprove.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照所在省id', field: 'provinceId', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照所在市id', field: 'cityId', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照所在区县id', field: 'areaId', visible: true, align: 'center', valign: 'middle'},
            {title: '是否法人 1法人 0不是法人', field: 'isLegal', visible: true, align: 'center', valign: 'middle'},
            {title: '法人', field: 'legalPerson', visible: true, align: 'center', valign: 'middle'},
            {title: '法人身份证号', field: 'IDnumber', visible: true, align: 'center', valign: 'middle'},
            {title: '法人手持身份证照片', field: 'legalPersonImg', visible: true, align: 'center', valign: 'middle'},
            {title: '公司名称', field: 'companyName', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照号码', field: 'licenseNo', visible: true, align: 'center', valign: 'middle'},
            {title: '企业类型：0个体户 1公司', field: 'companyType', visible: true, align: 'center', valign: 'middle'},
            {title: '是否三证合一：0未合并 1合并', field: 'licenseMerge', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照', field: 'licenseImg', visible: true, align: 'center', valign: 'middle'},
            {title: '组织机构代码证', field: 'codeImg', visible: true, align: 'center', valign: 'middle'},
            {title: '税务登记证', field: 'taxImg', visible: true, align: 'center', valign: 'middle'},
            {title: '授权书', field: 'authorizationImg', visible: true, align: 'center', valign: 'middle'},
            {title: '行业ID', field: 'industry', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '经营地址', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '经营电话', field: 'telephone', visible: true, align: 'center', valign: 'middle'},
            {title: 'logo', field: 'logo', visible: true, align: 'center', valign: 'middle'},
            {title: '门店状态：-1拒绝0未审核1已审核', field: 'shopStatus', visible: true, align: 'center', valign: 'middle'},
            {title: '状态说明(一般用于停止和拒绝说明)', field: 'statusDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核时间', field: 'checkTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核人', field: 'checkStaffId', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktShopApprove.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktShopApprove.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
SktShopApprove.openAddSktShopApprove = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktShopApprove/sktShopApprove_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
SktShopApprove.openSktShopApproveDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktShopApprove/sktShopApprove_update/' + SktShopApprove.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
SktShopApprove.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktShopApprove/delete", function (data) {
            Feng.success("删除成功!");
            SktShopApprove.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktShopApproveId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
SktShopApprove.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktShopApprove.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktShopApprove.initColumn();
    var table = new BSTable(SktShopApprove.id, "/sktShopApprove/list", defaultColunms);
    table.setPaginationType("client");
    SktShopApprove.table = table.init();
});
