/**
 * 初始化商品规格详情对话框
 */
var GoodsSpecsInfoDlg = {
    goodsSpecsInfoData : {}
};

/**
 * 清除数据
 */
GoodsSpecsInfoDlg.clearData = function() {
    this.goodsSpecsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsSpecsInfoDlg.set = function(key, val) {
    this.goodsSpecsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsSpecsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
GoodsSpecsInfoDlg.close = function() {
    parent.layer.close(window.parent.GoodsSpecs.layerIndex);
}

/**
 * 收集数据
 */
GoodsSpecsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('shopId')
    .set('goodsId')
    .set('productNo')
    .set('specIds')
    .set('marketPrice')
    .set('specPrice')
    .set('specStock')
    .set('warnStock')
    .set('saleNum')
    .set('isDefault')
    .set('dataFlag');
}

/**
 * 提交添加
 */
GoodsSpecsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsSpecs/add", function(data){
        Feng.success("添加成功!");
        window.parent.GoodsSpecs.table.refresh();
        GoodsSpecsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsSpecsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
GoodsSpecsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsSpecs/update", function(data){
        Feng.success("修改成功!");
        window.parent.GoodsSpecs.table.refresh();
        GoodsSpecsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsSpecsInfoData);
    ajax.start();
}

$(function() {

});
