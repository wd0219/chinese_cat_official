/**
 * 商品规格管理初始化
 */
var GoodsSpecs = {
    id: "GoodsSpecsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
GoodsSpecs.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '店铺Id', field: 'shopId', visible: true, align: 'center', valign: 'middle'},
            {title: '商品Id', field: 'goodsId', visible: true, align: 'center', valign: 'middle'},
            {title: '商品货号', field: 'productNo', visible: true, align: 'center', valign: 'middle'},
            {title: '规格ID格式', field: 'specIds', visible: true, align: 'center', valign: 'middle'},
            {title: '市场价', field: 'marketPrice', visible: true, align: 'center', valign: 'middle'},
            {title: '商品价', field: 'specPrice', visible: true, align: 'center', valign: 'middle'},
            {title: '库存', field: 'specStock', visible: true, align: 'center', valign: 'middle'},
            {title: '预警值', field: 'warnStock', visible: true, align: 'center', valign: 'middle'},
            {title: '销量', field: 'saleNum', visible: true, align: 'center', valign: 'middle'},
            {title: '默认规格', field: 'isDefault', visible: true, align: 'center', valign: 'middle'},
            {title: '有效状态', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
GoodsSpecs.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        GoodsSpecs.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品规格
 */
GoodsSpecs.openAddGoodsSpecs = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品规格',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/goodsSpecs/goodsSpecs_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品规格详情
 */
GoodsSpecs.openGoodsSpecsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品规格详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goodsSpecs/goodsSpecs_update/' + GoodsSpecs.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品规格
 */
GoodsSpecs.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/goodsSpecs/delete", function (data) {
            Feng.success("删除成功!");
            GoodsSpecs.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("goodsSpecsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商品规格列表
 */
GoodsSpecs.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    GoodsSpecs.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = GoodsSpecs.initColumn();
    var table = new BSTable(GoodsSpecs.id, "/goodsSpecs/list", defaultColunms);
    table.setPaginationType("client");
    GoodsSpecs.table = table.init();
});
