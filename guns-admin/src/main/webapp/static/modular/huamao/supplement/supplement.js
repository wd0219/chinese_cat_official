/**
 * 页面初始化时加载
 */
var userPhone = null;
var num = null;
var userId = null;
var boo = true;
/**
 * 提交
 */
	function submint() {
	   // 获取手动补单类型传来的值
	    var decide = $("#supplementType").val();
	    //判断补单类型
	    if(decide != 0){
	    	if(decide == 1){
		    	var object = 1;
		    	var type = 1;
		    }else if(decide == 2){
		    	var object = 1;
		    	var type = -1;
		    }else if(decide == 3){
		    	var object = 2;
		    	var type = 1;
		    }else if(decide == 4){
		    	var object = 2;
		    	var type = -1;
		    }else if(decide == 5){
		    	var object = 3;
		    	var type = 1;
		    }else if(decide == 6){
		    	var object = 3;
		    	var type = -1;
		    }else if(decide == 7){
		    	var object = 4;
		    	var type = 1;
		    }else if(decide == 8){
		    	var object = 4;
		    	var type = -1;
		    }else if(decide == 9){
                var object = 5;
                var type = 1;
			}
		    if($("#userPhone").val() != null && $("#userPhone").val() != ''){
	    		if(boo){
	    			if($("#num").val() == null || $("#num").val() == ""){
                        $("#numS").html("<font color='red'>不能为空</font>");
                    }else{
                        if($("#num").val() <= 0){
                            Feng.info("补单额度不能为0或是小于0！");
                        }else{
                            var queryData = {};
                            queryData['object'] = object;
                            queryData['type'] = type;
                            queryData['userPhone'] = userPhone;
                            queryData['loginName'] = userPhone;
                            queryData['num'] = num;
                            queryData['remark'] = $("#remark").val();
                            $.post('/supplement/add',
                                { "userId": userId, "object": object, "type": type, "userPhone": userPhone, "loginName": userPhone, "num": num, "remark": $("#remark").val()},
                                function (req) {
                                    if(req.code == 1){
                                        Feng.success("操作成功!");
                                        //重置页面
                                        $("#supplementType").val(0);
                                        $("#userPhone").val("");
                                        $("#num").val("");
                                        $("#remark").val("");
                                        $("#phoneS").html("");
                                    }else{
                                        Feng.error(req.msg);
                                    }
                                });
                        }
					}
				}
			}else{
                num = $("#num").val();
                if(num =='' || num == null){
                    $("#numS").html("<font color='red'>不能为空</font>");
                    //如果补单额度为空将num设置为空
                    num = null;
                }
                $("#phoneS").html("<font color='red'>不能为空</font>");
                Feng.info("请填写补单账户！");
			}
//	        $.ajax({  
//			    url:"/supplement/add",    //请求的url地址  
//			    dataType:"json",   //返回格式为json  
//			    async:true,//请求是否异步，默认为异步，这也是ajax重要特性  
//			    data:{"object":object,"type":type,"userPhone":userPhone,
//			    	"loginName":loginName,"num":num,"remark":remark},    //参数值  
//			    type:"GET",   //请求方式 get 或者post  
//			    success:function(req){  
//			        //请求成功时处理  
//			    	alert(req);
//			    },  
//			    error:function(){  
//			        alert(500);
//			    }  
//			});
	    }else{
            if($("#num").val() =='' || $("#num").val() == null){
                $("#numS").html("<font color='red'>不能为空</font>");
                //如果补单额度为空将num设置为空
                num = null;
            }
            if($("#userPhone").val() == '' || $("#userPhone").val() == null){
                $("#phoneS").html("<font color='red'>不能为空</font>");
			}
	    	Feng.info("请输入手动补单类型！");
	    }
	};
	function loseU(){
		//因为用户名和手机号同在一个框中，所以让它到后台用户名、手机号都是它查询
		userPhone = $("#userPhone").val();
		var loginName = $("#userPhone").val();
		//判断是否为空
		if(userPhone !='' && userPhone != null){
			$.ajax({  
			    url:"/supplement/checkUser?userPhone="+userPhone+" &loginName="+loginName,    //请求的url地址  
			    dataType:"json",   //返回格式为json  
			    async:true,//请求是否异步，默认为异步，这也是ajax重要特性  
			    //data:{"userPhone":upserPhone,"loginName":loginName},    //参数值  
			    type:"GET",   //请求方式 get 或者post  
			    success:function(req){  
			        //请求成功时处理  
			    	if(req == "no"){
			    		boo = false;
			    		$("#phoneS").html("<font color='red'>用户不存在</font>");
			    		//如果用户不存在将userPhone设置为空
			    		userPhone = null;
			    	}else{
			    		boo = true;
			    		userId = req.userId;
			    		if(req.userPhone == userPhone){
			    			$("#phoneS").html("<font color='green'>"+req.loginName+"</font>");
			    		}else{
			    			$("#phoneS").html("<font color='green'>"+req.userPhone+"</font>");
			    		}
			    	}
			    },  
			    error:function(){  
			        alert(500);
			    }  
			});
		}else{
			$("#phoneS").html("<font color='red'>不能为空</font>");
			//如果用户为空将userPhone设置为空
    		userPhone = null;
		}
	};
	function getU(){
		$("#phoneS").html("");
	};
	function loseN(){
		num = $("#num").val();
		if(num =='' || num == null){
			$("#numS").html("<font color='red'>不能为空</font>");
			//如果补单额度为空将num设置为空
    		num = null;
		}
	};
	function getN(){
		$("#numS").html("");
	};