/**
 * 初始化商家每日营业额统计详情对话框
 */
var SktSalesDateInfoDlg = {
    sktSalesDateInfoData : {}
};

/**
 * 清除数据
 */
SktSalesDateInfoDlg.clearData = function() {
    this.sktSalesDateInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktSalesDateInfoDlg.set = function(key, val) {
    this.sktSalesDateInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktSalesDateInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktSalesDateInfoDlg.close = function() {
    parent.layer.close(window.parent.SktSalesDate.layerIndex);
}

/**
 * 收集数据
 */
SktSalesDateInfoDlg.collectData = function() {
    this
    .set('id')
    .set('shopId')
    .set('userId')
    .set('date')
    .set('month')
    .set('storeTotal')
    .set('storeCash')
    .set('storeKaiyuan')
    .set('mallTotal')
    .set('mallCash')
    .set('mallKaiyuan')
    .set('profitTotal')
    .set('createTime');
}

/**
 * 提交添加
 */
SktSalesDateInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSalesDate/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktSalesDate.table.refresh();
        SktSalesDateInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktSalesDateInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktSalesDateInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSalesDate/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktSalesDate.table.refresh();
        SktSalesDateInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktSalesDateInfoData);
    ajax.start();
}

$(function() {

});
