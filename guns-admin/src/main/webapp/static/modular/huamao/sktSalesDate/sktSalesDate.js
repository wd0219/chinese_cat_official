/**
 * 商家每日营业额统计管理初始化
 */
var SktSalesDate = {
    id: "SktSalesDateTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktSalesDate.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户名', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '商家名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '营业日期', field: 'date', visible: true, align: 'center', valign: 'middle'},
            {title: '营业月份', field: 'month', visible: true, align: 'center', valign: 'middle'},
            {title: '门店营业总额', field: 'storeTotal', visible: true, align: 'center', valign: 'middle'},
            {title: '门店营业额现金部分', field: 'storeCash', visible: true, align: 'center', valign: 'middle'},
            {title: '门店营业额华宝部分', field: 'storeKaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '商城营业总额', field: 'mallTotal', visible: true, align: 'center', valign: 'middle'},
            {title: '商城营业额现金部分', field: 'mallCash', visible: true, align: 'center', valign: 'middle'},
            {title: '商城营业额华宝部分', field: 'mallKaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '商家参与分红总额', field: 'profitTotal', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktSalesDate.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktSalesDate.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商家每日营业额统计
 */
SktSalesDate.openAddSktSalesDate = function () {
    var index = layer.open({
        type: 2,
        title: '添加商家每日营业额统计',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktSalesDate/sktSalesDate_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商家每日营业额统计详情
 */
SktSalesDate.openSktSalesDateDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商家每日营业额统计详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktSalesDate/sktSalesDate_update/' + SktSalesDate.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商家每日营业额统计
 */
SktSalesDate.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktSalesDate/delete", function (data) {
            Feng.success("删除成功!");
            SktSalesDate.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktSalesDateId",this.seItem.id);
        ajax.start();
    }
};
//表格导出
SktSalesDate.export= function(){
	$('#'+SktSalesDate.id).tableExport({
		fileName:'线下商家购买牌匾',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}

/**
 * 查询商家每日营业额统计列表
 */
SktSalesDate.search = function () {
    var queryData = {};
    queryData['userName'] = $("#userName").val();
    queryData['shopName'] = $("#shopName").val();
    queryData['month'] = $("#month").val();
    queryData['createTime'] = $("#createTime").val();
    SktSalesDate.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktSalesDate.initColumn();
    var table = new BSTable(SktSalesDate.id, "/sktSalesDate/list", defaultColunms);
    table.setPaginationType("server");
    SktSalesDate.table = table.init();
});
