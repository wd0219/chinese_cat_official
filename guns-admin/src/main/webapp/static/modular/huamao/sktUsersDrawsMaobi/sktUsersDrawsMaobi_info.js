/**
 * 初始化用户猫币提现详情对话框
 */
var SktUsersDrawsMaobiInfoDlg = {
    sktUsersDrawsMaobiInfoData : {}
};

/**
 * 清除数据
 */
SktUsersDrawsMaobiInfoDlg.clearData = function() {
    this.sktUsersDrawsMaobiInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktUsersDrawsMaobiInfoDlg.set = function(key, val) {
    this.sktUsersDrawsMaobiInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktUsersDrawsMaobiInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktUsersDrawsMaobiInfoDlg.close = function() {
    parent.layer.close(window.parent.SktUsersDrawsMaobi.layerIndex);
}

/**
 * 收集数据
 */
SktUsersDrawsMaobiInfoDlg.collectData = function() {
    this
    .set('drawId')
    .set('drawNo')
    .set('userId')
    .set('type')
    .set('money')
    .set('maobi')
    .set('fee')
    .set('ratio')
    .set('taxratio')
    .set('cashSatus')
    .set('createTime')
    .set('checkRemark')
    .set('checkStaffId')
    .set('specialType');
}

/**
 * 提交添加
 */
SktUsersDrawsMaobiInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktUsersDrawsMaobi/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktUsersDrawsMaobi.table.refresh();
        SktUsersDrawsMaobiInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktUsersDrawsMaobiInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktUsersDrawsMaobiInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktUsersDrawsMaobi/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktUsersDrawsMaobi.table.refresh();
        SktUsersDrawsMaobiInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktUsersDrawsMaobiInfoData);
    ajax.start();
}

$(function() {

});
