/**
 * 用户猫币提现管理初始化
 */
var SktUsersDrawsMaobi = {
    id: "SktUsersDrawsMaobiTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktUsersDrawsMaobi.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            //{title: '自增ID', field: 'drawId', visible: true, align: 'center', valign: 'middle'},
            {title: '提现单号', field: 'drawNo', visible: true, align: 'center', valign: 'middle'},
            //{title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '用户昵称/登录名', field: 'loginName', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	return "<a href='javascript:void(0)' onclick='SktUsersDrawsMaobi.openSktUsersDrawsMaobiDetail()'>"+value+"</a>";
            }},
            {title: '真实姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号', field: 'cardID', visible: true, align: 'center', valign: 'middle'},
            {title: '提现的类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:SktUsersDrawsMaobi.tihuanType},
            {title: '到账金额', field: 'money', visible: true, align: 'center', valign: 'middle'},
            {title: '预计到账猫币', field: 'maobi', visible: true, align: 'center', valign: 'middle'},
            {title: '手续费+税率', field: 'fee', visible: true, align: 'center', valign: 'middle'},
            {title: '提现手续费比例 %', field: 'ratio', visible: true, align: 'center', valign: 'middle'},
            {title: '提现税率比例 %', field: 'taxratio', visible: true, align: 'center', valign: 'middle'},
            {title: '转为猫币状态 ', field: 'cashSatus', visible: true, align: 'center', valign: 'middle',formatter:SktUsersDrawsMaobi.tihuanCashSatus},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '拒绝理由', field: 'checkRemark', visible: true, align: 'center', valign: 'middle'},
            //{title: '审核员工ID', field: 'checkStaffId', visible: true, align: 'center', valign: 'middle'},
            {title: '是否手动处理', field: 'specialType', visible: true, align: 'center', valign: 'middle',formatter:SktUsersDrawsMaobi.tihuanSpecialType},
            {title: '操作', field: 'op', visible: true, align: 'center', valign: 'middle'}
    ];
};

SktUsersDrawsMaobi.tihuanType = function(value,row,index){
	//1开元宝提现 2现金提现 3开元宝营业额提现
	if(value == 1){
		return "开元宝提现 ";
	}else if(value == 2){
		return "现金提现";
	}else{
		return "开元宝营业额提现";
	}
}
SktUsersDrawsMaobi.tihuanCashSatus = function(value,row,index){
	// 0审核拒绝  1受理成功
	if(value == 0){
		return "审核拒绝";
	}else{
		return "受理成功";
	}
}

SktUsersDrawsMaobi.tihuanSpecialType = function(value,row,index){
	//0：非手动处理；2：手动处理成功；3：手动处理失败
	if(value == 0){
		return "非手动处理";
	}else if(value == 2){
		return "手动处理成功";
	}else {
		return "手动处理失败";
	}
}

/**
 * 检查是否选中
 */
SktUsersDrawsMaobi.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktUsersDrawsMaobi.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户猫币提现
 */
SktUsersDrawsMaobi.openAddSktUsersDrawsMaobi = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户猫币提现',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktUsersDrawsMaobi/sktUsersDrawsMaobi_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户猫币提现详情
 */
SktUsersDrawsMaobi.openSktUsersDrawsMaobiDetail = function () {
	var SktUsersDrawsMaobi2 = {
		    id: "SktUsersDrawsMaobiTable",	//表格id
		    seItem: null,		//选中的条目
		    table: null,
		    layerIndex: -1
		};
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户猫币提现详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
           // content: Feng.ctxPath + '/sktUsersDrawsMaobi/sktUsersDrawsMaobi_update/' + SktUsersDrawsMaobi.seItem.id
            content: Feng.ctxPath + '/account?loginName=' + SktUsersDrawsMaobi.seItem.loginName
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户猫币提现
 */
SktUsersDrawsMaobi.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktUsersDrawsMaobi/delete", function (data) {
            Feng.success("删除成功!");
            SktUsersDrawsMaobi.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktUsersDrawsMaobiId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户猫币提现列表
 */
SktUsersDrawsMaobi.search = function () {
    var queryData = {};
    queryData['drawNo'] = $("#drawNo").val();
    queryData['accNo'] = $("#accNo").val();
    queryData['loginName'] = $("#loginName").val();
    queryData['userPhone'] = $("#userPhone").val();
    queryData['cashSatus'] = $("#cashSatus").val();
    queryData['specialType'] = $("#specialType").val();
    queryData['ybeginTime'] = $("#ybeginTime").val();
    queryData['yendTime'] = $("#yendTime").val();
    queryData['cbeginTime'] = $("#cbeginTime").val();
    queryData['cendTime'] = $("#cendTime").val();
    SktUsersDrawsMaobi.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktUsersDrawsMaobi.initColumn();
    var table = new BSTable(SktUsersDrawsMaobi.id, "/sktUsersDrawsMaobi/list", defaultColunms);
    table.setPaginationType("server");
    SktUsersDrawsMaobi.table = table.init();
});
