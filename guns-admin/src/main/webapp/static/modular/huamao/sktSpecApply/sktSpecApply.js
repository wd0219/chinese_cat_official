/**
 * 商品规格申请管理初始化
 */
var SktSpecApply = {
    id: "SktSpecApplyTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktSpecApply.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {
                title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row, index) {
                    return index+1;
                }
            },
            //{title: '最后一级商品分类ID', field: 'goodsCatId', visible: true, align: 'center', valign: 'middle'},
            //{title: '商品分类路径', field: 'goodsCatPath', visible: true, align: 'center', valign: 'middle'},
            {title: '最后一级商品分类', field: 'lastClassifyPath', visible: true, align: 'center', valign: 'middle'},
            {title: '商品分类路径', field: 'goodClassifyPath', visible: true, align: 'center', valign: 'middle'},
            {title: '规格名称', field: 'catName', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'applicationStatus', visible: true, align: 'center', valign: 'middle',
                formatter:function(value,row,index){
                    var result;
                    switch(value){
                        case -1:
                            result = "已拒绝";
                            break;
                        case 0:
                            result = "未审核";
                            break;
                        case 1:
                            result = "已审核";
                            break;

                    }
                    return result;

                }
            },
            {title: '审核人', field: 'staffName', visible: true, align: 'center', valign: 'middle'},
            {title: '审核拒绝原因', field: 'refuseDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'complainId', visible: true, align: 'center', valign: 'middle',formatter:SktSpecApply.op},
    ];
};

/*
 * 操作
 * */
SktSpecApply.op = function (value,ros,index) {
    if(ros.applicationStatus==0) {
        return "<a href='javascript:void(0)' onclick=\"SktSpecApply.openSktSpecApplyDetail()\">审核</a>"
    }else {
        return "<a href='javascript:void(0)' onclick=\"SktSpecApply.openSktSpecApplyDetail()\">详情</a>"
    }
}



/**
 * 检查是否选中
 */
SktSpecApply.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktSpecApply.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品规格申请
 */
SktSpecApply.openAddSktSpecApply = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品规格申请',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktSpecApply/sktSpecApply_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品规格申请详情
 */
SktSpecApply.openSktSpecApplyDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品规格申请详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktSpecApply/sktSpecApply_update/' + SktSpecApply.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品规格申请
 */
SktSpecApply.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktSpecApply/delete", function (data) {
            Feng.success("删除成功!");
            SktSpecApply.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktSpecApplyId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商品规格申请列表
 */
SktSpecApply.search = function () {
    var queryData = {};
    queryData['goods1'] = $("#goods1").val();
    queryData['goods2'] = $("#goods2").val();
    queryData['goods3'] = $("#goods3").val();
    queryData['catName'] = $("#catName").val();
    queryData['applicationStatus'] = $("#applicationStatus").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    SktSpecApply.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktSpecApply.initColumn();
    var table = new BSTable(SktSpecApply.id, "/sktSpecApply/list", defaultColunms);
    table.setPaginationType("server");
    SktSpecApply.table = table.init();
});
