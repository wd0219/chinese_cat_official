/**
 * 初始化商品规格申请详情对话框
 */
var SktSpecApplyInfoDlg = {
    sktSpecApplyInfoData : {}
};

/**
 * 清除数据
 */
SktSpecApplyInfoDlg.clearData = function() {
    this.sktSpecApplyInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktSpecApplyInfoDlg.set = function(key, val) {
    this.sktSpecApplyInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktSpecApplyInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktSpecApplyInfoDlg.close = function() {
    parent.layer.close(window.parent.SktSpecApply.layerIndex);
}
SktSpecApplyInfoDlg.close2 = function() {
    parent.parent.layer.close(window.parent.parent.SktSpecApply.layerIndex);
}

/**
 * 收集数据
 */
SktSpecApplyInfoDlg.collectData = function() {
    this
    .set('id')
    .set('goodsCatId')
    .set('goodsCatPath')
    .set('catName')
    .set('applicationStatus')
    .set('staffId')
    .set('refuseDesc')
    .set('createTime');
}

/**
 * 提交添加
 */
SktSpecApplyInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSpecApply/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktSpecApply.table.refresh();
        SktSpecApplyInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktSpecApplyInfoData);
    ajax.start();
}

/**
 * 跳转页面
 */
SktSpecApplyInfoDlg.jump=function(){
    var sktSpecApplyId= document.getElementById("id").value
    var index = layer.open({
        type: 2,
        title: '商品规格申请详情',
        area: ['50%', '50%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktSpecApply/detail2/' +  sktSpecApplyId
    });
    this.layerIndex = index;
}

/**
 * 提交修改
 */
SktSpecApplyInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSpecApply/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktSpecApply.table.refresh();
        SktSpecApplyInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktSpecApplyInfoData);
    ajax.start();
}


SktSpecApplyInfoDlg.timedMsg = function() {
    SktSpecApplyInfoDlg.close2();
    SktSpecApplyInfoDlg.close();
}


/**
 * 提交修改
 */
SktSpecApplyInfoDlg.editSubmit2 = function() {
    this.clearData();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktSpecApply/update2", function(data){
        if(data.hasOwnProperty('SUCCESS')){
            Feng.success(data.SUCCESS);
            setTimeout(SktSpecApplyInfoDlg.timedMsg,2000);
            window.parent.parent.SktSpecApply.table.refresh();

            // SktSpecApplyInfoDlg.close2();
            // SktSpecApplyInfoDlg.close();
        }else{
            Feng.error(data.ERROR);
            setTimeout(SktSpecApplyInfoDlg.timedMsg,2000);
            window.parent.parent.SktSpecApply.table.refresh();
            // SktSpecApplyInfoDlg.close2();
            // SktSpecApplyInfoDlg.close();
        }
    });
    this.sktSpecApplyInfoData['id']=$("#id").val();
    this.sktSpecApplyInfoData['applicationStatus']=$("input:radio[name=applicationStatus]:checked").val();
    this.sktSpecApplyInfoData['refuseDesc']=$("#refuseDesc").val();
    this.sktSpecApplyInfoData['goodsCatId']=$("#goodsCatId").val();
    this.sktSpecApplyInfoData['goodsCatPath']=$("#goodsCatPath").val();
    this.sktSpecApplyInfoData['catName']=$("#catName").val();
    ajax.set(this.sktSpecApplyInfoData);
    ajax.start();

}


$(function() {
    var applicationStatus = $('#applicationStatus').val();
    switch(applicationStatus){
        case '0':
            $('#applicationStatus').val("未审核");
            break;
        case '1':
            $('#applicationStatus').val("已审核");
            $("#ensure").remove();
            $("#cancel").remove();
            break;
        case '-1':
            $('#applicationStatus').val("已拒绝");
            $("#ensure").remove();
            $("#cancel").remove();
            break;
        default:
            break;
    }
});
