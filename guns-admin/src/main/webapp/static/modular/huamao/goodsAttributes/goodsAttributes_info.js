/**
 * 初始化商品属性详情对话框
 */
var GoodsAttributesInfoDlg = {
    goodsAttributesInfoData : {}
};

/**
 * 清除数据
 */
GoodsAttributesInfoDlg.clearData = function() {
    this.goodsAttributesInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsAttributesInfoDlg.set = function(key, val) {
    this.goodsAttributesInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsAttributesInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
GoodsAttributesInfoDlg.close = function() {
    parent.layer.close(window.parent.GoodsAttributes.layerIndex);
}

/**
 * 收集数据
 */
GoodsAttributesInfoDlg.collectData = function() {
    this
    .set('id')
    .set('shopId')
    .set('goodsId')
    .set('attrId')
    .set('attrVal')
    .set('createTime');
}

/**
 * 提交添加
 */
GoodsAttributesInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsAttributes/add", function(data){
        Feng.success("添加成功!");
        window.parent.GoodsAttributes.table.refresh();
        GoodsAttributesInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsAttributesInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
GoodsAttributesInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsAttributes/update", function(data){
        Feng.success("修改成功!");
        window.parent.GoodsAttributes.table.refresh();
        GoodsAttributesInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsAttributesInfoData);
    ajax.start();
}

$(function() {

});
