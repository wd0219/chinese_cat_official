/**
 * 商品属性管理初始化
 */
var GoodsAttributes = {
    id: "GoodsAttributesTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
GoodsAttributes.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '门店ID', field: 'shopId', visible: true, align: 'center', valign: 'middle'},
            {title: '商品ID', field: 'goodsId', visible: true, align: 'center', valign: 'middle'},
            {title: '属性名称', field: 'attrId', visible: true, align: 'center', valign: 'middle'},
            {title: '属性值', field: 'attrVal', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
GoodsAttributes.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        GoodsAttributes.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品属性
 */
GoodsAttributes.openAddGoodsAttributes = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品属性',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/goodsAttributes/goodsAttributes_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品属性详情
 */
GoodsAttributes.openGoodsAttributesDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品属性详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goodsAttributes/goodsAttributes_update/' + GoodsAttributes.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品属性
 */
GoodsAttributes.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/goodsAttributes/delete", function (data) {
            Feng.success("删除成功!");
            GoodsAttributes.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("goodsAttributesId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商品属性列表
 */
GoodsAttributes.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    GoodsAttributes.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = GoodsAttributes.initColumn();
    var table = new BSTable(GoodsAttributes.id, "/goodsAttributes/list", defaultColunms);
    table.setPaginationType("client");
    GoodsAttributes.table = table.init();
});
