/**
 * 初始化视屏评论详情对话框
 */
var SktCommentInfoDlg = {
    sktCommentInfoData : {}
};

/**
 * 清除数据
 */
SktCommentInfoDlg.clearData = function() {
    this.sktCommentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktCommentInfoDlg.set = function(key, val) {
    this.sktCommentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktCommentInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktCommentInfoDlg.close = function() {
    parent.layer.close(window.parent.SktComment.layerIndex);
}

/**
 * 收集数据
 */
SktCommentInfoDlg.collectData = function() {
    this
    .set('commentId')
    .set('userId')
    .set('content')
    .set('creatTime');
}

/**
 * 提交添加
 */
SktCommentInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktComment/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktComment.table.refresh();
        SktCommentInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktCommentInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktCommentInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktComment/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktComment.table.refresh();
        SktCommentInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktCommentInfoData);
    ajax.start();
}

$(function() {

});
