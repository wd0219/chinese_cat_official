/**
 * 视屏评论管理初始化
 */
var SktComment = {
    id: "SktCommentTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktComment.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '序号', field: 'commentId', visible: true, align: 'center', valign: 'middle',
                formatter: function (value, row, index) {
                    return index+1;
                }
                },
            {title: '评论者昵称', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '评论视频', field: 'className', visible: true, align: 'center', valign: 'middle'},
            {title: '评论内容', field: 'content', visible: true, align: 'center', valign: 'middle'},
            {title: '评论时间', field: 'creatTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SktComment.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktComment.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加视屏评论
 */
SktComment.openAddSktComment = function () {
    var index = layer.open({
        type: 2,
        title: '添加视屏评论',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktComment/sktComment_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看视屏评论详情
 */
SktComment.openSktCommentDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '视屏评论详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktComment/sktComment_update/' + SktComment.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除视屏评论
 */
SktComment.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktComment/delete", function (data) {
            Feng.success("删除成功!");
            SktComment.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktCommentId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询视屏评论列表
 */
SktComment.search = function () {
    var queryData = {};
    queryData['className'] = $("#className").val();
    SktComment.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktComment.initColumn();
    var table = new BSTable(SktComment.id, "/sktComment/list", defaultColunms);
    table.setPaginationType("server");
    SktComment.table = table.init();
});
