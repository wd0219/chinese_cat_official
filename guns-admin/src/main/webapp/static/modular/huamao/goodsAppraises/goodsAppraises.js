/**
 * 商品评价管理初始化
 */
var GoodsAppraises = {
    id: "GoodsAppraisesTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
GoodsAppraises.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '订单编号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '商品名称', field: 'goodsName', visible: true, align: 'center', valign: 'middle'},
            {title: '商品主图', field: 'images', visible: true, align: 'center', valign: 'middle',
        	formatter:function(value,row,index){
        		var imgs = new Array();
        		if(typeof value != 'undefined' && value != ''){
        			imgs = value.split(",");
            		var img = '<a class = "view"  href="javascript:void(0)">';
            		for(var i = 0; i < imgs.length; i++){
            			var dd = orImg(imgs[i]);
            			img += '<img style="width:70;height:30px;"  src="'+dd+'" />';
            		}
            		img += '</a>';
            		return img;
        		}
        		return value;
        	}},
           {title: '商品评分', field: 'goodsScore', visible: true, align: 'center', valign: 'middle',
        		formatter:function(value,row,index){
            		var result;
            		switch(value){
            		case 1:
            			result = "<font color='#e08707'>★</font>";
            			break;
            		case 2:
            			result = "<font color='#e08707'>★★ </font>";
            			break;
            		case 3:
            			result = "<font color='#e08707'>★★★</font>";
            			break;
            		case 4:
            			result = "<font color='#e08707'>★★★★</font>";
            			break;
            		case 5:
            			result = "<font color='#e08707'>★★★★★</font>";
            			break;
            		}
            		return result;
            		
            	}
           },
           {title: '服务评分', field: 'serviceScore', visible: true, align: 'center', valign: 'middle',formatter:GoodsAppraises.color,
        	   formatter:function(value,row,index){
        		    var result;
	           		switch(value){
	           		case 1:
	           			result = "<font color='#e08707'>★</font>";
	           			break;
	           		case 2:
	           			result = "<font color='#e08707'>★★ </font>";
	           			break;
	           		case 3:
	           			result = "<font color='#e08707'>★★★</font>";
	           			break;
	           		case 4:
	           			result = "<font color='#e08707'>★★★★</font>";
	           			break;
	           		case 5:
	           			result = "<font color='#e08707'>★★★★★</font>";
	           			break;
	           		}
	           		return result;
           		
           	}  
           },
           {title: '时效评分', field: 'timeScore', visible: true, align: 'center', valign: 'middle',formatter:GoodsAppraises.color,
        	   
        	   formatter:function(value,row,index){
	        		var result;
	           		switch(value){
	           		case 1:
	           			result = "<font color='#e08707'>★</font>";
	           			break;
	           		case 2:
	           			result = "<font color='#e08707'>★★ </font>";
	           			break;
	           		case 3:
	           			result = "<font color='#e08707'>★★★</font>";
	           			break;
	           		case 4:
	           			result = "<font color='#e08707'>★★★★</font>";
	           			break;
	           		case 5:
	           			result = "<font color='#e08707'>★★★★★</font>";
	           			break;
	           		}
	           		return result;
           		
           	}},
           {title: '点评内容', field: 'content', visible: true, align: 'center', valign: 'middle'},
           {title: '是否显示', field: 'isShow', visible: true, align: 'center', valign: 'middle',
        	   formatter:function(value,row,index){
              		var result;
              		switch(value){
              		case 0:
              			result = "不显示";
              			break;
              		case 1:
              			result = "显示 ";
              			break;
       
              		}
              		return result; 
              		
        	   }},
           {title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:GoodsAppraises.open}
            
     /* {title: '商品-规格Id', field: 'goodsSpecId', visible: true, align: 'center', valign: 'middle'},
            {title: '会员ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺回复', field: 'shopReply', visible: true, align: 'center', valign: 'middle'},
                  
       {title: '有效状态', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '商家回复时间', field: 'replyTime', visible: true, align: 'center', valign: 'middle'},*/
            
            
            ];
};

function orImg(src){
	const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
    if(src.indexOf('http')==-1){
        src=imgUrl+src
    }else{
        src=src
    }
    return  src
}
/**
 * 给字段添加颜色
 */
GoodsAppraises.color = function (value) {
	return "<font color='yellow'>"+value+"</font>"
}
/**
 * 操作
 */
GoodsAppraises.open = function () {
	return "<a href='javascript:void(0)' onclick=\"GoodsAppraises.delete2()\">删除</a>"
}

/**
 * 检查是否选中
 */
GoodsAppraises.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        GoodsAppraises.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品评价
 */
GoodsAppraises.openAddGoodsAppraises = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品评价',
        area:['100%', '100%'],//宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/goodsAppraises/goodsAppraises_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品评价详情
 */
GoodsAppraises.openGoodsAppraisesDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品评价详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/goodsAppraises/goodsAppraises_update/' + GoodsAppraises.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品评价
 */
GoodsAppraises.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/goodsAppraises/delete", function (data) {
            Feng.success("删除成功!");
            GoodsAppraises.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("goodsAppraisesId",this.seItem.id);
        ajax.start();
    }
};
/**
 * 删除评价
 */
GoodsAppraises.delete2 = function () {
    if (this.check()) {

        var operation = function(){
        	 var goodsAppraisesId = GoodsAppraises.seItem.id;
        	var ajax = new $ax(Feng.ctxPath + "/goodsAppraises/delete", function (data) {
                Feng.success("删除成功!");
                GoodsAppraises.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("goodsAppraisesId",goodsAppraisesId);
            ajax.start();
        };

        Feng.confirm("是否删除 该条记录?",operation);
    }
};
/**
 * 查询商品评价列表
 */
GoodsAppraises.search = function () {
    var queryData = {};
    queryData['goodsName'] = $("#goodsName").val();
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['shopName'] = $("#shopName").val();
    GoodsAppraises.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = GoodsAppraises.initColumn();
    var table = new BSTable(GoodsAppraises.id, "/goodsAppraises/list", defaultColunms);
    table.setPaginationType("server");
    GoodsAppraises.table = table.init();
});
