/**
 * 初始化商品评价详情对话框
 */
var GoodsAppraisesInfoDlg = {
    goodsAppraisesInfoData : {}
};

/**
 * 清除数据
 */
GoodsAppraisesInfoDlg.clearData = function() {
    this.goodsAppraisesInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsAppraisesInfoDlg.set = function(key, val) {
    this.goodsAppraisesInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
GoodsAppraisesInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
GoodsAppraisesInfoDlg.close = function() {
    parent.layer.close(window.parent.GoodsAppraises.layerIndex);
}

/**
 * 收集数据
 */
GoodsAppraisesInfoDlg.collectData = function() {
    this
    .set('id')
    .set('shopId')
    .set('orderId')
    .set('goodsId')
    .set('goodsSpecId')
    .set('userId')
    .set('goodsScore')
    .set('serviceScore')
    .set('timeScore')
    .set('content')
    .set('shopReply')
    .set('images')
    .set('isShow')
    .set('dataFlag')
    .set('createTime')
    .set('replyTime');
}

/**
 * 提交添加
 */
GoodsAppraisesInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsAppraises/add", function(data){
        Feng.success("添加成功!");
        window.parent.GoodsAppraises.table.refresh();
        GoodsAppraisesInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsAppraisesInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
GoodsAppraisesInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/goodsAppraises/update", function(data){
        Feng.success("修改成功!");
        window.parent.GoodsAppraises.table.refresh();
        GoodsAppraisesInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.goodsAppraisesInfoData);
    ajax.start();
}

$(function() {

});
