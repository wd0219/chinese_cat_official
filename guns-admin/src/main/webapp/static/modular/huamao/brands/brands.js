/**
 * 品牌管理初始化
 */
var Brands = {
    id: "BrandsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Brands.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '品牌名称', field: 'brandName', visible: true, align: 'center', valign: 'middle'},
            {title: '所属分类', field: 'catName', visible: true, align: 'center', valign: 'middle'},
            {title: '品牌图标', field: 'brandImg', visible: true, align: 'center', valign: 'middle', formatter:Brands.brandImg},
            {title: '品牌介绍', field: 'brandDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '建立时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:Brands.open}
    ];
};
Brands.open = function () {
	return "<a href='javascript:void(0)' onclick=\"Brands.openBrandsDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"Brands.delete2()\">删除</a>"
}
/**
 * 检查是否选中
 */
Brands.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Brands.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加品牌
 */
Brands.openAddBrands = function () {
    var index = layer.open({
        type: 2,
        title: '添加品牌',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/brands/brands_add'
    });
    this.layerIndex = index;
};

//图片展示
Brands.brandImg = function(value,row,index){
    const imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";
    if(value.indexOf('http')==-1){
        value=imgUrl+value
    }else{
        value=value
    }
    var img = '<a class = "view"  href="javascript:void(0)"><img style="width:70px;height:30px;"  src="'+value+'" /></a>';
    return img;
};

/**
 * 打开查看品牌详情
 */
Brands.openBrandsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '品牌详情',
            area:['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/brands/brands_update/' + Brands.seItem.brandId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除品牌
 */
Brands.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/brands/delete", function (data) {
            Feng.success("删除成功!");
            Brands.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("brandsId",this.seItem.id);
        ajax.start();
    }
};
/**
 * 删除品牌
 */
Brands.delete2 = function () {
    if (this.check()) {
    	 var operation = function(){
    		 var brandId=Brands.seItem.brandId;
	        var ajax = new $ax(Feng.ctxPath + "/brands/delete", function (data) {
	            Feng.success("删除成功!");
	            Brands.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("brandsId",brandId);
	        ajax.start();
    	};
    	 Feng.confirm("是否删除 该条记录?",operation);
    }
};
/**
 * 查询品牌列表
 */
Brands.search = function () {
    var queryData = {};
    queryData['brandName'] = $("#brandName").val();
    queryData['goods1'] = $("#goods1").val();
    queryData['goods2'] = $("#goods2").val();
    queryData['goods3'] = $("#goods3").val();
    Brands.table.refresh({query: queryData});
};

$(function () {
	 var avatarUp = new $WebUpload("brandImg");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.init();
  //  BrandsInfoDlg.getBrandImg();
    var defaultColunms = Brands.initColumn();
    var table = new BSTable(Brands.id, "/brands/list", defaultColunms);
    table.setPaginationType("server");
    Brands.table = table.init();
});
