/**
 * 初始化品牌详情对话框
 */
var BrandsInfoDlg = {
    brandsInfoData : {}
};

/**
 * 清除数据
 */
BrandsInfoDlg.clearData = function() {
    this.brandsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
BrandsInfoDlg.set = function(key, val) {
    this.brandsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
BrandsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
BrandsInfoDlg.close = function() {
    parent.layer.close(window.parent.Brands.layerIndex);
}

/**
 * 收集数据
 */
BrandsInfoDlg.collectData = function() {
    this
    .set('brandId')
    .set('brandName')
    .set('brandImg')
    .set('brandDesc')
    .set('createTime')
    .set('dataFlag')
    .set("goods1")
    .set("goods2")
    .set("goods3");
}

/**
 * 提交添加
 */
BrandsInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var brandName = $("#brandName").val();
    var goods1 = $("#goods1").val();
    var goods2 = $("#goods2").val();
    var goods3 = $("#goods3").val();
    var goods;
    if(goods3!=null && goods3!=''){
        goods=goods3;
    }else{
        if(goods2!=null && goods2!=''){
            goods=goods2;
        }else{
            if(goods1!=null && goods1!=''){
                goods=goods1;
            }
        }
    }
    var brandDesc = $("#brandDesc").val();
    var brandImg = $("#brandImg").val();

    if(brandName == null || brandName ==""){
        var html='<span class="error">品牌名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#brandName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(goods1 == null || goods1 ==""){
        var html='<span class="error">所属分类不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#goods1').after(html);
        falg = 1;
    }
    
    if(brandDesc == null || brandDesc ==""){
        var html='<span class="error">品牌介绍不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#brandDesc').after(html);
        falg = 1;
    }

    if(brandImg == null || brandImg ==""){
        var html='<span class="error">上传图片不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#brandImg').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();
    this.brandsInfoData.goods=goods;
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/brands/add", function(data){
        Feng.success("添加成功!");
        window.parent.Brands.table.refresh();
        //$('#'+BrandsInfoDlg.id).bootstrapTable("load",data);
        //$('#fanhui').show();
        BrandsInfoDlg.close();
	    },function(data){
	        Feng.error("添加失败!" + data.responseJSON.message + "!");
	    });
	    ajax.set(this.brandsInfoData);
	    ajax.start();
		}else{
		    Feng.success("请核实添加字段!");
		}
	}

	function isRealNum(val){
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	if(val === "" || val ==null){
	    return false;
	}
	if(!isNaN(val)){
	    return true;
	}else{
	    return false;
	}
	}

/**
 * 提交修改
 */
BrandsInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var brandName = $("#brandName").val();
    var goods1 = $("#goods1").val();
    var brandDesc = $("#brandDesc").val();
    var brandImg = $("#brandImg").val();

    if(brandName == null || brandName ==""){
        var html='<span class="error">品牌名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#brandName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(goods1 == null || goods1 ==""){
        var html='<span class="error">所属分类不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#goods1').after(html);
        falg = 1;
    }
    
    if(brandDesc == null || brandDesc ==""){
        var html='<span class="error">品牌介绍不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#brandDesc').after(html);
        falg = 1;
    }

    if(brandImg == null || brandImg ==""){
        var html='<span class="error">上传图片不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#brandImg').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/brands/update", function(data){
        Feng.success("修改成功!");
        window.parent.Brands.table.refresh();
        BrandsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.brandsInfoData);
    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
}
/*function(){
	window.location.href='/goodsCats';
}*/

$(function() {
	 $('#goods1').val($('#catId1').val());
	//$('#fanhui').hide();
	 for(let e of document.getElementsByClassName('upload-btn')){
			let id = $(e).attr("id").replace(/BtnId/g,'').replace(/PreId/g, '');
			uploader = new $WebUpload(id);
			uploader.setUploadBarId(id);
			uploader.init();
		}
	// 初始化头像上传
    /*var avatarUp = new $WebUpload("brandImg");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.init();*/
  //  BrandsInfoDlg.getBrandImg();
   
});
