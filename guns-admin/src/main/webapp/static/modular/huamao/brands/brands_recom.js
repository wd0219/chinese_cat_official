/**
 * 品牌管理初始化
 */
var Brands = {
    id: "BrandsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Brands.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '自增ID', field: 'brandId', visible: false, align: 'center', valign: 'middle'},
            {title: '品牌名称', field: 'brandName', visible: true, align: 'center', valign: 'middle'},
            {title: '所属分类', field: 'catName', visible: true, align: 'center', valign: 'middle'},
            {title: '分类ID', field: 'catId', visible: true, align: 'center', valign: 'middle'},
            {title: '品牌图标', field: 'brandImg', visible: true, align: 'center', valign: 'middle'},
            {title: '品牌介绍', field: 'brandDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '建立时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '删除标志 -1：删除 1：有效', field: 'dataFlag', visible: false, align: 'center', valign: 'middle'},
            {title: '操作', field: 'open', visible: true, align: 'center', valign: 'middle',formatter:Brands.open}
    ];
};
Brands.open = function () {
	return "<a href='javascript:void(0)' onclick=\"Brands.openBrandsDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"Brands.delete2()\">删除</a>"
}
/**
 * 检查是否选中
 */
Brands.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Brands.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加品牌
 */
Brands.openAddBrands = function () {
    var index = layer.open({
        type: 2,
        title: '添加品牌',
        area:['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/brands/brands_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看品牌详情
 */
Brands.openBrandsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '品牌详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/brands/brands_update/' + Brands.seItem.brandId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除品牌
 */
Brands.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/brands/delete", function (data) {
            Feng.success("删除成功!");
            Brands.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("brandsId",this.seItem.id);
        ajax.start();
    }
};
/**
 * 删除品牌
 */
Brands.delete2 = function () {
    if (this.check()) {
    	 var operation = function(){
    		 var brandId=Brands.seItem.brandId;
	        var ajax = new $ax(Feng.ctxPath + "/brands/delete", function (data) {
	            Feng.success("删除成功!");
	            Brands.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("brandsId",brandId);
	        ajax.start();
    	};
    	 Feng.confirm("是否删除 该条记录?",operation);
    }
};
/**
 * 查询品牌列表
 */
Brands.search = function () {
    var queryData = {};
    queryData['brandName'] = $("#brandName").val();
    queryData['goods1'] = $("#goods1").val();
    Brands.table.refresh({query: queryData});
};

$(function () {
  /*  var defaultColunms = Brands.initColumn();
    var table = new BSTable(Brands.id, "/brands/list", defaultColunms);
    table.setPaginationType("client");
    Brands.table = table.init();*/
});



/**
 * 查询品牌列表左侧
 */
Brands.search1 = function () {
	var sb = [];
	var brandName=$("#brandName").val();
	var goods1=$("#goods1").val();
	var goods2=$("#goods2").val();
	var goods3=$("#goods3").val();
    if(brandName == '' || typeof brandName == 'undefined'){
        if(goods1 == '' || typeof goods1 == 'undefined'){
            Feng.success("请选择商品分类！");
            return ;
        }
    }

	var obj={
			brandName:brandName,
			goods1:goods1,
			goods2:goods2,
			goods3:goods3,
		
	}
	$.post("/brands/list",obj,function(result){
		list.splice(0,list.length);
		$.each(result, function(i,val){
			
			var obj = {
					name: val.brandName,
					id: val.brandId,
					isRecommend: '否',
                    dataSort:'',
			}
			list.push(obj);
		})
	});
};


$(function () {
/*    var defaultColunms = Goods.initColumn();
    var table = new BSTable(Goods.goodsId, "/goods/list", defaultColunms);
    table.setPaginationType("client");
    Goods.table = table.init();*/
});

//右侧搜索
$("#goods11,#goods21,#goods31").change(function(){
	var goods1=$("#goods11").val();
	var goods2=$("#goods21").val();
	var goods3=$("#goods31").val();
	var brandName=$("#brandName").val();
    // var goods;
    // if(goods3!=null && goods3!=''){
    //     goods=goods3;
    // }else{
    //     if(goods2!=null && goods2!=''){
    //         goods=goods2;
    //     }else{
    //         if(goods1!=null && goods1!=''){
    //             goods=goods1;
    //         }
    //     }
    // }
	var obj={
			goods1:goods1,
			goods2:goods2,
			goods3:goods3,

	}
	$.post("/brands/selectRecommend",obj,function(result){
		list2.splice(0,list2.length);
        list.splice(0,list.length);
		$.each(result, function(i,val){
			var obj = {
					name: val.brandName,
					id: val.brandId,
					isRecommend: '是',
                     dataSort:val.dataSort,
			}
			list2.push(obj);
		})
		
	});

});