/**
 * 代理公司提现管理初始化
 */
var AgentsDraws = {
    id: "AgentsDrawsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AgentsDraws.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'drawId', visible: true, align: 'center', valign: 'middle'},
        	{title: '预计到账时间', field: 'getTime', visible: true, align: 'center', valign: 'middle'},
            {title: '赎回编号', field: 'drawNo', visible: true, align: 'center', valign: 'middle'},
            {title: '代理公司名称', field: 'agentName', visible: true, align: 'center', valign: 'middle'},
            {title: '持卡人姓名', field: 'accUser', visible: true, align: 'center', valign: 'middle'},
            {title: '持卡人身份证', field: 'number', visible: true, align: 'center', valign: 'middle'},
            //{title: '持卡人身份证', field: 'IDnumber', visible: true, align: 'center', valign: 'middle'},
            {title: '银行卡号', field: 'accNo', visible: true, align: 'center', valign: 'middle'},
            //{title: '代理公司ID', field: 'agentId', visible: true, align: 'center', valign: 'middle'},
            //{title: '银行卡表ID', field: 'bankCardId', visible: true, align: 'center', valign: 'middle'},
            {title: '到账金额', field: 'money', visible: true, align: 'center', valign: 'middle'},
            {title: '手续费+综合服务费', field: 'fee', visible: true,width:50, align: 'center', valign: 'middle'},
            {title: '赎回手续费比例  %', field: 'ratio', visible: true,width:50, align: 'center', valign: 'middle'},
            {title: '赎回综合服务费率比例  %', field: 'taxratio', visible:true,width:50, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核时间', field: 'checkTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核状态 ', field: 'cashSatus', visible: true, align: 'center', valign: 'middle',formatter:AgentsDraws.cashSatus},
            {title: '是否手动处理', field: 'specialType', visible: true, align: 'center', valign: 'middle',formatter:AgentsDraws.specialType},
            //{title: '拒绝理由', field: 'checkRemark', visible: true, align: 'center', valign: 'middle'},
            //{title: '审核员工ID', field: 'checkStaffId', visible: true, align: 'center', valign: 'middle'},
            {title: '操作',field: 'cashSatus', visible: true,width:150,align: 'center', valign: 'middle',formatter:AgentsDraws.operate},
    ];
};
/**
 * 审核状态对应： -2受理失败 -1审核拒绝 0审核中 2受理中 3受理成功 4线下打款
 */
AgentsDraws.cashSatus = function(value,row,index){
	if(value == -2){
		return "受理失败";
	}else if(value == -1){
		return "审核拒绝";
	}else if(value == 0){
		return "审核中";
	}else if(value == 2){
		return "受理中";
	}else if(value == 3){
		return "受理成功";
	}else if(value == 4){
		return "线下打款";
	}
}
/**
 * 是否手动处理对应：0非手动处理>2处理成功>3处理失败
 */
AgentsDraws.specialType = function(value,row,index){
	if(value == 0){
		return "非手动处理";
	}else if(value == 2){
		return "处理成功";
	}else if(value == 3){
		return "处理失败";
	}
}
/**
 * 操作的链接
 */
AgentsDraws.operate = function (value,row,index) {
	if(value==0){
		return "<a href='javascript:void(0)'onclick=\"AgentsDraws.openAgentsDrawsPass()\">审核通过</a>&nbsp;&nbsp;<a href='javascript:void(0)'onclick=\"AgentsDraws.openAgentsDrawsRefuse()\">审核拒绝</a>&nbsp;&nbsp;<a href='javascript:void(0)'onclick=\"AgentsDraws.openAgentsDrawsMoney()\">线下打款</a>"
	}else if(value==2){
		return "<a href='javascript:void(0)'onclick=\"AgentsDraws.openAgentsDrawsInHand()\">订单处理中处理</a>"
	}else if(value==-2){
		return "<a href='javascript:void(0)'onclick=\"AgentsDraws.openAgentsDrawsDefeated()\">订单失败处理</a>"
	}
}
/**
 * 检查是否选中
 */
AgentsDraws.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AgentsDraws.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理公司提现
 */
AgentsDraws.openAddAgentsDraws = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理公司提现',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/agentsDraws/agentsDraws_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代理公司提现详情
 */
AgentsDraws.openAgentsDrawsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理公司提现详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/agentsDraws/agentsDraws_update/' + AgentsDraws.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代理公司提现
 */
AgentsDraws.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/agentsDraws/delete", function (data) {
            Feng.success("删除成功!");
            AgentsDraws.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("agentsDrawsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代理公司提现列表
 */
AgentsDraws.search = function () {
    var queryData = {};
    queryData['drawNo'] = $("#drawNo").val();
    queryData['accNo'] = $("#accNo").val();
    queryData['agentName'] = $("#agentName").val();
    queryData['cashSatus'] = $("#cashSatus").val();
    queryData['specialType'] = $("#specialType").val();
    queryData['ybeginTime'] = $("#ybeginTime").val();
    queryData['yendTime'] = $("#yendTime").val();
    queryData['cbeginTime'] = $("#cbeginTime").val();
    queryData['cendTime'] = $("#cendTime").val();
    AgentsDraws.table.refresh({query: queryData});
};
/**
 * 审核通过
 */
AgentsDraws.openAgentsDrawsPass = function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代付信息',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/agentsDraws/agentsDrawsPass?drawId=' + AgentsDraws.seItem.drawId	//将昵称传到用户账户记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 审核拒绝
 */
AgentsDraws.openAgentsDrawsRefuse = function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '请输入拒绝理由',
            area: ['400px', '250px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/agentsDraws/agentsDrawsRefuse?drawId=' + AgentsDraws.seItem.drawId	//将昵称传到用户账户记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 订单处理中处理
 */
AgentsDraws.openAgentsDrawsInHand = function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '银行处理中订单',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/agentsDraws/agentsDrawsInHand?drawId=' + AgentsDraws.seItem.drawId	//将昵称传到用户账户记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 订单失败处理
 */
AgentsDraws.openAgentsDrawsDefeated = function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '订单受理失败',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/agentsDraws/agentsDrawsDefeated?drawId=' + AgentsDraws.seItem.drawId	//将昵称传到用户账户记录表查询数据
        });
        this.layerIndex = index;
    }
};
/**
 * 线下打款
 */
AgentsDraws.openAgentsDrawsMoney = function() {
    if (this.check()) {
        var operation = function () {
            var drawId = AgentsDraws.seItem.drawId;
            var ajax = new $ax(Feng.ctxPath + "/agentsDraws/agentsDrawsMoney", function (data) {
                Feng.success("操作成功!");
                AgentsDraws.table.refresh();
            }, function (data) {
                Feng.error("操作失败!" + data.responseJSON.message + "!");
            });
            ajax.set("drawId", drawId);
            ajax.start();
        };
        Feng.confirm("是否进行线下打款操作?", operation);
    }
}
$(function () {
    var defaultColunms = AgentsDraws.initColumn();
    var table = new BSTable(AgentsDraws.id, "/agentsDraws/list", defaultColunms);
    table.setPaginationType("server");
    AgentsDraws.table = table.init();
});
//导出Excel
AgentsDraws.export= function(){
	$('#'+AgentsDraws.id).tableExport({
		fileName:'代理公司提现记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}