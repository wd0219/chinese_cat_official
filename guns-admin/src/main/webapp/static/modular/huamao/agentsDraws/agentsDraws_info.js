/**
 * 初始化代理公司提现详情对话框
 */
var AgentsDrawsInfoDlg = {
    agentsDrawsInfoData : {}
};

/**
 * 清除数据
 */
AgentsDrawsInfoDlg.clearData = function() {
    this.agentsDrawsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AgentsDrawsInfoDlg.set = function(key, val) {
    this.agentsDrawsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AgentsDrawsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AgentsDrawsInfoDlg.close = function() {
    parent.layer.close(window.parent.AgentsDraws.layerIndex);
}

/**
 * 收集数据
 */
AgentsDrawsInfoDlg.collectData = function() {
    this
    .set('drawId')
    .set('drawNo')
    .set('agentId')
    .set('bankCardId')
    .set('money')
    .set('fee')
    .set('ratio')
    .set('taxratio')
    .set('cashSatus')
    .set('getTime')
    .set('createTime')
    .set('checkRemark')
    .set('checkStaffId')
    .set('checkTime')
    .set('specialType');
}

/**
 * 提交添加
 */
AgentsDrawsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/agentsDraws/add", function(data){
        Feng.success("添加成功!");
        window.parent.AgentsDraws.table.refresh();
        AgentsDrawsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.agentsDrawsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AgentsDrawsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/agentsDraws/update", function(data){
        Feng.success("修改成功!");
        window.parent.AgentsDraws.table.refresh();
        AgentsDrawsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.agentsDrawsInfoData);
    ajax.start();
}
/**
 * 审核拒绝
 */
AgentsDrawsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/agentsDraws/updateSatus", function(data){
        Feng.success("success!");
        window.parent.AgentsDraws.table.refresh();
        AgentsDrawsInfoDlg.close();
    },function(data){
        Feng.error("拒绝失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.agentsDrawsInfoData);
    ajax.start();
}
$(function() {

});
