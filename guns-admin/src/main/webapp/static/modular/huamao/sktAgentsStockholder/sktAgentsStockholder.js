/**
 * 代理公司股东管理初始化
 */
var SktAgentsStockholder = {
    id: "SktAgentsStockholderTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktAgentsStockholder.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '代理公司ID', field: 'agentId', visible: false, align: 'center', valign: 'middle'},
            {title: '用户ID', field: 'userId', visible: false, align: 'center', valign: 'middle'},
            {title: '真实姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '股东类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:SktAgentsStockholder.type},
            {title: '代理公司名称', field: 'agentName', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		return "<a href='javascript:void(0)' onclick=\"SktAgentsStockholder.agentName()\">"+value+"</a>"
            	}
            },
            {title: '代理级别', field: 'alevel', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	if(value==10){
            		return "省代";
            	}else if(value == 11){
            		return "市代";
            	}else if(value == 12){
            		return "区县"
            	}
            }},
            {title: '持有股票数', field: 'stockNum', visible: true, align: 'center', valign: 'middle'},
            {title: '发展商家数', field: 'storeNum', visible: true, align: 'center', valign: 'middle'},
            {title: '累计分红', field: 'totalDividend', visible: true, align: 'center', valign: 'middle'},
            {title: '上次分红时间', field: 'dividendTime', visible: true, align: 'center', valign: 'middle'},
            {title: '上次分红额度', field: 'lastDividend', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:SktAgentsStockholder.op}
    ];
};


/**
 * 操作
 */
SktAgentsStockholder.op = function(value,row,index){
//	1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
	var result= '';
	switch(value){
	case 1:
	case 6:
		break;
	case 2:
	case 3:
	case 4:
	case 5:
		result = "<a href='javascript:void(0)' onclick=\"SktAgentsStockholder.down()\">降级</a>"
		break;
	case 7:
		result = "<a href='javascript:void(0)' onclick=\"SktAgentsStockholder.zhuanyi()\">转移</a>&nbsp;&nbsp;<a href='javascript:void(0)' onclick=\"SktAgentsStockholder.up()\">升级</a>"
		break;
	}
	return result;
}

SktAgentsStockholder.type = function(value){
//	1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
	var result= '';
	switch(value){
	case 1:
		result = "董事长";
		break;
	case 6:
		result = "监事";
		break;
	case 2:
		result = "总裁";
		break;
	case 3:
		result = "行政总监";
		break;
	case 4:
		result = "财务总监";
		break;
	case 5:
		result = "部门经理";
		break;
	case 7:
		result = "普通股东"
		break;
	}
	return result;
}

/**
 * 降级
 */
SktAgentsStockholder.down = function(){
	if (this.check()) {
        var type = SktAgentsStockholder.type(SktAgentsStockholder.seItem.type);
        var trueName = SktAgentsStockholder.seItem.trueName;
        var shId = SktAgentsStockholder.seItem.shId;
		var index = layer.open({
	        type: 2,
	        title: '股东降级',
	        area: ['100%', '100%'], //宽高  trueName shId type
	        fix: false, //不固定
	        maxmin: false,
	        content: Feng.ctxPath + '/sktAgentsStockholder/sktAgentsStockholder_down?shId=' +shId + "&trueName=" + trueName + "&type=" + type
	    });
	    this.layerIndex = index;
	}
}

/**
 * 升级
 */
SktAgentsStockholder.up = function(){
	if (this.check()) {
		var type = SktAgentsStockholder.type(SktAgentsStockholder.seItem.type);
		var agentId = SktAgentsStockholder.seItem.agentId;
		var trueName = SktAgentsStockholder.seItem.trueName;
		var shId = SktAgentsStockholder.seItem.shId;
		var index = layer.open({
	        type: 2,
	        title: '股东升级',
	        area: ['100%', '100%'], //宽高  trueName shId type
	        fix: false, //不固定
	        maxmin: false,
	        content: Feng.ctxPath + '/sktAgentsStockholder/sktAgentsStockholder_up?shId=' +shId + "&trueName=" + trueName + "&type=" + type +"&agentId=" + agentId
	    });
	    this.layerIndex = index;
	}
}

/**
 * 转移
 */
SktAgentsStockholder.zhuanyi = function(){
	if (this.check()) {
		var userPhone = SktAgentsStockholder.seItem.userPhone;
        var trueName = SktAgentsStockholder.seItem.trueName;
        var shId = SktAgentsStockholder.seItem.shId;
        var userId = SktAgentsStockholder.seItem.userId;
		var index = layer.open({
	        type: 2,
	        title: '股东转移',
	        area: ['100%', '100%'], //宽高  trueName shId type
	        fix: false, //不固定
	        maxmin: false,
	        content: Feng.ctxPath + '/sktAgentsStockholder/sktAgentsStockholder_zhuanyi?shId=' +shId + "&trueName=" + trueName + "&userPhone=" + userPhone + "&userId=" + userId
	    });
	    this.layerIndex = index;
	}
}
/**
 * 代理公司名称
 */
SktAgentsStockholder.agentName  = function(){
	if (this.check()) {
		window.location.href = Feng.ctxPath + '/sktAgents?agentId=' +this.seItem.agentId
	}
}


/**
 * 检查是否选中
 */
SktAgentsStockholder.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktAgentsStockholder.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理公司股东
 */
SktAgentsStockholder.openAddSktAgentsStockholder = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理公司股东',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/sktAgentsStockholder/sktAgentsStockholder_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代理公司股东详情
 */
SktAgentsStockholder.openSktAgentsStockholderDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理公司股东详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/sktAgentsStockholder/sktAgentsStockholder_update/' + SktAgentsStockholder.seItem.shId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代理公司股东
 */
SktAgentsStockholder.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktAgentsStockholder/delete", function (data) {
            Feng.success("删除成功!");
            SktAgentsStockholder.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktAgentsStockholderId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代理公司股东列表
 */
SktAgentsStockholder.search = function () {
    var queryData = {};
    queryData['type'] = $("#type").val();
    queryData['agentId'] = '';
    SktAgentsStockholder.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktAgentsStockholder.initColumn();
    var table = new BSTable(SktAgentsStockholder.id, "/sktAgentsStockholder/list", defaultColunms);
    table.setPaginationType("server");
    var obj = {};
    obj['agentId'] = $('#agentId').val();
    table.setQueryParams(obj);
    SktAgentsStockholder.table = table.init();
});
