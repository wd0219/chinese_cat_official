/**
 * 初始化代理公司股东详情对话框
 */
var SktAgentsStockholderInfoDlg = {
    sktAgentsStockholderInfoData : {}
};

/**
 * 清除数据
 */
SktAgentsStockholderInfoDlg.clearData = function() {
    this.sktAgentsStockholderInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAgentsStockholderInfoDlg.set = function(key, val) {
    this.sktAgentsStockholderInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAgentsStockholderInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktAgentsStockholderInfoDlg.close = function() {
    parent.layer.close(window.parent.SktAgentsStockholder.layerIndex);
}

/**
 * 收集数据
 */
SktAgentsStockholderInfoDlg.collectData = function() {
    this
    .set('shId')
    .set('agentId')
    .set('userId')
    .set('type')
    .set('stockNum')
    .set('storeNum')
    .set('twoFreezeDividend')
    .set('threeFreezeDividend')
    .set('storeFreezeDividend')
    .set('lastDividend')
    .set('dividendTime')
    .set('totalDividend')
    .set('createTime')
    .set('province')
    .set('citys')
    .set('county')
    .set('alevel')
    .set("userRe")
    .set('typeRe')
    .set('nameOrPhone');
}

/**
 * 提交添加
 */
SktAgentsStockholderInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var type = $("#type").val(); 
    var nameOrPhone = $("#nameOrPhone").val();
    var alevel = $("#alevel").val();
    var province = $("#province").val();
    
    if(type == null || type ==""){
        var html='<span class="error">股东类型不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#type').after(html);
        falg = 1;
    }else {
        var html = '';
    }
    
    if(nameOrPhone == null || nameOrPhone ==""){
        var html='<span class="error">用户账号不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#nameOrPhone').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(alevel == null || alevel ==""){
        var html='<span class="error">代理级别不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#alevel').after(html);
        falg = 1;
    }else { 
        var html = '';
    }
    
    if(province == null || province =="-1"){
        var html='<span class="error">用户所在省不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#province').after(html);
        falg = 1;
    }else { 
        var html = '';
    }
    
    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktAgentsStockholder/add", function(data){
    	if(data == 'TYPE'){
    		 Feng.success("普通用户不能申请，请先升级!");
    		 return;
    	}else if(data == 'ISAGENTS'){
    		 Feng.success("已经是股东!");
    		 return;
    	}else if(data == 'AUDITSTATUS'){
    		Feng.success("请先实名认证!");
    		return;
    	}else if(data == 'HASAGENT'){
    		Feng.success("已经有该职位!");
    		return;
    	}
		Feng.success("添加成功!");
        window.parent.SktAgentsStockholder.table.refresh();
        SktAgentsStockholderInfoDlg.close();
        
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAgentsStockholderInfoData);
    ajax.start();
	}else{
	    Feng.success("请核实添加字段!");
	}
	}
	
	function isRealNum(val){
	// isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
	if(val === "" || val ==null){
	    return false;
	}
	if(!isNaN(val)){
	    return true;
	}else{
	    return false;
	}
}

/**
 * 提交修改
 */
SktAgentsStockholderInfoDlg.editSubmit = function(upOrDown) {

    this.clearData();
    this.collectData();
    
    var typeRe = $('#typeRe').val();
    this.sktAgentsStockholderInfoData.typeRe = SktAgentsStockholderInfoDlg.tranType(typeRe);
    this.sktAgentsStockholderInfoData.userRe = upOrDown;
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktAgentsStockholder/update", function(data){
    	if(data == 'FALSE'){
    		 Feng.error("该单位已经有该职位!");
    	}else{
    		Feng.success("修改成功!");
	        window.parent.SktAgentsStockholder.table.refresh();
	        SktAgentsStockholderInfoDlg.close();
    	}
       
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAgentsStockholderInfoData);
    ajax.start();
}
SktAgentsStockholderInfoDlg.tranType = function(typeRe){
	var tp = "";
	//1董事长 2总裁 3行政总监 4财务总监 5部门经理
	if(typeRe == "普通股东"){
		tp = 7;
	}
	if(typeRe == "董事长"){
		tp = 1;
	}
	if(typeRe == "总裁"){
		tp = 2;
	}
	if(typeRe == "行政总监"){
		tp = 3;
	}
	if(typeRe == "财务总监"){
		tp = 4;
	}
	if(typeRe == "部门经理"){
		tp = 5;
	}
	return tp;
}

SktAgentsStockholderInfoDlg.inpNameOrPhone = function(){
	 //提交信息
	var n = $('#inpNameOrPhone').val();
    var ajax = new $ax(Feng.ctxPath + "/sktAgentsStockholder/selectNameOrPhone", function(data){
    	if(data != 'null'){
    		var obj = JSON.parse(data);
    		if(obj.userPhone == n){
    			$('#dd').html(obj.loginName);
    		}else{
    			$('#dd').html(obj.userPhone);
    		}
        	$('#userId').val(obj.userId);
    	}else{
    		$('#dd').html("该用户不存在!");
    	}
    	
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set("nameOrPhone", n);
    ajax.start();
}
/**
 * 转移
 */
SktAgentsStockholderInfoDlg.Submit = function() {
	var n = $('#inpNameOrPhone').val();
	if(n=='undefined' || n ==''){
		Feng.success("转移用户不能为空!");
		return ;
	}

    this.clearData();
    this.collectData();
    // this.sktAgentsStockholderInfoData.userId = $('#userId').val();
    // this.sktAgentsStockholderInfoData.shId = $('#shId').val();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktAgentsStockholder/updateSktA", function(data){
        if(data.code == '00'){
            Feng.success(data.msg);
            return ;
        }
		Feng.success(data.msg);
        window.parent.SktAgentsStockholder.table.refresh();
        SktAgentsStockholderInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });

    ajax.set(this.sktAgentsStockholderInfoData);
    ajax.start();
}
/**
 * 查询用户名字电话 id
 */
SktAgentsStockholderInfoDlg.selectNameOrPhone = function(){
	var n = $('#nameOrPhone').val();
	//提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktAgents/selectNameOrPhone", function(data){
    	if(data == 'null'){
    		$('#showInfo').html('该用户不存在！');
    		return ;
    	}
    	var obj = $.parseJSON(data);
    	if(obj.loginName == n){
    		$('#showInfo').html(obj.userPhone);
    	}else{
    		$('#showInfo').html(obj.loginName);
    	}
    	if($('#userRe').length>0){
            $('#userRe').val(obj.userId);
        }else{
            $('#userId').val(obj.userId);
        }

    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set("nameOrPhone", n );
    ajax.start();
}

$(function() {
	if($('#userRe').length>0){
		$('#userRe').val($('#userId').val());
	}
});
