/**
 * 商品规格值管理初始化
 */
var SpecItems = {
    id: "SpecItemsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SpecItems.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '店铺ID', field: 'shopId', visible: true, align: 'center', valign: 'middle'},
            {title: '类型ID', field: 'catId', visible: true, align: 'center', valign: 'middle'},
            {title: '商品ID', field: 'goodsId', visible: true, align: 'center', valign: 'middle'},
            {title: '项名称', field: 'itemName', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'itemDesc', visible: true, align: 'center', valign: 'middle'},
            {title: '规格图片', field: 'itemImg', visible: true, align: 'center', valign: 'middle'},
            {title: '有效状态：1有效-1无效', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
SpecItems.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SpecItems.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加商品规格值
 */
SpecItems.openAddSpecItems = function () {
    var index = layer.open({
        type: 2,
        title: '添加商品规格值',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/specItems/specItems_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看商品规格值详情
 */
SpecItems.openSpecItemsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '商品规格值详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/specItems/specItems_update/' + SpecItems.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除商品规格值
 */
SpecItems.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/specItems/delete", function (data) {
            Feng.success("删除成功!");
            SpecItems.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("specItemsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询商品规格值列表
 */
SpecItems.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SpecItems.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SpecItems.initColumn();
    var table = new BSTable(SpecItems.id, "/specItems/list", defaultColunms);
    table.setPaginationType("client");
    SpecItems.table = table.init();
});
