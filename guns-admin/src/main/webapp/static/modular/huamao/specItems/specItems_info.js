/**
 * 初始化商品规格值详情对话框
 */
var SpecItemsInfoDlg = {
    specItemsInfoData : {}
};

/**
 * 清除数据
 */
SpecItemsInfoDlg.clearData = function() {
    this.specItemsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SpecItemsInfoDlg.set = function(key, val) {
    this.specItemsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SpecItemsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SpecItemsInfoDlg.close = function() {
    parent.layer.close(window.parent.SpecItems.layerIndex);
}

/**
 * 收集数据
 */
SpecItemsInfoDlg.collectData = function() {
    this
    .set('itemId')
    .set('shopId')
    .set('catId')
    .set('goodsId')
    .set('itemName')
    .set('itemDesc')
    .set('itemImg')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
SpecItemsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/specItems/add", function(data){
        Feng.success("添加成功!");
        window.parent.SpecItems.table.refresh();
        SpecItemsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.specItemsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SpecItemsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/specItems/update", function(data){
        Feng.success("修改成功!");
        window.parent.SpecItems.table.refresh();
        SpecItemsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.specItemsInfoData);
    ajax.start();
}

$(function() {

});
