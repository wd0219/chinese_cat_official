/**
 * 代理股东申请管理初始化
 */
var AgentsApplys = {
    id: "AgentsApplysTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AgentsApplys.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '代理公司ID', field: 'agentId', visible: false, align: 'center', valign: 'middle'},
            {title: '代理公司名称', field: 'agentName', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value, row, index){
            		return "<a href='javascript:void(0)' onclick='AgentsApplys.showGongSi()'>"+value+"</a>"
            	}
            },
            {title: '代理公司类型', field: 'alevel', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	var result = "";
            	switch(value){
            	case 10:
            		result = "省级";
            		break;
            	case 11:
            		result = "市级";
            		break;
            	case 12:
            		result = "区县";
            		break;
            	}
            	return result;
            }},
            {title: '所属省市区', field: 'areas', visible: true, align: 'center', valign: 'middle'},
            {title: '用户昵称', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '真实姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号码', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '股东类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	var result = '';
            	//1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
            	switch(value){
            	case 1:
            		result = '董事长';
            		break;
            	case 2:
            		result = '总裁 ';
            		break;
            	case 3:
            		result = '行政总监';
            		break;
            	case 4:
            		result = '财务总监';
            		break;
            	case 5:
            		result = '部门经理';
            		break;
            	case 6:
            		result = '监事';
            		break;
            	case 7:
            		result = '普通股东';
            		break;
            	}
            	return result;
            	
            }},
            {title: '持有股票分数', field: 'stockNum', visible: true, align: 'center', valign: 'middle'},
            {title: '缴纳代理费用', field: 'money', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
            	//-2二审拒绝 -1一审拒绝 0审核中 1一审同意 2汇款单已上传 3二审完结
            	switch(value){
            	case -2:
            		return "二审拒绝";
            		break;
            	case -1:
            		return "一审拒绝";
            		break;
            	case 0:
            		return "未审核";
            		break;
            	case 1:
            		return "一审同意 ";
            		break;
            	case 2:
            		return "汇款单已上传 ";
            		break;
            	case 3:
            		return "二审同意";
            		break;
            		
            	}
            }},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'status', visible: true, align: 'center', valign: 'middle',formatter:AgentsApplys.op}
    ];
};

/**
 * 操作
 */
AgentsApplys.op=function(value,row,index){
	switch(value){
	case -2:
	case -1:
	case 1:
		return "";
		break;
		//查看添加信息
		//return "<a href='javascript:void(0)' onclick=\"AgentsApplys.showAddInfo()\">查看</a>";
	case 0:
		return "<a href='javascript:void(0)' onclick=\"AgentsApplys.pass(3)\">通过</a> " +
		"<a href='javascript:void(0)' onclick=\"AgentsApplys.refuse(-2)\">拒绝</a> " +
		"<a href='javascript:void(0)' onclick=\"AgentsApplys.showInfo()\">查看</a>";
//		return "<a href='javascript:void(0)' onclick=\"AgentsApplys.pass(1)\">通过</a> " +
//				"<a href='javascript:void(0)' onclick=\"AgentsApplys.refuse(-1)\">拒绝</a>";
		break;
	case 2:
		return "";
		break;
	case 3:
		return "<a href='javascript:void(0)' onclick=\"AgentsApplys.showInfo()\">查看</a>";
		break;
	}
}
/**
 * 通过
 */
AgentsApplys.pass =  function(status){
	if (this.check()) {
	  var index = layer.open({
	        type: 2,
	        title: '系统提示',
	        area: ['600', '500'], //宽高
	        fix: false, //不固定
	        maxmin: false,
	        content: Feng.ctxPath + '/agentsApplys/agentsApplys_pass/'+AgentsApplys.seItem.shaId+"/"+status
	    });
	    this.layerIndex = index;
	}
}
/**
 * 拒绝
 */
AgentsApplys.refuse = function(status){
	if (this.check()) {
		  var index = layer.open({
		        type: 2,
		        title: '请输入拒绝理由',
		        area: ['800', '600'], //宽高
		        fix: false, //不固定
		        maxmin: false,
		        content: Feng.ctxPath + '/agentsApplys/agentsApplys_refuse/'+AgentsApplys.seItem.shaId+'/'+status
		    });
		    this.layerIndex = index;
		}
}
/**
 * 显示一审添加信息
 */
AgentsApplys.showAddInfo = function(){
	if (this.check()) {
		  var index = layer.open({
		        type: 2,
		        title: '申请信息',
		        area: ['800px', '600px'], //宽高
		        fix: false, //不固定
		        maxmin: false,
		        content: Feng.ctxPath + '/agentsApplys/showAddInfo/'+AgentsApplys.seItem.shaId
		    });
		    this.layerIndex = index;
		}
}
/**
 * 显示汇款单信息
 */
AgentsApplys.showInfo = function(){
	if (this.check()) {
		  var index = layer.open({
		        type: 2,
		        title: '汇款单信息',
		        area: ['800px', '600px'], //宽高
		        fix: false, //不固定
		        maxmin: false,
		        content: Feng.ctxPath + '/agentsApplys/showInfo/'+AgentsApplys.seItem.shaId
		    });
		    this.layerIndex = index;
		}
}

/**
 * 显示公司
 */
AgentsApplys.showGongSi = function(){
	if (this.check()) {
		window.location.href = Feng.ctxPath + '/sktAgents?agentId=' +this.seItem.agentId
	}
}
/**
 * 检查是否选中
 */
AgentsApplys.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AgentsApplys.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加代理股东申请
 */
AgentsApplys.openAddAgentsApplys = function () {
    var index = layer.open({
        type: 2,
        title: '添加代理股东申请',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/agentsApplys/agentsApplys_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看代理股东申请详情
 */
AgentsApplys.openAgentsApplysDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '代理股东申请详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/agentsApplys/agentsApplys_update/' + AgentsApplys.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除代理股东申请
 */
AgentsApplys.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/agentsApplys/delete", function (data) {
            Feng.success("删除成功!");
            AgentsApplys.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("agentsApplysId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询代理股东申请列表
 */
AgentsApplys.search = function () {
    var queryData = {};
    queryData['province'] = $("#province").val();
    queryData['citys'] = $("#citys").val();
    queryData['county'] = $("#county").val();
    queryData['alevel'] = $("#alevel").val();
    queryData['status'] = $("#status").val();
    queryData['userType'] = $("#userType").val();
    queryData['phone'] = $("#phone").val();
    AgentsApplys.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AgentsApplys.initColumn();
    var table = new BSTable(AgentsApplys.id, "/agentsApplys/list", defaultColunms);
    table.setPaginationType("server");
    AgentsApplys.table = table.init();
});
