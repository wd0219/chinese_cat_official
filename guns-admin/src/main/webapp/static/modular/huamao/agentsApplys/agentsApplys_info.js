/**
 * 初始化代理股东申请详情对话框
 */
var AgentsApplysInfoDlg = {
    agentsApplysInfoData : {}
};

/**
 * 清除数据
 */
AgentsApplysInfoDlg.clearData = function() {
    this.agentsApplysInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AgentsApplysInfoDlg.set = function(key, val) {
    this.agentsApplysInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AgentsApplysInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AgentsApplysInfoDlg.close = function() {
    parent.layer.close(window.parent.AgentsApplys.layerIndex);
}

/**
 * 收集数据
 */
AgentsApplysInfoDlg.collectData = function() {
    this
    .set('shaId')
    .set('orderNo')
    .set('agentId')
    .set('userId')
    .set('phone')
    .set('type')
    .set('stockNum')
    .set('money')
    .set('remark')
    .set('status')
    .set('moneyOrderImg')
    .set('createTime')
    .set('checkRemark')
    .set('oneCheckStaffId')
    .set('oneCheckTime')
    .set('twoCheckStaffId')
    .set('twoCheckTime')
    .set('testTime')
    .set('batFlag')
    .set('batTime');
}

/**
 * 提交添加
 */
AgentsApplysInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/agentsApplys/add", function(data){
        Feng.success("添加成功!");
        window.parent.AgentsApplys.table.refresh();
        AgentsApplysInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.agentsApplysInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AgentsApplysInfoDlg.editSubmit = function(status) {

    this.clearData();
    this.collectData();
    this.agentsApplysInfoData['status']=status;
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/agentsApplys/update", function(data){
        Feng.success(data);
        window.parent.AgentsApplys.table.refresh();
        AgentsApplysInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.agentsApplysInfoData);
    ajax.start();
}

$(function() {
	var stauts = $('#status').val();
	if(stauts=='1'){
		$('#showStautsInof').html('您同意该用户成为代理公司股东,用户开始汇款?');
	}else{
		$('#showStautsInof').html('您同意该用户成为代理公司股东,用户已经汇款！');
	}
});
