/**
 * 初始化用户银行卡详情对话框
 */
var UsersBankcardsInfoDlg = {
    usersBankcardsInfoData : {}
};

/**
 * 清除数据
 */
UsersBankcardsInfoDlg.clearData = function() {
    this.usersBankcardsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersBankcardsInfoDlg.set = function(key, val) {
    this.usersBankcardsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersBankcardsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UsersBankcardsInfoDlg.close = function() {
    parent.layer.close(window.parent.UsersBankcards.layerIndex);
}

/**
 * 收集数据
 */
UsersBankcardsInfoDlg.collectData = function() {
    this
    .set('bankCardId')
    .set('userId')
    .set('type')
    .set('provinceId')
    .set('cityId')
    .set('areaId')
    .set('bankId')
    .set('accArea')
    .set('accNo')
    .set('phone')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
UsersBankcardsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersBankcards/add", function(data){
        Feng.success("添加成功!");
        window.parent.UsersBankcards.table.refresh();
        UsersBankcardsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersBankcardsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UsersBankcardsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersBankcards/update", function(data){
        Feng.success("修改成功!");
        window.parent.UsersBankcards.table.refresh();
        UsersBankcardsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersBankcardsInfoData);
    ajax.start();
}

$(function() {

});
