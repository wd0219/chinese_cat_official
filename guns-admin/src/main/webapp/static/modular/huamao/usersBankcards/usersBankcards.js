/**
 * 用户银行卡管理初始化
 */
var UsersBankcards = {
    id: "UsersBankcardsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
UsersBankcards.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
        	{title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '昵称', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
        	{title: '银行名称', field: 'bankName', visible: true, align: 'center', valign: 'middle'},
        	{title: '开户行', field: 'accArea', visible: true, align: 'center', valign: 'middle'},
        	{title: '银行卡号', field: 'accNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '持卡人', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            //{title: '是否是默认银行卡 1默认 2不是', field: 'type', visible: true, align: 'center', valign: 'middle'},
            //{title: '省id', field: 'provinceId', visible: true, align: 'center', valign: 'middle'},
            //{title: '市id', field: 'cityId', visible: true, align: 'center', valign: 'middle'},
            //{title: '区县id', field: 'areaId', visible: true, align: 'center', valign: 'middle'},
            //{title: '银行ID', field: 'bankId', visible: true, align: 'center', valign: 'middle'},
            //{title: '银行预留手机号码', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效标志', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '绑卡时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: '', visible: true, align: 'center', valign: 'middle',formatter:UsersBankcards.deleteById}
    ];
};
/*
 * 操作：删除本条数据
 * */
UsersBankcards.deleteById = function () {
	return "<a href='javascript:void(0)'onclick=\"UsersBankcards.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
UsersBankcards.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        UsersBankcards.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户银行卡
 */
UsersBankcards.openAddUsersBankcards = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户银行卡',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/usersBankcards/usersBankcards_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户银行卡详情
 */
UsersBankcards.openUsersBankcardsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户银行卡详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/usersBankcards/usersBankcards_update/' + UsersBankcards.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 通过ID删除用户银行卡
 */
UsersBankcards.delete = function () {
    if (this.check()) {
      var operation = function (){
    	var usersBankcardsId = UsersBankcards.seItem.bankCardId;
    	var ajax = new $ax(Feng.ctxPath + "/usersBankcards/deleteById", function (data) {
            Feng.success("删除成功!");
            UsersBankcards.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("usersBankcardsId",usersBankcardsId);
        ajax.start();
       };
        Feng.confirm("是否删除该条记录?",operation);
    }
};

/**
 * 查询用户银行卡列表
 */
UsersBankcards.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['loginName'] = $("#loginName").val();
    queryData['accNo'] = $("#accNo").val();
    UsersBankcards.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = UsersBankcards.initColumn();
    var table = new BSTable(UsersBankcards.id, "/usersBankcards/list", defaultColunms);
    table.setPaginationType("server");
    UsersBankcards.table = table.init();
});
//导出Excel
UsersBankcards.export= function(){
	$('#'+UsersBankcards.id).tableExport({
		fileName:'用户银行卡管理表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}