/**
 * 初始化订单管理详情对话框
 */
var SktOrdersInfoDlg = {
    sktOrdersInfoData : {}
};

/**
 * 清除数据
 */
SktOrdersInfoDlg.clearData = function() {
    this.sktOrdersInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrdersInfoDlg.set = function(key, val) {
    this.sktOrdersInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktOrdersInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktOrdersInfoDlg.close = function() {
    parent.layer.close(window.parent.SktOrders.layerIndex);
}

/**
 * 收集数据
 */
SktOrdersInfoDlg.collectData = function() {
    this
    .set('orderId')
    .set('orderNo')
    .set('shopId')
    .set('userId')
    .set('orderStatus')
    .set('isPay')
    .set('isClosed')
    .set('goodsMoney')
    .set('deliverMoney')
    .set('totalMoney')
    .set('payType')
    .set('realMoney')
    .set('cash')
    .set('kaiyuan')
    .set('kaiyuanFee')
    .set('payScore')
    .set('areaId')
    .set('areaIdPath')
    .set('userName')
    .set('userAddress')
    .set('userPhone')
    .set('isInvoice')
    .set('invoiceClient')
    .set('orderRemarks')
    .set('isRefund')
    .set('isAppraise')
    .set('cancelReason')
    .set('orderunique')
    .set('receiveTime')
    .set('deliveryTime')
    .set('paymentTime')
    .set('expressId')
    .set('expressNo')
    .set('dataFlag')
    .set('createTime')
    .set('batFlag');
}

/**
 * 提交添加
 */
SktOrdersInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrders/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktOrders.table.refresh();
        SktOrdersInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrdersInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktOrdersInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktOrders/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktOrders.table.refresh();
        SktOrdersInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktOrdersInfoData);
    ajax.start();
}

$(function() {

});
