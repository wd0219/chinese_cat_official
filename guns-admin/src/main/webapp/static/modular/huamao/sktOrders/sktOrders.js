/**
 * 订单管理管理初始化
 */
var SktOrders = {
    id: "SktOrdersTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktOrders.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '订单编号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '订单流水号', field: 'orderunique', visible: true, align: 'center', valign: 'middle'},
            {title: '收货人', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '收件人手机', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            
            {title: '店铺名称', field: 'shopName', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺手机', field: 'telephone', visible: true, align: 'center', valign: 'middle'},
            
            {title: '订单总金额 ', field: 'totalMoney', visible: true, align: 'center', valign: 'middle'},
            // {title: '实收金额', field: 'realMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '支付方式', field: 'payType', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		var result;
            		switch(value){
            		case 1:
            			result = "线下现金支付";
            			break;
            		case 2:
            			result = "现金账户支付 ";
            			break;
            		case 3:
            			result = "华宝支付";
            			break;
            		case 4:
            			result = "第三方支付";
            			break;
            		case 5:
            			result = "混合支付";
            			break;
            		case 6:
            			result = "华宝营业额";
            			break;
					case 10:
						result = "现金+优惠券支付";
						break;
            		}
            		return result;
            		
            	}},
            {title: '订单状态', field: 'orderStatus', visible: true, align: 'center', valign: 'middle',
            	formatter:function(value,row,index){
            		var result;
            		switch(value){
            		case -3:
            			result = "退款";
            			break;
            		case -2:
            			result = "未付款订单";
            			break;
            		case -1:
            			result = "用户取消";
            			break;
            		case 0:
            			result = "待发送";
            			break;
            		case 1:
            			result = "配送中";
            			break;
            		case 2:
            			result = "用户确认收货";
            			break;
            		case 3:
            			result = "系统确认收货";
            			break;
					case 4:
						result = "审核中";
						break;
					case -4:
					result = "审核拒绝";
					break;
            		}
            		return result;
            	}},
            {title: '下单时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '支付时间', field: 'paymentTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',
            	formatter:function(){
            		return "<a href='javascript:void(0)' onclick=\"SktOrders.openSktOrdersDetail()\">详情</a>";
            	}}
            
    ];
};

/**
 * 检查是否选中
 */
SktOrders.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktOrders.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加订单管理
 */
SktOrders.openAddSktOrders = function () {
    var index = layer.open({
        type: 2,
        title: '添加订单管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktOrders/sktOrders_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看订单管理详情
 */
SktOrders.openSktOrdersDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '订单管理详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktOrders/sktOrdersShow/'+ SktOrders.seItem.orderId
        });
        this.layerIndex = index;
    }
};
/**
 * 打开查看订单管理详情
 */
SktOrders.openSktOrdersDetailold = function () {
	if (this.check()) {
		var index = layer.open({
			type: 2,
			title: '订单管理详情',
			area: ['100%', '100%'], //宽高
			fix: false, //不固定
			maxmin: true,
			content: Feng.ctxPath + '/sktOrders/sktOrders_update/' + SktOrders.seItem.orderId
		});
		this.layerIndex = index;
	}
};

/**
 * 删除订单管理
 */
SktOrders.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktOrders/delete", function (data) {
            Feng.success("删除成功!");
            SktOrders.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktOrdersId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询订单管理列表
 */
SktOrders.search = function () {
    var queryData = {};
    queryData['provinceId'] = $("#province").val();
    queryData['cityId'] = $("#citys").val();
    queryData['areaId'] = $("#county").val();
    queryData['userPhone'] = $("#userPhone").val();
    queryData['orderNo'] = $("#orderNo").val();
    queryData['shopName'] = $("#shopName").val();
    queryData['telephone'] = $("#shopUserPhone").val();
    
    queryData['orderStatus'] = $("#orderStatus").val();
    queryData['payType'] = $("#payType").val();
    
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    SktOrders.table.refresh({query: queryData});
};
/**
 * 查询订单管理列表
 */
SktOrders.refresh = function () {
	$("#province").val("");
	$("#citys").val("");
	$("#county").val("");
	$("#userPhone").val("");
	$("#orderNo").val("");
	$("#shopName").val("");
	$("#shopUserPhone").val("");
	
	$("#orderStatus").val("");
	$("#payType").val("");
	
	$("#beginTime").val("");
	$("#endTime").val("");
	
};

SktOrders.selectCon = function(pid){
	/**
	 * 当pid的值为45时，表示订单状态，操作orderStatus的option
	 * 当pid的值为53时，表示支付当时，操作payType的option
	 */
	 $.ajax({
         type:"get",
         url:"/dict/namelist", //必选。规定吧请求发送到那个URL。
         data: {pid:pid},//传递参数
       //  data:null,//可选。映射或字符串值。规定连同请求发送到服务器的数据。
         async:false,
         success:function(result){
        	 $.each(result, function(i,val){
        		 if(pid == 45){
        			 $("#orderStatus").append("<option value='" + val.num + "'>" + val.name + "</option>")
        		 }
        		 if(pid == 53){
        			 $("#payType").append("<option value='" + val.num + "'>" + val.name + "</option>")
        		 }
        	 });
        	 
         }, //可选，请求成功时执行的回调函数。

         datatype:"json"//可选。规定预期的服务器响应的数据类型。默认执行职能判断(xml,json,script 或html)

       });
}

$(function () {
    var defaultColunms = SktOrders.initColumn();
    var table = new BSTable(SktOrders.id, "/sktOrders/alllist", defaultColunms);
    table.setPaginationType("server");
    SktOrders.table = table.init();
    SktOrders.selectCon("45");
    SktOrders.selectCon("53");
});
