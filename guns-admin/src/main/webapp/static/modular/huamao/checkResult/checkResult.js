/**
 * 平台对账初始化
 */
var CheckResult = {
    id: "CheckResultTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
CheckResult.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '日期', field: 'checkDate', visible: true, align: 'center', valign: 'middle'},
            {title: '资金对账差异', field: 'zjCheckdiff', visible: true, align: 'center', valign: 'middle'},
            {title: '积分对账差异', field: 'jfCheckdiff', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝对账差异', field: 'kybCheckdiff', visible: true, align: 'center', valign: 'middle'},
            {title: '昨日资金', field: 'zjYesterday', visible: true, align: 'center', valign: 'middle'},
            {title: '今日资金', field: 'zjToday', visible: true, align: 'center', valign: 'middle'},
            {title: '今日业务增量', field: 'zjChange', visible: true, align: 'center', valign: 'middle'},
            {title: '门店订单支付金额', field: 'zjStorepay', visible: true, align: 'center', valign: 'middle'},
            {title: '商城订单支付金额', field: 'zjMallpay', visible: true, align: 'center', valign: 'middle'},
            /*{title: '提现申请', field: 'zjTixianapply', visible: true, align: 'center', valign: 'middle'},
            {title: '提现手续费', field: 'zjTixianfee', visible: true, align: 'center', valign: 'middle'},
            {title: '提现不通过', field: 'zjTixiannopass', visible: true, align: 'center', valign: 'middle'},
            {title: '退款', field: 'zjTuikuan', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'zjChongzhi', visible: true, align: 'center', valign: 'middle'},
            {title: '昨日积分', field: 'jfYesterday', visible: true, align: 'center', valign: 'middle'},
            {title: '今日积分', field: 'jfToday', visible: true, align: 'center', valign: 'middle'},
            {title: '今日业务增量', field: 'jfChange', visible: true, align: 'center', valign: 'middle'},
            {title: '积分转换', field: 'jfZhuanhuan', visible: true, align: 'center', valign: 'middle'},
            {title: '门店消费获得', field: 'jfStorepay', visible: true, align: 'center', valign: 'middle'},
            {title: '商城消费获得', field: 'jfMallpay', visible: true, align: 'center', valign: 'middle'},
            {title: '邀请奖励', field: 'jfInvitereward', visible: true, align: 'center', valign: 'middle'},
            {title: '其它', field: 'jfOther', visible: true, align: 'center', valign: 'middle'},
            {title: '昨日华宝', field: 'kybYesterday', visible: true, align: 'center', valign: 'middle'},
            {title: '今日华宝', field: 'kybToday', visible: true, align: 'center', valign: 'middle'},
            {title: '今日业务增量', field: 'kybChange', visible: true, align: 'center', valign: 'middle'},
            {title: '积分转换', field: 'kybZhuanhuan', visible: true, align: 'center', valign: 'middle'},
            {title: '门店支付', field: 'kybStorepay', visible: true, align: 'center', valign: 'middle'},
            {title: '门店支付手续费', field: 'kybStorepayfee', visible: true, align: 'center', valign: 'middle'},
            {title: '商城支付', field: 'kybMallpay', visible: true, align: 'center', valign: 'middle'},
            {title: '商城支付手续费', field: 'kybMallpayfee', visible: true, align: 'center', valign: 'middle'},
            {title: '提现', field: 'kybTixianapply', visible: true, align: 'center', valign: 'middle'},
            {title: '提现手续费', field: 'kybTixianfee', visible: true, align: 'center', valign: 'middle'},
            {title: '提现不通过', field: 'kybTixiannopass', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝退款', field: 'kybTuikuan', visible: true, align: 'center', valign: 'middle'},
            {title: '其它', field: 'kybOther', visible: true, align: 'center', valign: 'middle'}*/
            {title: '其它', field: '', visible: true, align: 'center', valign: 'middle',formatter:CheckResult.op}
    ];
};
/*
 * 操作
 * */
CheckResult.op = function () {
	return "<a href='javascript:void(0)'onclick=\"CheckResult.openCheckResultDetail()\">详情</a>"
}
/**
 * 检查是否选中
 */
CheckResult.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CheckResult.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加平台对账
 */
CheckResult.openAddCheckResult = function () {
    var index = layer.open({
        type: 2,
        title: '添加平台对账',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/checkResult/checkResult_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看平台对账详情
 */
CheckResult.openCheckResultDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '平台对账详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/checkResult/detail/' + CheckResult.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除平台对账
 */
CheckResult.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/checkResult/delete", function (data) {
            Feng.success("删除成功!");
            CheckResult.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("checkResultId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询平台对账列表
 */
CheckResult.search = function () {
    var queryData = {};
    queryData['beginTime1'] = $("#beginTime").val();
    queryData['endTime1'] = $("#endTime").val();
    CheckResult.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = CheckResult.initColumn();
    var table = new BSTable(CheckResult.id, "/checkResult/list", defaultColunms);
    table.setPaginationType("server");
    CheckResult.table = table.init();
});
//导出Excel
CheckResult.export= function(){
	$('#'+CheckResult.id).tableExport({
		fileName:'平台对账记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}