/**
 * 初始化pingtaiduizhang详情对话框
 */
var CheckResultInfoDlg = {
    checkResultInfoData : {}
};

/**
 * 清除数据
 */
CheckResultInfoDlg.clearData = function() {
    this.checkResultInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CheckResultInfoDlg.set = function(key, val) {
    this.checkResultInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CheckResultInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CheckResultInfoDlg.close = function() {
    parent.layer.close(window.parent.CheckResult.layerIndex);
}

/**
 * 收集数据
 */
CheckResultInfoDlg.collectData = function() {
    this
    .set('id')
    .set('checkDate')
    .set('zjCheckdiff')
    .set('jfCheckdiff')
    .set('kybCheckdiff')
    .set('zjYesterday')
    .set('zjToday')
    .set('zjChange')
    .set('zjStorepay')
    .set('zjMallpay')
    .set('zjTixianapply')
    .set('zjTixianfee')
    .set('zjTixiannopass')
    .set('zjTuikuan')
    .set('zjChongzhi')
    .set('jfYesterday')
    .set('jfToday')
    .set('jfChange')
    .set('jfZhuanhuan')
    .set('jfStorepay')
    .set('jfMallpay')
    .set('jfInvitereward')
    .set('jfOther')
    .set('kybYesterday')
    .set('kybToday')
    .set('kybChange')
    .set('kybZhuanhuan')
    .set('kybStorepay')
    .set('kybStorepayfee')
    .set('kybMallpay')
    .set('kybMallpayfee')
    .set('kybTixianapply')
    .set('kybTixianfee')
    .set('kybTixiannopass')
    .set('kybTuikuan')
    .set('kybOther');
}

/**
 * 提交添加
 */
CheckResultInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/checkResult/add", function(data){
        Feng.success("添加成功!");
        window.parent.CheckResult.table.refresh();
        CheckResultInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.checkResultInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
CheckResultInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/checkResult/update", function(data){
        Feng.success("修改成功!");
        window.parent.CheckResult.table.refresh();
        CheckResultInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.checkResultInfoData);
    ajax.start();
}

$(function() {

});
