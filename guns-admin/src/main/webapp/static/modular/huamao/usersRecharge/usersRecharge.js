/**
 * 用户充值管理初始化
 */
var UsersRecharge = {
    id: "UsersRechargeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
UsersRecharge.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
         {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '联系电话', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'trueName', visible: true, align: 'center', valign: 'middle'},
            {title: '订单编号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '充值总金额 ', field: 'cash', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            {title: '状态 ', field: 'status', visible: true, align: 'center', valign: 'middle',formatter:function(value,row,index){
//            	状态 -1线下支付审核拒绝 0已下单 1已支付或审核通过 2已上传图片
            	if(value == 0){
            		return "未支付";
            	}else if(value == 1){
            		return "已支付";
            	}else if(value == 2){
            		return "已上传图片";
            	}else if(value == -1){
            		return "线下支付审核拒绝";
            	}
            }},
            {title: '充值时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '审核时间', field: 'checkTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'payType', visible: true, align: 'center', valign: 'middle',formatter:UsersRecharge.checkInfo}
    ];
};

/**
 * 检查是否选中
 */
UsersRecharge.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        UsersRecharge.seItem = selected[0];
        return true;
    }
};

/**
 * 查看
 */
UsersRecharge.checkInfo = function(value,row,index){
	var status = row.status;
	if(status==1 || status == -1){
		return "<a href='javascript:void(0)' onclick=\"UsersRecharge.openUsersRechargeDetail()\">查看</a>";
	}
	
	if(value==1){
		return "<a href='javascript:void(0)' onclick=\"UsersRecharge.pass()\">通过</a>&nbsp;&nbsp;" +
				"<a href='javascript:void(0)' onclick=\"UsersRecharge.refuse()\">拒绝</a>&nbsp;&nbsp;" +
				"<a href='javascript:void(0)' onclick=\"UsersRecharge.openUsersRechargeDetail()\">查看</a>"
	}else {
		return "";
	}
}

/**
 * 通过
 */
UsersRecharge.pass =  function(){
	if (this.check()) {
	  var index = layer.open({
	        type: 2,
	        title: '系统提示',
	        area: ['440px', '230px'], //宽高
	        fix: false, //不固定
	        maxmin: false,
	        content: Feng.ctxPath + '/usersRecharge/usersRecharge_pass/'+UsersRecharge.seItem.id
	    });
	    this.layerIndex = index;
	}
}
/**
 * 拒绝
 */
UsersRecharge.refuse = function(){
	if (this.check()) {
		  var index = layer.open({
		        type: 2,
		        title: '请输入拒绝理由',
		        area: ['440px', '230px'], //宽高
		        fix: false, //不固定
		        maxmin: true,
		        content: Feng.ctxPath + '/usersRecharge/usersRecharge_refuse/'+UsersRecharge.seItem.id
		    });
		    this.layerIndex = index;
		}
}
/**
 * 点击添加用户充值
 */
UsersRecharge.openAddUsersRecharge = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户充值',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/usersRecharge/usersRecharge_add'
    });
    this.layerIndex = index;
};

/**
 * 表格导出
 */
UsersRecharge.export= function(){
	$('#'+UsersRecharge.id).tableExport({
		fileName:'充值记录',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}
/**
 * 打开查看用户充值详情
 */
UsersRecharge.openUsersRechargeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户充值详情',
            area: ['800px', '600px'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/usersRecharge/usersRecharge_update/' + UsersRecharge.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户充值
 */
UsersRecharge.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/usersRecharge/delete", function (data) {
            Feng.success("删除成功!");
            UsersRecharge.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("usersRechargeId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户充值列表
 */
UsersRecharge.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['status'] = $("#status").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    UsersRecharge.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = UsersRecharge.initColumn();
    var table = new BSTable(UsersRecharge.id, "/usersRecharge/list", defaultColunms);
    table.setPaginationType("server");
    UsersRecharge.table = table.init();
});
