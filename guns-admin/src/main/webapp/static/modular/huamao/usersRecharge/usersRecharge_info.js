/**
 * 初始化用户充值详情对话框
 */
var UsersRechargeInfoDlg = {
    usersRechargeInfoData : {}
};

/**
 * 清除数据
 */
UsersRechargeInfoDlg.clearData = function() {
    this.usersRechargeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersRechargeInfoDlg.set = function(key, val) {
    this.usersRechargeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UsersRechargeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UsersRechargeInfoDlg.close = function() {
    parent.layer.close(window.parent.UsersRecharge.layerIndex);
}

/**
 * 收集数据
 */
UsersRechargeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('orderNo')
    .set('userId')
    .set('payType')
    .set('cash')
    .set('remark')
    .set('status')
    .set('dataFlag')
    .set('createTime')
    .set('checkRemark');
}

/**
 * 提交添加
 */
UsersRechargeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersRecharge/add", function(data){
        Feng.success("添加成功!");
        window.parent.UsersRecharge.table.refresh();
        UsersRechargeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersRechargeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UsersRechargeInfoDlg.editSubmit = function(status) {
	var kong = $('#checkRemark').val();
	if( kong == ''){
		Feng.success("请输入拒绝理由!");
		return ;
	}
    this.clearData();
    this.collectData();
    this.usersRechargeInfoData['status']=status;
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/usersRecharge/update", function(data){
        Feng.success("修改成功!");
        window.parent.UsersRecharge.table.refresh();
        UsersRechargeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.usersRechargeInfoData);
    ajax.start();
}


$(function() {
	UsersRechargeInfoDlg.tranf();
	/*$('#showBig').click(function(){
		var src = this.src
		layer.open({
		    type: 1,
		    title: '汇款单图片',
		    closeBtn: 1,
		    //area: '1000px',
		    skin: 'layui-layer-nobg', //没有背景色
		    shadeClose: true,
		    content: "<img src='"+src+"'>"
		});
	});*/
});

/**
 * 转换方式
 */
UsersRechargeInfoDlg.tranf = function(){
	let payType = $('#payType').val();
	//支付方式1:线下现金支付 4第三方支付
	switch(payType){
	case '1':
		$('#payType').val('线下现金支付');
		break;
	case '4':
		$('#payType').val('第三方支付');
		break;
	default:
		$('#payType').val('');
		break;
	}
	
	let status = $('#status').val();
	//状态0未支付 1已支付
	switch(status){
	case '0':
		$('#status').val('未支付');
		break;
	case '1':
		$('#status').val('已支付');
		break;
	default:
		$('#status').val('');
		break;
	}
	
	let dataFlag = $('#dataFlag').val();
	//有效状态 1有效 0删除
	switch(status){
	case '0':
		$('#dataFlag').val('删除');
		break;
	case '1':
		$('#dataFlag').val('有效');
		break;
	default:
		$('#dataFlag').val('');
		break;
	}
}
