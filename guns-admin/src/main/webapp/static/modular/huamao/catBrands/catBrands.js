/**
 * 品牌分类管理初始化
 */
var CatBrands = {
    id: "CatBrandsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
CatBrands.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '分类ID', field: 'catId', visible: true, align: 'center', valign: 'middle'},
            {title: '品牌ID', field: 'brandId', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
CatBrands.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CatBrands.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加品牌分类
 */
CatBrands.openAddCatBrands = function () {
    var index = layer.open({
        type: 2,
        title: '添加品牌分类',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/catBrands/catBrands_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看品牌分类详情
 */
CatBrands.openCatBrandsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '品牌分类详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/catBrands/catBrands_update/' + CatBrands.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除品牌分类
 */
CatBrands.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/catBrands/delete", function (data) {
            Feng.success("删除成功!");
            CatBrands.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("catBrandsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询品牌分类列表
 */
CatBrands.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    CatBrands.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = CatBrands.initColumn();
    var table = new BSTable(CatBrands.id, "/catBrands/list", defaultColunms);
    table.setPaginationType("client");
    CatBrands.table = table.init();
});
