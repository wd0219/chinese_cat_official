/**
 * 初始化品牌分类详情对话框
 */
var CatBrandsInfoDlg = {
    catBrandsInfoData : {}
};

/**
 * 清除数据
 */
CatBrandsInfoDlg.clearData = function() {
    this.catBrandsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CatBrandsInfoDlg.set = function(key, val) {
    this.catBrandsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CatBrandsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CatBrandsInfoDlg.close = function() {
    parent.layer.close(window.parent.CatBrands.layerIndex);
}

/**
 * 收集数据
 */
CatBrandsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('catId')
    .set('brandId');
}

/**
 * 提交添加
 */
CatBrandsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/catBrands/add", function(data){
        Feng.success("添加成功!");
        window.parent.CatBrands.table.refresh();
        CatBrandsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.catBrandsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
CatBrandsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/catBrands/update", function(data){
        Feng.success("修改成功!");
        window.parent.CatBrands.table.refresh();
        CatBrandsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.catBrandsInfoData);
    ajax.start();
}

$(function() {

});
