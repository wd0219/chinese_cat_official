/**
 * 华宝货款记录管理初始化
 */
var LogKaiyuanTurnover = {
    id: "LogKaiyuanTurnoverTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
LogKaiyuanTurnover.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            //{title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
        	{title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
        	{title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动前华宝余额', field: 'preKaiyuan', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动金额', field: 'kaiyuanp', visible: true, align: 'center', valign: 'middle'},
        	{title: '变动后华宝余额', field: 'akaiyuan', visible: true, align: 'center', valign: 'middle'},
        	{title: '资金变动类型', field: 'type', visible: true, align: 'center', valign: 'middle',formatter:LogKaiyuanTurnover.type},
            //{title: '发起用户ID 0为平台', field: 'fromId', visible: true, align: 'center', valign: 'middle'},
        	//{title: '目标用户ID 0为平台', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            //{title: '流水标志 -1减少 1增加', field: 'kaiyuanType', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            //{title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};
/**
 * 资金变动类型对应：1商城货款>2提现驳回>3提现驳回退还手续费>4待发货款转入>5商家货款>6消费退款>
7支付工单服务费>8赎回工单服务费>9支付工单手续费>10赎回工单手续费>11后台手动添加>31商城消费>
32消费服务费>33提现>34提现手续费>35购买库存支出>36购买积分手续费>37商家消费>38购买牌匾>
39购买牌匾服务费>40后台手动减少
 */
LogKaiyuanTurnover.type = function(value,row,index){
	if(value == 1){
		return "商城货款";
	}else if(value == 2){
		return "提现驳回";
	}else if(value == 3){
		return "提现驳回退还手续费";
	}else if(value == 4){
		return "待发货款转入";
	}else if(value == 5){
		return "商家货款";
	}else if(value == 6){
		return "消费退款";
	}else if(value == 7){
		return "手动补发";
	}else if(value == 8){
		return "赎回工单服务费";
	}else if(value == 9){
		return "支付工单手续费";
	}else if(value == 10){
		return "赎回工单手续费";
	}else if(value == 11){
		return "后台手动添加";
	}else if(value == 31){
		return "商城消费";
	}else if(value == 32){
		return "消费服务费";
	}else if(value == 33){
		return "提现";
	}else if(value == 34){
		return "提现手续费";
	}else if(value == 35){
		return "购买库存支出";
	}else if(value == 36){
		return "购买积分手续费";
	}else if(value == 37){
		return "商家消费";
	}else if(value == 38){
		return "购买牌匾";
	}else if(value == 39){
		return "购买牌匾服务费";
	}else if(value == 40){
		return "后台手动减少";
	}else if(value == 41){
		return "转化猫币";
	}
}
/**
 * 检查是否选中
 */
LogKaiyuanTurnover.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        LogKaiyuanTurnover.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加华宝货款记录
 */
LogKaiyuanTurnover.openAddLogKaiyuanTurnover = function () {
    var index = layer.open({
        type: 2,
        title: '添加华宝货款记录',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/logKaiyuanTurnover/logKaiyuanTurnover_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看华宝货款记录详情
 */
LogKaiyuanTurnover.openLogKaiyuanTurnoverDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '华宝货款记录详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/logKaiyuanTurnover/logKaiyuanTurnover_update/' + LogKaiyuanTurnover.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除华宝货款记录
 */
LogKaiyuanTurnover.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/logKaiyuanTurnover/delete", function (data) {
            Feng.success("删除成功!");
            LogKaiyuanTurnover.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("logKaiyuanTurnoverId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询华宝货款记录列表
 */
LogKaiyuanTurnover.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['type'] = $("#type").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    LogKaiyuanTurnover.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = LogKaiyuanTurnover.initColumn();
    var table = new BSTable(LogKaiyuanTurnover.id, "/logKaiyuanTurnover/list/"+$('#userPhone2').val(), defaultColunms);
    table.setPaginationType("server");
    LogKaiyuanTurnover.table = table.init();
});
//导出Excel
LogKaiyuanTurnover.export= function(){
	$('#'+LogKaiyuanTurnover.id).tableExport({
		fileName:'华宝货款记录表',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}