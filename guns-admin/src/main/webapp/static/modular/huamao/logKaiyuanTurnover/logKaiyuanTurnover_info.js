/**
 * 初始化华宝货款记录详情对话框
 */
var LogKaiyuanTurnoverInfoDlg = {
    logKaiyuanTurnoverInfoData : {}
};

/**
 * 清除数据
 */
LogKaiyuanTurnoverInfoDlg.clearData = function() {
    this.logKaiyuanTurnoverInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogKaiyuanTurnoverInfoDlg.set = function(key, val) {
    this.logKaiyuanTurnoverInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LogKaiyuanTurnoverInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LogKaiyuanTurnoverInfoDlg.close = function() {
    parent.layer.close(window.parent.LogKaiyuanTurnover.layerIndex);
}

/**
 * 收集数据
 */
LogKaiyuanTurnoverInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('fromId')
    .set('userId')
    .set('orderNo')
    .set('preKaiyuan')
    .set('kaiyuanType')
    .set('kaiyuan')
    .set('remark')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
LogKaiyuanTurnoverInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logKaiyuanTurnover/add", function(data){
        Feng.success("添加成功!");
        window.parent.LogKaiyuanTurnover.table.refresh();
        LogKaiyuanTurnoverInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logKaiyuanTurnoverInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LogKaiyuanTurnoverInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/logKaiyuanTurnover/update", function(data){
        Feng.success("修改成功!");
        window.parent.LogKaiyuanTurnover.table.refresh();
        LogKaiyuanTurnoverInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.logKaiyuanTurnoverInfoData);
    ajax.start();
}

$(function() {

});
