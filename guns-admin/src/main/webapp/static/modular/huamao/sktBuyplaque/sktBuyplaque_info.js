/**
 * 初始化线下商家购买牌匾详情对话框
 */
var SktBuyplaqueInfoDlg = {
    sktBuyplaqueInfoData : {}
};

/**
 * 清除数据
 */
SktBuyplaqueInfoDlg.clearData = function() {
    this.sktBuyplaqueInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktBuyplaqueInfoDlg.set = function(key, val) {
    this.sktBuyplaqueInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktBuyplaqueInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktBuyplaqueInfoDlg.close = function() {
    parent.layer.close(window.parent.SktBuyplaque.layerIndex);
}

/**
 * 收集数据
 */
SktBuyplaqueInfoDlg.collectData = function() {
    this
    .set('orderId')
    .set('orderNo')
    .set('userId')
    .set('payType')
    .set('totalMoney')
    .set('realMoney')
    .set('cash')
    .set('kaiyuan')
    .set('kaiyuanFee')
    .set('orderRemarks')
    .set('createTime')
    .set('dataFlag')
    .set('status');
}

/**
 * 提交添加
 */
SktBuyplaqueInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktBuyplaque/add", function(data){
        Feng.success("添加成功!");
        window.parent.SktBuyplaque.table.refresh();
        SktBuyplaqueInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktBuyplaqueInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SktBuyplaqueInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktBuyplaque/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktBuyplaque.table.refresh();
        SktBuyplaqueInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktBuyplaqueInfoData);
    ajax.start();
}

SktBuyplaqueInfoDlg.editSubmit2 = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/sktBuyplaque/update2", function(data){
        Feng.success("修改成功!");
        window.parent.SktBuyplaque.table.refresh();
        SktBuyplaqueInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktBuyplaqueInfoData);
    ajax.start();
}

$(function() {

});
