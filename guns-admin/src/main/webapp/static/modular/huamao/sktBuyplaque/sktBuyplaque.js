/**
 * 线下商家购买牌匾管理初始化
 */
var SktBuyplaque = {
    id: "SktBuyplaqueTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktBuyplaque.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '用户手机号', field: 'userPhone', visible: true, align: 'center', valign: 'middle'},
            {title: '用户类型', field: 'userType', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 0:
        			result = "普通";
        			break;
        		case 1:
        			result = "主管";
        			break;
        		case 2:
        			result = "经理";
        			break;
        		}
        		return result;
        	}},
            {title: '订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '支付方式 ', field: 'payType', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "线下现金支付";
        			break;
        		case 2:
        			result = "现金账户支付";
        			break;
        		case 3:
        			result = "华宝支付";
        			break;
        		case 4:
        			result = "第三方支付";
        			break;
        		case 5:
        			result = "混合支付";
        			break;
        		case 6:
        			result = "华宝营业额";
        			break;
        		}
        		return result;
        	}},
            {title: '订单总金额', field: 'totalMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '通过支付公司现金支付的金额', field: 'realMoney', visible: true, align: 'center', valign: 'middle'},
            {title: '平台现金账户金额', field: 'cash', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝余额', field: 'kaiyuan', visible: true, align: 'center', valign: 'middle'},
            {title: '华宝费率', field: 'kaiyuanFee', visible: true, align: 'center', valign: 'middle'},
            {title: '订单备注', field: 'orderRemarks', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 0:
        			result = "未支付";
        			break;
        		case 1:
        			result = "已付款未处理";
        			break;
        		case 2:
        			result = "已取消";
        			break;
        		case 3:
        			result = "已处理";
        			break;
        		}
        		return result;
        	}},
            {title: '下单时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'status', visible: true, align: 'center', valign: 'middle',formatter:SktBuyplaque.op}
    ];
};
/*
 * 操作
 * */
SktBuyplaque.op = function (value,row,index) { 
	if(value==1){
		return "<a href='javascript:void(0)' onclick=\"SktBuyplaque.editSubmit()\">处理</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktBuyplaque.editSubmit2()\">取消</a>"
	}else{
		return "";
	}
	
}
/**
 * 检查是否选中
 */
SktBuyplaque.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktBuyplaque.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加线下商家购买牌匾
 */
SktBuyplaque.openAddSktBuyplaque = function () {
    var index = layer.open({
        type: 2,
        title: '添加线下商家购买牌匾',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktBuyplaque/sktBuyplaque_add'
    });
    this.layerIndex = index;
};

//表格导出
SktBuyplaque.export= function(){
	$('#'+SktBuyplaque.id).tableExport({
		fileName:'线下商家购买牌匾',
		type: 'excel', 
		escape: 'false',
		mso: {
			fileFormat:'xmlss',
			worksheetName: ['Table 1','Table 2', 'Table 3']
		}
	});
}
/**
 * 打开查看线下商家购买牌匾详情
 */
SktBuyplaque.openSktBuyplaqueDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '线下商家购买牌匾详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktBuyplaque/sktBuyplaque_update/' + SktBuyplaque.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 提交修改
 */
SktBuyplaque.editSubmit = function() {
	
	
	if (this.check()) {
    //提交信息
		var orderId = this.seItem.orderId;
		var operation = function(){
    var ajax = new $ax(Feng.ctxPath + "/sktBuyplaque/update", function(data){
        Feng.success("修改成功!");
        SktBuyplaque.table.refresh();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set("orderId",orderId);
   // ajax.set(this.SktBuyplaque);
    ajax.start();
	};
	Feng.confirm("您确定处理该商家购买牌匾吗 " + "?",operation);
	}
};

SktBuyplaque.editSubmit2 = function() {
	if (this.check()) {
    //提交信息
	var orderId = this.seItem.orderId;
	var operation = function(){
    var ajax = new $ax(Feng.ctxPath + "/sktBuyplaque/updatea", function(data){
        Feng.success("修改成功!");
        SktBuyplaque.table.refresh();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set("orderId",orderId);
   // ajax.set(this.SktBuyplaque);
    ajax.start();
	};
	Feng.confirm("您确定取消该商家购买牌匾吗 " +  "?",operation);
	}
};

/**
 * 删除线下商家购买牌匾
 */
SktBuyplaque.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/sktBuyplaque/delete", function (data) {
            Feng.success("删除成功!");
            SktBuyplaque.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktBuyplaqueId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询线下商家购买牌匾列表
 */
SktBuyplaque.search = function () {
    var queryData = {};
    queryData['userPhone'] = $("#userPhone").val();
    queryData['userType'] = $("#userType").val();
    queryData['payType'] = $("#payType").val();
    queryData['status'] = $("#status").val();
    queryData['createTime'] = $("#createTime").val();
    queryData['checkTime'] = $("#checkTime").val();
    SktBuyplaque.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktBuyplaque.initColumn();
    var table = new BSTable(SktBuyplaque.id, "/sktBuyplaque/list", defaultColunms);
    table.setPaginationType("server");
    SktBuyplaque.table = table.init();
});
