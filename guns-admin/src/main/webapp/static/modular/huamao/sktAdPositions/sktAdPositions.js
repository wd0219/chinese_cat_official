/**
 * 广告位置管理初始化
 */
var SktAdPositions = {
    id: "SktAdPositionsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
SktAdPositions.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '广告类型', field: 'positionType', visible: true, align: 'center', valign: 'middle',
            formatter:function(value,row,index){
        		var result;
        		switch(value){
        		case 1:
        			result = "PC版";
        			break;
        		case 2:
        			result = "微信版 ";
        			break;
        		}
        		return result;
        		
        	}},
            {title: '建议宽度', field: 'positionWidth', visible: true, align: 'center', valign: 'middle'},
            {title: '建议高度', field: 'positionHeight', visible: true, align: 'center', valign: 'middle'},
            {title: '广告位置', field: 'positionName', visible: true, align: 'center', valign: 'middle'},
            {title: '广告位置代码', field: 'positionCode', visible: true, align: 'center', valign: 'middle'},
            {title: '排序号', field: 'apSort', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',formatter:SktAdPositions.op}
    ];
};
/*
 * 操作
 * */
SktAdPositions.op = function () {
	return "<a href='javascript:void(0)' onclick=\"SktAdPositions.openSktAdPositionsDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"SktAdPositions.delete()\">删除</a>"
}
/**
 * 检查是否选中
 */
SktAdPositions.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        SktAdPositions.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加广告位置
 */
SktAdPositions.openAddSktAdPositions = function () {
    var index = layer.open({
        type: 2,
        title: '添加广告位置',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sktAdPositions/sktAdPositions_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看广告位置详情
 */
SktAdPositions.openSktAdPositionsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '广告位置详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sktAdPositions/sktAdPositions_update/' + SktAdPositions.seItem.positionId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除广告位置
 */
SktAdPositions.delete = function () {
    if (this.check()) {
    	var id = this.seItem.positionId;
    	var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/sktAdPositions/delete", function (data) {
            Feng.success("删除成功!");
            SktAdPositions.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("sktAdPositionsId",id);
        ajax.start();
    };
    Feng.confirm("是否删除该角色 ?",operation);
    };
};

/**
 * 查询广告位置列表
 */
SktAdPositions.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    SktAdPositions.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = SktAdPositions.initColumn();
    var table = new BSTable(SktAdPositions.id, "/sktAdPositions/list", defaultColunms);
    table.setPaginationType("server");
    SktAdPositions.table = table.init();
});
