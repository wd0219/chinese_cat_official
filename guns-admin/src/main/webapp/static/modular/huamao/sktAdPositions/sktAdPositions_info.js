/**
 * 初始化广告位置详情对话框
 */
var SktAdPositionsInfoDlg = {
    sktAdPositionsInfoData : {}
};

/**
 * 清除数据
 */
SktAdPositionsInfoDlg.clearData = function() {
    this.sktAdPositionsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAdPositionsInfoDlg.set = function(key, val) {
    this.sktAdPositionsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SktAdPositionsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SktAdPositionsInfoDlg.close = function() {
    parent.layer.close(window.parent.SktAdPositions.layerIndex);
}

/**
 * 收集数据
 */
SktAdPositionsInfoDlg.collectData = function() {
    this
    .set('positionId')
    .set('positionType')
    .set('positionName')
    .set('positionWidth')
    .set('positionHeight')
    .set('dataFlag')
    .set('positionCode')
    .set('apSort');
}

/**
 * 提交添加
 */
SktAdPositionsInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var positionName = $("#positionName").val();
    var positionWidth = $("#positionWidth").val();
    var positionHeight = $("#positionHeight").val();
    var positionCode = $("#positionCode").val();
    var positionType = $("#positionType").val();

    if(positionName == null || positionName ==""){
        var html='<span class="error">广告位置不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#positionName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(positionType == null || positionType =="-1"){
        var html='<span class="error">位置类型不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#positionType').after(html);
        falg = 1;
    }
    
    if(positionWidth == null || positionWidth ==""){
        var html='<span class="error">建议宽度不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#positionWidth').after(html);
        falg = 1;
    }

    if(positionHeight == null || positionHeight ==""){
        var html='<span class="error">建议高度不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#positionHeight').after(html);
        falg = 1;
    }

    if(positionCode == null || positionCode ==""){
        var html='<span class="error">广告位置代码不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#positionCode').after(html);
        falg = 1;
    }

    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktAdPositions/add", function(data){
        Feng.success(data);
        window.parent.SktAdPositions.table.refresh();
        SktAdPositionsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAdPositionsInfoData);
    ajax.start();
    }else{
    Feng.success("请核实添加字段!");
    }
}

/**
 * 提交修改
 */
SktAdPositionsInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var positionName = $("#positionName").val();
    var positionWidth = $("#positionWidth").val();
    var positionHeight = $("#positionHeight").val();
    var positionCode = $("#positionCode").val();
    var positionType = $("#positionType").val();

    if(positionName == null || positionName ==""){
        var html='<span class="error">广告位置不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#positionName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(positionType == null || positionType =="-1"){
        var html='<span class="error">位置类型不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#positionType').after(html);
        falg = 1;
    }
    
    if(positionWidth == null || positionWidth ==""){
        var html='<span class="error">建议宽度不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#positionWidth').after(html);
        falg = 1;
    }

    if(positionHeight == null || positionHeight ==""){
        var html='<span class="error">建议高度不能为空!</span>';
        //$('#menuUrl').parent().append(html);
        $('#positionHeight').after(html);
        falg = 1;
    }

    if(positionCode == null || positionCode ==""){
        var html='<span class="error">广告位置代码不能为空!</span>';
        //$('#iconClass').parent().append(html);
        $('#positionCode').after(html);
        falg = 1;
    }


    this.clearData();
    this.collectData();

    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/sktAdPositions/update", function(data){
        Feng.success("修改成功!");
        window.parent.SktAdPositions.table.refresh();
        SktAdPositionsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.sktAdPositionsInfoData);
    ajax.start();
}else{
    Feng.success("请核实添加字段!");
}
}

$(function() {
    $('#positionType').val($('#positionType1').val());
});
