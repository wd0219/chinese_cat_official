/**
 * 文章分类管理初始化
 */
var ArticleCats = {
    id: "ArticleCatsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ArticleCats.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
	        {
	        	title:'id',align: 'center', valign: 'middle',width:50,  
			    formatter: function (value, row, index) {
			        return index+1;
			    }
			},
            {title: '分类名称', field: 'catName', visible: true, align: 'center', valign: 'middle'},
            /*{title: '文章分类ID', field: 'catId', visible: false, align: 'center', valign: 'middle'},
            {title: '文章分类父ID', field: 'parentId', visible: false, align: 'center', valign: 'middle'},
            */{title: '级别类型', field: 'catType', visible: true, align: 'center', valign: 'middle'},
            {title: '是否显示', field: 'isShowName', visible: true, align: 'center', valign: 'middle'
            	},           
            {title: '排序号', field: 'catSort', visible: true, align: 'center', valign: 'middle'},
            /*{title: '删除标志', field: 'dataFlag', visible: false, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: false, align: 'center', valign: 'middle'},
           */ {title: '操作', field:'op', visible: true, align: 'center', valign: 'middle',formatter:ArticleCats.op}

            
            ];
};
ArticleCats.op = function () {
	return "<a href='javascript:void(0)' onclick=\"ArticleCats.openAddArticleChildCats()\">增加子分类</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"ArticleCats.openArticleCatsDetail()\">修改</a>&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)'onclick=\"ArticleCats.delete2()\">删除</a>"
}
/**
 * 检查是否选中
 */
ArticleCats.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ArticleCats.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加文章分类
 */
ArticleCats.openAddArticleCats = function () {
	
    var index = layer.open({
        type: 2,
        title: '添加文章分类',
        area: ['100%', '100%'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/articleCats/articleCats_add'
    });
    this.layerIndex = index;
};
/**
 * 点击添加文章分类(子类)
 */
ArticleCats.openAddArticleChildCats = function () {
	if (this.check()) {
	    var index = layer.open({
	        type: 2,
	        title: '添加文章分类子类',
	        area: ['100%', '100%'], //宽高
	        fix: false, //不固定
	        maxmin: true,
	        content: Feng.ctxPath + '/articleCats/articleChildCats_add/'+ArticleCats.seItem.id
	    });
	    this.layerIndex = index;
	}
};

/**
 * 打开查看文章分类详情
 */
ArticleCats.openArticleCatsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '文章分类详情',
            area: ['100%', '100%'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/articleCats/articleCats_update/' + ArticleCats.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除文章分类
 */
ArticleCats.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/articleCats/delete", function (data) {
            Feng.success("删除成功!");
            ArticleCats.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("articleCatsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 删除文章分类
 */
ArticleCats.delete2 = function () {
		if (this.check()) {
	    	var catId=this.seItem.id;
	    	var operation = function(){
	        var ajax = new $ax(Feng.ctxPath + "/articleCats/delete", function (data) {
	            Feng.success("删除成功!");
	            ArticleCats.table.refresh();
	        }, function (data) {
	            Feng.error("删除失败!" + data.responseJSON.message + "!");
	        });
	        ajax.set("articleCatsId",catId);
	        ajax.start();
	    };
	    Feng.confirm("是否删除该条记录?",operation);
    }
};
/**
 * 查询文章分类列表
 */
ArticleCats.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    ArticleCats.table.refresh({query: queryData});
};

$(function () {
  /*  var defaultColunms = ArticleCats.initColumn();
    var table = new BSTable(ArticleCats.id, "/articleCats/list", defaultColunms);
    table.setPaginationType("client");
    ArticleCats.table = table.init();*/
	var defaultColunms = ArticleCats.initColumn();
	var table = new BSTreeTable(ArticleCats.id, "/articleCats/list", defaultColunms);
	table.setExpandColumn(0);
	table.setIdField("catId");
	table.setCodeField("catId");
	table.setParentCodeField("parentId");
	table.setExpandAll(false);
	table.init();
	ArticleCats.table = table;
});
