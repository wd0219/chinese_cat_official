/**
 * 初始化文章分类详情对话框
 */
var ArticleCatsInfoDlg = {
    articleCatsInfoData : {}
};

/**
 * 清除数据
 */
ArticleCatsInfoDlg.clearData = function() {
    this.articleCatsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ArticleCatsInfoDlg.set = function(key, val) {
    this.articleCatsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ArticleCatsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ArticleCatsInfoDlg.close = function() {
    parent.layer.close(window.parent.ArticleCats.layerIndex);
}

/**
 * 收集数据
 */
ArticleCatsInfoDlg.collectData = function() {
    this
    .set('catId')
    .set('parentId')
    .set('catType')
    .set('isShow')
    .set('catName')
    .set('catSort')
    .set('dataFlag')
    .set('createTime');
}

/**
 * 提交添加
 */
ArticleCatsInfoDlg.addSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var catName = $("#catName").val();
    var catSort = $("#catSort").val();

    if(catName == null || catName ==""){
        var html='<span class="error">分类名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#catName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(catSort == null || catSort ==""){
        var html='<span class="error">排序号不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#catSort').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    this.clearData();
    this.collectData();
    this.articleCatsInfoData['isShow']= $('input[name=isShow]:checked').val();
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/articleCats/add", function(data){
        Feng.success("添加成功!");
        window.parent.ArticleCats.table.refresh();
        ArticleCatsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.articleCatsInfoData);
    ajax.start();
}else{
    Feng.success("请核实添加字段!");
}
}

/**
 * 提交修改
 */
ArticleCatsInfoDlg.editSubmit = function() {
    $('.col-sm-9 span').html("");
    var falg = 0;

    var catName = $("#catName").val();
    var catSort = $("#catSort").val();

    if(catName == null || catName ==""){
        var html='<span class="error">分类名称不能为空!</span>';
        //$('#menuName'.parent()).append(html);
        $('#catName').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    if(catSort == null || catSort ==""){
        var html='<span class="error">排序号不能为空!</span>';
        //$('#menuType').parent().append(html);
        $('#catSort').after(html);
        falg = 1;
    }else {
        var html = '';
    }

    this.clearData();
    this.collectData();
    var radios = document.getElementsByName("isShow");
    // alert(radios);
     for ( var i = 0; i < radios.length; i++) {
 	    if (radios[i].checked==true){
 	    	this.articleCatsInfoData.isShow=$(radios[i]).val();
 	    }
     }
    //提交信息
    if(falg == 0){
    var ajax = new $ax(Feng.ctxPath + "/articleCats/update", function(data){
        Feng.success("修改成功!");
        window.parent.ArticleCats.table.refresh();
        ArticleCatsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.articleCatsInfoData);
    ajax.start();
}else{
    Feng.success("请核实添加字段!");
}
}

$(function() {
	if($('#isShowC').val()==1){
		$('#isShow').attr('checked', 'checked');
	}else{
		$('#isNotShow').attr('checked', 'checked');
	}
});
