/**
 * 推荐记录表管理初始化
 */
var Recommends = {
    id: "RecommendsTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Recommends.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '商品分类ID', field: 'goodsCatId', visible: true, align: 'center', valign: 'middle'},
            {title: '数据类型', field: 'dataType', visible: true, align: 'center', valign: 'middle'},
            {title: '数据来源', field: 'dataSrc', visible: true, align: 'center', valign: 'middle'},
            {title: '数据再其表中的主键', field: 'dataId', visible: true, align: 'center', valign: 'middle'},
            {title: '数据排序号', field: 'dataSort', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Recommends.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Recommends.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加推荐记录表
 */
Recommends.openAddRecommends = function () {
    var index = layer.open({
        type: 2,
        title: '添加推荐记录表',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/recommends/recommends_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看推荐记录表详情
 */
Recommends.openRecommendsDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '推荐记录表详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/recommends/recommends_update/' + Recommends.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除推荐记录表
 */
Recommends.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/recommends/delete", function (data) {
            Feng.success("删除成功!");
            Recommends.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("recommendsId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询推荐记录表列表
 */
Recommends.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Recommends.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Recommends.initColumn();
    var table = new BSTable(Recommends.id, "/recommends/list", defaultColunms);
    table.setPaginationType("client");
    Recommends.table = table.init();
});
