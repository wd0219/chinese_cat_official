/**
 * 初始化推荐记录表详情对话框
 */
var RecommendsInfoDlg = {
    recommendsInfoData : {}
};

/**
 * 清除数据
 */
RecommendsInfoDlg.clearData = function() {
    this.recommendsInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RecommendsInfoDlg.set = function(key, val) {
    this.recommendsInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RecommendsInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RecommendsInfoDlg.close = function() {
    parent.layer.close(window.parent.Recommends.layerIndex);
}

/**
 * 收集数据
 */
RecommendsInfoDlg.collectData = function() {
    this
    .set('id')
    .set('goodsCatId')
    .set('dataType')
    .set('dataSrc')
    .set('dataId')
    .set('dataSort');
}

/**
 * 提交添加
 */
RecommendsInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/recommends/add", function(data){
        Feng.success("添加成功!");
        window.parent.Recommends.table.refresh();
        RecommendsInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.recommendsInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
RecommendsInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/recommends/update", function(data){
        Feng.success("修改成功!");
        window.parent.Recommends.table.refresh();
        RecommendsInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.recommendsInfoData);
    ajax.start();
}

$(function() {

});
