package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktOrdersStockscore;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersStockscoreService;

import java.util.List;

/**
 * 购买积分控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 14:21:33
 */
@Controller
@RequestMapping("/sktOrdersStockscore")
public class SktOrdersStockscoreController extends BaseController {

    private String PREFIX = "/huamao/sktOrdersStockscore/";

    @Autowired
    private ISktOrdersStockscoreService sktOrdersStockscoreService;

    /**
     * 跳转到购买积分首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktOrdersStockscore.html";
    }

    /**
     * 跳转到添加购买积分
     */
    @RequestMapping("/sktOrdersStockscore_add")
    public String sktOrdersStockscoreAdd() {
        return PREFIX + "sktOrdersStockscore_add.html";
    }

    /**
     * 跳转到修改购买积分
     */
    @RequestMapping("/sktOrdersStockscore_update/{sktOrdersStockscoreId}")
    public String sktOrdersStockscoreUpdate(@PathVariable Integer sktOrdersStockscoreId, Model model) {
        SktOrdersStockscore sktOrdersStockscore = sktOrdersStockscoreService.selectById(sktOrdersStockscoreId);
        model.addAttribute("item",sktOrdersStockscore);
        LogObjectHolder.me().set(sktOrdersStockscore);
        return PREFIX + "sktOrdersStockscore_edit.html";
    }

    /**
     * 获取购买积分列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktOrdersStockscore sktOrdersStockscore) {
        Page<SktOrdersStockscore> page = new PageFactory<SktOrdersStockscore>().defaultPage();
        List<SktOrdersStockscore> sktOrdersStockscores = sktOrdersStockscoreService.sktOrdersStockscorefindall(page,sktOrdersStockscore);
        page.setRecords(sktOrdersStockscores);
        return super.packForBT(page);
    }

    /**
     * 新增购买积分
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktOrdersStockscore sktOrdersStockscore) {
        sktOrdersStockscoreService.insert(sktOrdersStockscore);
        return SUCCESS_TIP;
    }

    /**
     * 删除购买积分
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktOrdersStockscoreId) {
        sktOrdersStockscoreService.deleteById(sktOrdersStockscoreId);
        return SUCCESS_TIP;
    }

    /**
     * 修改购买积分
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktOrdersStockscore sktOrdersStockscore) {
        sktOrdersStockscoreService.updateById(sktOrdersStockscore);
        return SUCCESS_TIP;
    }

    /**
     * 购买积分详情
     */
    @RequestMapping(value = "/detail/{sktOrdersStockscoreId}")
    @ResponseBody
    public Object detail(@PathVariable("sktOrdersStockscoreId") Integer sktOrdersStockscoreId) {
        return sktOrdersStockscoreService.selectById(sktOrdersStockscoreId);
    }
}
