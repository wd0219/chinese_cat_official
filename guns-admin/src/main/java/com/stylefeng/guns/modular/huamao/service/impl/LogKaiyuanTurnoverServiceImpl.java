package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanTurnoverMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnover;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnoverList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanTurnoverService;

/**
 * <p>
 * 开元宝营业额流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
@Service
public class LogKaiyuanTurnoverServiceImpl extends ServiceImpl<LogKaiyuanTurnoverMapper, LogKaiyuanTurnover>
		implements ILogKaiyuanTurnoverService {

	@Autowired
	private LogKaiyuanTurnoverMapper ltm;

	// 开元宝营业额流水表展示
	@Override
	public List<LogKaiyuanTurnoverList> selectLogKaiyuanTurnoverAll(Page<LogKaiyuanTurnoverList> page, LogKaiyuanTurnoverList logKaiyuanTurnoverList) {
		Integer total = ltm.selectLogKaiyuanTurnoverAllCount(logKaiyuanTurnoverList);
		page.setTotal(total);
		return ltm.selectLogKaiyuanTurnoverAll(page,logKaiyuanTurnoverList);
	}

}
