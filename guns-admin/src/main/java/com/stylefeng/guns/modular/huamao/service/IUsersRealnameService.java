package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;
import com.stylefeng.guns.modular.huamao.model.UsersRealnameDTO;

/**
 * <p>
 * 实名认证信息表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
public interface IUsersRealnameService extends IService<UsersRealname> {
	/**
	 * 查询认证列表
	 * @param page 
	 * @param usersRealname
	 * @return
	 */
	public List<UsersRealnameDTO> showRenZhengInfo(Page<UsersRealnameDTO> page, UsersRealnameDTO usersRealname);
	
	/**
	 * 详情
	 * @param realId
	 * @return
	 */
	public UsersRealnameDTO showRenZhengXiangQing(Integer realId);
	
	/**
	 * 根据用户id查询用户身份证
	 * @param userId
	 * @return
	 */
	public Map<String,Object> selectUserCardId(Integer userId);
	
}
