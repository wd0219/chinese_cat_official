package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktBanks;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 银行表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface SktBanksMapper extends BaseMapper<SktBanks> {

}
