package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktClassType;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 视频类型表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface SktClassTypeMapper extends BaseMapper<SktClassType> {
    List<SktClassType> selectSktClassTypeAll(@Param("page") Page<SktClassType> page, @Param("sktClassType") SktClassType sktClassType);
    Integer selectSktClassTypeAllCount(@Param("sktClassType") SktClassType sktClassType);

}
