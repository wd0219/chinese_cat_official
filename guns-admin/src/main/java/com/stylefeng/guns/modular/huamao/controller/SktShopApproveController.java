package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import com.stylefeng.guns.modular.huamao.service.ISktShopApproveService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2018-05-10 16:36:36
 */
@Controller
@RequestMapping("/sktShopApprove")
public class SktShopApproveController extends BaseController {

    private String PREFIX = "/huamao/sktShopApprove/";

    @Autowired
    private ISktShopApproveService sktShopApproveService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopApprove.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/sktShopApprove_add")
    public String sktShopApproveAdd() {
        return PREFIX + "sktShopApprove_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/sktShopApprove_update/{sktShopApproveId}")
    public String sktShopApproveUpdate(@PathVariable Integer sktShopApproveId, Model model) {
        SktShopApprove sktShopApprove = sktShopApproveService.selectById(sktShopApproveId);
        model.addAttribute("item",sktShopApprove);
        LogObjectHolder.me().set(sktShopApprove);
        return PREFIX + "sktShopApprove_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktShopApproveService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopApprove sktShopApprove) {
        sktShopApproveService.insert(sktShopApprove);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopApproveId) {
        sktShopApproveService.deleteById(sktShopApproveId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopApprove sktShopApprove) {
        sktShopApproveService.updateById(sktShopApprove);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{sktShopApproveId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopApproveId") Integer sktShopApproveId) {
        return sktShopApproveService.selectById(sktShopApproveId);
    }
}
