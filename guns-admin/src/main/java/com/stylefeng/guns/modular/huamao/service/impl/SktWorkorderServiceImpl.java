package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktWorkorderMapper;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;
import com.stylefeng.guns.modular.huamao.service.ISktWorkorderService;

/**
 * <p>
 * 工单表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
@Service
public class SktWorkorderServiceImpl extends ServiceImpl<SktWorkorderMapper, SktWorkorder> implements ISktWorkorderService {
	@Autowired
	private SktWorkorderMapper sktWorkorderMapper;
	@Override
	public List<SktWorkorder> sktWorkorderfindAll(Page<SktWorkorder>page,SktWorkorder sktWorkorder) {
		List<SktWorkorder> list = sktWorkorderMapper.sktWorkorderfindAll(page,sktWorkorder);
		Integer total = sktWorkorderMapper.sktWorkorderfindAllCount(sktWorkorder);
		page.setTotal(total);
		return list;
	}
	@Override
	public List<SktWorkorder> sktWorkorderfindup(SktWorkorder sktWorkorder) {
		return sktWorkorderMapper.sktWorkorderfindup(sktWorkorder);
	}

}
