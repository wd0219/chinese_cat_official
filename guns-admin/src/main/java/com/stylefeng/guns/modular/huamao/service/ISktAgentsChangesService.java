package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktAgentsChanges;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 代理公司的股东更改表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-10
 */
public interface ISktAgentsChangesService extends IService<SktAgentsChanges> {

}
