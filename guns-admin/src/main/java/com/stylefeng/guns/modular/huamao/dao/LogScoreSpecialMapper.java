package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecial;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecialList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 待发特别积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface LogScoreSpecialMapper extends BaseMapper<LogScoreSpecial> {

	// 待发特别积分流水表展示
	List<LogScoreSpecialList> selectLogScoreSpecialAll(@Param("page")Page<LogScoreSpecialList> page,@Param("logScoreSpecialList") LogScoreSpecialList logScoreSpecialList);

    Integer selectLogScoreSpecialAllCount(@Param("logScoreSpecialList") LogScoreSpecialList logScoreSpecialList);
}
