package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.annotion.Permission;
import com.stylefeng.guns.core.common.constant.Const;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.system.model.OperationLog;
import com.stylefeng.guns.modular.system.warpper.LogWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.LogScoreList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreService;

import java.util.List;

/**
 * 积分记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-16 10:13:58
 */
@Controller
@RequestMapping("/logScore")
public class LogScoreController extends BaseController {

	private String PREFIX = "/huamao/logScore/";

	@Autowired
	private ILogScoreService logScoreService;

	/**
	 * 跳转到积分记录首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, Model model) {
		// 参数是为了在用户点击积分查看积分详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logScore.html";
	}

	/**
	 * 跳转到添加积分记录
	 */
	@RequestMapping("/logScore_add")
	public String logScoreAdd() {
		return PREFIX + "logScore_add.html";
	}

	/**
	 * 跳转到修改积分记录
	 */
	@RequestMapping("/logScore_update/{logScoreId}")
	public String logScoreUpdate(@PathVariable Integer logScoreId, Model model) {
		LogScore logScore = logScoreService.selectById(logScoreId);
		model.addAttribute("item", logScore);
		LogObjectHolder.me().set(logScore);
		return PREFIX + "logScore_edit.html";
	}

	/**
	 * 获取积分记录列表 ：{userPhone2}用户查看积分详情传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone, LogScoreList logScoreList) {
		// 判断如果不是‘手机号’并且logScoreList.getUserPhone()不是null说明要看用户积分详情，是‘手机号’并且logScoreList.getUserPhone()是''或是有数据说明是普通的查询所有的积分详情
		if (!phone.equals("手机号") && logScoreList.getUserPhone() == null) {
			logScoreList.setUserPhone(phone);
		}
		Page<LogScoreList> page = new PageFactory<LogScoreList>().defaultPage();
		List<LogScoreList> logScoreLists = logScoreService.selectLogScoreAll(page,logScoreList);
		page.setRecords(logScoreLists);
		return super.packForBT(page);
	}

	/**
	 * 新增积分记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogScore logScore) {
		logScoreService.insert(logScore);
		return SUCCESS_TIP;
	}

	/**
	 * 删除积分记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logScoreId) {
		logScoreService.deleteById(logScoreId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改积分记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogScore logScore) {
		logScoreService.updateById(logScore);
		return SUCCESS_TIP;
	}

	/**
	 * 积分记录详情
	 */
	@RequestMapping(value = "/detail/{logScoreId}")
	@ResponseBody
	public Object detail(@PathVariable("logScoreId") Integer logScoreId) {
		return logScoreService.selectById(logScoreId);
	}
}
