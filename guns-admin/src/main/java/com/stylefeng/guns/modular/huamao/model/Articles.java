package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 文章记录表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
@TableName("skt_articles")
public class Articles extends Model<Articles> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "articleId", type = IdType.AUTO)
    private Integer articleId;
    /**
     * 父类ID
     */
    private Integer catId;
    /**
     * 文章标题
     */
    private String articleTitle;
    /**
     * 是否显示
     */
    private Integer isShow;
    /**
     * 文章内容
     */
    private String articleContent;
    /**
     * 关键字
     */
    private String articleKey;
    /**
     * 创建者
     */
    private Integer staffId;
    /**
     * 状态 0未读 1已读
     */
    private Integer status;
    /**
     * 有效状态
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 觉得文章有帮助的次数
     */
    private Integer solve;
    /**
     * 觉得文章没帮助的次数
     */
    private Integer unsolve;


    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public String getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }

    public String getArticleKey() {
        return articleKey;
    }

    public void setArticleKey(String articleKey) {
        this.articleKey = articleKey;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getSolve() {
        return solve;
    }

    public void setSolve(Integer solve) {
        this.solve = solve;
    }

    public Integer getUnsolve() {
        return unsolve;
    }

    public void setUnsolve(Integer unsolve) {
        this.unsolve = unsolve;
    }

    @Override
    protected Serializable pkVal() {
        return this.articleId;
    }

    @Override
    public String toString() {
        return "Articles{" +
        "articleId=" + articleId +
        ", catId=" + catId +
        ", articleTitle=" + articleTitle +
        ", isShow=" + isShow +
        ", articleContent=" + articleContent +
        ", articleKey=" + articleKey +
        ", staffId=" + staffId +
        ", status=" + status +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", solve=" + solve +
        ", unsolve=" + unsolve +
        "}";
    }
}
