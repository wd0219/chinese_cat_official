package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.model.UsersAddressDTO;

/**
 * <p>
 * 会员地址表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
public interface UsersAddressMapper extends BaseMapper<UsersAddress> {
	/**
	 * 查询用户地址列表
	 * @param page 
	 * @return
	 */
	public List<UsersAddressDTO> showUserAddressInfo(@Param("page")Page<UsersAddressDTO> page, 
			@Param("usersAddressDTO")UsersAddressDTO usersAddressDTO);

	public Integer showUserAddressInfoCount(@Param("usersAddressDTO")UsersAddressDTO usersAddressDTO); 
	
}
