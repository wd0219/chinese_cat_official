package com.stylefeng.guns.modular.app.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.app.service.IAppUserService;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.model.Users;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author wangang
 * @since 2018-05-7
 */
@Service
public class AppUserServiceImpl extends ServiceImpl<UsersMapper, Users> implements IAppUserService {
	@Autowired
	UsersMapper appUserMapper;

	// 查询用户账户列表
	@Override
	public List<Users> findListByLoginName(@Param("loginName")String loginName) {
		List<Users> listUser = appUserMapper.findListByLoginName(loginName);
		return listUser;
	}

	/**
	 * 查询分享人
	 * @param shareMan
	 * @return
	 */
	@Override
	public Users findUserByLoginNameOrUserPhone(String shareMan) {
		return appUserMapper.selectUserByLoginNameOrUserPhone(shareMan);
	}

	@Override
	public boolean addUserRegister(Users users){
		int usermsgs=appUserMapper.insert(users);
		if(usermsgs==0){
			return false;
		}
		return true;
	}

	@Override
	public Users findUsersByUserPhoneAndOldpayPwd(Users users) {
		return appUserMapper.selectUsersByUSerPhoneAndPayPwd(users);
	}

}
