package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.spec.EncodedKeySpec;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroUser;

import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.ArticleCats;
import com.stylefeng.guns.modular.huamao.model.Articles;
import com.stylefeng.guns.modular.huamao.model.ArticlesCustem;
import com.stylefeng.guns.modular.huamao.service.IArticleCatsService;
import com.stylefeng.guns.modular.huamao.service.IArticlesService;
import com.stylefeng.guns.modular.huamao.warpper.ArticlesWapper;
import org.springframework.web.util.HtmlUtils;

/**
 * 文章管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-26 20:21:13
 */
@Controller
@RequestMapping("/articles")
public class ArticlesController extends BaseController {

    private String PREFIX = "/huamao/articles/";

    @Autowired
    private IArticlesService articlesService;
    @Autowired
    private IArticleCatsService articleCatsService;
    /**
     * 跳转到文章管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "articles.html";
    }

    /**
     * 跳转到添加文章管理
     */
    @RequestMapping("/articles_add")
    public String articlesAdd() {
        return PREFIX + "articles_add.html";
    }
    /**
     * 跳转到添加文章管理
     */
    @RequestMapping("/model")
    public String model() {
        return PREFIX + "model.html";
    }
    /**
     * 跳转到修改文章管理
     */
    @RequestMapping("/articles_update/{articlesId}")
    public String articlesUpdate(@PathVariable Integer articlesId, Model model) {
        Articles articles = articlesService.selectById(articlesId);
        Integer catId = articles.getCatId();
        ArticleCats articleCats = articleCatsService.selectById(catId);
        model.addAttribute("parentId",articleCats.getParentId());      
        model.addAttribute("item",articles);
        
        LogObjectHolder.me().set(articles);
        return PREFIX + "articles_edit.html";
    }

    /**
     * 获取文章管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(Articles articles) {
    	Page<Map<String,Object>> page = new PageFactory<Map<String,Object>>().defaultPage();
    	List<Map<String, Object>> articlesList= articlesService.selectArticles(page,articles);
    	page.setRecords(articlesList);
    	return super.packForBT(page);
    	
    	//	 return new ArticlesWapper(articlesList).warp();
    }

    /**
     * 新增文章管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ArticlesCustem articlesCustem) {
        String s = org.springframework.web.util.HtmlUtils.htmlUnescape(articlesCustem.getArticleContent());
        articlesCustem.setArticleContent(s);
        Articles articles=new Articles();
    	if(articlesCustem.getArticle1()!=null && !"".equals(articlesCustem.getArticle1())){
    		if(articlesCustem.getArticle2()!=null && !"".equals(articlesCustem.getArticle2())){
    			articles.setCatId(articlesCustem.getArticle2());;
    		}else{
    			articles.setCatId(articlesCustem.getArticle1());;
    		}
    	}
    	articles.setArticleTitle(articlesCustem.getArticleTitle());
    	articles.setIsShow(articlesCustem.getIsShow());
    	articles.setArticleContent(articlesCustem.getArticleContent());
    	articles.setArticleKey(articlesCustem.getArticleKey());
    	// 获取操作人员id
		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
	//	ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
		int staffId=shiroUser.getId();
    	articles.setStaffId(staffId);//取session
    	articles.setStatus(0);
    	articles.setCreateTime( new Date());
        articlesService.insert(articles);
        return SUCCESS_TIP;
    }

    /**
     * 删除文章管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer articlesId) {
        articlesService.deleteById(articlesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改文章管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ArticlesCustem articlesCustem) {
        String s = org.springframework.web.util.HtmlUtils.htmlUnescape(articlesCustem.getArticleContent());
        articlesCustem.setArticleContent(s);
    	Articles articles=articlesService.selectById(articlesCustem.getArticleId());
    	if(articlesCustem.getArticle1()!=null && !"".equals(articlesCustem.getArticle1())){
    		if(articlesCustem.getArticle2()!=null && !"".equals(articlesCustem.getArticle2())){
    			articles.setCatId(articlesCustem.getArticle2());
    		}else{
    			articles.setCatId(articlesCustem.getArticle1());
    		}
    	}
    	articles.setArticleTitle(articlesCustem.getArticleTitle());
    	articles.setIsShow(articlesCustem.getIsShow());
    	articles.setArticleContent(articlesCustem.getArticleContent());
    	articles.setArticleKey(articlesCustem.getArticleKey());
        articlesService.updateById(articles);
        return SUCCESS_TIP;
    }

    /**
     * 文章管理详情
     */
    @RequestMapping(value = "/detail/{articlesId}")
    @ResponseBody
    public Object detail(@PathVariable("articlesId") Integer articlesId) {
        return articlesService.selectById(articlesId);
    }
}
