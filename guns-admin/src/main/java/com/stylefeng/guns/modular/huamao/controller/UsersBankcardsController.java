package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.model.UsersBankcardsList;
import com.stylefeng.guns.modular.huamao.service.IUsersBankcardsService;

/**
 * 用户银行卡控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 16:01:08
 */
@Controller
@RequestMapping("/usersBankcards")
public class UsersBankcardsController extends BaseController {

	private String PREFIX = "/huamao/usersBankcards/";

	@Autowired
	private IUsersBankcardsService usersBankcardsService;

	/**
	 * 跳转到用户银行卡首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "usersBankcards.html";
	}

	/**
	 * 跳转到添加用户银行卡
	 */
	@RequestMapping("/usersBankcards_add")
	public String usersBankcardsAdd() {
		return PREFIX + "usersBankcards_add.html";
	}

	/**
	 * 跳转到修改用户银行卡
	 */
	@RequestMapping("/usersBankcards_update/{usersBankcardsId}")
	public String usersBankcardsUpdate(@PathVariable Integer usersBankcardsId, Model model) {
		UsersBankcards usersBankcards = usersBankcardsService.selectById(usersBankcardsId);
		model.addAttribute("item", usersBankcards);
		LogObjectHolder.me().set(usersBankcards);
		return PREFIX + "usersBankcards_edit.html";
	}

	/**
	 * 获取用户银行卡列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(UsersBankcardsList usersBankcardsList) {
		Page<UsersBankcardsList> page = new PageFactory<UsersBankcardsList>().defaultPage();
		List<UsersBankcardsList> usersBankcardsLists = usersBankcardsService.selectUsersBankcardsAll(page,usersBankcardsList);
		page.setRecords(usersBankcardsLists);
		return super.packForBT(page);
		// return usersBankcardsService.selectList(null);
	}

	/**
	 * 新增用户银行卡
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(UsersBankcards usersBankcards) {
		usersBankcards.setCreateTime(new Date());
		usersBankcards.setDataFlag(1);
		usersBankcardsService.insert(usersBankcards);
		return SUCCESS_TIP;
	}

	/**
	 * 删除用户银行卡
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer usersBankcardsId) {
		usersBankcardsService.deleteById(usersBankcardsId);
		return SUCCESS_TIP;
	}

	/**
	 * 通过ID删除用户银行卡
	 */
	@RequestMapping(value = "/deleteById")
	@ResponseBody
	public Object deleteById(@RequestParam Integer usersBankcardsId) {
		// 通过传过来的ID进行逻辑删除
		usersBankcardsService.deleteById2(usersBankcardsId);
		// usersBankcardsService.deleteById(usersBankcardsId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改用户银行卡
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(UsersBankcards usersBankcards) {
		usersBankcardsService.updateById(usersBankcards);
		return SUCCESS_TIP;
	}

	/**
	 * 用户银行卡详情
	 */
	@RequestMapping(value = "/detail/{usersBankcardsId}")
	@ResponseBody
	public Object detail(@PathVariable("usersBankcardsId") Integer usersBankcardsId) {
		return usersBankcardsService.selectById(usersBankcardsId);
	}
	
	/*********************首页****************************/
	/**
	 * 根据用户id获取用户银行卡列表
	 */
	@RequestMapping(value = "/selectUserBranklist")
	@ResponseBody
	public Object selectUserBranklist(UsersBankcardsList usersBankcardsList) {
		return usersBankcardsService.selectUserBranks(usersBankcardsList);
	}	
	
	/**
	 * 根据用户id快速认证银行卡?
	 */
	@RequestMapping(value = "/quickApprove")
	@ResponseBody
	public Object quickApprove(UsersBankcardsList usersBankcardsList) {
		return usersBankcardsService.selectUserBranks(usersBankcardsList);
	}
}
