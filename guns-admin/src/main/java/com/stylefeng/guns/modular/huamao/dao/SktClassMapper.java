package com.stylefeng.guns.modular.huamao.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktClass;
import com.stylefeng.guns.modular.huamao.model.SktClassDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商学院表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface SktClassMapper extends BaseMapper<SktClass> {

    SktClass selectSktClassByClassName(String className);


    List<SktClassDTO> selectSktClassAll(@Param("page") Page<SktClassDTO> page,@Param("sktClassDTO") SktClassDTO sktClassDTO);

    Integer selectSktClassAllCount(@Param("sktClassDTO") SktClassDTO sktClassDTO);
}
