package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 关注(商品/店铺)表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@TableName("skt_favorites")
public class SktFavorites extends Model<SktFavorites> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "favoriteId", type = IdType.AUTO)
    private Integer favoriteId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 收藏类型
     */
    private Integer favoriteType;
    /**
     * 对象ID
     */
    private Integer targetId;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(Integer favoriteId) {
        this.favoriteId = favoriteId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFavoriteType() {
        return favoriteType;
    }

    public void setFavoriteType(Integer favoriteType) {
        this.favoriteType = favoriteType;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.favoriteId;
    }

    @Override
    public String toString() {
        return "SktFavorites{" +
        "favoriteId=" + favoriteId +
        ", userId=" + userId +
        ", favoriteType=" + favoriteType +
        ", targetId=" + targetId +
        ", createTime=" + createTime +
        "}";
    }
}
