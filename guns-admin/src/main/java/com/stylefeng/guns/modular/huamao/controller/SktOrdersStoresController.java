package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStoresDto;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersStoresService;

/**
 * 线下订单管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 09:56:55
 */
@Controller
@RequestMapping("/sktOrdersStores")
public class SktOrdersStoresController extends BaseController {

    private String PREFIX = "/huamao/sktOrdersStores/";

    @Autowired
    private ISktOrdersStoresService sktOrdersStoresService;

    /**
     * 跳转到线下订单管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktOrdersStores.html";
    }

    /**
     * 跳转到添加线下订单管理
     */
    @RequestMapping("/sktOrdersStores_add")
    public String sktOrdersStoresAdd() {
        return PREFIX + "sktOrdersStores_add.html";
    }

    /**
     * 跳转到修改线下订单管理
     */
    @RequestMapping("/sktOrdersStores_update/{sktOrdersStoresId}")
    public String sktOrdersStoresUpdate(@PathVariable Integer sktOrdersStoresId, Model model) {
    	SktOrdersStoresDto  sktOrdersStoresDto = new SktOrdersStoresDto();
    	sktOrdersStoresDto.setOrderId(sktOrdersStoresId);
        List<SktOrdersStoresDto> sktOrdersStores = sktOrdersStoresService.showOrdersStoresInfo2(sktOrdersStoresDto);
        model.addAttribute("item",sktOrdersStores.size()>0?sktOrdersStores.get(0):null);
        LogObjectHolder.me().set(sktOrdersStores);
        return PREFIX + "sktOrdersStores_edit.html";
    }

    /**
     * 获取线下订单管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktOrdersStoresDto  sktOrdersStoresDto) {
    	 Page<SktOrdersStoresDto> page = new PageFactory<SktOrdersStoresDto>().defaultPage();
         List<SktOrdersStoresDto> list = sktOrdersStoresService.showOrdersStoresInfo(page,sktOrdersStoresDto);
         page.setRecords(list);
 		 return super.packForBT(page);
    }

    /**
     * 新增线下订单管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktOrdersStores sktOrdersStores) {
        sktOrdersStoresService.insert(sktOrdersStores);
        return SUCCESS_TIP;
    }

    /**
     * 删除线下订单管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktOrdersStoresId) {
        sktOrdersStoresService.deleteById(sktOrdersStoresId);
        return SUCCESS_TIP;
    }

    /**
     * 修改线下订单管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktOrdersStores sktOrdersStores) {
        sktOrdersStoresService.updateById(sktOrdersStores);
        return SUCCESS_TIP;
    }

    /**
     * 线下订单管理详情
     */
    @RequestMapping(value = "/detail/{sktOrdersStoresId}")
    @ResponseBody
    public Object detail(@PathVariable("sktOrdersStoresId") Integer sktOrdersStoresId) {
        return sktOrdersStoresService.selectById(sktOrdersStoresId);
    }
}
