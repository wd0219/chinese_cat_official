package com.stylefeng.guns.modular.huamao.model;

public class SktShoppingDTO extends  SktShopping {
    private String userPhone;
    /**
     * 查询时的开始时间
     */
    private String beginTime;
    /**
     * 查询时的结束时间
     */
    private String endTime;

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
