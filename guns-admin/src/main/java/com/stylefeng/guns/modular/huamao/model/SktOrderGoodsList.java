package com.stylefeng.guns.modular.huamao.model;

public class SktOrderGoodsList extends SktOrderGoods{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shopName;
	
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	
}
