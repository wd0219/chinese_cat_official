package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AgentsBankcardsMapper;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcards;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcardsList;
import com.stylefeng.guns.modular.huamao.service.IAgentsBankcardsService;

/**
 * <p>
 * 代理公司的银行卡表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
@Service
public class AgentsBankcardsServiceImpl extends ServiceImpl<AgentsBankcardsMapper, AgentsBankcards>
		implements IAgentsBankcardsService {

	@Autowired
	private AgentsBankcardsMapper abm;

	// 代理公司的银行卡表展示
	@Override
	public List<AgentsBankcardsList> selectAgentsBankcardsAll(Page<AgentsBankcardsList> page, AgentsBankcardsList agentsBankcardsList) {
		Integer total = abm.selectAgentsBankcardsAllCount(agentsBankcardsList);
		page.setTotal(total);
		return abm.selectAgentsBankcardsAll(page,agentsBankcardsList);
	}

	// 通过传过来的ID进行逻辑删除
	@Override
	public void deleteById2(Integer agentsDrawsId) {
		abm.deleteById2(agentsDrawsId);
	}

}
