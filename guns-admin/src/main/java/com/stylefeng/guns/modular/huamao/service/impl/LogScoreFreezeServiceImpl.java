package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreFreezeMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreeze;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreezeList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreFreezeService;

/**
 * <p>
 * 待发积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogScoreFreezeServiceImpl extends ServiceImpl<LogScoreFreezeMapper, LogScoreFreeze>
		implements ILogScoreFreezeService {

	@Autowired
	private LogScoreFreezeMapper lsfm;

	// 展示代发积分列表
	@Override
	public List<LogScoreFreezeList> selectLogScoreFreezeAll(Page<LogScoreFreezeList> page , LogScoreFreezeList logScoreFreezeList) {
		Integer total = lsfm.selectScoreFreezeListCount(logScoreFreezeList);
		page.setTotal(total);
		return lsfm.selectScoreFreezeList(page,logScoreFreezeList);
	}

}
