package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktLogAgentsLogins;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 代理公司登陆记录表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
public interface SktLogAgentsLoginsMapper extends BaseMapper<SktLogAgentsLogins> {
	
	public List<SktLogAgentsLogins> sktLogAgentsLoginfindup(@Param("page")Page<SktLogAgentsLogins> page,@Param("sktLogAgentsLogins") SktLogAgentsLogins sktLogAgentsLogins);

    Integer sktLogAgentsLoginfindupCount(@Param("sktLogAgentsLogins") SktLogAgentsLogins sktLogAgentsLogins);
}
