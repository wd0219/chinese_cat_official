package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktAgentsChanges;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsChangesMapper;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsChangesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代理公司的股东更改表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-10
 */
@Service
public class SktAgentsChangesServiceImpl extends ServiceImpl<SktAgentsChangesMapper, SktAgentsChanges> implements ISktAgentsChangesService {

}
