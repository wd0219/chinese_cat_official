package com.stylefeng.guns.modular.huamao.controller;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.stylefeng.guns.config.properties.GunsProperties;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktSysConfigs;
import com.stylefeng.guns.modular.huamao.service.ISktSysConfigsService;

/**	
 * 基础设置控制器
 *
 * @author fengshuonan
 * @Date 2018-04-26 11:07:45
 */
@Controller
@RequestMapping("/sktSysConfigs")
public class SktSysConfigsController extends BaseController {

    private String PREFIX = "/huamao/sktSysConfigs/";

    @Autowired
    private ISktSysConfigsService sktSysConfigsService;
    @Autowired
    private GunsProperties gunsProperties;
    /**
     * 跳转到基础设置首页
     */
    @RequestMapping("")
    public String index(Model model) {
    	model.addAttribute("page",1);
        return PREFIX + "sktSysConfigs.html";
    }

    @RequestMapping("/fuwuqi")
    public String fuwuqi(Model model) {
    	model.addAttribute("page",2);
        return PREFIX + "sktSysConfigs2.html";
    }
    @RequestMapping("/seo")
    public String seo(Model model) {
    	model.addAttribute("page",3);
        return PREFIX + "sktSysConfigs2.html";
    }
    
    @RequestMapping("/yunying")
    public String yunying(Model model) {
    	model.addAttribute("page",5);
        return PREFIX + "sktSysConfigs3.html";
    }
    @RequestMapping("/shuhui")
    public String shuhui(Model model) {
    	model.addAttribute("page",6);
        return PREFIX + "sktSysConfigs2.html";
    }

    /**
     * 获取二维码
     * @param model
     * @return
     */
    @RequestMapping("/getQR")
    public String getQR(Model model) {
        Map<String, Object> map = sktSysConfigsService.getQR();
        if (map!=null && map.size()>0){
            String alipayQR="";
            String weChatQR ="";
            String alipayQRId ="";
            String weChatQRId = "";
            if(!"".equals(map.get("alipayQR").toString())&& map.get("alipayQR").toString()!=null){
                alipayQR = map.get("alipayQR").toString();
            }
            if(!"".equals( map.get("weChatQR").toString())&& map.get("weChatQR").toString()!=null){
                weChatQR = map.get("weChatQR").toString();
            }

            if(!"".equals(map.get("alipayQRId").toString())&& map.get("alipayQRId").toString()!=null){
                alipayQRId = map.get("alipayQRId").toString();
            }
            if(!"".equals( map.get("weChatQRId").toString())&& map.get("weChatQRId").toString()!=null){
                weChatQRId = map.get("weChatQRId").toString();
            }
            model.addAttribute("alipayQR",alipayQR);
            model.addAttribute("weChatQR",weChatQR);
            model.addAttribute("alipayQRId",alipayQRId);
            model.addAttribute("weChatQRId",weChatQRId);

        }
        return PREFIX + "sktSysConfigs_QR.html";
    }
    /**
     * 跳转到基础设置首页
     */
    @RequestMapping("/pic")
    public String pic(Model model) {
    	model.addAttribute("page",4);
        return PREFIX + "sktSysConfigs_pic.html";
    }
    /**
     * 跳转到添加基础设置
     */
    @RequestMapping("/sktSysConfigs_add")
    public String sktSysConfigsAdd() {
        return PREFIX + "sktSysConfigs_add.html";
    }

    /**
     * 跳转到修改基础设置
     */
    @RequestMapping("/sktSysConfigs_update/{sktSysConfigsId}")
    public String sktSysConfigsUpdate(@PathVariable Integer sktSysConfigsId, Model model) {
        SktSysConfigs sktSysConfigs = sktSysConfigsService.selectById(sktSysConfigsId);
        model.addAttribute("item",sktSysConfigs);
        LogObjectHolder.me().set(sktSysConfigs);
        return PREFIX + "sktSysConfigs_edit.html";
    }

    /**
     * 获取基础设置列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(Integer page) {
    	List<Map<String,Object>> list = sktSysConfigsService.selectBaseSetInfo(page);
        return list;
    }

    /**
     * 新增基础设置
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktSysConfigs sktSysConfigs) {
        sktSysConfigsService.insert(sktSysConfigs);
        return SUCCESS_TIP;
    }

    /**
     * 删除基础设置
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktSysConfigsId) {
        sktSysConfigsService.deleteById(sktSysConfigsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改基础设置
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktSysConfigs sktSysConfigs) {
        sktSysConfigsService.updateById(sktSysConfigs);
        return SUCCESS_TIP;
    }
    /**
     * 上传图片(上传到项目的webapp/static/img)
     */
    @RequestMapping(method = RequestMethod.POST, path = "/upload/{fieldCode}")
    @ResponseBody
    public String upload(@RequestParam("imgs") String imgs,@PathVariable String fieldCode) {
        try {
            sktSysConfigsService.updateFieldCode(fieldCode,imgs);
        } catch (Exception e) {
            throw new GunsException(BizExceptionEnum.UPLOAD_ERROR);
        }
        return imgs;
    }
    /**
     * 修改基础设置
     */
    @RequestMapping(value = "/updateJc")
    @ResponseBody
    public Object updateJc(@RequestBody List<SktSysConfigs> sktSysConfigs) {
        sktSysConfigsService.updateByIds(sktSysConfigs);
        return SUCCESS_TIP;
    }

    /**
     * 基础设置详情
     */
    @RequestMapping(value = "/detail/{sktSysConfigsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktSysConfigsId") Integer sktSysConfigsId) {
        return sktSysConfigsService.selectById(sktSysConfigsId);
    }
}
