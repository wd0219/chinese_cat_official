package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 店铺分类表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-29
 */
@TableName("skt_shop_cats")
public class SktShopCats extends Model<SktShopCats> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "catId", type = IdType.AUTO)
    private Integer catId;
    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 父ID
     */
    private Integer parentId;
    /**
     * 是否显示：0隐藏1显示
     */
    private Integer isShow;
    /**
     * 分类名称
     */
    private String catName;
    /**
     * 分类图片
     */
    private String catImg;
    /**
     * 排序号
     */
    private Integer catSort;
    /**
     * 有效状态：1有效-1无效
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatImg() {
        return catImg;
    }

    public void setCatImg(String catImg) {
        this.catImg = catImg;
    }

    public Integer getCatSort() {
        return catSort;
    }

    public void setCatSort(Integer catSort) {
        this.catSort = catSort;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.catId;
    }

    @Override
    public String toString() {
        return "SktShopCats{" +
        "catId=" + catId +
        ", shopId=" + shopId +
        ", parentId=" + parentId +
        ", isShow=" + isShow +
        ", catName=" + catName +
        ", catImg=" + catImg +
        ", catSort=" + catSort +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
