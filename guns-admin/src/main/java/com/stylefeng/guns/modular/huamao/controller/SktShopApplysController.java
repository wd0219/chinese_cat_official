package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.shiro.ShiroUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.support.DateTime;

import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;
import com.stylefeng.guns.modular.huamao.service.ISktShopApplysService;

/**
 * 商城商家开店申请控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 17:51:01
 */
@Controller
@RequestMapping("/sktShopApplys")
public class SktShopApplysController extends BaseController {

    private String PREFIX = "/huamao/sktShopApplys/";
    @Autowired
    private ISktShopApplysService sktShopApplysService;

    /**
     * 跳转到商城商家开店申请首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopApplys.html";
    }

    /**
     * 跳转到添加商城商家开店申请
     */
    @RequestMapping("/sktShopApplys_add")
    public String sktShopApplysAdd() {
        return PREFIX + "sktShopApplys_add.html";
    }

    /**
     * 跳转到修改商城商家开店申请
     */
    @RequestMapping("/sktShopApplys_update/{sktShopApplysId}")
    public String sktShopApplysUpdate(@PathVariable Integer sktShopApplysId, Model model) {
        SktShopApplys sktShopApplys = sktShopApplysService.selectById(sktShopApplysId);
        model.addAttribute("item",sktShopApplys);
        LogObjectHolder.me().set(sktShopApplys);
        return PREFIX + "sktShopApplys_edit.html";
    }

    /**
     * 获取商城商家开店申请列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(MyDTO myDto) {
        Page<MyDTO> page = new PageFactory<MyDTO>().defaultPage();
        List<MyDTO> myDTOS = sktShopApplysService.shopApplysfindall(page, myDto);
        page.setRecords(myDTOS);
        return super.packForBT(page);
    }
    /*
     * 搜索
     * */

    /**
     * 新增商城商家开店申请
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopApplys sktShopApplys) {
        sktShopApplysService.insert(sktShopApplys);
        return SUCCESS_TIP;
    }

    /**
     * 删除商城商家开店申请
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopApplysId) {
        sktShopApplysService.deleteById(sktShopApplysId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商城商家开店申请
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object shopApplyupdate(MyDTO myDto) {
        ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
        Integer  shiroUserId=shiroUser.getId();
    	sktShopApplysService.shopApplyupdate(myDto, shiroUserId);
        return SUCCESS_TIP;
    }

    /**
     * 商城商家开店申请详情
     */
    @RequestMapping(value = "/detail/{sktShopApplysId}")
    public String detail(@PathVariable("sktShopApplysId") Integer sktShopApplysId,MyDTO myDto,Model model) {
    	myDto.setApplyId(sktShopApplysId);
    	List<Map<String,Object>> list = sktShopApplysService.shopApplysfindup(myDto);
    	 model.addAttribute("item",list.get(0));
    	 return PREFIX +"sktShopApplys_edit.html";
    }
    
    /*
     *处理
     * */
    @RequestMapping(value = "/detail2/{sktShopApplysId}")
    public String detail1(@PathVariable("sktShopApplysId") Integer sktShopApplysId,MyDTO myDto,Model model) {
    	myDto.setApplyId(sktShopApplysId);
        List<Map<String,Object>> list = sktShopApplysService.shopApplysfindup(myDto);
        myDto.setCheckTime(new DateTime());
        model.addAttribute("item",list.get(0));
    	 return PREFIX +"sktShopApplys_edit1.html";
    }
}
