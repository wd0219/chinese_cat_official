package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgent;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgentList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreAgentService;

import java.util.List;

/**
 * 代理公司积分记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-16 19:26:30
 */
@Controller
@RequestMapping("/logScoreAgent")
public class LogScoreAgentController extends BaseController {

	private String PREFIX = "/huamao/logScoreAgent/";

	@Autowired
	private ILogScoreAgentService logScoreAgentService;

	/**
	 * 跳转到代理公司积分记录首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "logScoreAgent.html";
	}

	/**
	 * 跳转到添加代理公司积分记录
	 */
	@RequestMapping("/logScoreAgent_add")
	public String logScoreAgentAdd() {
		return PREFIX + "logScoreAgent_add.html";
	}

	/**
	 * 跳转到修改代理公司积分记录
	 */
	@RequestMapping("/logScoreAgent_update/{logScoreAgentId}")
	public String logScoreAgentUpdate(@PathVariable Integer logScoreAgentId, Model model) {
		LogScoreAgent logScoreAgent = logScoreAgentService.selectById(logScoreAgentId);
		model.addAttribute("item", logScoreAgent);
		LogObjectHolder.me().set(logScoreAgent);
		return PREFIX + "logScoreAgent_edit.html";
	}

	/**
	 * 获取代理公司积分记录列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(LogScoreAgentList logScoreAgentList) {
		Page<LogScoreAgentList> page = new PageFactory<LogScoreAgentList>().defaultPage();
		List<LogScoreAgentList> logScoreAgentLists = logScoreAgentService.selectLogScoreAgentAll(page,logScoreAgentList);
		page.setRecords(logScoreAgentLists);
		return super.packForBT(page);
		// return logScoreAgentService.selectList(null);
	}

	/**
	 * 新增代理公司积分记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogScoreAgent logScoreAgent) {
		logScoreAgentService.insert(logScoreAgent);
		return SUCCESS_TIP;
	}

	/**
	 * 删除代理公司积分记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logScoreAgentId) {
		logScoreAgentService.deleteById(logScoreAgentId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改代理公司积分记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogScoreAgent logScoreAgent) {
		logScoreAgentService.updateById(logScoreAgent);
		return SUCCESS_TIP;
	}

	/**
	 * 代理公司积分记录详情
	 */
	@RequestMapping(value = "/detail/{logScoreAgentId}")
	@ResponseBody
	public Object detail(@PathVariable("logScoreAgentId") Integer logScoreAgentId) {
		return logScoreAgentService.selectById(logScoreAgentId);
	}
}
