package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商家购买库存积分订单表
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
@TableName("skt_orders_stockscore")
public class SktOrdersStockscore extends Model<SktOrdersStockscore> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "orderId", type = IdType.AUTO)
    private Integer orderId;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户Id
     */
    private Integer userId;
    /**
     * 订单状态 1 下单 2平台取消 3已完成
     */
    private Integer orderStatus;
    /**
     * 支付方式 支付方式 1:线下现金支付 2 现金账户支付 3开元宝支付 4第三方支付 5混合支付 6开元宝营业额
     */
    private Integer payType;
    /**
     * 订单总金额 (realMoney+cash+kaiyuan)
     */
    private BigDecimal totalMoney;
    /**
     * 第三方支付的金额
     */
    private BigDecimal realMoney;
    /**
     * 平台现金账户金额
     */
    private BigDecimal cash;
    /**
     * 开元宝金额 1个开元宝=1分钱
     */
    private BigDecimal kaiyuan;
    /**
     * 使用开元宝支付的手续费和税率
     */
    private BigDecimal kaiyuanFee;
    /**
     * 购买的积分数量
     */
    private BigDecimal score;
    /**
     * 订单备注
     */
    private String orderRemarks;
    /**
     * 下单时间
     */
    private Date createTime;
    /**
     * 审核时间
     */
    private Date checkTime;
    /**
     * 有效标志 1有效 0删除
     */
    private Integer dataFlag;
    
    private String userPhone;

    public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getKaiyuanFee() {
        return kaiyuanFee;
    }

    public void setKaiyuanFee(BigDecimal kaiyuanFee) {
        this.kaiyuanFee = kaiyuanFee;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public String getOrderRemarks() {
        return orderRemarks;
    }

    public void setOrderRemarks(String orderRemarks) {
        this.orderRemarks = orderRemarks;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

	@Override
	public String toString() {
		return "SktOrdersStockscore [orderId=" + orderId + ", orderNo=" + orderNo + ", userId=" + userId
				+ ", orderStatus=" + orderStatus + ", payType=" + payType + ", totalMoney=" + totalMoney
				+ ", realMoney=" + realMoney + ", cash=" + cash + ", kaiyuan=" + kaiyuan + ", kaiyuanFee=" + kaiyuanFee
				+ ", score=" + score + ", orderRemarks=" + orderRemarks + ", createTime=" + createTime + ", checkTime="
				+ checkTime + ", dataFlag=" + dataFlag + ", userPhone=" + userPhone + "]";
	}

	public SktOrdersStockscore(Integer orderId, String orderNo, Integer userId, Integer orderStatus, Integer payType,
			BigDecimal totalMoney, BigDecimal realMoney, BigDecimal cash, BigDecimal kaiyuan, BigDecimal kaiyuanFee,
			BigDecimal score, String orderRemarks, Date createTime, Date checkTime, Integer dataFlag,
			String userPhone) {
		super();
		this.orderId = orderId;
		this.orderNo = orderNo;
		this.userId = userId;
		this.orderStatus = orderStatus;
		this.payType = payType;
		this.totalMoney = totalMoney;
		this.realMoney = realMoney;
		this.cash = cash;
		this.kaiyuan = kaiyuan;
		this.kaiyuanFee = kaiyuanFee;
		this.score = score;
		this.orderRemarks = orderRemarks;
		this.createTime = createTime;
		this.checkTime = checkTime;
		this.dataFlag = dataFlag;
		this.userPhone = userPhone;
	}

	public SktOrdersStockscore() {
		super();
		// TODO Auto-generated constructor stub
	}
    
}
