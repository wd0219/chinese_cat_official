package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 前台菜单表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-28
 */
@TableName("skt_home_menus")
public class HomeMenus extends Model<HomeMenus> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "menuId", type = IdType.AUTO)
    private Integer menuId;
    /**
     * 父ID
     */
    private Integer parentId;
    /**
     * ico类的名称
     */
    private String iconClass;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 菜单Url
     */
    private String menuUrl;
    /**
     * 关联Url
     */
    private String menuOtherUrl;
    /**
     * 菜单类型
     */
    private Integer menuType;
    /**
     * 是否显示
     */
    private Integer isShow;
    /**
     * 菜单排序
     */
    private Integer menuSort;
    /**
     * 有效状态
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getIconClass() {
        return iconClass;
    }

    public void setIconClass(String iconClass) {
        this.iconClass = iconClass;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuOtherUrl() {
        return menuOtherUrl;
    }

    public void setMenuOtherUrl(String menuOtherUrl) {
        this.menuOtherUrl = menuOtherUrl;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getMenuSort() {
        return menuSort;
    }

    public void setMenuSort(Integer menuSort) {
        this.menuSort = menuSort;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.menuId;
    }

    @Override
    public String toString() {
        return "HomeMenus{" +
        "menuId=" + menuId +
        ", parentId=" + parentId +
        ", iconClass=" + iconClass +
        ", menuName=" + menuName +
        ", menuUrl=" + menuUrl +
        ", menuOtherUrl=" + menuOtherUrl +
        ", menuType=" + menuType +
        ", isShow=" + isShow +
        ", menuSort=" + menuSort +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
