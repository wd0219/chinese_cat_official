package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktAdPositions;
import com.stylefeng.guns.modular.huamao.dao.SktAdPositionsMapper;
import com.stylefeng.guns.modular.huamao.service.ISktAdPositionsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 广告位置表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@Service
public class SktAdPositionsServiceImpl extends ServiceImpl<SktAdPositionsMapper, SktAdPositions> implements ISktAdPositionsService {
	@Autowired
	private SktAdPositionsMapper sktAdPositionsMapper;
	@Override
	public List<SktAdPositions> sktAdPositionsfinAll(Page<SktAdPositions> page, SktAdPositions sktAdPositions) {
		Integer total = sktAdPositionsMapper.sktAdPositionsfinAllCount(sktAdPositions);
		page.setTotal(total);
		return sktAdPositionsMapper.sktAdPositionsfinAll(page,sktAdPositions);
	}

}
