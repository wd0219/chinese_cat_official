package com.stylefeng.guns.modular.app.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Users;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author wangang
 * @since 2018-05-6
 */
public interface IAppUserService extends IService<Users> {

	//根据登录名查询会员集合
	public List<Users> findListByLoginName(@Param("loginName")String LoginName);
	//查询分享人
	Users findUserByLoginNameOrUserPhone(String shareMan);
	//添加用户
	boolean addUserRegister(Users uses);

	Users findUsersByUserPhoneAndOldpayPwd(Users users);
}
