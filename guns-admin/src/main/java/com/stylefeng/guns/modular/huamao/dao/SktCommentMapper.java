package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktComment;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktCommentDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 评论表(预留表) Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface SktCommentMapper extends BaseMapper<SktComment> {
    public List<SktCommentDTO> selectSktCommentAll(@Param("page") Page<SktCommentDTO> page,@Param("sktCommentDTO") SktCommentDTO sktCommentDTO);
    public Integer selectSktCommentAllCount(@Param("sktCommentDTO") SktCommentDTO sktCommentDTO);

}
