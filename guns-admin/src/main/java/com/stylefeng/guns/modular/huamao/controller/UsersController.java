package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersDTO;
import com.stylefeng.guns.modular.huamao.model.UsersLoginDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersService;

/**
 * 用户管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 19:42:53
 */
@Controller
@RequestMapping("/users")
public class UsersController extends BaseController {

	private String PREFIX = "/huamao/users/";

	@Autowired
	private IUsersService usersService;

	/**
	 * 跳转到用户管理首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "users.html";
	}

	/**
	 * 跳转到添加用户管理
	 */
	@RequestMapping("/users_add")
	public String usersAdd() {
		return PREFIX + "users_add.html";
	}

	/**
	 * 跳转到修改用户管理
	 */
	@RequestMapping("/users_update/{usersId}")
	public String usersUpdate(@PathVariable Integer usersId, Model model) {
		if (ToolUtil.isEmpty(usersId)) {
            throw new GunsException(BizExceptionEnum.REQUEST_NULL);
        }
		UsersDTO usersDTO = new UsersDTO();
		usersDTO.setUserId(usersId);
		usersDTO = usersService.selectOneUsersInfo(usersDTO);
		String allUrl = UrlUtils.getAllUrl(usersDTO.getUserPhoto());
		usersDTO.setUserPhoto(allUrl);
		model.addAttribute("item", usersDTO);
		LogObjectHolder.me().set(usersDTO);
		return PREFIX + "users_edit.html";
	}

	/**
	 * 获取用户管理列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(UsersDTO usersDTO) {
		List<UsersDTO> list = usersService.selectUsersInfo2(usersDTO);
//		Page<UsersDTO> page = new PageFactory<UsersDTO>().defaultPage();
//		List<UsersDTO> list = usersService.selectUsersInfo(page,usersDTO);
//		page.setRecords(list);
//      return super.packForBT(page);
		return list;
	}
	/**
	 * 推荐列表
	 */
	@RequestMapping(value = "/tuiList/{inviteId}")
	@ResponseBody
	public Object tuiList(@PathVariable Integer inviteId) {
		List<UsersDTO> list = usersService.selectTuiUsersInfo(inviteId);
		return list;
	}
	/**
	 * 获取推荐人id，name，phone
	 * @param tuiPhone
	 * @return
	 */
	@RequestMapping(value = "selectTuiUsersName/{tuiPhone}")
	@ResponseBody
	public Object selectTuiUsersInfo(@PathVariable String tuiPhone) {
		Users users = new Users();
		users.setUserPhone(tuiPhone);
		users = usersService.selectTuiUsersName(users);
		return users;
	}
	/**
	 * 新增用户管理
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(Users users) {
		usersService.insert(users);
		return SUCCESS_TIP;
	}

	/**
	 * 删除用户管理
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(UsersDTO usersDTO) {
		usersService.deleteById(usersDTO.getInviteId());
		return SUCCESS_TIP;
	}

	/**
	 * 修改用户管理
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(UsersDTO usersDTO) {
		String result = usersService.updateUsersInfo(usersDTO);
		if(SUCCESS.equals(result)){
			return "SUCCESS";
		}else{
			return result;
		}
	}
	
	/**
	 * 用户管理详情
	 */
	@RequestMapping(value = "/detail/{usersId}")
	@ResponseBody
	public Object detail(@PathVariable("usersId") Integer usersId) {
		return usersService.selectById(usersId);
	}
	
	/**
     * 根据用户id 姓名 电话查询身份
     */
	@RequestMapping(value = "/selectUsers")
	@ResponseBody
    public Object selectUsers(Users users){
		Map<String,Object> user = usersService.selectUsersPhone(users);
		return user;
    }
	
	/*******************登录**************************/
	
	/**
	 * 用户登录信息后
	 * @param users
	 * @return
	 */
	@RequestMapping(value = "/userLogin")
	@ResponseBody
    public Object userLogin(Users users){
		UsersLoginDTO user = usersService.userLogin(users);
		return user;
    }
	/**
	 * 核实用户信息
	 * @param users
	 * @return
	 */
	@RequestMapping(value = "/checkUserInfo")
	@ResponseBody
    public Object userLogin(@RequestParam(value="phoneOrLoginName") String userPhoneOrloginName){
		Users user = usersService.checkUserPhoneOrloginName(userPhoneOrloginName);
		return user;
    }
	/**
     * 查询用户上10级
     */
    @RequestMapping("/userUpLevel")
    @ResponseBody
    public Object userUpLevel(@RequestParam(value="usersId") Integer id){
        Map<String,Object> map = usersService.selectUsersUpLevel(id);
        return map;
    }
    
    /**
     * 根据Id查询注册信息
     */
    @RequestMapping("/selectByUsersInfo")
    @ResponseBody
    public Object getUserById(@RequestParam(value="usersId") Integer id){
        Map<String,Object> usersMap= usersService.selectByUsersInfo(id);
        return usersMap;
    }
    /**
     * 修改用户密码
     */
    @RequestMapping("/updateUsersPwd")
    @ResponseBody
    public Object updateUsersPwd(UsersDTO users){
         int i = usersService.updateUsersPwd(users);
        return i;
    }
    /**
	 * 用户注册
	 */
	@RequestMapping(value = "/addUser")
	@ResponseBody
	public Object addUser(Users users) {
		String result = usersService.insertUsers(users);
		if("REPEAT".equals(result)){
			return "REPEAT";
		}
		return SUCCESS_TIP;
	}
	
	
}
