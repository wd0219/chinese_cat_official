package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcards;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcardsList;
import com.stylefeng.guns.modular.huamao.service.IAgentsBankcardsService;

import java.util.List;

/**
 * 代理公司银行卡控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 10:25:08
 */
@Controller
@RequestMapping("/agentsBankcards")
public class AgentsBankcardsController extends BaseController {

	private String PREFIX = "/huamao/agentsBankcards/";

	@Autowired
	private IAgentsBankcardsService agentsBankcardsService;

	/**
	 * 跳转到代理公司银行卡首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "agentsBankcards.html";
	}

	/**
	 * 跳转到添加代理公司银行卡
	 */
	@RequestMapping("/agentsBankcards_add")
	public String agentsBankcardsAdd() {
		return PREFIX + "agentsBankcards_add.html";
	}

	/**
	 * 跳转到修改代理公司银行卡
	 */
	@RequestMapping("/agentsBankcards_update/{agentsBankcardsId}")
	public String agentsBankcardsUpdate(@PathVariable Integer agentsBankcardsId, Model model) {
		AgentsBankcards agentsBankcards = agentsBankcardsService.selectById(agentsBankcardsId);
		model.addAttribute("item", agentsBankcards);
		LogObjectHolder.me().set(agentsBankcards);
		return PREFIX + "agentsBankcards_edit.html";
	}

	/**
	 * 获取代理公司银行卡列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(AgentsBankcardsList agentsBankcardsList) {
		Page<AgentsBankcardsList> page = new PageFactory<AgentsBankcardsList>().defaultPage();
		List<AgentsBankcardsList> agentsBankcardsLists = agentsBankcardsService.selectAgentsBankcardsAll(page,agentsBankcardsList);
		page.setRecords(agentsBankcardsLists);
		return super.packForBT(page);
		// return agentsBankcardsService.selectList(null);
	}

	/**
	 * 新增代理公司银行卡
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(AgentsBankcards agentsBankcards) {
		agentsBankcardsService.insert(agentsBankcards);
		return SUCCESS_TIP;
	}

	/**
	 * 删除代理公司银行卡
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer agentsBankcardsId) {
		agentsBankcardsService.deleteById(agentsBankcardsId);
		return SUCCESS_TIP;
	}

	/**
	 * 通过ID删除代理公司银行卡
	 */
	@RequestMapping(value = "/deleteById")
	@ResponseBody
	public Object deleteById(@RequestParam Integer bankCardId) {
		// 通过传过来的ID进行逻辑删除
		agentsBankcardsService.deleteById2(bankCardId);
		// usersBankcardsService.deleteById(usersBankcardsId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改代理公司银行卡
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(AgentsBankcards agentsBankcards) {
		agentsBankcardsService.updateById(agentsBankcards);
		return SUCCESS_TIP;
	}

	/**
	 * 代理公司银行卡详情
	 */
	@RequestMapping(value = "/detail/{agentsBankcardsId}")
	@ResponseBody
	public Object detail(@PathVariable("agentsBankcardsId") Integer agentsBankcardsId) {
		return agentsBankcardsService.selectById(agentsBankcardsId);
	}
}
