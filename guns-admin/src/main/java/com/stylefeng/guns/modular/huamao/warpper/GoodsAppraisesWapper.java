package com.stylefeng.guns.modular.huamao.warpper;

import java.util.List;
import java.util.Map;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;

public class GoodsAppraisesWapper extends BaseControllerWarpper {
	public GoodsAppraisesWapper(List<Map<String, Object>> list){
		super(list);
	}
	 public void warpTheMap(Map<String, Object> map) {
		 map.put("goodsScoreStar", ConstantFactory.me().getStar((Integer)map.get("goodsScore")));
		 map.put("serviceScoreStar", ConstantFactory.me().getStar((Integer)map.get("serviceScore")));
		 map.put("timeScoreStar", ConstantFactory.me().getStar((Integer)map.get("timeScore")));
		 map.put("isShowName", ConstantFactory.me().getIsShowName((Integer)map.get("isShow")));		 
	 }
}
