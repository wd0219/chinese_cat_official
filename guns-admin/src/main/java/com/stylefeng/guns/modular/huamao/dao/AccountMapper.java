package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Account;
import com.stylefeng.guns.modular.huamao.model.AccountList;
import com.stylefeng.guns.modular.huamao.model.SktDate;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 会员账户表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-11
 */
public interface AccountMapper extends BaseMapper<Account> {
	// 展示用户账户列表
	public List<AccountList> selectAccountAll(@Param("page")Page<AccountList> page, @Param("accountList") AccountList accountList);
	/**
	 * 审核通过退款底单时，账户余额发生的改变
	 * @param account
	 * @return
	 */
	public Object updateOrderRefunds(AccountList AccountList);
	
	/**
	 * 根据用户id查询账户
	 */
	Account selectAccountBuyuserId(Integer userId);

    Account findScoreById(Integer userId);

	Integer updateByUserId(Account account);

	Integer updateByUserId2(Account account);

	Integer updateByUserId3(Account account);

    List<SktDate> getSevenWorkDay(Integer date);

	Integer updateFreezeCash(Account account);

	Integer updateCash(Account account);

	Integer updateScore(Account account);
	/**
	 * 根据用户id查询账户
	 */
	public Account selectAccountByuserId(Integer userId);

    Integer selectAccountAllCount(@Param("accountList") AccountList accountList);
}
