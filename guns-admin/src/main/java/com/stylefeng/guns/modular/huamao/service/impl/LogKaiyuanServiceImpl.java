package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanService;

/**
 * <p>
 * 开元宝流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogKaiyuanServiceImpl extends ServiceImpl<LogKaiyuanMapper, LogKaiyuan> implements ILogKaiyuanService {

	@Autowired
	private LogKaiyuanMapper lkm;

	// 开元宝流水返回
	@Override
	public List<LogKaiyuanList> selectLogKaiyuanAll(Page<LogKaiyuanList> page , LogKaiyuanList logKaiyuanList) {
		Integer total = lkm.selectLogKaiyuanAllCount(logKaiyuanList);
		page.setTotal(total);
		return lkm.selectLogKaiyuanAll(page,logKaiyuanList);
	}

}
