package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktAdPositions;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 广告位置表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface ISktAdPositionsService extends IService<SktAdPositions> {
	
	public List<SktAdPositions> sktAdPositionsfinAll(Page<SktAdPositions> page, SktAdPositions sktAdPositions);
}
