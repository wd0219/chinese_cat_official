package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktLogUsersLogins;
import com.stylefeng.guns.modular.huamao.service.ISktLogUsersLoginsService;

import java.util.List;

/**
 * 会员登录日志控制器
 *
 * @author fengshuonan
 * @Date 2018-04-27 11:46:00
 */
@Controller
@RequestMapping("/sktLogUsersLogins")
public class SktLogUsersLoginsController extends BaseController {

    private String PREFIX = "/huamao/sktLogUsersLogins/";

    @Autowired
    private ISktLogUsersLoginsService sktLogUsersLoginsService;

    /**
     * 跳转到会员登录日志首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktLogUsersLogins.html";
    }

    /**
     * 跳转到添加会员登录日志
     */
    @RequestMapping("/sktLogUsersLogins_add")
    public String sktLogUsersLoginsAdd() {
        return PREFIX + "sktLogUsersLogins_add.html";
    }

    /**
     * 跳转到修改会员登录日志
     */
    @RequestMapping("/sktLogUsersLogins_update/{sktLogUsersLoginsId}")
    public String sktLogUsersLoginsUpdate(@PathVariable Integer sktLogUsersLoginsId, Model model) {
        SktLogUsersLogins sktLogUsersLogins = sktLogUsersLoginsService.selectById(sktLogUsersLoginsId);
        model.addAttribute("item",sktLogUsersLogins);
        LogObjectHolder.me().set(sktLogUsersLogins);
        return PREFIX + "sktLogUsersLogins_edit.html";
    }

    /**
     * 获取会员登录日志列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktLogUsersLogins sktLogUsersLogins) {
        Page<SktLogUsersLogins> page= new PageFactory<SktLogUsersLogins>().defaultPage();
        List<SktLogUsersLogins> usersLogins = sktLogUsersLoginsService.sktLogUsersLoginsfindup(page, sktLogUsersLogins);
        page.setRecords(usersLogins);
        return  super.packForBT(page);
    }

    /**
     * 新增会员登录日志
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktLogUsersLogins sktLogUsersLogins) {
        sktLogUsersLoginsService.insert(sktLogUsersLogins);
        return SUCCESS_TIP;
    }

    /**
     * 删除会员登录日志
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktLogUsersLoginsId) {
        sktLogUsersLoginsService.deleteById(sktLogUsersLoginsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改会员登录日志
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktLogUsersLogins sktLogUsersLogins) {
        sktLogUsersLoginsService.updateById(sktLogUsersLogins);
        return SUCCESS_TIP;
    }

    /**
     * 会员登录日志详情
     */
    @RequestMapping(value = "/detail/{sktLogUsersLoginsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktLogUsersLoginsId") Integer sktLogUsersLoginsId) {
        return sktLogUsersLoginsService.selectById(sktLogUsersLoginsId);
    }
}
