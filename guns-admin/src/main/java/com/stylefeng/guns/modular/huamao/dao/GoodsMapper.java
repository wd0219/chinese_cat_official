package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsMapper extends BaseMapper<Goods> {
	List<Map<String,Object>> selectByConditions(@Param("page")Page<Map<String, Object>> page, 
			@Param("goodsCustm")GoodsCustm goodsCustm);
	Integer selectByConditionsCount(@Param("goodsCustm")GoodsCustm goodsCustm);
	List<Goods> selectGoodsByGoodsCatId (int id);
    int batchUpdate(List<Map<String,Object>> list);
    int batchUpdatePo(List<Goods> list);
	public int selectisSaleCount();
	public int selectGoodsStatusCount();
	public int selectisSaleTotal();
	public int selectGoodsStatusTotal();
	/**
	 * 今日热销
	 * @return
	 */
	List<Map<String, Object>> selectTodayHotSale();
	/**
	 * 查询品牌所有商品
	 * @param list
	 * @return
	 */
	List<Map<String,Object>> selectTodayHotSaleAll(@Param(value="brandId") Integer brandId);

	List<Map<String,Object>> selectNotRecomGoods(GoodsCustm goodsCustm);
	List<Map<String,Object>> selectRecomGoods(GoodsCustm goodsCustm);

    List<Goods> adsSelectGoodsName(String soName);
	public int updateSktGoodsByMap(Map<String, Object> map);
}
