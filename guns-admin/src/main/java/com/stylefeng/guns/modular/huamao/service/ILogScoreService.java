package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.LogScoreList;

/**
 * <p>
 * 积分流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface ILogScoreService extends IService<LogScore> {

	// 展示积分列表
	public List<LogScoreList> selectLogScoreAll(Page<LogScoreList> page, LogScoreList logScoreList);

}
