package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktSalesMonth;

/**
 * <p>
 * 商家每月营业额统计表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
public interface SktSalesMonthMapper extends BaseMapper<SktSalesMonth> {
	
	/*
	 * 展示全部
	 * */
	public List<SktSalesMonth> sktSalesMonthfindAll(@Param("page")Page<SktSalesMonth> page, 
			@Param("sktSalesMonth")SktSalesMonth sktSalesMonth);

	public Integer sktSalesMonthfindAllCount(@Param("sktSalesMonth")SktSalesMonth sktSalesMonth);
}
