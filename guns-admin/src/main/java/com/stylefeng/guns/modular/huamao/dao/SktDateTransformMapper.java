package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktDateTransform;

/**
 * <p>
 * 积分转换开元宝每日记录表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
public interface SktDateTransformMapper extends BaseMapper<SktDateTransform> {
	
	public List<SktDateTransform> sktDateTransformfindAll(@Param("page")Page<SktDateTransform> page, 
			@Param("sktDateTransform")SktDateTransform sktDateTransform);
	public Integer sktDateTransformfindAllCount(@Param("sktDateTransform")SktDateTransform sktDateTransform);
	
//	public List<SktDateTransform> SktDateTransformfindAll2(@Param("page")Page<SktDateTransform> page,
//			@Param("sktDateTransform2") SktDateTransform sktDateTransform2);
//	public Integer SktDateTransformfindAll2Count(@Param("sktDateTransform2") SktDateTransform sktDateTransform2);

}
