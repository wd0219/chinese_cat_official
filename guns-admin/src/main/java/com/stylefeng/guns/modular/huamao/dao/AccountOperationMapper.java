package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.AccountOperation;
import com.stylefeng.guns.modular.huamao.model.AccountOperationList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 修改账户数据记录 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-13
 */
public interface AccountOperationMapper extends BaseMapper<AccountOperation> {

	// 展示手动补单记录列表
	public List<AccountOperationList> selectAccountOperationAll(@Param("page")Page<AccountOperationList> page, @Param("accountOperationList") AccountOperationList accountOperationList);

	Integer selectAccountOperationAllCount(@Param("accountOperationList") AccountOperationList accountOperationList);
}
