package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.dao.ArticlesMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsMapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopsMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AdsMapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 广告表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
@Service
public class AdsServiceImpl extends ServiceImpl<AdsMapper, Ads> implements IAdsService {

	@Autowired
	private AdsMapper am;
	@Autowired
	private SktShopsMapper sktShopsMapper;
	@Autowired
	private GoodsMapper goodsMapper;

	@Autowired
	private ArticlesMapper articlesMapper;
	@Autowired
	private ISktAdPositionsService sktAdPositionsService;
	// 广告表展示
	@Override
	public List<AdsList> selectAdsAll(Page<AdsList> page, AdsList adsList) {
		Integer total = am.selectAdsAllCount(adsList);
		page.setTotal(total);
		return am.selectAdsAll(page,adsList);
	}

	// 异步获取广告位置
	@Override
	public List<AdsList> getPosition(String positionType) {
		return am.getPosition(positionType);
	}

	// 添加广告
	@Override
	public Integer addAds(AdsList adsList) {
		return am.addAds(adsList);
	}

	// 通过id查询广告为修改作准备
	@Override
	public AdsList selectAdById(Integer adId) {
		return am.selectAdById(adId);
	}

	// 通过id修改广告
	@Override
	@Transactional
	public void updateAdById(AdsList adsList) {
		am.updateAdById(adsList);
		boolean flag=false;
		if (flag==true){
			Integer adPositionId = adsList.getAdPositionId();
			SktAdPositions sktAdPositions = sktAdPositionsService.selectById(adPositionId);
			if (adsList.getDataFlag()==1){
				if ("app-2-1".equals(sktAdPositions.getPositionCode()) || "pcPopUp".equals(sktAdPositions.getPositionCode())){
					am.updatePopUp(adsList);
				}
			}

		}

	}

	@Override
	public Map<String, Object> getSo(String searchType, String soName) {
		Map<String,Object> map = new HashMap<String,Object>();
		if(searchType == "" || searchType == null){
			map.put("code","00");
			map.put("msg","搜索类型不能为空");
			return map;
		}
		//1店铺 2商品 3公告
		Integer type = Integer.valueOf(searchType);
		if(type.equals(1)){
			//查询店铺
			List<SktShops> shops = sktShopsMapper.adsSelectShopName(soName);
			if(shops != null && shops.size() > 0){
				map.put("code","01");
				map.put("type",1);
				map.put("shops",shops);
				return map;
			}else{
				map.put("code","00");
				map.put("msg","搜索商城店铺为空");
				return map;
			}
		}
		if(type.equals(2)){
			//查询商品
			List<Goods> goods = goodsMapper.adsSelectGoodsName(soName);
			if(goods != null && goods.size() > 0){
				map.put("code","01");
				map.put("type",2);
				map.put("goods",goods);
				return map;
			}else{
				map.put("code","00");
				map.put("msg","搜索商城商品为空");
				return map;
			}
		}
		if(type.equals(3)){
			//查询商城公告
			List<Articles> articles = articlesMapper.adsSelectArticleTitle(soName);
			if(articles != null && articles.size() > 0){
				map.put("code","01");
				map.put("type",3);
				map.put("articles",articles);
				return map;
			}else{
				map.put("code","00");
				map.put("msg","搜索商城公告为空");
				return map;
			}
		}
		return null;
	}

}
