package com.stylefeng.guns.modular.huamao.model;

public class SktOrdersStoresDto extends  SktOrdersStores{
	/*----------------------*/
	    private String loginName;
	    /**
	     * 用户电话
	     */
	    private String userPhone;
	    /**
	     * 商店名称
	     */
	    private String shopName;
	    /**
	     * 支付金额
	     */
	    private String payMoney;
	    /**
	     * 开始时间
	     */
	    private String beginTime;
	    /**
	     * 结束时间
	     */
	    private String endTime;
	    /**
		 * 省id
		 */
		private String province;
		/**
		 * 市id
		 */
		private String citys;
		/**
		 * 县id
		 */
		private String county;
		public String getLoginName() {
			return loginName;
		}
		public void setLoginName(String loginName) {
			this.loginName = loginName;
		}
		public String getUserPhone() {
			return userPhone;
		}
		public void setUserPhone(String userPhone) {
			this.userPhone = userPhone;
		}
		public String getShopName() {
			return shopName;
		}
		public void setShopName(String shopName) {
			this.shopName = shopName;
		}
		public String getPayMoney() {
			return payMoney;
		}
		public void setPayMoney(String payMoney) {
			this.payMoney = payMoney;
		}
		public String getBeginTime() {
			return beginTime;
		}
		public void setBeginTime(String beginTime) {
			this.beginTime = beginTime;
		}
		public String getEndTime() {
			return endTime;
		}
		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}
		public String getProvince() {
			return province;
		}
		public void setProvince(String province) {
			this.province = province;
		}
		public String getCitys() {
			return citys;
		}
		public void setCitys(String citys) {
			this.citys = citys;
		}
		public String getCounty() {
			return county;
		}
		public void setCounty(String county) {
			this.county = county;
		}
		
	    
}
