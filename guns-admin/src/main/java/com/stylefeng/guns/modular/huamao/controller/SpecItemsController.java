package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.SpecItems;
import com.stylefeng.guns.modular.huamao.service.ISpecItemsService;

/**
 * 商品规格值控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:44:41
 */
@Controller
@RequestMapping("/specItems")
public class SpecItemsController extends BaseController {

    private String PREFIX = "/huamao/specItems/";

    @Autowired
    private ISpecItemsService specItemsService;

    /**
     * 跳转到商品规格值首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "specItems.html";
    }

    /**
     * 跳转到添加商品规格值
     */
    @RequestMapping("/specItems_add")
    public String specItemsAdd() {
        return PREFIX + "specItems_add.html";
    }

    /**
     * 跳转到修改商品规格值
     */
    @RequestMapping("/specItems_update/{specItemsId}")
    public String specItemsUpdate(@PathVariable Integer specItemsId, Model model) {
        SpecItems specItems = specItemsService.selectById(specItemsId);
        model.addAttribute("item",specItems);
        LogObjectHolder.me().set(specItems);
        return PREFIX + "specItems_edit.html";
    }

    /**
     * 获取商品规格值列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return specItemsService.selectList(null);
    }

    /**
     * 新增商品规格值
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SpecItems specItems) {
        specItemsService.insert(specItems);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品规格值
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer specItemsId) {
        specItemsService.deleteById(specItemsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品规格值
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SpecItems specItems) {
        specItemsService.updateById(specItems);
        return SUCCESS_TIP;
    }

    /**
     * 商品规格值详情
     */
    @RequestMapping(value = "/detail/{specItemsId}")
    @ResponseBody
    public Object detail(@PathVariable("specItemsId") Integer specItemsId) {
        return specItemsService.selectById(specItemsId);
    }
}
