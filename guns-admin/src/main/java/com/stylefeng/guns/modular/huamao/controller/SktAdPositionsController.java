package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktAdPositions;
import com.stylefeng.guns.modular.huamao.service.ISktAdPositionsService;

import java.util.List;

/**
 * 广告位置控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 19:46:59
 */
@Controller
@RequestMapping("/sktAdPositions")
public class SktAdPositionsController extends BaseController {

    private String PREFIX = "/huamao/sktAdPositions/";

    @Autowired
    private ISktAdPositionsService sktAdPositionsService;

    /**
     * 跳转到广告位置首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktAdPositions.html";
    }

    /**
     * 跳转到添加广告位置
     */
    @RequestMapping("/sktAdPositions_add")
    public String sktAdPositionsAdd() {
        return PREFIX + "sktAdPositions_add.html";
    }

    /**
     * 跳转到修改广告位置
     */
    @RequestMapping("/sktAdPositions_update/{sktAdPositionsId}")
    public String sktAdPositionsUpdate(@PathVariable Integer sktAdPositionsId, Model model) {
        SktAdPositions sktAdPositions = sktAdPositionsService.selectById(sktAdPositionsId);
        model.addAttribute("item",sktAdPositions);
        LogObjectHolder.me().set(sktAdPositions);
        return PREFIX + "sktAdPositions_edit.html";
    }

    /**
     * 获取广告位置列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktAdPositions sktAdPositions) {
        Page<SktAdPositions> page = new PageFactory<SktAdPositions>().defaultPage();
        List<SktAdPositions> adPositions = sktAdPositionsService.sktAdPositionsfinAll(page,sktAdPositions);
        page.setRecords(adPositions);
        return super.packForBT(page);
    }

    /**
     * 新增广告位置
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktAdPositions sktAdPositions) {
        String positionCode = sktAdPositions.getPositionCode();
        SktAdPositions adPositions=new SktAdPositions();
        adPositions.setPositionCode(positionCode);
        EntityWrapper<SktAdPositions> ew = new EntityWrapper<SktAdPositions>(adPositions);
        List<SktAdPositions> list = sktAdPositionsService.selectList(ew);
        if (list!=null && list.size()>0){
            return "广告位置代码"+positionCode+"已存在,不要重复录入";
        }
        sktAdPositionsService.insert(sktAdPositions);
        return "添加成功!";
    }

    /**
     * 删除广告位置
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktAdPositionsId) {
        sktAdPositionsService.deleteById(sktAdPositionsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改广告位置
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktAdPositions sktAdPositions) {
        sktAdPositionsService.updateById(sktAdPositions);
        return SUCCESS_TIP;
    }

    /**
     * 广告位置详情
     */
    @RequestMapping(value = "/detail/{sktAdPositionsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktAdPositionsId") Integer sktAdPositionsId) {
        return sktAdPositionsService.selectById(sktAdPositionsId);
    }
}
