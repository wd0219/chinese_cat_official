package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AgentsDrawsMapper;
import com.stylefeng.guns.modular.huamao.model.AgentsDraws;
import com.stylefeng.guns.modular.huamao.model.AgentsDrawsList;
import com.stylefeng.guns.modular.huamao.service.IAgentsDrawsService;

/**
 * <p>
 * 代理公司提现记录表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
@Service
public class AgentsDrawsServiceImpl extends ServiceImpl<AgentsDrawsMapper, AgentsDraws> implements IAgentsDrawsService {

	@Autowired
	private AgentsDrawsMapper adm;

	// 代理公司提现记录表展示
	@Override
	public List<AgentsDrawsList> selectAgentsDrawsAll(Page<AgentsDrawsList> page, AgentsDrawsList agentsDrawsList) {
		Integer total = adm.selectAgentsDrawsAllCount(agentsDrawsList);
		page.setTotal(total);
		return adm.selectAgentsDrawsAll(page,agentsDrawsList);
	}

	@Override
	public void updateSatus(AgentsDrawsList agentsDrawsList) {
		adm.updateSatus(agentsDrawsList);
	}

}
