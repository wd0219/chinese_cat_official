package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktSalesMonth;
import com.stylefeng.guns.modular.huamao.service.ISktSalesMonthService;

/**
 * 商家每月营业额控制器
 *
 * @author fengshuonan
 * @Date 2018-04-22 22:10:30
 */
@Controller
@RequestMapping("/sktSalesMonth")
public class SktSalesMonthController extends BaseController {

    private String PREFIX = "/huamao/sktSalesMonth/";

    @Autowired
    private ISktSalesMonthService sktSalesMonthService;

    /**
     * 跳转到商家每月营业额首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktSalesMonth.html";
    }

    /**
     * 跳转到添加商家每月营业额
     */
    @RequestMapping("/sktSalesMonth_add")
    public String sktSalesMonthAdd() {
        return PREFIX + "sktSalesMonth_add.html";
    }

    /**
     * 跳转到修改商家每月营业额
     */
    @RequestMapping("/sktSalesMonth_update/{sktSalesMonthId}")
    public String sktSalesMonthUpdate(@PathVariable Integer sktSalesMonthId, Model model) {
        SktSalesMonth sktSalesMonth = sktSalesMonthService.selectById(sktSalesMonthId);
        model.addAttribute("item",sktSalesMonth);
        LogObjectHolder.me().set(sktSalesMonth);
        return PREFIX + "sktSalesMonth_edit.html";
    }

    /**
     * 获取商家每月营业额列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktSalesMonth sktSalesMonth) {
    	Page<SktSalesMonth> page = new PageFactory<SktSalesMonth>().defaultPage();
        List<SktSalesMonth> list = sktSalesMonthService.sktSalesMonthfindAll(page,sktSalesMonth);
        page.setRecords(list);
		return super.packForBT(page);
    }

    /**
     * 新增商家每月营业额
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktSalesMonth sktSalesMonth) {
        sktSalesMonthService.insert(sktSalesMonth);
        return SUCCESS_TIP;
    }

    /**
     * 删除商家每月营业额
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktSalesMonthId) {
        sktSalesMonthService.deleteById(sktSalesMonthId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商家每月营业额
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktSalesMonth sktSalesMonth) {
        sktSalesMonthService.updateById(sktSalesMonth);
        return SUCCESS_TIP;
    }

    /**
     * 商家每月营业额详情
     */
    @RequestMapping(value = "/detail/{sktSalesMonthId}")
    @ResponseBody
    public Object detail(@PathVariable("sktSalesMonthId") Integer sktSalesMonthId) {
        return sktSalesMonthService.selectById(sktSalesMonthId);
    }
}
