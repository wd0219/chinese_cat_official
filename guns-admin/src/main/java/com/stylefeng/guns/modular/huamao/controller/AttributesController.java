package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.model.Attributes;
import com.stylefeng.guns.modular.huamao.model.AttributesCustem;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.service.IAttributesService;
import com.stylefeng.guns.modular.huamao.service.IGoodsCatsService;
import com.stylefeng.guns.modular.huamao.service.IGoodsService;

/**
 * 商品属性控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:23:36
 */
@Controller
@RequestMapping("/attributes")
public class AttributesController extends BaseController {

    private String PREFIX = "/huamao/attributes/";

    @Autowired
    private IAttributesService attributesService;
    @Autowired
    private IGoodsCatsService goodsCatsService;
    @Autowired
    private IGoodsService goodsServiceImpl;
    /**
     * 跳转到商品属性首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "attributes.html";
    }

    /**
     * 跳转到添加商品属性
     */
    @RequestMapping("/attributes_add")
    public String attributesAdd() {

        return PREFIX + "attributes_add.html";
    }

    /**
     * 跳转到修改商品属性
     */
    @RequestMapping("/attributes_update/{attributesId}")
    public String attributesUpdate(@PathVariable Integer attributesId, Model model) {
    	Attributes attributes = attributesService.selectById(attributesId);
		int goodsCatId1=attributes.getGoodsCatId();
		int goodsCatId2=0;
		int goodsCatId3=0;
		if(goodsCatId1!=0){//最后级分类id
			GoodsCats goodsCats1 = goodsCatsService.selectById(goodsCatId1);
			goodsCatId2=goodsCats1.getParentId();
			if(goodsCatId2!=0){//第二级分类id
				GoodsCats goodsCats2 = goodsCatsService.selectById(goodsCatId2);
				goodsCatId3=goodsCats2.getParentId();//三级分类id
			}
		}
		model.addAttribute("item",attributes);
		model.addAttribute("goodsCatId2",goodsCatId2);
		model.addAttribute("goodsCatId3",goodsCatId3);
        model.addAttribute("item",attributes);
        LogObjectHolder.me().set(attributes);
        return PREFIX + "attributes_edit.html";
    }

    /**
     * 获取商品属性列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AttributesCustem attributesCustem) {
    	Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().defaultPage();
        List<Map<String, Object>> attributes = attributesService.selectAll(page,attributesCustem);
        for(Map<String, Object> map:attributes ){
	    		String goodsCatIdPath=(String) map.get("goodsCatPath");
	    		String[]  catIdPathList=goodsCatIdPath.split("_");
	    		String goodsCatNamePath = "";
	    		for (int i = 0; i < catIdPathList.length; i++) {
	    			if(isInteger(catIdPathList[i])){
	    				GoodsCats goodsCats = goodsCatsService.selectById(catIdPathList[i]);
	        			String catName = goodsCats.getCatName();
	        			goodsCatNamePath=goodsCatNamePath+catName+"→";
	    			}
	    		}
	    		if(!"".equals(goodsCatNamePath)){
	    			goodsCatNamePath=goodsCatNamePath.substring(0,goodsCatNamePath.lastIndexOf("→") );
		    		map.put("goodsCatPath", goodsCatNamePath);
	    		}
    	}
        page.setRecords(attributes);
		return super.packForBT(page);
    }
    public boolean isInteger(String str) {  
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");  
        return pattern.matcher(str).matches();  
    }
    /**
     * 新增商品属性
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AttributesCustem attributesCustem) {
    	String goods1Name=null;
    	String goods2Name=null;
    	String goods3Name=null;
    	
    	Integer goods1=attributesCustem.getGoods1();
    	Integer goods2=attributesCustem.getGoods2();
    	Integer goods3=attributesCustem.getGoods3();
    //	Integer goods=attributesCustem.getGoods();
    	String goodsCatPath=null;
    	Integer goodsCatId =null;
    	if(goods1!=null && !"".equals(goods1)
    			&&goods2!=null && !"".equals(goods2)
    			&& goods3!=null && !"".equals(goods3)
    		){
//    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
//    		goodsCatPath=goods1Name+"→"+goods2Name+"→"+goods3Name;
			goodsCatPath=goods1+"_"+goods2+"_"+goods3;
			goodsCatId=goods3;
    	}else if(goods1!=null && !"".equals(goods1)
    			&&goods2!=null && !"".equals(goods2)
    			&& (goods3==null ||"".equals(goods3))
    			){
//    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
//    		goodsName = goodsCatsService.selectById(goods).getCatName();
//    		goodsCatPath=goods1Name+"→"+goods2Name;
			goodsCatPath=goods1+"_"+goods2;
    		goodsCatId=goods2;
    	}else if(goods1!=null && !"".equals(goods1)
    			&&(goods2==null ||"".equals(goods2))
    			){
//    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
//    		goodsName = goodsCatsService.selectById(goods).getCatName();

    		goodsCatId=goods1;
    		goodsCatPath=goods1+"";
    	}
    	attributesCustem.setGoodsCatPath(goodsCatPath);
    	attributesCustem.setGoodsCatId(goodsCatId);
    	Date d=new Date();
    	attributesCustem.setCreateTime(d);
    	attributesCustem.setDataFlag(1);
        attributesService.insertAttributes(attributesCustem);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品属性
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer attributesId) {
        attributesService.deleteById(attributesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品属性
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AttributesCustem attributesCustem) {
    	Attributes attributes=attributesService.selectById(attributesCustem.getAttrId());
    	String goods1Name=null;
    	String goods2Name=null;
    	String goods3Name=null;
    	
    	Integer goods1=attributesCustem.getGoods1();
    	Integer goods2=attributesCustem.getGoods2();
    	Integer goods3=attributesCustem.getGoods3();
    //	Integer goods=attributesCustem.getGoods();
    	String goodsCatPath=null;
    	Integer goodsCatId =null;
    	if(goods1!=null && !"".equals(goods1)
    			&&goods2!=null && !"".equals(goods2)
    			&& goods3!=null && !"".equals(goods3)
    		){
//    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
//    		goodsCatPath=goods1Name+"→"+goods2Name+"→"+goods3Name;
			goodsCatPath=goods1+"_"+goods2+"_"+goods3;
    		goodsCatId=goods3;
    	}else if(goods1!=null && !"".equals(goods1)
    			&&goods2!=null && !"".equals(goods2)
    			&& (goods3==null ||"".equals(goods3))
    			){
//    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
//    		goodsName = goodsCatsService.selectById(goods).getCatName();
// 			goodsCatPath=goods1Name+"→"+goods2Name;
			goodsCatPath=goods1+"_"+goods2;
    		goodsCatId=goods2;
    	}else if(goods1!=null && !"".equals(goods1)
    			&&(goods2==null ||"".equals(goods2))
    			){
    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
//    		goodsName = goodsCatsService.selectById(goods).getCatName();
    		goodsCatId=goods1;
    		goodsCatPath=goods1+"";
    	}
    	attributes.setGoodsCatPath(goodsCatPath);
    	attributes.setGoodsCatId(goodsCatId);
    	attributes.setAttrName(attributesCustem.getAttrName());
    	attributes.setAttrType(attributesCustem.getAttrType());
    	attributes.setIsShow(attributesCustem.getIsShow());
    	attributes.setAttrSort(attributesCustem.getAttrSort());
    	attributesService.updateById(attributes);
        return SUCCESS_TIP;
    }

    /**
     * 商品属性详情
     */
    @RequestMapping(value = "/detail/{attributesId}")
    @ResponseBody
    public Object detail(@PathVariable("attributesId") Integer attributesId) {
		return attributesService.selectById(attributesId);
    	
    }
}
