package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AccountShop;
import com.stylefeng.guns.modular.huamao.model.AccountShopList;

/**
 * <p>
 * 商家账户表(包括线上和线下) 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-12
 */
public interface IAccountShopService extends IService<AccountShop> {
	// 展示商家账户列表
	public List<AccountShopList> selectAccountShopAll(Page<AccountShopList> page, AccountShopList accountShopList);
}
