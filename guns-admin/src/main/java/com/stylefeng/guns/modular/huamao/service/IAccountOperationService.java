package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AccountOperation;
import com.stylefeng.guns.modular.huamao.model.AccountOperationList;

/**
 * <p>
 * 修改账户数据记录 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-13
 */
public interface IAccountOperationService extends IService<AccountOperation> {

	// 展示手动补单记录列表
	public List<AccountOperationList> selectAccountOperationAll(Page<AccountOperationList> page, AccountOperationList accountOperationList);
}
