package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktAreas;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 区域表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface ISktAreasService extends IService<SktAreas> {
	List<Map<String,Object>> selectByParentId(Integer parentId);
	boolean deleteByParentId(Integer parentId);
	List<Map<String,Object>> selectByName(SktAreas sktAreas);
	Integer selectMaxAreaSort();
}
