package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.stylefeng.guns.modular.huamao.dao.GoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopsMapper;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.model.SktShops2;
import com.stylefeng.guns.modular.huamao.service.ISktShopsService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * <p>
 * 商家信息表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
@Service
public class SktShopsServiceImpl extends ServiceImpl<SktShopsMapper, SktShops> implements ISktShopsService {
	@Autowired
	private SktShopsMapper sktShopsMapper;
	@Autowired
	private GoodsMapper goodsMapper;
	@Override
	public List<SktShops> sktShopsfindAll(SktShops sktShops) {
		List<SktShops> list = sktShopsMapper.sktShopsfindAll(sktShops);
		return list;
	}
	@Override
	public int selectCount() {
		// TODO Auto-generated method stub
		return sktShopsMapper.selectCount();
	}
	@Override
	public int getTotal() {
		// TODO Auto-generated method stub
		return sktShopsMapper.getTotal();
	}
	/*
	 * 店铺街展示店铺
	 * */
	@Override
	public List<Map<String,Object>> findShopsAll() {
		List<Map<String, Object>> m = sktShopsMapper.findShopsAll();
		 return m;
	}
	/*
	 * 店铺街展示店铺推荐
	 * */
	@Override
	public List<Map<String, Object>> findShopsByIdtj(Integer shopId) {
		List<Map<String, Object>> map = sktShopsMapper.findShopsByIdtj(shopId);
		return map;
	}
	/*
	 * 店铺街查询行业
	 * */
	@Override
	public List<Map<String, Object>> findShopsHyName() {
		return sktShopsMapper.findShopsHyName();
	}
	@Override
	public List<Map<String, Object>> selectByHyIdShops(Integer hyId) {
		return sktShopsMapper.selectByHyIdShops(hyId);
		 
	}
	/*
	 * 查询热门品牌findBrandId
	 * */
	@Override
	public List<Map<String, Object>> findBrandId() {
		return sktShopsMapper.findBrandId();
	}
	  /*
     * 根据店铺ID查询商品
     * */
	@Override
	public List<Map<String, Object>> selectShopsGoodsId(Integer shopId) {
		return sktShopsMapper.selectShopsGoodsId(shopId);
	}
	/*
	 * 自营街店铺展示
	 * */
	@Override
	public List<Map<String, Object>> selectShopsZy() {
		return sktShopsMapper.selectShopsZy();
	}
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectShopNameShops(SktShops2 sktShops2){
		List<Map<String,Object>> list = sktShopsMapper.selectShopNameShops(sktShops2);
		return list;
	}
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	@Override
	public List<Map<String, Object>> selectLogoShops(SktShops2 sktShops2) {
		return sktShopsMapper.selectLogoShops(sktShops2);
	} 
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	@Override
	public List<Map<String, Object>> selectLoginNameShops(SktShops2 sktShops2) {
		return sktShopsMapper.selectLoginNameShops(sktShops2);
	}
	/*
     * 根据店铺ID查询ZY商品
     * */
	@Override
	public List<Map<String, Object>> selectShopsZyGoodsId(Integer shopId) {
		return sktShopsMapper.selectShopsZyGoodsId(shopId);
	}
	@Override
	public List<Map<String, Object>> findWebLunbo() {
		return sktShopsMapper.findWebLunbo();
	}

	//查询所有商家的userId
	@Override
	public List<Integer> selectAllId() {
		return sktShopsMapper.selectAllId();
	}
	@Override
	@Transactional
	public  Map<String, Object> updateSktShops(SktShops sktShops){
		Map<String, Object> result=new HashMap<>();
		try {
			if (sktShops.getShopStatus()==0){
				Map<String, Object> map=new HashMap<>();
				map.put("shopId",sktShops.getShopId());
				map.put("isSale",0);
//				map.put("goodsStatus",-1);
				map.put("dataFlag",1);
				goodsMapper.updateSktGoodsByMap(map);

			}

			this.updateById(sktShops);
			result.put("code","1");
		}catch (Exception e){
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			result.put("code","0");
		}

		return  result;
	}

}
