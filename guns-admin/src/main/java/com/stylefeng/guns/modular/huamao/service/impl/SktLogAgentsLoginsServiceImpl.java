package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktLogAgentsLogins;
import com.stylefeng.guns.modular.huamao.dao.SktLogAgentsLoginsMapper;
import com.stylefeng.guns.modular.huamao.service.ISktLogAgentsLoginsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代理公司登陆记录表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
@Service
public class SktLogAgentsLoginsServiceImpl extends ServiceImpl<SktLogAgentsLoginsMapper, SktLogAgentsLogins> implements ISktLogAgentsLoginsService {
	@Autowired
	private SktLogAgentsLoginsMapper sktLogAgentsLoginsMapper;
	@Override
	public List<SktLogAgentsLogins> sktLogAgentsLoginfindup(Page<SktLogAgentsLogins> page, SktLogAgentsLogins sktLogAgentsLogins){
		Integer total= sktLogAgentsLoginsMapper.sktLogAgentsLoginfindupCount(sktLogAgentsLogins);
		page.setTotal(total);
		return sktLogAgentsLoginsMapper.sktLogAgentsLoginfindup(page,sktLogAgentsLogins);
	}
	
	
}
