package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanFreezeMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreeze;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreezeList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanFreezeService;

/**
 * <p>
 * 待发开元宝营业额流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
@Service
public class LogKaiyuanFreezeServiceImpl extends ServiceImpl<LogKaiyuanFreezeMapper, LogKaiyuanFreeze>
		implements ILogKaiyuanFreezeService {

	@Autowired
	private LogKaiyuanFreezeMapper lkfm;

	// 待发华宝营业额流水表展示
	@Override
	public List<LogKaiyuanFreezeList> selectLogKaiyuanFreezeAll(Page<LogKaiyuanFreezeList> page, LogKaiyuanFreezeList logKaiyuanFreezeList) {
		Integer total = lkfm.selectLogKaiyuanFreezeAllCount(logKaiyuanFreezeList);
		page.setTotal(total);
		return lkfm.selectLogKaiyuanFreezeAll(page,logKaiyuanFreezeList);
	}

}
