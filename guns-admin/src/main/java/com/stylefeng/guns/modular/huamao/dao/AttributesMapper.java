package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Attributes;
import com.stylefeng.guns.modular.huamao.model.AttributesCustem;

/**
 * <p>
 * 商品属性表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface AttributesMapper extends BaseMapper<Attributes> {
	Integer insertAttributes(AttributesCustem attributesCustem);
	List<Map<String, Object>> selectAll(@Param("page")Page<Map<String, Object>> page, 
			@Param("attributesCustem")AttributesCustem attributesCustem); 
	Integer selectAllCount(@Param("attributesCustem")AttributesCustem attributesCustem);
	AttributesCustem selectAttributesCustem(Integer id);
}
