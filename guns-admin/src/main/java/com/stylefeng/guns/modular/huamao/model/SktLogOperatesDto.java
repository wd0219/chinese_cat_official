package com.stylefeng.guns.modular.huamao.model;

import java.util.Date;

public class SktLogOperatesDto extends SktLogOperates {

	    /**
	     * 开始时间
	     */
	    private String beginTime;
	    /**
	     * 结束时间
	     */
	    private String endTime;

		public String getBeginTime() {
			return beginTime;
		}

		public void setBeginTime(String beginTime) {
			this.beginTime = beginTime;
		}

		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}
	    
}
