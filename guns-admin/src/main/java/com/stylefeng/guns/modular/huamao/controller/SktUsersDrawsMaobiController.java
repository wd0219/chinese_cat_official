package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktUsersDrawsMaobi;
import com.stylefeng.guns.modular.huamao.model.SktUsersDrawsMaobiDto;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsList;
import com.stylefeng.guns.modular.huamao.service.ISktUsersDrawsMaobiService;

/**
 * 用户猫币提现控制器
 *
 * @author fengshuonan
 * @Date 2018-07-30 15:41:21
 */
@Controller
@RequestMapping("/sktUsersDrawsMaobi")
public class SktUsersDrawsMaobiController extends BaseController {

    private String PREFIX = "/huamao/sktUsersDrawsMaobi/";

    @Autowired
    private ISktUsersDrawsMaobiService sktUsersDrawsMaobiService;

    /**
     * 跳转到用户猫币提现首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktUsersDrawsMaobi.html";
    }

    /**
     * 跳转到添加用户猫币提现
     */
    @RequestMapping("/sktUsersDrawsMaobi_add")
    public String sktUsersDrawsMaobiAdd() {
        return PREFIX + "sktUsersDrawsMaobi_add.html";
    }

    /**
     * 跳转到修改用户猫币提现
     */
    @RequestMapping("/sktUsersDrawsMaobi_update/{sktUsersDrawsMaobiId}")
    public String sktUsersDrawsMaobiUpdate(@PathVariable Integer sktUsersDrawsMaobiId, Model model) {
        SktUsersDrawsMaobi sktUsersDrawsMaobi = sktUsersDrawsMaobiService.selectById(sktUsersDrawsMaobiId);
        model.addAttribute("item",sktUsersDrawsMaobi);
        LogObjectHolder.me().set(sktUsersDrawsMaobi);
        return PREFIX + "sktUsersDrawsMaobi_edit.html";
    }

    /**
     * 获取用户猫币提现列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktUsersDrawsMaobiDto sktUsersDrawsMaobiDto) {
    	Page<SktUsersDrawsMaobiDto> page = new PageFactory<SktUsersDrawsMaobiDto>().defaultPage();
		List<SktUsersDrawsMaobiDto> usersDrawsLists = sktUsersDrawsMaobiService.selectMaoBiList(page,sktUsersDrawsMaobiDto);
		page.setRecords(usersDrawsLists);
		return super.packForBT(page);
    }

    /**
     * 新增用户猫币提现
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktUsersDrawsMaobi sktUsersDrawsMaobi) {
        sktUsersDrawsMaobiService.insert(sktUsersDrawsMaobi);
        return SUCCESS_TIP;
    }

    /**
     * 删除用户猫币提现
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktUsersDrawsMaobiId) {
        sktUsersDrawsMaobiService.deleteById(sktUsersDrawsMaobiId);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户猫币提现
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktUsersDrawsMaobi sktUsersDrawsMaobi) {
        sktUsersDrawsMaobiService.updateById(sktUsersDrawsMaobi);
        return SUCCESS_TIP;
    }

    /**
     * 用户猫币提现详情
     */
    @RequestMapping(value = "/detail/{sktUsersDrawsMaobiId}")
    @ResponseBody
    public Object detail(@PathVariable("sktUsersDrawsMaobiId") Integer sktUsersDrawsMaobiId) {
        return sktUsersDrawsMaobiService.selectById(sktUsersDrawsMaobiId);
    }
}
