package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktFriendlinks;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 友情连接表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface ISktFriendlinksService extends IService<SktFriendlinks> {

}
