package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AgentsDraws;
import com.stylefeng.guns.modular.huamao.model.AgentsDrawsList;

/**
 * <p>
 * 代理公司提现记录表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface IAgentsDrawsService extends IService<AgentsDraws> {

	// 代理公司提现记录表展示
	public List<AgentsDrawsList> selectAgentsDrawsAll(Page<AgentsDrawsList> page, AgentsDrawsList agentsDrawsList);

    void updateSatus(AgentsDrawsList agentsDrawsList);
}
