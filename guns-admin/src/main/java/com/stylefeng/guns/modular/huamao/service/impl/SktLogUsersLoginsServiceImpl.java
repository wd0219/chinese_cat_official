package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktLogUsersLogins;
import com.stylefeng.guns.modular.huamao.dao.SktLogUsersLoginsMapper;
import com.stylefeng.guns.modular.huamao.service.ISktLogUsersLoginsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员登陆记录表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
@Service
public class SktLogUsersLoginsServiceImpl extends ServiceImpl<SktLogUsersLoginsMapper, SktLogUsersLogins> implements ISktLogUsersLoginsService {
	@Autowired
	private SktLogUsersLoginsMapper SktLogUsersLoginsMapper;
	@Override
	public List<SktLogUsersLogins> sktLogUsersLoginsfindup(Page<SktLogUsersLogins> page, SktLogUsersLogins sktLogUsersLogins) {
		Integer total = SktLogUsersLoginsMapper.sktLogUsersLoginsfindupCount(sktLogUsersLogins);
		page.setTotal(total);
		return SktLogUsersLoginsMapper.sktLogUsersLoginsfindup(page,sktLogUsersLogins);
	}

}
