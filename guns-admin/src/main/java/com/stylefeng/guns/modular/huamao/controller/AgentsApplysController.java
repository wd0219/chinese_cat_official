package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.model.AgentsApplys;
import com.stylefeng.guns.modular.huamao.model.AgentsApplysDTO;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholder;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IAgentsApplysService;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsStockholderService;

/**
 * 代理股东申请控制器
 *
 * @author fengshuonan
 * @Date 2018-04-17 16:06:28
 */
@Controller
@RequestMapping("/agentsApplys")
public class AgentsApplysController extends BaseController {

    private String PREFIX = "/huamao/agentsApplys/";

    @Autowired
    private IAgentsApplysService agentsApplysService;
    @Autowired
    private IAccountService iAccountService;  
    @Autowired
    private UsersMapper usersMapper;
    @Autowired
    private ISktAgentsStockholderService iSktAgentsStockholderService;
	@Autowired
	private IAccountService accountService;
    /**
     * 跳转到代理股东申请首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "agentsApplys.html";
    }

    /**
     * 跳转到添加代理股东申请
     */
    @RequestMapping("/agentsApplys_add")
    public String agentsApplysAdd() {
        return PREFIX + "agentsApplys_add.html";
    }
    
    /**
    * 跳转到通过
    */
   @RequestMapping("/agentsApplys_pass/{shaId}/{status}")
   public String agentsApplysPass(@PathVariable(value="shaId") Integer shaId,
		   @PathVariable(value="status") Integer status,Model model) {
	   model.addAttribute("shaId",shaId);
	   model.addAttribute("status",status);
       return PREFIX + "agentsApplys_pass.html";
   }
   /**
    * 跳转到刷新
    */
   @RequestMapping("/agentsApplys_refuse/{shaId}/{status}")
   public String agentsApplysRefuse(@PathVariable(value="shaId") Integer shaId,
		   @PathVariable(value="status") Integer status,Model model) {
	   model.addAttribute("shaId",shaId);
	   model.addAttribute("status",status);
       return PREFIX + "agentsApplys_refuse.html";
   }

    /**
     * 跳转到修改代理股东申请
     */
    @RequestMapping("/agentsApplys_update/{agentsApplysId}")
    public String agentsApplysUpdate(@PathVariable Integer agentsApplysId, Model model) {
        AgentsApplys agentsApplys = agentsApplysService.selectById(agentsApplysId);
        model.addAttribute("item",agentsApplys);
        LogObjectHolder.me().set(agentsApplys);
        return PREFIX + "agentsApplys_edit.html";
    }

    /**
     * 获取代理股东申请列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AgentsApplysDTO agentsApplysDTO) {
    	Page<AgentsApplysDTO> page = new PageFactory<AgentsApplysDTO>().defaultPage();
    	List<AgentsApplysDTO> list = agentsApplysService.showDaiLiShenQingInfo(page,agentsApplysDTO);
    	page.setRecords(list);
        return super.packForBT(page);
    }

    /**
     * 新增代理股东申请
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AgentsApplys agentsApplys) {
        agentsApplysService.insert(agentsApplys);
        return SUCCESS_TIP;
    }

    /**
     * 显示添加信息代理
     */
    @RequestMapping(value = "/showAddInfo/{shaId}")
    public Object showAddInfo(@PathVariable Integer shaId,Model model) {
    	 AgentsApplysDTO selectById = agentsApplysService.selectAddInfo(shaId);
    	 model.addAttribute("item",selectById);
    	return PREFIX + "agentsApplys_showAddInfo.html";
    }
    /**
     * 显示汇款单
     */
    @RequestMapping(value = "/showInfo/{shaId}")
    public Object showInfo(@PathVariable Integer shaId,Model model) {
    	AgentsApplys selectById = agentsApplysService.selectById(shaId);
    	String url = selectById.getMoneyOrderImg();
 		Map<String,Object> map = new HashMap<>();
 		if(url != null && !"".equals(url)){
 			String[] allurl = url.split(",");
 			for(int i = 0; i < allurl.length; i++){
 				map.put("moneyOrderImg"+i,UrlUtils.getAllUrl(allurl[i]));
 			}
 		}else{
 			map.put("moneyOrderImg"+0,"/static/img/girl.gif");
 			map.put("moneyOrderImg"+1,"/static/img/girl.gif");
 			map.put("moneyOrderImg"+2,"/static/img/girl.gif");
 		}
 		map.put("shaId", selectById.getShaId());
 		map.put("remark", selectById.getRemark());
 		model.addAttribute("item", map);
//    	 String str = UrlUtils.getAllUrl(selectById.getMoneyOrderImg());
//    	 if(str==null || "".equals(str)){
//    		 selectById.setMoneyOrderImg("/static/img/girl.gif");
//    	 }else{
//    		 selectById.setMoneyOrderImg(str);
//    	 }
//    	 model.addAttribute("item",selectById);
    	return PREFIX + "agentsApplys_showInfo.html";
    }
    /**
     * 删除代理股东申请
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer agentsApplysId) {
        agentsApplysService.deleteById(agentsApplysId);
        return SUCCESS_TIP;
    }

    /**
     * 修改代理股东申请
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AgentsApplys agentsApplys) {
    //    String result = "";
        Map<String, Object> map=new HashMap<>();
    	if(	agentsApplys.getStatus()==-2){
    		AgentsApplys agentsApplys2 = agentsApplysService.selectById(agentsApplys.getShaId());
    		agentsApplys2.setTwoCheckTime(new Date());
    		agentsApplys2.setStatus(agentsApplys.getStatus());
    		agentsApplys2.setCheckRemark(agentsApplys.getCheckRemark());
    		agentsApplysService.updateById(agentsApplys2);
    		//代理升级 不通过
    		iAccountService.resetAgentUPGrade(agentsApplys2);
    		return "操作成功";
    	}else if(agentsApplys.getStatus() == -1 ||  agentsApplys.getStatus() == 1 ||
    			agentsApplys.getStatus()==3){
    		// 发送者id为操作员id
            ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
            Integer  shiroUserId=shiroUser.getId();
           map = agentsApplysService.applysSetStatus(agentsApplys.getShaId(), shiroUserId);
            return map.get("msg").toString();
    	}
        return "操作成功";
    }

    /**
     * 代理股东申请详情
     */
    @RequestMapping(value = "/detail/{agentsApplysId}")
    @ResponseBody
    public Object detail(@PathVariable("agentsApplysId") Integer agentsApplysId) {
        return agentsApplysService.selectById(agentsApplysId);
    }
}
