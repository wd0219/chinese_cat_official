package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 购买牌匾 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
public interface ISktBuyplaqueService extends IService<SktBuyplaque> {
	
	/*
	 * 展示
	 * */
	public List<SktBuyplaque> sktBuyplaquefindall(Page<SktBuyplaque> page, SktBuyplaque buyplaque);
	/*
	 * 修改 
	 * */
	public void sktBuyplaqueUpdate(SktBuyplaque buyplaque);
	
	public void sktBuyplaqueUpdate2(SktBuyplaque buyplaque);
	
}
