package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktPointuser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 点赞用户表(预留表) 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface ISktPointuserService extends IService<SktPointuser> {

}
