package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;

/**
 * <p>
 * 商品分类表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsCatsMapper extends BaseMapper<GoodsCats> {
	List<GoodsCats> selectByParentId(int parentId);
	boolean deleteByPId (Integer pid);
	List<Map<String,Object>> selectMap(@Param("goodsCats")GoodsCats goodsCats);
	Integer selectMapCount(@Param("goodsCats") GoodsCats goodsCats);
	List<Brands> selectCatName(@Param(value="sIds") String sIds);
	Integer updateByPId (Integer catId);

	Integer deleteDataFlagById(Integer catId);
}
