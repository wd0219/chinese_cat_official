package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 华宝流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogKaiyuanMapper extends BaseMapper<LogKaiyuan> {

	// 华宝流水返回
	List<LogKaiyuanList> selectLogKaiyuanAll(@Param("page")Page<LogKaiyuanList> page, @Param("logKaiyuanList") LogKaiyuanList logKaiyuanList);

    Integer selectLogKaiyuanAllCount(@Param("logKaiyuanList") LogKaiyuanList logKaiyuanList);
}
