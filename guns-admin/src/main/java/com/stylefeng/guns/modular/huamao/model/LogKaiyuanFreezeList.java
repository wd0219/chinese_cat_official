package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 待发开元宝营业额流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public class LogKaiyuanFreezeList extends LogKaiyuanFreeze {


	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String kaiyuanp;
	/**
	 * 变动后金额
	 */
	private BigDecimal akaiyuan;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	public String getKaiyuanp() {
		if (this.getKaiyuanType() == -1) {
			return "-" + this.getKaiyuan();
		}
		return "+" + this.getKaiyuan();
	}

	// public void setKaiyuanp(String kaiyuanp) {
	// this.kaiyuanp = kaiyuanp;
	// }

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	/**
	 * 变动后金额，直接让变动前加上变动的就行，不需要赋值。
	 */
	public BigDecimal getAkaiyuan() {
		if(this.getKaiyuanType() == 1){
			return this.getPreKaiyuan().add(this.getKaiyuan());
		}else{
			return this.getPreKaiyuan().subtract(this.getKaiyuan());
		}
	}

	// public void setAkaiyuan(BigDecimal akaiyuan) {
	// this.akaiyuan = akaiyuan;
	// }


	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
