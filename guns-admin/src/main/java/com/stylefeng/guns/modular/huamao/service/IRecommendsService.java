package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.BrandsCustem;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;
import com.stylefeng.guns.modular.huamao.model.Recommends;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 推荐记录表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-24
 */
public interface IRecommendsService extends IService<Recommends> {
	Map<String,Object> selectMax();
	boolean deleteByDataId(Integer dataId);
	Map<String,Object>selectBrand(Integer dataId);

	public boolean deleteByMap (Map<String,Object> map);
	public Integer insertRrecommends (Map<String,Object> map);
	public  void addBrandsList(List<Map<String,Object>> list1,Integer catId);
	public  void addGoodsList(List<Map<String,Object>> list1,Integer catId,Integer dataType);
	List<Map<String,Object>> selectRecomGoods(GoodsCustm goodsCustm);
	List<Map<String,Object>> selectRecommendBrands(BrandsCustem brandsCustem);


}
