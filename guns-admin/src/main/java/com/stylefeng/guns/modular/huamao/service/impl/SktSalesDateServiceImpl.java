package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktSalesDateMapper;
import com.stylefeng.guns.modular.huamao.model.SktSalesDate;
import com.stylefeng.guns.modular.huamao.service.ISktSalesDateService;

/**
 * <p>
 * 商家每日营业额统计表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
@Service
public class SktSalesDateServiceImpl extends ServiceImpl<SktSalesDateMapper, SktSalesDate> implements ISktSalesDateService {
	@Autowired
	private SktSalesDateMapper salesDateMapper;
	@Override
	public List<SktSalesDate> sktSalesDatefindAll(Page<SktSalesDate>page,SktSalesDate sktSalesDate) {
		List<SktSalesDate> list = salesDateMapper.sktSalesDatefindAll(page,sktSalesDate);
		Integer total = salesDateMapper.sktSalesDatefindAllCount(sktSalesDate);
		page.setTotal(total);
		return list;
	}

}
