package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktAgentsProfit;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsProfitMapper;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsProfitService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 代理股东分红记录表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-12
 */
@Service
public class SktAgentsProfitServiceImpl extends ServiceImpl<SktAgentsProfitMapper, SktAgentsProfit> implements ISktAgentsProfitService {

}
