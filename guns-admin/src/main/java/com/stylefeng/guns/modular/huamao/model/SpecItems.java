package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商品规格值表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_spec_items")
public class SpecItems extends Model<SpecItems> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "itemId", type = IdType.AUTO)
    private Integer itemId;
    /**
     * 店铺ID
     */
    private Integer shopId;
    /**
     * 类型ID
     */
    private Integer catId;
    /**
     * 商品ID
     */
    private Integer goodsId;
    /**
     * 项名称
     */
    private String itemName;
    private String itemDesc;
    /**
     * 规格图片
     */
    private String itemImg;
    /**
     * 有效状态：1有效-1无效
     */
    private Integer dataFlag;
    private Date createTime;


    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemImg() {
        return itemImg;
    }

    public void setItemImg(String itemImg) {
        this.itemImg = itemImg;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.itemId;
    }

    @Override
    public String toString() {
        return "SpecItems{" +
        "itemId=" + itemId +
        ", shopId=" + shopId +
        ", catId=" + catId +
        ", goodsId=" + goodsId +
        ", itemName=" + itemName +
        ", itemDesc=" + itemDesc +
        ", itemImg=" + itemImg +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
