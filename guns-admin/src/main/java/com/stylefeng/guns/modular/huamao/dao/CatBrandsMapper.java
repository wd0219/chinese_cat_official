package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.CatBrands;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 分类-品牌表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface CatBrandsMapper extends BaseMapper<CatBrands> {

}
