package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogCashFreeze;
import com.stylefeng.guns.modular.huamao.model.LogCashFreezeList;

/**
 * <p>
 * 待发现金流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface ILogCashFreezeService extends IService<LogCashFreeze> {

	// 代发现金流水表返回
	public List<LogCashFreezeList> selectLogCashFreezeAll(Page<LogCashFreezeList> page, LogCashFreezeList logCashFreezeList);

}
