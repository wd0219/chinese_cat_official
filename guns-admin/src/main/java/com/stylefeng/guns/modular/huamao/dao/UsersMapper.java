package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersDTO;
import com.stylefeng.guns.modular.huamao.model.UsersLoginDTO;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-10
 */
public interface UsersMapper extends BaseMapper<Users> {
	/**
	 * 查询显示页面数据
	 * @param page 
	 * @param usersDTO
	 * @return
	 */
	public List<UsersDTO> selectUsersInfo(@Param("page")Page<UsersDTO> page,
			@Param("usersDTO")UsersDTO usersDTO);
	public Integer selectUsersInfoCount(@Param("usersDTO")UsersDTO usersDTO);
	/**
	 * 修改显示页面数据
	 * @param usersDTO
	 * @return
	 */
	public UsersDTO selectOneUsersInfo(UsersDTO usersDTO);
	/**
	 * 更新用户数据
	 * @param usersDTO
	 */
	public void updateUsersInfo(UsersDTO usersDTO);
	/**
	 * 查询推荐人列表
	 * @param inviteId
	 * @return
	 */
	public List<UsersDTO> selectTuiUsersInfo(Integer inviteId);
	/**
	 * 查询推荐人用户电话
	 * @param usersPhone
	 * @param usersId
	 * @return
	 */
	public List<Users> selectTuiPhone(@Param(value="usersPhone") String usersPhone, @Param(value="usersId") Integer usersId);
	/**
	 * 查询推荐人登录名
	 * @param usersName
	 * @param usersId
	 * @return
	 */
	public List<Users> selectTuiName(@Param(value="usersName") String usersName, @Param(value="usersId") Integer usersId);
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int getTotal();
	/**
	 * 查询用户id电话 姓名身份证号 用户类型 推荐人 代理股东 线上线下商家  是不是特权账号 签约卡的手机号
	 * @param users
	 * @return
	 */
	public Map<String, Object> selectUsersPhone(Users users);
	
	/**
	 * 用户登录
	 * @param loginName
	 * @param loginPwd
	 * @return
	 */
	public UsersLoginDTO userLogin(Users users);
	/**
	 * 根据用户名查询用户
	 * @param LoginName
	 * @return
	 */
	List<Users> findListByLoginName(@Param("loginName") String LoginName);

	/**
	 * 查询用户分享人
	 * @param shareMan
	 * @return
	 */
	Users selectUserByLoginNameOrUserPhone(@Param("LoginName") String shareMan);

	Users selectUsersByUSerPhoneAndPayPwd(Users users);
	/**
	 * 核实用户身份
	 * @param userPhoneOrloginName
	 * @return
	 */
	public Users checkUserInfo(@Param(value="userPhoneOrloginName") String userPhoneOrloginName);
	 /**
     * 查询用户注册信息
     * @param id
     * @return
     */
    public  Map<String,Object> selectByUsersInfo(Integer id);
    public Integer selectTuiUsersInfoCount(Integer inviteId);
    /**
     * 查询用户注册信息
     * @param id
     * @return
     */
    public int updateUsersPwd(UsersDTO usersDTO);
    /**
     * 用户名查询
     * @param loginName
     * @return
     */
	public List<Users> selectByLoginName(@Param(value="loginName")String loginName,@Param(value="userPhone") String userPhone);
	/**
	 * 根据用户名跟密码查询用户
	 * @param users
	 * @return
	 */
	public Users selectByUserInfo(Users users);

    List<Integer> findDownWires(Integer userId);

    Users findShareMan(Integer userId);

	Integer findShareManId(Integer userId);

	public Integer selectCountIsStore(Integer inviteId);
	
	public List<UsersDTO> selectUsersInfo2(UsersDTO usersDTO);
	
	
}
