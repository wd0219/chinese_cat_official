package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStockscore;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商家购买库存积分订单表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
public interface SktOrdersStockscoreMapper extends BaseMapper<SktOrdersStockscore> {
	
	/*
	 * 展示全部
	 * */
	public List<SktOrdersStockscore> sktOrdersStockscorefindall(@Param("page")Page<SktOrdersStockscore> page, @Param("ordersStockscore") SktOrdersStockscore ordersStockscore);

    Integer sktOrdersStockscorefindallCount(@Param("ordersStockscore") SktOrdersStockscore ordersStockscore);
}
