package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktBanks;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 银行表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface ISktBanksService extends IService<SktBanks> {

}
