package com.stylefeng.guns.modular.huamao.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktOrdersList;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersService;

/**
 * 订单管理控制器
 *
 * @author caody
 * @Date 2018-04-14 10:01:10
 */
@Controller
@RequestMapping("/sktOrders")
public class SktOrdersController extends BaseController {

    private String PREFIX = "/huamao/sktOrders/";

    @Autowired
    private ISktOrdersService sktOrdersService;

    /**
     * 跳转到订单管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktOrders.html";
    }
    /**
     * 跳转到订单管理首页
     */
    @RequestMapping(value = "/alllist")
    @ResponseBody
    public Object allList(SktOrdersList sktOrdersList) {
    	Page<SktOrdersList> page = new PageFactory<SktOrdersList>().defaultPage();
    	List<SktOrdersList> list = sktOrdersService.selectOrderShops(page,sktOrdersList);
    	page.setRecords(list);
		return super.packForBT(page);
    }

    /**
     * 跳转到添加订单管理
     */
    @RequestMapping("/sktOrders_add")
    public String sktOrdersAdd() {
        return PREFIX + "sktOrders_add.html";
    }

    /**
     * 跳转到订单详情
     */
    @RequestMapping("/sktOrdersShow/{sktOrdersId}")
    public String sktOrdersShow(@PathVariable Integer sktOrdersId, Model model) {
    	model.addAttribute("sktOrdersId", sktOrdersId);
        return PREFIX + "sktOrders_show.html";
    }
    /**
     * 加载订单显示内容
     */
    @RequestMapping(value = "/sktOrders_load")
    @ResponseBody
    public Object sktOrdersLoad(Integer sktOrdersId) {
    	return sktOrdersService.selectById(sktOrdersId);
    }
    /**
     * 加载商品显示内容
     */
    @RequestMapping(value = "/sktOrdersGoods_load")
    @ResponseBody
    public Object sktOrdersGoodsLoad(Integer sktOrdersId) {
    	Map<String,Object> param = new HashMap<String,Object>();
    	param.put("sktOrdersId", sktOrdersId);
    	return sktOrdersService.selectOrderGoods(param);
    }
    /**
     * 加载商品参数显示内容
     */
    @RequestMapping(value = "/sktOrdersGoodsParameter_load")
    @ResponseBody
    public Object sktOrdersGoodsParameterLoad(Integer goodsId) {
    	Map<String,Object> param = new HashMap<String,Object>();
    	param.put("goodsId", goodsId);
    	return sktOrdersService.selectOrderGoodsParameter(param);
    }
    
    /**
     * 跳转到订单详情
     */
    @RequestMapping("/sktOrders_update/{sktOrdersId}")
    public String sktOrdersUpdate(@PathVariable Integer sktOrdersId, Model model) {
    	SktOrders sktOrders = sktOrdersService.selectById(sktOrdersId);
    	model.addAttribute("item",sktOrders);
    	LogObjectHolder.me().set(sktOrders);
    	return PREFIX + "sktOrders_show.html";
    }

    /**
     * 获取订单管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
    	HashMap<String, Object> paramMap = new HashMap<String, Object>();
    	//return sktOrdersService.selecOrdertShops(paramMap);
        return sktOrdersService.selectList(null);
    }

    /**
     * 新增订单管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktOrders sktOrders) {
        sktOrdersService.insert(sktOrders);
        return SUCCESS_TIP;
    }

    /**
     * 删除订单管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktOrdersId) {
        sktOrdersService.deleteById(sktOrdersId);
        return SUCCESS_TIP;
    }

    /**
     * 修改订单管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktOrders sktOrders) {
        sktOrdersService.updateById(sktOrders);
        return SUCCESS_TIP;
    }

    /**
     * 订单管理详情
     */
    @RequestMapping(value = "/detail/{sktOrdersId}")
    @ResponseBody
    public Object detail(@PathVariable("sktOrdersId") Integer sktOrdersId) {
        return sktOrdersService.selectById(sktOrdersId);
    }
}
