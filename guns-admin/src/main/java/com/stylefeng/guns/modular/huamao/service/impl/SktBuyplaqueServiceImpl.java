package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktBuyplaqueMapper;
import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.ISktBuyplaqueService;

/**
 * <p>
 * 购买牌匾 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
@Service
public class SktBuyplaqueServiceImpl extends ServiceImpl<SktBuyplaqueMapper, SktBuyplaque> implements ISktBuyplaqueService {
	@Autowired
	private SktBuyplaqueMapper buyplaqueMapper;
	@Autowired
	private IAccountService AccountService;
	@Override
	public List<SktBuyplaque> sktBuyplaquefindall(Page<SktBuyplaque> page,SktBuyplaque buyplaque) {
		List<SktBuyplaque> list = buyplaqueMapper.sktBuyplaquefindall(page,buyplaque);
		Integer total = buyplaqueMapper.sktBuyplaquefindallCount(buyplaque);
		page.setTotal(total);
		return list;
	}
	@Override
	@Transactional
	//处理成功
	public void sktBuyplaqueUpdate(SktBuyplaque buyplaque) {
		try {
			buyplaqueMapper.sktBuyplaqueUpdate(buyplaque);
			AccountService.applysBuyplaqueTrue(buyplaque.getOrderId());
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	@Override
	@Transactional
	public void sktBuyplaqueUpdate2(SktBuyplaque buyplaque) {
		try {
		buyplaqueMapper.sktBuyplaqueUpdate2(buyplaque);
		AccountService.applysBuyplaqueFalse(buyplaque.getOrderId());
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
}
