package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 行业管理表 服务类
 * </p>
 *
 * @author ck123123
 * @since 2018-04-19
 */
public interface ISktShopIndustryService extends IService<SktShopIndustry> {
	
	public List<SktShopIndustry> shopIndustryfindinfo(Page<SktShopIndustry> page, SktShopIndustry shopIndustry);

	public List<SktShopIndustry> shopIndustryfindinfo2(SktShopIndustry shopIndustry);
	
}
