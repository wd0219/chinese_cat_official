package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnover;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnoverList;

/**
 * <p>
 * 开元宝营业额流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface ILogKaiyuanTurnoverService extends IService<LogKaiyuanTurnover> {

	// 开元宝营业额流水表展示
	public List<LogKaiyuanTurnoverList> selectLogKaiyuanTurnoverAll(Page<LogKaiyuanTurnoverList> page, LogKaiyuanTurnoverList logKaiyuanTurnoverList);

}
