package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktShopAccreds;
import com.stylefeng.guns.modular.huamao.model.SktShopAccredsDTO;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 店铺认证信息表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-13
 */
public interface SktShopAccredsMapper extends BaseMapper<SktShopAccreds> {
	
	/*展示*/
	public List<SktShopAccredsDTO> sktShopAccredsfindall(@Param("page")Page<SktShopAccredsDTO> page,@Param("sktShopAccredsDTO") SktShopAccredsDTO sktShopAccredsDTO);
	/*
	 * 删除
	 * */
	public void sktShopAccredsdelete(@Param(value="shopId") Integer shopId);
	/*
	 * 展示
	 * */
	public List<SktShopAccredsDTO> sktShopAccredsfindup(SktShopAccredsDTO shopAccredsDTO);

	Integer sktShopAccredsfindallCount(@Param("sktShopAccredsDTO") SktShopAccredsDTO sktShopAccredsDTO);
}
