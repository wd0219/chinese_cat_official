package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktComment;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktCommentDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 评论表(预留表) 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface ISktCommentService extends IService<SktComment> {
    public List<SktCommentDTO> selectSktCommentAll(@Param("page") Page<SktCommentDTO> page, @Param("sktCommentDTO") SktCommentDTO sktCommentDTO);

}
