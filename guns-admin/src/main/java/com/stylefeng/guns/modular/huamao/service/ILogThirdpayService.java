package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogThirdpay;
import com.stylefeng.guns.modular.huamao.model.LogThirdpayList;

/**
 * <p>
 * 第三方支付记录表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface ILogThirdpayService extends IService<LogThirdpay> {

	// 第三方支付记录表展示
	public List<LogThirdpayList> slectLogThirdpayAll(Page<LogThirdpayList> page, LogThirdpayList logThirdpayList);

}
