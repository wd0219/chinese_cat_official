package com.stylefeng.guns.modular.huamao.model;

public class SktCommentDTO  extends SktComment{
    private  String loginName;
    private  String className;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
