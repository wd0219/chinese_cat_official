package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktAdPositions;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 广告位置表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface SktAdPositionsMapper extends BaseMapper<SktAdPositions> {
	
	public List<SktAdPositions> sktAdPositionsfinAll(@Param("page") Page<SktAdPositions> page,@Param("sktAdPositions") SktAdPositions sktAdPositions);

    Integer sktAdPositionsfinAllCount(@Param("sktAdPositions") SktAdPositions sktAdPositions);
}
