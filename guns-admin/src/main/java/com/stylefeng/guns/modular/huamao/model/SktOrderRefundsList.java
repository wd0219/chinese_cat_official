package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;

public class SktOrderRefundsList extends SktOrderRefunds {

	/**
     * 退款备注
     */
    private String refundRemark;
	
	private String orderNo;
	
	private String loginName;
	
	private String userPhone;
	
	private String shopName;
	
	 /**
     * 实际支付金额 通过支付公司现金支付的金额
     */
    private BigDecimal realMoney;
	
	private String isRefund;
	
	/**
     * 营业执照所在省id
     */
    private Integer provinceId;
    /**
     * 营业执照所在市id
     */
    private Integer cityId;
    /**
     * 营业执照所在区县id
     */
    private Integer areaId;
    
    /**
     * 查询时间开始
     */
    private String beginTime;
    /**
     * 查询时间结束
     */
    private String endTime;
    
    
    public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getRefundRemark() {
		return refundRemark;
	}
	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}
	
	public Integer getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getAreaId() {
		return areaId;
	}
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getIsRefund() {
		return isRefund;
	}
	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}
	public BigDecimal getRealMoney() {
		return realMoney;
	}
	public void setRealMoney(BigDecimal realMoney) {
		this.realMoney = realMoney;
	}

}
