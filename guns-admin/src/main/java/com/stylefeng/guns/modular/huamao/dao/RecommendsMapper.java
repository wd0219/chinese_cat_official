package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.BrandsCustem;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;
import com.stylefeng.guns.modular.huamao.model.Recommends;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 推荐记录表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-24
 */
public interface RecommendsMapper extends BaseMapper<Recommends> {
	Map<String,Object> selectMax();
	boolean deleteByDataId(Integer dataId);
	Map<String,Object>selectBrand(Integer dataId);
	public Integer deleteByMap (Map<String,Object> map);
	public Integer insertRrecommends (Map<String,Object> map);
	List<Map<String,Object>> selectRecomGoods(GoodsCustm goodsCustm);
	List<Map<String,Object>> selectRecommendBrands(BrandsCustem brandsCustem);

}
