package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktAreas;
import com.stylefeng.guns.modular.huamao.dao.SktAreasMapper;
import com.stylefeng.guns.modular.huamao.service.ISktAreasService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 区域表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class SktAreasServiceImpl extends ServiceImpl<SktAreasMapper, SktAreas> implements ISktAreasService {

	@Override
	public List<Map<String, Object>> selectByParentId(Integer parentId) {
		// TODO Auto-generated method stub
		return baseMapper.selectByParentId(parentId);
		
	}

	@Override
	public boolean deleteByParentId(Integer parentId) {
		// TODO Auto-generated method stub
		return baseMapper.deleteByParentId(parentId);
	}

	@Override
	public List<Map<String, Object>> selectByName(SktAreas sktAreas) {
		// TODO Auto-generated method stub
		return baseMapper.selectByName(sktAreas);
	}

	@Override
	public Integer selectMaxAreaSort() {
		// TODO Auto-generated method stub
		return baseMapper.selectMaxAreaSort();
	}

}
