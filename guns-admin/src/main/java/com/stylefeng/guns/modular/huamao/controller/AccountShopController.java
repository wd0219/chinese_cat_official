package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.AccountShop;
import com.stylefeng.guns.modular.huamao.model.AccountShopList;
import com.stylefeng.guns.modular.huamao.service.IAccountShopService;

import java.util.List;

/**
 * 商家账户控制器
 *
 * @author fengshuonan
 * @Date 2018-04-12 16:50:36
 */
@Controller
@RequestMapping("/accountShop")
public class AccountShopController extends BaseController {

	private String PREFIX = "/huamao/accountShop/";

	@Autowired
	private IAccountShopService accountShopService;

	/**
	 * 跳转到商家账户首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "accountShop.html";
	}

	/**
	 * 跳转到添加商家账户
	 */
	@RequestMapping("/accountShop_add")
	public String accountShopAdd() {
		return PREFIX + "accountShop_add.html";
	}

	/**
	 * 跳转到修改商家账户
	 */
	@RequestMapping("/accountShop_update/{accountShopId}")
	public String accountShopUpdate(@PathVariable Integer accountShopId, Model model) {
		AccountShop accountShop = accountShopService.selectById(accountShopId);
		model.addAttribute("item", accountShop);
		LogObjectHolder.me().set(accountShop);
		return PREFIX + "accountShop_edit.html";
	}

	/**
	 * 获取商家账户列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(AccountShopList accountShopList) {
		Page<AccountShopList> page = new PageFactory<AccountShopList>().defaultPage();
		List<AccountShopList> accountShopLists = accountShopService.selectAccountShopAll(page,accountShopList);
		page.setRecords(accountShopLists);
		return super.packForBT(page);
	}

	/**
	 * 新增商家账户
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(AccountShop accountShop) {
		accountShopService.insert(accountShop);
		return SUCCESS_TIP;
	}

	/**
	 * 删除商家账户
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer accountShopId) {
		accountShopService.deleteById(accountShopId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改商家账户
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(AccountShop accountShop) {
		accountShopService.updateById(accountShop);
		return SUCCESS_TIP;
	}

	/**
	 * 商家账户详情
	 */
	@RequestMapping(value = "/detail/{accountShopId}")
	@ResponseBody
	public Object detail(@PathVariable("accountShopId") Integer accountShopId) {
		return accountShopService.selectById(accountShopId);
	}
}
