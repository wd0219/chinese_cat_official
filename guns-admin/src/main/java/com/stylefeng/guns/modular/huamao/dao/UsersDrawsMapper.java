package com.stylefeng.guns.modular.huamao.dao;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.UsersDraws;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户提现表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface UsersDrawsMapper extends BaseMapper<UsersDraws> {

	// 用户提现表展示
	List<UsersDrawsList> selectUsersDrawsAll(@Param("page")Page<UsersDrawsList> page,@Param("usersDrawsList") UsersDrawsList usersDrawsList);

	// 修改审核状态
	void updateSatus(UsersDrawsList usersDrawsList);

	//批量拒绝
    Integer batchEdit(UsersDrawsList usersDrawsList);


    Integer selectUsersDrawsAllCount(@Param("usersDrawsList") UsersDrawsList usersDrawsList);
}
