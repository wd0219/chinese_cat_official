package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesCustm;

/**
 * <p>
 * 商品评价表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsAppraisesMapper extends BaseMapper<GoodsAppraises> {
	List<Map<String,Object>> selectAll(@Param("page")Page<Map<String, Object>> page, 
			@Param("goodsAppraisesCustm")GoodsAppraisesCustm goodsAppraisesCustm);
	public int selectGoodsAppraisesTotal();
	Integer selectAllCount(@Param("goodsAppraisesCustm")GoodsAppraisesCustm goodsAppraisesCustm);
}
