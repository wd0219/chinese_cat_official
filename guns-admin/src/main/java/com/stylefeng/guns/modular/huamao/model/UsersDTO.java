package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.annotations.TableField;

/**
 * 
 * @author slt
 *
 */
@SuppressWarnings("serial")
public class UsersDTO extends Users{
	/**
	 * 推荐人名称
	 */
	@TableField(exist = false)
	private String tuiName;
	/**
	 * 推荐人电话
	 */
	@TableField(exist = false)
	private String tuiPhone;
	/**
	 * 新密码
	 */
	@TableField(exist = false)
	private String newLoginPwd;
	
	/**
	 * shijian
	 */
	@TableField(exist = false)
	private String shijianTime;
	
	public String getTuiName() {
		return tuiName;
	}


	public void setTuiName(String tuiName) {
		this.tuiName = tuiName;
	}

	public String getTuiPhone() {
		return tuiPhone;
	}

	public void setTuiPhone(String tuiPhone) {
		this.tuiPhone = tuiPhone;
	}

	public String getNewLoginPwd() {
		return newLoginPwd;
	}

	public void setNewLoginPwd(String newLoginPwd) {
		this.newLoginPwd = newLoginPwd;
	}

	public String getShijianTime() {
		return shijianTime;
	}

	public void setShijianTime(String shijianTime) {
		this.shijianTime = shijianTime;
	}
	
}
