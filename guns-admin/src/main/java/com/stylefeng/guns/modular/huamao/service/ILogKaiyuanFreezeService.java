package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreeze;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreezeList;

/**
 * <p>
 * 待发开元宝营业额流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface ILogKaiyuanFreezeService extends IService<LogKaiyuanFreeze> {

	// 待发华宝营业额流水表展示
	public List<LogKaiyuanFreezeList> selectLogKaiyuanFreezeAll(Page<LogKaiyuanFreezeList> page, LogKaiyuanFreezeList logKaiyuanFreezeList);

}
