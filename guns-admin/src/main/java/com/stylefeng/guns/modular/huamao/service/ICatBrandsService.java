package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.BrandsCustem;
import com.stylefeng.guns.modular.huamao.model.CatBrands;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 分类-品牌表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface ICatBrandsService extends IService<CatBrands> {
	 

}
