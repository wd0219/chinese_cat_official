package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreMapper;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.LogScoreList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreService;

/**
 * <p>
 * 积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogScoreServiceImpl extends ServiceImpl<LogScoreMapper, LogScore> implements ILogScoreService {

	@Autowired
	private LogScoreMapper lsm;

	// 展示积分列表
	@Override
	public List<LogScoreList> selectLogScoreAll(Page<LogScoreList> page, LogScoreList logScoreList) {
		Integer count = lsm.selectLogScoreAllCount(logScoreList);
		page.setTotal(count);
		return lsm.selectLogScoreAll(page,logScoreList);
	}

}
