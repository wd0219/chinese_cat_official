package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktClass;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktClassDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商学院表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface ISktClassService extends IService<SktClass> {
    public Map<String,Object> insertSktClass(SktClass sktClass );

    SktClass selectSktClassByClassName(String className);
    public List<SktClassDTO> selectSktClassAll(Page<SktClassDTO> page, SktClassDTO sktClassDTO);
}
