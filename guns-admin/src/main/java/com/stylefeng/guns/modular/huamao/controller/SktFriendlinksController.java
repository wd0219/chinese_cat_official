package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktFriendlinks;
import com.stylefeng.guns.modular.huamao.service.ISktFriendlinksService;

/**
 * 友情链接控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 14:38:43
 */
@Controller
@RequestMapping("/sktFriendlinks")
public class SktFriendlinksController extends BaseController {

    private String PREFIX = "/huamao/sktFriendlinks/";

    @Autowired
    private ISktFriendlinksService sktFriendlinksService;

    /**
     * 跳转到友情链接首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktFriendlinks.html";
    }

    /**
     * 跳转到添加友情链接
     */
    @RequestMapping("/sktFriendlinks_add")
    public String sktFriendlinksAdd() {
        return PREFIX + "sktFriendlinks_add.html";
    }

    /**
     * 跳转到修改友情链接
     */
    @RequestMapping("/sktFriendlinks_update/{sktFriendlinksId}")
    public String sktFriendlinksUpdate(@PathVariable Integer sktFriendlinksId, Model model) {
        SktFriendlinks sktFriendlinks = sktFriendlinksService.selectById(sktFriendlinksId);
        model.addAttribute("item",sktFriendlinks);
        LogObjectHolder.me().set(sktFriendlinks);
        return PREFIX + "sktFriendlinks_edit.html";
    }

    /**
     * 获取友情链接列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<SktFriendlinks> page = new PageFactory<SktFriendlinks>().defaultPage();
        int total = sktFriendlinksService.selectCount(null);
        Page<SktFriendlinks> sktFriendlinksPage = sktFriendlinksService.selectPage(page, null);
        sktFriendlinksPage.setTotal(total);
        return super.packForBT(page);
    }

    /**
     * 新增友情链接
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktFriendlinks sktFriendlinks) {
    	sktFriendlinks.setCreateTime(new Date());
        sktFriendlinksService.insert(sktFriendlinks);
        return SUCCESS_TIP;
    }

    /**
     * 删除友情链接
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktFriendlinksId) {
        sktFriendlinksService.deleteById(sktFriendlinksId);
        return SUCCESS_TIP;
    }

    /**
     * 修改友情链接
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktFriendlinks sktFriendlinks) {
        sktFriendlinksService.updateById(sktFriendlinks);
        return SUCCESS_TIP;
    }

    /**
     * 友情链接详情
     */
    @RequestMapping(value = "/detail/{sktFriendlinksId}")
    @ResponseBody
    public Object detail(@PathVariable("sktFriendlinksId") Integer sktFriendlinksId) {
        return sktFriendlinksService.selectById(sktFriendlinksId);
    }
}
