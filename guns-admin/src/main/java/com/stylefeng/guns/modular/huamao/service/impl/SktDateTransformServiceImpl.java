package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktDateTransformMapper;
import com.stylefeng.guns.modular.huamao.model.SktDateTransform;
import com.stylefeng.guns.modular.huamao.service.ISktDateTransformService;

/**
 * <p>
 * 积分转换开元宝每日记录表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
@Service
public class SktDateTransformServiceImpl extends ServiceImpl<SktDateTransformMapper, SktDateTransform> implements ISktDateTransformService {
	@Autowired
	private SktDateTransformMapper dateTransformMapper;
	@Override
	public List<SktDateTransform> sktDateTransformfindAll(Page<SktDateTransform>page,SktDateTransform sktDateTransform) {
		 List<SktDateTransform> list = dateTransformMapper.sktDateTransformfindAll(page,sktDateTransform);
		 Integer total = dateTransformMapper.sktDateTransformfindAllCount(sktDateTransform);
		 page.setTotal(total);
		return list;
		
	}
	@Override
	public List<SktDateTransform> SktDateTransformfindAll2(Page<SktDateTransform> page,
			SktDateTransform sktDateTransform2) {
		 List<SktDateTransform> list = dateTransformMapper.sktDateTransformfindAll(page,sktDateTransform2);
		 Integer total = dateTransformMapper.sktDateTransformfindAllCount(sktDateTransform2);
		 page.setTotal(total);
		 return list;
	}

}
