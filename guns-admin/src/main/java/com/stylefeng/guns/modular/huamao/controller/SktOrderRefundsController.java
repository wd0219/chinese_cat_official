package com.stylefeng.guns.modular.huamao.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefunds;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefundsList;
import com.stylefeng.guns.modular.huamao.service.ISktOrderRefundsService;
import com.stylefeng.guns.modular.system.model.User;
import com.stylefeng.guns.modular.system.service.IUserService;

/**
 * 退款订单控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 16:37:14
 */
@Controller
@RequestMapping("/sktOrderRefunds")
public class SktOrderRefundsController extends BaseController {

    private String PREFIX = "/huamao/sktOrderRefunds/";
    @Autowired
    private ISktOrderRefundsService sktOrderRefundsService;
    @Autowired
    private IUserService userService;


    /**
     * 跳转到退款订单首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktOrderRefunds.html";
    }

    /**
     * 跳转到添加退款订单
     */
    @RequestMapping("/sktOrderRefunds_add")
    public String sktOrderRefundsAdd() {
        return PREFIX + "sktOrderRefunds_add.html";
    }
    
    /**
     * 审核页面跳转
     */
    @RequestMapping(value = "/sktOrderRefunds_toRefund/{sktOrderRefundsId}")
    public Object toRefund(@PathVariable Integer sktOrderRefundsId, Model model) {
    	model.addAttribute("sktOrderRefundsId", sktOrderRefundsId);
    	return PREFIX + "sktOrderRefunds_toRefund.html";
    }
    /**
     * 拒绝操作页面跳转
     */
    @RequestMapping(value = "/sktOrderRefunds_toRefundReject/{sktOrderRefundsId}")
    public Object toRefundReject(@PathVariable Integer sktOrderRefundsId, Model model) {
    	model.addAttribute("sktOrderRefundsId", sktOrderRefundsId);
    	return PREFIX + "sktOrderRefunds_toRefundReject.html";
    }
    /**
     * 跳转退款订单详情
     */
    @RequestMapping(value = "/sktOrderRefunds_toOrdersDetail/{sktOrderRefundsId}")
    public Object toSktOrderRefunds_toOrdersDetail(@PathVariable Integer sktOrderRefundsId, Model model) {
    	SktOrderRefunds sktOrderRefunds = sktOrderRefundsService.selectById(sktOrderRefundsId);
    	model.addAttribute("orderId", sktOrderRefunds.getOrderId());
    	return PREFIX + "sktOrderRefunds_toOrdersDetail.html";
    }
    /**
     * 查询退款订单详情
     * @param sktOrderRefundsId
     * @return
     */
    @RequestMapping(value = "/sktOrderRefunds_selectOrdersDetail")
    @ResponseBody
    public Object sktOrderRefunds_selectOrdersDetail(Integer sktOrderRefundsId) {
    	List<Map<String,Object>> list  = sktOrderRefundsService.selectOrdersDetail(sktOrderRefundsId);
    	return list.size()>0?list.get(0):null;
    }
    
    
    /**
     * 跳转到修改退款订单
     */
    @RequestMapping("/sktOrderRefunds_update/{sktOrderRefundsId}")
    public String sktOrderRefundsUpdate(@PathVariable Integer sktOrderRefundsId, Model model) {
        SktOrderRefunds sktOrderRefunds = sktOrderRefundsService.selectById(sktOrderRefundsId);
        model.addAttribute("item",sktOrderRefunds);
        LogObjectHolder.me().set(sktOrderRefunds);
        return PREFIX + "sktOrderRefunds_edit.html";
    }

    /**
     * 获取退款订单列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktOrderRefundsService.selectList(null);
    }
    
    /**
     * 获取退款订单列表
     */
    @RequestMapping(value = "/alllist")
    @ResponseBody
    public Object allList(SktOrderRefundsList sktOrderRefundsList) {
    	Page<SktOrderRefundsList> page = new PageFactory<SktOrderRefundsList>().defaultPage();
    	List<SktOrderRefundsList> list = sktOrderRefundsService.selectOrderRefundList(page,sktOrderRefundsList);
    	page.setRecords(list);
		return super.packForBT(page);
    }
    
    /**
     * 获取退款订单列表
     */
    @RequestMapping(value = "/sktOrderRefundsToRefund_load")
    @ResponseBody
    public Object sktOrderRefundsToRefundLoad(Integer sktOrderRefundsId) {
    	Map<String,Object> param = new HashMap<String,Object>();
    	param.put("id", sktOrderRefundsId);
    	Object test = sktOrderRefundsService.selectOrderRefundsToRefund(param);
    	return test;
    }  
    
    /**
     * 获取退款订单列表
     */
    @RequestMapping(value = "/sktOrderRefunds_selectGoodsDetail")
    @ResponseBody
    public Object sktOrderRefundsSelectGoodsDetail(Integer orderId) {
    	List<Map<String,Object>> list = sktOrderRefundsService.sktOrderRefundsSelectGoodsDetail(orderId);
    	return list;
    }

    /**
     * 新增退款订单
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktOrderRefunds sktOrderRefunds) {
        sktOrderRefundsService.insert(sktOrderRefunds);
        return SUCCESS_TIP;
    }

    /**
     * 删除退款订单
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktOrderRefundsId) {
        sktOrderRefundsService.deleteById(sktOrderRefundsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改退款订单
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktOrderRefunds sktOrderRefunds) {
        sktOrderRefundsService.updateById(sktOrderRefunds);
        return SUCCESS_TIP;
    }
    
    /**
     * 审核退款订单
     */
    @RequestMapping(value = "/sktOrderRefundsToRefund_submit")
    @ResponseBody
    public Object sktOrderRefundsToRefundSubmit(SktOrderRefunds sktOrderRefunds,String orderNo) {
    	String account = (String) super.getSession().getAttribute("username");
    	System.out.println("account="+account);
    	User temp = new User();
    	temp.setAccount(account);
    	User user = userService.selectOne(new EntityWrapper<>(temp));
    	Integer staffId = user.getId();
    	//return sktOrderRefundsService.updateById(sktOrderRefunds,staffId,orderNo);
        sktOrderRefundsService.agreeRefunds(sktOrderRefunds);
    	return sktOrderRefundsService.examineOrderRefundsToRefund(sktOrderRefunds, staffId, orderNo);
    }
    /**
     * 平台不同意退款订单
     */
    @RequestMapping(value = "/sktOrderRefundsToRefundReject_submit")
    @ResponseBody
    public Object sktOrderRefundsToRefundRejectSubmit(SktOrderRefunds sktOrderRefunds) {
          sktOrderRefundsService.notAgreeRefunds(sktOrderRefunds);
          return sktOrderRefundsService.updateById(sktOrderRefunds);
    }

    /**
     * 退款订单详情
     */
    @RequestMapping(value = "/detail/{sktOrderRefundsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktOrderRefundsId") Integer sktOrderRefundsId) {
        return sktOrderRefundsService.selectById(sktOrderRefundsId);
    }
}
