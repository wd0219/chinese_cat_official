package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.CatBrands;
import com.stylefeng.guns.modular.huamao.service.ICatBrandsService;

/**
 * 品牌分类控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:25:23
 */
@Controller
@RequestMapping("/catBrands")
public class CatBrandsController extends BaseController {

    private String PREFIX = "/huamao/catBrands/";

    @Autowired
    private ICatBrandsService catBrandsService;

    /**
     * 跳转到品牌分类首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "catBrands.html";
    }

    /**
     * 跳转到添加品牌分类
     */
    @RequestMapping("/catBrands_add")
    public String catBrandsAdd() {
        return PREFIX + "catBrands_add.html";
    }

    /**
     * 跳转到修改品牌分类
     */
    @RequestMapping("/catBrands_update/{catBrandsId}")
    public String catBrandsUpdate(@PathVariable Integer catBrandsId, Model model) {
        CatBrands catBrands = catBrandsService.selectById(catBrandsId);
        model.addAttribute("item",catBrands);
        LogObjectHolder.me().set(catBrands);
        return PREFIX + "catBrands_edit.html";
    }

    /**
     * 获取品牌分类列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return catBrandsService.selectList(null);
    }

    /**
     * 新增品牌分类
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(CatBrands catBrands) {
        catBrandsService.insert(catBrands);
        return SUCCESS_TIP;
    }

    /**
     * 删除品牌分类
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer catBrandsId) {
        catBrandsService.deleteById(catBrandsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改品牌分类
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(CatBrands catBrands) {
        catBrandsService.updateById(catBrands);
        return SUCCESS_TIP;
    }

    /**
     * 品牌分类详情
     */
    @RequestMapping(value = "/detail/{catBrandsId}")
    @ResponseBody
    public Object detail(@PathVariable("catBrandsId") Integer catBrandsId) {
        return catBrandsService.selectById(catBrandsId);
    }
}
