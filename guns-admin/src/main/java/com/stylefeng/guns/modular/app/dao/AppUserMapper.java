package com.stylefeng.guns.modular.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.Account;
import com.stylefeng.guns.modular.huamao.model.AccountList;
import com.stylefeng.guns.modular.huamao.model.Users;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 会员账户表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-11
 */
public interface AppUserMapper extends BaseMapper<Users> {
	//根据登录名查询会员集合
	public List<Users> findListByLoginName(@Param("loginName")String LoginName);

}
