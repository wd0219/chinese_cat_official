package com.stylefeng.guns.modular.huamao.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.stylefeng.guns.core.shiro.ShiroUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholder;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsStockholderService;

/**
 * 代理公司股东控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 09:34:46
 */
@Controller
@RequestMapping("/sktAgentsStockholder")
public class SktAgentsStockholderController extends BaseController {

    private String PREFIX = "/huamao/sktAgentsStockholder/";

    @Autowired
    private ISktAgentsStockholderService sktAgentsStockholderService;

    /**
     * 跳转到代理公司股东首页
     */
    @RequestMapping("")
    public String index(@RequestParam(required = false) String agentId, Model model) {
    	model.addAttribute("agentId",agentId);
        return PREFIX + "sktAgentsStockholder.html";
    }

    /**
     * 跳转到添加代理公司股东
     */
    @RequestMapping("/sktAgentsStockholder_add")
    public String sktAgentsStockholderAdd() {
        return PREFIX + "sktAgentsStockholder_add.html";
    }
    
    /**
     * 跳转到添加代理公司股东降级
     */
    @RequestMapping("/sktAgentsStockholder_down")
    public String sktAgentsStockholderDown(@RequestParam(value="shId") Integer shId,
    		@RequestParam(value="trueName") String trueName,
    		@RequestParam(value="type") String type, Model model
    		) {
    	Map<String,Object> map = new HashMap<String,Object>();
    	map.put("shId", shId);
    	map.put("trueName", trueName);
    	map.put("type", type);
    	model.addAttribute("item",map);
        return PREFIX + "sktAgentsStockholder_down.html";
    }
    /**
     * 跳转到添加代理公司股东升级
     */
    @RequestMapping("/sktAgentsStockholder_up")
    public String sktAgentsStockholderUp(@RequestParam(value="shId") Integer shId,
    		@RequestParam(value="trueName") String trueName,
    		@RequestParam(value="agentId") String agentId,
    		@RequestParam(value="type") String type, Model model
    		) {
    	Map<String,Object> map = new HashMap<String,Object>();
    	map.put("shId", shId);
    	map.put("trueName", trueName);
    	map.put("type", type);
    	map.put("agentId", agentId);
    	model.addAttribute("item",map);
        return PREFIX + "sktAgentsStockholder_up.html";
    }
    
    /**
     * 跳转到添加代理公司股东转移
     */
    @RequestMapping("/sktAgentsStockholder_zhuanyi")
    public String sktAgentsStockholderZhuanYi(@RequestParam(value="shId") Integer shId,
    		@RequestParam(value="trueName") String trueName,
    		@RequestParam(value="userPhone") String userPhone,
            @RequestParam(value="userId") Integer userId,Model model
    		) {
    	Map<String,Object> map = new HashMap<String,Object>();
    	map.put("shId", shId);
    	map.put("trueName", trueName);
    	map.put("userPhone", userPhone);
        map.put("userId", userId);
    	model.addAttribute("item",map);
        return PREFIX + "sktAgentsStockholder_zhuanyi.html";
    }
    /**
     * 查看用户电话姓名
     */
    @RequestMapping("/selectNameOrPhone")
    @ResponseBody
    public Object selectNameOrPhone(@RequestParam String nameOrPhone) {
    	Map<String,Object> map = sktAgentsStockholderService.selectNameOrPhone(nameOrPhone);
    	String jsonString = JSONUtils.toJSONString(map);;  
        return jsonString;
    }
    
    /**
     * 跳转到修改代理公司股东
     */
    @RequestMapping("/sktAgentsStockholder_update/{sktAgentsStockholderId}")
    public String sktAgentsStockholderUpdate(@PathVariable Integer sktAgentsStockholderId, Model model) {
        SktAgentsStockholder sktAgentsStockholder = sktAgentsStockholderService.selectById(sktAgentsStockholderId);
        model.addAttribute("item",sktAgentsStockholder);
        LogObjectHolder.me().set(sktAgentsStockholder);
        return PREFIX + "sktAgentsStockholder_edit.html";
    }

    /**
     * 获取代理公司股东列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktAgentsStockholderDTO sktAgentsStockholderDTO) {
    	Page<SktAgentsStockholderDTO> page = new PageFactory<SktAgentsStockholderDTO>().defaultPage();
        List<SktAgentsStockholderDTO> list = sktAgentsStockholderService.showSktStcokHolderInfo(page,sktAgentsStockholderDTO);
        page.setRecords(list);
		return super.packForBT(page);
    }

    /**
     * 新增代理公司股东
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktAgentsStockholderDTO sktAgentsStockholder) {
        Map<String, Object> map=new HashMap<>();
        Integer userId=sktAgentsStockholder.getUserId();
        Integer alevel=sktAgentsStockholder.getAlevel();
        Integer  provinceId=sktAgentsStockholder.getProvince();
        Integer cityId=   sktAgentsStockholder.getCitys();
        Integer  areaId=sktAgentsStockholder.getCounty();
        Integer type=sktAgentsStockholder.getType();
        //获取操作员id
        ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
        if(shiroUser==null){
            map.put("code","00");
            map.put("msg","登录超时请重新登录");
            return map;
        }
        Integer  shiroUserId=shiroUser.getId();
       map = sktAgentsStockholderService.addShareHolder(userId, alevel, provinceId, cityId, areaId, type,shiroUserId);
        if ("01".equals(map.get("code").toString())){
            return SUCCESS_TIP;
        }else {
            return map;
        }
        //   String result = sktAgentsStockholderService.insertSktStockHodler(sktAgentsStockholder);
     //   return result;
    }

    /**
     * 删除代理公司股东
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktAgentsStockholderId) {
        sktAgentsStockholderService.deleteById(sktAgentsStockholderId);
        return SUCCESS_TIP;
    }

    /**
     * 修改代理公司股东
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktAgentsStockholder sktAgentsStockholder) {
        Integer type=sktAgentsStockholder.getUserRe();//变化类型   1  降级  2 升级
        Integer  typeRe=sktAgentsStockholder.getType();//变化后的类型
        Integer shId=sktAgentsStockholder.getShId();//股东id
        Map<String, Object> map = sktAgentsStockholderService.change(type, shId, typeRe);
        if ("01".equals(map.get("code").toString())){
            return SUCCESS_TIP;
        }else {
            return map;
        }
//        String result = sktAgentsStockholderService.selectSktStcokManger(sktAgentsStockholder.getAgentId(),sktAgentsStockholder.getType());
//    	if("SUCCESS".equals(result)){
//    		sktAgentsStockholderService.updateById(sktAgentsStockholder);
//    		return SUCCESS_TIP;
//    	}else{
//    		return result;
//    	}
    }
    
    /**
     * 修改代理公司股东
     */
    @RequestMapping(value = "/updateSktA")
    @ResponseBody
    public Object updateSktA(SktAgentsStockholder sktAgentsStockholder) {
        Integer transferUserId= sktAgentsStockholder.getUserId();
        Integer shId=sktAgentsStockholder.getUserRe();
        Map<String, Object> map = sktAgentsStockholderService.agentTransfer(shId, transferUserId);
        return map;
        //	sktAgentsStockholderService.updateSktAgentsStockHolder(sktAgentsStockholder);
//		return SUCCESS_TIP;
    }
    
    /**
     * 代理公司股东详情
     */
    @RequestMapping(value = "/detail/{sktAgentsStockholderId}")
    @ResponseBody
    public Object detail(@PathVariable("sktAgentsStockholderId") Integer sktAgentsStockholderId) {
        return sktAgentsStockholderService.selectById(sktAgentsStockholderId);
    }
}
