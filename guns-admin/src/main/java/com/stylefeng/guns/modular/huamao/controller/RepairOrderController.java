package com.stylefeng.guns.modular.huamao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.RepairOrder;
import com.stylefeng.guns.modular.huamao.model.RepairOrderList;
import com.stylefeng.guns.modular.huamao.service.IRepairOrderService;

/**
 * 第三方支付补单记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-26 09:36:49
 */
@Controller
@RequestMapping("/repairOrder")
public class RepairOrderController extends BaseController {

	private String PREFIX = "/huamao/repairOrder/";

	@Autowired
	private IRepairOrderService repairOrderService;

	/**
	 * 跳转到第三方支付补单记录首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "repairOrder.html";
	}

	/**
	 * 跳转到添加第三方支付补单记录
	 */
	@RequestMapping("/repairOrder_add")
	public String repairOrderAdd() {
		return PREFIX + "repairOrder_add.html";
	}

	/**
	 * 跳转到修改第三方支付补单记录
	 */
	@RequestMapping("/repairOrder_update/{repairOrderId}")
	public String repairOrderUpdate(@PathVariable Integer repairOrderId, Model model) {
		RepairOrder repairOrder = repairOrderService.selectById(repairOrderId);
		model.addAttribute("item", repairOrder);
		LogObjectHolder.me().set(repairOrder);
		return PREFIX + "repairOrder_edit.html";
	}

	/**
	 * 获取第三方支付补单记录列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(RepairOrderList repairOrderList) {
		return repairOrderService.selectRepairOrderAll(repairOrderList);
	}

	/**
	 * 新增第三方支付补单记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(RepairOrder repairOrder) {
		repairOrderService.insert(repairOrder);
		return SUCCESS_TIP;
	}

	/**
	 * 删除第三方支付补单记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer repairOrderId) {
		repairOrderService.deleteById(repairOrderId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改第三方支付补单记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(RepairOrder repairOrder) {
		repairOrderService.updateById(repairOrder);
		return SUCCESS_TIP;
	}

	/**
	 * 第三方支付补单记录详情
	 */
	@RequestMapping(value = "/detail/{repairOrderId}")
	@ResponseBody
	public Object detail(@PathVariable("repairOrderId") Integer repairOrderId) {
		return repairOrderService.selectById(repairOrderId);
	}
}
