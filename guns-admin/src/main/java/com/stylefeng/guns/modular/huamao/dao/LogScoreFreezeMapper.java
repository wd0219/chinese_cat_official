package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreeze;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreezeList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 待发积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogScoreFreezeMapper extends BaseMapper<LogScoreFreeze> {

	// 展示代发积分列表
	List<LogScoreFreezeList> selectScoreFreezeList(@Param("page")Page<LogScoreFreezeList> page, @Param("logScoreFreezeList") LogScoreFreezeList logScoreFreezeList);

    Integer selectScoreFreezeListCount(@Param("logScoreFreezeList") LogScoreFreezeList logScoreFreezeList);
}
