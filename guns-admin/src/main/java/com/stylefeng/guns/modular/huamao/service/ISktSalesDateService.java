package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktSalesDate;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商家每日营业额统计表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
public interface ISktSalesDateService extends IService<SktSalesDate> {
	
	/*
	 * 展示全部
	 * */
	public List<SktSalesDate> sktSalesDatefindAll(Page<SktSalesDate> page, SktSalesDate sktSalesDate);
}
