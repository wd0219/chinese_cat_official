package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商品规则申请表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-08-23
 */
@TableName("skt_spec_apply")
public class SktSpecApply extends Model<SktSpecApply> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 最后一级商品分类ID
     */
    private Integer goodsCatId;
    /**
     * 商品分类路径
     */
    private String goodsCatPath;
    /**
     * 类型名称
     */
    private String catName;
    /**
     * 0申请中  1已通过  -1已拒绝
     */
    private Integer applicationStatus;
    /**
     * 审核人ID
     */
    private Integer staffId;
    /**
     * 审核拒绝原因
     */
    private String refuseDesc;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsCatId() {
        return goodsCatId;
    }

    public void setGoodsCatId(Integer goodsCatId) {
        this.goodsCatId = goodsCatId;
    }

    public String getGoodsCatPath() {
        return goodsCatPath;
    }

    public void setGoodsCatPath(String goodsCatPath) {
        this.goodsCatPath = goodsCatPath;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Integer getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(Integer applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getRefuseDesc() {
        return refuseDesc;
    }

    public void setRefuseDesc(String refuseDesc) {
        this.refuseDesc = refuseDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktSpecApply{" +
        "id=" + id +
        ", goodsCatId=" + goodsCatId +
        ", goodsCatPath=" + goodsCatPath +
        ", catName=" + catName +
        ", applicationStatus=" + applicationStatus +
        ", staffId=" + staffId +
        ", refuseDesc=" + refuseDesc +
        ", createTime=" + createTime +
        "}";
    }
}
