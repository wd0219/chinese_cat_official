package com.stylefeng.guns.modular.huamao.model;

@SuppressWarnings("serial")
public class SktAgentsDTO extends SktAgents {
	
    /**
     * 地区
     */
    private String areas;
    /**
     * 省id
     */
    private Integer province;
    /**
     * 市id
     */
    private Integer citys;
    /**
     * 区县id
     */
    private Integer county;
    /**
     * 股东完成度
     */
    private Integer gdNum;
    /**
     * 商家完成度
     */
    private Integer gxNum;
   
	public String getAreas() {
		return areas;
	}
	public void setAreas(String areas) {
		this.areas = areas;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	public Integer getCitys() {
		return citys;
	}
	public void setCitys(Integer citys) {
		this.citys = citys;
	}
	public Integer getCounty() {
		return county;
	}
	public void setCounty(Integer county) {
		this.county = county;
	}
	public Integer getGdNum() {
		return gdNum;
	}
	public void setGdNum(Integer gdNum) {
		this.gdNum = gdNum;
	}
	public Integer getGxNum() {
		return gxNum;
	}
	public void setGxNum(Integer gxNum) {
		this.gxNum = gxNum;
	}
    
}
