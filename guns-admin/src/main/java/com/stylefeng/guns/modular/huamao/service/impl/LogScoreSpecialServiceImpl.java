package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreSpecialMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecial;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecialList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreSpecialService;

/**
 * <p>
 * 待发特别积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
@Service
public class LogScoreSpecialServiceImpl extends ServiceImpl<LogScoreSpecialMapper, LogScoreSpecial>
		implements ILogScoreSpecialService {

	@Autowired
	private LogScoreSpecialMapper lssm;

	// 待发特别积分流水表展示
	@Override
	public List<LogScoreSpecialList> selectLogScoreSpecialAll(Page<LogScoreSpecialList> page, LogScoreSpecialList logScoreSpecialList) {
		Integer total = lssm.selectLogScoreSpecialAllCount(logScoreSpecialList);
		page.setTotal(total);
		return lssm.selectLogScoreSpecialAll(page,logScoreSpecialList);
	}

}
