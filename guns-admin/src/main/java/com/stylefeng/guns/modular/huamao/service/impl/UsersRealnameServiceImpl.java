package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersRealnameMapper;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;
import com.stylefeng.guns.modular.huamao.model.UsersRealnameDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersRealnameService;

/**
 * <p>
 * 实名认证信息表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
@Service
public class UsersRealnameServiceImpl extends ServiceImpl<UsersRealnameMapper, UsersRealname> implements IUsersRealnameService {

	@Autowired
	private UsersRealnameMapper usersRealnameMapper;
	
	@Override
	public List<UsersRealnameDTO> showRenZhengInfo(Page<UsersRealnameDTO> page,UsersRealnameDTO usersRealname) {
		List<UsersRealnameDTO> list = usersRealnameMapper.showRenZhengInfo(page,usersRealname);
		Integer total = usersRealnameMapper.showRenZhengInfoCount(usersRealname);
		page.setTotal(total);
		return list;
	}

	@Override
	public UsersRealnameDTO showRenZhengXiangQing(Integer realId) {
		UsersRealnameDTO usersRealnameDTO = usersRealnameMapper.showRenZhengXiangQing(realId);
		return usersRealnameDTO;
	}

	@Override
	public Map<String,Object> selectUserCardId(Integer userId) {
		Map<String,Object> map = usersRealnameMapper.selectUserCardId(userId);
		return map;
	}

}
