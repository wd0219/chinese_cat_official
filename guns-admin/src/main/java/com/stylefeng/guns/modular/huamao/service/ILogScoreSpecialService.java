package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecial;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecialList;

/**
 * <p>
 * 待发特别积分流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface ILogScoreSpecialService extends IService<LogScoreSpecial> {

	// 待发特别积分流水表展示
	public List<LogScoreSpecialList> selectLogScoreSpecialAll(Page<LogScoreSpecialList> page, LogScoreSpecialList logScoreSpecialList);

}
