package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;

/**
 * <p>
 * 购买牌匾 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
public interface SktBuyplaqueMapper extends BaseMapper<SktBuyplaque> {
	
	/*
	 * 展示全部
	 * */
	public List<SktBuyplaque> sktBuyplaquefindall(@Param("page")Page<SktBuyplaque> page, 
			@Param("buyplaque")SktBuyplaque buyplaque);
	public Integer sktBuyplaquefindallCount(@Param("buyplaque")SktBuyplaque buyplaque);
	
	public void sktBuyplaqueUpdate(SktBuyplaque buyplaque);
	
	public void sktBuyplaqueUpdate2(SktBuyplaque buyplaque);

}
