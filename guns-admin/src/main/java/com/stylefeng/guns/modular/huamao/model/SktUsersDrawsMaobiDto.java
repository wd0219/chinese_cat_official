package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;

public class SktUsersDrawsMaobiDto extends SktUsersDrawsMaobi{
	/**
	 * 手机号
	 */
	private String userPhone;
	/**
	 * 用户昵称
	 */
	private String loginName;
	/**
	 * 真实姓名
	 */
	private String trueName;
	/**
	 * 身份证
	 */
	private String cardID;
	/**
	 * 银行卡号
	 */
	private String accNo;
	/**
	 * 账户华宝
	 */
	private BigDecimal kaiyuan;
	/**
	 * 查询创建时的开始时间
	 */
	private String cbeginTime;
	/**
	 * 查询创建时的结束时间
	 */
	private String cendTime;
	/**
	 * 查询预计到账时的开始时间
	 */
	private String ybeginTime;
	/**
	 * 查询预计到账时的结束时间
	 */
	private String yendTime;
	
	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getCardID() {
		return cardID;
	}

	public void setCardID(String cardID) {
		this.cardID = cardID;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getCbeginTime() {
		return cbeginTime;
	}

	public void setCbeginTime(String cbeginTime) {
		this.cbeginTime = cbeginTime;
	}

	public String getCendTime() {
		return cendTime;
	}

	public void setCendTime(String cendTime) {
		this.cendTime = cendTime;
	}

	public String getYbeginTime() {
		return ybeginTime;
	}

	public void setYbeginTime(String ybeginTime) {
		this.ybeginTime = ybeginTime;
	}

	public String getYendTime() {
		return yendTime;
	}

	public void setYendTime(String yendTime) {
		this.yendTime = yendTime;
	}

	public BigDecimal getKaiyuan() {
		return kaiyuan;
	}

	public void setKaiyuan(BigDecimal kaiyuan) {
		this.kaiyuan = kaiyuan;
	}
}
