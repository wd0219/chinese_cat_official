package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktFriendlinks;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 友情连接表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface SktFriendlinksMapper extends BaseMapper<SktFriendlinks> {

}
