package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogThirdpay;
import com.stylefeng.guns.modular.huamao.model.LogThirdpayList;
import com.stylefeng.guns.modular.huamao.service.ILogThirdpayService;

import java.util.List;

/**
 * 第三方支付记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 11:43:04
 */
@Controller
@RequestMapping("/logThirdpay")
public class LogThirdpayController extends BaseController {

	private String PREFIX = "/huamao/logThirdpay/";

	@Autowired
	private ILogThirdpayService logThirdpayService;

	/**
	 * 跳转到第三方支付记录首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "logThirdpay.html";
	}

	/**
	 * 跳转到添加第三方支付记录
	 */
	@RequestMapping("/logThirdpay_add")
	public String logThirdpayAdd() {
		return PREFIX + "logThirdpay_add.html";
	}

	/**
	 * 跳转到修改第三方支付记录
	 */
	@RequestMapping("/logThirdpay_update/{logThirdpayId}")
	public String logThirdpayUpdate(@PathVariable Integer logThirdpayId, Model model) {
		LogThirdpay logThirdpay = logThirdpayService.selectById(logThirdpayId);
		model.addAttribute("item", logThirdpay);
		LogObjectHolder.me().set(logThirdpay);
		return PREFIX + "logThirdpay_edit.html";
	}

	/**
	 * 获取第三方支付记录列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(LogThirdpayList logThirdpayList) {
		Page<LogThirdpayList> page = new PageFactory<LogThirdpayList>().defaultPage();
		List<LogThirdpayList> logThirdpayLists = logThirdpayService.slectLogThirdpayAll(page,logThirdpayList);
		page.setRecords(logThirdpayLists);
		return super.packForBT(page);
		// return logThirdpayService.selectList(null);
	}

	/**
	 * 新增第三方支付记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogThirdpay logThirdpay) {
		logThirdpayService.insert(logThirdpay);
		return SUCCESS_TIP;
	}

	/**
	 * 删除第三方支付记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logThirdpayId) {
		logThirdpayService.deleteById(logThirdpayId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改第三方支付记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogThirdpay logThirdpay) {
		logThirdpayService.updateById(logThirdpay);
		return SUCCESS_TIP;
	}

	/**
	 * 第三方支付记录详情
	 */
	@RequestMapping(value = "/detail/{logThirdpayId}")
	@ResponseBody
	public Object detail(@PathVariable("logThirdpayId") Integer logThirdpayId) {
		return logThirdpayService.selectById(logThirdpayId);
	}
}
