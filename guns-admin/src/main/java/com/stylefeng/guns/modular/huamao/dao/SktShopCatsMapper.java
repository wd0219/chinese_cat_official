package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktShopCats;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 店铺分类表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-29
 */
public interface SktShopCatsMapper extends BaseMapper<SktShopCats> {

}
