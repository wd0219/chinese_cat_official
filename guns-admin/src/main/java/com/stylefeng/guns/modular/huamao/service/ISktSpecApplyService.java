package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktSpecApply;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktSpecApplyDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规则申请表 服务类
 * </p>
 *
 * @author py
 * @since 2018-08-23
 */
public interface ISktSpecApplyService extends IService<SktSpecApply> {
    /**
     * 页面显示
     * @param page
     * @param sktSpecApplyDto
     * @return
     */
    public List<SktSpecApplyDto> showSpecApplyInfo (Page<SktSpecApplyDto> page, SktSpecApplyDto sktSpecApplyDto);

    public SktSpecApplyDto findSktSpecApplyById(SktSpecApplyDto sktSpecApplyDto);

    public Map<String,String> updateSktSpecApplyById(SktSpecApply sktSpecApply);

}
