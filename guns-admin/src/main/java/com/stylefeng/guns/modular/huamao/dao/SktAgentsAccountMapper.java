package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccount;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccountDTO;

/**
 * <p>
 * 代理公司账户表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-18
 */
public interface SktAgentsAccountMapper extends BaseMapper<SktAgentsAccount> {
	/**
	 * 页面显示
	 * @param page 
	 * @param sktAgentsAccountDTO
	 * @return
	 */
	public List<SktAgentsAccountDTO> showAgentsAccountInfo (@Param("page")Page<SktAgentsAccountDTO> page, 
			@Param("sktAgentsAccountDTO")SktAgentsAccountDTO sktAgentsAccountDTO);
	public Integer showAgentsAccountInfoCount(@Param("sktAgentsAccountDTO")SktAgentsAccountDTO sktAgentsAccountDTO);

	SktAgentsAccount findAgentAccountById(Integer agentId);


}
