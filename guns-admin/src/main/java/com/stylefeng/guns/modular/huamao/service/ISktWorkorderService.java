package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktWorkorder;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 工单表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
public interface ISktWorkorderService extends IService<SktWorkorder> {
	
	public List<SktWorkorder> sktWorkorderfindAll(Page<SktWorkorder> page, SktWorkorder sktWorkorder);
	
	public List<SktWorkorder> sktWorkorderfindup(SktWorkorder sktWorkorder);
}
