package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 开元宝流水表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@TableName("skt_log_kaiyuan")
public class LogKaiyuan extends Model<LogKaiyuan> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 开元宝类型 1转换获得 2赎回拒绝退还 3赎回拒绝的手续费和税率的退还 4待发开元宝账户转入 5消费退款 31线上商城消费支出 32消费的手续费和税率的支出 33赎回支出  34赎回的手续费和税率 35购买库存积分支出 36购买库存积分的手续费和税率支出 37线下商城消费支出 38购买牌匾 39购买牌匾的手续费和税率
     */
    private Integer type;
    /**
     * 发起用户ID 0为平台
     */
    private Integer fromId;
    /**
     * 目标用户ID 0为平台
     */
    private Integer userId;
    /**
     * 对应订单号
     */
    private String orderNo;
    /**
     * 操作前的金额
     */
    private BigDecimal preKaiyuan;
    /**
     * 流水标志 -1减少 1增加
     */
    private Integer kaiyuanType;
    /**
     * 金额
     */
    private BigDecimal kaiyuan;
    /**
     * 备注
     */
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreKaiyuan() {
        return preKaiyuan;
    }

    public void setPreKaiyuan(BigDecimal preKaiyuan) {
        this.preKaiyuan = preKaiyuan;
    }

    public Integer getKaiyuanType() {
        return kaiyuanType;
    }

    public void setKaiyuanType(Integer kaiyuanType) {
        this.kaiyuanType = kaiyuanType;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogKaiyuan{" +
        "id=" + id +
        ", type=" + type +
        ", fromId=" + fromId +
        ", userId=" + userId +
        ", orderNo=" + orderNo +
        ", preKaiyuan=" + preKaiyuan +
        ", kaiyuanType=" + kaiyuanType +
        ", kaiyuan=" + kaiyuan +
        ", remark=" + remark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
