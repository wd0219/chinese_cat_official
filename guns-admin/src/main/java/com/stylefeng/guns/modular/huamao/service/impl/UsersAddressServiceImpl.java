package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersAddressMapper;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.model.UsersAddressDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersAddressService;

/**
 * <p>
 * 会员地址表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
@Service
public class UsersAddressServiceImpl extends ServiceImpl<UsersAddressMapper, UsersAddress> implements IUsersAddressService {
	@Autowired
	private UsersAddressMapper usersAddressMapper;
	
	public List<UsersAddressDTO> showUserAddressInfo(Page<UsersAddressDTO> page,UsersAddressDTO usersAddressDTO){
		List<UsersAddressDTO> list = usersAddressMapper.showUserAddressInfo(page,usersAddressDTO);
		Integer total = usersAddressMapper.showUserAddressInfoCount(usersAddressDTO);
		page.setTotal(total);
		return list;
	}
}
