package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 点赞用户表(预留表)
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
@TableName("skt_pointuser")
public class SktPointuser extends Model<SktPointuser> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 点赞视频Id
     */
    private Integer classId;
    /**
     * 点赞用户Id
     */
    private Integer userId;
    /**
     * 点赞时间
     */
    private Date creatTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktPointuser{" +
        "id=" + id +
        ", classId=" + classId +
        ", userId=" + userId +
        ", creatTime=" + creatTime +
        "}";
    }
}
