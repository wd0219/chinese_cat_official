package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogStockscoreMapper;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;
import com.stylefeng.guns.modular.huamao.model.LogStockscoreList;
import com.stylefeng.guns.modular.huamao.service.ILogStockscoreService;

/**
 * <p>
 * 库存积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
@Service
public class LogStockscoreServiceImpl extends ServiceImpl<LogStockscoreMapper, LogStockscore>
		implements ILogStockscoreService {

	@Autowired
	private LogStockscoreMapper lsm;

	// 库存积分流水表展示
	@Override
	public List<LogStockscoreList> selectLogStockScoreAll(Page<LogStockscoreList> page, LogStockscoreList logStockscoreList) {
		Integer total = lsm.selectLogStockScoreAllCount(logStockscoreList);
		page.setTotal(total);
		return lsm.selectLogStockScoreAll(page,logStockscoreList);
	}

}
