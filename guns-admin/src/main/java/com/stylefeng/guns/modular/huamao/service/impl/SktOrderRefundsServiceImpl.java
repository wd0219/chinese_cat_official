package com.stylefeng.guns.modular.huamao.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.modular.huamao.common.CacheUtil;
import com.stylefeng.guns.modular.huamao.dao.*;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.common.CacheUtil;
import com.stylefeng.guns.modular.huamao.dao.AccountMapper;
import com.stylefeng.guns.modular.huamao.dao.AccountOperationMapper;
import com.stylefeng.guns.modular.huamao.dao.AccountShopMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsSpecsMapper;
import com.stylefeng.guns.modular.huamao.dao.LogCashMapper;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanMapper;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanTurnoverMapper;
import com.stylefeng.guns.modular.huamao.dao.LogScoreMapper;
import com.stylefeng.guns.modular.huamao.dao.LogStockscoreMapper;
import com.stylefeng.guns.modular.huamao.dao.SktOrderGoodsMapper;
import com.stylefeng.guns.modular.huamao.dao.SktOrderRefundsMapper;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersExtensionMapper;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersMapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopsMapper;
import com.stylefeng.guns.modular.huamao.model.Account;
import com.stylefeng.guns.modular.huamao.model.AccountShop;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsSpecs;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnover;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefunds;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefundsList;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktOrdersExtension;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IAccountShopService;
import com.stylefeng.guns.modular.huamao.service.IMessagesService;
import com.stylefeng.guns.modular.huamao.service.ISktOrderRefundsService;

/**
 * <p>
 * 订单退款记录表 服务实现类
 * </p>
 *
 * @author caody123
 * @since 2018-04-19
 */
@Service
public class SktOrderRefundsServiceImpl extends ServiceImpl<SktOrderRefundsMapper, SktOrderRefunds> implements ISktOrderRefundsService {
	
	@Autowired
	SktOrderRefundsMapper sorm;

	@Autowired
	private GoodsSpecsMapper goodsSpecsMapper;

	@Autowired
	private IAccountService accountService;

	@Autowired
	private LogKaiyuanTurnoverMapper logKaiyuanTurnoverMapper;

	@Autowired
	private LogScoreMapper logScoreMapper;

	@Autowired
	private SktShopsMapper sktShopsMapper;

	@Autowired
	private SktOrdersExtensionMapper sktOrdersExtensionMapper;

	@Autowired
	private GoodsMapper goodsMapper;

	@Autowired
	private IAccountShopService accountShopService;

	@Autowired
	private LogCashMapper logCashMapper;

	@Autowired
	private LogKaiyuanMapper logKaiyuanMapper;

	@Autowired
	private LogStockscoreMapper logStockscoreMapper;

	@Autowired
	SktOrdersMapper som;
	
	@Autowired
	AccountMapper am;

	@Autowired
	AccountShopMapper accountShopMapper;

	@Autowired
	private SktOrderGoodsMapper sktOrderGoodsMapper;
	
	@Autowired
	AccountOperationMapper aom;
	@Autowired
	private IMessagesService messagesService;

	@Autowired
	private CacheUtil cacheUtil;

	@Override
	public List<SktOrderRefundsList> selectOrderRefundList(Page<SktOrderRefundsList> page,SktOrderRefundsList sktOrderRefundsList) {
		List<SktOrderRefundsList> list = sorm.selectOrderRefundsList(page,sktOrderRefundsList);
		Integer total = sorm.selectOrderRefundsListCount(sktOrderRefundsList);
		page.setTotal(total);
		return list;
	}

	@Override		
	public Object selectOrderRefundsToRefund(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return sorm.selectOrderRefundsToRefund(param);
	}

	@Override
	@Transactional
	public Object examineOrderRefundsToRefund(SktOrderRefunds sktOrderRefunds,Integer staffId,String orderNo) {
		
		
		try {
			 /**
	         *退单成功，更新 退单表相关状态		
	         */
			Integer integer1 = sorm.updateById(sktOrderRefunds);
			if(integer1 == 0){
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			}
			/**
	 		 * 退单成功，更新 订单表相关状态
	 		 */
			SktOrders sktOrders = new SktOrders();
			sktOrders.setOrderId(sktOrderRefunds.getOrderId());
			sktOrders.setIsRefund(1);
			Integer integer2 = som.updateById(sktOrders);
			if(integer2 == 0){
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			}

			/**
             * 判断支付类型，给用户添加相应金额
             * 支付方式支付方式 1:线下现金支付 2 现金账户支付
             * 3开元宝支付 4第三方支付 5混合支付 6开元宝营业额
             */

            //订单信息
            SktOrders sktOrders1 = som.selectById(sktOrderRefunds.getOrderId());
            if(sktOrders1.getPayType() == 1){
                //线下支付

            }
            if(sktOrders1.getPayType() == 2){
                //现金账户支付
                Account account = new Account();
                account.setUserId(sktOrderRefunds.getRefundTo());
                Account result= am.selectOne(account);

				/**
				 * 生成现金记录
				 */
				//实时查询账户信息
				Account account1 = new Account();
				account1.setUserId(sktOrders.getUserId());
				EntityWrapper<Account> ew = new EntityWrapper<Account>(account1);
				Account account2 = accountService.selectOne(ew);
				//如果退款金额为0不生成记录
				if(sktOrderRefunds.getBackMoney().compareTo(new BigDecimal(0)) != 0){
					this.insertLogCash(1,0,sktOrders1.getUserId(),sktOrders1.getOrderNo(),account2.getCash(),"订单【"+sktOrders1.getOrderNo()+"】退款¥"+sktOrderRefunds.getBackMoney()+"。",7,sktOrderRefunds.getBackMoney());
				}
				//判断是否有自营店支付积分，如果有就退回
				if(sktOrders1.getPayScore() != 0){
					//生成积分记录
					this.insertLogScore(0,18,sktOrders1.getOrderNo(),account2.getScore(),"订单【"+sktOrders1.getOrderNo()+"】退款¥"+sktOrders1.getPayScore()+"。",new BigDecimal(sktOrders1.getPayScore()),1,sktOrders1.getUserId());
				}

                account.setAccountId(result.getAccountId());
                account.setCash(result.getCash().add(sktOrderRefunds.getBackMoney()));
                account.setTotalCash(result.getTotalCash().add(sktOrderRefunds.getBackMoney()));
                account.setScore(result.getScore().add(new BigDecimal(sktOrders1.getPayScore())));
                account.setTotalScore(result.getTotalScore().add(new BigDecimal(sktOrders1.getPayScore())));
                //am.updateOrderRefunds(account);
				Integer integer = am.updateById(account);
				if(integer == 0){
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return false;
				}
			}
            if(sktOrders1.getPayType() == 3){
                //华宝支付
				Account account = new Account();
				account.setUserId(sktOrderRefunds.getRefundTo());
				Account result= am.selectOne(account);

				/**
				 * 生成华宝记录
				 */
				//实时查询账户信息
				Account account1 = new Account();
				account1.setUserId(sktOrders.getUserId());
				EntityWrapper<Account> ew = new EntityWrapper<Account>(account1);
				Account account2 = accountService.selectOne(ew);
				//如果退款金额为0不生成记录
				if(sktOrderRefunds.getBackMoney().compareTo(new BigDecimal(0)) != 0){
					this.insertLogKaiyuan(0,sktOrderRefunds.getBackMoney().multiply(new BigDecimal(100)),1,sktOrders1.getOrderNo(),account2.getKaiyuan(),"订单【"+sktOrders1.getOrderNo()+"】退款¥"+sktOrderRefunds.getBackMoney()+"。",5,sktOrders1.getUserId());
				}
				//判断是否有自营店支付积分，如果有就退回
				if(sktOrders1.getPayScore() != 0){
					//生成积分记录
					this.insertLogScore(0,18,sktOrders1.getOrderNo(),account2.getScore(),"订单【"+sktOrders1.getOrderNo()+"】退款¥"+sktOrders1.getPayScore()+"。",new BigDecimal(sktOrders1.getPayScore()),1,sktOrders1.getUserId());
				}

				account.setAccountId(result.getAccountId());
				account.setKaiyuan(result.getKaiyuan().add(sktOrderRefunds.getBackMoney().multiply(new BigDecimal(100))));
				account.setTotalKaiyuan(result.getTotalKaiyuan().add(sktOrderRefunds.getBackMoney().multiply(new BigDecimal(100))));
				account.setScore(result.getScore().add(new BigDecimal(sktOrders1.getPayScore())));
				account.setTotalScore(result.getTotalScore().add(new BigDecimal(sktOrders1.getPayScore())));
				//am.updateOrderRefunds(account);
				Integer integer = am.updateById(account);
				if(integer == 0){
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return false;
				}
			}
            if(sktOrders1.getPayType() == 4){
                //第三方支付

            }
            if(sktOrders1.getPayType() == 5){
                //混合支付

            }
            if(sktOrders1.getPayType() == 6){
                //华宝货款支付
				//商家账户信息
				AccountShop accountShop2 = new AccountShop();
				accountShop2.setUserId(sktOrderRefunds.getRefundTo());
				AccountShop result= accountShopMapper.selectOne(accountShop2);
				//商家对应的用户账户信息
				Account account1 = new Account();
				account1.setUserId(sktOrderRefunds.getRefundTo());
				EntityWrapper<Account> ew = new EntityWrapper<Account>(account1);
				Account account2 = accountService.selectOne(ew);

				/**
				 * 生成华宝货款记录
				 */
				//实时查询商家账户信息
				AccountShop accountShop = new AccountShop();
				accountShop.setUserId(sktOrders.getUserId());
				EntityWrapper<AccountShop> ew2 = new EntityWrapper<AccountShop>(accountShop);
				AccountShop accountShop1 = accountShopService.selectOne(ew2);
				if(accountShop1 == null){
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return false;
				}
				//如果退款金额为0不生成记录
				if(sktOrderRefunds.getBackMoney().compareTo(new BigDecimal(0)) != 0){
					this.insertLogKaiYuanTurnover(6,0,sktOrders1.getUserId(),sktOrders1.getOrderNo(),accountShop.getKaiyuanTurnover(),1,sktOrderRefunds.getBackMoney().multiply(new BigDecimal(100)),"订单【"+sktOrders1.getOrderNo()+"】退款¥"+sktOrderRefunds.getBackMoney()+"。");
				}

				//判断是否有自营店支付积分，如果有就退回
				if(sktOrders1.getPayScore() != 0){
					//生成积分记录
					this.insertLogScore(0,18,sktOrders1.getOrderNo(),account2.getScore(),"订单【"+sktOrders1.getOrderNo()+"】退款¥"+sktOrders1.getPayScore()+"。",new BigDecimal(sktOrders1.getPayScore()),1,sktOrders1.getUserId());
				}

				accountShop2.setAccountId(result.getAccountId());
				accountShop2.setKaiyuanTurnover(result.getKaiyuanTurnover().add(sktOrderRefunds.getBackMoney().multiply(new BigDecimal(100))));
				accountShop2.setTotalKaiyuanTurnover(result.getTotalKaiyuanTurnover().add(sktOrderRefunds.getBackMoney().multiply(new BigDecimal(100))));
				//am.updateOrderRefunds(account);
				accountShopMapper.updateById(accountShop2);
				account1.setScore(account2.getScore().add(new BigDecimal(sktOrders1.getPayScore())));
				account1.setTotalScore(account2.getTotalScore().add(new BigDecimal(sktOrders1.getPayScore())));
				//am.updateOrderRefunds(account);
				Integer integer = am.updateById(account1);
				if(integer == 0){
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return false;
				}
			}

			SktOrdersExtension sktOrdersExtension = new SktOrdersExtension();
            sktOrdersExtension.setOrderId(sktOrders1.getOrderId().toString());
			SktOrdersExtension sktOrdersExtension1 = sktOrdersExtensionMapper.selectOne(sktOrdersExtension);
			if(sktOrdersExtension1.getIsAutoTake() == 0 && sktOrdersExtension1.getIsSettle() == 1){
				//库存积分标识：1店铺库存积分充足 2库存不足扣营业额购买库存积分
				if(sktOrdersExtension1.getScoreFlag() == 1){
					//充足:获取冻结的库存积分，加到商家账户
					//获取商家账户   扣除冻结库存积分，添加库存积分
					//商家所对应的用户
					SktShops sktShops = sktShopsMapper.selectById(sktOrders1.getShopId());
					//实时查询商家账户
					AccountShop accountShop = new AccountShop();
					accountShop.setShopId(sktShops.getShopId());
					AccountShop accountShop1 = accountShopMapper.selectOne(accountShop);

					this.insertLogStockScore(3,0,sktShops.getUserId(),sktOrders1.getOrderNo(),accountShop1.getStockScore(),1,sktOrdersExtension1.getScore(),"商城订单退货返还");
					accountShop.setAccountId(accountShop1.getAccountId());
					accountShop.setTotalStockScore(accountShop1.getTotalStockScore().add(sktOrdersExtension1.getScore()));
					accountShop.setStockScore(accountShop1.getStockScore().add(sktOrdersExtension1.getScore()));
					Integer integer = accountShopMapper.updateById(accountShop);
					if(integer == 0){
						return false;
					}
				}else{
					//不足：将扩展表跑批状态结束  批处理标示符：1未处理 2已处理 9处理中
					sktOrdersExtension.setId(sktOrdersExtension1.getId());
					sktOrdersExtension.setScoreFlag(1);
					Integer integer = sktOrdersExtensionMapper.updateById(sktOrdersExtension);
					if(integer == 0){
						return false;
					}
				}

				//添加商品库存
				//判断商品是否有规格
				//先获取订单下所有商品id
				SktOrderGoods sktOrderGoods = new SktOrderGoods();
				sktOrderGoods.setOrderId(sktOrders1.getOrderId());
				EntityWrapper<SktOrderGoods> ew = new EntityWrapper<SktOrderGoods>(sktOrderGoods);
				List<SktOrderGoods> goodsList = sktOrderGoodsMapper.selectList(ew);
				for (SktOrderGoods sktOrderGoods1:
					 goodsList) {
					//查询商品对应的商品订单表
					//判断是否有规格
					String goodsSpecId = sktOrderGoods1.getGoodsSpecId();
					if(goodsSpecId != null && "".equals("")){
						int i = Integer.parseInt(goodsSpecId);
						//如果有规格，将规格库存加回去
						if(i > 0){
							GoodsSpecs goodsSpecs = goodsSpecsMapper.selectById(goodsSpecId);
							if(goodsSpecs != null){
								//添加规格
								goodsSpecs.setSpecStock(goodsSpecs.getSpecStock() + sktOrderGoods1.getGoodsNum());
								Integer integer = goodsSpecsMapper.updateById(goodsSpecs);
								if(integer == 0){
									TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
									return false;
								}
							}
						}
					}
					//添加库存
					Goods goods = goodsMapper.selectById(sktOrderGoods1.getGoodsId());
					goods.setGoodsStock(goods.getGoodsStock() + sktOrderGoods1.getGoodsNum());
					Integer integer = goodsMapper.updateById(goods);
					if(integer == 0){
						TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
						return false;
					}

					/**
					 * 从缓存中删除本商品信息
					 */
//					String redisKey = "redis_key:stock:" + sktOrderGoods1.getGoodsId();
//					cacheUtil.remove(redisKey);
				}
			}else{
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			}

//			/**
//			 * 退单成功，增加修改账户记录
//			 */
//			AccountOperation accountOperation = new AccountOperation();
//			accountOperation.setOrderNo(orderNo);
//			accountOperation.setUserId(sktOrderRefunds.getRefundTo());
//			accountOperation.setNum(sktOrderRefunds.getBackMoney());
//			accountOperation.setStaffId(staffId);
//			accountOperation.setRemark("退单金额");
//			accountOperation.setObject(2);
//			accountOperation.setType(1);
//			accountOperation.setDataFlag(1);
//			Date d = new Date();
//			accountOperation.setCreateTime(d);
//			aom.insert(accountOperation);
			
			return true; 
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return false;
		}
       
		
	}

	@Override
	public List<Map<String, Object>> selectOrdersDetail(Integer sktOrderRefundsId) {
		List<Map<String, Object>> list = sorm.selectOrdersDetail(sktOrderRefundsId);
		return list;
	}

	@Override
	public List<Map<String, Object>> sktOrderRefundsSelectGoodsDetail(Integer orderId) {
		List<Map<String, Object>> list = sorm.sktOrderRefundsSelectGoodsDetail(orderId);
		return list;
	}

	public  void agreeRefunds(SktOrderRefunds sktOrderRefunds){
		SktOrders sktOrders = som.selectById(sktOrderRefunds.getOrderId());
		List<Integer> usersIds=new ArrayList<Integer>();
		usersIds.add(sktOrderRefunds.getRefundTo());
		messagesService.addMessages(1,1,usersIds,"您提交的退款订单，【"+sktOrders.getOrderNo()+"】平台同意退款",null);

	}

	public  void notAgreeRefunds(SktOrderRefunds sktOrderRefunds){
		SktOrders sktOrders = som.selectById(sktOrderRefunds.getOrderId());
		List<Integer> usersIds=new ArrayList<Integer>();
		usersIds.add(sktOrderRefunds.getRefundTo());
		messagesService.addMessages(1,1,usersIds,"您提交的退款订单，【"+sktOrders.getOrderNo()+"】平台不同意退款,原因"+sktOrderRefunds.getShopRejectReason(),null);

	}

	/**  现金流水
	 * @param cashType
	 * @param fromId
	 * @param userId
	 * @param firstNum
	 * @param preCash
	 * @param remark
	 * @param type
	 * @param cash
	 * @return
	 */
	@Transactional
	public boolean insertLogCash(Integer cashType, Integer fromId, Integer userId , String orderNo, BigDecimal preCash, String remark, Integer type, BigDecimal cash){
		LogCash logCash=new LogCash();
		logCash.setCashType(cashType);
		logCash.setCreateTime(new Date());
		logCash.setDataFlag(1);
		logCash.setFromId(fromId);
		logCash.setUserId(userId);
		// 创建订单号
//		SimpleDateFormat sfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//		String strDate = sfDate.format(new Date());
//		String num = GetOrderNum.getRandom620(2);
		logCash.setOrderNo(orderNo);
		logCash.setPreCash(preCash);
		logCash.setRemark(remark);
		logCash.setType(type);
		logCash.setCash(cash);
		int flag=logCashMapper.insert(logCash);
		if(flag==1){
			return true;
		}else{
			return false;
		}
	}

	/** 华宝流水
	 * @param fromId
	 * @param kaiyuan
	 * @param kaiyuanType
	 * @param orderNo
	 * @param preKaiyuan
	 * @param remark
	 * @param type
	 * @param userId
	 * @return
	 */
	public boolean insertLogKaiyuan(Integer fromId,BigDecimal kaiyuan,Integer kaiyuanType,String orderNo,BigDecimal preKaiyuan,String remark,Integer type,Integer userId){
		LogKaiyuan logKaiyuan=new LogKaiyuan();
		logKaiyuan.setCreateTime(new Date());
		logKaiyuan.setFromId(fromId);
		logKaiyuan.setKaiyuan(kaiyuan);
		logKaiyuan.setKaiyuanType(kaiyuanType);
//		SimpleDateFormat sfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//		String strDate = sfDate.format(new Date());
//		String num = GetOrderNum.getRandom620(2);
		logKaiyuan.setOrderNo(orderNo);
		logKaiyuan.setPreKaiyuan(preKaiyuan);
		logKaiyuan.setRemark(remark);
		logKaiyuan.setType(type);
		logKaiyuan.setUserId(userId);
		Integer insert = logKaiyuanMapper.insert(logKaiyuan);//华宝消费记录
		if(insert != 0){
			return true;
		}else{
			return false;
		}
	}

	//商家华宝货款记录
	public Integer insertLogKaiYuanTurnover(Integer type,Integer fromId,Integer userId,String orderNo,BigDecimal preKaiyuan,Integer kaiyuanType,BigDecimal kaiyuan,String remark){
		LogKaiyuanTurnover logKaiyuanTurnover = new LogKaiyuanTurnover();
		logKaiyuanTurnover.setType(type);
		logKaiyuanTurnover.setFromId(fromId);
		logKaiyuanTurnover.setUserId(userId);
		// 生成订单号
//		SimpleDateFormat sfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//		String strDate = sfDate.format(new Date());
//		String num = GetOrderNum.getRandom620(2);
		logKaiyuanTurnover.setOrderNo(orderNo);
		logKaiyuanTurnover.setPreKaiyuan(preKaiyuan);
		logKaiyuanTurnover.setKaiyuanType(kaiyuanType);
		logKaiyuanTurnover.setKaiyuan(kaiyuan);
		logKaiyuanTurnover.setRemark(remark);
		logKaiyuanTurnover.setCreateTime(new Date());
		Integer insert = logKaiyuanTurnoverMapper.insert(logKaiyuanTurnover);
		return insert;
	}

	/**   插入积分流水
	 * @param fromId
	 * @param preScore
	 * @param remark
	 * @param score
	 * @param scoreType
	 * @param userId
	 * @return
	 */
	@Transactional
	public boolean insertLogScore(int fromId,int type, String orderNo,BigDecimal preScore,String remark,BigDecimal score,int scoreType,int userId){
		LogScore logScore=new LogScore();
		logScore.setCreateTime(new Date());
		logScore.setDataFlag(1);
		logScore.setFromId(fromId);
//		SimpleDateFormat sfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//		String strDate = sfDate.format(new Date());
//		String num = GetOrderNum.getRandom620(2);
		logScore.setOrderNo(orderNo);
		logScore.setPreScore(preScore);
		logScore.setRemark(remark);
		logScore.setScore(score);
		logScore.setScoreType(scoreType);
		logScore.setType(type);
		logScore.setUserId(userId);
		Integer flag = logScoreMapper.insert(logScore);
		if(flag==1){
			return true;
		}else{
			return false;
		}
	}

	//添加库存记录
	@Transactional
	public Integer insertLogStockScore(Integer type,Integer fromId,Integer userId,String orderNo,BigDecimal preScore,Integer scoreType,BigDecimal score,String remark){
		LogStockscore logStockscore = new LogStockscore();
		logStockscore.setType(type);
		logStockscore.setFromId(fromId);
		logStockscore.setUserId(userId);
		// 生成订单号
//        SimpleDateFormat sfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//        String strDate = sfDate.format(new Date());
//        String num = GetOrderNum.getRandom620(2);
		logStockscore.setOrderNo(orderNo);
		logStockscore.setPreScore(preScore);
		logStockscore.setScoreType(scoreType);
		logStockscore.setScore(score);
		logStockscore.setRemark(remark);
		logStockscore.setCreateTime(new Date());
		Integer insert = logStockscoreMapper.insert(logStockscore);
		return insert;
	}

}
