package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.GoodsAttributes;
import com.stylefeng.guns.modular.huamao.service.IGoodsAttributesService;

/**
 * 商品属性控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:30:34
 */
@Controller
@RequestMapping("/goodsAttributes")
public class GoodsAttributesController extends BaseController {

    private String PREFIX = "/huamao/goodsAttributes/";

    @Autowired
    private IGoodsAttributesService goodsAttributesService;

    /**
     * 跳转到商品属性首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "goodsAttributes.html";
    }

    /**
     * 跳转到添加商品属性
     */
    @RequestMapping("/goodsAttributes_add")
    public String goodsAttributesAdd() {
        return PREFIX + "goodsAttributes_add.html";
    }

    /**
     * 跳转到修改商品属性
     */
    @RequestMapping("/goodsAttributes_update/{goodsAttributesId}")
    public String goodsAttributesUpdate(@PathVariable Integer goodsAttributesId, Model model) {
        GoodsAttributes goodsAttributes = goodsAttributesService.selectById(goodsAttributesId);
        model.addAttribute("item",goodsAttributes);
        LogObjectHolder.me().set(goodsAttributes);
        return PREFIX + "goodsAttributes_edit.html";
    }

    /**
     * 获取商品属性列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return goodsAttributesService.selectList(null);
    }

    /**
     * 新增商品属性
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(GoodsAttributes goodsAttributes) {
        goodsAttributesService.insert(goodsAttributes);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品属性
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer goodsAttributesId) {
        goodsAttributesService.deleteById(goodsAttributesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品属性
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(GoodsAttributes goodsAttributes) {
        goodsAttributesService.updateById(goodsAttributes);
        return SUCCESS_TIP;
    }

    /**
     * 商品属性详情
     */
    @RequestMapping(value = "/detail/{goodsAttributesId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsAttributesId") Integer goodsAttributesId) {
        return goodsAttributesService.selectById(goodsAttributesId);
    }
}
