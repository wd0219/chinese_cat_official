package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 工单表
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
@TableName("skt_workorder")
public class SktWorkorder extends Model<SktWorkorder> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "orderId", type = IdType.AUTO)
    private Integer orderId;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 类型 1:修改推荐关系 2 修改手机号码
     */
    private Integer type;
    /**
     * 修改前的值
     */
    private String preValue;
    /**
     * 修改后的值
     */
    private String afterValue;
    /**
     * 订单备注
     */
    private String orderRemarks;
    /**
     * 附件图片
     */
    private String imgs;
    /**
     * 支付方式 1:线下现金支付 2 现金账户支付 3开元宝支付 4第三方支付 5混合支付 6开元宝营业额
     */
    private Integer payType;
    /**
     * 订单总金额 (realMoney+cash+kaiyuan)
     */
    private BigDecimal totalMoney;
    /**
     * 通过支付公司现金支付的金额
     */
    private BigDecimal realMoney;
    /**
     * 平台现金账户金额
     */
    private BigDecimal cash;
    /**
     * 开元宝金额 1个开元宝=1分钱
     */
    private BigDecimal kaiyuan;
    /**
     * 使用开元宝支付的手续费和税率
     */
    private BigDecimal kaiyuanFee;
    /**
     * 处理备注 拒绝理由
     */
    private String remarks;
    /**
     * 下单时间
     */
    private String createTime;
    /**
     * 员工ID
     */
    private Integer checkStaffId;
    /**
     * 审核时间
     */
    private String checkTime;
    /**
     * 状态 0未支付 1已付款 2审核拒绝 3审核通过
     */
    private Integer status;
    /**
     * 有效标志 1有效 0删除
     */
    private Integer dataFlag;
    
    private String userName;
    
    private String imgs1;
    private String imgs2;
    private String loginName;

    public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getImgs1() {
		return imgs1;
	}

	public void setImgs1(String imgs1) {
		this.imgs1 = imgs1;
	}

	public String getImgs2() {
		return imgs2;
	}

	public void setImgs2(String imgs2) {
		this.imgs2 = imgs2;
	}

	public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPreValue() {
        return preValue;
    }

    public void setPreValue(String preValue) {
        this.preValue = preValue;
    }

    public String getAfterValue() {
        return afterValue;
    }

    public void setAfterValue(String afterValue) {
        this.afterValue = afterValue;
    }

    public String getOrderRemarks() {
        return orderRemarks;
    }

    public void setOrderRemarks(String orderRemarks) {
        this.orderRemarks = orderRemarks;
    }

    public String getImgs() {
        return imgs;
    }

    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getKaiyuanFee() {
        return kaiyuanFee;
    }

    public void setKaiyuanFee(BigDecimal kaiyuanFee) {
        this.kaiyuanFee = kaiyuanFee;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getCheckStaffId() {
        return checkStaffId;
    }

    public void setCheckStaffId(Integer checkStaffId) {
        this.checkStaffId = checkStaffId;
    }

    public String getCheckTime() {
        return checkTime;
    }

    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setCheckTime(String checkTime) {
        this.checkTime = checkTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

	@Override
	public String toString() {
		return "SktWorkorder [orderId=" + orderId + ", orderNo=" + orderNo + ", userId=" + userId + ", type=" + type
				+ ", preValue=" + preValue + ", afterValue=" + afterValue + ", orderRemarks=" + orderRemarks + ", imgs="
				+ imgs + ", payType=" + payType + ", totalMoney=" + totalMoney + ", realMoney=" + realMoney + ", cash="
				+ cash + ", kaiyuan=" + kaiyuan + ", kaiyuanFee=" + kaiyuanFee + ", remarks=" + remarks
				+ ", createTime=" + createTime + ", checkStaffId=" + checkStaffId + ", checkTime=" + checkTime
				+ ", status=" + status + ", dataFlag=" + dataFlag + ", userName=" + userName + ", imgs1=" + imgs1
				+ ", imgs2=" + imgs2 + ", loginName=" + loginName + "]";
	}

	public SktWorkorder(Integer orderId, String orderNo, Integer userId, Integer type, String preValue,
			String afterValue, String orderRemarks, String imgs, Integer payType, BigDecimal totalMoney,
			BigDecimal realMoney, BigDecimal cash, BigDecimal kaiyuan, BigDecimal kaiyuanFee, String remarks,
			String createTime, Integer checkStaffId, String checkTime, Integer status, Integer dataFlag,
			String userName, String imgs1, String imgs2, String loginName) {
		super();
		this.orderId = orderId;
		this.orderNo = orderNo;
		this.userId = userId;
		this.type = type;
		this.preValue = preValue;
		this.afterValue = afterValue;
		this.orderRemarks = orderRemarks;
		this.imgs = imgs;
		this.payType = payType;
		this.totalMoney = totalMoney;
		this.realMoney = realMoney;
		this.cash = cash;
		this.kaiyuan = kaiyuan;
		this.kaiyuanFee = kaiyuanFee;
		this.remarks = remarks;
		this.createTime = createTime;
		this.checkStaffId = checkStaffId;
		this.checkTime = checkTime;
		this.status = status;
		this.dataFlag = dataFlag;
		this.userName = userName;
		this.imgs1 = imgs1;
		this.imgs2 = imgs2;
		this.loginName = loginName;
	}

	public SktWorkorder() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
}
