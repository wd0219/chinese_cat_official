package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogDaifu;
import com.stylefeng.guns.modular.huamao.model.LogDaifuList;
import com.stylefeng.guns.modular.huamao.service.ILogDaifuService;

import java.util.List;

/**
 * 代付明细控制器
 *
 * @author fengshuonan
 * @Date 2018-04-17 14:27:52
 */
@Controller
@RequestMapping("/logDaifu")
public class LogDaifuController extends BaseController {

	private String PREFIX = "/huamao/logDaifu/";

	@Autowired
	private ILogDaifuService logDaifuService;

	/**
	 * 跳转到代付明细首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "logDaifu.html";
	}

	/**
	 * 跳转到添加代付明细
	 */
	@RequestMapping("/logDaifu_add")
	public String logDaifuAdd() {
		return PREFIX + "logDaifu_add.html";
	}

	/**
	 * 跳转到修改代付明细
	 */
	@RequestMapping("/logDaifu_update/{logDaifuId}")
	public String logDaifuUpdate(@PathVariable Integer logDaifuId, Model model) {
		LogDaifu logDaifu = logDaifuService.selectById(logDaifuId);
		model.addAttribute("item", logDaifu);
		LogObjectHolder.me().set(logDaifu);
		return PREFIX + "logDaifu_edit.html";
	}

	/**
	 * 获取代付明细列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(LogDaifuList logDaifuList) {
		Page<LogDaifuList> page = new PageFactory<LogDaifuList>().defaultPage();
		List<LogDaifuList> logDaifuLists = logDaifuService.selectLogDaifuAll(page,logDaifuList);
		page.setRecords(logDaifuLists);
		return super.packForBT(page);
		// return logDaifuService.selectList(null);
	}

	/**
	 * 新增代付明细
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogDaifu logDaifu) {
		logDaifuService.insert(logDaifu);
		return SUCCESS_TIP;
	}

	/**
	 * 删除代付明细
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logDaifuId) {
		logDaifuService.deleteById(logDaifuId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改代付明细
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogDaifu logDaifu) {
		logDaifuService.updateById(logDaifu);
		return SUCCESS_TIP;
	}

	/**
	 * 代付明细详情
	 */
	@RequestMapping(value = "/detail/{logDaifuId}")
	@ResponseBody
	public Object detail(@PathVariable("logDaifuId") Integer logDaifuId) {
		return logDaifuService.selectById(logDaifuId);
	}
}
