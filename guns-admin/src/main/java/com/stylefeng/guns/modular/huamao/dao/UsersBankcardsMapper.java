package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.model.UsersBankcardsList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户的银行卡表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface UsersBankcardsMapper extends BaseMapper<UsersBankcards> {

	// 用户的银行卡表展示
	List<UsersBankcardsList> selectUsersBankcardsAll(@Param("page")Page<UsersBankcardsList> page,@Param("usersBankcardsList") UsersBankcardsList usersBankcardsList);

	// 通过id逻辑删除
	public void deleteById2(Integer usersBankcardsId);
	/**
	 * 通过用户id查询所有银行卡
	 * @param usersBankcardsList
	 * @return
	 */
	List<UsersBankcards> selectUserBranks(UsersBankcardsList usersBankcardsList);

	Integer selectUsersBankcardsAllCount(@Param("usersBankcardsList") UsersBankcardsList usersBankcardsList);
}
