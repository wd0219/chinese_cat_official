package com.stylefeng.guns.modular.huamao.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.modular.huamao.model.SktDateTransform;
import com.stylefeng.guns.modular.huamao.service.ISktDateTransformService;

/**
 * 每日积分转化控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 14:55:19
 */
@Controller
@RequestMapping("/sktDateTransform")
public class SktDateTransformController extends BaseController {

    private String PREFIX = "/huamao/sktDateTransform/";

    @Autowired
    private ISktDateTransformService sktDateTransformService;

    /**
     * 跳转到每日积分转化首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktDateTransform.html";
    }

    /**
     * 跳转到添加每日积分转化
     */
    @RequestMapping("/sktDateTransform_add")
    public String sktDateTransformAdd() {
        return PREFIX + "sktDateTransform_add.html";
    }

    /**
     * 跳转到修改每日积分转化
     */
    @RequestMapping("/sktDateTransform_update/{sktDateTransformId}")
    public String sktDateTransformUpdate(@PathVariable Integer sktDateTransformId, Model model) {
        SktDateTransform sktDateTransform = sktDateTransformService.selectById(sktDateTransformId);
        model.addAttribute("item",sktDateTransform);
        LogObjectHolder.me().set(sktDateTransform);
        return PREFIX + "sktDateTransform_edit.html";
    }

    /**
     * 获取每日积分转化列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktDateTransform sktDateTransform) {
    	Page<SktDateTransform> page = new PageFactory<SktDateTransform>().defaultPage();
        List<SktDateTransform> list = sktDateTransformService.sktDateTransformfindAll(page,sktDateTransform);
        page.setRecords(list);
		return super.packForBT(page);
    }

    /**
     * 新增每日积分转化
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktDateTransform sktDateTransform) {
    	sktDateTransform.setTransformNum(new BigDecimal(0.000));
    	sktDateTransform.setKaiyuan(new BigDecimal(0.000));
    	sktDateTransform.setCreateTime(DateUtil.getDay());
        sktDateTransformService.insert(sktDateTransform);
        return SUCCESS_TIP;
    }

    /**
     * 删除每日积分转化
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktDateTransformId) {
        sktDateTransformService.deleteById(sktDateTransformId);
        return SUCCESS_TIP;
    }

    /**
     * 修改每日积分转化
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktDateTransform sktDateTransform) {
        sktDateTransformService.updateById(sktDateTransform);
        return SUCCESS_TIP;
    }

    /**
     * 每日积分转化详情
     */
    @RequestMapping(value = "/detail/{sktDateTransformId}")
    @ResponseBody
    public Object detail(@PathVariable("sktDateTransformId") Integer sktDateTransformId) {
        return sktDateTransformService.selectById(sktDateTransformId);
    }
}
