package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoodsList;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoodsParameter;
import com.stylefeng.guns.modular.huamao.model.SktOrderScore;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktOrdersList;

/**
 * <p>
 * 线上商城订单表 Mapper 接口
 * </p>
 *
 * @author caody123
 * @since 2018-04-14
 */
public interface SktOrdersMapper extends BaseMapper<SktOrders> {
	/**
	 * 查询订单列表
	 * @param page 
	 * @param paramMap
	 * @return
	 */
	public List<SktOrdersList> selectOrderShops(@Param("page")Page<SktOrdersList> page, 
			@Param("sktOrdersList")SktOrdersList sktOrdersList);
	public Integer selectOrderShopsCount(@Param("sktOrdersList")SktOrdersList sktOrdersList);
	/**
	 * 订单积分详情数据
	 * @param param
	 * @return
	 */
	public SktOrderScore selectOrderScore(Map<String,Object> param);
	/**
	 * 订单商品列表
	 * @param param
	 * @return
	 */
	public List<SktOrderGoodsList> selectOrderGoods(Map<String,Object> param);
	/**
	 * 订单商品参数列表
	 * @param param
	 * @return
	 */
	public List<SktOrderGoodsParameter> selectOrderGoodsParameter(Map<String,Object> param);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int selectOrderTotal();
	
}
