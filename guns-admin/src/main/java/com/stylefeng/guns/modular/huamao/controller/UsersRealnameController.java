package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;
import com.stylefeng.guns.modular.huamao.model.UsersRealnameDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersRealnameService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;

/**
 * 个人认证控制器
 *
 * @author fengshuonan
 * @Date 2018-04-14 11:55:15
 */
@Controller
@RequestMapping("/usersRealname")
public class UsersRealnameController extends BaseController {

    private String PREFIX = "/huamao/usersRealname/";

    @Autowired
    private IUsersRealnameService usersRealnameService;
    @Autowired
    private IUsersService usersService;
    /**
     * 跳转到个人认证首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "usersRealname.html";
    }

    /**
     * 跳转到添加个人认证
     */
    @RequestMapping("/usersRealname_add")
    public String usersRealnameAdd() {
        return PREFIX + "usersRealname_add.html";
    }

    /**
     * 跳转到详情
     */
    @RequestMapping("/usersRealname_XiangQing/{usersRealnameId}")
    public String usersRealnameXiangQing(@PathVariable(value="usersRealnameId") Integer usersRealnameId, Model model) {
        UsersRealnameDTO usersRealname = usersRealnameService.showRenZhengXiangQing(usersRealnameId);
        
        String cardUrl = UrlUtils.getAllUrl(usersRealname.getCardUrl());
        if(cardUrl== null || "".equals(cardUrl)){
        	 usersRealname.setCardUrl("/static/img/girl.gif");
        }else{
        	usersRealname.setCardUrl(cardUrl);
        }
        
        String allUrl = UrlUtils.getAllUrl(usersRealname.getCardBackUrl());
        if(allUrl== null || "".equals(allUrl)){
        	 usersRealname.setCardBackUrl("/static/img/girl.gif");
        }else{
        	 usersRealname.setCardBackUrl(allUrl);
        }
        String handCardUrl = UrlUtils.getAllUrl(usersRealname.getHandCardUrl());
        if(allUrl== null || "".equals(allUrl)){
        	usersRealname.setHandCardUrl("/static/img/girl.gif");
        }else{
    	   usersRealname.setHandCardUrl(handCardUrl);
        }
        
        model.addAttribute("item",usersRealname);
        LogObjectHolder.me().set(usersRealname);
        return PREFIX + "usersRealname_xiangqing.html";
    }
    
    /**
     * 跳转到编辑
     */
    @RequestMapping("/usersRealname_Update/{usersRealnameId}")
    public String usersRealnameUpdate(@PathVariable(value="usersRealnameId") Integer usersRealnameId, Model model) {
        UsersRealnameDTO usersRealname = usersRealnameService.showRenZhengXiangQing(usersRealnameId);
        String cardUrl = UrlUtils.getAllUrl(usersRealname.getCardUrl());
        if(cardUrl== null || "".equals(cardUrl)){
        	 usersRealname.setCardUrl("/static/img/girl.gif");
        }else{
        	usersRealname.setCardUrl(cardUrl);
        }
        
        String allUrl = UrlUtils.getAllUrl(usersRealname.getCardBackUrl());
        if(allUrl== null || "".equals(allUrl)){
        	 usersRealname.setCardBackUrl("/static/img/girl.gif");
        }else{
        	 usersRealname.setCardBackUrl(allUrl);
        }
        String handCardUrl = UrlUtils.getAllUrl(usersRealname.getHandCardUrl());
        if(allUrl== null || "".equals(allUrl)){
        	usersRealname.setHandCardUrl("/static/img/girl.gif");
        }else{
    	   usersRealname.setHandCardUrl(handCardUrl);
        }
        
        model.addAttribute("item",usersRealname);
        LogObjectHolder.me().set(usersRealname);
        return PREFIX + "usersRealname_edit.html";
    }

    /**
     * 获取个人认证列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(UsersRealnameDTO usersRealname) {
    	Page<UsersRealnameDTO> page = new PageFactory<UsersRealnameDTO>().defaultPage();
    	List<UsersRealnameDTO> showRenZhengInfo = usersRealnameService.showRenZhengInfo(page,usersRealname);
    	page.setRecords(showRenZhengInfo);
        return super.packForBT(page);
    }

    /**
     * 新增个人认证
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(UsersRealname usersRealname) {
        usersRealnameService.insert(usersRealname);

        return SUCCESS_TIP;
    }

    /**
     * 删除个人认证
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer usersRealnameId) {
        usersRealnameService.deleteById(usersRealnameId);
        return SUCCESS_TIP;
    }

    /**
     * 修改个人认证
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(@RequestParam(value="realId") Integer realId,@RequestParam(value="auditStatus") Integer auditStatus,
    		@RequestParam(value="auditRemark") String auditRemark){
    	UsersRealname usersRealname = new UsersRealname();
    	usersRealname.setRealId(realId);
    	usersRealname.setAuditStatus(auditStatus);
    	usersRealname.setAuditRemark(auditRemark);
        usersRealname.setAuditDatetime(new Date());
        usersRealnameService.updateById(usersRealname);
        UsersRealname usersRealname1 = usersRealnameService.selectById(realId);
        if(auditStatus.intValue()==1){
        	 Users users = usersService.selectById(usersRealname1.getUserId());
             users.setTrueName(usersRealname1.getTrueName());
             usersService.updateById(users);
        }
       
        return SUCCESS_TIP;
    }

    /**
     * 个人认证详情
     */
    @RequestMapping(value = "/detail/{usersRealnameId}")
    @ResponseBody
    public Object detail(@PathVariable("usersRealnameId") Integer usersRealnameId) {
        return usersRealnameService.selectById(usersRealnameId);
    }
    
    /********************首页************************/
    /**
     * 个人认证详情
     */
    @RequestMapping(value = "/selectUserCardId")
    @ResponseBody
    public Object selectUserCardId(@RequestParam("userId") Integer userId) {
        return usersRealnameService.selectUserCardId(userId);
    }
}