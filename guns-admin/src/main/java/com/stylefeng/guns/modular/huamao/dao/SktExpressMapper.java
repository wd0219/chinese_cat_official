package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 快递表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface SktExpressMapper extends BaseMapper<SktExpress> {

}
