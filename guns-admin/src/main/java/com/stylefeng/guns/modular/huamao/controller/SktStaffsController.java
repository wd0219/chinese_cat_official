package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.core.support.DateTime;
import com.stylefeng.guns.core.util.ToolUtil;

import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustryDTO;
import com.stylefeng.guns.modular.huamao.model.SktStaffs;
import com.stylefeng.guns.modular.huamao.service.ISktStaffsService;

/**
 * lianmengshangjia2控制器
 *
 * @author fengshuonan
 * @Date 2018-04-25 14:20:31
 */
@Controller
@RequestMapping("/sktStaffs")
public class SktStaffsController extends BaseController {

    private String PREFIX = "/huamao/sktStaffs/";

    @Autowired
    private ISktStaffsService sktStaffsService;

    /**
     * 跳转到lianmengshangjia2首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktStaffs.html";
    }

    /**
     * 跳转到添加lianmengshangjia2
     */
    @RequestMapping("/sktStaffs_add")
    public String sktStaffsAdd() {
        return PREFIX + "sktStaffs_add.html";
    }

    /**
     * 跳转到修改lianmengshangjia2
     */
    @RequestMapping("/sktStaffs_update/{sktStaffsId}")
    public String sktStaffsUpdate(@PathVariable Integer sktStaffsId, Model model,SktShopIndustryDTO sktShopIndustryDTO) {
    	sktShopIndustryDTO.setApplyId(sktStaffsId);
    	List<SktShopIndustryDTO> list = sktStaffsService.sktStaffsfindup(sktShopIndustryDTO);
        if(list.size()>0 && list!=null){
            SktShopIndustryDTO shopIndustryDTO = list.get(0);
            String legalPersonImg = shopIndustryDTO.getLegalPersonImg();
            String allUrl = UrlUtils.getAllUrl(legalPersonImg);
            shopIndustryDTO.setLegalPersonImg(allUrl);
            String licenseImg = shopIndustryDTO.getLicenseImg();
            String allUrl1 = UrlUtils.getAllUrl(licenseImg);
            shopIndustryDTO.setLicenseImg(allUrl1);
            String logo = shopIndustryDTO.getLogo();
            String allUrl2 = UrlUtils.getAllUrl(logo);
            shopIndustryDTO.setLogo(allUrl2);
            model.addAttribute("item",shopIndustryDTO);
        }else{
        	model.addAttribute("item",null);
        }
        return PREFIX + "sktStaffs_edit.html";
      
    }

    /**
     * 获取lianmengshangjia2列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktShopIndustryDTO sktShopIndustryDTO) {
        Page<SktShopIndustryDTO> page = new PageFactory<SktShopIndustryDTO>().defaultPage();
        List<SktShopIndustryDTO> sktShopIndustryDTOS = sktStaffsService.sktStaffsfindAll(page,sktShopIndustryDTO);
        page.setRecords(sktShopIndustryDTOS);
        return super.packForBT(page);
    }

    /**
     * 新增lianmengshangjia2
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktStaffs sktStaffs) {
        sktStaffsService.insert(sktStaffs);
        return SUCCESS_TIP;
    }

    /**
     * 删除lianmengshangjia2
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktStaffsId) {
        sktStaffsService.deleteById(sktStaffsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改lianmengshangjia2
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopIndustryDTO sktShopIndustryDTO) {
        sktStaffsService.sktStaffsupdate(sktShopIndustryDTO);
        return SUCCESS_TIP;
    }
    
    /**
     * 修改lianmengshangjia2
     */
    @RequestMapping(value = "/update2")
    @ResponseBody
    public Object update2(SktShopIndustryDTO sktShopIndustryDTO) {
    	// 发送者id为操作员id
        ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
        Integer  shiroUserId=shiroUser.getId();
        String sktStaffsup1 = sktStaffsService.sktStaffsup1(sktShopIndustryDTO,shiroUserId);
       if(ToolUtil.isEmpty(sktStaffsup1) || "0".equals(sktStaffsup1) || "2".equals(sktStaffsup1)){
    	   return sktStaffsup1;
       }
        return SUCCESS_TIP;
    }

    /**
     * lianmengshangjia2详情
     */
    @RequestMapping(value = "/detail/{sktStaffsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktStaffsId") Integer sktStaffsId) {
        return sktStaffsService.selectById(sktStaffsId);
    }
    
    @RequestMapping(value = "/detail2/{sktShopApplysId}")
    public String detail1(@PathVariable("sktShopApplysId") Integer sktStaffsId,SktShopIndustryDTO sktShopIndustryDTO,Model model,String legalPerson ) {
    	sktShopIndustryDTO.setApplyId(sktStaffsId);
    	sktShopIndustryDTO.setLegalPerson(legalPerson);
    	List<SktShopIndustryDTO> list = sktStaffsService.sktStaffsfindup(sktShopIndustryDTO);
    	sktShopIndustryDTO.setCheckTime(new DateTime().toString());
    	 model.addAttribute("item",list.get(0));
    	 return PREFIX +"sktStaffs_edit2.html";
    }
}
