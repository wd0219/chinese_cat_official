package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.asyncTask.MyAsyncTask;
import com.stylefeng.guns.modular.huamao.common.QueueUtils;
import com.stylefeng.guns.modular.huamao.dao.UsersDrawsMapper;
import com.stylefeng.guns.modular.huamao.model.UsersDraws;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsList;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IAccountShopService;
import com.stylefeng.guns.modular.huamao.service.IUsersDrawsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户提现表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
@Service
public class UsersDrawsServiceImpl extends ServiceImpl<UsersDrawsMapper, UsersDraws> implements IUsersDrawsService {

	@Autowired
	private UsersDrawsMapper udm;
	@Autowired
	private IAccountService accountService;
	@Autowired
	private IAccountShopService accountShopService;
	@Autowired
	private MyAsyncTask myAsyncTask;

	// 用户提现表展示
	@Override
	public List<UsersDrawsList> selectUsersDrawsAll(Page<UsersDrawsList> page, UsersDrawsList usersDrawsList) {
		Integer total = udm.selectUsersDrawsAllCount(usersDrawsList);
		page.setTotal(total);
		return udm.selectUsersDrawsAll(page,usersDrawsList);
	}

	// 修改审核状态
	@Transactional
	@Override
	public void updateSatus(UsersDrawsList usersDrawsList) {

			udm.updateSatus(usersDrawsList);

	}

	//批量删除
	@Override
	public Integer batchEdit(UsersDrawsList usersDrawsList) {
		return udm.batchEdit(usersDrawsList);
	}


	/**
	 * 华宝批量转换
	 * @param list
	 * @return
	 */
	@Override
	public Object batchRecharge(List<Map<String, Object>> list) {
		try {
			for(Map<String, Object> map:list){
				QueueUtils.put(map);
			}
			myAsyncTask.refreshMyDbAsync();
			return "正在执行";
		} catch (InterruptedException e) {
			e.printStackTrace();
			return "执行失败";
		}



	}
}
