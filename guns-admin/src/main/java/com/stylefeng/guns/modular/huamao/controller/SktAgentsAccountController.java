package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccount;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccountDTO;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsAccountService;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsService;

/**
 * 代理公司账户控制器
 *
 * @author fengshuonan
 * @Date 2018-04-18 11:40:09
 */
@Controller
@RequestMapping("/sktAgentsAccount")
public class SktAgentsAccountController extends BaseController {

    private String PREFIX = "/huamao/sktAgentsAccount/";

    @Autowired
    private ISktAgentsAccountService sktAgentsAccountService;
    @Autowired
    private ISktAgentsService sktAgentsService;
    /**
     * 跳转到代理公司账户首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktAgentsAccount.html";
    }

    /**
     * 跳转到添加代理公司账户
     */
    @RequestMapping("/sktAgentsAccount_add")
    public String sktAgentsAccountAdd() {
        return PREFIX + "sktAgentsAccount_add.html";
    }

    /**
     * 跳转到修改代理公司账户
     */
    @RequestMapping("/sktAgentsAccount_update/{sktAgentsAccountId}")
    public String sktAgentsAccountUpdate(@PathVariable Integer sktAgentsAccountId, Model model) {
        SktAgentsAccount sktAgentsAccount = sktAgentsAccountService.selectById(sktAgentsAccountId);
        model.addAttribute("item",sktAgentsAccount);
        LogObjectHolder.me().set(sktAgentsAccount);
        return PREFIX + "sktAgentsAccount_edit.html";
    }
    
    /**
     * 跳转到修改代理公司分红系数
     */
    @RequestMapping("/sktAgentsAccount_setRatio/{agentId}")
    public String sktAgentsAccountSetRatio(@PathVariable Integer agentId, Model model) {
        model.addAttribute("agentId",agentId);
        return PREFIX + "sktAgentsAccount_setRatio.html";
    }
    
    /**
     * 跳转到更新代理公司状态
     */
    @RequestMapping("/sktAgentsAccount_openOrClose/{accountId}/{runFlag}")
    public String sktAgentsAccountOpenOrClose(@PathVariable Integer accountId,@PathVariable Integer runFlag, Model model) {
    	model.addAttribute("accountId",accountId);
    	String str = "";
    	if(runFlag==1){
    		str = "开启";
    	}else{
    		str = "关闭";
    	}
    	model.addAttribute("str",str);
    	model.addAttribute("runFlag",runFlag);
    	return PREFIX + "sktAgentsAccount_openOrClose.html";
    }

    /**
     * 获取代理公司账户列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktAgentsAccountDTO sktAgentsAccountDTO) {
    	Page<SktAgentsAccountDTO> page = new PageFactory<SktAgentsAccountDTO>().defaultPage();
         List<SktAgentsAccountDTO> list = sktAgentsAccountService.showAgentsAccountInfo (page,sktAgentsAccountDTO);
        page.setRecords(list);
 		return super.packForBT(page);
    }

    /**
     * 新增代理公司账户
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktAgents sktAgents) {
        sktAgentsService.updateById(sktAgents);
        return SUCCESS_TIP;
    }

    /**
     * 删除代理公司账户
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktAgentsAccountId) {
        sktAgentsAccountService.deleteById(sktAgentsAccountId);
        return SUCCESS_TIP;
    }

    /**
     * 修改代理公司账户
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktAgentsAccount sktAgentsAccount) {
    	if(sktAgentsAccount.getRunFlag()==1 ){
    		sktAgentsAccount.setRunTime(new Date());
    	}
        sktAgentsAccountService.updateById(sktAgentsAccount);
        return SUCCESS_TIP;
    }

    /**
     * 代理公司账户详情
     */
    @RequestMapping(value = "/detail/{sktAgentsAccountId}")
    @ResponseBody
    public Object detail(@PathVariable("sktAgentsAccountId") Integer sktAgentsAccountId) {
        return sktAgentsAccountService.selectById(sktAgentsAccountId);
    }
}
