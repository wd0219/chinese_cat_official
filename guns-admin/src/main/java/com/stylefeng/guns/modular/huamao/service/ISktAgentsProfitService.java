package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktAgentsProfit;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 代理股东分红记录表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-12
 */
public interface ISktAgentsProfitService extends IService<SktAgentsProfit> {

}
