package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Articles;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 文章记录表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
public interface ArticlesMapper extends BaseMapper<Articles> {
	List<Map<String,Object>>selectArticles(@Param("page")Page<Map<String,Object>> page,@Param("sktFriendlinksPage") Articles articles);

    Integer selectArticlesCount(@Param("sktFriendlinksPage") Articles articles);

    List<Articles> adsSelectArticleTitle(String soName);
}
