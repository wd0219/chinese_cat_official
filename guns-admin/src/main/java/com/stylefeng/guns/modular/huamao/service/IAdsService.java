package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Ads;
import com.stylefeng.guns.modular.huamao.model.AdsList;

/**
 * <p>
 * 广告表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface IAdsService extends IService<Ads> {

	// 广告表展示
	public List<AdsList> selectAdsAll(Page<AdsList> page, AdsList adsList);

	// 异步获取广告位置
	public List<AdsList> getPosition(String positionType);

	// 添加广告
	public Integer addAds(AdsList adsList);

	// 通过id查询广告为修改做准备
	public AdsList selectAdById(Integer adId);

	// 通过id修改广告
	public void updateAdById(AdsList adsList);

    Map<String,Object> getSo(String searchType, String soName);
}
