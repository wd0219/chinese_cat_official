package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStockscore;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersStockscoreMapper;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersStockscoreService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商家购买库存积分订单表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
@Service
public class SktOrdersStockscoreServiceImpl extends ServiceImpl<SktOrdersStockscoreMapper, SktOrdersStockscore> implements ISktOrdersStockscoreService {
	@Autowired
	private SktOrdersStockscoreMapper orderStockscoreMapper;
	@Override
	public List<SktOrdersStockscore> sktOrdersStockscorefindall(Page<SktOrdersStockscore> page, SktOrdersStockscore ordersStockscore) {
		Integer total = orderStockscoreMapper.sktOrdersStockscorefindallCount(ordersStockscore);
		page.setTotal(total);
		List<SktOrdersStockscore> list = orderStockscoreMapper.sktOrdersStockscorefindall(page,ordersStockscore);
		return list;
	}

}
