package com.stylefeng.guns.modular.huamao.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import com.stylefeng.guns.modular.huamao.model.Ads;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.AdsList;
import com.stylefeng.guns.modular.huamao.service.IAdsService;

/**
 * 广告管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 16:35:08
 */
@Controller
@RequestMapping("/ads")
public class AdsController extends BaseController {

	private String PREFIX = "/huamao/ads/";

	@Autowired
	private IAdsService adsService;

	/**
	 * 跳转到广告管理首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "ads.html";
	}

	/**
	 * 跳转到添加广告管理
	 */
	@RequestMapping("/ads_add")
	public String adsAdd() {
		return PREFIX + "ads_add.html";
	}

	/**
	 * 跳转到修改广告管理:通过id查询广告为修改广告做准备
	 */
	@RequestMapping("/ads_update/{adId}")
	public String adsUpdate(@PathVariable Integer adId, Model model) {
		AdsList adslist = adsService.selectAdById(adId);
		String adFile = adslist.getAdFile();
		String allUrl = UrlUtils.getAllUrl(adFile);
		adslist.setAdFile(allUrl);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		adslist.setStartDate(sdf.format(adslist.getAdStartDate()));
		adslist.setEndDate(sdf.format(adslist.getAdEndDate()));
		model.addAttribute("item", adslist);
		// LogObjectHolder.me().set(ads);
		return PREFIX + "ads_edit.html";
	}

	/**
	 * 获取广告管理列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(AdsList adsList) {
		Page<AdsList> page = new PageFactory<AdsList>().defaultPage();
		List<AdsList> adsLists = adsService.selectAdsAll(page,adsList);
		page.setRecords(adsLists);
		return super.packForBT(page);
	}
	/**
	 * 新增广告管理
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(AdsList adsList) {


		// 创建时间
		adsList.setCreateTime(new Date());
		// 添加广告
		Integer update = adsService.addAds(adsList);
		if(update != 0){
			return SUCCESS_TIP;
		}
		// adsService.insert(ads);
		return ERROR;
	}

	/**
	 * 删除广告管理
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer adsId) {
//		Ads ads = new Ads();
//		ads.setAdId(adsId);
//		ads.setDataFlag(-1);
//		adsService.updateById(ads);
		adsService.deleteById(adsId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改广告管理
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(AdsList adsList) {
		// 通过id修改广告
		adsService.updateAdById(adsList);
		// adsService.updateById(ads);
		return SUCCESS_TIP;
	}

	/**
	 * 广告管理详情
	 */
	@RequestMapping(value = "/detail/{adsId}")
	@ResponseBody
	public Object detail(@PathVariable("adsId") Integer adsId) {
		return adsService.selectById(adsId);
	}

	/**
	 * 异步获取广告位置
	 */
	@PostMapping("/getPosition")
	@ResponseBody
	public Object getPosition(String positionType) {
		return adsService.getPosition(positionType);
	}

	/**
	 * 异步获取添加广告是内部链接搜索的内容和搜索类型
	 */
	@PostMapping("/getSo")
	@ResponseBody
	public Object getSo(String searchType,String soName) {
		return adsService.getSo(searchType,soName);
	}


	@RequestMapping("/adsSelect")
	public String adsSelect() {
		return PREFIX + "ads_adsSelect.html";
	}

}
