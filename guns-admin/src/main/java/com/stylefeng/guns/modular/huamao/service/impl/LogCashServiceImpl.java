package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogCashMapper;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogCashList;
import com.stylefeng.guns.modular.huamao.service.ILogCashService;

/**
 * <p>
 * 现金流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogCashServiceImpl extends ServiceImpl<LogCashMapper, LogCash> implements ILogCashService {

	@Autowired
	private LogCashMapper lcm;

	// 现金流水表返回
	@Override
	public List<LogCashList> selectLogCashAll(Page<LogCashList> page, LogCashList logCashList) {
		Integer total = lcm.selectLogCashAllCount(logCashList);
		page.setTotal(total);
		return lcm.selectLogCashAll(page,logCashList);
	}

}
