package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.shiro.ShiroUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.AgentsDraws;
import com.stylefeng.guns.modular.huamao.model.AgentsDrawsList;
import com.stylefeng.guns.modular.huamao.service.IAgentsDrawsService;

import java.util.Date;
import java.util.List;

/**
 * 代理公司提现控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 16:06:06
 */
@Controller
@RequestMapping("/agentsDraws")
public class AgentsDrawsController extends BaseController {

	private String PREFIX = "/huamao/agentsDraws/";

	@Autowired
	private IAgentsDrawsService agentsDrawsService;

	/**
	 * 跳转到代理公司提现首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "agentsDraws.html";
	}

	/**
	 * 跳转到添加代理公司提现
	 */
	@RequestMapping("/agentsDraws_add")
	public String agentsDrawsAdd() {
		return PREFIX + "agentsDraws_add.html";
	}

	/**
	 * 跳转到修改代理公司提现
	 */
	@RequestMapping("/agentsDraws_update/{agentsDrawsId}")
	public String agentsDrawsUpdate(@PathVariable Integer agentsDrawsId, Model model) {
		AgentsDraws agentsDraws = agentsDrawsService.selectById(agentsDrawsId);
		model.addAttribute("item", agentsDraws);
		LogObjectHolder.me().set(agentsDraws);
		return PREFIX + "agentsDraws_edit.html";
	}

	/**
	 * 获取代理公司提现列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(AgentsDrawsList agentsDrawsList) {
		Page<AgentsDrawsList> page = new PageFactory<AgentsDrawsList>().defaultPage();
		List<AgentsDrawsList> agentsDrawsLists = agentsDrawsService.selectAgentsDrawsAll(page,agentsDrawsList);
		page.setRecords(agentsDrawsLists);
		return super.packForBT(page);
		// return agentsDrawsService.selectList(null);
	}

	/**
	 * 新增代理公司提现
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(AgentsDraws agentsDraws) {
		agentsDrawsService.insert(agentsDraws);
		return SUCCESS_TIP;
	}

	/**
	 * 删除代理公司提现
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer agentsDrawsId) {
		agentsDrawsService.deleteById(agentsDrawsId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改代理公司提现
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(AgentsDraws agentsDraws) {
		agentsDrawsService.updateById(agentsDraws);
		return SUCCESS_TIP;
	}

	/**
	 * 代理公司提现详情
	 */
	@RequestMapping(value = "/detail/{agentsDrawsId}")
	@ResponseBody
	public Object detail(@PathVariable("agentsDrawsId") Integer agentsDrawsId) {
		return agentsDrawsService.selectById(agentsDrawsId);
	}

	/**
	 * 审核通过
	 */
	@RequestMapping("/agentsDrawsPass")
	public String agentsDrawsPass(@RequestParam String drawId, Model model) {
		// 将获取到id存入model为修改做准备
		model.addAttribute("drawId", drawId);
		return PREFIX + "agentsDraws_Pass.html";
	}

	/**
	 * 审核拒绝
	 */
	@RequestMapping("/agentsDrawsRefuse")
	public String agentsDrawsRefuse(@RequestParam String drawId, Model model) {
		// 将获取到id存入model为修改做准备
		model.addAttribute("drawId", drawId);
		return PREFIX + "agentsDraws_Refuse.html";
	}
	/**
	 * 审核拒绝后的修改数据
	 */
	@RequestMapping("/updateSatus")
	@ResponseBody
	public Object updateSatus(AgentsDrawsList agentsDrawsList) {
		// 获取操作人员id
		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
		// 将审核状态改为审核拒绝
		agentsDrawsList.setCashSatus(-1);
		// 审核时间
		agentsDrawsList.setCheckTime(new Date());
		// 设置审核员id
		agentsDrawsList.setCheckStaffId(shiroUser.getId());
		// 将审核状态改为审核失败
		agentsDrawsService.updateSatus(agentsDrawsList);
		return SUCCESS_TIP;
	}

	/**
	 * 订单处理中处理
	 */
	@RequestMapping("/agentsDrawsInHand")
	public String agentsDrawsInHand(@RequestParam String drawId, Model model) {
		// 将获取到id存入model为修改做准备
		model.addAttribute("drawId", drawId);
		return PREFIX + "agentsDraws_InHand.html";
	}

	/**
	 * 订单处理中处理
	 */
	@RequestMapping("/agentsDrawsDefeated")
	public String agentsDrawsDefeated(@RequestParam String drawId, Model model) {
		// 将获取到id存入model为修改做准备
		model.addAttribute("drawId", drawId);
		return PREFIX + "agentsDraws_Defeated.html";
	}
	/**
	 * 线下打款
	 */
	@RequestMapping("/agentsDrawsMoney")
	@ResponseBody
	public Object agentsDrawsMoney(AgentsDrawsList agentsDrawsList) {
		// 获取操作人员id
		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
		// 将审核状态改为线下打款
		agentsDrawsList.setCashSatus(4);
		// 审核时间
		agentsDrawsList.setCheckTime(new Date());
		// 设置审核员id
		agentsDrawsList.setCheckStaffId(shiroUser.getId());
		// 修改审核状态
		agentsDrawsService.updateSatus(agentsDrawsList);
		return SUCCESS_TIP;
	}
}
