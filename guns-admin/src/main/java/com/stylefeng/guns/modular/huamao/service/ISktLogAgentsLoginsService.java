package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktLogAgentsLogins;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 代理公司登陆记录表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
public interface ISktLogAgentsLoginsService extends IService<SktLogAgentsLogins> {
	
	public List<SktLogAgentsLogins> sktLogAgentsLoginfindup(Page<SktLogAgentsLogins> page, SktLogAgentsLogins sktLogAgentsLogins);
}
