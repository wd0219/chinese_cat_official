package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.HomeMenus;
import com.stylefeng.guns.modular.huamao.dao.HomeMenusMapper;
import com.stylefeng.guns.modular.huamao.service.IHomeMenusService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 前台菜单表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-28
 */
@Service
public class HomeMenusServiceImpl extends ServiceImpl<HomeMenusMapper, HomeMenus> implements IHomeMenusService {

	@Override
	public List<Map<String, Object>> selectHomeMenus(Page<Map<String,Object>> page, HomeMenus homeMenus) {
		// TODO Auto-generated method stub
		Integer total =baseMapper.selectHomeMenusCount(homeMenus);
		page.setTotal(total);
		return baseMapper.selectHomeMenus(page,homeMenus);
	}

	@Override
	public Integer selectHomeMenusCount(HomeMenus homeMenus) {
		return baseMapper.selectHomeMenusCount(homeMenus);
	}

}
