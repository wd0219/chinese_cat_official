package com.stylefeng.guns.modular.huamao.model;

public class SktAgentsStockholderDTO extends SktAgentsStockholder{
	
	/**
	 * 用户姓名
	 */
	private String trueName;
	/**
	 * 用户电话
	 */
	private String userPhone;
	/**
	 * 代理公司名称
	 */
	private String agentName;
	/**
	 * 代理级别
	 */
	private Integer alevel;
	/**
	 * 省id
	 */
	private Integer province ; 
	/**
	 * 市id
	 */
	private Integer citys; 
	/**
	 * 县id
	 */
	private Integer county;
	
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public Integer getAlevel() {
		return alevel;
	}
	public void setAlevel(Integer alevel) {
		this.alevel = alevel;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	public Integer getCitys() {
		return citys;
	}
	public void setCitys(Integer citys) {
		this.citys = citys;
	}
	public Integer getCounty() {
		return county;
	}
	public void setCounty(Integer county) {
		this.county = county;
	}
	
	
}
