package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.dao.GoodsCatsMapper;
import com.stylefeng.guns.modular.huamao.dao.SpecCatsMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.dao.SktSpecApplyMapper;
import com.stylefeng.guns.modular.huamao.service.ISktSpecApplyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规则申请表 服务实现类
 * </p>
 *
 * @author py
 * @since 2018-08-23
 */
@Service
public class SktSpecApplyServiceImpl extends ServiceImpl<SktSpecApplyMapper, SktSpecApply> implements ISktSpecApplyService {
    @Autowired
    private SktSpecApplyMapper sktSpecApplyMapper;
    @Autowired
    private GoodsCatsMapper goodsCatsMapper;
    @Autowired
    private SpecCatsMapper specCatsMapper;
    @Autowired
    private SpecCatsServiceImpl specCatsServiceImpl;
    @Override
    public List<SktSpecApplyDto> showSpecApplyInfo(Page<SktSpecApplyDto> page, SktSpecApplyDto sktSpecApplyDto) {
        List<SktSpecApplyDto> list = sktSpecApplyMapper.showSpecApplyInfo(page,sktSpecApplyDto);
        //
       for (SktSpecApplyDto sktSpecApplyDtoDB:list) {
            String goodsCatPath = sktSpecApplyDtoDB.getGoodsCatPath();
            String[] split = goodsCatPath.split("_");
            String goodsCatNamePath = "";
            for (String goodspath:split) {
                GoodsCats goodsCats = goodsCatsMapper.selectById(Integer.parseInt(goodspath));
                String catName = goodsCats.getCatName();
                goodsCatNamePath=goodsCatNamePath+catName+"→";
            }
            goodsCatNamePath=goodsCatNamePath.substring(0,goodsCatNamePath.lastIndexOf("→") );
            sktSpecApplyDtoDB.setGoodClassifyPath(goodsCatNamePath);
            sktSpecApplyDtoDB.setLastClassifyPath(goodsCatsMapper.selectById(sktSpecApplyDtoDB.getGoodsCatId()).getCatName());
        }
        Integer total = sktSpecApplyMapper.showSpecApplyInfoCount(sktSpecApplyDto);
        page.setTotal(total);
        return list;
    }

    @Override
    public SktSpecApplyDto findSktSpecApplyById(SktSpecApplyDto sktSpecApplyDto) {
        SktSpecApplyDto sktSpecApplyDtoDB = sktSpecApplyMapper.findSktSpecApplyById(sktSpecApplyDto);
        String goodsCatPath = sktSpecApplyDtoDB.getGoodsCatPath();
        String[] split = goodsCatPath.split("_");
        String goodsCatNamePath = "";
        for (String goodspath:split) {
            GoodsCats goodsCats = goodsCatsMapper.selectById(Integer.parseInt(goodspath));
            String catName = goodsCats.getCatName();
            goodsCatNamePath=goodsCatNamePath+catName+"→";
        }
        goodsCatNamePath=goodsCatNamePath.substring(0,goodsCatNamePath.lastIndexOf("→") );
        sktSpecApplyDtoDB.setGoodClassifyPath(goodsCatNamePath);
        sktSpecApplyDtoDB.setLastClassifyPath(goodsCatsMapper.selectById(sktSpecApplyDtoDB.getGoodsCatId()).getCatName());

        return sktSpecApplyDtoDB;
    }


    @Override
    public Map<String,String> updateSktSpecApplyById(SktSpecApply sktSpecApply) {
        Map<String, String> map = new HashMap<String, String>();
      List<SpecCats> SpecCatsDB = specCatsServiceImpl.selectList(new EntityWrapper<SpecCats>().eq(
                "goodsCatId", sktSpecApply.getGoodsCatId()
        ));
        //遍历最后一级商品分类下属否已存在申请添加的规格，不存在允许申请，存在不允许申请
        for (SpecCats specCats: SpecCatsDB ) {
           if (specCats.getCatName().equals(sktSpecApply.getCatName())){
               //修改当前申请记录状态拒绝，拒绝原因为当前申请的规格已存在
               sktSpecApply.setRefuseDesc("申请失败,当商品分类该规格已存在");
               sktSpecApply.setApplicationStatus(-1);
               sktSpecApplyMapper.updateById(sktSpecApply);
               map.put("ERROR","申请失败,当商品分类该规格已存在");
               return map;
           }
        }
        sktSpecApplyMapper.updateById(sktSpecApply);
        //新增一条商品分类记录
        SpecCats specCats = new SpecCats();
        specCats.setGoodsCatId(sktSpecApply.getGoodsCatId());
        specCats.setGoodsCatPath(sktSpecApply.getGoodsCatPath());
        specCats.setCatName(sktSpecApply.getCatName());
        specCats.setCreateTime(new Date());
        boolean insert = specCatsServiceImpl.insert(specCats);
        if (insert){
            map.put("SUCCESS","审核成功");
        }else {
            map.put("ERROR","审核失败");
        }
        return map;
    }
}
