package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 待发特别积分流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public class LogScoreSpecialList extends LogScoreSpecial {

	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String scorep;
	/**
	 * 变动后金额
	 */
	private BigDecimal ascore;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	public String getScorep() {
		if (this.getScoreType() == -1) {
			return "-" + this.getScore();
		}
		return "+" + this.getScore();
	}

	// public void setScorep(String scorep) {
	// this.scorep = scorep;
	// }

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	/**
	 * 变动后金额，直接让变动前加上变动的就行，不需要赋值。
	 */
	public BigDecimal getAscore() {
		return this.getPreScore().add(this.getScore());
	}

	// public void setAscore(BigDecimal ascore) {
	// this.ascore = ascore;
	// }

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
