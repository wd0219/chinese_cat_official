package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 积分流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public class LogScoreList extends	LogScore {

	/**
	 * 用户手机
	 */
	private String userPhone;
//	/**
//	 * 积分类型 1商城消费获取 2线下商家消费获得 3购买库存积分获得 4下线消费获得 5下线营业额获得 6特别奖励 7用户升级奖励 8下线升级奖励
//	 * 9升级代理 10下线升级代理奖励 11分红获得 12待发积分账户转入 14后台手动增加 15待发特别奖励转入 16购买牌匾奖励
//	 * 17下线购买牌匾奖励 18消费退款 31转换开元宝减少 32后台手动减少 33购买自营商品
//	 */
//	private Integer type;
//	/**
//	 * 发起用户ID 0为平台
//	 */
//	private Integer fromId;
	/**
	 * 发起用户名字
	 */
	private String fromName;
	/**
	 * 目标用户名字
	 */
	private String loginName;
	/**
	 * 目标用户ID
	 */
//	private Integer userId;
//	/**
//	 * 对应订单号
//	 */
//	private String orderNo;
//	/**
//	 * 操作前的金额
//	 */
//	private BigDecimal preScore;
//	/**
//	 * 流水标志 -1减少 1增加
//	 */
//	private Integer scoreType;
//	/**
//	 * 金额
//	 */
//	private BigDecimal score;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String scorep;
	/**
	 * 变动后金额
	 */
	private BigDecimal ascore;
//	/**
//	 * 备注
//	 */
//	private String remark;
//	/**
//	 * 有效状态 1有效 0删除
//	 */
//	private Integer dataFlag;
//	/**
//	 * 创建时间
//	 */
//	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	public String getScorep() {
		if (this.getScoreType() == -1) {
			return "-" + this.getScore();
		}
		return "+" + this.getScore();
	}

	// public void setScorep(String scorep) {
	// this.scorep = scorep;
	// }

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

//	public Integer getType() {
//		return type;
//	}
//
//	public void setType(Integer type) {
//		this.type = type;
//	}
//
//	public Integer getFromId() {
//		return fromId;
//	}
//
//	public void setFromId(Integer fromId) {
//		this.fromId = fromId;
//	}
//
//	public Integer getUserId() {
//		return userId;
//	}
//
//	public void setUserId(Integer userId) {
//		this.userId = userId;
//	}
//
//	public String getOrderNo() {
//		return orderNo;
//	}
//
//	public void setOrderNo(String orderNo) {
//		this.orderNo = orderNo;
//	}
//
//	public BigDecimal getPreScore() {
//		return preScore;
//	}
//
//	public void setPreScore(BigDecimal preScore) {
//		this.preScore = preScore;
//	}
//
//	public Integer getScoreType() {
//		return scoreType;
//	}
//
//	public void setScoreType(Integer scoreType) {
//		this.scoreType = scoreType;
//	}
//
//	public BigDecimal getScore() {
//		return score;
//	}

	// public void setScore(BigDecimal score) {
	// this.score = score;
	// }

	/**
	 * 变动后金额，直接让变动前加上变动的就行，不需要赋值。
	 */
	public BigDecimal getAscore() {
		if(this.getScoreType() == 1){
			return this.getPreScore().add(this.getScore());
		}else{
			return this.getPreScore().subtract(this.getScore());
		}
	}

	public void setAscore(BigDecimal ascore) {
		this.ascore = ascore;
	}

//	public String getRemark() {
//		return remark;
//	}
//
//	public void setRemark(String remark) {
//		this.remark = remark;
//	}
//
//	public Integer getDataFlag() {
//		return dataFlag;
//	}
//
//	public void setDataFlag(Integer dataFlag) {
//		this.dataFlag = dataFlag;
//	}
//
//	public Date getCreateTime() {
//		return createTime;
//	}
//
//	public void setCreateTime(Date createTime) {
//		this.createTime = createTime;
//	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "LogScoreList{" +
				"userPhone='" + userPhone + '\'' +
				", fromName='" + fromName + '\'' +
				", loginName='" + loginName + '\'' +
				", scorep='" + scorep + '\'' +
				", ascore=" + ascore +
				", beginTime='" + beginTime + '\'' +
				", endTime='" + endTime + '\'' +
				'}';
	}
}
