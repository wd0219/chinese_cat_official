package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogCashList;

/**
 * <p>
 * 现金流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface ILogCashService extends IService<LogCash> {

	// 现金流水表返回
	public List<LogCashList> selectLogCashAll(Page<LogCashList> pgge, LogCashList logCashList);

}
