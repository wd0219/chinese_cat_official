package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.CatBrands;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;
import com.stylefeng.guns.modular.huamao.service.ICatBrandsService;
import com.stylefeng.guns.modular.huamao.service.IGoodsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.Recommends;
import com.stylefeng.guns.modular.huamao.service.IRecommendsService;

/**
 * 推荐记录表控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 14:10:43
 */
@Controller
@RequestMapping("/recommends")
public class RecommendsController extends BaseController {

    private String PREFIX = "/system/recommends/";

    @Autowired
    private IRecommendsService recommendsService;
    @Autowired
    private ICatBrandsService catBrandsService;
    @Autowired
    private IGoodsService goodsService;

    /**
     * 跳转到推荐记录表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "recommends.html";
    }

    /**
     * 跳转到添加推荐记录表
     */
    @RequestMapping("/recommends_add")
    public String recommendsAdd() {
        return PREFIX + "recommends_add.html";
    }

    /**
     * 跳转到修改推荐记录表
     */
    @RequestMapping("/recommends_update/{recommendsId}")
    public String recommendsUpdate(@PathVariable Integer recommendsId, Model model) {
        Recommends recommends = recommendsService.selectById(recommendsId);
        model.addAttribute("item",recommends);
        LogObjectHolder.me().set(recommends);
        return PREFIX + "recommends_edit.html";
    }

    /**
     * 获取推荐记录表列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return recommendsService.selectList(null);
    }

    /**
     * 新增推荐记录表
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Recommends recommends) {
        recommendsService.insert(recommends);
        return SUCCESS_TIP;
    }
    /**
     * 批量新增和删除品牌推荐记录表
     */
    @RequestMapping(value = "/addBrandsList")
    @ResponseBody
    public Object addBrandsList(
    		@RequestParam(value="to_right",required = false) String to_right,
                          Integer catId) {
        List<Map<String, Object>> listMap = JSON.parseObject(to_right, new TypeReference<List<Map<String,Object>>>(){});
        recommendsService.addBrandsList(listMap,catId);
        return SUCCESS_TIP;
    }

    /**
     * 查询未推荐商品
     * @param goodsCustm
     * @return
     */
    @RequestMapping(value = "/selectNotRecomGoods")
    @ResponseBody
    public List<Map<String,Object>> selectNotRecomGoods(GoodsCustm goodsCustm) {
        return  goodsService.selectNotRecomGoods(goodsCustm);
    }
    /**
     * 查询推荐商品
     * @param goodsCustm
     * @return
     */
    @RequestMapping(value = "/selectRecomGoods")
    @ResponseBody
    public List<Map<String,Object>> selectRecomGoods(GoodsCustm goodsCustm) {
        return  recommendsService.selectRecomGoods(goodsCustm);
    }
    /**
     * 批量新增和删除商品推荐记录表
     */
    @RequestMapping(value = "/addGoodsList")
    @ResponseBody
    public Object addGoodsList(  @RequestParam(value="to_right",required = false) String to_right,Integer dataType,
                                Integer catId) {
        List<Map<String, Object>> listMap = JSON.parseObject(to_right, new TypeReference<List<Map<String,Object>>>(){});
        recommendsService.addGoodsList(listMap,catId,dataType);
        return SUCCESS_TIP;
    }

    /**
     * 删除推荐记录表
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer recommendsId) {
        recommendsService.deleteById(recommendsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改推荐记录表
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Recommends recommends) {
        recommendsService.updateById(recommends);
        return SUCCESS_TIP;
    }

    /**
     * 推荐记录表详情
     */
    @RequestMapping(value = "/detail/{recommendsId}")
    @ResponseBody
    public Object detail(@PathVariable("recommendsId") Integer recommendsId) {
        return recommendsService.selectById(recommendsId);
    }
}
