package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanAgentMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgent;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgentList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanAgentService;

/**
 * <p>
 * 代理公司开元宝流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
@Service
public class LogKaiyuanAgentServiceImpl extends ServiceImpl<LogKaiyuanAgentMapper, LogKaiyuanAgent>
		implements ILogKaiyuanAgentService {

	@Autowired
	private LogKaiyuanAgentMapper lam;

	// 代理公司华宝流水表展示
	@Override
	public List<LogKaiyuanAgentList> selectLogKaiyuanAgentAll(Page<LogKaiyuanAgentList> page, LogKaiyuanAgentList logKaiyuanAgentList) {
		Integer total = lam.selectLogKaiyuanAgentAllCount(logKaiyuanAgentList);
		page.setTotal(total);
		return lam.selectLogKaiyuanAgentAll(page,logKaiyuanAgentList);
	}

}
