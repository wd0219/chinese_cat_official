package com.stylefeng.guns.modular.huamao.model;

public class SktOrdersList extends SktOrders{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 点评名称
     */
    private String shopName;
    /**
     * 店铺手机
     */
    private String telephone;
    /**
     * 营业执照所在省id
     */
    private Integer provinceId;
    /**
     * 营业执照所在市id
     */
    private Integer cityId;
    /**
     * 营业执照所在区县id
     */
    private Integer areaId;
    /**
     * 查询时间开始
     */
    private String beginTime;
    /**
     * 查询时间结束
     */
    private String endTime;
    
    
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Integer getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getAreaId() {
		return areaId;
	}
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
}
