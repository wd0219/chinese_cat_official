package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreeze;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreezeList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanFreezeService;

import java.util.List;

/**
 * 待发华宝货款记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 11:11:51
 */
@Controller
@RequestMapping("/logKaiyuanFreeze")
public class LogKaiyuanFreezeController extends BaseController {

	private String PREFIX = "/huamao/logKaiyuanFreeze/";

	@Autowired
	private ILogKaiyuanFreezeService logKaiyuanFreezeService;

	/**
	 * 跳转到待发华宝货款记录首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, Model model) {
		// 参数是为了在用户点击现金查看积分详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logKaiyuanFreeze.html";
	}

	/**
	 * 跳转到添加待发华宝货款记录
	 */
	@RequestMapping("/logKaiyuanFreeze_add")
	public String logKaiyuanFreezeAdd() {
		return PREFIX + "logKaiyuanFreeze_add.html";
	}

	/**
	 * 跳转到修改待发华宝货款记录
	 */
	@RequestMapping("/logKaiyuanFreeze_update/{logKaiyuanFreezeId}")
	public String logKaiyuanFreezeUpdate(@PathVariable Integer logKaiyuanFreezeId, Model model) {
		LogKaiyuanFreeze logKaiyuanFreeze = logKaiyuanFreezeService.selectById(logKaiyuanFreezeId);
		model.addAttribute("item", logKaiyuanFreeze);
		LogObjectHolder.me().set(logKaiyuanFreeze);
		return PREFIX + "logKaiyuanFreeze_edit.html";
	}

	/**
	 * 获取待发华宝货款记录列表：{userPhone2}商家待发华宝营业额传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone,
			LogKaiyuanFreezeList logKaiyuanFreezeList) {
		// 判断如果不是‘手机号’并且logKaiyuanFreezeList.getUserPhone()不是null说明要看商家待发华宝营业额详情，是‘手机号’并且logKaiyuanFreezeList.getUserPhone()是''或是有数据说明是普通的查询所有的商家待发华宝营业额详情
		if (!phone.equals("手机号") && logKaiyuanFreezeList.getUserPhone() == null) {
			logKaiyuanFreezeList.setUserPhone(phone);
		}
		Page<LogKaiyuanFreezeList> page = new PageFactory<LogKaiyuanFreezeList>().defaultPage();
		List<LogKaiyuanFreezeList> logKaiyuanFreezeLists = logKaiyuanFreezeService.selectLogKaiyuanFreezeAll(page,logKaiyuanFreezeList);
		page.setRecords(logKaiyuanFreezeLists);
		return super.packForBT(page);
		// return logKaiyuanFreezeService.selectList(null);
	}

	/**
	 * 新增待发华宝货款记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogKaiyuanFreeze logKaiyuanFreeze) {
		logKaiyuanFreezeService.insert(logKaiyuanFreeze);
		return SUCCESS_TIP;
	}

	/**
	 * 删除待发华宝货款记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logKaiyuanFreezeId) {
		logKaiyuanFreezeService.deleteById(logKaiyuanFreezeId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改待发华宝货款记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogKaiyuanFreeze logKaiyuanFreeze) {
		logKaiyuanFreezeService.updateById(logKaiyuanFreeze);
		return SUCCESS_TIP;
	}

	/**
	 * 待发华宝货款记录详情
	 */
	@RequestMapping(value = "/detail/{logKaiyuanFreezeId}")
	@ResponseBody
	public Object detail(@PathVariable("logKaiyuanFreezeId") Integer logKaiyuanFreezeId) {
		return logKaiyuanFreezeService.selectById(logKaiyuanFreezeId);
	}
}
