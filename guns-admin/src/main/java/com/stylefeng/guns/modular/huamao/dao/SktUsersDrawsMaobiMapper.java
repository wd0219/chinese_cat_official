package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktUsersDrawsMaobi;
import com.stylefeng.guns.modular.huamao.model.SktUsersDrawsMaobiDto;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

/**
 * <p>
 * 用户提现表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-30
 */
public interface SktUsersDrawsMaobiMapper extends BaseMapper<SktUsersDrawsMaobi> {

	List<SktUsersDrawsMaobiDto> selectMaoBiList(@Param("page")Page<SktUsersDrawsMaobiDto> page,
			@Param("sktUsersDrawsMaobiDto")SktUsersDrawsMaobiDto sktUsersDrawsMaobiDto);

	Integer selectMaoBiListCount(@Param("sktUsersDrawsMaobiDto") SktUsersDrawsMaobiDto sktUsersDrawsMaobiDto);

}
