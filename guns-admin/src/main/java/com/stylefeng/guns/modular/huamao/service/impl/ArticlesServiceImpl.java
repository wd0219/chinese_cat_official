package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Articles;
import com.stylefeng.guns.modular.huamao.dao.ArticlesMapper;
import com.stylefeng.guns.modular.huamao.service.IArticlesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章记录表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
@Service
public class ArticlesServiceImpl extends ServiceImpl<ArticlesMapper, Articles> implements IArticlesService {

	@Override
	public List<Map<String, Object>> selectArticles(Page<Map<String,Object>> page, Articles articles) {
		Integer total = baseMapper.selectArticlesCount(articles);
		page.setTotal(total);
		return baseMapper.selectArticles(page,articles);
	}

}
