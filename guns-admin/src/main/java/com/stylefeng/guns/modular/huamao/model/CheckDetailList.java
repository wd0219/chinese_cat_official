package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * 用户每日对账返回
 * 
 * @author gxz
 *
 */
public class CheckDetailList extends CheckDetail {

	/**
	 * 用户名
	 */
	private String loginName;
	/**
	 * 查询时的开始时间
	 */
	private Integer beginTime;
	/**
	 * 查询时的结束时间
	 */
	private Integer endTime;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Integer beginTime) {
		this.beginTime = beginTime;
	}

	public Integer getEndTime() {
		return endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}

}
