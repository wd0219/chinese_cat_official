package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogCashFreeze;
import com.stylefeng.guns.modular.huamao.model.LogCashFreezeList;
import com.stylefeng.guns.modular.huamao.service.ILogCashFreezeService;

import java.util.List;

/**
 * 待发现金记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-16 15:44:41
 */
@Controller
@RequestMapping("/logCashFreeze")
public class LogCashFreezeController extends BaseController {

	private String PREFIX = "/huamao/logCashFreeze/";

	@Autowired
	private ILogCashFreezeService logCashFreezeService;

	/**
	 * 跳转到待发现金记录首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, Model model) {
		// 参数是为了在用户点击现金查看积分详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logCashFreeze.html";
	}

	/**
	 * 跳转到添加待发现金记录
	 */
	@RequestMapping("/logCashFreeze_add")
	public String logCashFreezeAdd() {
		return PREFIX + "logCashFreeze_add.html";
	}

	/**
	 * 跳转到修改待发现金记录
	 */
	@RequestMapping("/logCashFreeze_update/{logCashFreezeId}")
	public String logCashFreezeUpdate(@PathVariable Integer logCashFreezeId, Model model) {
		LogCashFreeze logCashFreeze = logCashFreezeService.selectById(logCashFreezeId);
		model.addAttribute("item", logCashFreeze);
		LogObjectHolder.me().set(logCashFreeze);
		return PREFIX + "logCashFreeze_edit.html";
	}

	/**
	 * 获取待发现金记录列表：{userPhone2}用户查看待发现金详情传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone,
			LogCashFreezeList logCashFreezeList) {
		// 判断如果不是‘手机号’并且logCashFreezeList.getUserPhone()不是null说明要看用户积分详情，是‘手机号’并且logCashFreezeList.getUserPhone()是''或是有数据说明是普通的查询所有的现金详情
		if (!phone.equals("手机号") && logCashFreezeList.getUserPhone() == null) {
			logCashFreezeList.setUserPhone(phone);
		}
		Page<LogCashFreezeList> page = new PageFactory<LogCashFreezeList>().defaultPage();
		List<LogCashFreezeList> logCashFreezeLists = logCashFreezeService.selectLogCashFreezeAll(page,logCashFreezeList);
		// return logCashFreezeService.selectList(null);
		page.setRecords(logCashFreezeLists);
		return super.packForBT(page);
	}

	/**
	 * 新增待发现金记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogCashFreeze logCashFreeze) {
		logCashFreezeService.insert(logCashFreeze);
		return SUCCESS_TIP;
	}

	/**
	 * 删除待发现金记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logCashFreezeId) {
		logCashFreezeService.deleteById(logCashFreezeId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改待发现金记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogCashFreeze logCashFreeze) {
		logCashFreezeService.updateById(logCashFreeze);
		return SUCCESS_TIP;
	}

	/**
	 * 待发现金记录详情
	 */
	@RequestMapping(value = "/detail/{logCashFreezeId}")
	@ResponseBody
	public Object detail(@PathVariable("logCashFreezeId") Integer logCashFreezeId) {
		return logCashFreezeService.selectById(logCashFreezeId);
	}
}
