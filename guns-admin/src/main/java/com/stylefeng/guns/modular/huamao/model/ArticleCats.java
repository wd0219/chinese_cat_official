package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
@TableName("skt_article_cats")
public class ArticleCats extends Model<ArticleCats> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "catId", type = IdType.AUTO)
    private Integer catId;
    private Integer parentId;
    private Integer catType;
    private Integer isShow;
    private String catName;
    private Integer catSort;
    private Integer dataFlag;
    private Date createTime;


    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getCatType() {
        return catType;
    }

    public void setCatType(Integer catType) {
        this.catType = catType;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Integer getCatSort() {
        return catSort;
    }

    public void setCatSort(Integer catSort) {
        this.catSort = catSort;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.catId;
    }

    @Override
    public String toString() {
        return "ArticleCats{" +
        "catId=" + catId +
        ", parentId=" + parentId +
        ", catType=" + catType +
        ", isShow=" + isShow +
        ", catName=" + catName +
        ", catSort=" + catSort +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
