package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktPointuser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 点赞用户表(预留表) Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface SktPointuserMapper extends BaseMapper<SktPointuser> {

}
