package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.model.UsersBankcardsList;

/**
 * <p>
 * 用户的银行卡表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface IUsersBankcardsService extends IService<UsersBankcards> {

	// 用户银行卡表展示
	public List<UsersBankcardsList> selectUsersBankcardsAll(Page<UsersBankcardsList> page, UsersBankcardsList usersBankcardsList);

	// 通过id进行逻辑删除
	public void deleteById2(Integer usersBankcardsId);
	/**
	 * 根据用户查询用户银行卡
	 * @param usersBankcardsList
	 * @return
	 */
	public List<UsersBankcards> selectUserBranks(UsersBankcardsList usersBankcardsList);

}
