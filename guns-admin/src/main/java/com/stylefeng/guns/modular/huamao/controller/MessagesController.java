package com.stylefeng.guns.modular.huamao.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;
import com.stylefeng.guns.modular.huamao.service.IMessagesService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 商城消息控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 10:33:09
 */
@Controller
@RequestMapping("/messages")
public class MessagesController extends BaseController {

    private String PREFIX = "/huamao/messages/";

    @Autowired
    private IMessagesService messagesService;

    /**
     * 跳转到商城消息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "messages.html";
    }

    /**
     * 跳转到添加商城消息
     */
    @RequestMapping("/messages_add")
    public String messagesAdd() {
        return PREFIX + "messages_add.html";
    }

    /**
     * 跳转到修改商城消息
     */
    @RequestMapping("/messages_update/{messagesId}")
    public String messagesUpdate(@PathVariable Integer messagesId, Model model) {
        Messages messages = messagesService.selectById(messagesId);
        model.addAttribute("item", messages);
        LogObjectHolder.me().set(messages);
        return PREFIX + "messages_edit.html";
    }

    /**
     * 获取商城消息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(MessagesList messagesList) {
        Page<MessagesList> page = new PageFactory<MessagesList>().defaultPage();
        List<MessagesList> messagesLists = messagesService.selectMessagesAll(page,messagesList);
        page.setRecords(messagesLists);
        return super.packForBT(page);

    }

    /**
     * 新增商城消息：li为空说明是给所有商家发送，不为空为给指定用户发送
     * 消息类型：0手工 1系统
     */
    @RequestMapping(value = "/addMessages")
    @ResponseBody
    public Object addMessages(@RequestParam(value = "usersId[]", required = false) List<Integer> usersId, @RequestParam(value = "msgContent") String msgContent) {
        Messages messages = new Messages();
        msgContent = org.springframework.web.util.HtmlUtils.htmlUnescape(msgContent);
        messages.setMsgContent(msgContent);
        // 发送者id为操作员id
        ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
        // 传入service层
        boolean b = messagesService.addMessages(0,shiroUser.getId(), usersId,msgContent,null);
        // 如果是true说明成功，否则失败
        if (b) {
            return b;
        }
        return null;
    }

    /**
     * 通过ID删除代理公司银行卡
     */
    @RequestMapping(value = "/deleteById")
    @ResponseBody
    public Object deleteById(@RequestParam Integer id) {
        // 通过传过来的ID进行逻辑删除
        messagesService.deleteById2(id);
        return SUCCESS_TIP;
    }

    /**
     * 删除商城消息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer messagesId) {
        messagesService.deleteById(messagesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商城消息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Messages messages) {
        messagesService.updateById(messages);
        return SUCCESS_TIP;
    }

    /**
     * 跳转到消息列表页面
     */
    @RequestMapping("/getList")
    public String getList() {
        return PREFIX + "messagesList.html";
    }

    /**
     * 跳转到发送消息页面
     */
    @RequestMapping("/sendMessage")
    public String sendMessage() {
        return PREFIX + "messages.html";
    }

    /**
     * 跳转到消息内容详情页面
     */
    @RequestMapping("/messageDetail")
    public String messageDetail(Integer id, Model model) {
        Messages messages = messagesService.selectById(id);
        String msgContent = messages.getMsgContent();
        model.addAttribute("msgContent", msgContent.toString());
        return PREFIX + "messageDetail.html";
    }

    /**
     * 商城消息详情
     */
    @RequestMapping(value = "/detail/{messagesId}")
    @ResponseBody
    public Object detail(@PathVariable("messagesId") Integer messagesId) {
        return messagesService.selectById(messagesId);
    }

    /**
     * 获取用户
     */
    @RequestMapping(value = "/getUsers")
    @ResponseBody
    public Object getUsers(String userPhone) {
        // 获取用户
        List<MessagesList> users = messagesService.getUsers(userPhone);
        // 计数器
        // int cont = 0;
        // // 拼接成前台需要的格式
        // for (MessagesList messagesList : users) {
        // if (cont == users.size() - 1) {
        // list += messagesList.getReceiveName() + "',";
        // list += "id:'" + messagesList.getReceiveUserId() + "'}]";
        // } else {
        // list += messagesList.getReceiveName() + "',";
        // list += "id:'" + messagesList.getReceiveUserId() + "'},{name:'";
        // }
        // cont++;
        // }
        return users;
    }

    /**
     * 图片上传阿里云
     *
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/aliyunUpload")
    @ResponseBody
    public Map<String, Object> upload(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        return messagesService.upload(request);
    }
}
