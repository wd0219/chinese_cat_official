package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 代理公司登陆记录表
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
@TableName("skt_log_agents_logins")
public class SktLogAgentsLogins extends Model<SktLogAgentsLogins> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "loginId", type = IdType.AUTO)
    private Integer loginId;
    /**
     * 职员ID
     */
    private Integer agentId;
    /**
     * 登陆时间
     */
    private Date loginTime;
    /**
     * 登陆IP
     */
    private String loginIp;
    
    private String agentName;
    private String createTime;
    private String checkTime;

    
   

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    @Override
    protected Serializable pkVal() {
        return this.loginId;
    }

	@Override
	public String toString() {
		return "SktLogAgentsLogins [loginId=" + loginId + ", agentId=" + agentId + ", loginTime=" + loginTime
				+ ", loginIp=" + loginIp + ", agentName=" + agentName + ", createTime=" + createTime + ", checkTime="
				+ checkTime + "]";
	}

	public SktLogAgentsLogins(Integer loginId, Integer agentId, Date loginTime, String loginIp, String agentName,
			String createTime, String checkTime) {
		super();
		this.loginId = loginId;
		this.agentId = agentId;
		this.loginTime = loginTime;
		this.loginIp = loginIp;
		this.agentName = agentName;
		this.createTime = createTime;
		this.checkTime = checkTime;
	}

	public SktLogAgentsLogins() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    
}
