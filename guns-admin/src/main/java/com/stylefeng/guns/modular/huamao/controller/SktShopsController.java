package com.stylefeng.guns.modular.huamao.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.ErrorTip;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.exception.ServiceExceptionEnum;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;


import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.model.SktShops2;
import com.stylefeng.guns.modular.huamao.service.ISktShopsService;


/**
 * 商家信息控制器
 *
 * @author fengshuonan
 * @Date 2018-04-14 14:19:44
 */
@Controller
@RequestMapping("/sktShops")
public class SktShopsController extends BaseController {

    private String PREFIX = "/huamao/sktShops/";

    @Autowired
    private ISktShopsService sktShopsService;

    /**
     * 跳转到商家信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShops.html";
    }

    /**
     * 跳转到添加商家信息
     */
    @RequestMapping("/sktShops_add")
    public String sktShopsAdd() {
        return PREFIX + "sktShops_add.html";
    }

    /**
     * 跳转到修改商家信息
     */
    @RequestMapping("/sktShops_update/{sktShopsId}/{shopType}")
    public String sktShopsUpdate(@PathVariable Integer shopType,@PathVariable Integer sktShopsId, Model model,SktShops sktShops) {
    	
    	sktShops.setShopId(sktShopsId);
        List<SktShops> list = sktShopsService.sktShopsfindAll(sktShops);
        SktShops sktShops1 = list.get(0);

        String legalPersonImg = sktShops1.getLegalPersonImg();
        String allUrl = UrlUtils.getAllUrl(legalPersonImg);
        sktShops1.setLegalPersonImg(allUrl);

        String licenseImg = sktShops1.getLicenseImg();
        String allUrl1 = UrlUtils.getAllUrl(licenseImg);
        sktShops1.setLicenseImg(allUrl1);

        String codeImg = sktShops1.getCodeImg();
        String allUrl2 = UrlUtils.getAllUrl(codeImg);
        sktShops1.setCodeImg(allUrl2);

        String taxImg = sktShops1.getTaxImg();
        String allUrl3 = UrlUtils.getAllUrl(taxImg);
        sktShops1.setTaxImg(allUrl3);

        String logo = sktShops1.getLogo();
        String allUrl4 = UrlUtils.getAllUrl(logo);
        sktShops1.setLogo(allUrl4);

        String storeImg1 = sktShops1.getStoreImg1();
        String allUrl5 = UrlUtils.getAllUrl(storeImg1);
        sktShops1.setStoreImg1(allUrl5);

        String storeImg2 = sktShops1.getStoreImg2();
        String allUrl6 = UrlUtils.getAllUrl(storeImg2);
        sktShops1.setStoreImg2(allUrl6);

        String storeImg3 = sktShops1.getStoreImg3();
        String allUrl7 = UrlUtils.getAllUrl(storeImg3);
        sktShops1.setStoreImg3(allUrl7);

        String shopImg1 = sktShops1.getShopImg1();
        String allUrl8 = UrlUtils.getAllUrl(shopImg1);
        sktShops1.setShopImg1(allUrl8);

        String shopImg2 = sktShops1.getShopImg2();
        String allUrl9 = UrlUtils.getAllUrl(shopImg2);
        sktShops1.setShopImg2(allUrl9);

        String shopImg3 = sktShops1.getShopImg3();
        String allUrl10 = UrlUtils.getAllUrl(shopImg3);
        sktShops1.setShopImg3(allUrl10);
        model.addAttribute("item",sktShops1);
        model.addAttribute("shopType",shopType);

        //LogObjectHolder.me().set(sktShops);
        return PREFIX + "sktShops_edit.html";
    }

    /**
     * 跳转到修改商家信息（线上商家）
     */
    @RequestMapping("/sktShops_update1/{sktShopsId}")
    public String sktShopUpdate(@PathVariable Integer sktShopsId, Model model,SktShops sktShops) {

        sktShops.setShopId(sktShopsId);
        List<SktShops> list = sktShopsService.sktShopsfindAll(sktShops);
        SktShops sktShops1 = list.get(0);

        String legalPersonImg = sktShops1.getLegalPersonImg();
        String allUrl = UrlUtils.getAllUrl(legalPersonImg);
        sktShops1.setLegalPersonImg(allUrl);

        String licenseImg = sktShops1.getLicenseImg();
        String allUrl1 = UrlUtils.getAllUrl(licenseImg);
        sktShops1.setLicenseImg(allUrl1);

        String codeImg = sktShops1.getCodeImg();
        String allUrl2 = UrlUtils.getAllUrl(codeImg);
        sktShops1.setCodeImg(allUrl2);

        String taxImg = sktShops1.getTaxImg();
        String allUrl3 = UrlUtils.getAllUrl(taxImg);
        sktShops1.setTaxImg(allUrl3);

        String logo = sktShops1.getLogo();
        String allUrl4 = UrlUtils.getAllUrl(logo);
        sktShops1.setLogo(allUrl4);

        String storeImg1 = sktShops1.getStoreImg1();
        String allUrl5 = UrlUtils.getAllUrl(storeImg1);
        sktShops1.setStoreImg1(allUrl5);

        String storeImg2 = sktShops1.getStoreImg2();
        String allUrl6 = UrlUtils.getAllUrl(storeImg2);
        sktShops1.setStoreImg2(allUrl6);

        String storeImg3 = sktShops1.getStoreImg3();
        String allUrl7 = UrlUtils.getAllUrl(storeImg3);
        sktShops1.setStoreImg3(allUrl7);

        String shopImg1 = sktShops1.getShopImg1();
        String allUrl8 = UrlUtils.getAllUrl(shopImg1);
        sktShops1.setShopImg1(allUrl8);

        String shopImg2 = sktShops1.getShopImg2();
        String allUrl9 = UrlUtils.getAllUrl(shopImg2);
        sktShops1.setShopImg2(allUrl9);

        String shopImg3 = sktShops1.getShopImg3();
        String allUrl10 = UrlUtils.getAllUrl(shopImg3);
        sktShops1.setShopImg3(allUrl10);
        model.addAttribute("item",sktShops1);
        //LogObjectHolder.me().set(sktShops);
        return PREFIX + "sktShop_edit.html";
    }

    /**
     * 获取商家信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktShopsService.selectList(null);
    }
    /**
     * 获取商家信息列表
     */
    @RequestMapping(value = "/list2")
    @ResponseBody
    public Object list2(String condition) {
        return sktShopsService.selectList(null);
    }

    /**
     * 新增商家信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShops sktShops) {
        sktShopsService.insert(sktShops);
        return SUCCESS_TIP;
    }

    /**
     * 删除商家信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopsId) {
        sktShopsService.deleteById(sktShopsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商家信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShops sktShops)  throws  RuntimeException{

     //   sktShopsService.updateById(sktShops);
        Map<String, Object> map = sktShopsService.updateSktShops(sktShops);
        if ("0".equals(map.get("code").toString())){
              throw new RuntimeException();
            //return error;
        }else {
            return SUCCESS_TIP;
        }

    }

    /**
     * 修改商家信息
     */
    @RequestMapping(value = "/update1")
    @ResponseBody
    public Object update1(SktShops sktShops) {
        sktShopsService.updateById(sktShops);
        return SUCCESS_TIP;
    }

    /**
     * 商家信息详情
     */
    @RequestMapping(value = "/detail/{sktShopsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopsId") Integer sktShopsId) {
        return sktShopsService.selectById(sktShopsId);
    }
    /*
     * 店铺街展示
     * */
    private Integer pageSize5 = 10;
    private Integer pageNum5 = 1;
    @RequestMapping(value="/findShopsAll")
    @ResponseBody
    public Object findShopsAll(Integer pageSize1,Integer pageNum1){
    	pageSize5 = pageSize1!=null?pageSize1:pageSize;
    	pageNum5 = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
   	 	PageHelper.startPage(pageNum,pageSize);
    	List<Map<String, Object>> list = sktShopsService.findShopsAll();
    	for(Map<String, Object> x : list){
    		List<Map<String,Object>> list2 = sktShopsService.findShopsByIdtj((Integer)x.get("shopId"));
    		x.put("isRecom",list2);
    	}
    	PageInfo<Map<String, Object>> info = new PageInfo<>(list);
    	list = info.getList();
    	 return list;
    }
    /*
     * 店铺街店家推荐商品
     * */
    @RequestMapping(value="/findShopsByIdtj")
    @ResponseBody
    public Object findShopsByIdtj(Integer shopId){
    	List<Map<String, Object>> list = sktShopsService.findShopsByIdtj(shopId);
    	return list;
    }
    /*
     * 店铺街查询行业
     * */
    @RequestMapping(value="/findShopsHyName")
    @ResponseBody
    public Object findShopsHyName(){
    	List<Map<String, Object>> list = sktShopsService.findShopsHyName();
    	return list;
    }
    /*
     * 店铺街根据行业ID查询店铺
     * */
    private Integer pageSize4 = 20;
    private Integer pageNum4 = 1;
    @RequestMapping(value="/selectByHyIdShops")
    @ResponseBody
    public Object selectByHyIdShops(Integer hyId,Integer pageSize1,Integer pageNum1){
    	pageSize4 = pageSize1!=null?pageSize1:pageSize;
    	pageNum4 = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
   	 	PageHelper.startPage(pageNum,pageSize);
    	List<Map<String, Object>> map = sktShopsService.selectByHyIdShops(hyId);
    	PageInfo<Map<String, Object>> info = new PageInfo<>(map);
    	map = info.getList();
    	return map;
    }
    /*
     * 查询热门品牌
     * */
    private Integer pageSize3 = 20;
    private Integer pageNum3 = 1;
    @RequestMapping(value="/findBrandId")
    @ResponseBody
    public Object findBrandId(Integer pageSize1,Integer pageNum1){
    	pageSize3 = pageSize1!=null?pageSize1:pageSize;
    	pageNum3 = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
   	 	PageHelper.startPage(pageNum,pageSize);
    	List<Map<String, Object>> findBrandId = sktShopsService.findBrandId();
    	PageInfo<Map<String, Object>> info = new PageInfo<>(findBrandId);
    	findBrandId = info.getList();
    	return findBrandId;
    }
    /*
     * 根据店铺ID查询商品
     * */
    @RequestMapping(value="/selectShopsGoodsId")
    @ResponseBody
    public Object selectShopsGoodsId(Integer shopId,Integer pageSize1,Integer pageNum1){
    	pageSize2 = pageSize1!=null?pageSize1:pageSize;
    	pageNum2 = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
   	 	PageHelper.startPage(pageNum,pageSize);
    	List<Map<String, Object>> selectShopsGoodsId = sktShopsService.selectShopsGoodsId(shopId);
    	PageInfo<Map<String, Object>> info = new PageInfo<>(selectShopsGoodsId);
     	selectShopsGoodsId = info.getList();
     	return selectShopsGoodsId;
    }
    private Integer pageSize = 5;
    private Integer pageNum = 1;
    /*
     *自营超市展示 
     * */
     @RequestMapping(value="/selectShopsZy")
     @ResponseBody
     public Object selectShopsZy(Integer pageSize1,Integer pageNum1){
    	 pageSize = pageSize1!=null?pageSize1:pageSize;
    	 pageNum = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
    	PageHelper.startPage(pageNum,pageSize);
     	List<Map<String, Object>> list = sktShopsService.selectShopsZy();
     	for(Map<String, Object> x : list){
     		List<Map<String,Object>> list2 = sktShopsService.findShopsByIdtj((Integer)x.get("shopId"));
     		x.put("isRecom",list2);
     	}
     	PageInfo<Map<String, Object>> info = new PageInfo<>(list);
     	list = info.getList();
     	 return list;
     } 
     /*
      * 根据LOGO 店铺名 loginName查询店铺ID selectLogoShops
      * */
     @RequestMapping(value="/selectShopNameShops")
     @ResponseBody
     public Object selectShopNameShops(SktShops2 sktShops2){
    	 List<Map<String, Object>> list = sktShopsService.selectShopNameShops(sktShops2);
    	 return list;
     }
     
     /*
      * 根据LOGO 店铺名 loginName查询店铺ID 
      * */
     @RequestMapping(value="/selectLogoShops")
     @ResponseBody
     public Object selectLogoShops(SktShops2 sktShops2){
    	 List<Map<String, Object>> list = sktShopsService.selectLogoShops(sktShops2);
    	 return list;
     } 
     /*
      * 根据LOGO 店铺名 loginName查询店铺ID 
      * */
     @RequestMapping(value="/selectLoginNameShops")
     @ResponseBody
     public Object selectLoginNameShops(SktShops2 sktShops2){
    	 List<Map<String, Object>> list = sktShopsService.selectLoginNameShops(sktShops2);
    	 return list;
     }
     /*
      * 根据店铺ID查询商品
      * */ 
     private Integer pageSize2 = 10;
     private Integer pageNum2 = 1;
     @RequestMapping(value="/selectShopsZyGoodsId")
     @ResponseBody
     public Object selectShopsZyGoodsId(Integer shopId,Integer pageSize1,Integer pageNum1){
    	 pageSize2 = pageSize1!=null?pageSize1:pageSize;
    	 pageNum2 = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
    	PageHelper.startPage(pageNum,pageSize);
     	List<Map<String, Object>> selectShopsGoodsId = sktShopsService.selectShopsZyGoodsId(shopId);
     	PageInfo<Map<String, Object>> info = new PageInfo<>(selectShopsGoodsId);
     	selectShopsGoodsId = info.getList();
     	return selectShopsGoodsId;
     }
      
      /*
       * 首页轮播图
       * */ 
      @RequestMapping(value="/findWebLunbo")
      @ResponseBody
      public Object findWebLunbo(){
      	List<Map<String, Object>> findWebLunbo = sktShopsService.findWebLunbo();
      	return findWebLunbo;
      }
}
