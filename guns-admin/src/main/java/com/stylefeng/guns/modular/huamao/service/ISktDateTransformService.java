package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktDateTransform;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 积分转换开元宝每日记录表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
public interface ISktDateTransformService extends IService<SktDateTransform> {
	
	public List<SktDateTransform> sktDateTransformfindAll(Page<SktDateTransform> page, SktDateTransform sktDateTransform);

	public List<SktDateTransform> SktDateTransformfindAll2(Page<SktDateTransform> page,
			SktDateTransform sktDateTransform2);
}
