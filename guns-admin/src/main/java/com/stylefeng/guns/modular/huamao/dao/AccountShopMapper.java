package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.AccountShop;
import com.stylefeng.guns.modular.huamao.model.AccountShopList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商家账户表(包括线上和线下) Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-12
 */
public interface AccountShopMapper extends BaseMapper<AccountShop> {
	// 展示用户账户列表
	public List<AccountShopList> selectAccountShopAll(@Param("page") Page<AccountShopList> page, @Param("accountShopList") AccountShopList accountShopList);

    AccountShop findShopId(Integer userId);

	AccountShop findKaiyuanTurnover(Integer userId);

	Integer updateKaiyuanByuserId(AccountShop findKaiyuanTurnover);

    AccountShop findShopById(Integer userId);

	Integer selectAccountShopAllCount(@Param("accountShopList") AccountShopList accountShopList);
}
