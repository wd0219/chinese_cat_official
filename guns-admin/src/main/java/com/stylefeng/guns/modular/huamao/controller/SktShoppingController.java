package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.huamao.model.SktShoppingDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.SktShopping;
import com.stylefeng.guns.modular.huamao.service.ISktShoppingService;

import java.util.List;

/**
 * 购物券流水控制器
 *
 * @author fengshuonan
 * @Date 2018-09-29 09:27:38
 */
@Controller
@RequestMapping("/sktShopping")
public class SktShoppingController extends BaseController {

    private String PREFIX = "/huamao/sktShopping/";

    @Autowired
    private ISktShoppingService sktShoppingService;

    /**
     * 跳转到购物券流水首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopping.html";
    }

    /**
     * 跳转到添加购物券流水
     */
    @RequestMapping("/sktShopping_add")
    public String sktShoppingAdd() {
        return PREFIX + "sktShopping_add.html";
    }

    /**
     * 跳转到修改购物券流水
     */
    @RequestMapping("/sktShopping_update/{sktShoppingId}")
    public String sktShoppingUpdate(@PathVariable Integer sktShoppingId, Model model) {
        SktShopping sktShopping = sktShoppingService.selectById(sktShoppingId);
        model.addAttribute("item",sktShopping);
        LogObjectHolder.me().set(sktShopping);
        return PREFIX + "sktShopping_edit.html";
    }

    /**
     * 获取购物券流水列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktShoppingDTO sktShoppingDTO) {
        Page<SktShoppingDTO> page = new PageFactory<SktShoppingDTO>().defaultPage();
        List<SktShoppingDTO> list = sktShoppingService.selectAll(page,sktShoppingDTO);
        page.setRecords(list);
        return super.packForBT(page);
//        return sktShoppingService.selectList(null);
    }

    /**
     * 新增购物券流水
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopping sktShopping) {
        sktShoppingService.insert(sktShopping);
        return SUCCESS_TIP;
    }

    /**
     * 删除购物券流水
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShoppingId) {
        sktShoppingService.deleteById(sktShoppingId);
        return SUCCESS_TIP;
    }

    /**
     * 修改购物券流水
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopping sktShopping) {
        sktShoppingService.updateById(sktShopping);
        return SUCCESS_TIP;
    }

    /**
     * 购物券流水详情
     */
    @RequestMapping(value = "/detail/{sktShoppingId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShoppingId") Integer sktShoppingId) {
        return sktShoppingService.selectById(sktShoppingId);
    }
}
