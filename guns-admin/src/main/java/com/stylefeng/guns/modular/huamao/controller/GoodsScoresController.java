package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.GoodsScores;
import com.stylefeng.guns.modular.huamao.service.IGoodsScoresService;

/**
 * 商品评分控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:31:33
 */
@Controller
@RequestMapping("/goodsScores")
public class GoodsScoresController extends BaseController {

    private String PREFIX = "/huamao/goodsScores/";

    @Autowired
    private IGoodsScoresService goodsScoresService;

    /**
     * 跳转到商品评分首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "goodsScores.html";
    }

    /**
     * 跳转到添加商品评分
     */
    @RequestMapping("/goodsScores_add")
    public String goodsScoresAdd() {
        return PREFIX + "goodsScores_add.html";
    }

    /**
     * 跳转到修改商品评分
     */
    @RequestMapping("/goodsScores_update/{goodsScoresId}")
    public String goodsScoresUpdate(@PathVariable Integer goodsScoresId, Model model) {
        GoodsScores goodsScores = goodsScoresService.selectById(goodsScoresId);
        model.addAttribute("item",goodsScores);
        LogObjectHolder.me().set(goodsScores);
        return PREFIX + "goodsScores_edit.html";
    }

    /**
     * 获取商品评分列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return goodsScoresService.selectList(null);
    }

    /**
     * 新增商品评分
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(GoodsScores goodsScores) {
        goodsScoresService.insert(goodsScores);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品评分
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer goodsScoresId) {
        goodsScoresService.deleteById(goodsScoresId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品评分
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(GoodsScores goodsScores) {
        goodsScoresService.updateById(goodsScores);
        return SUCCESS_TIP;
    }

    /**
     * 商品评分详情
     */
    @RequestMapping(value = "/detail/{goodsScoresId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsScoresId") Integer goodsScoresId) {
        return goodsScoresService.selectById(goodsScoresId);
    }
}
