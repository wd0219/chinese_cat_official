package com.stylefeng.guns.modular.huamao.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;
import com.stylefeng.guns.modular.huamao.model.UsersRechargeDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersRechargeService;

/**
 * 用户充值控制器
 *
 * @author fengshuonan
 * @Date 2018-04-13 17:25:56
 */
@Controller
@RequestMapping("/usersRecharge")
public class UsersRechargeController extends BaseController {

	private String PREFIX = "/huamao/usersRecharge/";

	@Autowired
	private IUsersRechargeService usersRechargeService;

	/**
	 * 跳转到用户充值首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "usersRecharge.html";
	}

	/**
	 * 跳转到添加用户充值
	 */
	@RequestMapping("/usersRecharge_add")
	public String usersRechargeAdd() {
		return PREFIX + "usersRecharge_add.html";
	}

	/**
	 * 跳转到修改用户充值
	 */
	@RequestMapping("/usersRecharge_update/{usersRechargeId}")
	public String usersRechargeUpdate(@PathVariable Integer usersRechargeId, Model model) {
		UsersRecharge usersRecharge = usersRechargeService.selectById(usersRechargeId);
		String url = usersRecharge.getOrderImg();
		Map<String,Object> map = new HashMap<>();
		if(url != null && !"".equals(url)){
			String[] allurl = url.split(",");
			for(int i = 0; i < allurl.length; i++){
				map.put("orderImg"+i,UrlUtils.getAllUrl(allurl[i]));
			}
		}else{
			map.put("orderImg"+0,"/static/img/girl.gif");
			map.put("orderImg"+1,"/static/img/girl.gif");
			map.put("orderImg"+2,"/static/img/girl.gif");
		}
		map.put("remark", usersRecharge.getRemark());
//		String allUrl = UrlUtils.getAllUrl(usersRecharge.getOrderImg());
//		if(allUrl==null || "".equals(allUrl)){
//			usersRecharge.setOrderImg("/static/img/girl.gif");
//		}else{
//			usersRecharge.setOrderImg(allUrl);
//		}
//		model.addAttribute("item", usersRecharge);
		model.addAttribute("item", map);
		LogObjectHolder.me().set(usersRecharge);
		return PREFIX + "usersRecharge_xiangqing.html";
	}
	/**
    * 跳转到通过
    */
   @RequestMapping("/usersRecharge_pass/{shaId}")
   public String agentsApplysPass(@PathVariable(value="shaId") Integer shaId, Model model) {
	   model.addAttribute("shaId",shaId);
       return PREFIX + "usersRecharge_pass.html";
   }
   /**
    * 跳转到刷新
    */
   @RequestMapping("/usersRecharge_refuse/{shaId}")
   public String agentsApplysRefuse(@PathVariable(value="shaId") Integer shaId, Model model) {
	   model.addAttribute("shaId",shaId);
       return PREFIX + "usersRecharge_refuse.html";
   }
   
	/**
	 * 获取用户充值列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(UsersRechargeDTO usersRechargeDTO) {
		Page<UsersRechargeDTO> page = new PageFactory<UsersRechargeDTO>().defaultPage();
		List<UsersRechargeDTO> list = usersRechargeService.showUserDengDanInfo(page,usersRechargeDTO);
		page.setRecords(list);
        return super.packForBT(page);
	}

	/**
	 * 新增用户充值
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(UsersRecharge usersRecharge) {
		usersRechargeService.insert(usersRecharge);
		return SUCCESS_TIP;
	}

	/**
	 * 删除用户充值
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer usersRechargeId) {
		usersRechargeService.deleteById(usersRechargeId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改用户充值
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(UsersRecharge usersRecharge) {
		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
		boolean b = usersRechargeService.updateUserRecharge(usersRecharge,shiroUser.getId());
		if (b==true){
			return SUCCESS_TIP;
		}
		return ERROR;
	}

	/**
	 * 用户充值详情
	 */
	@RequestMapping(value = "/detail/{usersRechargeId}")
	@ResponseBody
	public Object detail(@PathVariable("usersRechargeId") Integer usersRechargeId) {
		return usersRechargeService.selectById(usersRechargeId);
	}

}
