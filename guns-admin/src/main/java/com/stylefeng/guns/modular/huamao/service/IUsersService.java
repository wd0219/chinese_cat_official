package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersDTO;
import com.stylefeng.guns.modular.huamao.model.UsersLoginDTO;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-10
 */
public interface IUsersService extends IService<Users> {
	/**
	 * 查询页面显示数据
	 * @param page 
	 * @param usersDTO
	 * @return
	 */
	public List<UsersDTO> selectUsersInfo(Page<UsersDTO> page, UsersDTO usersDTO);
	/**
	 * 修改显示页面数据
	 * @param usersDTO
	 * @return
	 */
	public UsersDTO selectOneUsersInfo(UsersDTO usersDTO);
	/**
	 * 显示推荐列表
	 * @param inviteId
	 * @return
	 */
	public List<UsersDTO> selectTuiUsersInfo(Integer inviteId);
	/**
	 * 更新用户数据
	 * @param usersDTO
	 */
	public String updateUsersInfo(UsersDTO usersDTO);
	/**
	 * 获得推荐人姓名id，name
	 * @param users
	 * @return
	 */
	public Users selectTuiUsersName(Users users);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int getTotal();
	
	/**
	 * 根据id查询用户电话真实姓名
	 * @param users
	 * @return
	 */
	public Map<String,Object> selectUsersPhone(Users users);
	
	/**
	 *  用户登录
	 * @param users
	 * @return
	 */
	public UsersLoginDTO userLogin(Users users);
	/**
	 * 核实用户身份
	 * @param userPhoneOrloginName
	 * @return
	 */
	public Users checkUserPhoneOrloginName(String userPhoneOrloginName);
	/**
     * 查询用户上10级
     * @param id
     * @return
     */
    public Map<String,Object> selectUsersUpLevel(Integer id);
    
    /**
     * 根据id注册信息
     * @return
     */
    public Map<String,Object> selectByUsersInfo(Integer id);
    /**
     * 根据id 修改密码
     * @param users
     * @return
     */
	public int updateUsersPwd(UsersDTO users);
	/**
	 * 用户注册
	 * @param users
	 */
	public String insertUsers(Users users);

	public List<Integer> getIds(Integer userId);
	public  int selectCountIsStore(Integer userId);
	
	public List<UsersDTO> selectUsersInfo2(UsersDTO usersDTO);
}

