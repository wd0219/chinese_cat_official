package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.ArticleCats;
import com.stylefeng.guns.modular.huamao.dao.ArticleCatsMapper;
import com.stylefeng.guns.modular.huamao.service.IArticleCatsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
@Service
public class ArticleCatsServiceImpl extends ServiceImpl<ArticleCatsMapper, ArticleCats> implements IArticleCatsService {

	@Override
	public List<Map<String, Object>> selectByParentId(Integer parentId) {
		// TODO Auto-generated method stub
		return baseMapper.selectByParentId(parentId);
	}

	@Override
	public boolean deleteByParentId(Integer parentId) {
		// TODO Auto-generated method stub
		return baseMapper.deleteByParentId(parentId);
	}

	@Override
	public List<Map<String, Object>> selectAll() {
		// TODO Auto-generated method stub
		return baseMapper.selectAll();
	}

}
