package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.GoodsSpecs;
import com.stylefeng.guns.modular.huamao.dao.GoodsSpecsMapper;
import com.stylefeng.guns.modular.huamao.service.IGoodsSpecsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品规格表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsSpecsServiceImpl extends ServiceImpl<GoodsSpecsMapper, GoodsSpecs> implements IGoodsSpecsService {

}
