package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Ads;
import com.stylefeng.guns.modular.huamao.model.AdsList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 广告表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface AdsMapper extends BaseMapper<Ads> {

	// 广告表展示
	public List<AdsList> selectAdsAll(@Param("page") Page<AdsList> page, @Param("adsList") AdsList adsList);

	// 异步获取广告位置
	public List<AdsList> getPosition(String positionType);

	// 添加广告
	public Integer addAds(AdsList adsList);

	// 通过id查询广告为修改做准备
	public AdsList selectAdById(Integer adId);

	// 通过id修改广告
	public void updateAdById(AdsList adsList);

    Integer selectAdsAllCount(@Param("adsList") AdsList adsList);

    Integer updatePopUp (AdsList adsList);
}
