package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktClassType;
import com.stylefeng.guns.modular.huamao.dao.SktClassTypeMapper;
import com.stylefeng.guns.modular.huamao.service.ISktClassTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 视频类型表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
@Service
public class SktClassTypeServiceImpl extends ServiceImpl<SktClassTypeMapper, SktClassType> implements ISktClassTypeService {
    @Autowired
    private SktClassTypeMapper sktClassTypeMapper;
@Override
    public List<SktClassType> selectSktClassTypeAll(Page<SktClassType> page, SktClassType sktClassType){
        Integer total = sktClassTypeMapper.selectSktClassTypeAllCount(sktClassType);
        page.setTotal(total);
        return   sktClassTypeMapper.selectSktClassTypeAll(page,sktClassType);
    }

}
