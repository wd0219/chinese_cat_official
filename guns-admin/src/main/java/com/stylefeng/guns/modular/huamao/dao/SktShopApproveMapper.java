package com.stylefeng.guns.modular.huamao.dao;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopApprove;

import java.util.List;

/**
 * <p>
 * 企业认证表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
public interface SktShopApproveMapper extends BaseMapper<SktShopApprove> {

	public SktShopApprove selectApplyIdByUser(@Param(value="userId")Integer userId);

    public SktShopApprove selectApplyInfo(@Param(value="userId")Integer userId);

    List<SktShopApprove> selectApplyInfo2(@Param(value="userId")Integer userId);
}
