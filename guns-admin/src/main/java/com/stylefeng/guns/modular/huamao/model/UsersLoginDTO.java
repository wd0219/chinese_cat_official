package com.stylefeng.guns.modular.huamao.model;

/**
 * 
 * @author slt
 *
 */
public class UsersLoginDTO {
	private Integer userId;
	private String loginName;
	private String lastLoginTime;
	private String loginAdress;
	private Integer cash;
	private Integer kaiyuan;
	private Integer freezeCash;
	private Integer score;
	private Integer freezeScore;
	private String freezeSpecialAwardScore;
	
	private Integer status;
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 用户类型 0普通 1主管 2经理
	 */
	private Integer userType;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getLoginAdress() {
		return loginAdress;
	}
	public void setLoginAdress(String loginAdress) {
		this.loginAdress = loginAdress;
	}
	public Integer getCash() {
		return cash;
	}
	public void setCash(Integer cash) {
		this.cash = cash;
	}
	public Integer getKaiyuan() {
		return kaiyuan;
	}
	public void setKaiyuan(Integer kaiyuan) {
		this.kaiyuan = kaiyuan;
	}
	public Integer getFreezeCash() {
		return freezeCash;
	}
	public void setFreezeCash(Integer freezeCash) {
		this.freezeCash = freezeCash;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getFreezeScore() {
		return freezeScore;
	}
	public void setFreezeScore(Integer freezeScore) {
		this.freezeScore = freezeScore;
	}
	public String getFreezeSpecialAwardScore() {
		return freezeSpecialAwardScore;
	}
	public void setFreezeSpecialAwardScore(String freezeSpecialAwardScore) {
		this.freezeSpecialAwardScore = freezeSpecialAwardScore;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	
	

}
