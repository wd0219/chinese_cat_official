package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersDTO;
import com.stylefeng.guns.modular.huamao.model.UsersLoginDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersService;
import com.stylefeng.guns.modular.system.dao.UserMapper;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-10
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements IUsersService {
	@Autowired
	private UsersMapper usersMapper;
	
	private static final String REPEAT_LOGIN_NAME = "REPEAT_NAME";//登录名重复
	private static final String REPEAT_LOGIN_PHONE = "REPEAT_PHONE";//用户电话重复
	private static final String SUCCESS = "SUCCESS";
	private static final String SLAT = "ssts3";
	private final Integer COUNT = 9;
	private final String REPEAT = "REPEAT";
	@Override
	public List<UsersDTO> selectUsersInfo(Page<UsersDTO>page,UsersDTO usersDTO) {
		List<UsersDTO> list = usersMapper.selectUsersInfo(page,usersDTO);
		Integer total = usersMapper.selectUsersInfoCount(usersDTO);
		page.setTotal(total);
		return list;
	}
	@Override
	public UsersDTO selectOneUsersInfo(UsersDTO usersDTO) {
		UsersDTO usersDTO2= usersMapper.selectOneUsersInfo(usersDTO);
		return usersDTO2;
	}
	@Override
	public String updateUsersInfo(UsersDTO usersDTO) {
		List<Users> listName = usersMapper.selectTuiName(usersDTO.getLoginName(), usersDTO.getUserId());
		if(listName.size()>0){
			return REPEAT_LOGIN_NAME;
		}
		List<Users> listPhone = usersMapper.selectTuiPhone(usersDTO.getUserPhone(), usersDTO.getUserId());
		if(listPhone.size()>0){
			return REPEAT_LOGIN_PHONE;
		}

		Integer userId = usersDTO.getUserId();
		Users users = usersMapper.selectById(userId);
		Users users1 = new Users();
		users1.setUserId(userId);
		String newPwd = usersDTO.getLoginPwd();
		if (newPwd!=null && !"".equals(newPwd)){
			String encrypt = MD5Util.encrypt(newPwd + users.getLoginSecret());
			users1.setLoginPwd(encrypt);
		}
		users1.setIsWithdrawCash(usersDTO.getIsWithdrawCash());
		users1.setUserStatus(usersDTO.getUserStatus());
		users1.setIsPrivilege(usersDTO.getIsPrivilege());
		users1.setIsOperation(usersDTO.getIsOperation());
		Integer integer = usersMapper.updateById(users1);
		if(integer.equals(0)){
			return "error";
		}
		//newPwd = ShiroKit.md5(newPwd,SLAT);
		//usersDTO.setLoginPwd(newPwd);
		//usersMapper.updateUsersInfo(usersDTO);
		return SUCCESS;
	}
	@Override
	public List<UsersDTO> selectTuiUsersInfo(Integer inviteId) {
		List<UsersDTO> list = usersMapper.selectTuiUsersInfo(inviteId);
		return list;
	}
	@Override
	public Users selectTuiUsersName(Users users) {
		Users users2 = usersMapper.selectOne(users);
		return users2;
	}
	@Override
	public int selectCount() {
		// TODO Auto-generated method stub
		return usersMapper.selectCount();
	}
	@Override
	public int getTotal() {
		// TODO Auto-generated method stub
		return usersMapper.getTotal();
	}
	
	@Override
	public Map<String,Object> selectUsersPhone(Users users) {
		Map<String,Object> user = usersMapper.selectUsersPhone(users);
		return user;
	}
	@Override
	public UsersLoginDTO userLogin(Users users) {
		Users users2 = usersMapper.selectByUserInfo(users);
		if(users2 == null){
			return new UsersLoginDTO();
		}
		String salt = MD5Util.encrypt(users.getLoginPwd()+users2.getLoginSecret());
		if(!(users2.getLoginPwd().equals(salt))){//判断密码是否一致
			return new UsersLoginDTO();
		}
		String ss = MD5Util.encrypt(users.getLoginPwd()+users2.getLoginSecret());
		users.setLoginPwd(ss);
		UsersLoginDTO user = usersMapper.userLogin(users);
		return user;
	}
	@Override
	public Users checkUserPhoneOrloginName(String userPhoneOrloginName) {
		return usersMapper.checkUserInfo(userPhoneOrloginName);
	}
	@Override
    public Map<String, Object> selectUsersUpLevel(Integer id) {
        Users users = usersMapper.selectById(id);
        Map<String,Object> mapUp = new HashMap<>();
        if(users.getInviteId() == null && users.getInviteId() == 0){
            return mapUp;
        }

        StringBuffer sb = new StringBuffer();
        int num = 0;
        selectUp(sb,users.getInviteId(),num);
        mapUp.put("upUser",users.getInviteId());
        mapUp.put("upUsers",sb);

        return mapUp;
    }
	public void selectUp(StringBuffer sb, Integer userId, Integer num){
        Users users = usersMapper.selectById(userId);
        Integer userIds = users.getInviteId();
        if(userIds != 0 &&  num < COUNT){
            num++;
            sb.append(userIds).append(",");
            selectUp(sb,userIds,num);
        }
	}
	
	@Override
    public Map<String,Object> selectByUsersInfo(Integer id) {
        Map<String,Object> userMap = usersMapper.selectByUsersInfo(id);
        return userMap;
    }
	
	@Override
	public int updateUsersPwd(UsersDTO users) {
		int i = 0;
		Integer userId = users.getUserId();
		Users user = usersMapper.selectById(userId);
		String sPwd = MD5Util.encrypt(users.getLoginPwd()+user.getLoginSecret());
		if(user.getLoginPwd().equals(sPwd)){
			String newPwd = MD5Util.encrypt(users.getNewLoginPwd()+user.getLoginSecret());
			users.setNewLoginPwd(newPwd);
			i = usersMapper.updateUsersPwd(users);
		}
		return i;
	}
	
	@Override
	public String insertUsers(Users users) {
		 //查询用户名、电话号是否重复
        List<Users> user = usersMapper.selectByLoginName(users.getLoginName(),users.getUserPhone());
        if(user.size()>0){
            return REPEAT;
        }
        Random random = new Random();
        int a = random.nextInt(9000) + 1000;
        users.setUserType(0);
        users.setIsPrivilege(0);
        users.setUserStatus(1);
        users.setCreateTime(new Date());
        users.setLoginPwd(MD5Util.encrypt(users.getLoginPwd()+a));
        users.setLoginSecret(a);
        Integer str = usersMapper.insert(users);
        return str.toString();
	}

	@Override
	public List<Integer> getIds(Integer userId){
		List<Integer> list=new ArrayList<>();
		Users user = usersMapper.selectById(userId);
		if(user!=null){
			int InviteId=user.getInviteId();
			if (InviteId!=0){
				for (int i=0;i<10;i++){
					Users user1 = usersMapper.selectById(InviteId);
					if(user1.getInviteId()==0){
						break;
					}else{
						list.add(user1.getInviteId());
						InviteId=user1.getInviteId();
					}
				}
			}
		}
		return list;
	}
	public  int selectCountIsStore(Integer userId){
		return 	usersMapper.selectCountIsStore(userId);
	}
	@Override
	public List<UsersDTO> selectUsersInfo2(UsersDTO usersDTO) {
		List<UsersDTO> list = usersMapper.selectUsersInfo2(usersDTO);
		return list;
	}
}
