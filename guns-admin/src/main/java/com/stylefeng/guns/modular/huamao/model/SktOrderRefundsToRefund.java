package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;

public class SktOrderRefundsToRefund extends SktOrderRefunds{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 操作员id
	 */
	private Integer staffId;
	
	/**
     * 订单号
     */
    private String orderNo;
    /**
     * 商品总金额
     */
    private BigDecimal goodsMoney;
    /**
     * 总运费
     */
    private BigDecimal deliverMoney;
    /**
     * 订单总金额 (goodsMoney+deliverMoney 或 realMoney+cash+kaiyuan)
     */
    private BigDecimal totalMoney;
    /**
     * 支付方式支付方式 1:线下现金支付 2 现金账户支付 3开元宝支付 4第三方支付 5混合支付 6开元宝营业额
     */
    private Integer payType;
    /**
     * 实际支付金额 通过支付公司现金支付的金额
     */
    private BigDecimal realMoney;
    
    
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getGoodsMoney() {
		return goodsMoney;
	}
	public void setGoodsMoney(BigDecimal goodsMoney) {
		this.goodsMoney = goodsMoney;
	}
	public BigDecimal getDeliverMoney() {
		return deliverMoney;
	}
	public void setDeliverMoney(BigDecimal deliverMoney) {
		this.deliverMoney = deliverMoney;
	}
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}
	public Integer getPayType() {
		return payType;
	}
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	public BigDecimal getRealMoney() {
		return realMoney;
	}
	public void setRealMoney(BigDecimal realMoney) {
		this.realMoney = realMoney;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
    
}
