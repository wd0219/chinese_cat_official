package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcards;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcardsList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 代理公司的银行卡表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface AgentsBankcardsMapper extends BaseMapper<AgentsBankcards> {

	// 代理公司的银行卡表展示
	List<AgentsBankcardsList> selectAgentsBankcardsAll(@Param("page")Page<AgentsBankcardsList> page,@Param("agentsBankcardsList") AgentsBankcardsList agentsBankcardsList);

	// 通过传过来的ID进行逻辑删除
	void deleteById2(Integer agentsDrawsId);

    Integer selectAgentsBankcardsAllCount(@Param("agentsBankcardsList") AgentsBankcardsList agentsBankcardsList);
}
