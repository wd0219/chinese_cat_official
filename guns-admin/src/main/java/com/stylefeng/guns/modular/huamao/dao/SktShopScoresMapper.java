package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopScores;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 商家评分表 Mapper 接口
 * </p>
 *
 * @author liuduan
 * @since 2018-06-19
 */
public interface SktShopScoresMapper extends BaseMapper<SktShopScores> {

    void updateSktShopScores(Map<String, Object> goodsShopScoresMap);

    List<SktShopScores> selectByShopId(@Param(value = "shopId") Integer shopId);
}
