package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商城商家开店申请表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-10
 */
public interface ISktShopApplysService extends IService<SktShopApplys> {
	/*
	 * 展示全部
	 * */
	public List<MyDTO> shopApplysfindall(Page<MyDTO> page, MyDTO myDto);
	/*
	 * 查看详情
	 * */
	public List<Map<String,Object>> shopApplysfindup(MyDTO myDto);
	/*
	 * 修改后台
	 * */
	public void shopApplyupdate(MyDTO myDto, Integer shiroUserId);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
}
