package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccount;
import com.stylefeng.guns.modular.huamao.model.SktAgentsDTO;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsAccountService;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsService;

/**
 * 代理公司管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 13:09:35
 */
@Controller
@RequestMapping("/sktAgents")
public class SktAgentsController extends BaseController {

    private String PREFIX = "/huamao/sktAgents/";

    @Autowired
    private ISktAgentsService sktAgentsService;
    @Autowired
    private ISktAgentsAccountService iSktAgentsAccountService;
    /**
     * 跳转到代理公司管理首页
     */
    @RequestMapping("")
    public String index(@RequestParam(required = false) String agentId, Model model) {
    	model.addAttribute("agentId",agentId);
        return PREFIX + "sktAgents.html";
    }

    /**
     * 跳转到添加代理公司管理
     */
    @RequestMapping("/sktAgents_add")
    public String sktAgentsAdd() {
        return PREFIX + "sktAgents_add.html";
    }

    /**
     * 跳转到修改代理公司管理
     */
    @RequestMapping("/sktAgents_update/{sktAgentsId}")
    public String sktAgentsUpdate(@PathVariable Integer sktAgentsId, Model model) {
        SktAgents sktAgents = sktAgentsService.selectById(sktAgentsId);
        model.addAttribute("item",sktAgents);
        LogObjectHolder.me().set(sktAgents);
        return PREFIX + "sktAgents_edit.html";
    }

    /**
     * 获取代理公司管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktAgentsDTO sktAgentsDTO) {
    	Page<SktAgentsDTO> page = new PageFactory<SktAgentsDTO>().defaultPage();
        List<SktAgentsDTO> list = sktAgentsService.showSktAgentsIno(page,sktAgentsDTO);
        page.setRecords(list);
        return super.packForBT(page);
    }
    /**
     * 获取人员名称 电话id
     */
    @RequestMapping(value = "/selectNameOrPhone")
    @ResponseBody
    public Object selectNameOrPhone(@RequestParam String nameOrPhone){
    	Map<String, Object> map = sktAgentsService.selectNameOrPhone(nameOrPhone);
    	String jsonString = JSONObject.toJSONString(map);
    	return jsonString;
    }
    
    /**
     * 新增代理公司管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktAgents sktAgents) {
    	//查询是否有该代理公司
    	List<SktAgents> list = sktAgentsService.selectHasAgents(sktAgents);
    	if(list.size()>0){
    		return "REAPT";
    	}
    	sktAgents.setCreateTime(new Date());
        sktAgentsService.insert(sktAgents);
        //新增代理账户
        SktAgentsAccount sktAgentsAccount = new SktAgentsAccount();
        sktAgentsAccount.setAgentId(sktAgents.getAgentId());
        sktAgentsAccount.setCreateTime(new Date());
        iSktAgentsAccountService.insert(sktAgentsAccount);
        return SUCCESS_TIP;
    }

    /**
     * 删除代理公司管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktAgentsId) {
        sktAgentsService.deleteById(sktAgentsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改代理公司管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktAgents sktAgents) {
        sktAgentsService.updateById(sktAgents);
        return SUCCESS_TIP;
    }

    /**
     * 代理公司管理详情
     */
    @RequestMapping(value = "/detail/{sktAgentsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktAgentsId") Integer sktAgentsId) {
        return sktAgentsService.selectById(sktAgentsId);
    }
}
