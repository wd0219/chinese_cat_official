package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商品属性表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_attributes")
public class Attributes extends Model<Attributes> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "attrId", type = IdType.AUTO)
    private Integer attrId;
    /**
     * 最后一级商品分类ID
     */
    private Integer goodsCatId;
    /**
     * 商品分类路径
     */
    private String goodsCatPath;
    /**
     * 属性名称
     */
    private String attrName;
    /**
     * 属性类型 0：输入框 1：多选框 2：下拉框
     */
    private Integer attrType;
    /**
     * 属性值
     */
    private String attrVal;
    /**
     * 排序号
     */
    private Integer attrSort;
    /**
     * 是否显示 1：显示 0：不显示
     */
    private Integer isShow;
    /**
     * 有效状态 1：有效 -1：无效
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getAttrId() {
        return attrId;
    }

    public void setAttrId(Integer attrId) {
        this.attrId = attrId;
    }

    public Integer getGoodsCatId() {
        return goodsCatId;
    }

    public void setGoodsCatId(Integer goodsCatId) {
        this.goodsCatId = goodsCatId;
    }

    public String getGoodsCatPath() {
        return goodsCatPath;
    }

    public void setGoodsCatPath(String goodsCatPath) {
        this.goodsCatPath = goodsCatPath;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public Integer getAttrType() {
        return attrType;
    }

    public void setAttrType(Integer attrType) {
        this.attrType = attrType;
    }

    public String getAttrVal() {
        return attrVal;
    }

    public void setAttrVal(String attrVal) {
        this.attrVal = attrVal;
    }

    public Integer getAttrSort() {
        return attrSort;
    }

    public void setAttrSort(Integer attrSort) {
        this.attrSort = attrSort;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.attrId;
    }

    @Override
    public String toString() {
        return "Attributes{" +
        "attrId=" + attrId +
        ", goodsCatId=" + goodsCatId +
        ", goodsCatPath=" + goodsCatPath +
        ", attrName=" + attrName +
        ", attrType=" + attrType +
        ", attrVal=" + attrVal +
        ", attrSort=" + attrSort +
        ", isShow=" + isShow +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
