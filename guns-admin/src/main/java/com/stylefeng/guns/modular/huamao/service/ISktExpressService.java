package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 快递表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface ISktExpressService extends IService<SktExpress> {

}
