package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.model.SpecCats;
import com.stylefeng.guns.modular.huamao.model.SpecCatsCustem;
import com.stylefeng.guns.modular.huamao.service.IGoodsCatsService;
import com.stylefeng.guns.modular.huamao.service.ISpecCatsService;

/**
 * 商品规格分类控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:43:53
 */
@Controller
@RequestMapping("/specCats")
public class SpecCatsController extends BaseController {

    private String PREFIX = "/huamao/specCats/";

    @Autowired
    private ISpecCatsService specCatsService;
    @Autowired
    private IGoodsCatsService goodsCatsService;
    /**
     * 跳转到商品规格分类首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "specCats.html";
    }

    /**
     * 跳转到添加商品规格分类
     */
    @RequestMapping("/specCats_add")
    public String specCatsAdd() {
        return PREFIX + "specCats_add.html";
    }

    /**
     * 跳转到修改商品规格分类
     */
    @RequestMapping("/specCats_update/{specCatsId}")
    public String specCatsUpdate(@PathVariable Integer specCatsId, Model model) {
        SpecCats specCats = specCatsService.selectById(specCatsId);
        int goodsCatId1=specCats.getGoodsCatId();
        int goodsCatId2=0;
        int goodsCatId3=0;
        if(goodsCatId1!=0){//最后级分类id
        	GoodsCats goodsCats1 = goodsCatsService.selectById(goodsCatId1);
        	goodsCatId2=goodsCats1.getParentId();
        	if(goodsCatId2!=0){//第二级分类id
        		GoodsCats goodsCats2 = goodsCatsService.selectById(goodsCatId2);
        		goodsCatId3=goodsCats2.getParentId();//三级分类id
        	}
        }
        model.addAttribute("item",specCats);
        model.addAttribute("goodsCatId2",goodsCatId2);
        model.addAttribute("goodsCatId3",goodsCatId3);
        LogObjectHolder.me().set(specCats);
        return PREFIX + "specCats_edit.html";
    }

    /**
     * 获取商品规格分类列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SpecCatsCustem specCatsCustem) {
    	Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().defaultPage();
        List<Map<String, Object>> specCatsList =specCatsService.selectAll(page,specCatsCustem);
		for(Map<String, Object> map:specCatsList ){
			String goodsCatIdPath=(String) map.get("goodsCatPath");
			String[]  catIdPathList=goodsCatIdPath.split("_");
			String goodsCatNamePath = null;
			for (int i = 0; i < catIdPathList.length; i++) {
				GoodsCats goodsCats = goodsCatsService.selectById(catIdPathList[i]);
				String catName = goodsCats.getCatName();
				goodsCatNamePath=goodsCatNamePath+catName+"→";
			}
			goodsCatNamePath=goodsCatNamePath.substring(4,goodsCatNamePath.lastIndexOf("→") );
			map.put("goodsCatPath", goodsCatNamePath);

		}
		page.setRecords(specCatsList);
		return super.packForBT(page);
    
    }

    /**
     * 新增商品规格分类
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SpecCatsCustem specCatsCustem) {
    	String goods1Name=null;
    	String goods2Name=null;
    	String goods3Name=null;
    	Integer goods1=specCatsCustem.getGoods1();
    	Integer goods2=specCatsCustem.getGoods2();
    	Integer goods3=specCatsCustem.getGoods3();
    	String goodsCatPath=null;
    	Integer goodsCatId =null;
    	if(goods1!=null && !"".equals(goods1)
    			&&goods2!=null && !"".equals(goods2)
    			&& goods3!=null && !"".equals(goods3)
    		){
//    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
    	//	goodsName = goodsServiceImpl.selectById(goods).getGoodsName();
    		goodsCatPath=goods1+"_"+goods2+"_"+goods3;
    		goodsCatId=goods3;
    	}else if(goods1!=null && !"".equals(goods1)
    			&&goods2!=null && !"".equals(goods2)
    			&& (goods3==null ||"".equals(goods3))
    			){
//    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
//    		goodsName = goodsCatsService.selectById(goods).getCatName();
    		goodsCatPath=goods1+"_"+goods2;
    		goodsCatId=goods2;
    	}else if(goods1!=null && !"".equals(goods1)
    			&&(goods2==null ||"".equals(goods2))
    			){
    		goods1Name = goodsCatsService.selectById(goods1).getCatName();
//    		goods2Name = goodsCatsService.selectById(goods2).getCatName();
//    		goods3Name = goodsCatsService.selectById(goods3).getCatName();
//    		goodsName = goodsCatsService.selectById(goods).getCatName();
    		goodsCatId=goods1;
    		goodsCatPath=goods1+"_";
    	}
    	Date d=new Date();
    	specCatsCustem.setGoodsCatId(goodsCatId);
    	specCatsCustem.setGoodsCatPath(goodsCatPath);
    	specCatsCustem.setCreateTime(d);
    	specCatsCustem.setDataFlag(1);
        specCatsService.insertSpecCats(specCatsCustem);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品规格分类
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer specCatsId) {
    	
        specCatsService.deleteById(specCatsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品规格分类
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SpecCatsCustem specCatsCustem) {
    	SpecCats specCats=specCatsService.selectById(specCatsCustem.getCatId());
    	String goods1Name=null;
    	String goods2Name=null;
    	String goods3Name=null;
    	Integer goods1=specCatsCustem.getGoods1();
    	Integer goods2=specCatsCustem.getGoods2();
    	Integer goods3=specCatsCustem.getGoods3();
    	String goodsCatPath=null;
    	Integer goodsCatId =null;
    	if(goods1!=null && !"".equals(goods1)
    			&&goods2!=null && !"".equals(goods2)
    			&& goods3!=null && !"".equals(goods3)
    		){
            goodsCatPath=goods1+"_"+goods2+"_"+goods3;
    		goodsCatId=goods3;
    	}else if(goods1!=null && !"".equals(goods1)
    			&&goods2!=null && !"".equals(goods2)
    			&& (goods3==null ||"".equals(goods3))
    			){
            goodsCatPath=goods1+"_"+goods2;
    		goodsCatId=goods2;
    	}else if(goods1!=null && !"".equals(goods1)
    			&&(goods2==null ||"".equals(goods2))
    			){
    		goodsCatId=goods1;
    		goodsCatPath=goods1+"_";
    	}
    	specCats.setGoodsCatId(goodsCatId);
    	specCats.setGoodsCatPath(goodsCatPath);
    	specCats.setIsAllowImg(specCatsCustem.getIsAllowImg());
    	specCats.setIsShow(specCatsCustem.getIsShow());
    	specCats.setCatName(specCatsCustem.getCatName());
        specCatsService.updateById(specCats);
        return SUCCESS_TIP;
    }

    /**
     * 商品规格分类详情
     */
    @RequestMapping(value = "/detail/{specCatsId}")
    @ResponseBody
    public Object detail(@PathVariable("specCatsId") Integer specCatsId) {
        return specCatsService.selectById(specCatsId);
    }
}
