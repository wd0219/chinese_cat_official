package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogDaifu;
import com.stylefeng.guns.modular.huamao.model.LogDaifuList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 代付记录表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface LogDaifuMapper extends BaseMapper<LogDaifu> {

	// 代付记录表展示
	List<LogDaifuList> selectLogDaifuAll(@Param("page")Page<LogDaifuList> page,@Param("logDaifuList") LogDaifuList logDaifuList);

    Integer selectLogDaifuAllCount(@Param("logDaifuList") LogDaifuList logDaifuList);
}
