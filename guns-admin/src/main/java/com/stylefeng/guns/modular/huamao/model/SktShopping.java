package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 购物券表
 * </p>
 *
 * @author wudi123
 * @since 2018-09-29
 */
@TableName("skt_shopping")
public class SktShopping extends Model<SktShopping> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户身份  1会员   2代理，3 商家
     */
    private Integer type;
    /**
     * 目标用户ID
     */
    private Integer dataId;
    /**
     * 对应订单号
     */
    private String orderNo;
    /**
     * 金额
     */
    private BigDecimal money;
    /**
     * 备注
     */
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 购物券过期时间
     */
    private Date overdueTime;
    /**
     * 购物券状态 1未使用 2已使用 3已过期
     */
    private Integer status;
    /**
     * 购物券使用场景 1通用 2线上 3线下 4无人超市
     */
    private Integer sceneType;
    /**
     * 流水标志 -1减少 1增加
     */
    private Integer shoppingType;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getDataId() {
        return dataId;
    }

    public void setDataId(Integer dataId) {
        this.dataId = dataId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getOverdueTime() {
        return overdueTime;
    }

    public void setOverdueTime(Date overdueTime) {
        this.overdueTime = overdueTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSceneType() {
        return sceneType;
    }

    public void setSceneType(Integer sceneType) {
        this.sceneType = sceneType;
    }

    public Integer getShoppingType() {
        return shoppingType;
    }

    public void setShoppingType(Integer shoppingType) {
        this.shoppingType = shoppingType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktShopping{" +
        "id=" + id +
        ", type=" + type +
        ", dataId=" + dataId +
        ", orderNo=" + orderNo +
        ", money=" + money +
        ", remark=" + remark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", overdueTime=" + overdueTime +
        ", status=" + status +
        ", sceneType=" + sceneType +
        ", shoppingType=" + shoppingType +
        "}";
    }
}
