package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktOrdersList;

/**
 * <p>
 * 线上商城订单表 服务类
 * </p>
 *
 * @author caody123
 * @since 2018-04-14
 */
public interface ISktOrdersService extends IService<SktOrders> {

	/**
	 * 查询订单列表
	 * @param page 
	 * @param paramMap
	 * @return
	 */
	public List<SktOrdersList> selectOrderShops(Page<SktOrdersList> page, SktOrdersList sktOrdersList);
	/**
	 * 查询订单积分信息
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderScore(Map<String,Object> param);
	/**
	 * 查询订单商品信息
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderGoods(Map<String,Object> param);
	/**
	 * 查询订单商品参数信息
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderGoodsParameter(Map<String,Object> param);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	
	/**
	 * 查询订单总数
	 * @param 
	 * @return
	 */
	public int selectOrderTotal();
	
}
