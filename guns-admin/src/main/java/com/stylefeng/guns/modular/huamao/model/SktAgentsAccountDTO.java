package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;

@SuppressWarnings("serial")
public class SktAgentsAccountDTO extends SktAgentsAccount{
	/**
	 * 代理公司名称
	 */
	private String agentName;
	/**
	 * 成为股东的分红系数
	 */
	private BigDecimal oneRatio; 
	/**
	 * 完成70%(落地)的分红系数
	 */
	private BigDecimal twoRatio; 
	/**
	 * 全部完成的分红系数
	 */
	private BigDecimal threeRatio;
	/**
	 * 省id
	 */
	private Integer province;
	/**
	 * 市id
	 */
	private Integer citys;
	/**
	 * 区域id
	 */
	private Integer county;
	/**
	 * 开始时间
	 */
	private String beginTime;
	/**
	 * 结束时间
	 */
	private String endTime;
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public BigDecimal getOneRatio() {
		return oneRatio;
	}
	public void setOneRatio(BigDecimal oneRatio) {
		this.oneRatio = oneRatio;
	}
	public BigDecimal getTwoRatio() {
		return twoRatio;
	}
	public void setTwoRatio(BigDecimal twoRatio) {
		this.twoRatio = twoRatio;
	}
	public BigDecimal getThreeRatio() {
		return threeRatio;
	}
	public void setThreeRatio(BigDecimal threeRatio) {
		this.threeRatio = threeRatio;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	public Integer getCitys() {
		return citys;
	}
	public void setCitys(Integer citys) {
		this.citys = citys;
	}
	public Integer getCounty() {
		return county;
	}
	public void setCounty(Integer county) {
		this.county = county;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	
}
