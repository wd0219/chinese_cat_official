package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.Attributes;
import com.stylefeng.guns.modular.huamao.model.AttributesCustem;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品属性表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IAttributesService extends IService<Attributes> {
	Integer insertAttributes(AttributesCustem attributesCustem);
	List<Map<String, Object>> selectAll(Page<Map<String, Object>> page, AttributesCustem attributesCustem);
	AttributesCustem selectAttributesCustem(Integer id);

}
