package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 行业管理表 Mapper 接口
 * </p>
 *
 * @author ck123123
 * @since 2018-04-19
 */
public interface SktShopIndustryMapper extends BaseMapper<SktShopIndustry> {
	/*
	 * 展示全部+sort排序
	 * */
	public List<SktShopIndustry> shopIndustryfindinfo(@Param("page")Page<SktShopIndustry> page,@Param("shopIndustry") SktShopIndustry shopIndustry);

	Integer shopIndustryfindinfoCount(@Param("shopIndustry") SktShopIndustry shopIndustry);

	public List<SktShopIndustry> shopIndustryfindinfo2(@Param("shopIndustry") SktShopIndustry shopIndustry);
}
