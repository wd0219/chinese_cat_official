package com.stylefeng.guns.modular.huamao.model;

public class UsersUpgradeDTO extends UsersUpgrade{
	/**
	 * 用户昵称
	 */
	private String loginName;
	/**
	 * 用户姓名
	 */
	private String trueName;
	/**
	 * 用户电话
	 */
	private String userPhone;
    
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
    
}
