package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStoresDto;

/**
 * <p>
 * 线下商家订单表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-24
 */
public interface SktOrdersStoresMapper extends BaseMapper<SktOrdersStores> {
	/**
	 * 页面显示列表
	 * @param page 
	 * @param sktOrdersStoresDto
	 * @return
	 */
	public List<SktOrdersStoresDto> showOrdersStoresInfo (@Param("page")Page<SktOrdersStoresDto> page, 
			@Param("sktOrdersStoresDto")SktOrdersStoresDto sktOrdersStoresDto);
	public Integer showOrdersStoresInfoCount(@Param("sktOrdersStoresDto")SktOrdersStoresDto sktOrdersStoresDto);
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int selectOrdersStoresTotal();

    List<SktOrdersStoresDto> showOrdersStoresInfo2(@Param("sktOrdersStoresDto")SktOrdersStoresDto sktOrdersStoresDto);
}
