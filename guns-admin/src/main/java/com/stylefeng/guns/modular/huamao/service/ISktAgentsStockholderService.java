package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholder;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;

/**
 * <p>
 * 代理公司的股东表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-19
 */
public interface ISktAgentsStockholderService extends IService<SktAgentsStockholder> {
	/**
	 * 页面显示列表
	 * @param page 
	 * @param sktAgentsStockholderDTO
	 * @return
	 */
	public List<SktAgentsStockholderDTO> showSktStcokHolderInfo(Page<SktAgentsStockholderDTO> page, SktAgentsStockholderDTO sktAgentsStockholderDTO);
	
	/**
	 * 查询该代理公司有没有这个管理
	 * @param agentId
	 * @param type
	 * @return
	 */
	public String selectSktStcokManger(Integer agentId,Integer type );
	
	/**
	 * 查询用户电话
	 * @param nameOrPhone
	 * @return
	 */
	public Map<String,Object> selectNameOrPhone(@Param(value="nameOrPhone") String nameOrPhone);
	
	/**
	 * 更新
	 * @param sktAgentsStockholder
	 */
	public void updateSktAgentsStockHolder(SktAgentsStockholder sktAgentsStockholder);
	/**
	 * 新增代理股东
	 * @param sktAgentsStockholder
	 * @return
	 */
	public String insertSktStockHodler(SktAgentsStockholderDTO sktAgentsStockholder);

	public  Map<String,Object> agentTransfer(Integer shId,Integer userId);
	public   Map<String,Object> change( Integer type,Integer shId,Integer typeRe);
	public Map<String,Object> addShareHolder(Integer userId,Integer alevel,Integer provinceId,Integer cityId,Integer areaId,Integer type,Integer staffId);
	public Map<String,Object> applysSetStatus(Integer shaId,Integer staffId);
	public   Map<String,Object> checkRunOrProfit(Integer agentId,String changeNo);
}
