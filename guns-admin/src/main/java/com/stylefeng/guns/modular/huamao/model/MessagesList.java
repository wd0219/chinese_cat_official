package com.stylefeng.guns.modular.huamao.model;

import io.swagger.models.auth.In;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统消息表扩展类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public class MessagesList extends Messages {



	/**
	 * 发送者
	 */
	private String sendName;

	/**
	 * 接受者
	 */
	private String receiveName;





	/**
	 * 指定发送用户时使用，存的是userId
	 */
	private List<Integer> usersId;

	public MessagesList() {
		super();
		// TODO Auto-generated constructor stub
	}




	public String getSendName() {
		return sendName;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}



	public String getReceiveName() {
		return receiveName;
	}

	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}


	public List<Integer> getUsersId() {
		return usersId;
	}

	public void setUsersId(List<Integer> usersId) {
		this.usersId = usersId;
	}

	@Override
	public String toString() {
		return "MessagesList{" +
				"sendName='" + sendName + '\'' +
				", receiveName='" + receiveName + '\'' +
				", usersId=" + usersId +
				'}';
	}
}
