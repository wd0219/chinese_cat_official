package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersDraws;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsList;

/**
 * <p>
 * 用户提现表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface IUsersDrawsService extends IService<UsersDraws> {

	// 用户提现表展示
	public List<UsersDrawsList> selectUsersDrawsAll(Page<UsersDrawsList> page, UsersDrawsList usersDrawsList);

	// 修改审核状态
	public void updateSatus(UsersDrawsList usersDrawsList);

	//批量拒绝
    Integer batchEdit(UsersDrawsList usersDrawsList);

    //华宝批量转换
	public Object batchRecharge(List<Map<String,Object>> list);
}
