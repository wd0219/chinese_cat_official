package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Articles;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 文章记录表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
public interface IArticlesService extends IService<Articles> {
	List<Map<String,Object>>selectArticles(Page<Map<String,Object>> page, Articles articles);
}
