package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktShopping;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktShoppingDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 购物券表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-09-29
 */
public interface SktShoppingMapper extends BaseMapper<SktShopping> {
    List<SktShoppingDTO> selectAll(@Param("page") Page<SktShoppingDTO> page, @Param("sktShoppingDTO") SktShoppingDTO sktShoppingDTO);

    Integer selectAllCount(@Param("sktShoppingDTO") SktShoppingDTO sktShoppingDTO);

}
