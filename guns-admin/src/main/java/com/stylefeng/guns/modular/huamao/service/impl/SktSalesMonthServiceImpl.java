package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktSalesMonthMapper;
import com.stylefeng.guns.modular.huamao.model.SktSalesMonth;
import com.stylefeng.guns.modular.huamao.service.ISktSalesMonthService;

/**
 * <p>
 * 商家每月营业额统计表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
@Service
public class SktSalesMonthServiceImpl extends ServiceImpl<SktSalesMonthMapper, SktSalesMonth> implements ISktSalesMonthService {
	@Autowired
	private SktSalesMonthMapper sktSalesMonthMapper;
	@Override
	public List<SktSalesMonth> sktSalesMonthfindAll(Page<SktSalesMonth>page,SktSalesMonth sktSalesMonth) {
		List<SktSalesMonth> list = sktSalesMonthMapper.sktSalesMonthfindAll(page,sktSalesMonth);
		Integer total = sktSalesMonthMapper.sktSalesMonthfindAllCount(sktSalesMonth);
		page.setTotal(total);
		return list;
		 
	}

}
