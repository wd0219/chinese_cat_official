package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商家每月营业额统计表
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
@TableName("skt_sales_month")
public class SktSalesMonth extends Model<SktSalesMonth> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商家id
     */
    private Integer shopId;
    /**
     * 商家用户id
     */
    private Integer userId;
    /**
     * 营业月份(201711)
     */
    private Integer month;
    /**
     * 门店营业总额
     */
    private BigDecimal storeTotal;
    /**
     * 门店营业额现金部分
     */
    private BigDecimal storeCash;
    /**
     * 门店营业额开元宝部分
     */
    private BigDecimal storeKaiyuan;
    /**
     * 商城营业总额
     */
    private BigDecimal mallTotal;
    /**
     * 商城营业额现金部分
     */
    private BigDecimal mallCash;
    /**
     * 商城营业额开元宝部分
     */
    private BigDecimal mallKaiyuan;
    /**
     * 商家参与分红总额
     */
    private BigDecimal profitTotal;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 是否已参与计算分红 1:是 0:否
     */
    private Integer isProfit;

    private String userName;
    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	private String shopName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public BigDecimal getStoreTotal() {
        return storeTotal;
    }

    public void setStoreTotal(BigDecimal storeTotal) {
        this.storeTotal = storeTotal;
    }

    public BigDecimal getStoreCash() {
        return storeCash;
    }

    public void setStoreCash(BigDecimal storeCash) {
        this.storeCash = storeCash;
    }

    public BigDecimal getStoreKaiyuan() {
        return storeKaiyuan;
    }

    public void setStoreKaiyuan(BigDecimal storeKaiyuan) {
        this.storeKaiyuan = storeKaiyuan;
    }

    public BigDecimal getMallTotal() {
        return mallTotal;
    }

    public void setMallTotal(BigDecimal mallTotal) {
        this.mallTotal = mallTotal;
    }

    public BigDecimal getMallCash() {
        return mallCash;
    }

    public void setMallCash(BigDecimal mallCash) {
        this.mallCash = mallCash;
    }

    public BigDecimal getMallKaiyuan() {
        return mallKaiyuan;
    }

    public void setMallKaiyuan(BigDecimal mallKaiyuan) {
        this.mallKaiyuan = mallKaiyuan;
    }

    public BigDecimal getProfitTotal() {
        return profitTotal;
    }

    public void setProfitTotal(BigDecimal profitTotal) {
        this.profitTotal = profitTotal;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getIsProfit() {
        return isProfit;
    }

    public void setIsProfit(Integer isProfit) {
        this.isProfit = isProfit;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

	@Override
	public String toString() {
		return "SktSalesMonth [id=" + id + ", shopId=" + shopId + ", userId=" + userId + ", month=" + month
				+ ", storeTotal=" + storeTotal + ", storeCash=" + storeCash + ", storeKaiyuan=" + storeKaiyuan
				+ ", mallTotal=" + mallTotal + ", mallCash=" + mallCash + ", mallKaiyuan=" + mallKaiyuan
				+ ", profitTotal=" + profitTotal + ", createTime=" + createTime + ", isProfit=" + isProfit
				+ ", userName=" + userName + ", shopName=" + shopName + "]";
	}

	public SktSalesMonth(Integer id, Integer shopId, Integer userId, Integer month, BigDecimal storeTotal,
			BigDecimal storeCash, BigDecimal storeKaiyuan, BigDecimal mallTotal, BigDecimal mallCash,
			BigDecimal mallKaiyuan, BigDecimal profitTotal, Date createTime, Integer isProfit, String userName,
			String shopName) {
		super();
		this.id = id;
		this.shopId = shopId;
		this.userId = userId;
		this.month = month;
		this.storeTotal = storeTotal;
		this.storeCash = storeCash;
		this.storeKaiyuan = storeKaiyuan;
		this.mallTotal = mallTotal;
		this.mallCash = mallCash;
		this.mallKaiyuan = mallKaiyuan;
		this.profitTotal = profitTotal;
		this.createTime = createTime;
		this.isProfit = isProfit;
		this.userName = userName;
		this.shopName = shopName;
	}

	public SktSalesMonth() {
		super();
		// TODO Auto-generated constructor stub
	}
    
}
