package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SpecItems;
import com.stylefeng.guns.modular.huamao.dao.SpecItemsMapper;
import com.stylefeng.guns.modular.huamao.service.ISpecItemsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品规格值表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class SpecItemsServiceImpl extends ServiceImpl<SpecItemsMapper, SpecItems> implements ISpecItemsService {

}
