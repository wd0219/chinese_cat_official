package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.AccountOperation;
import com.stylefeng.guns.modular.huamao.model.AccountOperationList;
import com.stylefeng.guns.modular.huamao.service.IAccountOperationService;

import java.util.List;

/**
 * 手动补单记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-13 10:33:42
 */
@Controller
@RequestMapping("/accountOperation")
public class AccountOperationController extends BaseController {

	private String PREFIX = "/huamao/accountOperation/";

	@Autowired
	private IAccountOperationService accountOperationService;

	/**
	 * 跳转到手动补单记录首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "accountOperation.html";
	}

	/**
	 * 跳转到添加手动补单记录
	 */
	@RequestMapping("/accountOperation_add")
	public String accountOperationAdd() {
		return PREFIX + "accountOperation_add.html";
	}

	/**
	 * 跳转到修改手动补单记录
	 */
	@RequestMapping("/accountOperation_update/{accountOperationId}")
	public String accountOperationUpdate(@PathVariable Integer accountOperationId, Model model) {
		AccountOperation accountOperation = accountOperationService.selectById(accountOperationId);
		model.addAttribute("item", accountOperation);
		LogObjectHolder.me().set(accountOperation);
		return PREFIX + "accountOperation_edit.html";
	}

	/**
	 * 获取手动补单记录列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(AccountOperationList accountOperationList) {
		Page<AccountOperationList> page = new PageFactory<AccountOperationList>().defaultPage();
		List<AccountOperationList> accountOperationLists = accountOperationService.selectAccountOperationAll(page,accountOperationList);
		page.setRecords(accountOperationLists);
		return super.packForBT(page);
		// return accountOperationService.selectList(null);
	}

	/**
	 * 新增手动补单记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(AccountOperation accountOperation) {
		accountOperationService.insert(accountOperation);
		return SUCCESS_TIP;
	}

	/**
	 * 删除手动补单记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer accountOperationId) {
		accountOperationService.deleteById(accountOperationId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改手动补单记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(AccountOperation accountOperation) {
		accountOperationService.updateById(accountOperation);
		return SUCCESS_TIP;
	}

	/**
	 * 手动补单记录详情
	 */
	@RequestMapping(value = "/detail/{accountOperationId}")
	@ResponseBody
	public Object detail(@PathVariable("accountOperationId") Integer accountOperationId) {
		return accountOperationService.selectById(accountOperationId);
	}
}
