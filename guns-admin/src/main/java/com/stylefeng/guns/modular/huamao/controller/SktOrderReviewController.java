package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogManager;
import com.stylefeng.guns.core.log.factory.LogTaskFactory;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.huamao.model.SktOrderReview;
import com.stylefeng.guns.modular.huamao.model.SktOrderReviewDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.service.ISktOrderReviewService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.stylefeng.guns.core.support.HttpKit.getIp;

/**
 * 商城订单审核控制器
 *
 * @author fengshuonan
 * @Date 2018-09-19 10:46:57
 */
@Controller
@RequestMapping("/sktOrderReview")
public class SktOrderReviewController extends BaseController {

    private String PREFIX = "/huamao/sktOrderReview/";

    @Autowired
    private ISktOrderReviewService sktOrderReviewService;

    /**
     * 跳转到商城订单审核首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktOrderReview.html";
    }

    /**
     * 跳转到添加商城订单审核
     */
    @RequestMapping("/sktOrderReview_add")
    public String sktOrderReviewAdd() {
        return PREFIX + "sktOrderReview_add.html";
    }

    /**
     * 跳转到修改商城订单审核
     */
    @RequestMapping("/sktOrderReview_update/{sktOrderReviewId}")
    public String sktOrderReviewUpdate(@PathVariable Integer sktOrderReviewId, Model model) {
        SktOrderReview sktOrderReview = sktOrderReviewService.selectById(sktOrderReviewId);
        model.addAttribute("item",sktOrderReview);
        LogObjectHolder.me().set(sktOrderReview);
        return PREFIX + "sktOrderReview_edit.html";
    }

    /**
     * 获取商城订单审核列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktOrderReviewDTO sktOrderReviewDTO) {
        Page<SktOrderReviewDTO> page = new PageFactory<SktOrderReviewDTO>().defaultPage();
        List<SktOrderReviewDTO> list = sktOrderReviewService.selectAll(page,sktOrderReviewDTO);
        page.setRecords(list);
        return super.packForBT(page);
    }

    /**
     * 新增商城订单审核
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktOrderReview sktOrderReview) {
        sktOrderReviewService.insert(sktOrderReview);
        return SUCCESS_TIP;
    }

    /**
     * 删除商城订单审核
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktOrderReviewId) {
        sktOrderReviewService.deleteById(sktOrderReviewId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商城订单审核
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktOrderReview sktOrderReview) {
        sktOrderReviewService.updateById(sktOrderReview);
        return SUCCESS_TIP;
    }

    /**
     * 商城订单审核详情
     */
    @RequestMapping(value = "/detail/{sktOrderReviewId}")
    @ResponseBody
    public Object detail(@PathVariable("sktOrderReviewId") Integer sktOrderReviewId) {
        return sktOrderReviewService.selectById(sktOrderReviewId);
    }

    /**
     * 商城订单审通过、拒绝
     * @param id
     * @param orderStatus
     * @param checkRemark
     * @return
     */
    @RequestMapping(value = "/updatePass")
    @ResponseBody
    public String updatePass(@RequestParam(value = "id") String id,@RequestParam(value = "orderStatus") String orderStatus,@RequestParam(value = "checkRemark",required = false) String checkRemark) {
        ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
        if(shiroUser == null){
            LogManager.me().executeLog(LogTaskFactory.exitLog(ShiroKit.getUser().getId(),
                    getIp()));
            ShiroKit.getSubject().logout();
            return REDIRECT + "/login";
        }
        Integer staffId = shiroUser.getId();
        Map<String, Object> map = sktOrderReviewService.updateStatus(staffId, id,
                orderStatus, checkRemark);
        Object code = map.get("code");
        if("00".equals(code.toString())){
            Object msg = map.get("msg");
            return msg.toString();
        }
        return "01";
    }

    @RequestMapping(value = "/sktOrderReview_refused/{id}")
    public String refused(@PathVariable("id") Integer id,Model model) {
        model.addAttribute("id",id);
        return PREFIX + "sktOrderReview_refused.html";
    }
}
