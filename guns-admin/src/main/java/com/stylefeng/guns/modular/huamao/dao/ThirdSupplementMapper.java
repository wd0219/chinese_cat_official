package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.ThirdSupplementList;

/**
 * 第三方支付补单 Mapper 接口
 * 
 * @author gxz
 *
 */
public interface ThirdSupplementMapper extends BaseMapper<ThirdSupplementList> {

	List<ThirdSupplementList> selectThirdSupplementAll(ThirdSupplementList thirdSupplementList);

}
