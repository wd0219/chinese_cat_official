package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.modular.huamao.common.HttpUtil;
import com.stylefeng.guns.modular.huamao.common.StringUtils;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.dao.SktHuamaobtLogMapper;
import com.stylefeng.guns.modular.huamao.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author caody123
 * @since 2018-06-23
 */
@Service
public class SktHuamaobtLogServiceImpl extends ServiceImpl<SktHuamaobtLogMapper, SktHuamaobtLog> implements ISktHuamaobtLogService {

    @Autowired
    ISktExchangeUserService exchangeUserService;

    @Autowired
    IAccountService accountService;

    @Autowired
    ILogKaiyuanService logKaiyuanService;

    @Autowired
    IUsersDrawsService usersDrawsService;



    @Override
    @Transactional
    public Object recharge(Integer userId, Integer amount,Integer drawId){

        SktExchangeUser exchangeUser = new SktExchangeUser();
        exchangeUser.setUserId(userId);
        SktExchangeUser  rexchangeUser = new SktExchangeUser();
        EntityWrapper<SktExchangeUser> ew =   new EntityWrapper<SktExchangeUser>(exchangeUser);

        rexchangeUser =  exchangeUserService.selectOne(ew);

        if(rexchangeUser == null){
            return "用户ID不存在！";
        }

        String url = "https://api1.digsg.com/assets/recharge";

        /**
         * 用户交易所的ID
         */
        Integer userID = rexchangeUser.getExchangeUserId();
        String companyID = "1";
        String secretKey = "iL34.#dnZ2ubJJFt";

        String key = Integer.toString(userID)+Integer.toString(amount)+companyID+secretKey;
        String md5Info = MD5Util.encrypt(key);

        Map<String,Object> map = new HashMap<String, Object>();
        map.put("userID",userID);
        map.put("amount",amount);
        map.put("companyID",companyID);
        map.put("md5Info",md5Info);

        /**
         *调用用户充值接口
         */
        String json =  HttpUtil.doPost(url,map);
        JSONObject jsonObject = JSONObject.parseObject(json);
        if(jsonObject.getBoolean("isSuc")){

            //生成相关的订单号
            String orderNo = StringUtils.getOrderIdByTime("5");

            /**
             * 减少用户相应的华宝
             */
            Account account = new Account();
            //userId代表用户id，userID代表交易所id
            account.setUserId(userId);
            Account raccount  = new Account();
            EntityWrapper<Account> entityWrapper = new EntityWrapper<Account>(account);
            raccount = accountService.selectOne(entityWrapper);
            raccount.getKaiyuan();
            BigDecimal temp = new BigDecimal(amount);
            raccount.setKaiyuan(raccount.getKaiyuan().subtract(temp));
            accountService.updateById(raccount);

            /**
             * 增加华宝交易记录
             */
            LogKaiyuan logKaiyuan = new LogKaiyuan();
            logKaiyuan.setType(33);
            logKaiyuan.setFromId(userId);
            logKaiyuan.setUserId(userID);
            logKaiyuan.setOrderNo(orderNo);
            logKaiyuan.setPreKaiyuan(account.getKaiyuan());
            logKaiyuan.setKaiyuanType(-1);
            logKaiyuan.setKaiyuan(temp);
            logKaiyuan.setRemark("");
            logKaiyuan.setDataFlag(1);
            logKaiyuan.setCreateTime(new Date());
            logKaiyuanService.insert(logKaiyuan);

            UsersDraws usersDraws = new UsersDraws();
            usersDraws = usersDrawsService.selectById(drawId);
            usersDraws.setMoney(new  BigDecimal(0));
            usersDrawsService.updateById(usersDraws);



            /**
             * 增加交易记录
             */
            SktHuamaobtLog huamaobtLog  = new SktHuamaobtLog();
            //userId代表用户id，userID代表交易所id
            huamaobtLog.setExchangeUserId(userID);
            huamaobtLog.setAmount(amount);
            huamaobtLog.setCreateTime(new Date());
            huamaobtLog.setOrderNo(orderNo);
            this.insert(huamaobtLog);

            return "success";

        }else{
            return jsonObject.getString("des");
        }

    }

}
