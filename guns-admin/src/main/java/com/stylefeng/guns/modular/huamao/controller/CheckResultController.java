package com.stylefeng.guns.modular.huamao.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.huamao.model.CheckDetailList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.CheckResult;
import com.stylefeng.guns.modular.huamao.model.CheckResultList;
import com.stylefeng.guns.modular.huamao.service.ICheckResultService;

/**
 * pingtaiduizhang控制器
 *
 * @author fengshuonan
 * @Date 2018-04-14 14:44:35
 */
@Controller
@RequestMapping("/checkResult")
public class CheckResultController extends BaseController {

	private String PREFIX = "/huamao/checkResult/";

	@Autowired
	private ICheckResultService checkResultService;

	/**
	 * 跳转到pingtaiduizhang首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "checkResult.html";
	}

	/**
	 * 跳转到添加pingtaiduizhang
	 */
	@RequestMapping("/checkResult_add")
	public String checkResultAdd() {
		return PREFIX + "checkResult_add.html";
	}

	/**
	 * 跳转到修改pingtaiduizhang
	 */
	@RequestMapping("/checkResult_update/{checkResultId}")
	public String checkResultUpdate(@PathVariable Integer checkResultId, Model model) {
		CheckResult checkResult = checkResultService.selectById(checkResultId);
		model.addAttribute("item", checkResult);
		LogObjectHolder.me().set(checkResult);
		return PREFIX + "checkResult_edit.html";
	}

	/**
	 * 获取pingtaiduizhang列表
	 * 
	 * @throws ParseException
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(CheckResultList checkResultList, String beginTime1, String endTime1) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyyMMdd");
		if (beginTime1 != null && beginTime1 != "") {
			// 将date转换成Integer
			Date parse = simpleDateFormat.parse(beginTime1);
			String format = simpleDateFormat2.format(parse);
			Integer valueOf = Integer.valueOf(format);
			checkResultList.setBeginTime(valueOf);
		}
		if (endTime1 != null && endTime1 != "") {
			Date parse2 = simpleDateFormat.parse(endTime1);
			String format2 = simpleDateFormat2.format(parse2);
			Integer valueOf2 = Integer.valueOf(format2);
			checkResultList.setEndTime(valueOf2);
		}
		Page<CheckResultList> page = new PageFactory<CheckResultList>().defaultPage();
		List<CheckResultList> checkResultLists = checkResultService.selectCheckResultAll(page,checkResultList);
		page.setRecords(checkResultLists);
		return super.packForBT(page);
		// return checkResultService.selectList(null);
	}

	/**
	 * 新增pingtaiduizhang
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(CheckResult checkResult) {
		checkResultService.insert(checkResult);
		return SUCCESS_TIP;
	}

	/**
	 * 删除pingtaiduizhang
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer checkResultId) {
		checkResultService.deleteById(checkResultId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改pingtaiduizhang
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(CheckResult checkResult) {
		checkResultService.updateById(checkResult);
		return SUCCESS_TIP;
	}

	/**
	 * pingtaiduizhang详情
	 */
	@RequestMapping(value = "/detail/{checkResultId}")
	public Object detail(@PathVariable("checkResultId") Integer checkResultId, Model model) {
		CheckResultList checkResultList = checkResultService.selectctDetailedById(checkResultId);
		model.addAttribute("item", checkResultList);
		return PREFIX + "checkResult_detailed.html";
		// return checkResultService.selectById(checkResultId);
	}
}
