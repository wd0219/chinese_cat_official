package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;
import com.stylefeng.guns.modular.huamao.model.UsersRechargeDTO;
import io.swagger.models.auth.In;
import org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor;

/**
 * <p>
 * 用户充值表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
public interface IUsersRechargeService extends IService<UsersRecharge> {
	/**
	 * 查询用户订单
	 * @param page 
	 * @param usersRechargeDTO
	 * @return
	 */
	public List<UsersRechargeDTO> showUserDengDanInfo(Page<UsersRechargeDTO> page, UsersRechargeDTO usersRechargeDTO);

	public JSONObject rechargeAfter(UsersRecharge usersRecharge, Integer userId);

    public boolean updateUserRecharge(UsersRecharge usersRecharge, Integer userId);
}
