package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogCashList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 现金流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogCashMapper extends BaseMapper<LogCash> {

	// 现金流水返回
	List<LogCashList> selectLogCashAll(@Param("page")Page<LogCashList> page, @Param("logCashList") LogCashList logCashList);

    Integer selectLogCashAllCount(@Param("logCashList") LogCashList logCashList);
}
