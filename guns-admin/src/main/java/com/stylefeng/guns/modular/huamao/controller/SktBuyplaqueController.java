package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;
import com.stylefeng.guns.modular.huamao.service.ISktBuyplaqueService;

/**
 * 线下商家购买牌匾控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 15:52:57
 */
@Controller
@RequestMapping("/sktBuyplaque")
public class SktBuyplaqueController extends BaseController {

    private String PREFIX = "/huamao/sktBuyplaque/";

    @Autowired
    private ISktBuyplaqueService sktBuyplaqueService;

    /**
     * 跳转到线下商家购买牌匾首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktBuyplaque.html";
    }

    /**
     * 跳转到添加线下商家购买牌匾
     */
    @RequestMapping("/sktBuyplaque_add")
    public String sktBuyplaqueAdd() {
        return PREFIX + "sktBuyplaque_add.html";
    }

    /**
     * 跳转到修改线下商家购买牌匾
     */
    @RequestMapping("/sktBuyplaque_update/{sktBuyplaqueId}")
    public String sktBuyplaqueUpdate(@PathVariable Integer sktBuyplaqueId, Model model) {
        SktBuyplaque sktBuyplaque = sktBuyplaqueService.selectById(sktBuyplaqueId);
        model.addAttribute("item",sktBuyplaque);
        LogObjectHolder.me().set(sktBuyplaque);
        return PREFIX + "sktBuyplaque_edit.html";
    }

    /**
     * 获取线下商家购买牌匾列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktBuyplaque sktBuyplaque) {
    	Page<SktBuyplaque> page = new PageFactory<SktBuyplaque>().defaultPage();
        List<SktBuyplaque> list = sktBuyplaqueService.sktBuyplaquefindall(page,sktBuyplaque);
        page.setRecords(list);
		return super.packForBT(page);
    }

    /**
     * 新增线下商家购买牌匾
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktBuyplaque sktBuyplaque) {
        sktBuyplaqueService.insert(sktBuyplaque);
        return SUCCESS_TIP;
    }

    /**
     * 删除线下商家购买牌匾
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktBuyplaqueId) {
        sktBuyplaqueService.deleteById(sktBuyplaqueId);
        return SUCCESS_TIP;
    }

    /**
     * 修改线下商家购买牌匾
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktBuyplaque sktBuyplaque,Integer orderId) {
    	sktBuyplaque.setOrderId(orderId);
        sktBuyplaqueService.sktBuyplaqueUpdate(sktBuyplaque);
        return SUCCESS_TIP;
    }
    
    /**
     * 修改线下商家购买牌匾2
     */
    @RequestMapping(value = "/updatea")
    @ResponseBody
    public Object updatea(SktBuyplaque sktBuyplaque,Integer orderId) {
    	sktBuyplaque.setOrderId(orderId);
        sktBuyplaqueService.sktBuyplaqueUpdate2(sktBuyplaque);
        return SUCCESS_TIP;
    }
    
    /**
     * 线下商家购买牌匾详情
     */
    @RequestMapping(value = "/detail/{sktBuyplaqueId}")
    @ResponseBody
    public Object detail(@PathVariable("sktBuyplaqueId") Integer sktBuyplaqueId) {
        return sktBuyplaqueService.selectById(sktBuyplaqueId);
    }
    
    

}
