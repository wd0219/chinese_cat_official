package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.GoodsScores;
import com.stylefeng.guns.modular.huamao.dao.GoodsScoresMapper;
import com.stylefeng.guns.modular.huamao.service.IGoodsScoresService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品评分表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsScoresServiceImpl extends ServiceImpl<GoodsScoresMapper, GoodsScores> implements IGoodsScoresService {

}
