package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.model.UsersUpgradeDTO;

/**
 * <p>
 * 用户升级角色订单表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
public interface UsersUpgradeMapper extends BaseMapper<UsersUpgrade> {
	/**
	 * 显示用户升级列表
	 * @param page 
	 * @param usersUpgradeDTO
	 * @return
	 */
	public List<UsersUpgradeDTO> showUsersShengJiInfo(@Param("page")Page<UsersUpgradeDTO> page, 
			@Param("usersUpgradeDTO")UsersUpgradeDTO usersUpgradeDTO);
	public Integer showUsersShengJiInfoCount(@Param("usersUpgradeDTO")UsersUpgradeDTO usersUpgradeDTO);
	/**
	 * 更新升级订单状态
	 * @param orderId
	 * @param status
	 */
	public void updateUpgradeStatus(@Param(value="orderId") Integer orderId,@Param(value="status") Integer status,
			@Param(value="preRole") Integer preRole,@Param(value="afterRole") Integer afterRole,
			@Param(value="upgradeTime") String upgradeTime
			);
}
