package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.BrandsCustem;

/**
 * <p>
 * 品牌表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface BrandsMapper extends BaseMapper<Brands> {
  public List<Map<String,Object>> selectAll1(@Param("page")Page<Map<String, Object>> page, 
		 @Param("brandsCustem") BrandsCustem brandsCustem);
  public Integer selectAllCount1(@Param("brandsCustem") BrandsCustem brandsCustem);
  Integer insert (BrandsCustem brandsCustem);
  Map<String,Object> selectMapById(Integer brandsId);
  List<Map<String,Object>> selectRecommend(BrandsCustem brandsCustem);
  List<Map<String,Object>>  selectNotRecommend(BrandsCustem brandsCustem);
 int selectBrandsTotal();
  
}
