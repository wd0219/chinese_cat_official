package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktPointuser;
import com.stylefeng.guns.modular.huamao.dao.SktPointuserMapper;
import com.stylefeng.guns.modular.huamao.service.ISktPointuserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 点赞用户表(预留表) 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
@Service
public class SktPointuserServiceImpl extends ServiceImpl<SktPointuserMapper, SktPointuser> implements ISktPointuserService {

}
