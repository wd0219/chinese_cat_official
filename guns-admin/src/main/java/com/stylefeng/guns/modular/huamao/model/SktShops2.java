package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Transient;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 商家信息表
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
@TableName("skt_shops")
public class SktShops2 extends Model<SktShops> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "shopId", type = IdType.AUTO)
    private Integer shopId;
    
    private String loginName;
    
    private String shopName;
    
    private String logo;
    
	public SktShops2() {
		super();
		// TODO Auto-generated constructor stub
	}


	public SktShops2(Integer shopId, String loginName, String shopName, String logo) {
		super();
		this.shopId = shopId;
		this.loginName = loginName;
		this.shopName = shopName;
		this.logo = logo;
	}


	@Override
	public String toString() {
		return "SktShops2 [shopId=" + shopId + ", loginName=" + loginName + ", shopName=" + shopName + ", logo=" + logo
				+ "]";
	}


	public Integer getShopId() {
		return shopId;
	}


	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}


	public String getLoginName() {
		return loginName;
	}


	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}


	public String getShopName() {
		return shopName;
	}


	public void setShopName(String shopName) {
		this.shopName = shopName;
	}


	public String getLogo() {
		return logo;
	}


	public void setLogo(String logo) {
		this.logo = logo;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	protected Serializable pkVal() {
		return null;
	}


    
}
