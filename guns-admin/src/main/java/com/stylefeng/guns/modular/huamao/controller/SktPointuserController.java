package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.SktPointuser;
import com.stylefeng.guns.modular.huamao.service.ISktPointuserService;

/**
 * 视屏点赞用户表控制器
 *
 * @author fengshuonan
 * @Date 2018-07-26 09:55:13
 */
@Controller
@RequestMapping("/sktPointuser")
public class SktPointuserController extends BaseController {

    private String PREFIX = "/huamao/sktPointuser/";

    @Autowired
    private ISktPointuserService sktPointuserService;

    /**
     * 跳转到视屏点赞用户表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktPointuser.html";
    }

    /**
     * 跳转到添加视屏点赞用户表
     */
    @RequestMapping("/sktPointuser_add")
    public String sktPointuserAdd() {
        return PREFIX + "sktPointuser_add.html";
    }

    /**
     * 跳转到修改视屏点赞用户表
     */
    @RequestMapping("/sktPointuser_update/{sktPointuserId}")
    public String sktPointuserUpdate(@PathVariable Integer sktPointuserId, Model model) {
        SktPointuser sktPointuser = sktPointuserService.selectById(sktPointuserId);
        model.addAttribute("item",sktPointuser);
        LogObjectHolder.me().set(sktPointuser);
        return PREFIX + "sktPointuser_edit.html";
    }

    /**
     * 获取视屏点赞用户表列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktPointuserService.selectList(null);
    }

    /**
     * 新增视屏点赞用户表
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktPointuser sktPointuser) {
        sktPointuserService.insert(sktPointuser);
        return SUCCESS_TIP;
    }

    /**
     * 删除视屏点赞用户表
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktPointuserId) {
        sktPointuserService.deleteById(sktPointuserId);
        return SUCCESS_TIP;
    }

    /**
     * 修改视屏点赞用户表
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktPointuser sktPointuser) {
        sktPointuserService.updateById(sktPointuser);
        return SUCCESS_TIP;
    }

    /**
     * 视屏点赞用户表详情
     */
    @RequestMapping(value = "/detail/{sktPointuserId}")
    @ResponseBody
    public Object detail(@PathVariable("sktPointuserId") Integer sktPointuserId) {
        return sktPointuserService.selectById(sktPointuserId);
    }
}
