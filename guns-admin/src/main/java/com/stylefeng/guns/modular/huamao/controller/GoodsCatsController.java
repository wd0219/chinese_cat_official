package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.AttributesCustem;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.model.SktAreas;
import com.stylefeng.guns.modular.huamao.model.SpecCatsCustem;
import com.stylefeng.guns.modular.huamao.service.IAttributesService;
import com.stylefeng.guns.modular.huamao.service.IGoodsCatsService;
import com.stylefeng.guns.modular.huamao.service.ISpecCatsService;
import com.stylefeng.guns.modular.huamao.warpper.GoodsCatsWapper;

/**
 * 商品分类控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:31:06
 */
@Controller
@RequestMapping("/goodsCats")
public class GoodsCatsController extends BaseController {

    private String PREFIX = "/huamao/goodsCats/";

    @Autowired
    private IGoodsCatsService goodsCatsService;
    @Autowired
    private IAttributesService attributesService;
    @Autowired
    private ISpecCatsService specCatsService;
    /**
     * 查询 根据父ID查询  返回json格式
     */
   @RequestMapping("/selectByParentId")
   @ResponseBody
    public JSONArray selectByParentId(@RequestParam(value ="id") Integer id,Model model) {
    	JSONArray jSONArray=new JSONArray();
    	
    	List<GoodsCats> goodscats = goodsCatsService.selectByParentId(id);
    	if(goodscats.size()>0 && goodscats!=null){
    		for (GoodsCats goodcat : goodscats) {
        		JSONObject json=new JSONObject();
        		json.put("catId", goodcat.getCatId());
        		json.put("catName", goodcat.getCatName());
        		jSONArray.add(json);	
    		}
    	}
    	
    	return jSONArray;
    }
   
   
   /**
    * 查询  返回json格式 修改商品属性时查询商品分类
    */
  @RequestMapping("/selectAllWhenAttributes")
  @ResponseBody
   public JSONObject selectAllWhenAttributes( Integer id) {
   	JSONArray jSONArray=new JSONArray();
   	List<GoodsCats> goodscats = goodsCatsService.selectList(null);
   	for (GoodsCats goodcat : goodscats) {
   		JSONObject json=new JSONObject();
   		json.put("catId", goodcat.getCatId());
   		json.put("catName", goodcat.getCatName());
   		jSONArray.add(json);	
	}
   	Integer goods1=null;
	Integer goods2=null;
	Integer goods3=null;
	JSONArray jSONArray1=new JSONArray();
	JSONArray jSONArray2=new JSONArray();
	JSONArray jSONArray3=new JSONArray();
//	AttributesCustem attributesCustem=new AttributesCustem();
    AttributesCustem attributesCustem =  attributesService.selectAttributesCustem(id);
    List <Integer> list1 =new ArrayList<Integer>();
  
	Integer goodsCatsId=attributesCustem.getGoodsCatId();
	GoodsCats goodsCats;
	goodsCats = goodsCatsService.selectById(goodsCatsId);
	while(goodsCats.getParentId()!=0){
		Integer catId = goodsCats.getCatId();
		list1.add(catId);
		goodsCats = goodsCatsService.selectById(goodsCats.getParentId());
		if(goodsCats.getParentId()==0){
			list1.add(goodsCats.getCatId());
		}
	}
	JSONObject jsons=new JSONObject();
	if(list1.size()==3){
		goods1=list1.get(2);
	//	JSONArray jSONArray1=new JSONArray();
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		jsons.put("goods1", goods1);
		goods2=list1.get(1);
		
		
		List<GoodsCats> goodscats2 = goodsCatsService.selectByParentId(goods1);
		for (GoodsCats goodcat : goodscats2) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray2.add(json);	
		}
		jsons.put("goods2", goods2);
		
		goods3=list1.get(0);
		
		List<GoodsCats> goodscats3 = goodsCatsService.selectByParentId(goods2);
		for (GoodsCats goodcat : goodscats3) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray3.add(json);	
		}
		jsons.put("goods3", goods3);
		
		
	}
	if(list1.size()==2){
		goods1=list1.get(1);
		jsons.put("goods1", goods1);
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		
		goods2=list1.get(0);
		jsons.put("goods2", goods2);
		List<GoodsCats> goodscats2 = goodsCatsService.selectByParentId(goods1);
		for (GoodsCats goodcat : goodscats2) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray2.add(json);	
		}
		jsons.put("goods3", "");
		
		
	}
	if(list1.size()==1){
		goods1=list1.get(0);
		jsons.put("goods1", goods1);
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		jsons.put("goods2", "");
		jsons.put("goods3", "");
		/*GoodsCats	goodsCats1 = goodsCatsService.selectById(goods1);
		attributesCustem.setGoods1(goods1);
		attributesCustem.setCatName1(goodsCats1.getCatName());*/
		/*goods2=list1.get(1);
		GoodsCats	goodsCats2 = goodsCatsService.selectById(goods2);
		attributesCustem.setGoods2(goods2);
		attributesCustem.setCatName2(goodsCats2.getCatName());
		goods3=list1.get(0);
		GoodsCats	goodsCats3 = goodsCatsService.selectById(goods1);
		attributesCustem.setGoods3(goods3);
		attributesCustem.setCatName3(goodsCats3.getCatName());*/
		
	}
	jsons.put("jSONArray1", jSONArray1);
	jsons.put("jSONArray2", jSONArray2);
	jsons.put("jSONArray3", jSONArray3);
	
	jsons.put("jSONArray", jSONArray);
   	return jsons;
   }
  /**
   * 查询  返回json格式 修改商品规格时查询商品分类
   */
 @RequestMapping("/selectAllWhenSpecCats")
 @ResponseBody
  public JSONObject selectAllWhenSpecCats( Integer id) {
  	JSONArray jSONArray=new JSONArray();
  	List<GoodsCats> goodscats = goodsCatsService.selectList(null);
  	for (GoodsCats goodcat : goodscats) {
  		JSONObject json=new JSONObject();
  		json.put("catId", goodcat.getCatId());
  		json.put("catName", goodcat.getCatName());
  		jSONArray.add(json);	
	}
  	Integer goods1=null;
	Integer goods2=null;
	Integer goods3=null;
	JSONArray jSONArray1=new JSONArray();
	JSONArray jSONArray2=new JSONArray();
	JSONArray jSONArray3=new JSONArray();
//	AttributesCustem attributesCustem=new AttributesCustem();
	SpecCatsCustem specCatsCustem =  specCatsService.selectSpecCatsCustemById(id);
   List <Integer> list1 =new ArrayList<Integer>();
 
	Integer goodsCatsId=specCatsCustem.getGoodsCatId();
	GoodsCats goodsCats;
	goodsCats = goodsCatsService.selectById(goodsCatsId);
	while(goodsCats.getParentId()!=0){
		Integer catId = goodsCats.getCatId();
		list1.add(catId);
		goodsCats = goodsCatsService.selectById(goodsCats.getParentId());
		if(goodsCats.getParentId()==0){
			list1.add(goodsCats.getCatId());
		}
	}
	JSONObject jsons=new JSONObject();
	if(list1.size()==3){
		goods1=list1.get(2);
	//	JSONArray jSONArray1=new JSONArray();
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		jsons.put("goods1", goods1);
		goods2=list1.get(1);
		
		
		List<GoodsCats> goodscats2 = goodsCatsService.selectByParentId(goods1);
		for (GoodsCats goodcat : goodscats2) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray2.add(json);	
		}
		jsons.put("goods2", goods2);
		
		goods3=list1.get(0);
		
		List<GoodsCats> goodscats3 = goodsCatsService.selectByParentId(goods2);
		for (GoodsCats goodcat : goodscats3) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray3.add(json);	
		}
		jsons.put("goods3", goods3);
		
		
	}
	if(list1.size()==2){
		goods1=list1.get(1);
		jsons.put("goods1", goods1);
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		
		goods2=list1.get(0);
		jsons.put("goods2", goods2);
		List<GoodsCats> goodscats2 = goodsCatsService.selectByParentId(goods1);
		for (GoodsCats goodcat : goodscats2) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray2.add(json);	
		}
		jsons.put("goods3", "");
		
		
	}
	if(list1.size()==1){
		goods1=list1.get(0);
		jsons.put("goods1", goods1);
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		jsons.put("goods2", "");
		jsons.put("goods3", "");
		/*GoodsCats	goodsCats1 = goodsCatsService.selectById(goods1);
		attributesCustem.setGoods1(goods1);
		attributesCustem.setCatName1(goodsCats1.getCatName());*/
		/*goods2=list1.get(1);
		GoodsCats	goodsCats2 = goodsCatsService.selectById(goods2);
		attributesCustem.setGoods2(goods2);
		attributesCustem.setCatName2(goodsCats2.getCatName());
		goods3=list1.get(0);
		GoodsCats	goodsCats3 = goodsCatsService.selectById(goods1);
		attributesCustem.setGoods3(goods3);
		attributesCustem.setCatName3(goodsCats3.getCatName());*/
		
	}
	jsons.put("jSONArray1", jSONArray1);
	jsons.put("jSONArray2", jSONArray2);
	jsons.put("jSONArray3", jSONArray3);
	
	jsons.put("jSONArray", jSONArray);
  	return jsons;
  }
    /**
     * 跳转到商品分类首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "goodsCats.html"; 
    }

    /**
     * 跳转到添加商品分类
     */
    @RequestMapping("/goodsCats_add")
    public String goodsCatsAdd() {
        return PREFIX + "goodsCats_add.html";
    }
    /**
     * 跳转到添加商品子类
     */
    @RequestMapping("/goodsCatsChild_add/{goodsCatsId}")
    public String goodsCatsChildAdd(@PathVariable Integer goodsCatsId, Model model) {
    	 model.addAttribute("goodsCatsId",goodsCatsId);
     //    LogObjectHolder.me().set(goodsCats);
        return PREFIX + "goodsCats_child_add.html";
    }
    /**
     * 跳转到修改商品分类
     */
    @RequestMapping("/goodsCats_update/{goodsCatsId}")
    public String goodsCatsUpdate(@PathVariable Integer goodsCatsId, Model model) {
        GoodsCats goodsCats = goodsCatsService.selectById(goodsCatsId);

        String catImg = goodsCats.getCatImg();
		String allUrl = UrlUtils.getAllUrl(catImg);
		goodsCats.setCatImg(allUrl);

		model.addAttribute("item",goodsCats);
        LogObjectHolder.me().set(goodsCats);
        return PREFIX + "goodsCats_edit.html";
    }

    /**
     * 获取商品分类列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(GoodsCats goodsCats) {
    	 return	goodsCatsService.selectMap(goodsCats);
//      List<Map<String, Object>> list = goodsCatsService.selectMaps(null);
//       return new GoodsCatsWapper(list).warp();
    }
   
    
    /**
     * 获取商品分类列表
     */
    @RequestMapping(value = "/list1")
    @ResponseBody
    public Object list1() {
    //	String a="[{text: \"p1\", nodes: [{ text: \"p1-1\", id: '00001', nodeId: '00001' },{ text: \"p1-2\", id: '00002' }, { text: \"p1-3\", id: '00003' },{ text: \"p1-4\", id: '00004', nodes: [{ text: 'p1-1-1', id: '00005'}]}]}]";
    	
    	 List<GoodsCats> goodsCatslist = goodsCatsService.selectList(null);
    	 JSONArray jSONArray=new JSONArray();
    	 for(GoodsCats goodsCat:goodsCatslist){
    		 JSONObject json=new JSONObject();
    		 json.put("id", goodsCat.getCatId());
    		 json.put("pid", goodsCat.getParentId());
    		 json.put("catName", goodsCat.getCatName());
    		 String isFloorName=null;
    		 String isShowName=null;
    		 if(goodsCat.getIsFloor()==1){
    			 isFloorName="推荐";
    		 }else if(goodsCat.getIsFloor()==0){
    			 isFloorName="不推荐";
    		 }
    		 if(goodsCat.getIsShow()==1){
    			 isShowName="显示";
    		 }else if(goodsCat.getIsShow()==0){
    			 isShowName="不显示";
    		 }
    		 json.put("isFloor", isFloorName);
    		 json.put("isShow", isShowName);
    		 json.put("catSort", goodsCat.getCatSort());
    		 jSONArray.add(json);
    		 
    	 }
    	  return jSONArray;
	}
    /**
     * 新增商品分类
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(GoodsCats goodsCats) {
    	if(goodsCats.getParentId()==null || "".equals(goodsCats.getParentId())){
    		goodsCats.setParentId(0);
    	}    	
    	goodsCats.setCreateTime(new Date());
    	goodsCats.setDataFlag(1);    	
        goodsCatsService.insert(goodsCats);
        return SUCCESS_TIP;
    }
    
 

    /**
     * 删除商品分类
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer goodsCatsId) {
    	List<GoodsCats> goodsCatslist = goodsCatsService.selectByParentId(goodsCatsId);
    	 if(goodsCatslist!=null && goodsCatslist.size()>0){
    		 for(GoodsCats goodsCat:goodsCatslist){
    			 List<GoodsCats> goodsCatslist2 = goodsCatsService.selectByParentId(goodsCat.getCatId()); 
	    		 if(goodsCatslist2!=null && goodsCatslist2.size()>0){
	    			 goodsCatsService.updateByPId(goodsCat.getCatId());
	    		 }
	    		 goodsCatsService.deleteDataFlagById(goodsCat.getCatId());
    		 }    		 
    	 }
        goodsCatsService.deleteDataFlagById(goodsCatsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品分类
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(GoodsCats goodsCats) {
        goodsCatsService.updateById(goodsCats);
        return SUCCESS_TIP;
    }

    /**
     * 商品分类详情
     */
    @RequestMapping(value = "/detail/{goodsCatsId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsCatsId") Integer goodsCatsId) {
        return goodsCatsService.selectById(goodsCatsId);
    }
    
    /**
     * 商品分类详情
     */
    @RequestMapping(value = "/selectByCatId/{goodsCatsId}")
    @ResponseBody
    public Object selectParentIdByCatId(@PathVariable("goodsCatsId") Integer goodsCatsId) {
    	JSONArray jSONArray=new JSONArray();
    	GoodsCats goodsCats;
    	goodsCats = goodsCatsService.selectById(goodsCatsId);
    	while (goodsCats.getParentId()!=0) {
    		goodsCats = goodsCatsService.selectById(goodsCats.getCatId());
    		
		}
        return goodsCats;
    }
    
}
