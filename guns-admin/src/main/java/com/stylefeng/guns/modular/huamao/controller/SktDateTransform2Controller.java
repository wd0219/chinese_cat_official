package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktDateTransform;
import com.stylefeng.guns.modular.huamao.service.ISktDateTransformService;

/**
 * 转换记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 18:28:42
 */
@Controller
@RequestMapping("/sktDateTransform2")
public class SktDateTransform2Controller extends BaseController {

    private String PREFIX = "/huamao/sktDateTransform2/";

    @Autowired
    private ISktDateTransformService sktDateTransform2Service;

    /**
     * 跳转到转换记录首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktDateTransform2.html";
    }

    /**
     * 跳转到添加转换记录
     */
    @RequestMapping("/sktDateTransform2_add")
    public String sktDateTransform2Add() {
        return PREFIX + "sktDateTransform2_add.html";
    }

    /**
     * 跳转到修改转换记录
     */
    @RequestMapping("/sktDateTransform2_update/{sktDateTransform2Id}")
    public String sktDateTransform2Update(@PathVariable Integer sktDateTransform2Id, Model model) {
        SktDateTransform sktDateTransform2 = sktDateTransform2Service.selectById(sktDateTransform2Id);
        model.addAttribute("item",sktDateTransform2);
        LogObjectHolder.me().set(sktDateTransform2);
        return PREFIX + "sktDateTransform2_edit.html";
    }

    /**
     * 获取转换记录列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktDateTransform sktDateTransform2) {
    	Page<SktDateTransform> page = new PageFactory<SktDateTransform>().defaultPage();
    	List<SktDateTransform> list = sktDateTransform2Service.SktDateTransformfindAll2(page,sktDateTransform2);
    	page.setRecords(list);
		return super.packForBT(page);
    }

    /**
     * 新增转换记录
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktDateTransform sktDateTransform2) {
        sktDateTransform2Service.insert(sktDateTransform2);
        return SUCCESS_TIP;
    }

    /**
     * 删除转换记录
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktDateTransform2Id) {
        sktDateTransform2Service.deleteById(sktDateTransform2Id);
        return SUCCESS_TIP;
    }

    /**
     * 修改转换记录
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktDateTransform sktDateTransform2) {
        sktDateTransform2Service.updateById(sktDateTransform2);
        return SUCCESS_TIP;
    }

    /**
     * 转换记录详情
     */
    @RequestMapping(value = "/detail/{sktDateTransform2Id}")
    @ResponseBody
    public Object detail(@PathVariable("sktDateTransform2Id") Integer sktDateTransform2Id) {
        return sktDateTransform2Service.selectById(sktDateTransform2Id);
    }
}
