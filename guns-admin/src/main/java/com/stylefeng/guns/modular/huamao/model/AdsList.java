package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.annotations.TableField;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 广告表扩展类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public class AdsList extends Ads {

	/**
	 * 广告位置
	 */
	@TableField(exist = false)
	private String positionName;
	/**
	 * 广告开始日期
	 */
	@TableField(exist = false)
	private String startDate;
	/**
	 * 广告结束日期
	 */
	@TableField(exist = false)
	private String endDate;

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
