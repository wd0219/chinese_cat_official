package com.stylefeng.guns.modular.system.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktShopCats;
import com.stylefeng.guns.modular.huamao.dao.SktShopCatsMapper;
import com.stylefeng.guns.modular.system.service.ISktShopCatsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 店铺分类表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-29
 */
@Service
public class SktShopCatsServiceImpl extends ServiceImpl<SktShopCatsMapper, SktShopCats> implements ISktShopCatsService {

}
