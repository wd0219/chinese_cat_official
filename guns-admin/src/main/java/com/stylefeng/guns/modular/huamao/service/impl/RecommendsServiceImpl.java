package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.dao.RecommendsMapper;
import com.stylefeng.guns.modular.huamao.service.ICatBrandsService;
import com.stylefeng.guns.modular.huamao.service.IGoodsService;
import com.stylefeng.guns.modular.huamao.service.IRecommendsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 推荐记录表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-24
 */
@Service
public class RecommendsServiceImpl extends ServiceImpl<RecommendsMapper, Recommends> implements IRecommendsService {
	@Autowired
	private ICatBrandsService catBrandsService;
	@Autowired
	private IGoodsService goodsService;
	@Override
	public 	Map<String,Object>  selectMax() {
		// TODO Auto-generated method stub
		return baseMapper.selectMax();
	}

	@Override
	public boolean deleteByDataId(Integer dataId) {
		// TODO Auto-generated method stub
		return baseMapper.deleteByDataId(dataId);
	}

	@Override
	public Map<String, Object> selectBrand(Integer dataId) {
		// TODO Auto-generated method stub
		return baseMapper.selectBrand(dataId);
	}
	@Override
	public boolean deleteByMap (Map<String,Object> map){
		int flag = baseMapper.deleteByMap(map);
		if (flag==1){
			return  true;
		}else {
			return false;
		}
	}

	public Integer insertRrecommends (Map<String,Object> map){
	return 	baseMapper.insertRrecommends(map);
	}

	/**
	 * 商品推荐
	 * @param list1
	 * @param catId
	 */
	@Override
	@Transactional
	public  void addBrandsList(List<Map<String,Object>> list1,Integer catId){
		//删除该分类下的品牌

		if(list1!=null && list1.size()>0){
			Set<Integer> set =new HashSet();
			for (Map<String,Object> map1:list1){
				Integer brandsId=Integer.parseInt(map1.get("id").toString());
				CatBrands catBrands=new CatBrands();
				catBrands.setBrandId(brandsId);
				EntityWrapper<CatBrands> entityWrapper=new EntityWrapper<CatBrands>(catBrands);
				CatBrands catBrands1 = catBrandsService.selectOne(entityWrapper);
				Integer goodsCatId = catBrands1.getCatId();
				set.add(goodsCatId);
			}
			//删除
			for (Integer goodsCatId:set){
				Map<String ,Object> map=new HashMap<>();
				map.put("goodsCatId",goodsCatId);
				map.put("dataType",0);
				map.put("dataSrc",2);
				baseMapper.deleteByMap(map);
			}
			//增加
			for (Map<String,Object> map1:list1){
				Map<String ,Object> map=new HashMap<>();
                CatBrands catBrands = catBrandsService.selectById(Integer.parseInt(map1.get("id").toString()));
                Integer goodsCatId = catBrands.getCatId();
				map.put("goodsCatId",goodsCatId);
				map.put("dataType",0);
				map.put("dataId",Integer.parseInt(map1.get("id").toString()));
				Integer dataSort=Integer.parseInt(map1.get("dataSort").toString());
				map.put("dataSort",dataSort);
				map.put("dataSrc",2);
				baseMapper.insertRrecommends(map);
			}
		}else {
			Map<String ,Object> map=new HashMap<>();
			map.put("dataType",0);
			map.put("dataSrc",2);
			map.put("goodsCatId",catId);
			baseMapper.deleteByMap(map);
		}
	}

	/**
	 * 商品推荐
	 * @param list1
	 * @param catId
	 * @param dataType
	 */
	@Override
@Transactional
	public  void addGoodsList(List<Map<String,Object>> list1,Integer catId,Integer dataType){
		//删除该分类下的商品

		if(list1!=null && list1.size()>0){
			Set<Integer> set =new HashSet();
			for (Map<String,Object> map1:list1){
				Goods goods = goodsService.selectById(Integer.parseInt(map1.get("id").toString()));
				Integer goodsCatId = goods.getGoodsCatId();
				set.add(goodsCatId);
			}

			for (Integer goodsCatId:set){
				Map<String ,Object> map=new HashMap<>();
				map.put("goodsCatId",goodsCatId);
				map.put("dataType",dataType);
				map.put("dataSrc",0);
				baseMapper.deleteByMap(map);
			}

			//增加

			for (Map<String,Object> map1:list1){
				Map<String ,Object> mapParam=new HashMap<>();
				Goods goods = goodsService.selectById(Integer.parseInt(map1.get("id").toString()));
				Integer goodsCatId = goods.getGoodsCatId();
				mapParam.put("goodsCatId",goodsCatId);
				mapParam.put("dataType",dataType);
				mapParam.put("dataId",Integer.parseInt(map1.get("id").toString()));
				Integer dataSort=Integer.parseInt(map1.get("dataSort").toString());
				mapParam.put("dataSort",dataSort);
				mapParam.put("dataSrc",0);
				baseMapper.insertRrecommends(mapParam);

			}
		}else {
			Map<String ,Object> map=new HashMap<>();
			map.put("goodsCatId",catId);
			map.put("dataType",dataType);
			map.put("dataSrc",0);
			baseMapper.deleteByMap(map);
		}
	}


	public  List<Map<String,Object>> selectRecomGoods(GoodsCustm goodsCustm){
		return  baseMapper.selectRecomGoods(goodsCustm);
	}

	public	List<Map<String,Object>> selectRecommendBrands(BrandsCustem brandsCustem){
		return  baseMapper.selectRecommendBrands(brandsCustem);
	}
}
