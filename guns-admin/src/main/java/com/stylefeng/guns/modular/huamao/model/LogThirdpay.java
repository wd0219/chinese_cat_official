package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 第三方支付记录表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
@TableName("skt_log_thirdpay")
public class LogThirdpay extends Model<LogThirdpay> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 内部订单号
	 */
	private String orderNo;
	/**
	 * 业务类型 BUY_GOODS:商城消费 BUY_SCORE:购买积分 RECHARGE:充值 UPGRADE:升级
	 */
	private String businessType;
	/**
	 * 支付类型 1-aipay微信 2-aipay支付宝 3-aipay快捷
	 */
	private Integer payType;
	/**
	 * 商户号
	 */
	private String ino;
	/**
	 * 支付金额
	 */
	private BigDecimal payMoney;
	/**
	 * 支付流水单号
	 */
	private String outTradeNo;
	/**
	 * 请求数据
	 */
	private String request;
	/**
	 * 银行流水号或支付渠道号
	 */
	private String payTypeOrderNo;
	/**
	 * 支付成功返回的数据
	 */
	private String response;
	/**
	 * 响应时间
	 */
	private Date responseTime;
	/**
	 * 状态1已获取支付链接或跳入快捷页面 2已接收到异步响应 3处理完回调方法 4处理失败
	 */
	private Integer payStatus;
	/**
	 * 
	 */
	private Integer handleStatus;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 删除标记 0删除 1显示）
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;

	public LogThirdpay() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LogThirdpay(Integer id, Integer userId, String orderNo, String businessType, Integer payType, String ino,
			BigDecimal payMoney, String outTradeNo, String request, String payTypeOrderNo, String response,
			Date responseTime, Integer payStatus, Integer handleStatus, String remark, Integer dataFlag,
			Date createTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.orderNo = orderNo;
		this.businessType = businessType;
		this.payType = payType;
		this.ino = ino;
		this.payMoney = payMoney;
		this.outTradeNo = outTradeNo;
		this.request = request;
		this.payTypeOrderNo = payTypeOrderNo;
		this.response = response;
		this.responseTime = responseTime;
		this.payStatus = payStatus;
		this.handleStatus = handleStatus;
		this.remark = remark;
		this.dataFlag = dataFlag;
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public String getIno() {
		return ino;
	}

	public void setIno(String ino) {
		this.ino = ino;
	}

	public BigDecimal getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getPayTypeOrderNo() {
		return payTypeOrderNo;
	}

	public void setPayTypeOrderNo(String payTypeOrderNo) {
		this.payTypeOrderNo = payTypeOrderNo;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Date getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Integer getHandleStatus() {
		return handleStatus;
	}

	public void setHandleStatus(Integer handleStatus) {
		this.handleStatus = handleStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "LogThirdpay [id=" + id + ", userId=" + userId + ", orderNo=" + orderNo + ", businessType="
				+ businessType + ", payType=" + payType + ", ino=" + ino + ", payMoney=" + payMoney + ", outTradeNo="
				+ outTradeNo + ", request=" + request + ", payTypeOrderNo=" + payTypeOrderNo + ", response=" + response
				+ ", responseTime=" + responseTime + ", payStatus=" + payStatus + ", handleStatus=" + handleStatus
				+ ", remark=" + remark + ", dataFlag=" + dataFlag + ", createTime=" + createTime + "]";
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
