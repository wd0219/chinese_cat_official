package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsDTO;

/**
 * <p>
 * 代理公司表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
public interface SktAgentsMapper extends BaseMapper<SktAgents> {
	/**
	 * 查询显示列表
	 * @param page 
	 * @param sktAgentsDTO
	 * @return
	 */
	public List<SktAgentsDTO> showSktAgentsIno(@Param(value="page")Page<SktAgentsDTO> page, 
			@Param(value="sktAgentsDTO")SktAgentsDTO sktAgentsDTO);
	public Integer showSktAgentsInoCount(@Param(value="sktAgentsDTO")SktAgentsDTO sktAgentsDTO);
	/**
	 * 查询名字跟id 电话
	 * @param nameOrPhone
	 * @return
	 */
	public Map<String, Object> selectNameOrPhone(@Param(value="nameOrPhone")String nameOrPhone);
	/**
	 * 重复
	 * @param sktAgents
	 * @return
	 */
	public List<SktAgents> selectHasAgents(SktAgents sktAgents);
}
