package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 分类-品牌表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_cat_brands")
public class CatBrands extends Model<CatBrands> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 分类ID
     */
    private Integer catId;
    /**
     * 品牌ID
     */
    private Integer brandId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CatBrands{" +
        "id=" + id +
        ", catId=" + catId +
        ", brandId=" + brandId +
        "}";
    }
}
