package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStoresDto;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 线下商家订单表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-24
 */
public interface ISktOrdersStoresService extends IService<SktOrdersStores> {
	/**
	 * 页面显示列表
	 * @param page 
	 * @param sktOrdersStoresDto
	 * @return
	 */
	public List<SktOrdersStoresDto> showOrdersStoresInfo (Page<SktOrdersStoresDto> page, SktOrdersStoresDto sktOrdersStoresDto);
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int selectOrdersStoresTotal();

    List<SktOrdersStoresDto> showOrdersStoresInfo2(SktOrdersStoresDto sktOrdersStoresDto);
}
