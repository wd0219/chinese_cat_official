package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktOrdersList;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersService;

/**
 * <p>
 * 线上商城订单表 服务实现类
 * </p>
 *
 * @author caody123
 * @since 2018-04-14
 */
@Service
public class SktOrdersServiceImpl extends ServiceImpl<SktOrdersMapper, SktOrders> implements ISktOrdersService {

	@Autowired
	SktOrdersMapper som;
	
	@Override
	public List<SktOrdersList> selectOrderShops(Page<SktOrdersList>page,SktOrdersList sktOrdersList) {
		List<SktOrdersList> list = som.selectOrderShops(page,sktOrdersList);
		Integer total = som.selectOrderShopsCount(sktOrdersList);
		page.setTotal(total);
		return list;
	}

	@Override
	public Object selectOrderScore(Map<String,Object> param) {
		// TODO Auto-generated method stub
		return som.selectOrderScore(param);
	}

	@Override
	public Object selectOrderGoods(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return som.selectOrderGoods(param);
	}

	@Override
	public Object selectOrderGoodsParameter(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return som.selectOrderGoodsParameter(param);
	}

	@Override
	public int selectCount() {
		// TODO Auto-generated method stub
		return som.selectCount();
	}

	@Override
	public int selectOrderTotal() {
		// TODO Auto-generated method stub
		return som.selectOrderTotal();
	}

}
