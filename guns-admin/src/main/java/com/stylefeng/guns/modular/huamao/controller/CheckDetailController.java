package com.stylefeng.guns.modular.huamao.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.huamao.model.AccountList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.CheckDetail;
import com.stylefeng.guns.modular.huamao.model.CheckDetailList;
import com.stylefeng.guns.modular.huamao.service.ICheckDetailService;

/**
 * 用户每日对账控制器
 *
 * @author fengshuonan
 * @Date 2018-04-13 14:54:33
 */
@Controller
@RequestMapping("/checkDetail")
public class CheckDetailController extends BaseController {

	private String PREFIX = "/huamao/checkDetail/";

	@Autowired
	private ICheckDetailService checkDetailService;

	/**
	 * 跳转到用户每日对账首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "checkDetail.html";
	}

	/**
	 * 跳转到添加用户每日对账
	 */
	@RequestMapping("/checkDetail_add")
	public String checkDetailAdd() {
		return PREFIX + "checkDetail_add.html";
	}

	/**
	 * 跳转到修改用户每日对账
	 */
	@RequestMapping("/checkDetail_update/{checkDetailId}")
	public String checkDetailUpdate(@PathVariable Integer checkDetailId, Model model) {
		CheckDetail checkDetail = checkDetailService.selectById(checkDetailId);
		model.addAttribute("item", checkDetail);
		LogObjectHolder.me().set(checkDetail);
		return PREFIX + "checkDetail_edit.html";
	}

	/**
	 * 获取用户每日对账列表
	 * 
	 * @throws ParseException
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(CheckDetailList checkDetailList, String beginTime1, String endTime1) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyyMMdd");
		if (beginTime1 != null && beginTime1 != "") {
			// 将date转换成Integer
			Date parse = simpleDateFormat.parse(beginTime1);
			String format = simpleDateFormat2.format(parse);
			Integer valueOf = Integer.valueOf(format);
			checkDetailList.setBeginTime(valueOf);
		}
		if (endTime1 != null && endTime1 != "") {
			Date parse2 = simpleDateFormat.parse(endTime1);
			String format2 = simpleDateFormat2.format(parse2);
			Integer valueOf2 = Integer.valueOf(format2);
			checkDetailList.setEndTime(valueOf2);
		}
		Page<CheckDetailList> page = new PageFactory<CheckDetailList>().defaultPage();
		List<CheckDetailList> checkDetailLists = checkDetailService.selectCheckDetailAll(page,checkDetailList);
		page.setRecords(checkDetailLists);
		// return checkDetailService.selectList(null);
		return super.packForBT(page);
	}

	/**
	 * 新增用户每日对账
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(CheckDetail checkDetail) {
		checkDetailService.insert(checkDetail);
		return SUCCESS_TIP;
	}

	/**
	 * 删除用户每日对账
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer checkDetailId) {
		checkDetailService.deleteById(checkDetailId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改用户每日对账
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(CheckDetail checkDetail) {
		checkDetailService.updateById(checkDetail);
		return SUCCESS_TIP;
	}

	/**
	 * 用户每日对账详情
	 */
	@RequestMapping(value = "/detail/{checkDetailId}")
	public Object detail(@PathVariable("checkDetailId") Integer checkDetailId, Model model) {
		CheckDetailList cd = checkDetailService.selectctDetailedById(checkDetailId);
		model.addAttribute("item", cd);
		return PREFIX + "checkDetail_detailed.html";
		// return checkDetailService.selectById(checkDetailId);
	}
}
