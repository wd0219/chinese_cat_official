package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktSysConfigs;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商城配置表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-26
 */
public interface SktSysConfigsMapper extends BaseMapper<SktSysConfigs> {
	/**
	 * 基础设置查询
	 * @return
	 */
	public List<Map<String, Object>> selectBaseSetInfo(@Param(value="page") Integer page);
	/**
	 * 更新图片信息
	 * @param fieldCode
	 * @param fieldValue
	 */
	public void updateFieldCode(@Param(value="fieldCode") String fieldCode,@Param(value="fieldValue") String fieldValue);

    SktSysConfigs selectSysConfigsByFieldCode(String directorProfit);
}
