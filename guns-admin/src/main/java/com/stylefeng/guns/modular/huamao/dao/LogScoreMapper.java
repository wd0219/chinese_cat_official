package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.LogScoreList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogScoreMapper extends BaseMapper<LogScore> {

	// 展示积分类表
	List<LogScoreList> selectLogScoreAll(@Param("page") Page<LogScoreList> page, @Param("logScoreList") LogScoreList logScoreList);

    Integer selectLogScoreAllCount(@Param("logScoreList") LogScoreList logScoreList);
}
