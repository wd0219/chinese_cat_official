package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktLogOperates;
import com.stylefeng.guns.modular.huamao.model.SktLogOperatesDto;
import com.stylefeng.guns.modular.huamao.service.ISktLogOperatesService;

import java.util.List;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2018-04-27 15:57:09
 */
@Controller
@RequestMapping("/sktLogOperates")
public class SktLogOperatesController extends BaseController {

    private String PREFIX = "/huamao/sktLogOperates/";

    @Autowired
    private ISktLogOperatesService sktLogOperatesService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktLogOperates.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/sktLogOperates_add")
    public String sktLogOperatesAdd() {
        return PREFIX + "sktLogOperates_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/sktLogOperates_update/{sktLogOperatesId}")
    public String sktLogOperatesUpdate(@PathVariable Integer sktLogOperatesId, Model model) {
        SktLogOperates sktLogOperates = sktLogOperatesService.selectById(sktLogOperatesId);
        model.addAttribute("item",sktLogOperates);
        LogObjectHolder.me().set(sktLogOperates);
        return PREFIX + "sktLogOperates_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktLogOperatesDto sktLogOperatesDto) {
        Page<SktLogOperatesDto> page = new PageFactory<SktLogOperatesDto>().defaultPage();
        List<SktLogOperatesDto> sktLogOperatesDtos = sktLogOperatesService.selectListDate(page,sktLogOperatesDto);
        page.setRecords(sktLogOperatesDtos);
        return super.packForBT(page);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktLogOperates sktLogOperates) {
        sktLogOperatesService.insert(sktLogOperates);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktLogOperatesId) {
        sktLogOperatesService.deleteById(sktLogOperatesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktLogOperates sktLogOperates) {
        sktLogOperatesService.updateById(sktLogOperates);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{sktLogOperatesId}")
    @ResponseBody
    public Object detail(@PathVariable("sktLogOperatesId") Integer sktLogOperatesId) {
        return sktLogOperatesService.selectById(sktLogOperatesId);
    }
}
