package com.stylefeng.guns.modular.system.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogManager;
import com.stylefeng.guns.core.log.factory.LogTaskFactory;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.huamao.service.IBrandsService;
import com.stylefeng.guns.modular.huamao.service.IGoodsAppraisesService;
import com.stylefeng.guns.modular.huamao.service.IGoodsService;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersService;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersStoresService;
import com.stylefeng.guns.modular.huamao.service.ISktShopApplysService;
import com.stylefeng.guns.modular.huamao.service.ISktShopsService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;
import com.stylefeng.guns.modular.system.model.LoginLog;


import com.stylefeng.guns.modular.system.service.ILoginLogService;
import com.stylefeng.guns.modular.system.service.INoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.List;
import java.util.Map;

import static com.stylefeng.guns.core.support.HttpKit.getIp;

/**
 * 总览信息
 *
 * @author fengshuonan
 * @Date 2017年3月4日23:05:54
 */
@Controller
@RequestMapping("/blackboard")
public class BlackboardController extends BaseController {

    @Autowired
    private INoticeService noticeService;
    @Autowired
	private IUsersService usersService;
    
    @Autowired
   	private ISktShopsService SktShopsService;
    
    @Autowired
   	private ISktShopApplysService sktShopApplysService;

    @Autowired
   	private IGoodsService goodsService;
    
    @Autowired
   	private ISktOrdersService sktOrdersService;
    
    @Autowired
   	private ISktOrdersStoresService sktOrdersStoresService;
    
    @Autowired
   	private IGoodsAppraisesService goodsAppraisesService;
    
    @Autowired
   	private IBrandsService brandsService;
    @Autowired
    private ILoginLogService loginLogService;
    

    /**
     * 跳转到黑板
     */
    @RequestMapping("")
    public String blackboard(Model model) {
        List<Map<String, Object>> notices = noticeService.list(null);
        model.addAttribute("noticeList", notices);
        int usersCount=  usersService.selectCount(); //今日新增会员数
        model.addAttribute("usersCount", usersCount);
        int shopCount =SktShopsService.selectCount();//今日新增商家数
        model.addAttribute("shopCount", shopCount);
        int shopApplyCount = sktShopApplysService.selectCount();//今日新增申请商家数
        model.addAttribute("shopApplyCount", shopApplyCount);
        int isSaleCount = goodsService.selectisSaleCount();//今日上架商品
        model.addAttribute("isSaleCount", isSaleCount);
        int goodsStatusCount = goodsService.selectGoodsStatusCount();//今日未审核商品
        model.addAttribute("goodsStatusCount", goodsStatusCount);
        int orderCount = sktOrdersService.selectCount(); //今日新增线上订单数量
        int ordersStoresCount=sktOrdersStoresService.selectCount();
        int orCount=orderCount+ordersStoresCount;//今日新增订单总数
        model.addAttribute("orCount", orCount);
        int usersTotal = usersService.getTotal();//会员总数
        model.addAttribute("usersTotal", usersTotal);
        int shopsTotal = SktShopsService.getTotal();//店铺总数量
        model.addAttribute("shopsTotal", shopsTotal);
        int goodsSaleTotal = goodsService.selectisSaleTotal();//上架商品数量
        model.addAttribute("goodsSaleTotal", goodsSaleTotal);
        int goodsStatusTotal = goodsService.selectGoodsStatusTotal();//待审核商品数量
        model.addAttribute("goodsStatusTotal", goodsStatusTotal);
        int orderTotal = sktOrdersService.selectOrderTotal();//商城线上订单总数量
        int ordersStoresTotal = sktOrdersStoresService.selectOrdersStoresTotal();//商城线下订单总数量
        int orTotal=orderTotal+ordersStoresTotal;
        model.addAttribute("orTotal", orTotal);
        int goodsAppraisesTotal = goodsAppraisesService.selectGoodsAppraisesTotal();//评价总数
        model.addAttribute("goodsAppraisesTotal", goodsAppraisesTotal);
        int brandsTotal = brandsService.selectBrandsTotal();
        model.addAttribute("brandsTotal", brandsTotal);
     // 获取操作人员id
     	ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
     	if(shiroUser == null){
            LogManager.me().executeLog(LogTaskFactory.exitLog(ShiroKit.getUser().getId(), getIp()));
            ShiroKit.getSubject().logout();
            return REDIRECT + "/login";
        }
     	int staffId=shiroUser.getId();
     	LoginLog loginLog=new LoginLog();
     	loginLog.setUserid(staffId);
     	Map<String, Object> map = loginLogService.selectLastLoginLogs(loginLog);
     	model.addAttribute("item", map);
     	String ip=(String) map.get("ip");
     
     	
//    	String address = loginLogService.getSubdivision(ip);
//     	model.addAttribute("address", address);
        return "/blackboard.html";
    }
}
