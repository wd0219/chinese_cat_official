package com.stylefeng.guns.modular.huamao.model;


public class SktSpecApplyDto extends SktSpecApply{
    /**
     * 分类id
     */
    private  Integer goods1;
    private  Integer goods2;
    private  Integer goods3;
    /**
     * 商品分类路径
     */
    private String goodClassifyPath;
    /**
     * 最后一级商品分类名称
     */
    private String lastClassifyPath;

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * 审核人
     */
    private String staffName;
    /**
     * 开始时间
     */
    private String beginTime;
    /**
     * 结束时间
     */
    private String endTime;

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public void setGoodClassifyPath(String goodClassifyPath) {
        this.goodClassifyPath = goodClassifyPath;
    }

    public void setLastClassifyPath(String lastClassifyPath) {
        this.lastClassifyPath = lastClassifyPath;
    }

    public String getGoodClassifyPath() {

        return goodClassifyPath;
    }

    public String getLastClassifyPath() {
        return lastClassifyPath;
    }

    public Integer getGoods1() {
        return goods1;
    }

    public void setGoods1(Integer goods1) {
        this.goods1 = goods1;
    }

    public Integer getGoods2() {
        return goods2;
    }

    public void setGoods2(Integer goods2) {
        this.goods2 = goods2;
    }

    public Integer getGoods3() {
        return goods3;
    }

    public void setGoods3(Integer goods3) {
        this.goods3 = goods3;
    }

    @Override
    public String toString() {
        return "SktSpecApplyDto{" +
                "goods1=" + goods1 +
                ", goods2=" + goods2 +
                ", goods3=" + goods3 +
                ", goodClassifyPath='" + goodClassifyPath + '\'' +
                ", lastClassifyPath='" + lastClassifyPath + '\'' +
                ", staffName='" + staffName + '\'' +
                ", beginTime='" + beginTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }
}
