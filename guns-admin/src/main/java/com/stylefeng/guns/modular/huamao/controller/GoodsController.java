package com.stylefeng.guns.modular.huamao.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;
import com.stylefeng.guns.modular.huamao.service.IGoodsCatsService;
import com.stylefeng.guns.modular.huamao.service.IGoodsService;
import com.stylefeng.guns.modular.huamao.service.ISktAreasService;

/**
 * 商品控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 16:16:23
 */
@Controller
@RequestMapping("/goods")
public class GoodsController extends BaseController {

    private String PREFIX = "/huamao/goods/";

    @Autowired
    private IGoodsService goodsService;

    @Autowired
    private ISktAreasService sktAreasService;
    @Autowired
    private IGoodsCatsService goodsCatsService;
    
    /**
     * 跳转到商品首页
     */
    
    @RequestMapping("")

//  @ResponseBody
    public String index() {
    	
       return PREFIX + "goods.html";
    }
    /**
     * 跳转到待审核商品首页
     */ 
    @RequestMapping("/goodsWarting")
    public String index2() {
        
    	return PREFIX + "goods_warting.html";
     }
    /**
     * 跳转到违规商品首页
     */ 
    @RequestMapping("/goodsUnqualified")
    public String index3() {
        
    	return PREFIX + "goods_unqualified.html";
     }
    /**
     * 跳转到推荐商品首页
     */ 
    @RequestMapping("/goodsRecom")
    public String index4() {
        
    	return PREFIX + "goods_Recom.html";
     }
    /**
     * 跳转到添加商品
     */
    @RequestMapping("/goods_add")
    public String goodsAdd() {
        return PREFIX + "goods_add.html";
    }

    /**
     * 跳转到修改商品
     */
    @RequestMapping("/goods_update/{goodsId}")
    public String goodsUpdate(@PathVariable Integer goodsId, Model model) {
        Goods goods = goodsService.selectById(goodsId);
        model.addAttribute("item",goods);
        LogObjectHolder.me().set(goods);
        return PREFIX + "goods_edit.html";
    }

    /**
     * 跳转到修改商品(待审核详情)
     */
    @RequestMapping("/goods_update1/{goodsId}")
    public String goodsUpdate1(@PathVariable Integer goodsId, Model model) {
        Goods goods = goodsService.selectById(goodsId);
        model.addAttribute("item",goods);
        LogObjectHolder.me().set(goods);
        return PREFIX + "goods_warting_edit.html";
    }
	/**
	 * 跳转到商品详情页面
	 */
	@RequestMapping("/showGoods/{goodsId}")
	public String showGoods(@PathVariable Integer goodsId, Model model) {
//		Goods goods = goodsService.selectById(goodsId);
//		model.addAttribute("item",goods);
//		LogObjectHolder.me().set(goods);
//		return PREFIX + "goods_warting_edit.html";
		return "redirect:http://rest.izzht.cn/view/detail.html?goodsId="+goodsId;
	}

    /**
     * 获取已上架商品列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(GoodsCustm goodsCustm ) { 
    	if(goodsCustm.getIsSale()==null ){
    		goodsCustm.setIsSale(1);
    	}
    	if(goodsCustm.getGoodsStatus()==null ){
    		goodsCustm.setGoodsStatus(1);
    	}
//    	if(goodsCustm.getGoodsName()!=null && !"".equals(goodsCustm.getGoodsName())){
//    		goodsCustm.setGoodsSn(goodsCustm.getGoodsName());
//    	}
    	Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().defaultPage();
    	List<Map<String, Object>> goodsCustemList = goodsService.selectByConditions(page,goodsCustm);
    	for(Map<String, Object> map:goodsCustemList ){
    		String goodsCatIdPath=(String) map.get("goodsCatIdPath");
    		String[]  catIdPathList=goodsCatIdPath.split("_");
    		String goodsCatNamePath = "";
    		for (int i = 0; i < catIdPathList.length; i++) {
    			GoodsCats goodsCats = goodsCatsService.selectById(catIdPathList[i]);
    			String catName = goodsCats.getCatName();
    			goodsCatNamePath=goodsCatNamePath+catName+"→";
			}
    		goodsCatNamePath=goodsCatNamePath.substring(0,goodsCatNamePath.lastIndexOf("→") );
    		map.put("goodsCatIdPath", goodsCatNamePath);
    		
    	}
    	page.setRecords(goodsCustemList);
 		return super.packForBT(page);
    }

    /**
     * 获取待审核商品列表
     */
    @RequestMapping(value = "/list2")
    @ResponseBody
    public Object list2(GoodsCustm goodsCustm ) { 
    	if(goodsCustm.getGoodsStatus()==null){
    		goodsCustm.setGoodsStatus(0);
    	}
    	if(goodsCustm.getGoodsName()!=null || !"".equals(goodsCustm.getGoodsName())){
    		goodsCustm.setGoodsSn(goodsCustm.getGoodsName());
    	}
    	Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().defaultPage();
    	 List<Map<String, Object>> goodsCustemList = goodsService.selectByConditions(page,goodsCustm);
     	for(Map<String, Object> map:goodsCustemList ){
     		String goodsCatIdPath=(String) map.get("goodsCatIdPath");
     		String[]  catIdPathList=goodsCatIdPath.split("_");
     		String goodsCatNamePath ="";
     		for (int i = 0; i < catIdPathList.length; i++) {
     			GoodsCats goodsCats = goodsCatsService.selectById(catIdPathList[i]);
     			String catName = goodsCats.getCatName();
     			goodsCatNamePath=goodsCatNamePath+catName+"→";
 			}
     		goodsCatNamePath=goodsCatNamePath.substring(0,goodsCatNamePath.lastIndexOf("→") );
     		map.put("goodsCatIdPath", goodsCatNamePath);
     		
     	}
     	 
     	page.setRecords(goodsCustemList);
 		return super.packForBT(page);
    }
    
    /**
     * 获取违规商品列表
     */
    @RequestMapping(value = "/list3")
    @ResponseBody
    public Object list3(GoodsCustm goodsCustm ) { 
    	if(goodsCustm.getGoodsStatus()==null){
    		goodsCustm.setGoodsStatus(-1);
    	}
    	if(goodsCustm.getGoodsName()!=null || !"".equals(goodsCustm.getGoodsName())){
    		goodsCustm.setGoodsSn(goodsCustm.getGoodsName());
    	}
    	Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().defaultPage();
    	List<Map<String, Object>> goodsCustemList = goodsService.selectByConditions(page,goodsCustm);
     	for(Map<String, Object> map:goodsCustemList ){
     		String goodsCatIdPath=(String) map.get("goodsCatIdPath");
     		String[]  catIdPathList=goodsCatIdPath.split("_");
     		String goodsCatNamePath = "";
     		for (int i = 0; i < catIdPathList.length; i++) {
     			GoodsCats goodsCats = goodsCatsService.selectById(catIdPathList[i]);
     			String catName = goodsCats.getCatName();
     			goodsCatNamePath=goodsCatNamePath+catName+"→";
 			}
     		goodsCatNamePath=goodsCatNamePath.substring(0,goodsCatNamePath.lastIndexOf("→") );
     		map.put("goodsCatIdPath", goodsCatNamePath);
     		
     	}
     	page.setRecords(goodsCustemList);
 		return super.packForBT(page);
    }
    
    /**
     * 
     */
    @RequestMapping(value = "/selectGoodsByGoodsCatId")
    @ResponseBody
    public Object selectGoodsByGoodsCatId(@RequestParam(value ="id") Integer id ) { 
    	
    	 JSONArray jSONArray=new JSONArray();
    	 List<Goods> goodsList = goodsService.selectGoodsByGoodsCatId(id);
     	for (Goods good : goodsList) {
     		JSONObject json=new JSONObject();
     		json.put("goodsId", good.getGoodsId());
     		json.put("goodsName", good.getGoodsName());
     		jSONArray.add(json);	
 		}
     	return jSONArray;
    	 
    	 
    }
    
    
    /**
     * 新增商品
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Goods goods) {
        goodsService.insert(goods);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer goodsId) {
        goodsService.deleteById(goodsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Goods goods) {
 
        goodsService.updateById(goods);
        return SUCCESS_TIP;
    }
    
    /**
     * 批量修改商品
     */
    @RequestMapping(value = "/batchUpdate")
    @ResponseBody
    public Object batchUpdate(
    		@RequestParam(value="to_left[]", required = false ) List<Integer> to_left,
    		@RequestParam(value="to_right[]",required = false) List<Integer> to_right,
    		@RequestParam(value="style") String style
    		) {
    	List<Integer> list1= to_left;
    	List<Integer> list2=  to_right; 
//    	List<Integer> list1 = goodsCustm.getTo_left();
//    	List<Integer> list2 = goodsCustm.getTo_right();
//
//    	Integer style=goodsCustm.getStyle();
    	List<Goods> goodsCustemlist1=new ArrayList<>();

    	if(list1!=null && list1.size()>0){
    		for (int i = 0; i <list1.size(); i++) {
    			Goods goods = goodsService.selectById(list1.get(i));
				  if("1".equals(style)){
					  goods.setIsBest(0);  
				  }
				  if("2".equals(style)){
					  goods.setIsHot(0);  
				  }
				  if("3".equals(style)){
					  goods.setIsNew(0);  
				  }
				  if("4".equals(style)){
					  goods.setIsRecom(0); 
				  }
				  goodsService.updateById(goods);
    			//  goodsCustemlist1.add(goods);  
    			  
			}
    		//goodsService.batchUpdatePo(goodsCustemlist1);
    	}
    	if(list2!=null &&list2.size()>0){
    		for (int i = 0; i <list2.size() ; i++) {
    			  Goods goods = goodsService.selectById(list2.get(i));
    			  if("1".equals(style)){
    				  goods.setIsBest(1);  
    			  }
    			  if("2".equals(style)){
    				  goods.setIsHot(1);  
    			  }
    			  if("3".equals(style)){
    				  goods.setIsNew(1);  
    			  }
    			  if("4".equals(style)){
    				  goods.setIsRecom(1); 
    			  }
    			  goodsService.updateById(goods);
    			//  goodsCustemlist1.addAll(goodsCustemlist1);  
			}
    		
    	}
    //	goodsService.batchUpdatePo(goodsCustemlist1);
        return SUCCESS_TIP;
    }

    
    /**
     * 商品详情
     */
    @RequestMapping(value = "/detail/{goodsId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsId") Integer goodsId) {
        return goodsService.selectById(goodsId);
    }
    
    /********************首页***************************/
    
    /**
     * 查看今日推荐
     */
    @RequestMapping(value = "/todayHotSale")
    @ResponseBody
    public Object TodayHotSale() {
    	List<Map<String,Object>> list = goodsService.TodayHotSale();
        return list;
    }
    /**
     * 查看今日推荐品牌列表商品
     */
    @RequestMapping(value = "/selectTodayHotSaleAll/{brandId}")
    @ResponseBody
    public Object selectTodayHotSaleAll(@PathVariable("brandId") Integer brandId) {
    	List<Map<String,Object>> list = goodsService.selectTodayHotSaleAll(brandId);
        return list;
    }
    
    
}
