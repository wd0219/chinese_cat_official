package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgent;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgentList;

/**
 * <p>
 * 代理公司开元宝流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface ILogKaiyuanAgentService extends IService<LogKaiyuanAgent> {

	// 代理公司划拨啊流水表展示
	public List<LogKaiyuanAgentList> selectLogKaiyuanAgentAll(Page<LogKaiyuanAgentList> page, LogKaiyuanAgentList logKaiyuanAgentList);

}
