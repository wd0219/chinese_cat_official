package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersBankcardsMapper;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.model.UsersBankcardsList;
import com.stylefeng.guns.modular.huamao.service.IUsersBankcardsService;

/**
 * <p>
 * 用户的银行卡表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
@Service
public class UsersBankcardsServiceImpl extends ServiceImpl<UsersBankcardsMapper, UsersBankcards>
		implements IUsersBankcardsService {

	@Autowired
	private UsersBankcardsMapper ubm;

	// 用户的银行卡表展示
	@Override
	public List<UsersBankcardsList> selectUsersBankcardsAll(@Param("page")Page<UsersBankcardsList> page, UsersBankcardsList usersBankcardsList) {
		Integer total = ubm.selectUsersBankcardsAllCount(usersBankcardsList);
		page.setTotal(total);
		return ubm.selectUsersBankcardsAll(page,usersBankcardsList);
	}

	// 通过id进行逻辑删除
	@Override
	public void deleteById2(Integer usersBankcardsId) {
		ubm.deleteById2(usersBankcardsId);
	}

	@Override
	public List<UsersBankcards> selectUserBranks(UsersBankcardsList usersBankcardsList) {
		List<UsersBankcards> list = ubm.selectUserBranks(usersBankcardsList);
		return list;
	}

}
