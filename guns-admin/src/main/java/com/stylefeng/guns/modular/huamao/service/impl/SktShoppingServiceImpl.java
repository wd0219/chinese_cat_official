package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktShopping;
import com.stylefeng.guns.modular.huamao.dao.SktShoppingMapper;
import com.stylefeng.guns.modular.huamao.model.SktShoppingDTO;
import com.stylefeng.guns.modular.huamao.service.ISktShoppingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 购物券表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-09-29
 */
@Service
public class SktShoppingServiceImpl extends ServiceImpl<SktShoppingMapper, SktShopping> implements ISktShoppingService {
    @Autowired
    private SktShoppingMapper sktShoppingMapper;
    @Override
    public List<SktShoppingDTO> selectAll(Page<SktShoppingDTO> page, SktShoppingDTO sktShoppingDTO) {
        Integer total = sktShoppingMapper.selectAllCount(sktShoppingDTO);
        page.setTotal(total);
        List<SktShoppingDTO> list = sktShoppingMapper.selectAll(page,sktShoppingDTO);
        return list;
    }

}
