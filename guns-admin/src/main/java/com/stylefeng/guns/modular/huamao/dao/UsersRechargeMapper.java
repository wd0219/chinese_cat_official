package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;
import com.stylefeng.guns.modular.huamao.model.UsersRechargeDTO;

/**
 * <p>
 * 用户充值表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
public interface UsersRechargeMapper extends BaseMapper<UsersRecharge> {
	/**
	 * 显示用户订单
	 * @param page 
	 * @param usersRechargeDTO
	 * @return
	 */
	public List<UsersRechargeDTO> showUserDengDanInfo(@Param("page")Page<UsersRechargeDTO> page,
			@Param("usersRechargeDTO")UsersRechargeDTO usersRechargeDTO);
	
	public Integer showUserDengDanInfoCount(@Param("usersRechargeDTO")UsersRechargeDTO usersRechargeDTO);
}
