package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;

/**
 * <p>
 * 工单表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
public interface SktWorkorderMapper extends BaseMapper<SktWorkorder> {
	
	public List<SktWorkorder> sktWorkorderfindAll(@Param("page")Page<SktWorkorder> page, 
			@Param("sktWorkorder")SktWorkorder sktWorkorder);
	public Integer sktWorkorderfindAllCount(@Param("sktWorkorder")SktWorkorder sktWorkorder);
	
	public List<SktWorkorder> sktWorkorderfindup(SktWorkorder sktWorkorder);

}
