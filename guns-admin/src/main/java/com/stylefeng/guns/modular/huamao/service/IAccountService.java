package com.stylefeng.guns.modular.huamao.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.*;

/**
 * <p>
 * 会员账户表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-11
 */
/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
public interface IAccountService extends IService<Account> {

	// 展示用户账户列表
	public List<AccountList> selectAccountAll(Page<AccountList> page, AccountList accountList);
	
	/*
	 * 根据用户id查询账户
	 */
	public Account selectAccountByuserId(Integer userId);
	
	/** 用户点击升级
	 * @param userId   用户id
	 
	 * @return
	 */
	public  Object getUseUpDate(Integer userId);
	/**
	 * 修改现金账户
	 * @param accountList
	 * @return
	 */
	public  boolean updateByUp(AccountList accountList,BigDecimal changeCash );
	/**
	 * 取消用户升级
	 * @param usersUpgrade 用户升级订单表
	 * @return
	 */
	public boolean resetUPGrade(UsersUpgrade usersUpgrade);
	
	public boolean userSale(int userId,BigDecimal num,String saleType);

	//用户升级以后操作
	public String upAfterAdd(Integer orderId);

	public boolean insertLogCash(Integer cashType,Integer fromId,Integer userId ,String firstNum,BigDecimal preCash,String remark,Integer type,BigDecimal cash);

	public boolean insertLogKaiyuan(Integer fromId,BigDecimal kaiyuan,Integer kaiyuanType,String firstNum,BigDecimal preKaiyuan,String remark,Integer type,Integer userId);

	public Integer insertLogKaiYuanTurnover(Integer type,Integer fromId,Integer userId,String firstNum,BigDecimal preKaiyuan,Integer kaiyuanType,BigDecimal kaiyuan,String remark);

	public String doUpAgent(Integer shaId,Integer staffId);

	public boolean insertLogScore(int fromId,int type, String firstNum,BigDecimal preScore,String remark,BigDecimal score,int scoreType,int userId);

	public boolean insertLogScoreFreeze(int fromId, int type,String firstNum,BigDecimal preScore,String remark,BigDecimal score,int scoreType,int userId);

	public void fenRun(Integer agentId,Integer shaId);

	public boolean applysBuyplaqueTrue(Integer orderId);

	public  boolean applysBuyplaqueFalse(Integer orderId);

	public boolean resetAgentUPGrade(AgentsApplys agentsApplys);

	public  boolean applyRedeemFalse(UsersDraws usersDraws);
	public  boolean enterOrderRefunds(SktOrderRefunds sktOrderRefunds);
	public Integer insertScoreAgent(Integer type,Integer agentId,String orderNo,BigDecimal preScore,Integer scoreType,BigDecimal score,String remark);
	public Map<String,Object> getSevenWorkDay();
	public List<AgentsApplys> getAgentsApplysSevenAgo(Integer userId);

}
