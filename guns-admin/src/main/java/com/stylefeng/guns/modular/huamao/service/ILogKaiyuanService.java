package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanList;

/**
 * <p>
 * 开元宝流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface ILogKaiyuanService extends IService<LogKaiyuan> {

	// 开元宝流水返回
	public List<LogKaiyuanList> selectLogKaiyuanAll(Page<LogKaiyuanList> page, LogKaiyuanList logKaiyuanList);

}
