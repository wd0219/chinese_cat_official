package com.stylefeng.guns.modular.system.service;

import com.stylefeng.guns.modular.huamao.model.SktShopCats;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 店铺分类表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-29
 */
public interface ISktShopCatsService extends IService<SktShopCats> {

}
