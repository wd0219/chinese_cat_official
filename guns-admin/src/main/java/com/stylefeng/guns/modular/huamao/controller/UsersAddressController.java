package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.model.UsersAddressDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersAddressService;

/**
 * 用户地址控制器
 *
 * @author fengshuonan
 * @Date 2018-04-13 10:40:20
 */
@Controller
@RequestMapping("/usersAddress")
public class UsersAddressController extends BaseController {

    private String PREFIX = "/huamao/usersAddress/";

    @Autowired
    private IUsersAddressService usersAddressService;

    /**
     * 跳转到用户地址首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "usersAddress.html";
    }

    /**
     * 跳转到添加用户地址
     */
    @RequestMapping("/usersAddress_add")
    public String usersAddressAdd() {
        return PREFIX + "usersAddress_add.html";
    }

    /**
     * 跳转到修改用户地址
     */
    @RequestMapping("/usersAddress_update/{usersAddressId}")
    public String usersAddressUpdate(@PathVariable Integer usersAddressId, Model model) {
        UsersAddress usersAddress = usersAddressService.selectById(usersAddressId);
        model.addAttribute("item",usersAddress);
        LogObjectHolder.me().set(usersAddress);
        return PREFIX + "usersAddress_edit.html";
    }

    /**
     * 获取用户地址列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(UsersAddressDTO usersAddressDTO) {
    	Page<UsersAddressDTO> page = new PageFactory<UsersAddressDTO>().defaultPage();
    	List<UsersAddressDTO> list = usersAddressService.showUserAddressInfo(page,usersAddressDTO);
    	page.setRecords(list);
        return super.packForBT(page);
    }

    /**
     * 新增用户地址
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(UsersAddress usersAddress) {
        usersAddressService.insert(usersAddress);
        return SUCCESS_TIP;
    }

    /**
     * 删除用户地址
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer usersAddressId) {
        usersAddressService.deleteById(usersAddressId);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户地址
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(UsersAddress usersAddress) {
        usersAddressService.updateById(usersAddress);
        return SUCCESS_TIP;
    }

    /**
     * 用户地址详情
     */
    @RequestMapping(value = "/detail/{usersAddressId}")
    @ResponseBody
    public Object detail(@PathVariable("usersAddressId") Integer usersAddressId) {
        return usersAddressService.selectById(usersAddressId);
    }
}
