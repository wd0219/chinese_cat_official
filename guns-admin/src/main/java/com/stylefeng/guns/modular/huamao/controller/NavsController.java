package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.Navs;
import com.stylefeng.guns.modular.huamao.service.INavsService;

/**
 * 导航管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 14:28:17
 */

@Controller
@RequestMapping("/navs")
public class NavsController extends BaseController {

	private String PREFIX = "/huamao/navs/";

	@Autowired
	private INavsService navsService;

	/**
	 * 跳转到导航管理首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "navs.html";
	}

	/**
	 * 跳转到添加导航管理
	 */
	@RequestMapping("/navs_add")
	public String navsAdd() {
		return PREFIX + "navs_add.html";
	}

	/**
	 * 跳转到修改导航管理
	 */
	@RequestMapping("/navs_update/{navsId}")
	public String navsUpdate(@PathVariable Integer navsId, Model model) {
		Navs navs = navsService.selectById(navsId);
		model.addAttribute("item", navs);
		LogObjectHolder.me().set(navs);
		return PREFIX + "navs_edit.html";
	}

	/**
	 * 获取导航管理列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(String condition) {
		Page<Navs> page = new PageFactory<Navs>().defaultPage();
		EntityWrapper wrapper = new EntityWrapper();
		wrapper.orderBy("createTime",false);
		Page<Navs> navsPage = navsService.selectPage(page, wrapper);
		int i = navsService.selectCount(null);
		navsPage.setTotal(i);
		return super.packForBT(page);
	}

	/**
	 * 新增导航管理
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(Navs navs) {
		navsService.insert(navs);
		return SUCCESS_TIP;
	}

	/**
	 * 删除导航管理
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer navsId) {
		navsService.deleteById(navsId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改导航管理
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(Navs navs) {
		navsService.updateById(navs);
		return SUCCESS_TIP;
	}

	/**
	 * 导航管理详情
	 */
	@RequestMapping(value = "/detail/{navsId}")
	@ResponseBody
	public Object detail(@PathVariable("navsId") Integer navsId) {
		return navsService.selectById(navsId);
	}
}
