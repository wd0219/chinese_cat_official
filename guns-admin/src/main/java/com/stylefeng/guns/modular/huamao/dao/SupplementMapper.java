package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SupplementList;

/**
 * 手动补单 Mapper 接口
 * 
 * @author gxz
 *
 */
public interface SupplementMapper extends BaseMapper<SupplementList> {

	// 校验用户名或手机号是否存在
	SupplementList checkUser(SupplementList supplementList);

	// 添加记录
	Boolean add(SupplementList supplementList);

}
