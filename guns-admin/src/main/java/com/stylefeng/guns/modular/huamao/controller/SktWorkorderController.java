package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;
import com.stylefeng.guns.modular.huamao.service.ISktWorkorderService;

/**
 * 工单列表控制器
 *
 * @author fengshuonan
 * @Date 2018-04-22 23:20:46
 */
@Controller
@RequestMapping("/sktWorkorder")
public class SktWorkorderController extends BaseController {

    private String PREFIX = "/huamao/sktWorkorder/";

    @Autowired
    private ISktWorkorderService sktWorkorderService;

    /**
     * 跳转到工单列表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktWorkorder.html";
    }

    /**
     * 跳转到添加工单列表
     */
    @RequestMapping("/sktWorkorder_add")
    public String sktWorkorderAdd() {
        return PREFIX + "sktWorkorder_add.html";
    }

    /**
     * 跳转到修改工单列表
     */
    @RequestMapping("/sktWorkorder_update/{sktWorkorderId}")
    public String sktWorkorderUpdate(@PathVariable Integer sktWorkorderId, Model model) {
    	
        SktWorkorder sktWorkorder = sktWorkorderService.selectById(sktWorkorderId);
        String[] split = sktWorkorder.getImgs().split(",");
        if(split.length > 0){
        	sktWorkorder.setImgs1(split[0]);
        	sktWorkorder.setImgs2(split[1]);
        }
        model.addAttribute("item",sktWorkorder);
        LogObjectHolder.me().set(sktWorkorder);
        return PREFIX + "sktWorkorder_edit.html";
    }

    /**
     * 获取工单列表列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktWorkorder sktWorkorder) {
    	Page<SktWorkorder> page = new PageFactory<SktWorkorder>().defaultPage();
        List<SktWorkorder> list = sktWorkorderService.sktWorkorderfindAll(page,sktWorkorder);
        page.setRecords(list);
		return super.packForBT(page);
    }

    /**
     * 新增工单列表
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktWorkorder sktWorkorder) {
        sktWorkorderService.insert(sktWorkorder);
        return SUCCESS_TIP;
    }

    /**
     * 删除工单列表
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktWorkorderId) {
        sktWorkorderService.deleteById(sktWorkorderId);
        return SUCCESS_TIP;
    }

    /**
     * 修改工单列表
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktWorkorder sktWorkorder) {
        sktWorkorderService.updateById(sktWorkorder);
        return SUCCESS_TIP;
    }

    /**
     * 工单列表详情
     */
    @RequestMapping(value = "/detail/{sktWorkorderId}")
    public Object detail(@PathVariable("sktWorkorderId") Integer userId,Model model,SktWorkorder sktWorkorder) {
    	sktWorkorder.setUserId(userId);
    	List<SktWorkorder> list = sktWorkorderService.sktWorkorderfindup(sktWorkorder);
    	SktWorkorder workorder = new SktWorkorder();
    	if(list.size()>0){
    		workorder = list.get(0);
    		String[] str = workorder.getImgs().split(","); 
    		String allUrl = UrlUtils.getAllUrl(str[0]);
    		workorder.setImgs1(allUrl);
    		String allUrl2 = UrlUtils.getAllUrl(str[1]);
    		workorder.setImgs2(allUrl2);
    	}
    	model.addAttribute("item", workorder);
    	return PREFIX +"sktWorkorder_edit.html";
    }
    
    /**
     * 工单列表详情2
     */
    @RequestMapping(value = "/detail2/{sktWorkorderId}")
    public Object detail2(@PathVariable("sktWorkorderId") Integer userId,Model model,SktWorkorder sktWorkorder) {
    	sktWorkorder.setUserId(userId);
    	List<SktWorkorder> list = sktWorkorderService.sktWorkorderfindup(sktWorkorder);
    	model.addAttribute("item", list.get(0));
    	return PREFIX +"sktWorkorder_edit2.html";
    }
}
