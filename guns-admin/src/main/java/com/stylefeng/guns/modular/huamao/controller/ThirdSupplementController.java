package com.stylefeng.guns.modular.huamao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.ThirdSupplementList;
import com.stylefeng.guns.modular.huamao.service.IThirdSupplementService;

/**
 * 第三方补单控制层
 * 
 * @author gxz
 *
 */
@Controller
@RequestMapping("/thirdSupplement")
public class ThirdSupplementController extends BaseController {
	private String PREFIX = "/huamao/thirdSupplement/";

	@Autowired
	private IThirdSupplementService thirdSupplementService;

	/**
	 * 跳转到手动补单记录首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "thirdSupplement.html";
	}

	/**
	 * 获取第三方支付补单记录列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(ThirdSupplementList thirdSupplementList) {
		return thirdSupplementService.selectThirdSupplementAll(thirdSupplementList);
		// return logThirdpayService.selectList(null);
	}

	//第三方补单supplement
	@RequestMapping("supplement")
	public String supplement() {
		return PREFIX + "thirdSupplement_supplement.html";
	}
}
