package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktFavorites;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 关注(商品/店铺)表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
public interface ISktFavoritesService extends IService<SktFavorites> {

}
