package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreAgentMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgent;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgentList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreAgentService;

/**
 * <p>
 * 代理公司积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogScoreAgentServiceImpl extends ServiceImpl<LogScoreAgentMapper, LogScoreAgent>
		implements ILogScoreAgentService {

	@Autowired
	private LogScoreAgentMapper lam;

	// 代理公司积分流水表返回
	@Override
	public List<LogScoreAgentList> selectLogScoreAgentAll(Page<LogScoreAgentList> page, LogScoreAgentList logScoreAgentList) {
		Integer total = lam.selectLogScoreAgentAllCount(logScoreAgentList);
		page.setTotal(total);
		return lam.selectLogScoreAgentAll(page,logScoreAgentList);
	}

}
