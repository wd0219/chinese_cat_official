package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 每日总对账数据表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-14
 */
@TableName("skt_check_result")
public class CheckResult extends Model<CheckResult> {

    private static final long serialVersionUID = 1L;

    /**
     * 账户明细ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 日期
     */
    @TableField("check_date")
    private Integer checkDate;
    /**
     * 资金对账差异
     */
    @TableField("zj_checkDiff")
    private BigDecimal zjCheckdiff;
    /**
     * 积分对账差异
     */
    @TableField("jf_checkDiff")
    private BigDecimal jfCheckdiff;
    /**
     * 开元宝对账差异
     */
    @TableField("kyb_checkDiff")
    private BigDecimal kybCheckdiff;
    /**
     * 昨日资金
     */
    @TableField("zj_yesterday")
    private BigDecimal zjYesterday;
    /**
     * 今日资金
     */
    @TableField("zj_today")
    private BigDecimal zjToday;
    /**
     * 今日业务增量
     */
    @TableField("zj_change")
    private BigDecimal zjChange;
    /**
     * 门店订单支付金额
     */
    @TableField("zj_storePay")
    private BigDecimal zjStorepay;
    /**
     * 商城订单支付金额
     */
    @TableField("zj_mallPay")
    private BigDecimal zjMallpay;
    /**
     * 提现申请
     */
    @TableField("zj_tixianApply")
    private BigDecimal zjTixianapply;
    /**
     * 提现手续费
     */
    @TableField("zj_tixianFee")
    private BigDecimal zjTixianfee;
    /**
     * 提现不通过
     */
    @TableField("zj_tixianNoPass")
    private BigDecimal zjTixiannopass;
    /**
     * 退款
     */
    @TableField("zj_tuikuan")
    private BigDecimal zjTuikuan;
    @TableField("zj_chongzhi")
    private BigDecimal zjChongzhi;
    /**
     * 昨日积分
     */
    @TableField("jf_yesterday")
    private BigDecimal jfYesterday;
    /**
     * 今日积分
     */
    @TableField("jf_today")
    private BigDecimal jfToday;
    /**
     * 今日业务增量
     */
    @TableField("jf_change")
    private BigDecimal jfChange;
    /**
     * 积分转换
     */
    @TableField("jf_zhuanHuan")
    private BigDecimal jfZhuanhuan;
    /**
     * 门店消费获得
     */
    @TableField("jf_storePay")
    private BigDecimal jfStorepay;
    /**
     * 商城消费获得
     */
    @TableField("jf_mallPay")
    private BigDecimal jfMallpay;
    /**
     * 邀请奖励
     */
    @TableField("jf_inviteReward")
    private BigDecimal jfInvitereward;
    /**
     * 其它
     */
    @TableField("jf_other")
    private BigDecimal jfOther;
    /**
     * 昨日开元宝
     */
    @TableField("kyb_yesterday")
    private BigDecimal kybYesterday;
    /**
     * 今日开元宝
     */
    @TableField("kyb_today")
    private BigDecimal kybToday;
    /**
     * 今日业务增量
     */
    @TableField("kyb_change")
    private BigDecimal kybChange;
    /**
     * 积分转换
     */
    @TableField("kyb_zhuanHuan")
    private BigDecimal kybZhuanhuan;
    /**
     * 门店支付
     */
    @TableField("kyb_storePay")
    private BigDecimal kybStorepay;
    /**
     * 门店支付手续费
     */
    @TableField("kyb_storePayFee")
    private BigDecimal kybStorepayfee;
    /**
     * 商城支付
     */
    @TableField("kyb_mallPay")
    private BigDecimal kybMallpay;
    /**
     * 商城支付手续费
     */
    @TableField("kyb_mallPayFee")
    private BigDecimal kybMallpayfee;
    /**
     * 提现
     */
    @TableField("kyb_tixianApply")
    private BigDecimal kybTixianapply;
    /**
     * 提现手续费
     */
    @TableField("kyb_tixianFee")
    private BigDecimal kybTixianfee;
    /**
     * 提现不通过
     */
    @TableField("kyb_tixianNoPass")
    private BigDecimal kybTixiannopass;
    /**
     * 开元宝退款
     */
    @TableField("kyb_tuikuan")
    private BigDecimal kybTuikuan;
    /**
     * 其它
     */
    @TableField("kyb_other")
    private BigDecimal kybOther;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Integer checkDate) {
        this.checkDate = checkDate;
    }

    public BigDecimal getZjCheckdiff() {
        return zjCheckdiff;
    }

    public void setZjCheckdiff(BigDecimal zjCheckdiff) {
        this.zjCheckdiff = zjCheckdiff;
    }

    public BigDecimal getJfCheckdiff() {
        return jfCheckdiff;
    }

    public void setJfCheckdiff(BigDecimal jfCheckdiff) {
        this.jfCheckdiff = jfCheckdiff;
    }

    public BigDecimal getKybCheckdiff() {
        return kybCheckdiff;
    }

    public void setKybCheckdiff(BigDecimal kybCheckdiff) {
        this.kybCheckdiff = kybCheckdiff;
    }

    public BigDecimal getZjYesterday() {
        return zjYesterday;
    }

    public void setZjYesterday(BigDecimal zjYesterday) {
        this.zjYesterday = zjYesterday;
    }

    public BigDecimal getZjToday() {
        return zjToday;
    }

    public void setZjToday(BigDecimal zjToday) {
        this.zjToday = zjToday;
    }

    public BigDecimal getZjChange() {
        return zjChange;
    }

    public void setZjChange(BigDecimal zjChange) {
        this.zjChange = zjChange;
    }

    public BigDecimal getZjStorepay() {
        return zjStorepay;
    }

    public void setZjStorepay(BigDecimal zjStorepay) {
        this.zjStorepay = zjStorepay;
    }

    public BigDecimal getZjMallpay() {
        return zjMallpay;
    }

    public void setZjMallpay(BigDecimal zjMallpay) {
        this.zjMallpay = zjMallpay;
    }

    public BigDecimal getZjTixianapply() {
        return zjTixianapply;
    }

    public void setZjTixianapply(BigDecimal zjTixianapply) {
        this.zjTixianapply = zjTixianapply;
    }

    public BigDecimal getZjTixianfee() {
        return zjTixianfee;
    }

    public void setZjTixianfee(BigDecimal zjTixianfee) {
        this.zjTixianfee = zjTixianfee;
    }

    public BigDecimal getZjTixiannopass() {
        return zjTixiannopass;
    }

    public void setZjTixiannopass(BigDecimal zjTixiannopass) {
        this.zjTixiannopass = zjTixiannopass;
    }

    public BigDecimal getZjTuikuan() {
        return zjTuikuan;
    }

    public void setZjTuikuan(BigDecimal zjTuikuan) {
        this.zjTuikuan = zjTuikuan;
    }

    public BigDecimal getZjChongzhi() {
        return zjChongzhi;
    }

    public void setZjChongzhi(BigDecimal zjChongzhi) {
        this.zjChongzhi = zjChongzhi;
    }

    public BigDecimal getJfYesterday() {
        return jfYesterday;
    }

    public void setJfYesterday(BigDecimal jfYesterday) {
        this.jfYesterday = jfYesterday;
    }

    public BigDecimal getJfToday() {
        return jfToday;
    }

    public void setJfToday(BigDecimal jfToday) {
        this.jfToday = jfToday;
    }

    public BigDecimal getJfChange() {
        return jfChange;
    }

    public void setJfChange(BigDecimal jfChange) {
        this.jfChange = jfChange;
    }

    public BigDecimal getJfZhuanhuan() {
        return jfZhuanhuan;
    }

    public void setJfZhuanhuan(BigDecimal jfZhuanhuan) {
        this.jfZhuanhuan = jfZhuanhuan;
    }

    public BigDecimal getJfStorepay() {
        return jfStorepay;
    }

    public void setJfStorepay(BigDecimal jfStorepay) {
        this.jfStorepay = jfStorepay;
    }

    public BigDecimal getJfMallpay() {
        return jfMallpay;
    }

    public void setJfMallpay(BigDecimal jfMallpay) {
        this.jfMallpay = jfMallpay;
    }

    public BigDecimal getJfInvitereward() {
        return jfInvitereward;
    }

    public void setJfInvitereward(BigDecimal jfInvitereward) {
        this.jfInvitereward = jfInvitereward;
    }

    public BigDecimal getJfOther() {
        return jfOther;
    }

    public void setJfOther(BigDecimal jfOther) {
        this.jfOther = jfOther;
    }

    public BigDecimal getKybYesterday() {
        return kybYesterday;
    }

    public void setKybYesterday(BigDecimal kybYesterday) {
        this.kybYesterday = kybYesterday;
    }

    public BigDecimal getKybToday() {
        return kybToday;
    }

    public void setKybToday(BigDecimal kybToday) {
        this.kybToday = kybToday;
    }

    public BigDecimal getKybChange() {
        return kybChange;
    }

    public void setKybChange(BigDecimal kybChange) {
        this.kybChange = kybChange;
    }

    public BigDecimal getKybZhuanhuan() {
        return kybZhuanhuan;
    }

    public void setKybZhuanhuan(BigDecimal kybZhuanhuan) {
        this.kybZhuanhuan = kybZhuanhuan;
    }

    public BigDecimal getKybStorepay() {
        return kybStorepay;
    }

    public void setKybStorepay(BigDecimal kybStorepay) {
        this.kybStorepay = kybStorepay;
    }

    public BigDecimal getKybStorepayfee() {
        return kybStorepayfee;
    }

    public void setKybStorepayfee(BigDecimal kybStorepayfee) {
        this.kybStorepayfee = kybStorepayfee;
    }

    public BigDecimal getKybMallpay() {
        return kybMallpay;
    }

    public void setKybMallpay(BigDecimal kybMallpay) {
        this.kybMallpay = kybMallpay;
    }

    public BigDecimal getKybMallpayfee() {
        return kybMallpayfee;
    }

    public void setKybMallpayfee(BigDecimal kybMallpayfee) {
        this.kybMallpayfee = kybMallpayfee;
    }

    public BigDecimal getKybTixianapply() {
        return kybTixianapply;
    }

    public void setKybTixianapply(BigDecimal kybTixianapply) {
        this.kybTixianapply = kybTixianapply;
    }

    public BigDecimal getKybTixianfee() {
        return kybTixianfee;
    }

    public void setKybTixianfee(BigDecimal kybTixianfee) {
        this.kybTixianfee = kybTixianfee;
    }

    public BigDecimal getKybTixiannopass() {
        return kybTixiannopass;
    }

    public void setKybTixiannopass(BigDecimal kybTixiannopass) {
        this.kybTixiannopass = kybTixiannopass;
    }

    public BigDecimal getKybTuikuan() {
        return kybTuikuan;
    }

    public void setKybTuikuan(BigDecimal kybTuikuan) {
        this.kybTuikuan = kybTuikuan;
    }

    public BigDecimal getKybOther() {
        return kybOther;
    }

    public void setKybOther(BigDecimal kybOther) {
        this.kybOther = kybOther;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CheckResult{" +
        "id=" + id +
        ", checkDate=" + checkDate +
        ", zjCheckdiff=" + zjCheckdiff +
        ", jfCheckdiff=" + jfCheckdiff +
        ", kybCheckdiff=" + kybCheckdiff +
        ", zjYesterday=" + zjYesterday +
        ", zjToday=" + zjToday +
        ", zjChange=" + zjChange +
        ", zjStorepay=" + zjStorepay +
        ", zjMallpay=" + zjMallpay +
        ", zjTixianapply=" + zjTixianapply +
        ", zjTixianfee=" + zjTixianfee +
        ", zjTixiannopass=" + zjTixiannopass +
        ", zjTuikuan=" + zjTuikuan +
        ", zjChongzhi=" + zjChongzhi +
        ", jfYesterday=" + jfYesterday +
        ", jfToday=" + jfToday +
        ", jfChange=" + jfChange +
        ", jfZhuanhuan=" + jfZhuanhuan +
        ", jfStorepay=" + jfStorepay +
        ", jfMallpay=" + jfMallpay +
        ", jfInvitereward=" + jfInvitereward +
        ", jfOther=" + jfOther +
        ", kybYesterday=" + kybYesterday +
        ", kybToday=" + kybToday +
        ", kybChange=" + kybChange +
        ", kybZhuanhuan=" + kybZhuanhuan +
        ", kybStorepay=" + kybStorepay +
        ", kybStorepayfee=" + kybStorepayfee +
        ", kybMallpay=" + kybMallpay +
        ", kybMallpayfee=" + kybMallpayfee +
        ", kybTixianapply=" + kybTixianapply +
        ", kybTixianfee=" + kybTixianfee +
        ", kybTixiannopass=" + kybTixiannopass +
        ", kybTuikuan=" + kybTuikuan +
        ", kybOther=" + kybOther +
        "}";
    }
}
