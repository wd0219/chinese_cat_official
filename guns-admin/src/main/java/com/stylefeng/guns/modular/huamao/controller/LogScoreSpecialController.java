package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecial;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecialList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreSpecialService;

import java.util.List;

/**
 * 待发特别积分记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-17 11:21:01
 */
@Controller
@RequestMapping("/logScoreSpecial")
public class LogScoreSpecialController extends BaseController {

	private String PREFIX = "/huamao/logScoreSpecial/";

	@Autowired
	private ILogScoreSpecialService logScoreSpecialService;

	/**
	 * 跳转到待发特别积分记录首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, Model model) {
		// 参数是为了在用户点击特别奖励查看特别奖励详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logScoreSpecial.html";
	}

	/**
	 * 跳转到添加待发特别积分记录
	 */
	@RequestMapping("/logScoreSpecial_add")
	public String logScoreSpecialAdd() {
		return PREFIX + "logScoreSpecial_add.html";
	}

	/**
	 * 跳转到修改待发特别积分记录
	 */
	@RequestMapping("/logScoreSpecial_update/{logScoreSpecialId}")
	public String logScoreSpecialUpdate(@PathVariable Integer logScoreSpecialId, Model model) {
		LogScoreSpecial logScoreSpecial = logScoreSpecialService.selectById(logScoreSpecialId);
		model.addAttribute("item", logScoreSpecial);
		LogObjectHolder.me().set(logScoreSpecial);
		return PREFIX + "logScoreSpecial_edit.html";
	}

	/**
	 * 获取待发特别积分记录列表：{userPhone2}用户查看特别奖励详情传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone,
			LogScoreSpecialList logScoreSpecialList) {
		// 判断如果不是‘手机号’并且logScoreSpecialList.getUserPhone()不是null说明要看用户积分详情，是‘手机号’并且logScoreSpecialList.getUserPhone()是''或是有数据说明是普通的查询所有的现金详情
		if (!phone.equals("手机号") && logScoreSpecialList.getUserPhone() == null) {
			logScoreSpecialList.setUserPhone(phone);
		}
		Page<LogScoreSpecialList> page = new PageFactory<LogScoreSpecialList>().defaultPage();
		List<LogScoreSpecialList> logScoreSpecialLists = logScoreSpecialService.selectLogScoreSpecialAll(page,logScoreSpecialList);
		page.setRecords(logScoreSpecialLists);
		return super.packForBT(page);
		// return logScoreSpecialService.selectList(null);
	}

	/**
	 * 新增待发特别积分记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogScoreSpecial logScoreSpecial) {
		logScoreSpecialService.insert(logScoreSpecial);
		return SUCCESS_TIP;
	}

	/**
	 * 删除待发特别积分记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logScoreSpecialId) {
		logScoreSpecialService.deleteById(logScoreSpecialId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改待发特别积分记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogScoreSpecial logScoreSpecial) {
		logScoreSpecialService.updateById(logScoreSpecial);
		return SUCCESS_TIP;
	}

	/**
	 * 待发特别积分记录详情
	 */
	@RequestMapping(value = "/detail/{logScoreSpecialId}")
	@ResponseBody
	public Object detail(@PathVariable("logScoreSpecialId") Integer logScoreSpecialId) {
		return logScoreSpecialService.selectById(logScoreSpecialId);
	}
}
