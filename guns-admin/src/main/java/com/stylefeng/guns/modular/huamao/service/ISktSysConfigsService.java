package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktSysConfigs;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商城配置表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-26
 */
public interface ISktSysConfigsService extends IService<SktSysConfigs> {
	/**
	 * 查询基础设置
	 * @return
	 */
	public List<Map<String, Object>> selectBaseSetInfo(Integer page);
	/**
	 * 批量更新
	 * @param sktSysConfigs
	 */
	public Object updateByIds(List<SktSysConfigs> sktSysConfigs);
	/**
	 * 更新图片信息
	 * @param fieldCode
	 * @param fieldValue
	 */
	public void updateFieldCode(String fieldCode, String fieldValue);
	public Map<String, Object> getQR();

}
