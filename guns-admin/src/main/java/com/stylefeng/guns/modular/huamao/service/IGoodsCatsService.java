package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;

/**
 * <p>
 * 商品分类表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IGoodsCatsService extends IService<GoodsCats> {
	List<GoodsCats> selectByParentId(int parentId);
	boolean deleteByPId (Integer pid);
	List<Map<String,Object>> selectMap(GoodsCats goodsCats);
	List<Brands> selectCatName(String sIds);

    Integer updateByPId(Integer catId);

	Integer deleteDataFlagById(Integer catId);
}
