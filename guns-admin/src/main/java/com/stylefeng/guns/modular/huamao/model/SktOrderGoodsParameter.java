package com.stylefeng.guns.modular.huamao.model;

public class SktOrderGoodsParameter {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	private String attrName;
	private String attrVal;
	
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getAttrVal() {
		return attrVal;
	}
	public void setAttrVal(String attrVal) {
		this.attrVal = attrVal;
	}
}
