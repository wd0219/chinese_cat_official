package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AttributesMapper;
import com.stylefeng.guns.modular.huamao.model.Attributes;
import com.stylefeng.guns.modular.huamao.model.AttributesCustem;
import com.stylefeng.guns.modular.huamao.service.IAttributesService;

/**
 * <p>
 * 商品属性表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class AttributesServiceImpl extends ServiceImpl<AttributesMapper, Attributes> implements IAttributesService {
	@Autowired
	private AttributesMapper attributesMapper;
	@Override
	public Integer insertAttributes(AttributesCustem attributesCustem) {
		// TODO Auto-generated method stub
		return baseMapper.insertAttributes(attributesCustem);
	}

	@Override
	public List<Map<String, Object>> selectAll(Page<Map<String,Object>> page,AttributesCustem attributesCustem) {
		// TODO Auto-generated method stub
		 List<Map<String, Object>> list = attributesMapper.selectAll(page,attributesCustem);
		 Integer total = attributesMapper.selectAllCount(attributesCustem);
		 page.setTotal(total);
		 return list;
	}

	@Override
	public AttributesCustem selectAttributesCustem(Integer id) {
		// TODO Auto-generated method stub
		return baseMapper.selectAttributesCustem(id);
	}

}
