package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktUsersDrawsMaobi;
import com.stylefeng.guns.modular.huamao.model.SktUsersDrawsMaobiDto;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户提现表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-30
 */
public interface ISktUsersDrawsMaobiService extends IService<SktUsersDrawsMaobi> {

	List<SktUsersDrawsMaobiDto> selectMaoBiList(Page<SktUsersDrawsMaobiDto> page,
			SktUsersDrawsMaobiDto sktUsersDrawsMaobiDto);

}
