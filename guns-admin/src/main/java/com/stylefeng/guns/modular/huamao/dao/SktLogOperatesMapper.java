package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktLogOperates;
import com.stylefeng.guns.modular.huamao.model.SktLogOperatesDto;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 操作记录表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-27
 */
public interface SktLogOperatesMapper extends BaseMapper<SktLogOperates> {
	/**
	 * 按时间查询列表
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public List<SktLogOperatesDto> selectListDate(@Param("page")Page<SktLogOperatesDto> page,@Param("sktLogOperatesDto") SktLogOperatesDto sktLogOperatesDto);

    Integer selectListDateCount(@Param("sktLogOperatesDto") SktLogOperatesDto sktLogOperatesDto);
}
