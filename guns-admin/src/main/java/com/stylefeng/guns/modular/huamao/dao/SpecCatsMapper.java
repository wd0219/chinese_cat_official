package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SpecCats;
import com.stylefeng.guns.modular.huamao.model.SpecCatsCustem;

/**
 * <p>
 * 商品规格分类表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface SpecCatsMapper extends BaseMapper<SpecCats> {
	Integer insertSpecCats(SpecCatsCustem specCatsCustem);
	List<Map<String, Object>> selectAll(@Param("page")Page<Map<String, Object>> page, 
			@Param("specCatsCustem")SpecCatsCustem specCatsCustem);
	Integer selectAllCount(@Param("specCatsCustem")SpecCatsCustem specCatsCustem);
	SpecCatsCustem selectSpecCatsCustemById(int id);
}
