package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.RepairOrder;
import com.stylefeng.guns.modular.huamao.model.RepairOrderList;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-26
 */
public interface RepairOrderMapper extends BaseMapper<RepairOrder> {

	// 第三方支付补单记录展示
	List<RepairOrderList> selectRepairOrderAll(RepairOrderList repairOrderList);

}
