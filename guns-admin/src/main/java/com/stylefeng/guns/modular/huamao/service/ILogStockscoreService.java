package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;
import com.stylefeng.guns.modular.huamao.model.LogStockscoreList;

/**
 * <p>
 * 库存积分流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface ILogStockscoreService extends IService<LogStockscore> {

	// 库存积分流水表展示
	public List<LogStockscoreList> selectLogStockScoreAll(Page<LogStockscoreList> page, LogStockscoreList logStockscoreList);

}
