package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsCatsMapper;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.service.IGoodsCatsService;

/**
 * <p>
 * 商品分类表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsCatsServiceImpl extends ServiceImpl<GoodsCatsMapper, GoodsCats> implements IGoodsCatsService {
@Autowired
private GoodsCatsMapper goodsCatsMapper;
	@Override
	public List<GoodsCats> selectByParentId(int parentId) {
		// TODO Auto-generated method stub
		return goodsCatsMapper.selectByParentId(parentId);
	}
	@Override
	public boolean deleteByPId(Integer pid) {
		// TODO Auto-generated method stub
		return goodsCatsMapper.deleteByPId(pid);
	}
	@Override
	public List<Map<String, Object>> selectMap(GoodsCats goodsCats) {
		 List<Map<String, Object>> list = goodsCatsMapper.selectMap(goodsCats);
		 return list;
	}
	@Override
	public List<Brands> selectCatName(String sIds) {
		List<Brands> list = goodsCatsMapper.selectCatName(sIds);
		return list;
	}

	@Override
	public Integer updateByPId(Integer catId) {
		return goodsCatsMapper.updateByPId(catId);
	}

	@Override
	public Integer deleteDataFlagById(Integer catId) {
		return goodsCatsMapper.deleteDataFlagById(catId);
	}

}
