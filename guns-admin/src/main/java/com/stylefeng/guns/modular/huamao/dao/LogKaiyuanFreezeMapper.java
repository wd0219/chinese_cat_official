package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreeze;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreezeList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 待发华宝营业额流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface LogKaiyuanFreezeMapper extends BaseMapper<LogKaiyuanFreeze> {

	// 代发华宝营业额流水表展示
	List<LogKaiyuanFreezeList> selectLogKaiyuanFreezeAll(@Param("page")Page<LogKaiyuanFreezeList> page, @Param("logKaiyuanFreezeList") LogKaiyuanFreezeList logKaiyuanFreezeList);

    Integer selectLogKaiyuanFreezeAllCount(@Param("logKaiyuanFreezeList") LogKaiyuanFreezeList logKaiyuanFreezeList);
}
