package com.stylefeng.guns.modular.huamao.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.dao.*;
import com.stylefeng.guns.modular.huamao.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.service.ISktShopApplysService;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 商城商家开店申请表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-10
 */
@Service
public class SktShopApplysServiceImpl extends ServiceImpl<SktShopApplysMapper, SktShopApplys> implements ISktShopApplysService {
	@Autowired
	private SktShopApplysMapper SktShopApplys;
	@Autowired
	private UsersMapper usersMapper;
	@Autowired
	private SktStaffsMapper sktStaffsMapper;
	@Autowired
	private SktShopApproveMapper sktShopApproveMapper;
	@Autowired
	private SktShopsMapper sktShopsMapper;
	@Autowired
	private AccountShopMapper accountShopMapper;
    @Autowired
    private SktShopScoresMapper sktShopScoresMapper;
	@Override
	public List<MyDTO> shopApplysfindall(Page<MyDTO> page, MyDTO myDto) {
		Integer total = SktShopApplys.shopApplysfindinfoCount(myDto);
		page.setTotal(total);
		List<MyDTO> list = SktShopApplys.shopApplysfindinfo(page,myDto);
		return list;
	}
	public List<Map<String,Object>> shopApplysfindup(MyDTO myDto) {
        List<Map<String,Object>> list = SktShopApplys.shopApplysfindup(myDto);
		return list;
	}
	@Override
	@Transactional
	public void shopApplyupdate(MyDTO myDto,Integer checkStaffId) {

		if(myDto.getApplyStatus()==3){//通过

			//修改用户是线上商家状态
			SktShopApplys selectById = SktShopApplys.selectById(myDto.getApplyId());
			Users users = new Users();
			users.setUserId(selectById.getUserId());
			users.setIsShop(1);
			usersMapper.updateById(users);
			//是否有shop表跟shop账户表
			String shopStatus = shopAndShopAccount(selectById.getUserId());
			if("SUCCESS".equals(shopStatus)){

				//更新shop表
				SktShops sktShops = sktShopsMapper.selectShopsInfo(selectById.getUserId());
				sktShops.setShopStatus(1);
				sktShopsMapper.updateById(sktShops);
				//更新线上商家申请表状态
				myDto.setShopId(sktShops.getShopId());
				myDto.setCheckStaffId(checkStaffId);
				SktShopApplys.shopApplyupdate(myDto);
                //查询有没有商店积分表
                List<SktShopScores> sktShopScores = sktShopScoresMapper.selectByShopId(sktShops.getShopId());
                if(sktShopScores.size() == 0){
                    SktShopScores sktShopScores2 = new SktShopScores();
                    sktShopScores2.setShopId(sktShops.getShopId());
                    sktShopScores2.setGoodsUsers(0);
                    sktShopScores2.setGoodsScore(0);
                    sktShopScores2.setServiceScore(0);
                    sktShopScores2.setServiceUsers(0);
                    sktShopScores2.setTimeScore(0);
                    sktShopScores2.setTimeUsers(0);
                    sktShopScores2.setTotalScore(0);
                    sktShopScores2.setTimeUsers(0);
                    sktShopScoresMapper.insert(sktShopScores2);
                }
			}else{
				//查询企业表
				List<SktShopApprove> list  = sktShopApproveMapper.selectApplyInfo2(selectById.getUserId());
				//SktShopApprove shopApprove = sktShopApproveMapper.selectApplyInfo(selectById.getUserId());
				SktShopApprove shopApprove = new SktShopApprove();
				if (list.size() > 0){
					shopApprove = list.get(0);
				}
				Integer userId = shopApprove.getUserId();
				//新增shops表
				SktShops sktShops = new SktShops();
				sktShops.setUserId(userId);
				sktShops.setProvinceId(shopApprove.getProvinceId());
				sktShops.setCityId(shopApprove.getCityId());
				sktShops.setAreaId(shopApprove.getAreaId());
				sktShops.setIsLegal(shopApprove.getIsLegal());
				sktShops.setAuthorizationImg(shopApprove.getAuthorizationImg());
				sktShops.setLegalPerson(shopApprove.getLegalPerson());
				sktShops.setIDnumber(shopApprove.getIDnumber());
				sktShops.setLegalPersonImg(shopApprove.getLegalPersonImg());
				sktShops.setCompanyName(shopApprove.getCompanyName());
				sktShops.setStoreStatus(0);
				sktShops.setShopStatus(1);
				sktShops.setLicenseNo(shopApprove.getLicenseNo());
				sktShops.setCompanyType(shopApprove.getCompanyType());
				sktShops.setLicenseMerge(shopApprove.getLicenseMerge());
				sktShops.setLicenseImg(shopApprove.getLicenseImg());
				sktShops.setCodeImg(shopApprove.getCodeImg());
				sktShops.setTaxImg(shopApprove.getTaxImg());
				sktShops.setIndustry(shopApprove.getIndustry());
				sktShops.setShopName(shopApprove.getShopName());
				sktShops.setLogo(shopApprove.getLogo());
				sktShops.setTelephone(shopApprove.getTelephone());
				sktShops.setAddress(shopApprove.getAddress());
				sktShops.setCreateTime(new Date());
				sktShops.setBusinessQuota(new BigDecimal(0));
				Integer shops = sktShopsMapper.insert(sktShops);

				//新增店铺账户表 skt_account_shop
				AccountShop accountShop = new AccountShop();
				accountShop.setUserId(userId);
				accountShop.setShopId(sktShops.getShopId());
				accountShop.setCreateTime(new Date());
				accountShopMapper.insert(accountShop);
				//修改个人账户shopId
				sktStaffsMapper.updataAccount(sktShops.getShopId(),userId);

                //更新线上商家申请表状态
                myDto.setShopId(sktShops.getShopId());
				myDto.setCheckStaffId(checkStaffId);
                SktShopApplys.shopApplyupdate(myDto);
                //查询有没有商店积分表
                List<SktShopScores> sktShopScores = sktShopScoresMapper.selectByShopId(sktShops.getShopId());
                if(sktShopScores.size() == 0){
                    SktShopScores sktShopScores2 = new SktShopScores();
                    sktShopScores2.setShopId(sktShops.getShopId());
                    sktShopScores2.setGoodsUsers(0);
                    sktShopScores2.setGoodsScore(0);
                    sktShopScores2.setServiceScore(0);
                    sktShopScores2.setServiceUsers(0);
                    sktShopScores2.setTimeScore(0);
                    sktShopScores2.setTimeUsers(0);
                    sktShopScores2.setTotalScore(0);
                    sktShopScores2.setTimeUsers(0);
                    sktShopScoresMapper.insert(sktShopScores2);
                }
			}

		}else{
			//更新状态
			SktShopApplys.shopApplyupdate(myDto);
		}
		
		
	}
	@Override
	public int selectCount() {
		// TODO Auto-generated method stub
		return SktShopApplys.selectCount();
	}

	/**
	 * 根据用户id查询用户shop表跟shop账户表
	 * @param usersId
	 * @return
	 */
	public String shopAndShopAccount(Integer usersId){
		Integer shopCount = sktStaffsMapper.selectShopsCount(usersId);
		Integer shopAcountCount = sktStaffsMapper.selectShopsAcountCount(usersId);
		if (shopCount != 0 && shopAcountCount != 0){
			return "SUCCESS";
		}
		return  "EMPTY";
	}
}
