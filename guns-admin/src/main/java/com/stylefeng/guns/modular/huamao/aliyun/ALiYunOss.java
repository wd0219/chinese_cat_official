package com.stylefeng.guns.modular.huamao.aliyun;

import com.alibaba.druid.util.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class ALiYunOss {
//    @Value("${aliyun.bucket}")
	private String bucket = "zzhtwl";
//	private String bucket;
//    @Value("${aliyun.accesskeyid}")
	private String accesskeyid = "LTAI39EEenendLvf";
//	private String accesskeyid;
//    @Value("${aliyun.accesskeysecret}")
	private String accesskeysecret = "95AakFFS3H504hp3AJxMr8eo9kK0nj";
//	private String accesskeysecret;
//    @Value("${aliyun.endpoint}")
	private String endpoint = "oss-cn-qingdao.aliyuncs.com";
//	private String endpoint;
//    @Value("${aliyun.protocol}")
	private String protocol = "http";
//	private String protocol;
//    @Value("${aliyun.domain}")
	private String customdomain = "dysmsapi.aliyuncs.com";
//	private String customdomain;
//    @Value("${aliyun.starpoint}")
	private String starpoint = "zzhtwl";
//	private String starpoint;
//	@Value("${aliyun.product}")
	private String product = "Dysmsapi";
//	private String product;

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
	
	/**
	 * 协议
	 * 
	 * @return
	 */
	
	public String getAccesskeyid() {
		return accesskeyid;
	}

	public void setAccesskeyid(String accesskeyid) {
		this.accesskeyid = accesskeyid;
	}

	public String getAccesskeysecret() {
		return accesskeysecret;
	}

	public void setAccesskeysecret(String accesskeysecret) {
		this.accesskeysecret = accesskeysecret;
	}
	
	public String getProtocol() {
		return protocol;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}


	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getStarpoint() {
		return starpoint;
	}

	public void setStarpoint(String starpoint) {
		this.starpoint = starpoint;
	}

	/**
	 * 协议
	 * 
	 * @return
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}


	/**
	 * 获取域名
	 * 
	 * @return
	 */
	public String getDomain() {
		if (StringUtils.isEmpty(customdomain))
			return getEndpoint().replace(getProtocol() + "://", getProtocol() + "://" + getBucket() + ".");
		else
			return this.customdomain;

	}
	
	
	public String getCustomdomain() {
		return customdomain;
	}
	
	/**
	 * 自定义域名，如果存在自定义域名，则直接使用自定义域名
	 * 
	 * @return
	 */

	public void setCustomdomain(String customdomain) {
		this.customdomain = customdomain;
	}

	/**
	 * 获取图片路径
	 * 
	 * @return
	 */
	public String getPoint(){
		return (this.protocol+"://"+this.starpoint+"."+this.endpoint+"/");
	}
	

}
