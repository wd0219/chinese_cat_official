package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 代理公司的股东更改表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-10
 */
@TableName("skt_agents_changes")
public class SktAgentsChanges extends Model<SktAgentsChanges> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 变更单号
     */
    private String changeNo;
    /**
     * 代理公司ID
     */
    private Integer agentId;
    /**
     * 类型 1升级 2降级 3转移 4任职
     */
    private Integer type;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 变化前角色 0不是股东 1董事长 2总裁 3行政总监 4财务总监 5部门经理 7普通股东
     */
    private Integer preRole;
    /**
     * 变化后角色  1董事长 2总裁 3行政总监 4财务总监 5部门经理 7普通股东
     */
    private Integer afterRole;
    /**
     * 升级补交股票份数
     */
    private Integer stockNum;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChangeNo() {
        return changeNo;
    }

    public void setChangeNo(String changeNo) {
        this.changeNo = changeNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPreRole() {
        return preRole;
    }

    public void setPreRole(Integer preRole) {
        this.preRole = preRole;
    }

    public Integer getAfterRole() {
        return afterRole;
    }

    public void setAfterRole(Integer afterRole) {
        this.afterRole = afterRole;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktAgentsChanges{" +
        "id=" + id +
        ", changeNo=" + changeNo +
        ", agentId=" + agentId +
        ", type=" + type +
        ", userId=" + userId +
        ", preRole=" + preRole +
        ", afterRole=" + afterRole +
        ", stockNum=" + stockNum +
        ", createTime=" + createTime +
        "}";
    }
}
