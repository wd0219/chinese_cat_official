package com.stylefeng.guns.modular.huamao.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.Account;
import com.stylefeng.guns.modular.huamao.model.AccountList;
import com.stylefeng.guns.modular.huamao.model.UsersDTO;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;
@RestController
@RequestMapping("/webAccount")
public class WebAccountController extends BaseController {
	@Autowired
	private IAccountService accountService;
	@Autowired
	private IUsersService usersService;
	
	
	/** 用户点击升级
	 * @param userId   用户id
	 
	 * @return
	 */
	@RequestMapping("/getUseUpDate")
	public  JSONObject getUseUpDate(Integer userId){
		JSONObject json=new JSONObject();
		UsersDTO usersDTO =new UsersDTO();
		usersDTO.setUserId(userId);
		UsersDTO usersDT = usersService.selectOneUsersInfo(usersDTO);
		if(usersDT!=null){
			int userType=usersDT.getUserType();
			if(userType==0){
				json.put("upAgentAccount", "997.00");
				json.put("upAgentScore", "99700.00");
				json.put("upManagerAccount", "9997.00");
				json.put("upManagerScore", "999700.00");
			}else if(userType==1 ){
				json.put("upManagerAccount", "9000.00");
				json.put("upManagerScore", "900000.00");
			}
			json.put("code", "01");
			json.put("msg", "成功");
		}else{
			json.put("code", "00");
			json.put("msg", "用户不存在");
		}
		return json;
		
	}
	
	/**  用户进入确认支付页面
	 * * @param userId   用户id
	 * @param userUpType 用户升级后角色
	 * @return
	 */
	@RequestMapping("/getCashToUp")
	public JSONObject getCashToUp(Integer userId, Integer userUpType) {
		JSONObject json = new JSONObject();
		Account account = accountService.selectAccountByuserId(userId);
		BigDecimal cash = account.getCash();
		UsersDTO usersDTO = new UsersDTO();
		usersDTO.setUserId(userId);
		UsersDTO usersDT = usersService.selectOneUsersInfo(usersDTO);
		if (usersDT != null) {
			int userType = usersDT.getUserType();
			if (userType == 0 && userUpType == 1) {
				json.put("upAccount", "997.00");
			//	json.put("upAgentScore", "99700.00");

			} else if (userType == 0 && userUpType == 2) {
				json.put("upAccount", "9997.00");
				//json.put("upManagerScore", "999700.00");

			} else if (userType == 1 && userUpType == 2) {
				json.put("upAccount", "9000.00");
				//json.put("upManagerScore", "900000.00");
			}
			json.put("code", "01");
			json.put("msg", "成功");
		} else {
			json.put("code", "00");
			json.put("msg", "用户不存在");
		}
		json.put("cash", cash);
	
		return json;

	}
	/**
	 * @param usersDTO  登录用户实体 前台传了userId
	 * @param cash 需要支付金额
	 * @param paypassword 支付密码
	 * @return
	 */
	@RequestMapping("/toPay")
	 public JSONObject toPay(UsersDTO usersDTO,BigDecimal cash,String paypassword){
		 JSONObject json=new JSONObject();
		 UsersDTO usersDT = usersService.selectOneUsersInfo(usersDTO);
		 Account account = accountService.selectAccountByuserId(usersDTO.getUserId());
		 BigDecimal formerCash = account.getCash();
		 BigDecimal cash2=formerCash.subtract(cash);
		 AccountList accountList=new AccountList();
		 accountList.setUserId(usersDTO.getUserId());
		 accountList.setCash(cash2);
		 if(paypassword.equals(usersDT.getPayPwd())){
			 Boolean flag = accountService.updateByUp(accountList, cash);
			 if(flag==true){
				 json.put("code", "01");
				 json.put("msg", "提交成功");  
			 }else{
				 json.put("code", "00");
				 json.put("msg", "提交失败");
			 }
		 }else{
			 json.put("code", "00");
			 json.put("msg", "用户密码错误"); 
		 }
		 
		 return json;
		 
	 }
	
	/**获得用户现金账户余额
	 * @param userId
	 * @return
	 */
	@RequestMapping("/getAccount")
	public  JSONObject getAccount(int userId){
		JSONObject json=new JSONObject();
		
		Account account = accountService.selectAccountByuserId(userId);
		if(account!=null){
			json.put("code", "01");
			json.put("msg", "成功");
			json.put("cash", account.getCash());
			json.put("kaiyuan", account.getKaiyuan());
			
		}else{
			json.put("code", "00");
			json.put("msg", "查询账户失败");
		}
		return json;
		
	}
	
	/**取消升级关于现金账户的变化
	 * @param usersUpgrade
	 * @return
	 */
	public JSONObject resetUPGrade(UsersUpgrade usersUpgrade){
		JSONObject json=new JSONObject();
		boolean flag = accountService.resetUPGrade(usersUpgrade);
		if(flag==true){
			json.put("code", "01");
			json.put("msg", "成功");	
		}else{
			json.put("code", "00");
			json.put("msg", "失败");
		}
		return json;
	}
	
	/**  用户消费
	 * @param userId  用户id
	 * @param num  消费金额
	 * @param  saleType 支付类型    1积分 2现金 3华宝 4库存积分
	 * @return
	 */
	public  JSONObject userSale(int userId,BigDecimal num,String saleType){
		return null;
		
	}

	@RequestMapping("/doUpAgent")
	public  void doUpAgent(){
		String s = accountService.doUpAgent(1,1);
		System.out.println(s);
	}
}
