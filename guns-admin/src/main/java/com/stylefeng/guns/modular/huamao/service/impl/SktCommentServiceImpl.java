package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktComment;
import com.stylefeng.guns.modular.huamao.dao.SktCommentMapper;
import com.stylefeng.guns.modular.huamao.model.SktCommentDTO;
import com.stylefeng.guns.modular.huamao.service.ISktCommentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 评论表(预留表) 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
@Service
public class SktCommentServiceImpl extends ServiceImpl<SktCommentMapper, SktComment> implements ISktCommentService {
    @Autowired
    private SktCommentMapper sktCommentMapper;
    @Override
    public List<SktCommentDTO> selectSktCommentAll(@Param("page") Page<SktCommentDTO> page, @Param("sktCommentDTO") SktCommentDTO sktCommentDTO){
        Integer total = sktCommentMapper.selectSktCommentAllCount(sktCommentDTO);
        page.setTotal(total);
        return sktCommentMapper.selectSktCommentAll(page, sktCommentDTO);

    }

}
