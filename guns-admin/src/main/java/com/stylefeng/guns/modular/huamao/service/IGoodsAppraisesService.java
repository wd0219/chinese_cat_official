package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesCustm;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品评价表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IGoodsAppraisesService extends IService<GoodsAppraises> {
	List<Map<String,Object>> selectAll(Page<Map<String, Object>> page, GoodsAppraisesCustm goodsAppraisesCustm);
	public int selectGoodsAppraisesTotal();
}
