package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktClassType;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 视频类型表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface ISktClassTypeService extends IService<SktClassType> {
    public List<SktClassType> selectSktClassTypeAll(Page<SktClassType> page, SktClassType sktClassType);

}
