package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AccountShopMapper;
import com.stylefeng.guns.modular.huamao.model.AccountShop;
import com.stylefeng.guns.modular.huamao.model.AccountShopList;
import com.stylefeng.guns.modular.huamao.service.IAccountShopService;

/**
 * <p>
 * 商家账户表(包括线上和线下) 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-12
 */
@Service
public class AccountShopServiceImpl extends ServiceImpl<AccountShopMapper, AccountShop> implements IAccountShopService {
	@Autowired
	private AccountShopMapper asm;

	// 展示用户账户列表
	@Override
	public List<AccountShopList> selectAccountShopAll(Page<AccountShopList> page, AccountShopList accountShopList) {
		Integer total = asm.selectAccountShopAllCount(accountShopList);
		page.setTotal(total);
		return asm.selectAccountShopAll(page,accountShopList);
	}

}
