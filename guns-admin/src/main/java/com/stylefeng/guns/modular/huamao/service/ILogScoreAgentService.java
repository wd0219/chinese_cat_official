package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgent;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgentList;

/**
 * <p>
 * 代理公司积分流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface ILogScoreAgentService extends IService<LogScoreAgent> {

	// 代理公司积分流水表返回
	public List<LogScoreAgentList> selectLogScoreAgentAll(Page<LogScoreAgentList> page, LogScoreAgentList logScoreAgentList);

}
