package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商家每日营业额统计表
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
@TableName("skt_sales_date")
public class SktSalesDate extends Model<SktSalesDate> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商家id
     */
    private Integer shopId;
    /**
     * 商家用户id
     */
    private Integer userId;
    /**
     * 营业日期(20171112)
     */
    private Integer date;
    /**
     * 营业月份(201711)
     */
    private Integer month;
    /**
     * 门店营业总额
     */
    private BigDecimal storeTotal;
    /**
     * 门店营业额现金部分
     */
    private BigDecimal storeCash;
    /**
     * 门店营业额开元宝部分
     */
    private BigDecimal storeKaiyuan;
    /**
     * 商城营业总额
     */
    private BigDecimal mallTotal;
    /**
     * 商城营业额现金部分
     */
    private BigDecimal mallCash;
    /**
     * 商城营业额开元宝部分
     */
    private BigDecimal mallKaiyuan;
    /**
     * 商家参与分红总额
     */
    private BigDecimal profitTotal;
    /**
     * 创建时间
     */
    private String createTime;
    
    private String shopName;
    
    private String userName;
    
    private String checkTime;

    public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public BigDecimal getStoreTotal() {
        return storeTotal;
    }

    public void setStoreTotal(BigDecimal storeTotal) {
        this.storeTotal = storeTotal;
    }

    public BigDecimal getStoreCash() {
        return storeCash;
    }

    public void setStoreCash(BigDecimal storeCash) {
        this.storeCash = storeCash;
    }

    public BigDecimal getStoreKaiyuan() {
        return storeKaiyuan;
    }

    public void setStoreKaiyuan(BigDecimal storeKaiyuan) {
        this.storeKaiyuan = storeKaiyuan;
    }

    public BigDecimal getMallTotal() {
        return mallTotal;
    }

    public void setMallTotal(BigDecimal mallTotal) {
        this.mallTotal = mallTotal;
    }

    public BigDecimal getMallCash() {
        return mallCash;
    }

    public void setMallCash(BigDecimal mallCash) {
        this.mallCash = mallCash;
    }

    public BigDecimal getMallKaiyuan() {
        return mallKaiyuan;
    }

    public void setMallKaiyuan(BigDecimal mallKaiyuan) {
        this.mallKaiyuan = mallKaiyuan;
    }

    public BigDecimal getProfitTotal() {
        return profitTotal;
    }

    public void setProfitTotal(BigDecimal profitTotal) {
        this.profitTotal = profitTotal;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	@Override
	public String toString() {
		return "SktSalesDate [id=" + id + ", shopId=" + shopId + ", userId=" + userId + ", date=" + date + ", month="
				+ month + ", storeTotal=" + storeTotal + ", storeCash=" + storeCash + ", storeKaiyuan=" + storeKaiyuan
				+ ", mallTotal=" + mallTotal + ", mallCash=" + mallCash + ", mallKaiyuan=" + mallKaiyuan
				+ ", profitTotal=" + profitTotal + ", createTime=" + createTime + ", shopName=" + shopName
				+ ", userName=" + userName + ", checkTime=" + checkTime + "]";
	}

	public SktSalesDate(Integer id, Integer shopId, Integer userId, Integer date, Integer month, BigDecimal storeTotal,
			BigDecimal storeCash, BigDecimal storeKaiyuan, BigDecimal mallTotal, BigDecimal mallCash,
			BigDecimal mallKaiyuan, BigDecimal profitTotal, String createTime, String shopName, String userName,
			String checkTime) {
		super();
		this.id = id;
		this.shopId = shopId;
		this.userId = userId;
		this.date = date;
		this.month = month;
		this.storeTotal = storeTotal;
		this.storeCash = storeCash;
		this.storeKaiyuan = storeKaiyuan;
		this.mallTotal = mallTotal;
		this.mallCash = mallCash;
		this.mallKaiyuan = mallKaiyuan;
		this.profitTotal = profitTotal;
		this.createTime = createTime;
		this.shopName = shopName;
		this.userName = userName;
		this.checkTime = checkTime;
	}

	public SktSalesDate() {
		super();
		// TODO Auto-generated constructor stub
	}

    
    
}
