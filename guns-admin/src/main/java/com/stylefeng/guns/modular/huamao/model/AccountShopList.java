package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 商家账户返回
 */
public class AccountShopList extends AccountShop {

	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 用户类型
	 */
	private Integer userType;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "AccountShopList{" +
				"userPhone='" + userPhone + '\'' +
				", userType=" + userType +
				", beginTime='" + beginTime + '\'' +
				", endTime='" + endTime + '\'' +
				'}';
	}
}
