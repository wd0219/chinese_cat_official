package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 代理公司的银行卡表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
@TableName("skt_agents_bankcards")
public class AgentsBankcards extends Model<AgentsBankcards> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "bankCardId", type = IdType.AUTO)
    private Integer bankCardId;
    /**
     * 代理公司ID
     */
    private Integer agentId;
    /**
     * 省id
     */
    private Integer provinceId;
    /**
     * 市id
     */
    private Integer cityId;
    /**
     * 区县id
     */
    private Integer areaId;
    /**
     * 银行ID
     */
    private Integer bankId;
    /**
     * 开户行
     */
    private String accArea;
    /**
     * 银行卡号
     */
    private String accNo;
    /**
     * 持卡人
     */
    private String accUser;
    /**
     * 身份证号
     */
    private String IDnumber;
    /**
     * 银行预留电话号码
     */
    private String phone;
    /**
     * 有效标志
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(Integer bankCardId) {
        this.bankCardId = bankCardId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getAccArea() {
        return accArea;
    }

    public void setAccArea(String accArea) {
        this.accArea = accArea;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getAccUser() {
        return accUser;
    }

    public void setAccUser(String accUser) {
        this.accUser = accUser;
    }

    public String getIDnumber() {
        return IDnumber;
    }

    public void setIDnumber(String IDnumber) {
        this.IDnumber = IDnumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.bankCardId;
    }

    @Override
    public String toString() {
        return "AgentsBankcards{" +
        "bankCardId=" + bankCardId +
        ", agentId=" + agentId +
        ", provinceId=" + provinceId +
        ", cityId=" + cityId +
        ", areaId=" + areaId +
        ", bankId=" + bankId +
        ", accArea=" + accArea +
        ", accNo=" + accNo +
        ", accUser=" + accUser +
        ", IDnumber=" + IDnumber +
        ", phone=" + phone +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
