package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统消息表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface MessagesMapper extends BaseMapper<Messages> {

	// 通过传过来的ID进行逻辑删除
	public void deleteById2(Integer id);

	// 系统消息展示
	public List<MessagesList> selectMessagesAll(@Param("page")Page<MessagesList> page,@Param("messagesList") MessagesList messagesList);

	// 获取用户
	public List<MessagesList> getUsers(String userPhone);

	// 添加信息
	public Integer shopAddMessages(
			@Param("messages") Messages messages,
			@Param("userIds") List<Integer> userIds
	);
	public Integer selectMessagesAllCount(@Param("messagesList")MessagesList messagesList);
}
