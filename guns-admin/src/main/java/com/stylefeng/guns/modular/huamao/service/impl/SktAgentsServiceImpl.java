package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsMapper;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsDTO;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsService;

/**
 * <p>
 * 代理公司表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
@Service
public class SktAgentsServiceImpl extends ServiceImpl<SktAgentsMapper, SktAgents> implements ISktAgentsService {
	
	@Autowired
	private SktAgentsMapper sktAgentsMapper;
	@Override
	public List<SktAgentsDTO> showSktAgentsIno(Page<SktAgentsDTO>page,SktAgentsDTO sktAgentsDTO) {
		List<SktAgentsDTO> list = sktAgentsMapper.showSktAgentsIno(page,sktAgentsDTO);
		Integer total = sktAgentsMapper.showSktAgentsInoCount(sktAgentsDTO);
		page.setTotal(total);
		return list;
	}
	@Override
	public Map<String, Object> selectNameOrPhone(String nameOrPhone) {
		Map<String, Object> map = sktAgentsMapper.selectNameOrPhone(nameOrPhone);
		return map;
	}
	@Override
	public List<SktAgents> selectHasAgents(SktAgents sktAgents) {
		List<SktAgents> list = sktAgentsMapper.selectHasAgents(sktAgents);
		return list;
	}

}
