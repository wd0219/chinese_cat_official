package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktShopAccreds;
import com.stylefeng.guns.modular.huamao.model.SktShopAccredsDTO;
import com.stylefeng.guns.modular.huamao.service.ISktShopAccredsService;

/**
 * 店铺管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-13 15:34:29
 */
@Controller
@RequestMapping("/sktShopAccreds")
public class SktShopAccredsController extends BaseController {

    private String PREFIX = "/huamao/sktShopAccreds/";

    @Autowired
    private ISktShopAccredsService sktShopAccredsService;

    /**
     * 跳转到线下店铺管理首页
     */
    @RequestMapping("/indexXianxia")
    public String indexXianxia() {
        return PREFIX + "sktShopAccreds.html";
    }


    /**
     * 跳转到线下店铺管理首页
     */
    @RequestMapping("")
    public String index(Integer shopType,Model model) {
        model.addAttribute("shopType",shopType);
        return PREFIX + "sktShopAccreds.html";
    }
    /**
     * 跳转到添加店铺管理
     */
    @RequestMapping("/sktShopAccreds_add")
    public String sktShopAccredsAdd() {
        return PREFIX + "sktShopAccreds_add.html";
    }

    /**
     * 跳转到修改店铺管理
     */
    @RequestMapping("/sktShopAccreds_update/{sktShopAccredsId}")
    public String sktShopAccredsUpdate(@PathVariable Integer sktShopAccredsId, Model model,SktShopAccredsDTO shopAccredsDTO) {
        List<SktShopAccredsDTO> list = sktShopAccredsService.sktShopAccredsfindup(shopAccredsDTO);
        model.addAttribute("item",list.get(0));
        //LogObjectHolder.me().set(sktShopAccreds);
        return PREFIX + "sktShopAccreds_edit.html";
    }

    /**
     * 获取店铺管理列表
     */
    @RequestMapping(value = "/list/{shopType}")
    @ResponseBody
    public Object list(SktShopAccredsDTO sktShopAccredsDTO) {
        Page<SktShopAccredsDTO> page = new PageFactory<SktShopAccredsDTO>().defaultPage();
        List<SktShopAccredsDTO> sktShopAccredsDTOS = sktShopAccredsService.sktShopAccredsfindall(page,sktShopAccredsDTO);
        page.setRecords(sktShopAccredsDTOS);
        return super.packForBT(page);
    }

    /**
     * 新增店铺管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopAccreds sktShopAccreds) {
        sktShopAccredsService.insert(sktShopAccreds);
        return SUCCESS_TIP;
    }

    /**
     * 删除店铺管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam(value="shopId") Integer shopId) {
        sktShopAccredsService.sktShopAccredsdelete(shopId);
        return SUCCESS_TIP;
    }

    /**
     * 修改店铺管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopAccreds sktShopAccreds) {
        sktShopAccredsService.updateById(sktShopAccreds);
        return SUCCESS_TIP;
    }

    /**
     * 店铺管理详情
     */
    @RequestMapping(value = "/detail/{sktShopAccredsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopAccredsId") Integer sktShopAccredsId) {
        return sktShopAccredsService.selectById(sktShopAccredsId);
    }
}
