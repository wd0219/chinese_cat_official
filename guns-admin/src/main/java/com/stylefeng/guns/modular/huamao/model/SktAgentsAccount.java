package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 代理公司账户表
 * </p>
 *
 * @author slt123
 * @since 2018-04-18
 */
@TableName("skt_agents_account")
public class SktAgentsAccount extends Model<SktAgentsAccount> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "accountId", type = IdType.AUTO)
    private Integer accountId;
    /**
     * 代理公司ID
     */
    private Integer agentId;
    /**
     * 积分
     */
    private BigDecimal score;
    /**
     * 待发积分 运营未开启时 代理股东通过后 奖励代理公司积分到此账号
     */
    private BigDecimal freezeScore;
    /**
     * 累计获得积分
     */
    private BigDecimal totalScore;
    /**
     * 开元宝
     */
    private BigDecimal kaiyuan;
    /**
     * 累计获得开元宝
     */
    private BigDecimal totalKaiyuan;
    /**
     * 运营账号是否开启 开启后账户积分开始转化开元宝 1开启 0关闭
     */
    private Integer runFlag;
    /**
     * 运营账号开启时间
     */
    private Date runTime;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 购物券金额
     */
    private BigDecimal shoppingMoney;

    public BigDecimal getShoppingMoney() {
        return shoppingMoney;
    }

    public void setShoppingMoney(BigDecimal shoppingMoney) {
        this.shoppingMoney = shoppingMoney;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public BigDecimal getFreezeScore() {
        return freezeScore;
    }

    public void setFreezeScore(BigDecimal freezeScore) {
        this.freezeScore = freezeScore;
    }

    public BigDecimal getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getTotalKaiyuan() {
        return totalKaiyuan;
    }

    public void setTotalKaiyuan(BigDecimal totalKaiyuan) {
        this.totalKaiyuan = totalKaiyuan;
    }

    public Integer getRunFlag() {
        return runFlag;
    }

    public void setRunFlag(Integer runFlag) {
        this.runFlag = runFlag;
    }

    public Date getRunTime() {
        return runTime;
    }

    public void setRunTime(Date runTime) {
        this.runTime = runTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.accountId;
    }

    @Override
    public String toString() {
        return "SktAgentsAccount{" +
        "accountId=" + accountId +
        ", agentId=" + agentId +
        ", score=" + score +
        ", freezeScore=" + freezeScore +
        ", totalScore=" + totalScore +
        ", kaiyuan=" + kaiyuan +
        ", totalKaiyuan=" + totalKaiyuan +
        ", runFlag=" + runFlag +
        ", runTime=" + runTime +
        ", createTime=" + createTime +
        "}";
    }
}
