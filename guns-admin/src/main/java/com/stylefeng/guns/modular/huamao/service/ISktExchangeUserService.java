package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktExchangeUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author caody123
 * @since 2018-06-23
 */
public interface ISktExchangeUserService extends IService<SktExchangeUser> {

}
