package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.SktClassType;
import com.stylefeng.guns.modular.huamao.service.ISktClassTypeService;

import java.util.Date;
import java.util.List;

/**
 * 视频类型控制器
 *
 * @author fengshuonan
 * @Date 2018-07-26 09:51:53
 */
@Controller
@RequestMapping("/sktClassType")
public class SktClassTypeController extends BaseController {

    private String PREFIX = "/huamao/sktClassType/";

    @Autowired
    private ISktClassTypeService sktClassTypeService;

    /**
     * 跳转到视频类型首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktClassType.html";
    }

    /**
     * 跳转到添加视频类型
     */
    @RequestMapping("/sktClassType_add")
    public String sktClassTypeAdd() {
        return PREFIX + "sktClassType_add.html";
    }

    /**
     * 跳转到修改视频类型
     */
    @RequestMapping("/sktClassType_update/{sktClassTypeId}")
    public String sktClassTypeUpdate(@PathVariable Integer sktClassTypeId, Model model) {
        SktClassType sktClassType = sktClassTypeService.selectById(sktClassTypeId);
        model.addAttribute("item",sktClassType);
        LogObjectHolder.me().set(sktClassType);
        return PREFIX + "sktClassType_edit.html";
    }

    /**
     * 获取视频类型列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktClassType sktClassType) {
        Page<SktClassType> page = new PageFactory<SktClassType>().defaultPage();
        List<SktClassType> sktClassTypes = sktClassTypeService.selectSktClassTypeAll(page, sktClassType);
        page.setRecords(sktClassTypes);
        return super.packForBT(page);
//        return sktClassTypeService.selectList(null);
    }

    /**
     * 新增视频类型
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktClassType sktClassType) {
        sktClassType.setCreatTime(new Date());
        sktClassTypeService.insert(sktClassType);
        return SUCCESS_TIP;
    }

    /**
     * 删除视频类型
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktClassTypeId) {
        sktClassTypeService.deleteById(sktClassTypeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改视频类型
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktClassType sktClassType) {
        sktClassTypeService.updateById(sktClassType);
        return SUCCESS_TIP;
    }

    /**
     * 视频类型详情
     */
    @RequestMapping(value = "/detail/{sktClassTypeId}")
    @ResponseBody
    public Object detail(@PathVariable("sktClassTypeId") String sktClassTypeId) {
        return sktClassTypeService.selectById(sktClassTypeId);
    }

    /**
     * 获取视频类型列表
     */
    @RequestMapping(value = "/getJsonData")
    @ResponseBody
    public JSONArray getJson() {
        List<SktClassType> list = sktClassTypeService.selectList(null);
        JSONArray jSONArray=new JSONArray();
        for (SktClassType classType : list) {
            JSONObject json=new JSONObject();
            json.put("typeId", classType.getTypeId());
            json.put("typeName", classType.getTypeName());
            jSONArray.add(json);
        }
        return jSONArray;

    }
    /**
     * 获取视频类型列表
     */
    @RequestMapping(value = "/getAll")
    @ResponseBody
    public Object getAll() {
        return sktClassTypeService.selectList(null);
    }
}
