package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 订单商品表
 * </p>
 *
 * @author caody123
 * @since 2018-04-18
 */
@TableName("skt_order_goods")
public class SktOrderGoods extends Model<SktOrderGoods> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 自增ID
     */
    private Integer orderId;
    /**
     * 商品ID
     */
    private Integer goodsId;
    /**
     * 商品数量
     */
    private Integer goodsNum;
    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;
    /**
     * 自营店铺的商品支付的积分
     */
    private Integer payScore;
    /**
     * 积分赠送比例
     */
    private Integer scoreRatio;
    /**
     * 赠送积分
     */
    private BigDecimal score;
    /**
     * 商品-规格ID
     */
    private String goodsSpecId;
    /**
     * 商品-规格值列表
     */
    private String goodsSpecNames;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品图
     */
    private String goodsImg;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getPayScore() {
        return payScore;
    }

    public void setPayScore(Integer payScore) {
        this.payScore = payScore;
    }

    public Integer getScoreRatio() {
        return scoreRatio;
    }

    public void setScoreRatio(Integer scoreRatio) {
        this.scoreRatio = scoreRatio;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public String getGoodsSpecId() {
        return goodsSpecId;
    }

    public void setGoodsSpecId(String goodsSpecId) {
        this.goodsSpecId = goodsSpecId;
    }

    public String getGoodsSpecNames() {
        return goodsSpecNames;
    }

    public void setGoodsSpecNames(String goodsSpecNames) {
        this.goodsSpecNames = goodsSpecNames;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktOrderGoods{" +
        "id=" + id +
        ", orderId=" + orderId +
        ", goodsId=" + goodsId +
        ", goodsNum=" + goodsNum +
        ", goodsPrice=" + goodsPrice +
        ", payScore=" + payScore +
        ", scoreRatio=" + scoreRatio +
        ", score=" + score +
        ", goodsSpecId=" + goodsSpecId +
        ", goodsSpecNames=" + goodsSpecNames +
        ", goodsName=" + goodsName +
        ", goodsImg=" + goodsImg +
        "}";
    }
}
