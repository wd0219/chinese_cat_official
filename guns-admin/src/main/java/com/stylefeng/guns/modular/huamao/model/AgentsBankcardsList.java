package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 代理公司的银行卡表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public class AgentsBankcardsList extends AgentsBankcards {
	/**
	 * 代理公司名称
	 */
	private String agentName;
	/**
	 * 银行名称
	 */
	private String bankName;
	// 因为前台获取IDnumber是出错所以让它获取number
	private String number;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	// 让number直接返回IDnumber，不需要set
	public String getNumber() {
		return this.getIDnumber();
	}

	// public void setNumber(String number) {
	// this.number = number;
	// }

}
