package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.CheckDetail;
import com.stylefeng.guns.modular.huamao.model.CheckDetailList;

/**
 * <p>
 * 每日用户对账结果数据表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-13
 */
public interface ICheckDetailService extends IService<CheckDetail> {

	// 展示用户每日对账列表
	public List<CheckDetailList> selectCheckDetailAll(Page<CheckDetailList> page, CheckDetailList checkDetailList);

	// 通过id查询详情
	public CheckDetailList selectctDetailedById(Integer checkDetailId);

}
