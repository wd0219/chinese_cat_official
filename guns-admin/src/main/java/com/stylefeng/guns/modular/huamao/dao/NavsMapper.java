package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.Navs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商城导航表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface NavsMapper extends BaseMapper<Navs> {

}
