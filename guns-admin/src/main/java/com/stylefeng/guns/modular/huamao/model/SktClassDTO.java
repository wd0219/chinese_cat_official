package com.stylefeng.guns.modular.huamao.model;

import java.util.Date;

public class SktClassDTO extends SktClass{
    /**
     * 上传k开始时间
     */
    private String startCreatTime;
    /**
     * 上传结束时间
     */
    private String endCreatTime;
    /**
     * 审核开始时间
     */
    private String startCheckTime;

    /**
     * 审核开始时间
     */
    private String endCheckTime;
/**
 * 视频类型名称
 */
    private String typeName;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getStartCreatTime() {
        return startCreatTime;
    }

    public void setStartCreatTime(String startCreatTime) {
        this.startCreatTime = startCreatTime;
    }

    public String getEndCreatTime() {
        return endCreatTime;
    }

    public void setEndCreatTime(String endCreatTime) {
        this.endCreatTime = endCreatTime;
    }

    public String getStartCheckTime() {
        return startCheckTime;
    }

    public void setStartCheckTime(String startCheckTime) {
        this.startCheckTime = startCheckTime;
    }

    public String getEndCheckTime() {
        return endCheckTime;
    }

    public void setEndCheckTime(String endCheckTime) {
        this.endCheckTime = endCheckTime;
    }
}
