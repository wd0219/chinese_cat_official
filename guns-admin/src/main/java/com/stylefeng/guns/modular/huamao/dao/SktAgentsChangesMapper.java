package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktAgentsChanges;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 代理公司的股东更改表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-10
 */
public interface SktAgentsChangesMapper extends BaseMapper<SktAgentsChanges> {

}
