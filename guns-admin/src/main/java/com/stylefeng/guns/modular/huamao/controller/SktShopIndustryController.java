package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;
import com.stylefeng.guns.modular.huamao.service.ISktShopIndustryService;

/**
 * 行业管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 09:10:27
 */
@Controller
@RequestMapping("/sktShopIndustry")
public class SktShopIndustryController extends BaseController {

    private String PREFIX = "/huamao/sktShopIndustry/";

    @Autowired
    private ISktShopIndustryService sktShopIndustryService;

    /**
     * 跳转到行业管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopIndustry.html";
    }

    /**
     * 跳转到添加行业管理
     */
    @RequestMapping("/sktShopIndustry_add")
    public String sktShopIndustryAdd() {
        return PREFIX + "sktShopIndustry_add.html";
    }

    /**
     * 跳转到修改行业管理
     */
    @RequestMapping("/sktShopIndustry_update/{sktShopIndustryId}")
    public String sktShopIndustryUpdate(@PathVariable Integer sktShopIndustryId, Model model) {
        SktShopIndustry sktShopIndustry = sktShopIndustryService.selectById(sktShopIndustryId);

        String icon = sktShopIndustry.getIcon();
        String allUrl = UrlUtils.getAllUrl(icon);
        sktShopIndustry.setIcon(allUrl);

        model.addAttribute("item",sktShopIndustry);
        LogObjectHolder.me().set(sktShopIndustry);
        return PREFIX + "sktShopIndustry_edit.html";
    }

    /**
     * 获取行业管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktShopIndustry shopIndustry,HttpServletRequest request) {
        Page<SktShopIndustry> page = new PageFactory<SktShopIndustry>().defaultPage();
    	List<SktShopIndustry> list = sktShopIndustryService.shopIndustryfindinfo(page,shopIndustry);
    	page.setRecords(list);
    	return super.packForBT(page);
    }
    
    /**
     * 获取行业管理列表
     */
    @RequestMapping(value = "/lists")
    @ResponseBody
    public Object lists(SktShopIndustry shopIndustry) {
    	List<SktShopIndustry> lists = sktShopIndustryService.shopIndustryfindinfo2(shopIndustry);
    	return lists;
    }
    /**
     * 新增行业管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopIndustry sktShopIndustry) {
        sktShopIndustryService.insert(sktShopIndustry);
        return SUCCESS_TIP;
    }

    /**
     * 删除行业管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopIndustryId) {
        sktShopIndustryService.deleteById(sktShopIndustryId);
        return SUCCESS_TIP;
    }

    /**
     * 修改行业管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopIndustry sktShopIndustry) {
        sktShopIndustryService.updateById(sktShopIndustry);
        return SUCCESS_TIP;
    }

    /**
     * 行业管理详情
     */
    @RequestMapping(value = "/detail/{sktShopIndustryId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopIndustryId") Integer sktShopIndustryId) {
        return sktShopIndustryService.selectById(sktShopIndustryId);
    }
}
