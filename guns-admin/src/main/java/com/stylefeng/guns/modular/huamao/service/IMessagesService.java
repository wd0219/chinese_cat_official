package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 系统消息表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface IMessagesService extends IService<Messages> {

	// 通过传过来的ID进行逻辑删除
	public void deleteById2(Integer id);

	// 系统消息展示
	public List<MessagesList> selectMessagesAll(Page<MessagesList> page, MessagesList messagesList);

	// 获取用户
	public List<MessagesList> getUsers(String userPhone);

	// 添加广告信息
	public boolean addMessages(Integer msgType,Integer sendUserId,List<Integer> usersId,String msgContent,String msgJson);

	//阿里云图片上传
	public Map<String,Object> upload(HttpServletRequest req);

}
