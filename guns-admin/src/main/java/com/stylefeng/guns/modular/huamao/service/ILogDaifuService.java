package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogDaifu;
import com.stylefeng.guns.modular.huamao.model.LogDaifuList;

/**
 * <p>
 * 代付记录表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface ILogDaifuService extends IService<LogDaifu> {

	// 代付记录表展示
	public List<LogDaifuList> selectLogDaifuAll(Page<LogDaifuList> page, LogDaifuList logDaifuList);

}
