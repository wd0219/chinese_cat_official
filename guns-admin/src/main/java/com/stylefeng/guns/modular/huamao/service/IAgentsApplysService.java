package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.AgentsApplys;
import com.stylefeng.guns.modular.huamao.model.AgentsApplysDTO;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 代理公司股东申请表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-17
 */
public interface IAgentsApplysService extends IService<AgentsApplys> {
	/**
	 * 查询列表
	 * @param page 
	 * @param agentsApplysDTO
	 * @return
	 */
	public List<AgentsApplysDTO> showDaiLiShenQingInfo (Page<AgentsApplysDTO> page, AgentsApplysDTO agentsApplysDTO);
	/**
	 * 显示添加信息
	 * @param shaId
	 * @return
	 */
	public AgentsApplysDTO selectAddInfo(Integer shaId);
	public List<AgentsApplys> getAgentsApplysSevenAgo(Integer userId);
	public Map<String,Object> applysSetStatus(Integer shaId, Integer staffId);
}
