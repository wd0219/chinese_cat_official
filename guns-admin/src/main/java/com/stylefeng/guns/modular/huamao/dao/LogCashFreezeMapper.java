package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogCashFreeze;
import com.stylefeng.guns.modular.huamao.model.LogCashFreezeList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 待发现金流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogCashFreezeMapper extends BaseMapper<LogCashFreeze> {

	List<LogCashFreezeList> selectLogCashFreezeAll(@Param("page")Page<LogCashFreezeList> page , @Param("logCashFreezeList") LogCashFreezeList logCashFreezeList);

	Integer selectLogCashFreezeAllCount(@Param("logCashFreezeList")LogCashFreezeList logCashFreezeList);
}
