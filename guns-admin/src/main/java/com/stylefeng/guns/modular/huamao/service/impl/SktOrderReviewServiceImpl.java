package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktOrderReview;
import com.stylefeng.guns.modular.huamao.dao.SktOrderReviewMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrderReviewDTO;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.service.ISktOrderReviewService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商城订单审核表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-09-19
 */
@Service
public class SktOrderReviewServiceImpl extends ServiceImpl<SktOrderReviewMapper, SktOrderReview> implements ISktOrderReviewService {

    @Autowired
    private SktOrderReviewMapper sktOrderReviewMapper;

    @Override
    public List<SktOrderReviewDTO> selectAll(Page<SktOrderReviewDTO> page, SktOrderReviewDTO sktOrderReviewDTO) {
        Integer total = sktOrderReviewMapper.selectAllCount(sktOrderReviewDTO);
        page.setTotal(total);
        List<SktOrderReviewDTO> list = sktOrderReviewMapper.selectAll(page,sktOrderReviewDTO);
        return list;
    }

    @Override
    @Transactional
    public Map<String, Object> updateStatus(Integer stassId,String id, String orderStatus, String checkRemark) {
        Map<String,Object> map = new HashMap<String,Object>();
        try {
            SktOrderReview sktOrderReview = new SktOrderReview();
            sktOrderReview.setId(Integer.parseInt(id));
            sktOrderReview.setOrderStatus(Integer.parseInt(orderStatus));
            if(checkRemark != null && !("").equals(checkRemark)){
                sktOrderReview.setCheckRemark(checkRemark);
            }
            sktOrderReview.setStaffId(stassId);
            sktOrderReview.setAuditDatetime(new Date());
            boolean b = this.updateById(sktOrderReview);

            if(b){
                SktOrderReview sktOrderReview1 = this.selectById(id);
                String orderunique = sktOrderReview1.getOrderunique();
                Map<String,Object> mmp = new HashMap<String,Object>();
                mmp.put("orderunique",orderunique);
                if("-1".equals(orderStatus)){
                    mmp.put("orderStatus","-4");
                }else{
                    mmp.put("orderStatus","0");
                }
                Integer update = sktOrderReviewMapper.updateSktOrderBath(mmp);
                if(update != 1){
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    map.put("code","00");
                    map.put("msg","未查到订单号");
                    return map;
                }
                map.put("code","01");
                map.put("msg","成功");
                return map;
            }
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            map.put("code","00");
            map.put("msg","出错了");
            return map;
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            map.put("code","00");
            map.put("msg","出异常了");
            return map;
        }
    }
}
