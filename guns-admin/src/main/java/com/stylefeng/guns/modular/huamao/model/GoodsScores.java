package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商品评分表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_goods_scores")
public class GoodsScores extends Model<GoodsScores> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "scoreId", type = IdType.AUTO)
    private Integer scoreId;
    /**
     * 商品Id
     */
    private Integer goodsId;
    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 总评分
     */
    private Integer totalScore;
    /**
     * 总评评分用户数
     */
    private Integer totalUsers;
    /**
     * 商品评分
     */
    private Integer goodsScore;
    /**
     * 商品评分用户数
     */
    private Integer goodsUsers;
    /**
     * 服务评分
     */
    private Integer serviceScore;
    /**
     * 服务评分用户数
     */
    private Integer serviceUsers;
    /**
     * 时效评分
     */
    private Integer timeScore;
    /**
     * 时效评分用户数
     */
    private Integer timeUsers;


    public Integer getScoreId() {
        return scoreId;
    }

    public void setScoreId(Integer scoreId) {
        this.scoreId = scoreId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Integer getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(Integer totalUsers) {
        this.totalUsers = totalUsers;
    }

    public Integer getGoodsScore() {
        return goodsScore;
    }

    public void setGoodsScore(Integer goodsScore) {
        this.goodsScore = goodsScore;
    }

    public Integer getGoodsUsers() {
        return goodsUsers;
    }

    public void setGoodsUsers(Integer goodsUsers) {
        this.goodsUsers = goodsUsers;
    }

    public Integer getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(Integer serviceScore) {
        this.serviceScore = serviceScore;
    }

    public Integer getServiceUsers() {
        return serviceUsers;
    }

    public void setServiceUsers(Integer serviceUsers) {
        this.serviceUsers = serviceUsers;
    }

    public Integer getTimeScore() {
        return timeScore;
    }

    public void setTimeScore(Integer timeScore) {
        this.timeScore = timeScore;
    }

    public Integer getTimeUsers() {
        return timeUsers;
    }

    public void setTimeUsers(Integer timeUsers) {
        this.timeUsers = timeUsers;
    }

    @Override
    protected Serializable pkVal() {
        return this.scoreId;
    }

    @Override
    public String toString() {
        return "GoodsScores{" +
        "scoreId=" + scoreId +
        ", goodsId=" + goodsId +
        ", shopId=" + shopId +
        ", totalScore=" + totalScore +
        ", totalUsers=" + totalUsers +
        ", goodsScore=" + goodsScore +
        ", goodsUsers=" + goodsUsers +
        ", serviceScore=" + serviceScore +
        ", serviceUsers=" + serviceUsers +
        ", timeScore=" + timeScore +
        ", timeUsers=" + timeUsers +
        "}";
    }
}
