package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.ArticleCats;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
public interface IArticleCatsService extends IService<ArticleCats> {
	List<Map<String,Object>>selectByParentId(Integer parentId);
	boolean deleteByParentId(Integer parentId);
	List<Map<String,Object>> selectAll();
}
