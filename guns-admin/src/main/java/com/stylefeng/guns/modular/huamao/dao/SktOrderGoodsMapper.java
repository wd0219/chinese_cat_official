package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单商品表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-11
 */
public interface SktOrderGoodsMapper extends BaseMapper<SktOrderGoods> {

    List<Map<String,Object>> appFindGoodById(Integer orderId);

    List<Map<String,Object>> appFindOrderMesById(Integer orderId);

    List<Map<String,Object>> selectOrderGoodsByOrderId(int orderId);

    Map<String,Object> selectOrderGoodsByGoodsId(Map<String, Object> map1);
}
