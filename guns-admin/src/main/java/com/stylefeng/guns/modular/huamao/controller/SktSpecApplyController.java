package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.huamao.model.SktSpecApplyDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.SktSpecApply;
import com.stylefeng.guns.modular.huamao.service.ISktSpecApplyService;

import java.util.List;

/**
 * 商品规格申请控制器
 *
 * @author py
 * @Date 2018-08-23 16:08:11
 */
@Controller
@RequestMapping("/sktSpecApply")
public class SktSpecApplyController extends BaseController {

    private String PREFIX = "/huamao/sktSpecApply/";

    @Autowired
    private ISktSpecApplyService sktSpecApplyService;

    /**
     * 跳转到商品规格申请首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktSpecApply.html";
    }

    /**
     * 跳转到添加商品规格申请
     */
    @RequestMapping("/sktSpecApply_add")
    public String sktSpecApplyAdd() {
        return PREFIX + "sktSpecApply_add.html";
    }

    /**
     * 跳转到修改商品规格申请
     */
    @RequestMapping("/sktSpecApply_update/{sktSpecApplyId}")
    public String sktSpecApplyUpdate(@PathVariable Integer sktSpecApplyId, Model model,SktSpecApplyDto sktSpecApplyDto) {
       sktSpecApplyDto.setId(sktSpecApplyId);

        SktSpecApplyDto sktSpecApplyDB = sktSpecApplyService.findSktSpecApplyById(sktSpecApplyDto);
        if(sktSpecApplyDB!=null){
            model.addAttribute("item",sktSpecApplyDB);
        }else{
            model.addAttribute("item",null);
        }
        return PREFIX + "sktSpecApply_edit.html";
    }

    /**
     * 获取商品规格申请列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktSpecApplyDto sktSpecApplyDto) {
        Page<SktSpecApplyDto> page = new PageFactory<SktSpecApplyDto>().defaultPage();
        List<SktSpecApplyDto> list = sktSpecApplyService.showSpecApplyInfo(page,sktSpecApplyDto);
        page.setRecords(list);
        return super.packForBT(page);
    }

    /**
     * 新增商品规格申请
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktSpecApply sktSpecApply) {
        sktSpecApplyService.insert(sktSpecApply);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品规格申请
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktSpecApplyId) {
        sktSpecApplyService.deleteById(sktSpecApplyId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品规格申请
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktSpecApply sktSpecApply) {
        sktSpecApplyService.updateById(sktSpecApply);
        return SUCCESS_TIP;
    }

    /**
     * 申请审核
     */
    @RequestMapping(value = "/update2")
    @ResponseBody
    public Object update2(SktSpecApply sktSpecApply) {
        // 发送者id为操作员id
        ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
        sktSpecApply.setStaffId(shiroUser.getId());
        return sktSpecApplyService.updateSktSpecApplyById(sktSpecApply);
        /*if(updateFlag){
            return SUCCESS;
        }else{
            return ERROR;
        }*/
    }

    /**
     * 商品规格申请详情
     */
    @RequestMapping(value = "/detail/{sktSpecApplyId}")
    @ResponseBody
    public Object detail(@PathVariable("sktSpecApplyId") Integer sktSpecApplyId) {
        return sktSpecApplyService.selectById(sktSpecApplyId);
    }
    /**
     * 商品规格申请详情
     */
    @RequestMapping(value = "/detail2/{sktSpecApplyId}")
    public Object detail2(@PathVariable("sktSpecApplyId") Integer sktSpecApplyId,Model model) {
        SktSpecApply sktSpecApply = sktSpecApplyService.selectById(sktSpecApplyId);
        model.addAttribute("item",sktSpecApply);
        return PREFIX + "sktSpecApply_edit2.html";
    }
}
