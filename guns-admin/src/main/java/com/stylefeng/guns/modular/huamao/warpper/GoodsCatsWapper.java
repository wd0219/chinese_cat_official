package com.stylefeng.guns.modular.huamao.warpper;

import java.util.List;
import java.util.Map;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;

public class GoodsCatsWapper extends BaseControllerWarpper {
	public GoodsCatsWapper(List<Map<String, Object>> list){
		super(list);
	}
	   public void warpTheMap(Map<String, Object> map) {
		   map.put("isShowName", ConstantFactory.me().getIsShowName((Integer)map.get("isShow")));
		   map.put("isFloorName", ConstantFactory.me().getIsFloorName((Integer)map.get("isFloor")));
	   }

}
