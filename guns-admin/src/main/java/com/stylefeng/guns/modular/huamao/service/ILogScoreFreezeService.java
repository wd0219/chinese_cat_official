package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreeze;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreezeList;

/**
 * <p>
 * 待发积分流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface ILogScoreFreezeService extends IService<LogScoreFreeze> {

	// 展示待发积分流水列表
	public List<LogScoreFreezeList> selectLogScoreFreezeAll(Page<LogScoreFreezeList> page, LogScoreFreezeList logScoreFreezeList);

}
