package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 商家账户表(包括线上和线下)
 * </p>
 *
 * @author gxz123
 * @since 2018-04-12
 */
@TableName("skt_account_shop")
public class AccountShop extends Model<AccountShop> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "accountId", type = IdType.AUTO)
	private Integer accountId;
	/**
	 * 会员ID
	 */
	private Integer userId;
	/**
	 * 商家ID
	 */
	private Integer shopId;
	/**
	 * 线上商城营业额
	 */
	private BigDecimal onlineTurnover;
	/**
	 * 线下商家营业额
	 */
	private BigDecimal offlineTurnover;
	/**
	 * 库存积分（商家专用）
	 */
	private BigDecimal stockScore;
	/**
	 * 冻结的积分(线上商家下单)
	 */
	private BigDecimal freezeStockScore;
	/**
	 * 累计充值库存积分
	 */
	private BigDecimal totalStockScore;
	/**
	 * 累计分发库存积分
	 */
	private BigDecimal totalGiveStockScore;
	/**
	 * 待发开元宝营业额
	 */
	private BigDecimal freezeKaiyuan;
	/**
	 * 门店获得开元宝支付的营业额
	 */
	private BigDecimal kaiyuanTurnover;
	/**
	 * 门店累计获得开元宝支付的营业额
	 */
	private BigDecimal totalKaiyuanTurnover;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 门店累计获得优惠券数量
	 *
	 */
	private BigDecimal totalShopping;

	public BigDecimal getTotalShopping() {
		return totalShopping;
	}

	public void setTotalShopping(BigDecimal totalShopping) {
		this.totalShopping = totalShopping;
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public BigDecimal getOnlineTurnover() {
		return onlineTurnover;
	}

	public void setOnlineTurnover(BigDecimal onlineTurnover) {
		this.onlineTurnover = onlineTurnover;
	}

	public BigDecimal getOfflineTurnover() {
		return offlineTurnover;
	}

	public void setOfflineTurnover(BigDecimal offlineTurnover) {
		this.offlineTurnover = offlineTurnover;
	}

	public BigDecimal getStockScore() {
		return stockScore;
	}

	public void setStockScore(BigDecimal stockScore) {
		this.stockScore = stockScore;
	}

	public BigDecimal getFreezeStockScore() {
		return freezeStockScore;
	}

	public void setFreezeStockScore(BigDecimal freezeStockScore) {
		this.freezeStockScore = freezeStockScore;
	}

	public BigDecimal getTotalStockScore() {
		return totalStockScore;
	}

	public void setTotalStockScore(BigDecimal totalStockScore) {
		this.totalStockScore = totalStockScore;
	}

	public BigDecimal getTotalGiveStockScore() {
		return totalGiveStockScore;
	}

	public void setTotalGiveStockScore(BigDecimal totalGiveStockScore) {
		this.totalGiveStockScore = totalGiveStockScore;
	}

	public BigDecimal getFreezeKaiyuan() {
		return freezeKaiyuan;
	}

	public void setFreezeKaiyuan(BigDecimal freezeKaiyuan) {
		this.freezeKaiyuan = freezeKaiyuan;
	}

	public BigDecimal getKaiyuanTurnover() {
		return kaiyuanTurnover;
	}

	public void setKaiyuanTurnover(BigDecimal kaiyuanTurnover) {
		this.kaiyuanTurnover = kaiyuanTurnover;
	}

	public BigDecimal getTotalKaiyuanTurnover() {
		return totalKaiyuanTurnover;
	}

	public void setTotalKaiyuanTurnover(BigDecimal totalKaiyuanTurnover) {
		this.totalKaiyuanTurnover = totalKaiyuanTurnover;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.accountId;
	}

	@Override
	public String toString() {
		return "AccountShop{" + "accountId=" + accountId + ", userId=" + userId + ", shopId=" + shopId
				+ ", onlineTurnover=" + onlineTurnover + ", offlineTurnover=" + offlineTurnover + ", stockScore="
				+ stockScore + ", freezeStockScore=" + freezeStockScore + ", totalStockScore=" + totalStockScore
				+ ", totalGiveStockScore=" + totalGiveStockScore + ", freezeKaiyuan=" + freezeKaiyuan
				+ ", kaiyuanTurnover=" + kaiyuanTurnover + ", totalKaiyuanTurnover=" + totalKaiyuanTurnover
				+ ", createTime=" + createTime + "}";
	}
}
