package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsDTO;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 代理公司表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
public interface ISktAgentsService extends IService<SktAgents> {
	/**
	 * 查询显示列表
	 * @param page 
	 * @param sktAgentsDTO
	 * @return
	 */
	public List<SktAgentsDTO> showSktAgentsIno(Page<SktAgentsDTO> page, SktAgentsDTO sktAgentsDTO);
	
	/**
	 * 查询用户名和电话 id
	 * @param nameOrPhone
	 * @return
	 */
	public Map<String, Object> selectNameOrPhone(String nameOrPhone);
	/**
	 * 重复代理公司
	 * @param sktAgents
	 * @return
	 */
	public List<SktAgents> selectHasAgents(SktAgents sktAgents);
}
