package com.stylefeng.guns.modular.huamao.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.common.GetOrderNum;
import com.stylefeng.guns.modular.huamao.dao.AccountMapper;
import com.stylefeng.guns.modular.huamao.dao.LogCashMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersRechargeMapper;
import com.stylefeng.guns.modular.huamao.model.Account;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;
import com.stylefeng.guns.modular.huamao.model.UsersRechargeDTO;
import com.stylefeng.guns.modular.huamao.service.IMessagesService;
import com.stylefeng.guns.modular.huamao.service.IUsersRechargeService;

/**
 * <p>
 * 用户充值表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
@Service
public class UsersRechargeServiceImpl extends ServiceImpl<UsersRechargeMapper, UsersRecharge> implements IUsersRechargeService {
	
	@Autowired
	private UsersRechargeMapper usersRechargeMapper;
	@Autowired
	private AccountMapper am;
	@Autowired
	private LogCashMapper logCashMapper;
	@Autowired
	private IMessagesService iMessagesService;
	@Override
	public List<UsersRechargeDTO> showUserDengDanInfo(@Param("page")Page<UsersRechargeDTO>page,
			@Param("usersRechargeDTO")UsersRechargeDTO usersRechargeDTO) {
		List<UsersRechargeDTO> list = usersRechargeMapper.showUserDengDanInfo(page,usersRechargeDTO);
		Integer total = usersRechargeMapper.showUserDengDanInfoCount(usersRechargeDTO);
		page.setTotal(total);
		return list;
	}

	//充值后，现金账户添加现金  需要传订单id
	@Transactional
	@Override
	public JSONObject rechargeAfter(UsersRecharge recharge,Integer userId) {
		JSONObject json = new JSONObject();
		//修改充值状态
		recharge.setStatus(1);
		Integer update1 = usersRechargeMapper.updateById(recharge);
		//查询充值订单信息
		recharge = usersRechargeMapper.selectById(recharge.getId());
		//实时查询账户余额
		Account account = am.findScoreById(recharge.getUserId());
		//生成现金流水
		this.insertLogCash(1, recharge.getUserId(), recharge.getUserId(), "7", account.getCash(), "用户充值", 1, recharge.getCash());
		//给用户账户添加现金，累计现金
		account.setCash(account.getCash().add(recharge.getCash()));
		account.setTotalCash(account.getTotalCash().add(recharge.getCash()));
		Integer update = am.updateByUserId3(account);
		json.put("code","01");
		json.put("msg","充值成功！");
		recharge.getOrderNo();
		List<Integer> usersId = new ArrayList<>();
		usersId.add(recharge.getUserId());
		iMessagesService.addMessages(1,userId, usersId,"您有一笔线下充值订单【"+recharge.getOrderNo()+"】审核已通过。",null);
		return json;
	}

	@Override
	@Transactional
	public boolean updateUserRecharge(UsersRecharge usersRecharge, Integer userId) {
		if(usersRecharge.getStatus() == 1 && usersRecharge.getStatus() != null){
			usersRecharge.setCheckTime(new Date());
			rechargeAfter(usersRecharge,userId);
		}
		Integer integer = usersRechargeMapper.updateById(usersRecharge);
		if (integer!=0 && ToolUtil.isEmpty(integer)){
			return  true;
		}
		return false;
	}

	/**  现金流水
	 * @param cashType
	 * @param fromId
	 * @param userId
	 * @param firstNum
	 * @param preCash
	 * @param remark
	 * @param type
	 * @param cash
	 * @return
	 */
	@Transactional
	public boolean insertLogCash(Integer cashType, Integer fromId, Integer userId , String firstNum, BigDecimal preCash, String remark, Integer type, BigDecimal cash){
		LogCash logCash=new LogCash();
		logCash.setCashType(cashType);
		logCash.setCreateTime(new Date());
		logCash.setDataFlag(1);
		logCash.setFromId(fromId);
		logCash.setUserId(userId);
		// 创建订单号
		SimpleDateFormat sfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String strDate = sfDate.format(new Date());
		String num = GetOrderNum.getRandom620(2);
		logCash.setOrderNo(firstNum+strDate + num);
		logCash.setPreCash(preCash);
		logCash.setRemark(remark);
		logCash.setType(type);
		logCash.setCash(cash);
		int flag=logCashMapper.insert(logCash);
		if(flag==1){
			return true;
		}else{
			return false;
		}
	}
}
