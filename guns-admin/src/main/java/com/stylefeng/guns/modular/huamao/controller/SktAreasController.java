package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.flowable.engine.impl.util.json.JSONString;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktAreas;
import com.stylefeng.guns.modular.huamao.service.ISktAreasService;
import com.stylefeng.guns.modular.huamao.warpper.AreasWapper;

/**
 * 地区管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 17:51:47
 */
@Controller
@RequestMapping("/sktAreas")
public class SktAreasController extends BaseController {

    private String PREFIX = "/huamao/sktAreas/";

    @Autowired
    private ISktAreasService sktAreasService;
    
    /**
     * 查询 返回json格式
     */
    @RequestMapping("/selectAll")
   @ResponseBody
    public JSONArray selectAll(Model model) {
    	List<SktAreas> sktAreas = sktAreasService.selectList(null);
    	List<JSONArray> list = new ArrayList<>();
    	JSONArray jSONArray=new JSONArray();
    	for (SktAreas sktArea : sktAreas) {
    		JSONObject json=new JSONObject();
    		json.put("item_code", sktArea.getAreaId()+"");
    		json.put("item_name", sktArea.getAreaName());
    		jSONArray.add(json); 		
		} 
    	return jSONArray;
    }
    /**
     * 跳转到地区管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktAreas.html";
    }

    /**
     * 跳转到添加地区管理
     */
    @RequestMapping("/sktAreas_add")
    public String sktAreasAdd() {
    	return PREFIX + "sktAreas_add.html";
    }

    /**
     * 跳转到修改地区管理
     */
    @RequestMapping("/sktAreas_update/{sktAreasId}")
    public String sktAreasUpdate(@PathVariable Integer sktAreasId, Model model) {
        SktAreas sktAreas = sktAreasService.selectById(sktAreasId);
        model.addAttribute("item",sktAreas);
        LogObjectHolder.me().set(sktAreas);
        return PREFIX + "sktAreas_edit.html";
    }
    /**
     * 跳转到添加子类地区
     */
    @RequestMapping("/sktAreas_addChild/{sktAreasId}")
    public String sktAreasAddChild(@PathVariable Integer sktAreasId, Model model) {
      
        model.addAttribute("sktAreasId",sktAreasId);
        LogObjectHolder.me().set(sktAreasId);
        return PREFIX + "sktAreas_addChild.html";
    }
    /**
     * 获取地区管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktAreas sktAreas) {
    	return sktAreasService.selectByName(sktAreas);
    }
    /**
     * 根据父id获取地区管理列表
     */
    @RequestMapping(value = "/list1")
    @ResponseBody
    public Object list1(@RequestParam Integer areaId) {
    	if(areaId==null ||"".equals(areaId)){
    		areaId=0;
    	}
        List<Map<String, Object>> areaList = sktAreasService.selectByParentId(areaId);
        return areaList;
        
        //  return new AreasWapper(areaList).warp();
    }
    
    /**
     * 根据父id获取地区管理列表返回是使用
     */
    @RequestMapping(value = "/list2")
    @ResponseBody
    public Object list2(@RequestParam Integer areaId) {
    	int ppId=0;
    	if(areaId!=0){
    		SktAreas ppArea = sktAreasService.selectById(areaId);
        	ppId=ppArea.getParentId();
    	}
    	
    	  List<Map<String, Object>> areaList = sktAreasService.selectByParentId(ppId);
    	  return areaList;
    	  //  return new AreasWapper(areaList).warp();
    }
    /**
     * 新增地区管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktAreas sktAreas) {
    	if(sktAreas.getParentId()==null || "".equals(sktAreas.getParentId())){
    		sktAreas.setParentId(0);
    		sktAreas.setAreaType(0);
    		sktAreas.setAreaSort(0);
    	}else{
    		SktAreas areas = sktAreasService.selectById(sktAreas.getParentId());
    		int areaType =areas.getAreaType()+1;
    		sktAreas.setAreaType(areaType);
    		int areaSort=sktAreasService.selectMaxAreaSort();
        	sktAreas.setAreaSort(areaSort+1);
    	}
    	if(sktAreas.getAreaKey()==null || "".equals(sktAreas.getAreaKey())){
    		sktAreas.setAreaKey("");
    	}
    	
    	sktAreas.setDataFlag(1);
    	sktAreas.setCreateTime(new Date());
        sktAreasService.insert(sktAreas);
        return SUCCESS_TIP;
    }

    /**
     * 删除地区管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam (value="sktAreasId",required=true)Integer sktAreasId) {
    	List<Map<String, Object>> list1 = sktAreasService.selectByParentId(sktAreasId);
    	if(list1!=null && list1.size()>0){
    		for(Map<String, Object> map1:list1){
    			List<Map<String, Object>> list2 = sktAreasService.selectByParentId((Integer)map1.get("areaId"));
	    		if(list2 !=null && list2.size()>0){
	    			sktAreasService.deleteByParentId((Integer)map1.get("areaId"));
	    		}
    		
    			sktAreasService.deleteById((Integer)map1.get("areaId"));
    		}
    	}
    	
    	
        sktAreasService.deleteById(sktAreasId);
        return SUCCESS_TIP;
    }

    /**
     * 修改地区管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktAreas sktAreas) {
        sktAreasService.updateById(sktAreas);
        return SUCCESS_TIP;
    }

    /**
     * 地区管理详情
     */
    @RequestMapping(value = "/detail/{sktAreasId}")
    @ResponseBody
    public Object detail(@PathVariable("sktAreasId") Integer sktAreasId) {
        return sktAreasService.selectById(sktAreasId);
    }
}
