package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.BrandsMapper;
import com.stylefeng.guns.modular.huamao.dao.CatBrandsMapper;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.BrandsCustem;
import com.stylefeng.guns.modular.huamao.model.CatBrands;
import com.stylefeng.guns.modular.huamao.service.IBrandsService;

/**
 * <p>
 * 品牌表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */

@Service
public class BrandsServiceImpl extends ServiceImpl<BrandsMapper, Brands> implements IBrandsService {
@Autowired
private CatBrandsMapper catBrandsMapper;
@Autowired
private BrandsMapper brandsMapper;
	@Override
	public List<Map<String, Object>> selectAll1(Page<Map<String,Object>>page,BrandsCustem brandsCustem) {
		 List<Map<String, Object>> list = brandsMapper.selectAll1(page,brandsCustem);
		 Integer total = brandsMapper.selectAllCount1(brandsCustem);
		 page.setTotal(total);
		 return list;
	}

	@Override
	@Transactional
	public boolean insertBrands(BrandsCustem brandsCustem) {
		// TODO Auto-generated method stub
		try {
			baseMapper.insert(brandsCustem);
			CatBrands catBrands=new CatBrands();
			catBrands.setBrandId(brandsCustem.getBrandId());
			catBrands.setCatId(brandsCustem.getGoods());
			catBrandsMapper.insert(catBrands);
			return true; 
		} catch (Exception e) {
			// TODO: handle exception
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return false;
		}
		
		
	}

	@Override
	public Map<String, Object> selectMapById(Integer brandsId) {
		// TODO Auto-generated method stub
		return baseMapper.selectMapById(brandsId);
	}

	@Override
	public List<Map<String, Object>> selectRecommend(BrandsCustem brandsCustem) {
		// TODO Auto-generated method stub
		return baseMapper.selectRecommend(brandsCustem);
	}

	@Override
	public List<Map<String, Object>> selectNotRecommend(BrandsCustem brandsCustem) {
		// TODO Auto-generated method stub
		return baseMapper.selectNotRecommend(brandsCustem);
	}

	@Override
	public int selectBrandsTotal() {
		// TODO Auto-generated method stub
		return baseMapper.selectBrandsTotal();
	}

}
