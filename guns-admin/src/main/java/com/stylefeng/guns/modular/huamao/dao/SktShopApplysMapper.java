package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商城商家开店申请表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-10
 */
public interface SktShopApplysMapper extends BaseMapper<SktShopApplys> {
	public List<MyDTO> shopApplysfindinfo(@Param("page") Page<MyDTO> page, @Param("myDto") MyDTO myDto);
	
	public List<Map<String,Object>> shopApplysfindup(MyDTO myDto);
	
	public void shopApplyupdate(MyDTO myDto);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();

    Integer shopApplysfindinfoCount(@Param("myDto") MyDTO myDto);
}
