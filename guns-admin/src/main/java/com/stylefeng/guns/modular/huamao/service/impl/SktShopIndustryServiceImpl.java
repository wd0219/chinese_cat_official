package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;
import com.stylefeng.guns.modular.huamao.dao.SktShopIndustryMapper;
import com.stylefeng.guns.modular.huamao.service.ISktShopIndustryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 行业管理表 服务实现类
 * </p>
 *
 * @author ck123123
 * @since 2018-04-19
 */
@Service
public class SktShopIndustryServiceImpl extends ServiceImpl<SktShopIndustryMapper, SktShopIndustry> implements ISktShopIndustryService {
	@Autowired
	private SktShopIndustryMapper shopIndustryMapper;
	@Override
	public List<SktShopIndustry> shopIndustryfindinfo(Page<SktShopIndustry> page, SktShopIndustry shopIndustry) {
		Integer total = shopIndustryMapper.shopIndustryfindinfoCount(shopIndustry);
		page.setTotal(total);
		List<SktShopIndustry> list = shopIndustryMapper.shopIndustryfindinfo(page,shopIndustry);
		return list;
	}
	@Override
	public List<SktShopIndustry> shopIndustryfindinfo2(SktShopIndustry shopIndustry) {
		List<SktShopIndustry> list = shopIndustryMapper.shopIndustryfindinfo2(shopIndustry);
		return list;
	}
}
