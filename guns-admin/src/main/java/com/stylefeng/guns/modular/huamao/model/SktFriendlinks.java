package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 友情连接表
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@TableName("skt_friendlinks")
public class SktFriendlinks extends Model<SktFriendlinks> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "friendlinkId", type = IdType.AUTO)
    private Integer friendlinkId;
    /**
     * 图标
     */
    private String friendlinkIco;
    /**
     * 名称
     */
    private String friendlinkName;
    /**
     * 网址
     */
    private String friendlinkUrl;
    /**
     * 排序号
     */
    private Integer friendlinkSort;
    /**
     * 删除标志
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getFriendlinkId() {
        return friendlinkId;
    }

    public void setFriendlinkId(Integer friendlinkId) {
        this.friendlinkId = friendlinkId;
    }

    public String getFriendlinkIco() {
        return friendlinkIco;
    }

    public void setFriendlinkIco(String friendlinkIco) {
        this.friendlinkIco = friendlinkIco;
    }

    public String getFriendlinkName() {
        return friendlinkName;
    }

    public void setFriendlinkName(String friendlinkName) {
        this.friendlinkName = friendlinkName;
    }

    public String getFriendlinkUrl() {
        return friendlinkUrl;
    }

    public void setFriendlinkUrl(String friendlinkUrl) {
        this.friendlinkUrl = friendlinkUrl;
    }

    public Integer getFriendlinkSort() {
        return friendlinkSort;
    }

    public void setFriendlinkSort(Integer friendlinkSort) {
        this.friendlinkSort = friendlinkSort;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.friendlinkId;
    }

    @Override
    public String toString() {
        return "SktFriendlinks{" +
        "friendlinkId=" + friendlinkId +
        ", friendlinkIco=" + friendlinkIco +
        ", friendlinkName=" + friendlinkName +
        ", friendlinkUrl=" + friendlinkUrl +
        ", friendlinkSort=" + friendlinkSort +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
