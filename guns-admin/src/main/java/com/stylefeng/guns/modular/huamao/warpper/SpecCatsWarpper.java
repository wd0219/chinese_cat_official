package com.stylefeng.guns.modular.huamao.warpper;

import java.util.List;
import java.util.Map;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.common.constant.factory.ConstantFactory;

public class SpecCatsWarpper extends BaseControllerWarpper {
	public SpecCatsWarpper(List<Map<String, Object>> list){
		super(list);
	}
	   public void warpTheMap(Map<String, Object> map) {
		   map.put("isShowName", ConstantFactory.me().getSpecIsShowName((Integer)map.get("isShow")));
		   map.put("isAllowImgName", ConstantFactory.me().getIsAllowImg((Integer)map.get("isAllowImg")));
		 
	   }

}
