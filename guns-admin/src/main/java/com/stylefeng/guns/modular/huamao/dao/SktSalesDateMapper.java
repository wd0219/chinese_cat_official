package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktSalesDate;

/**
 * <p>
 * 商家每日营业额统计表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
public interface SktSalesDateMapper extends BaseMapper<SktSalesDate> {
	
	/*
	 * 展示全部
	 * */
	public List<SktSalesDate> sktSalesDatefindAll(@Param("page")Page<SktSalesDate> page, 
			@Param("sktSalesDate")SktSalesDate sktSalesDate);

	public Integer sktSalesDatefindAllCount(@Param("sktSalesDate")SktSalesDate sktSalesDate);
}
