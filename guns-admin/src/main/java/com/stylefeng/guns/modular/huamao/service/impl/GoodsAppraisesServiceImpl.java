package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsAppraisesMapper;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesCustm;
import com.stylefeng.guns.modular.huamao.service.IGoodsAppraisesService;

/**
 * <p>
 * 商品评价表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsAppraisesServiceImpl extends ServiceImpl<GoodsAppraisesMapper, GoodsAppraises> implements IGoodsAppraisesService {

	@Override
	public List<Map<String, Object>> selectAll(Page<Map<String,Object>> page, GoodsAppraisesCustm goodsAppraisesCustm) {
		List<Map<String, Object>> list = baseMapper.selectAll(page,goodsAppraisesCustm);
		Integer total = baseMapper.selectAllCount(goodsAppraisesCustm);
		page.setTotal(total);
		return list; 
	}

	@Override
	public int selectGoodsAppraisesTotal() {
		// TODO Auto-generated method stub
		return baseMapper.selectGoodsAppraisesTotal();
	}

}
