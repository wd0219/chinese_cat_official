package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * 
 * 平台每日总对账数据表返回
 * 
 *
 * @author gxz123
 * @since 2018-04-14
 */
public class CheckResultList extends CheckResult {

	/**
	 * 查询时的开始时间
	 */
	private Integer beginTime;
	/**
	 * 查询时的结束时间
	 */
	private Integer endTime;



	public Integer getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Integer beginTime) {
		this.beginTime = beginTime;
	}

	public Integer getEndTime() {
		return endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}

}
