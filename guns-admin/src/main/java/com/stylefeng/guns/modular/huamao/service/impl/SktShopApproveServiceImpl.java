package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import com.stylefeng.guns.modular.huamao.dao.SktShopApproveMapper;
import com.stylefeng.guns.modular.huamao.service.ISktShopApproveService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业认证表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@Service
public class SktShopApproveServiceImpl extends ServiceImpl<SktShopApproveMapper, SktShopApprove> implements ISktShopApproveService {

}
