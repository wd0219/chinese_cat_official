package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.model.UsersUpgradeDTO;

/**
 * <p>
 * 用户升级角色订单表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
public interface IUsersUpgradeService extends IService<UsersUpgrade> {
	/**
	 * 显示用户升级列表
	 * @param page 
	 * @param usersUpgradeDTO
	 * @return
	 */
	public List<UsersUpgradeDTO> showUsersShengJiInfo(Page<UsersUpgradeDTO> page, UsersUpgradeDTO usersUpgradeDTO);
	/**
	 * 更新升级状态
	 * @param orderId
	 * @param status
	 * @param preRole
	 * @param afterRole
	 */
	public String updateUpgradeStatus(Integer orderId,Integer status,Integer preRole, Integer afterRole);
}
