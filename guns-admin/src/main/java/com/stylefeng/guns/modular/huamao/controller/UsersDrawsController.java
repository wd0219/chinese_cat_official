package com.stylefeng.guns.modular.huamao.controller;

import java.math.BigDecimal;
import java.util.*;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.huamao.service.ISktHuamaobtLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.huamao.model.UsersDraws;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsList;
import com.stylefeng.guns.modular.huamao.service.IUsersDrawsService;

/**
 * 用户提现控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 19:02:46
 */
@Controller
@RequestMapping("/usersDraws")
public class UsersDrawsController extends BaseController {

	private String PREFIX = "/huamao/usersDraws/";

	@Autowired
	private IUsersDrawsService usersDrawsService;

	@Autowired
	private ISktHuamaobtLogService sktHuamaobtLogService;

	/**
	 * 跳转到用户提现首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "usersDraws.html";
	}

	/**
	 * 跳转到添加用户提现
	 */
	@RequestMapping("/usersDraws_add")
	public String usersDrawsAdd() {
		return PREFIX + "usersDraws_add.html";
	}

	/**
	 * 跳转到修改用户提现
	 */
	@RequestMapping("/usersDraws_update/{usersDrawsId}")
	public String usersDrawsUpdate(@PathVariable Integer usersDrawsId, Model model) {
		UsersDraws usersDraws = usersDrawsService.selectById(usersDrawsId);
		model.addAttribute("item", usersDraws);
		LogObjectHolder.me().set(usersDraws);
		return PREFIX + "usersDraws_edit.html";
	}

	/**
	 * 获取用户提现列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(UsersDrawsList usersDrawsList) {
		Page<UsersDrawsList> page = new PageFactory<UsersDrawsList>().defaultPage();
		List<UsersDrawsList> usersDrawsLists = usersDrawsService.selectUsersDrawsAll(page,usersDrawsList);
		page.setRecords(usersDrawsLists);
		return super.packForBT(page);
		// return usersDrawsService.selectList(null);
	}

	/**
	 * 新增用户提现
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(UsersDraws usersDraws) {
		usersDrawsService.insert(usersDraws);
		return SUCCESS_TIP;
	}

	/**
	 * 删除用户提现
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer usersDrawsId) {
		usersDrawsService.deleteById(usersDrawsId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改用户提现
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(UsersDraws usersDraws) {
		usersDrawsService.updateById(usersDraws);
		return SUCCESS_TIP;
	}

	/**
	 * 用户提现详情
	 */
	@RequestMapping(value = "/detail/{usersDrawsId}")
	@ResponseBody
	public Object detail(@PathVariable("usersDrawsId") Integer usersDrawsId) {
		return usersDrawsService.selectById(usersDrawsId);
	}

	/**
	 * 审核通过
	 */
	@RequestMapping("/usersDrawsPass")
	public String usersDrawsPass(@RequestParam String drawId, Model model) {
		// 将获取到id存入model为修改做准备
		model.addAttribute("drawId", drawId);
		return PREFIX + "usersDraws_Pass.html";
	}

	/**
	 * 审核拒绝
	 */
	@RequestMapping("/usersDrawsRefuse")
	public String usersDrawsRefuse(@RequestParam String drawId, Model model) {
		// 将获取到id存入model为修改做准备
		model.addAttribute("drawId", drawId);
		return PREFIX + "usersDraws_Refuse.html";
	}
	/**
	 * 审核拒绝后的修改数据
	 */
	@RequestMapping("/updateSatus")
	@ResponseBody
	public Object updateSatus(UsersDrawsList usersDrawsList) {
		// 获取操作人员id
		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
		// 将审核状态改为审核拒绝
		usersDrawsList.setCashSatus(-1);
		// 审核时间
		usersDrawsList.setCheckTime(new Date());
		// 设置审核员id
		usersDrawsList.setCheckStaffId(shiroUser.getId());
		// 将审核状态改为审核失败
		usersDrawsService.updateSatus(usersDrawsList);
		return SUCCESS_TIP;
	}

	/**
	 * 订单处理中处理
	 */
	@RequestMapping("/usersDrawsInHand")
	public String usersDrawsInHand(@RequestParam String drawId, Model model) {
		// 将获取到id存入model为修改做准备
		model.addAttribute("drawId", drawId);
		return PREFIX + "usersDraws_InHand.html";
	}

	/**
	 * 订单处理中处理
	 */
	@RequestMapping("/usersDrawsDefeated")
	public String usersDrawsDefeated(@RequestParam String drawId, Model model) {
		// 将获取到id存入model为修改做准备
		model.addAttribute("drawId", drawId);
		return PREFIX + "usersDraws_Defeated.html";
	}

	/**
	 * 线下打款
	 */
	@RequestMapping("/usersDrawsMoney")
	@ResponseBody
	public Object usersDrawsMoney(UsersDrawsList usersDrawsList) {
		// 获取操作人员id
		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
		// 将审核状态改为线下打款
		usersDrawsList.setCashSatus(4);
		// 审核时间
		usersDrawsList.setCheckTime(new Date());
		// 设置审核员id
		usersDrawsList.setCheckStaffId(shiroUser.getId());
		// 修改审核状态
		usersDrawsService.updateSatus(usersDrawsList);
		return SUCCESS_TIP;
	}
	/**
	 * 批量审核前的跳转页面
	 */
	@RequestMapping("/batchRefuse")
	public String batchRefuse() {
		// 跳转到批量审核页
		return PREFIX + "usersDraws_batchRefuse.html";
	}
	/**
	 * 批量审核拒绝
	 */
	@RequestMapping("/batchEdit")
	@ResponseBody
	public String batchEdit(UsersDrawsList usersDrawsList) {
		// 获取操作人员id
		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
		// 将审核状态改为审核拒绝
		usersDrawsList.setCashSatus(-1);
		// 审核时间
		usersDrawsList.setCheckTime(new Date());
		// 设置审核员id
		usersDrawsList.setCheckStaffId(shiroUser.getId());
		//是否手动处理
		usersDrawsList.setSpecialType(2);
		Integer update = usersDrawsService.batchEdit(usersDrawsList);
		if (update == 0) {
			return "false";
		}
		return "true";
	}

	//转化猫币
	@RequestMapping("/recharge")
	@ResponseBody
	public Object recharge(Integer userId, BigDecimal money, Integer drawId) {
		Integer amount = Integer.parseInt(money.toString());
		Object recharge = sktHuamaobtLogService.recharge(userId, amount,drawId);

		if("success".equals(recharge.toString())){
			// 获取操作人员id
			ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
			UsersDrawsList usersDrawsList = new UsersDrawsList();
			usersDrawsList.setDrawId(drawId);
			// 审核时间
			usersDrawsList.setCheckTime(new Date());
			// 设置审核员id
			usersDrawsList.setCheckStaffId(shiroUser.getId());
			usersDrawsList.setCashSatus(3);
			usersDrawsService.updateSatus(usersDrawsList);
			return "success";
		}else{
			return recharge.toString();
		}

	}

	/**
	 * 华宝批量转换
	 */
	@RequestMapping("/batchRecharge")
	@ResponseBody
	public Object batchRecharge(){

		/**
		 * 测试数据
		 */
		List<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
		Map<String,Object> map = new HashMap<String, Object>();
		Map<String,Object> map1 = new HashMap<String, Object>();
		Map<String,Object> map2 = new HashMap<String, Object>();

		map.put("userId",1);
		map.put("amount",10);

		map1.put("userId",2);
		map1.put("amount",20);

		map2.put("userId",3);
		map2.put("amount",30);

		list.add(map);
		list.add(map1);
		list.add(map2);

		try {
			usersDrawsService.batchRecharge(list);
			return "成功";
		}catch (Exception e){
			return "失败";
		}


	}



}
