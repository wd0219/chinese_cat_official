package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 视频类型表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
@TableName("skt_class_type")
public class SktClassType extends Model<SktClassType> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "typeId", type = IdType.AUTO)
    private Integer typeId;
    /**
     * 类型名称
     */
    private String typeName;
    /**
     * 创建时间
     */
    private Date creatTime;
    /**
     * 类型图片
(预留字段)
     */
    private String typeUrl;


    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public String getTypeUrl() {
        return typeUrl;
    }

    public void setTypeUrl(String typeUrl) {
        this.typeUrl = typeUrl;
    }

    @Override
    protected Serializable pkVal() {
        return this.typeId;
    }

    @Override
    public String toString() {
        return "SktClassType{" +
        "typeId=" + typeId +
        ", typeName=" + typeName +
        ", creatTime=" + creatTime +
        ", typeUrl=" + typeUrl +
        "}";
    }
}
