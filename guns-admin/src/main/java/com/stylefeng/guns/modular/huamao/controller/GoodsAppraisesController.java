package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesCustm;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.service.IGoodsAppraisesService;
import com.stylefeng.guns.modular.huamao.warpper.GoodsAppraisesWapper;

/**
 * 商品评价控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:30:06
 */
@Controller
@RequestMapping("/goodsAppraises")
public class GoodsAppraisesController extends BaseController {

    private String PREFIX = "/huamao/goodsAppraises/";

    @Autowired
    private IGoodsAppraisesService goodsAppraisesService;

    /**
     * 跳转到商品评价首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "goodsAppraises.html";
    }

    /**
     * 跳转到添加商品评价
     */
    @RequestMapping("/goodsAppraises_add")
    public String goodsAppraisesAdd() {
        return PREFIX + "goodsAppraises_add.html";
    }

    /**
     * 跳转到修改商品评价
     */
    @RequestMapping("/goodsAppraises_update/{goodsAppraisesId}")
    public String goodsAppraisesUpdate(@PathVariable Integer goodsAppraisesId, Model model) {
        GoodsAppraises goodsAppraises = goodsAppraisesService.selectById(goodsAppraisesId);
        model.addAttribute("item",goodsAppraises);
        LogObjectHolder.me().set(goodsAppraises);
        return PREFIX + "goodsAppraises_edit.html";
    }

    /**
     * 获取商品评价列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(GoodsAppraisesCustm goodsAppraisesCustm) {
    	Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().defaultPage();
        List<Map<String, Object>> list = goodsAppraisesService.selectAll(page,goodsAppraisesCustm);
        page.setRecords(list);
		return super.packForBT(page);
    }

    /**
     * 新增商品评价
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(GoodsAppraises goodsAppraises) {
        goodsAppraisesService.insert(goodsAppraises);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品评价
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer goodsAppraisesId) {
        goodsAppraisesService.deleteById(goodsAppraisesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品评价
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(GoodsAppraises goodsAppraises) {
        goodsAppraisesService.updateById(goodsAppraises);
        return SUCCESS_TIP;
    }

    /**
     * 商品评价详情
     */
    @RequestMapping(value = "/detail/{goodsAppraisesId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsAppraisesId") Integer goodsAppraisesId) {
        return goodsAppraisesService.selectById(goodsAppraisesId);
    }
}
