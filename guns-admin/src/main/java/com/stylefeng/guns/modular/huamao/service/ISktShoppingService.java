package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktShopping;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShoppingDTO;

import java.util.List;

/**
 * <p>
 * 购物券表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-09-29
 */
public interface ISktShoppingService extends IService<SktShopping> {
    public List<SktShoppingDTO> selectAll(Page<SktShoppingDTO> page, SktShoppingDTO sktShoppingDTO);

}
