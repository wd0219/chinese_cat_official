package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;
import com.stylefeng.guns.modular.huamao.model.LogStockscoreList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 库存积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface LogStockscoreMapper extends BaseMapper<LogStockscore> {

	// 库存积分流水表展示
	List<LogStockscoreList> selectLogStockScoreAll(@Param("page")Page<LogStockscoreList> page,@Param("logStockscoreList") LogStockscoreList logStockscoreList);

    Integer selectLogStockScoreAllCount(@Param("logStockscoreList") LogStockscoreList logStockscoreList);
}
