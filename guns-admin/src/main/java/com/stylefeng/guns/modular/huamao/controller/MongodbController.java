package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.mongodb.BasicDBObject;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.support.HttpKit;
import com.stylefeng.guns.modular.huamao.mongodb.OperationLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoDbUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/mongodb")
public class MongodbController  extends BaseController {
    private String PREFIX = "/system/log/";
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 跳转到导航管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "interface_log.html";
    }
    @RequestMapping(value = "/getName")
    @ResponseBody
    public Object getName(){
        String name = mongoTemplate.getDb().getName();
        return name;
    }
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object getLog(OperationLog operationLog) throws Exception{
        BasicDBObject a =new BasicDBObject();

//       Integer currentPage = 1;
//       Integer pageSize = 15;
     //   Query query = new Query();
   //     List<OperationLog> logs = mongoTemplate.findAll(OperationLog.class);
        Criteria criteria=Criteria.where("logtype").ne("").ne(null);
        if (!"".equals(operationLog.getLogTypeId())&&operationLog!=null ){
            criteria.and("logTypeId").is(operationLog.getLogTypeId());
        }
        if (!"".equals(operationLog.getLogname()) && operationLog.getLogname()!=null ){
            criteria.and("logname").is(operationLog.getLogname());
        }
        if (!"".equals(operationLog.getBeginTime()) && operationLog.getBeginTime()!=null
                && !"".equals(operationLog.getEndTime()) && operationLog.getEndTime()!=null ){
            criteria.and("createtime").gte(operationLog.getBeginTime()).lte(operationLog.getEndTime());
        }else {
            if (!"".equals(operationLog.getBeginTime()) && operationLog.getBeginTime()!=null ){
                criteria.and("createtime").gte(operationLog.getBeginTime());
            }
            if (!"".equals(operationLog.getEndTime()) && operationLog.getEndTime()!=null ){
                criteria.and("createtime").lte(operationLog.getEndTime());
            }
        }

        Query query=new Query(criteria);
        query.with(new Sort(Sort.Direction.DESC,"createtime"));
        HttpServletRequest request = HttpKit.getRequest();
        int limit = Integer.valueOf(request.getParameter("limit"));     //每页多少条数据
        int offset = Integer.valueOf(request.getParameter("offset"));   //每页的偏移量(本页当前有多少条)
        query.skip((offset-1)*limit);
        query.limit(limit);
        Page<OperationLog> page = new PageFactory<OperationLog>().defaultPage();
        List<OperationLog> logs = mongoTemplate.find(query,OperationLog.class);

        Long count = mongoTemplate.count(query, OperationLog.class);
        page.setRecords(logs);
        page.setTotal(Integer.parseInt(count.toString()));
        return super.packForBT(page);
     //  return logs;
    }

    @RequestMapping("/delLog")
    @ResponseBody
    public  void delLog(){
        Query query=new Query();
        mongoTemplate.remove(query,OperationLog.class);
    }
}
