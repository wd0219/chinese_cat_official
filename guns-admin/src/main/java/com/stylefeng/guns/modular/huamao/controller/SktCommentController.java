package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.modular.huamao.model.SktCommentDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.SktComment;
import com.stylefeng.guns.modular.huamao.service.ISktCommentService;

import java.util.List;

/**
 * 视屏评论控制器
 *
 * @author fengshuonan
 * @Date 2018-07-26 09:54:21
 */
@Controller
@RequestMapping("/sktComment")
public class SktCommentController extends BaseController {

    private String PREFIX = "/huamao/sktComment/";

    @Autowired
    private ISktCommentService sktCommentService;

    /**
     * 跳转到视屏评论首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktComment.html";
    }

    /**
     * 跳转到添加视屏评论
     */
    @RequestMapping("/sktComment_add")
    public String sktCommentAdd() {
        return PREFIX + "sktComment_add.html";
    }

    /**
     * 跳转到修改视屏评论
     */
    @RequestMapping("/sktComment_update/{sktCommentId}")
    public String sktCommentUpdate(@PathVariable Integer sktCommentId, Model model) {
        SktComment sktComment = sktCommentService.selectById(sktCommentId);
        model.addAttribute("item",sktComment);
        LogObjectHolder.me().set(sktComment);
        return PREFIX + "sktComment_edit.html";
    }

    /**
     * 获取视屏评论列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktCommentDTO sktCommentDTO) {
        Page<SktCommentDTO> page = new PageFactory<SktCommentDTO>().defaultPage();
        List<SktCommentDTO> sktComments = sktCommentService.selectSktCommentAll(page, sktCommentDTO);
        page.setRecords(sktComments);
        return super.packForBT(page);
      //  return sktCommentService.selectList(null);
    }

    /**
     * 新增视屏评论
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktComment sktComment) {
        sktCommentService.insert(sktComment);
        return SUCCESS_TIP;
    }

    /**
     * 删除视屏评论
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktCommentId) {
        sktCommentService.deleteById(sktCommentId);
        return SUCCESS_TIP;
    }

    /**
     * 修改视屏评论
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktComment sktComment) {
        sktCommentService.updateById(sktComment);
        return SUCCESS_TIP;
    }

    /**
     * 视屏评论详情
     */
    @RequestMapping(value = "/detail/{sktCommentId}")
    @ResponseBody
    public Object detail(@PathVariable("sktCommentId") Integer sktCommentId) {
        return sktCommentService.selectById(sktCommentId);
    }
}
