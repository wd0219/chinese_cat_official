package com.stylefeng.guns.modular.system.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.system.dao.LoginLogMapper;
import com.stylefeng.guns.modular.system.model.LoginLog;
import com.stylefeng.guns.modular.system.model.OperationLog;
import com.stylefeng.guns.modular.system.service.ILoginLogService;

/**
 * <p>
 * 登录记录 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-02-22
 */
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLog> implements ILoginLogService {

    @Override
    public List<Map<String, Object>> getLoginLogs(Page<OperationLog> page, String beginTime, String endTime, String logName, String orderByField, boolean asc) {
        Integer count = baseMapper.getLoginLogsCount(page,beginTime, endTime, logName, orderByField, asc);
        page.setTotal(count);
        return this.baseMapper.getLoginLogs(page, beginTime, endTime, logName, orderByField, asc);
    }

	@Override
	public Map<String, Object> selectLastLoginLogs(LoginLog loginLog) {
		// TODO Auto-generated method stub
		return baseMapper.selectLastLoginLogs(loginLog);
	}
	
	/*private static Logger logger = LoggerFactory.getLogger(LoginLogServiceImpl.class);
    private static String dbPath = "E:/GeoLite2-City/GeoLite2-City.mmdb";

    private static DatabaseReader reader;

    @Autowired
    private Environment env;

    @PostConstruct
    public void init() {
        try {
            String path = env.getProperty("geolite2.city.db.path");
            if (StringUtils.isNotBlank(path)) {
                dbPath = path;
            }
            File database = new File(dbPath);
            reader = new DatabaseReader.Builder(database).build();
        } catch (Exception e) {
            logger.error("IP地址服务初始化异常:" + e.getMessage(), e);
        }
    }

	@Override
    public String getSubdivision(String ipAddress){
        try {
            CityResponse response = reader.city(InetAddress.getByName(ipAddress));
             return response.getCountry().getNames().get("zh-CN")+
            		response.getMostSpecificSubdivision().getNames().get("zh-CN")+
            		response.getCity().getNames().get("zh-CN");
        }catch (Exception e){
            logger.error("根据IP[{}]获取省份失败:{}", ipAddress, e.getMessage());
            return null;
        }
    }*/


	
}
