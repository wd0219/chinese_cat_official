package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnover;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnoverList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 开元宝营业额流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface LogKaiyuanTurnoverMapper extends BaseMapper<LogKaiyuanTurnover> {

	// 开元宝营业额流水表返回
	List<LogKaiyuanTurnoverList> selectLogKaiyuanTurnoverAll(@Param("page") Page<LogKaiyuanTurnoverList> page, @Param("logKaiyuanTurnoverList") LogKaiyuanTurnoverList logKaiyuanTurnoverList);

    Integer selectLogKaiyuanTurnoverAllCount(@Param("logKaiyuanTurnoverList") LogKaiyuanTurnoverList logKaiyuanTurnoverList);
}
