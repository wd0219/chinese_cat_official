package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustryDTO;
import com.stylefeng.guns.modular.huamao.model.SktStaffs;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商城职员表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-25
 */
public interface ISktStaffsService extends IService<SktStaffs> {
	
	public List<SktShopIndustryDTO> sktStaffsfindAll(Page<SktShopIndustryDTO> page, SktShopIndustryDTO sktShopIndustryDTO);
	
	public List<SktShopIndustryDTO> sktStaffsfindup(SktShopIndustryDTO sktShopIndustryDTO);
	
	public void sktStaffsupdate(SktShopIndustryDTO sktShopIndustryDTO);
	
	public String sktStaffsup1(SktShopIndustryDTO sktShopIndustryDTO, Integer shiroUserId);
	
	
}
