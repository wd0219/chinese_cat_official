package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogThirdpay;
import com.stylefeng.guns.modular.huamao.model.LogThirdpayList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 第三方支付记录表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface LogThirdpayMapper extends BaseMapper<LogThirdpay> {

	// 第三方支付记录表展示
	List<LogThirdpayList> selectLogThirdpayAll(@Param("page")Page<LogThirdpayList> page, @Param("logThirdpayList") LogThirdpayList logThirdpayList);

    Integer selectLogThirdpayAllCount(@Param("logThirdpayList") LogThirdpayList logThirdpayList);
}
