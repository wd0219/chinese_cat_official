package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktFavorites;
import com.stylefeng.guns.modular.huamao.dao.SktFavoritesMapper;
import com.stylefeng.guns.modular.huamao.service.ISktFavoritesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 关注(商品/店铺)表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@Service
public class SktFavoritesServiceImpl extends ServiceImpl<SktFavoritesMapper, SktFavorites> implements ISktFavoritesService {

}
