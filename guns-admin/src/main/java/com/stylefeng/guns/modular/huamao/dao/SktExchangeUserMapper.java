package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktExchangeUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author caody123
 * @since 2018-06-23
 */
public interface SktExchangeUserMapper extends BaseMapper<SktExchangeUser> {

}
