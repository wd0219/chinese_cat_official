package com.stylefeng.guns.modular.huamao.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.collections.iterators.ObjectArrayListIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.modular.huamao.common.GetOrderNum;
import com.stylefeng.guns.modular.huamao.model.SupplementList;
import com.stylefeng.guns.modular.huamao.service.ISupplementService;

/**
 * 手动补单控制层
 * 
 * @author gxz
 *
 */
@Controller
@RequestMapping("/supplement")
public class SupplementController extends BaseController {

	private String PREFIX = "/huamao/supplement/";

	@Autowired
	private ISupplementService supplementService;

	/**
	 * 跳转到手动补单首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "supplement.html";
	}

	// 校验用户名或手机号是否存在
	@RequestMapping("/checkUser")
	@ResponseBody
	public Object checkUser(SupplementList supplementList) {
		// 校验：返回实体类中保存了用户名、手机号
		SupplementList checkUser = supplementService.checkUser(supplementList);
		if (checkUser == null) {
			return "no";
		}
		return checkUser;
	}

	// 手动补单操作
	@PostMapping("/add")
	@ResponseBody
	public Object add(SupplementList supplementList) {
		// 获取操作人员id
		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
		// 创建订单号
		SimpleDateFormat sfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String strDate = sfDate.format(new Date());
		String num = GetOrderNum.getRandom620(2);
		supplementList.setOrderNo("8" + strDate + num);
		// 设置操作人员ID
		supplementList.setStaffId(shiroUser.getId());
		// 创建订单时间
		supplementList.setCreateTime(new Date());
		// 添加记录
		Map<String,Object> map = supplementService.add(supplementList);
		return map;
	}
}
