package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 积分流水表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@TableName("skt_log_score")
public class LogScore extends Model<LogScore> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 积分类型 1商城消费获取 2线下商家消费获得 3购买库存积分获得 4下线消费获得 5下线营业额获得 6特别奖励 7用户升级奖励 8下线升级奖励 9升级代理 10下线升级代理奖励 11分红获得 12待发积分账户转入 14后台手动增加 15待发特别奖励转入 16购买牌匾奖励 17下线购买牌匾奖励 18消费退款 31转换开元宝减少 32后台手动减少 33购买自营商品
     */
    private Integer type;
    /**
     * 发起用户ID 0为平台
     */
    private Integer fromId;
    /**
     * 目标用户ID
     */
    private Integer userId;
    /**
     * 对应订单号
     */
    private String orderNo;
    /**
     * 操作前的金额
     */
    private BigDecimal preScore;
    /**
     * 流水标志 -1减少 1增加
     */
    private Integer scoreType;
    /**
     * 金额
     */
    private BigDecimal score;
    /**
     * 备注
     */
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreScore() {
        return preScore;
    }

    public void setPreScore(BigDecimal preScore) {
        this.preScore = preScore;
    }

    public Integer getScoreType() {
        return scoreType;
    }

    public void setScoreType(Integer scoreType) {
        this.scoreType = scoreType;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogScore{" +
        "id=" + id +
        ", type=" + type +
        ", fromId=" + fromId +
        ", userId=" + userId +
        ", orderNo=" + orderNo +
        ", preScore=" + preScore +
        ", scoreType=" + scoreType +
        ", score=" + score +
        ", remark=" + remark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
