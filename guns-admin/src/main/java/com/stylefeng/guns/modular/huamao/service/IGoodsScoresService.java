package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.GoodsScores;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品评分表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IGoodsScoresService extends IService<GoodsScores> {

}
