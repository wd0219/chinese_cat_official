package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.BrandsCustem;
import com.stylefeng.guns.modular.huamao.service.IBrandsService;
import com.stylefeng.guns.modular.huamao.service.ICatBrandsService;

/**
 * 品牌控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:24:40
 */
@Controller
@RequestMapping("/brands")
public class BrandsController extends BaseController {

    private String PREFIX = "/huamao/brands/";

    @Autowired
    private IBrandsService brandsService;
    @Autowired
    private ICatBrandsService catBrandsService;

    /**
     * 跳转到品牌首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "brands.html";
    }
    /**
     * 跳转到品牌推荐
     */
    @RequestMapping("/brandsRecom")
    public String brandsRecom() {
        return PREFIX + "brands_recom.html";
    }

    /**
     * 跳转到添加品牌
     */
    @RequestMapping("/brands_add")
    public String brandsAdd() {
        return PREFIX + "brands_add.html";
    }

    /**
     * 跳转到修改品牌
     */
    @RequestMapping("/brands_update/{brandsId}")
    public String brandsUpdate(@PathVariable Integer brandsId, Model model) {
      //  Brands brands = brandsService.selectById(brandsId);
    	Map<String, Object> brands = brandsService.selectMapById(brandsId);
        Object brandImg = brands.get("brandImg");
        String allUrl = UrlUtils.getAllUrl(brandImg.toString());
        brands.put("brandImg",allUrl);
        model.addAttribute("item",brands);
//        CatBrands catBrands=new CatBrands();
//        int goodsCatId1=catBrandsService.getGoodsCatId();
//        int goodsCatId2=0;
//        int goodsCatId3=0;
//        if(goodsCatId1!=0){//最后级分类id
//            GoodsCats goodsCats1 = goodsCatsService.selectById(goodsCatId1);
//            goodsCatId2=goodsCats1.getParentId();
//            if(goodsCatId2!=0){//第二级分类id
//                GoodsCats goodsCats2 = goodsCatsService.selectById(goodsCatId2);
//                goodsCatId3=goodsCats2.getParentId();//三级分类id
//            }
//        }
//        model.addAttribute("item",attributes);
//        model.addAttribute("goodsCatId2",goodsCatId2);
//        model.addAttribute("goodsCatId3",goodsCatId3);
//        model.addAttribute("item",attributes);
        LogObjectHolder.me().set(brands);
        return PREFIX + "brands_edit.html";
    }

    /**
     * 获取品牌列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(BrandsCustem brandsCustem) {
    	Page<Map<String, Object>> page = new PageFactory<Map<String, Object>>().defaultPage();
        List<Map<String, Object>> list = brandsService.selectAll1(page,brandsCustem);
        page.setRecords(list);
		return super.packForBT(page);
    }
    /**
     * 获取品牌推荐列表
     */
    @RequestMapping(value = "/selectRecommend")
    @ResponseBody
    public Object selectRecommend(BrandsCustem brandsCustem) {
    	
        return brandsService.selectRecommend(brandsCustem);
    }
    /**
     * 获取品牌未推荐列表
     */
    @RequestMapping(value = "/selectNotRecommend")
    @ResponseBody
    public Object selectNotRecommend(BrandsCustem brandsCustem) {
    	
        return brandsService.selectNotRecommend(brandsCustem);
    }
    /**
     * 新增品牌
     */
   
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(BrandsCustem brandsCustem) {
    	Date d=new Date();
    	brandsCustem.setCreateTime(d);
        brandsService.insertBrands(brandsCustem);
        return SUCCESS_TIP;
    }

    /**
     * 删除品牌
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer brandsId) {
        brandsService.deleteById(brandsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改品牌
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Brands brands) {
        brandsService.updateById(brands);
        return SUCCESS_TIP;
    }

    /**
     * 品牌详情
     */
    @RequestMapping(value = "/detail/{brandsId}")
    @ResponseBody
    public Object detail(@PathVariable("brandsId") Integer brandsId) {
        return brandsService.selectById(brandsId);
    }
}
