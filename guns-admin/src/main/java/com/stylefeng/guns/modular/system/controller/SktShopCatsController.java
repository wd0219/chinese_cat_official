package com.stylefeng.guns.modular.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktShopCats;
import com.stylefeng.guns.modular.system.service.ISktShopCatsService;

/**
 * 店铺分类表控制器
 *
 * @author fengshuonan
 * @Date 2018-05-29 17:28:10
 */
@Controller
@RequestMapping("/sktShopCats")
public class SktShopCatsController extends BaseController {

    private String PREFIX = "/system/sktShopCats/";

    @Autowired
    private ISktShopCatsService sktShopCatsService;

    /**
     * 跳转到店铺分类表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopCats.html";
    }

    /**
     * 跳转到添加店铺分类表
     */
    @RequestMapping("/sktShopCats_add")
    public String sktShopCatsAdd() {
        return PREFIX + "sktShopCats_add.html";
    }

    /**
     * 跳转到修改店铺分类表
     */
    @RequestMapping("/sktShopCats_update/{sktShopCatsId}")
    public String sktShopCatsUpdate(@PathVariable Integer sktShopCatsId, Model model) {
        SktShopCats sktShopCats = sktShopCatsService.selectById(sktShopCatsId);
        model.addAttribute("item",sktShopCats);
        LogObjectHolder.me().set(sktShopCats);
        return PREFIX + "sktShopCats_edit.html";
    }

    /**
     * 获取店铺分类表列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktShopCatsService.selectList(null);
    }

    /**
     * 新增店铺分类表
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopCats sktShopCats) {
        sktShopCatsService.insert(sktShopCats);
        return SUCCESS_TIP;
    }

    /**
     * 删除店铺分类表
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopCatsId) {
        sktShopCatsService.deleteById(sktShopCatsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改店铺分类表
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopCats sktShopCats) {
        sktShopCatsService.updateById(sktShopCats);
        return SUCCESS_TIP;
    }

    /**
     * 店铺分类表详情
     */
    @RequestMapping(value = "/detail/{sktShopCatsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopCatsId") Integer sktShopCatsId) {
        return sktShopCatsService.selectById(sktShopCatsId);
    }
}
