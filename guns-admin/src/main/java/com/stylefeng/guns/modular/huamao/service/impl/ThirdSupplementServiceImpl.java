package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.ThirdSupplementMapper;
import com.stylefeng.guns.modular.huamao.model.ThirdSupplementList;
import com.stylefeng.guns.modular.huamao.service.IThirdSupplementService;

/**
 * 第三方支付补单记录服务层
 * 
 * @author gxz
 *
 */
@Service
public class ThirdSupplementServiceImpl extends ServiceImpl<ThirdSupplementMapper, ThirdSupplementList>
		implements IThirdSupplementService {

	@Autowired
	private ThirdSupplementMapper tsm;

	// 第三方支付补单记录展示
	@Override
	public List<ThirdSupplementList> selectThirdSupplementAll(ThirdSupplementList thirdSupplementList) {
		return tsm.selectThirdSupplementAll(thirdSupplementList);
	}

}
