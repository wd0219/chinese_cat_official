package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktFavorites;
import com.stylefeng.guns.modular.huamao.service.ISktFavoritesService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2018-05-10 16:37:40
 */
@Controller
@RequestMapping("/sktFavorites")
public class SktFavoritesController extends BaseController {

    private String PREFIX = "/huamao/sktFavorites/";

    @Autowired
    private ISktFavoritesService sktFavoritesService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktFavorites.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/sktFavorites_add")
    public String sktFavoritesAdd() {
        return PREFIX + "sktFavorites_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/sktFavorites_update/{sktFavoritesId}")
    public String sktFavoritesUpdate(@PathVariable Integer sktFavoritesId, Model model) {
        SktFavorites sktFavorites = sktFavoritesService.selectById(sktFavoritesId);
        model.addAttribute("item",sktFavorites);
        LogObjectHolder.me().set(sktFavorites);
        return PREFIX + "sktFavorites_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktFavoritesService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktFavorites sktFavorites) {
        sktFavoritesService.insert(sktFavorites);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktFavoritesId) {
        sktFavoritesService.deleteById(sktFavoritesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktFavorites sktFavorites) {
        sktFavoritesService.updateById(sktFavorites);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{sktFavoritesId}")
    @ResponseBody
    public Object detail(@PathVariable("sktFavoritesId") Integer sktFavoritesId) {
        return sktFavoritesService.selectById(sktFavoritesId);
    }
}
