package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgent;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgentList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanAgentService;

import java.util.List;

/**
 * 代理公司华宝记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-17 09:50:12
 */
@Controller
@RequestMapping("/logKaiyuanAgent")
public class LogKaiyuanAgentController extends BaseController {

	private String PREFIX = "/huamao/logKaiyuanAgent/";

	@Autowired
	private ILogKaiyuanAgentService logKaiyuanAgentService;

	/**
	 * 跳转到代理公司华宝记录首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "logKaiyuanAgent.html";
	}

	/**
	 * 跳转到添加代理公司华宝记录
	 */
	@RequestMapping("/logKaiyuanAgent_add")
	public String logKaiyuanAgentAdd() {
		return PREFIX + "logKaiyuanAgent_add.html";
	}

	/**
	 * 跳转到修改代理公司华宝记录
	 */
	@RequestMapping("/logKaiyuanAgent_update/{logKaiyuanAgentId}")
	public String logKaiyuanAgentUpdate(@PathVariable Integer logKaiyuanAgentId, Model model) {
		LogKaiyuanAgent logKaiyuanAgent = logKaiyuanAgentService.selectById(logKaiyuanAgentId);
		model.addAttribute("item", logKaiyuanAgent);
		LogObjectHolder.me().set(logKaiyuanAgent);
		return PREFIX + "logKaiyuanAgent_edit.html";
	}

	/**
	 * 获取代理公司华宝记录列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(LogKaiyuanAgentList logKaiyuanAgentList) {
		Page<LogKaiyuanAgentList> page = new PageFactory<LogKaiyuanAgentList>().defaultPage();
		List<LogKaiyuanAgentList> logKaiyuanAgentLists = logKaiyuanAgentService.selectLogKaiyuanAgentAll(page,logKaiyuanAgentList);
		page.setRecords(logKaiyuanAgentLists);
		return super.packForBT(page);
		// return logKaiyuanAgentService.selectList(null);
	}

	/**
	 * 新增代理公司华宝记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogKaiyuanAgent logKaiyuanAgent) {
		logKaiyuanAgentService.insert(logKaiyuanAgent);
		return SUCCESS_TIP;
	}

	/**
	 * 删除代理公司华宝记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logKaiyuanAgentId) {
		logKaiyuanAgentService.deleteById(logKaiyuanAgentId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改代理公司华宝记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogKaiyuanAgent logKaiyuanAgent) {
		logKaiyuanAgentService.updateById(logKaiyuanAgent);
		return SUCCESS_TIP;
	}

	/**
	 * 代理公司华宝记录详情
	 */
	@RequestMapping(value = "/detail/{logKaiyuanAgentId}")
	@ResponseBody
	public Object detail(@PathVariable("logKaiyuanAgentId") Integer logKaiyuanAgentId) {
		return logKaiyuanAgentService.selectById(logKaiyuanAgentId);
	}
}
