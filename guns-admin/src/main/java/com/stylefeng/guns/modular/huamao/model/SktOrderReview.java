package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商城订单审核表
 * </p>
 *
 * @author gxz123
 * @since 2018-09-19
 */
@TableName("skt_order_review")
public class SktOrderReview extends Model<SktOrderReview> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户Id
     */
    private Integer userId;
    /**
     * 内部流水号
     */
    private String orderunique;
    /**
     * 第三方订单号
     */
    private String outTradeNo;
    /**
     * 订单状态: -1拒绝 0未付款 1审核通过
     */
    private Integer orderStatus;
    /**
     * 是否支付 0未支付，1支付
     */
    private Integer isPay;
    /**
     * 支付方式支付方式 1微信 2支付宝 3快捷
     */
    private Integer payType;
    /**
     * 商城订单总金额（含运费）
     */
    private BigDecimal payMoney;
    /**
     * 购买商品支付的购物券
     */
    private BigDecimal payShopping;
    /**
     * 拒绝理由
     */
    private String checkRemark;
    /**
     * 订单有效标志（1有效  -1无效）
     */
    private Integer dataFlag;
    /**
     * 下单时间
     */
    private Date createTime;
    /**
     * 审核人id
     */
    private Integer staffId;
    /**
     * 审核时间
     */
    private Date auditDatetime;

    /**
     * 支付账号
     */
    private String payName;

    public String getPayName() {
        return payName;
    }

    public void setPayName(String payName) {
        this.payName = payName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderunique() {
        return orderunique;
    }

    public void setOrderunique(String orderunique) {
        this.orderunique = orderunique;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getIsPay() {
        return isPay;
    }

    public void setIsPay(Integer isPay) {
        this.isPay = isPay;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public BigDecimal getPayShopping() {
        return payShopping;
    }

    public void setPayShopping(BigDecimal payShopping) {
        this.payShopping = payShopping;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public Date getAuditDatetime() {
        return auditDatetime;
    }

    public void setAuditDatetime(Date auditDatetime) {
        this.auditDatetime = auditDatetime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktOrderReview{" +
        "id=" + id +
        ", userId=" + userId +
        ", orderunique=" + orderunique +
        ", outTradeNo=" + outTradeNo +
        ", orderStatus=" + orderStatus +
        ", isPay=" + isPay +
        ", payType=" + payType +
        ", payMoney=" + payMoney +
        ", payShopping=" + payShopping +
        ", checkRemark=" + checkRemark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", staffId=" + staffId +
        ", auditDatetime=" + auditDatetime +
        "}";
    }
}
