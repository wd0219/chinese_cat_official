package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStockscore;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商家购买库存积分订单表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
public interface ISktOrdersStockscoreService extends IService<SktOrdersStockscore> {
	
	public List<SktOrdersStockscore> sktOrdersStockscorefindall(Page<SktOrdersStockscore> page, SktOrdersStockscore ordersStockscore);
}
