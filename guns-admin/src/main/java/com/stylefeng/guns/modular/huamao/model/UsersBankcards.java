package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 用户的银行卡表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
@TableName("skt_users_bankcards")
public class UsersBankcards extends Model<UsersBankcards> {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	@TableId(value = "bankCardId", type = IdType.AUTO)
	private Integer bankCardId;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 是否是默认银行卡 1默认 2不是
	 */
	private Integer type;
	/**
	 * 省id
	 */
	private Integer provinceId;
	/**
	 * 市id
	 */
	private Integer cityId;
	/**
	 * 区县id
	 */
	private Integer areaId;
	/**
	 * 银行ID
	 */
	private Integer bankId;
	/**
	 * 开户行
	 */
	private String accArea;
	/**
	 * 银行卡号
	 */
	private String accNo;
	/**
	 * 银行预留手机号码
	 */
	private String phone;
	/**
	 * 有效标志
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;

	public Integer getBankCardId() {
		return bankCardId;
	}

	public void setBankCardId(Integer bankCardId) {
		this.bankCardId = bankCardId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	public String getAccArea() {
		return accArea;
	}

	public void setAccArea(String accArea) {
		this.accArea = accArea;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.bankCardId;
	}

	@Override
	public String toString() {
		return "UsersBankcards{" + "bankCardId=" + bankCardId + ", userId=" + userId + ", type=" + type
				+ ", provinceId=" + provinceId + ", cityId=" + cityId + ", areaId=" + areaId + ", bankId=" + bankId
				+ ", accArea=" + accArea + ", accNo=" + accNo + ", phone=" + phone + ", dataFlag=" + dataFlag
				+ ", createTime=" + createTime + "}";
	}
}
