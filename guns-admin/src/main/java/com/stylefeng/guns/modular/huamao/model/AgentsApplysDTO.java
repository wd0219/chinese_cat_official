package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class AgentsApplysDTO extends AgentsApplys {
	
	/**
	 * 企业名称
	 */
	private String agentName;
	private String trueName;
	/**
	 * 代理类行
	 */
	private Integer alevel;
	private String salevel;
	private String stype;
	/**
	 * 区域
	 */
	private String areas;
	/**
	 * 用户昵称
	 */
	private String loginName;
	/**
	 * 股东类型
	 */
	private Integer isAgent;
	/**
	 * 省id
	 */
	private Integer province;
	/**
	 * 市id
	 */
	private Integer citys;
	/**
	 * 区域id
	 */
	private Integer county;
	/**
	 * 用户类型
	 */
	private Integer userType;
	
    public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getCitys() {
		return citys;
	}

	public void setCitys(Integer citys) {
		this.citys = citys;
	}

	public Integer getCounty() {
		return county;
	}

	public void setCounty(Integer county) {
		this.county = county;
	}

	public Integer getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Integer isAgent) {
        this.isAgent = isAgent;
    }

    public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Integer getAlevel() {
		return alevel;
	}

	public void setAlevel(Integer alevel) {
		this.alevel = alevel;
	}

	public String getAreas() {
		return areas;
	}

	public void setAreas(String areas) {
		this.areas = areas;
	}

	public String getSalevel() {
		return salevel;
	}

	public void setSalevel(String salevel) {
		this.salevel = salevel;
	}

	public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	
}
