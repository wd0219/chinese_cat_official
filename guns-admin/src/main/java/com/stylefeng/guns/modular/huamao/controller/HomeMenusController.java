package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.HomeMenus;
import com.stylefeng.guns.modular.huamao.service.IHomeMenusService;

/**
 * 前台菜单控制器
 *
 * @author fengshuonan
 * @Date 2018-04-28 17:29:05
 */
@Controller
@RequestMapping("/homeMenus")
public class HomeMenusController extends BaseController {

    private String PREFIX = "/huamao/homeMenus/";

    @Autowired
    private IHomeMenusService homeMenusService;

    /**
     * 跳转到前台菜单首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "homeMenus.html";
    }

    /**
     * 跳转到添加前台菜单
     */
    @RequestMapping("/homeMenus_add")
    public String homeMenusAdd() {
        return PREFIX + "homeMenus_add.html";
    }

    /**
     * 跳转到添加前台菜单子菜单
     */
    @RequestMapping("/homeMenusChild_add/{menuId}")
    public String homeChildMenusAdd(@PathVariable Integer menuId, Model model) {
    	 model.addAttribute("menuId",menuId);
         LogObjectHolder.me().set(menuId);
    	
        return PREFIX + "homeMenusChild_add.html";
    }
    /**
     * 跳转到修改前台菜单
     */
    @RequestMapping("/homeMenus_update/{homeMenusId}")
    public String homeMenusUpdate(@PathVariable Integer homeMenusId, Model model) {
        HomeMenus homeMenus = homeMenusService.selectById(homeMenusId);
        model.addAttribute("item",homeMenus);
        LogObjectHolder.me().set(homeMenus);
        return PREFIX + "homeMenus_edit.html";
    }

    /**
     * 获取前台菜单列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(HomeMenus homeMenus) {
        Page<Map<String,Object>> page  = new PageFactory<Map<String,Object>>().defaultPage();
        List<Map<String, Object>> mapList = homeMenusService.selectHomeMenus(page, homeMenus);
        page.setRecords(mapList);
        return	super.packForBT(page);
      //  return homeMenusService.selectList(null);
    }

    /**
     * 新增前台菜单
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(HomeMenus homeMenus) {
    	if(homeMenus.getParentId()==null ||"".equals(homeMenus.getParentId()) ){
    		homeMenus.setParentId(0);
    	}
    	
    	homeMenus.setCreateTime(new Date());
        homeMenusService.insert(homeMenus);
        return SUCCESS_TIP;
    }

    /**
     * 删除前台菜单
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer homeMenusId) {
        homeMenusService.deleteById(homeMenusId);
        return SUCCESS_TIP;
    }

    /**
     * 修改前台菜单
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(HomeMenus homeMenus) {
        homeMenusService.updateById(homeMenus);
        return SUCCESS_TIP;
    }

    /**
     * 前台菜单详情
     */
    @RequestMapping(value = "/detail/{homeMenusId}")
    @ResponseBody
    public Object detail(@PathVariable("homeMenusId") Integer homeMenusId) {
        return homeMenusService.selectById(homeMenusId);
    }
}
