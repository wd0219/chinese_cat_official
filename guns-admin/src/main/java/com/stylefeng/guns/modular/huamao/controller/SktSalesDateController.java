package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktSalesDate;
import com.stylefeng.guns.modular.huamao.service.ISktSalesDateService;

/**
 * 商家每日营业额统计控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 19:31:33
 */
@Controller
@RequestMapping("/sktSalesDate")
public class SktSalesDateController extends BaseController {

    private String PREFIX = "/huamao/sktSalesDate/";

    @Autowired
    private ISktSalesDateService sktSalesDateService;

    /**
     * 跳转到商家每日营业额统计首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktSalesDate.html";
    }

    /**
     * 跳转到添加商家每日营业额统计
     */
    @RequestMapping("/sktSalesDate_add")
    public String sktSalesDateAdd() {
        return PREFIX + "sktSalesDate_add.html";
    }

    /**
     * 跳转到修改商家每日营业额统计
     */
    @RequestMapping("/sktSalesDate_update/{sktSalesDateId}")
    public String sktSalesDateUpdate(@PathVariable Integer sktSalesDateId, Model model) {
        SktSalesDate sktSalesDate = sktSalesDateService.selectById(sktSalesDateId);
        model.addAttribute("item",sktSalesDate);
        LogObjectHolder.me().set(sktSalesDate);
        return PREFIX + "sktSalesDate_edit.html";
    }

    /**
     * 获取商家每日营业额统计列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktSalesDate sktSalesDate) {
       Page<SktSalesDate> page = new PageFactory<SktSalesDate>().defaultPage();
       List<SktSalesDate> list = sktSalesDateService.sktSalesDatefindAll(page,sktSalesDate);
       page.setRecords(list);
	   return super.packForBT(page);
    }

    /**
     * 新增商家每日营业额统计
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktSalesDate sktSalesDate) {
        sktSalesDateService.insert(sktSalesDate);
        return SUCCESS_TIP;
    }

    /**
     * 删除商家每日营业额统计
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktSalesDateId) {
        sktSalesDateService.deleteById(sktSalesDateId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商家每日营业额统计
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktSalesDate sktSalesDate) {
        sktSalesDateService.updateById(sktSalesDate);
        return SUCCESS_TIP;
    }

    /**
     * 商家每日营业额统计详情
     */
    @RequestMapping(value = "/detail/{sktSalesDateId}")
    @ResponseBody
    public Object detail(@PathVariable("sktSalesDateId") Integer sktSalesDateId) {
        return sktSalesDateService.selectById(sktSalesDateId);
    }
}
