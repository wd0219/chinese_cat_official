package com.stylefeng.guns.modular.huamao.service;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktLogOperates;
import com.stylefeng.guns.modular.huamao.model.SktLogOperatesDto;

/**
 * <p>
 * 操作记录表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-27
 */
public interface ISktLogOperatesService extends IService<SktLogOperates> {
	/**
	 * an
	 * @param sktLogOperatesDto
	 * @return
	 */
	public List<SktLogOperatesDto> selectListDate(Page<SktLogOperatesDto> page, SktLogOperatesDto sktLogOperatesDto);

}
