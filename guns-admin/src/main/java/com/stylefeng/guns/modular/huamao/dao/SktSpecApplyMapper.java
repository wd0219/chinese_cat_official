package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktSpecApply;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktSpecApplyDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品规则申请表 Mapper 接口
 * </p>
 *
 * @author py
 * @since 2018-08-23
 */
public interface SktSpecApplyMapper extends BaseMapper<SktSpecApply> {
    /**
     * 页面显示
     * @param page
     * @param sktSpecApplyDto
     * @return
     */
    public List<SktSpecApplyDto> showSpecApplyInfo (@Param("page")Page<SktSpecApplyDto> page,
                                                 @Param("sktSpecApplyDto")SktSpecApplyDto sktSpecApplyDto);
    public Integer showSpecApplyInfoCount(@Param("sktSpecApplyDto")SktSpecApplyDto sktSpecApplyDto);

    public SktSpecApplyDto findSktSpecApplyById(@Param("sktSpecApplyDto")SktSpecApplyDto sktSpecApplyDto);

}
