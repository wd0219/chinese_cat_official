package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户的银行卡表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public class UsersBankcardsList extends UsersBankcards {

	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 昵称
	 */
	private String loginName;
	/**
	 * 真名
	 */
	private String trueName;
	/**
	 * 银行简称
	 */
	private String bankName;

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

}
