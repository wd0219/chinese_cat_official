package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 线上商城订单表
 * </p>
 *
 * @author caody123
 * @since 2018-04-14
 */
@TableName("skt_orders")
public class SktOrders extends Model<SktOrders> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "orderId", type = IdType.AUTO)
    private Integer orderId;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 用户Id
     */
    private Integer userId;
    /**
     * 订单状态:-3退款 -2未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
     */
    private Integer orderStatus;
    /**
     * 是否支付
     */
    private Integer isPay;
    /**
     * 是否订单已完结
     */
    private Integer isClosed;
    /**
     * 商品总金额
     */
    private BigDecimal goodsMoney;
    /**
     * 总运费
     */
    private BigDecimal deliverMoney;
    /**
     * 订单总金额 (goodsMoney+deliverMoney 或 realMoney+cash+kaiyuan)
     */
    private BigDecimal totalMoney;
    /**
     * 支付方式支付方式 1:线下现金支付 2 现金账户支付 3开元宝支付 4第三方支付 5混合支付 6开元宝营业额
     */
    private Integer payType;
    /**
     * 实际支付金额 通过支付公司现金支付的金额
     */
    private BigDecimal realMoney;
    /**
     * 平台现金账户金额
     */
    private BigDecimal cash;
    /**
     * 开元宝金额 1个开元宝=1分钱
     */
    private BigDecimal kaiyuan;
    /**
     * 使用开元宝支付的手续费和税率
     */
    private BigDecimal kaiyuanFee;
    /**
     * 自营店铺商品支付的积分
     */
    private Integer payScore;
    /**
     * 最后一级区域ID
     */
    private Integer areaId;
    /**
     * 区域ID路径
     */
    private String areaIdPath;
    /**
     * 收货人名称
     */
    private String userName;
    /**
     * 收货人地址
     */
    private String userAddress;
    /**
     * 收件人手机
     */
    private String userPhone;
    /**
     * 是否需要发票
     */
    private Integer isInvoice;
    /**
     * 发票抬头
     */
    private String invoiceClient;
    /**
     * 订单备注
     */
    private String orderRemarks;
    /**
     * 是否退款
     */
    private Integer isRefund;
    /**
     * 是否点评
     */
    private Integer isAppraise;
    /**
     * 取消原因ID
     */
    private Integer cancelReason;
    /**
     * 订单流水号
     */
    private String orderunique;
    /**
     * 收货时间
     */
    private Date receiveTime;
    /**
     * 发货时间
     */
    private Date deliveryTime;
    /**
     * 付款时间
     */
    private Date paymentTime;
    /**
     * 快递公司ID
     */
    private Integer expressId;
    /**
     * 快递号
     */
    private String expressNo;
    /**
     * 订单有效标志
     */
    private Integer dataFlag;
    /**
     * 下单时间
     */
    private Date createTime;
    /**
     * 批处理标示符：1未处理 2已处理 9处理中
     */
    private Integer batFlag;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getIsPay() {
        return isPay;
    }

    public void setIsPay(Integer isPay) {
        this.isPay = isPay;
    }

    public Integer getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Integer isClosed) {
        this.isClosed = isClosed;
    }

    public BigDecimal getGoodsMoney() {
        return goodsMoney;
    }

    public void setGoodsMoney(BigDecimal goodsMoney) {
        this.goodsMoney = goodsMoney;
    }

    public BigDecimal getDeliverMoney() {
        return deliverMoney;
    }

    public void setDeliverMoney(BigDecimal deliverMoney) {
        this.deliverMoney = deliverMoney;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getKaiyuanFee() {
        return kaiyuanFee;
    }

    public void setKaiyuanFee(BigDecimal kaiyuanFee) {
        this.kaiyuanFee = kaiyuanFee;
    }

    public Integer getPayScore() {
        return payScore;
    }

    public void setPayScore(Integer payScore) {
        this.payScore = payScore;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaIdPath() {
        return areaIdPath;
    }

    public void setAreaIdPath(String areaIdPath) {
        this.areaIdPath = areaIdPath;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Integer getIsInvoice() {
        return isInvoice;
    }

    public void setIsInvoice(Integer isInvoice) {
        this.isInvoice = isInvoice;
    }

    public String getInvoiceClient() {
        return invoiceClient;
    }

    public void setInvoiceClient(String invoiceClient) {
        this.invoiceClient = invoiceClient;
    }

    public String getOrderRemarks() {
        return orderRemarks;
    }

    public void setOrderRemarks(String orderRemarks) {
        this.orderRemarks = orderRemarks;
    }

    public Integer getIsRefund() {
        return isRefund;
    }

    public void setIsRefund(Integer isRefund) {
        this.isRefund = isRefund;
    }

    public Integer getIsAppraise() {
        return isAppraise;
    }

    public void setIsAppraise(Integer isAppraise) {
        this.isAppraise = isAppraise;
    }

    public Integer getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(Integer cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getOrderunique() {
        return orderunique;
    }

    public void setOrderunique(String orderunique) {
        this.orderunique = orderunique;
    }

    public Date getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Date receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Integer getExpressId() {
        return expressId;
    }

    public void setExpressId(Integer expressId) {
        this.expressId = expressId;
    }

    public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getBatFlag() {
        return batFlag;
    }

    public void setBatFlag(Integer batFlag) {
        this.batFlag = batFlag;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

    @Override
    public String toString() {
        return "SktOrders{" +
        "orderId=" + orderId +
        ", orderNo=" + orderNo +
        ", shopId=" + shopId +
        ", userId=" + userId +
        ", orderStatus=" + orderStatus +
        ", isPay=" + isPay +
        ", isClosed=" + isClosed +
        ", goodsMoney=" + goodsMoney +
        ", deliverMoney=" + deliverMoney +
        ", totalMoney=" + totalMoney +
        ", payType=" + payType +
        ", realMoney=" + realMoney +
        ", cash=" + cash +
        ", kaiyuan=" + kaiyuan +
        ", kaiyuanFee=" + kaiyuanFee +
        ", payScore=" + payScore +
        ", areaId=" + areaId +
        ", areaIdPath=" + areaIdPath +
        ", userName=" + userName +
        ", userAddress=" + userAddress +
        ", userPhone=" + userPhone +
        ", isInvoice=" + isInvoice +
        ", invoiceClient=" + invoiceClient +
        ", orderRemarks=" + orderRemarks +
        ", isRefund=" + isRefund +
        ", isAppraise=" + isAppraise +
        ", cancelReason=" + cancelReason +
        ", orderunique=" + orderunique +
        ", receiveTime=" + receiveTime +
        ", deliveryTime=" + deliveryTime +
        ", paymentTime=" + paymentTime +
        ", expressId=" + expressId +
        ", expressNo=" + expressNo +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", batFlag=" + batFlag +
        "}";
    }
}
