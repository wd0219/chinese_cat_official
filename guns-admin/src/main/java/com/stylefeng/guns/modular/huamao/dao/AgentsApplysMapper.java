package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.AgentsApplys;
import com.stylefeng.guns.modular.huamao.model.AgentsApplysDTO;

/**
 * <p>
 * 代理公司股东申请表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-17
 */
public interface AgentsApplysMapper extends BaseMapper<AgentsApplys> {
	/**
	 * 查询列表
	 * @param page 
	 * @param agentsApplysDTO
	 * @return
	 */
	public List<AgentsApplysDTO> showDaiLiShenQingInfo (@Param(value="page")Page<AgentsApplysDTO> page, 
			@Param(value="agentsApplysDTO")AgentsApplysDTO agentsApplysDTO);
	public Integer showDaiLiShenQingInfoCount(	@Param(value="agentsApplysDTO")AgentsApplysDTO agentsApplysDTO);

    public List<AgentsApplys> getAgentsApplysSevenAgo(Map<String, Object> map);

	public AgentsApplysDTO selectAddInfo(@Param(value="shaId") Integer shaId);

}
