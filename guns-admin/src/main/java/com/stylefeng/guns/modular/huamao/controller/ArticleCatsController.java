package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.ArticleCats;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.service.IArticleCatsService;
import com.stylefeng.guns.modular.huamao.warpper.ArticleCatsWarpper;

/**
 * 文章分类控制器
 *
 * @author fengshuonan
 * @Date 2018-04-26 15:42:06
 */
@Controller
@RequestMapping("/articleCats")
public class ArticleCatsController extends BaseController {

    private String PREFIX = "/huamao/articleCats/";

    @Autowired
    private IArticleCatsService articleCatsService;

    /**
     * 跳转到文章分类首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "articleCats.html";
    }

    /**
     * 跳转到添加文章分类
     */
    @RequestMapping("/articleCats_add")
    public String articleCatsAdd() {
        return PREFIX + "articleCats_add.html";
    }
    /**
     * 跳转到添加子类文章分类
     */
    @RequestMapping("/articleChildCats_add/{catId}")
    public String articleChildCatsAdd(@PathVariable Integer catId, Model model) {
    	 model.addAttribute("catId",catId);
    	
        return PREFIX + "articleCatsChild_add.html";
    }
    
    
    
    /**
     * 跳转到修改文章分类
     */
    @RequestMapping("/articleCats_update/{articleCatsId}")
    public String articleCatsUpdate(@PathVariable Integer articleCatsId, Model model) {
        ArticleCats articleCats = articleCatsService.selectById(articleCatsId);
        model.addAttribute("item",articleCats);
        LogObjectHolder.me().set(articleCats);
        return PREFIX + "articleCats_edit.html";
    }

    /**
     * 获取文章分类列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
    	List<Map<String, Object>> list = articleCatsService.selectAll();
    	return  list;
    //	return new ArticleCatsWarpper(list).warp();
    }
    /**
     * 根据父id获取文章分类列表
     */
    @RequestMapping(value = "/selectByParentId")
    @ResponseBody
    public Object selectByParentId(int parentId) {
    	JSONArray jSONArray=new JSONArray();
    	List<Map<String, Object>> list1 = articleCatsService.selectByParentId(parentId);
    	if(list1!=null && list1.size()>0  ){
    		for (Map<String,Object> map1:list1) {
        		JSONObject json=new JSONObject();
        		json.put("catId", map1.get("catId"));
        		json.put("catName", map1.get("catName"));
        		jSONArray.add(json);	
    		}
    	}
		return jSONArray;
    }
   
    /**
     * 
     * 新增文章分类
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(ArticleCats articleCats) {
    	if(articleCats.getParentId()==null || "".equals(articleCats.getParentId())){
    		articleCats.setParentId(0);
    	}else{
    		ArticleCats articleCats1=articleCatsService.selectById(articleCats.getParentId());
    		articleCats.setCatType(articleCats1.getCatType());
    	
    	}
    	articleCats.setDataFlag(1);
    	articleCats.setCreateTime(new Date());
        articleCatsService.insert(articleCats);
        return SUCCESS_TIP;
    }

    /**
     * 删除文章分类
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer articleCatsId) {
    	List<Map<String, Object>> list1 = articleCatsService.selectByParentId(articleCatsId);
    	if(list1!=null && list1.size()>0){
    		for(Map<String,Object> map1:list1 ){
    			List<Map<String, Object>> list2 = articleCatsService.selectByParentId((Integer)map1.get("catId"));
    			if(list2!=null && list2.size()>0){
    				articleCatsService.deleteByParentId((Integer)map1.get("catId"));
    			}
    			articleCatsService.deleteById((Integer)map1.get("catId"));
    		}
    		
    	}
    	articleCatsService.deleteById(articleCatsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改文章分类
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ArticleCats articleCats) {
        articleCatsService.updateById(articleCats);
        return SUCCESS_TIP;
    }

    /**
     * 文章分类详情
     */
    @RequestMapping(value = "/detail/{articleCatsId}")
    @ResponseBody
    public Object detail(@PathVariable("articleCatsId") Integer articleCatsId) {
        return articleCatsService.selectById(articleCatsId);
    }
}
