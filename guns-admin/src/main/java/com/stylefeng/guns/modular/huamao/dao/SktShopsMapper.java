package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.model.SktShops2;


import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商家信息表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
public interface SktShopsMapper extends BaseMapper<SktShops> {
	
	public List<SktShops> sktShopsfindAll(SktShops sktShops);
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int getTotal();
	/*
	 * 店铺街展示店铺
	 * */
	public List<Map<String, Object>> findShopsAll();
	/*
	 * 店铺街展示商品推荐
	 * */
	public List<Map<String, Object>> findShopsByIdtj(Integer shopId);
	/*
     * 店铺街查询行业
     * */
	public List<Map<String, Object>> findShopsHyName();
	/*
     * 店铺街根据行业ID点进行业查询该行业店铺
     * */
	public List<Map<String, Object>> selectByHyIdShops(Integer hyId);
	
	/*
	 * 查询热门品牌findBrandId
	 * */
	public List<Map<String, Object>> findBrandId();
	  /*
     * 根据店铺ID查询商品
     * */
	public List<Map<String, Object>> selectShopsGoodsId(Integer shopId);
	/*
	 * 自营街展示店铺
	 * */
	public List<Map<String, Object>> selectShopsZy();
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectShopNameShops(SktShops2 sktShops2);
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectLogoShops(SktShops2 sktShops2); 
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectLoginNameShops(SktShops2 sktShops2);
	  /*
     * 根据ZY店铺ID查询商品
     * */
	public List<Map<String, Object>> selectShopsZyGoodsId(Integer shopId);
	  /*
     * 轮播图首页
     * */
	public List<Map<String, Object>> findWebLunbo();

	//查询所有商家的userId
    List<Integer> selectAllId();

    List<SktShops> adsSelectShopName(String soName);

    public SktShops selectShopsInfo(@Param(value = "userId") Integer userId);

}
