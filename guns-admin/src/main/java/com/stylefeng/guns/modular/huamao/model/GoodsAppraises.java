package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 商品评价表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_goods_appraises")
public class GoodsAppraises extends Model<GoodsAppraises> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 订单ID
     */
    private Integer orderId;
    /**
     * 评价对象Id
     */
    private Integer goodsId;
    /**
     * 商品-规格Id
     */
    private Integer goodsSpecId;
    /**
     * 会员ID
     */
    private Integer userId;
    /**
     * 商品评分
     */
    private Integer goodsScore;
    /**
     * 服务评分
     */
    private Integer serviceScore;
    /**
     * 时效评分
     */
    private Integer timeScore;
    /**
     * 点评内容
     */
    private String content;
    /**
     * 店铺回复
     */
    private String shopReply;
    /**
     * 上传图片
     */
    private String images;
    /**
     * 是否显示
     */
    private Integer isShow;
    /**
     * 有效状态
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 商家回复时间
     */
    private Date replyTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getGoodsSpecId() {
        return goodsSpecId;
    }

    public void setGoodsSpecId(Integer goodsSpecId) {
        this.goodsSpecId = goodsSpecId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getGoodsScore() {
        return goodsScore;
    }

    public void setGoodsScore(Integer goodsScore) {
        this.goodsScore = goodsScore;
    }

    public Integer getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(Integer serviceScore) {
        this.serviceScore = serviceScore;
    }

    public Integer getTimeScore() {
        return timeScore;
    }

    public void setTimeScore(Integer timeScore) {
        this.timeScore = timeScore;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getShopReply() {
        return shopReply;
    }

    public void setShopReply(String shopReply) {
        this.shopReply = shopReply;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(Date replyTime) {
        this.replyTime = replyTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "GoodsAppraises{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", orderId=" + orderId +
        ", goodsId=" + goodsId +
        ", goodsSpecId=" + goodsSpecId +
        ", userId=" + userId +
        ", goodsScore=" + goodsScore +
        ", serviceScore=" + serviceScore +
        ", timeScore=" + timeScore +
        ", content=" + content +
        ", shopReply=" + shopReply +
        ", images=" + images +
        ", isShow=" + isShow +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", replyTime=" + replyTime +
        "}";
    }
}
