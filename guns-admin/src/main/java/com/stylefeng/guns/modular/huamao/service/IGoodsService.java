package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IGoodsService extends IService<Goods> {
	List<Map<String,Object>> selectByConditions(Page<Map<String, Object>> page, GoodsCustm goodsCustm);
	List<Goods> selectGoodsByGoodsCatId (int id);
	int batchUpdate(List<Map<String,Object>> list);
	int batchUpdatePo(List<Goods> list);
	public int selectisSaleCount();
	public int selectGoodsStatusCount();
	public int selectisSaleTotal();
	public int selectGoodsStatusTotal();
	/**
	 * 是否热销
	 * @return
	 */
	public List<Map<String,Object>> TodayHotSale();
	/**
	 * 根据品牌查询所有商品
	 * @return
	 */
	public List<Map<String,Object>> selectTodayHotSaleAll(Integer bandIds);
	List<Map<String,Object>> selectNotRecomGoods(GoodsCustm goodsCustm);
	List<Map<String,Object>> selectRecomGoods(GoodsCustm goodsCustm);
}
