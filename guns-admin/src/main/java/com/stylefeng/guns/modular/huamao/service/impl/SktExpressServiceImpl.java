package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.stylefeng.guns.modular.huamao.dao.SktExpressMapper;
import com.stylefeng.guns.modular.huamao.service.ISktExpressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 快递表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@Service
public class SktExpressServiceImpl extends ServiceImpl<SktExpressMapper, SktExpress> implements ISktExpressService {

}
