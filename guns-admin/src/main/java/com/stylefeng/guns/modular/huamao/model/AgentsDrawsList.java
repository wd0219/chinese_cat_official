package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 代理公司提现记录表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public class AgentsDrawsList extends AgentsDraws {

	/**
	 * 代理公司名称
	 */
	private String agentName;
	/**
	 * 持卡人姓名
	 */
	private String accUser;
	/**
	 * 持卡人身份证号
	 */
	private String IDnumber;
	// 因为前台获取IDnumber是出错所以让它获取number
	private String number;
	/**
	 * 银行卡号
	 */
	private String accNo;
	/**
	 * 查询创建时的开始时间
	 */
	private String cbeginTime;
	/**
	 * 查询创建时的结束时间
	 */
	private String cendTime;
	/**
	 * 查询预计到账时的开始时间
	 */
	private String ybeginTime;
	/**
	 * 查询预计到账时的结束时间
	 */
	private String yendTime;

	// 让number直接返回IDnumber，不需要set
	public String getNumber() {
		return IDnumber;
	}

	// public void setNumber(String number) {
	// this.number = number;
	// }

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAccUser() {
		return accUser;
	}

	public void setAccUser(String accUser) {
		this.accUser = accUser;
	}

	public String getIDnumber() {
		return IDnumber;
	}

	public void setIDnumber(String iDnumber) {
		IDnumber = iDnumber;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getCbeginTime() {
		return cbeginTime;
	}

	public void setCbeginTime(String cbeginTime) {
		this.cbeginTime = cbeginTime;
	}

	public String getCendTime() {
		return cendTime;
	}

	public void setCendTime(String cendTime) {
		this.cendTime = cendTime;
	}

	public String getYbeginTime() {
		return ybeginTime;
	}

	public void setYbeginTime(String ybeginTime) {
		this.ybeginTime = ybeginTime;
	}

	public String getYendTime() {
		return yendTime;
	}

	public void setYendTime(String yendTime) {
		this.yendTime = yendTime;
	}

}
