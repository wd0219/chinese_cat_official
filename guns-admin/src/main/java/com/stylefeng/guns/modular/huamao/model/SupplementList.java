package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 手动补单扩展类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-13
 */
public class SupplementList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 手机
	 */
	private String userPhone;
	/**
	 * 登录名 昵称
	 */
	private String loginName;
	/**
	 * 订单编号
	 */
	private String orderNo;
	/**
	 * 操作对象 1积分 2现金 3华宝 4库存积分
	 */
	private Integer object;
	/**
	 * 标志 -1减少 1增加
	 */
	private Integer type;
	/**
	 * 变化金额
	 */
	private BigDecimal num;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 操作员工id
	 */
	private Integer staffId;
	/**
	 * 创建时间
	 */
	private Date createTime;

	public SupplementList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public SupplementList(Integer userId, String userPhone, String loginName, String orderNo, Integer object,
			Integer type, BigDecimal num, String remark, Integer staffId, Date createTime) {
		super();
		this.userId = userId;
		this.userPhone = userPhone;
		this.loginName = loginName;
		this.orderNo = orderNo;
		this.object = object;
		this.type = type;
		this.num = num;
		this.remark = remark;
		this.staffId = staffId;
		this.createTime = createTime;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getObject() {
		return object;
	}

	public void setObject(Integer object) {
		this.object = object;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigDecimal getNum() {
		return num;
	}

	public void setNum(BigDecimal num) {
		this.num = num;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "SupplementList [userId=" + userId + ", userPhone=" + userPhone + ", loginName=" + loginName
				+ ", orderNo=" + orderNo + ", object=" + object + ", type=" + type + ", num=" + num + ", remark="
				+ remark + ", staffId=" + staffId + ", createTime=" + createTime + "]";
	}

}
