package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SupplementList;

import java.util.Map;

/**
 * 手动补单 服务类
 * 
 * @author gxz
 *
 */
public interface ISupplementService extends IService<SupplementList> {

	// 校验用户名或手机号是否存在
	public SupplementList checkUser(SupplementList supplementList);

	// 添加记录
	public Map<String,Object> add(SupplementList supplementList);

}
