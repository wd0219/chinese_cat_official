package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgent;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgentList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 代理公司积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogScoreAgentMapper extends BaseMapper<LogScoreAgent> {

	// 代理公司积分流水表返回
	List<LogScoreAgentList> selectLogScoreAgentAll(@Param("page")Page<LogScoreAgentList> page,@Param("logScoreAgentList") LogScoreAgentList logScoreAgentList);

    Integer selectLogScoreAgentAllCount(@Param("logScoreAgentList") LogScoreAgentList logScoreAgentList);
}
