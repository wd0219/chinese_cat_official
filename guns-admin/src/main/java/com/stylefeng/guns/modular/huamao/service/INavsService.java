package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.Navs;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商城导航表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface INavsService extends IService<Navs> {

}
