package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktAgentsAccount;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccountDTO;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 代理公司账户表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-18
 */
public interface ISktAgentsAccountService extends IService<SktAgentsAccount> {
	/**
	 * 页面显示
	 * @param page 
	 * @param sktAgentsAccountDTO
	 * @return
	 */
	public List<SktAgentsAccountDTO> showAgentsAccountInfo (Page<SktAgentsAccountDTO> page, SktAgentsAccountDTO sktAgentsAccountDTO);
}
