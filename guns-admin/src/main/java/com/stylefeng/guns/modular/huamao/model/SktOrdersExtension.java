package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 线上商城订单扩展表
 * </p>
 *
 * @author caody123
 * @since 2018-05-18
 */
@TableName("skt_orders_extension")
public class SktOrdersExtension extends Model<SktOrdersExtension> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 订单号
     */
	private String orderId;
    /**
     * 库存积分标识：1店铺库存积分充足 2库存不足扣营业额购买库存积分
     */
	private Integer scoreFlag;
    /**
     * 所得积分
     */
	private BigDecimal score;
    /**
     * 应自动收货时间
     */
	private Date autoTakeTime;
    /**
     * 应结算时间
     */
	private Date settleTime;
    /**
     * 是否自动收货 0否；1是 2已手动收货
     */
	private Integer isAutoTake;
    /**
     * 批处理标示符：1未处理 2已处理 9处理中
     */
	private Integer isSettle;
    /**
     * 实际结算时间
     */
	private Date realSettleTime;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getScoreFlag() {
		return scoreFlag;
	}

	public void setScoreFlag(Integer scoreFlag) {
		this.scoreFlag = scoreFlag;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public Date getAutoTakeTime() {
		return autoTakeTime;
	}

	public void setAutoTakeTime(Date autoTakeTime) {
		this.autoTakeTime = autoTakeTime;
	}

	public Date getSettleTime() {
		return settleTime;
	}

	public void setSettleTime(Date settleTime) {
		this.settleTime = settleTime;
	}

	public Integer getIsAutoTake() {
		return isAutoTake;
	}

	public void setIsAutoTake(Integer isAutoTake) {
		this.isAutoTake = isAutoTake;
	}

	public Integer getIsSettle() {
		return isSettle;
	}

	public void setIsSettle(Integer isSettle) {
		this.isSettle = isSettle;
	}

	public Date getRealSettleTime() {
		return realSettleTime;
	}

	public void setRealSettleTime(Date realSettleTime) {
		this.realSettleTime = realSettleTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
