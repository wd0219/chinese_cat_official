package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanService;

import java.util.List;

/**
 * 华宝记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-16 16:33:26
 */
@Controller
@RequestMapping("/logKaiyuan")
public class LogKaiyuanController extends BaseController {

	private String PREFIX = "/huamao/logKaiyuan/";

	@Autowired
	private ILogKaiyuanService logKaiyuanService;

	/**
	 * 跳转到华宝记录首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, Model model) {
		// 参数是为了在用户点击华宝查看华宝详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logKaiyuan.html";
	}

	/**
	 * 跳转到添加华宝记录
	 */
	@RequestMapping("/logKaiyuan_add")
	public String logKaiyuanAdd() {
		return PREFIX + "logKaiyuan_add.html";
	}

	/**
	 * 跳转到修改华宝记录
	 */
	@RequestMapping("/logKaiyuan_update/{logKaiyuanId}")
	public String logKaiyuanUpdate(@PathVariable Integer logKaiyuanId, Model model) {
		LogKaiyuan logKaiyuan = logKaiyuanService.selectById(logKaiyuanId);
		model.addAttribute("item", logKaiyuan);
		LogObjectHolder.me().set(logKaiyuan);
		return PREFIX + "logKaiyuan_edit.html";
	}

	/**
	 * 获取华宝记录列表 ：{userPhone2}用户查看华宝详情传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone,
			LogKaiyuanList logKaiyuanList) {
		// 判断如果不是‘手机号’并且logScoreList.getUserPhone()不是null说明要看用户积分详情，是‘手机号’并且logScoreList.getUserPhone()是''或是有数据说明是普通的查询所有的积分详情
		if (!phone.equals("手机号") && logKaiyuanList.getUserPhone() == null) {
			logKaiyuanList.setUserPhone(phone);
		}
		Page<LogKaiyuanList> page = new PageFactory<LogKaiyuanList>().defaultPage();
		List<LogKaiyuanList> logKaiyuanLists = logKaiyuanService.selectLogKaiyuanAll(page,logKaiyuanList);
		page.setRecords(logKaiyuanLists);
		return super.packForBT(page);
		// return logKaiyuanService.selectList(null);
	}

	/**
	 * 新增华宝记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogKaiyuan logKaiyuan) {
		logKaiyuanService.insert(logKaiyuan);
		return SUCCESS_TIP;
	}

	/**
	 * 删除华宝记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logKaiyuanId) {
		logKaiyuanService.deleteById(logKaiyuanId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改华宝记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogKaiyuan logKaiyuan) {
		logKaiyuanService.updateById(logKaiyuan);
		return SUCCESS_TIP;
	}

	/**
	 * 华宝记录详情
	 */
	@RequestMapping(value = "/detail/{logKaiyuanId}")
	@ResponseBody
	public Object detail(@PathVariable("logKaiyuanId") Integer logKaiyuanId) {
		return logKaiyuanService.selectById(logKaiyuanId);
	}
}
