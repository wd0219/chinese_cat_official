package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktHuamaobtLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author caody123
 * @since 2018-06-23
 */
public interface ISktHuamaobtLogService extends IService<SktHuamaobtLog> {

    public Object recharge(Integer userId,Integer amount,Integer drawId);

}
