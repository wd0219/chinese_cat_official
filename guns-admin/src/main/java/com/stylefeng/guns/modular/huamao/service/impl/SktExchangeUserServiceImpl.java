package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktExchangeUser;
import com.stylefeng.guns.modular.huamao.dao.SktExchangeUserMapper;
import com.stylefeng.guns.modular.huamao.service.ISktExchangeUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author caody123
 * @since 2018-06-23
 */
@Service
public class SktExchangeUserServiceImpl extends ServiceImpl<SktExchangeUserMapper, SktExchangeUser> implements ISktExchangeUserService {

}
