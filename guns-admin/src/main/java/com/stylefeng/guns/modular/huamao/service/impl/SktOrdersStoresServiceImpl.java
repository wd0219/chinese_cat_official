package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersStoresMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStoresDto;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersStoresService;

/**
 * <p>
 * 线下商家订单表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-24
 */
@Service
public class SktOrdersStoresServiceImpl extends ServiceImpl<SktOrdersStoresMapper, SktOrdersStores> implements ISktOrdersStoresService {

	@Autowired
	private SktOrdersStoresMapper sktOrdersStoresMapper;
	@Override
	public List<SktOrdersStoresDto> showOrdersStoresInfo(Page<SktOrdersStoresDto> page,SktOrdersStoresDto sktOrdersStoresDto) {
		List<SktOrdersStoresDto> list = sktOrdersStoresMapper.showOrdersStoresInfo(page,sktOrdersStoresDto);
		Integer total = sktOrdersStoresMapper.showOrdersStoresInfoCount(sktOrdersStoresDto);
		if(page != null){
			page.setTotal(total);
		}
		return list;
	}
	@Override
	public int selectCount() {
		// TODO Auto-generated method stub
		return sktOrdersStoresMapper.selectCount();
	}
	@Override
	public int selectOrdersStoresTotal() {
		// TODO Auto-generated method stub
		return sktOrdersStoresMapper.selectOrdersStoresTotal();
	}
	@Override
	public List<SktOrdersStoresDto> showOrdersStoresInfo2(SktOrdersStoresDto sktOrdersStoresDto) {
		List<SktOrdersStoresDto> list = sktOrdersStoresMapper.showOrdersStoresInfo2(sktOrdersStoresDto);
		return list;
	}
}
