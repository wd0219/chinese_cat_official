package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktLogAgentsLogins;
import com.stylefeng.guns.modular.huamao.service.ISktLogAgentsLoginsService;

import java.util.List;

/**
 * 代理公司登录日志控制器
 *
 * @author fengshuonan
 * @Date 2018-04-27 15:04:08
 */
@Controller
@RequestMapping("/sktLogAgentsLogins")
public class SktLogAgentsLoginsController extends BaseController {

    private String PREFIX = "/huamao/sktLogAgentsLogins/";

    @Autowired
    private ISktLogAgentsLoginsService sktLogAgentsLoginsService;

    /**
     * 跳转到代理公司登录日志首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktLogAgentsLogins.html";
    }

    /**
     * 跳转到添加代理公司登录日志
     */
    @RequestMapping("/sktLogAgentsLogins_add")
    public String sktLogAgentsLoginsAdd() {
        return PREFIX + "sktLogAgentsLogins_add.html";
    }

    /**
     * 跳转到修改代理公司登录日志
     */
    @RequestMapping("/sktLogAgentsLogins_update/{sktLogAgentsLoginsId}")
    public String sktLogAgentsLoginsUpdate(@PathVariable Integer sktLogAgentsLoginsId, Model model) {
        SktLogAgentsLogins sktLogAgentsLogins = sktLogAgentsLoginsService.selectById(sktLogAgentsLoginsId);
        model.addAttribute("item",sktLogAgentsLogins);
        LogObjectHolder.me().set(sktLogAgentsLogins);
        return PREFIX + "sktLogAgentsLogins_edit.html";
    }

    /**
     * 获取代理公司登录日志列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktLogAgentsLogins sktLogAgentsLogins) {
        Page<SktLogAgentsLogins> page = new PageFactory<SktLogAgentsLogins>().defaultPage();
        List<SktLogAgentsLogins> sktLogAgentsLogins1 = sktLogAgentsLoginsService.sktLogAgentsLoginfindup(page,sktLogAgentsLogins);
        page.setRecords(sktLogAgentsLogins1);
        return super.packForBT(page);
    }

    /**
     * 新增代理公司登录日志
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktLogAgentsLogins sktLogAgentsLogins) {
        sktLogAgentsLoginsService.insert(sktLogAgentsLogins);
        return SUCCESS_TIP;
    }

    /**
     * 删除代理公司登录日志
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktLogAgentsLoginsId) {
        sktLogAgentsLoginsService.deleteById(sktLogAgentsLoginsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改代理公司登录日志
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktLogAgentsLogins sktLogAgentsLogins) {
        sktLogAgentsLoginsService.updateById(sktLogAgentsLogins);
        return SUCCESS_TIP;
    }

    /**
     * 代理公司登录日志详情
     */
    @RequestMapping(value = "/detail/{sktLogAgentsLoginsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktLogAgentsLoginsId") Integer sktLogAgentsLoginsId) {
        return sktLogAgentsLoginsService.selectById(sktLogAgentsLoginsId);
    }
}
