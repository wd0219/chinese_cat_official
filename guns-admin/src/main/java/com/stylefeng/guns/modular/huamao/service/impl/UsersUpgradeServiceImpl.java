package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersUpgradeMapper;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.model.UsersUpgradeDTO;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IUsersUpgradeService;

/**
 * <p>
 * 用户升级角色订单表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
@Service
public class UsersUpgradeServiceImpl extends ServiceImpl<UsersUpgradeMapper, UsersUpgrade> implements IUsersUpgradeService {
	
	@Autowired
	private UsersUpgradeMapper usersUpgradeMapper;
	@Autowired
	private IAccountService iAccountService;
	@Autowired
	private UsersMapper usersMapper;
	private static final String FINAL_ROLE = "FINAL_ROLE";
	private static final String SUCCESS = "SUCCESS";
	@Override
	public List<UsersUpgradeDTO> showUsersShengJiInfo(Page<UsersUpgradeDTO>page,UsersUpgradeDTO usersUpgradeDTO) {
		List<UsersUpgradeDTO> list= usersUpgradeMapper.showUsersShengJiInfo(page,usersUpgradeDTO);
		Integer total = usersUpgradeMapper.showUsersShengJiInfoCount(usersUpgradeDTO);
		page.setTotal(total);
		return list;
	}
	
	@Transactional
	@Override
	public String updateUpgradeStatus(Integer orderId, Integer status,Integer preRole, Integer afterRole) {
		
		if(preRole == 2){//是不是经理
			return FINAL_ROLE;
		}
		//修改用户角色
		if(status == 3){
			//新增积分
			String upAfterAdd = iAccountService.upAfterAdd(orderId);
			if("success".equals(upAfterAdd)){
				String upgradeTime = DateUtil.getTime();
				usersUpgradeMapper.updateUpgradeStatus(orderId,status,preRole,afterRole,upgradeTime);
				Users users = new Users();
				UsersUpgrade selectById = usersUpgradeMapper.selectById(orderId);
				users.setUserId(selectById.getUserId());
				users.setUserType(afterRole);
				usersMapper.updateById(users);	
				return SUCCESS;
			}
		}else{
			UsersUpgrade usersUpgrade = new UsersUpgrade();
			usersUpgrade.setOrderId(orderId);
			usersUpgrade.setStatus(status);
			usersUpgradeMapper.updateById(usersUpgrade);
			//取消修改现金
			UsersUpgrade selectById = usersUpgradeMapper.selectById(orderId);
			iAccountService.resetUPGrade(selectById);
			return SUCCESS;
		}
		return FINAL_ROLE;
	}
	
}
