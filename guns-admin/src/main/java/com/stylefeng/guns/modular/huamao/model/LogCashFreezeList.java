package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 待发现金流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public class LogCashFreezeList extends LogCashFreeze {

	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String cashp;
	/**
	 * 变动后金额
	 */
	private BigDecimal acash;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;


	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	public String getCashp() {
		if (this.getCashType() == -1) {
			return "-" + this.getCash();
		}
		return "+" + this.getCash();
	}

	// public void setCashp(String cashp) {
	// this.cashp = cashp;
	// }

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	/**
	 * 变动后金额，直接让变动前加上变动的就行，不需要赋值。
	 */
	public BigDecimal getAcash() {
		if(this.getCashType() == 1){
			return this.getPreCash().add(this.getCash());
		}else{
			return this.getPreCash().subtract(this.getCash());
		}
	}

	// public void setAcash(BigDecimal acash) {
	// this.acash = acash;
	// }


	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
