package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholder;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;

/**
 * <p>
 * 代理公司的股东表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-19
 */
public interface SktAgentsStockholderMapper extends BaseMapper<SktAgentsStockholder> {
	/**
	 * 页面显示列表
	 * @param page 
	 * @param sktAgentsStockholderDTO
	 * @return
	 */
	public List<SktAgentsStockholderDTO> showSktStcokHolderInfo(@Param(value="page")Page<SktAgentsStockholderDTO> page, 
			@Param(value="sktAgentsStockholderDTO")SktAgentsStockholderDTO sktAgentsStockholderDTO);
	public Integer showSktStcokHolderInfoCount(@Param(value="sktAgentsStockholderDTO")SktAgentsStockholderDTO sktAgentsStockholderDTO);
	/**
	 * 查询该代理公司有没有这个管理
	 * @param agentId
	 * @param type
	 * @return
	 */
	public SktAgentsStockholder selectSktStcokManger(@Param(value="agentId") Integer agentId, @Param(value="type") Integer type );

	/**
	 * 查询用户电话
	 * @param nameOrPhone
	 * @return
	 */
	public Map<String,Object> selectNameOrPhone(@Param(value="nameOrPhone") String nameOrPhone);
	
	/**
	 * 更新
	 * @param sktAgentsStockholder
	 */
	public void updateSktAgentsStockHolder(SktAgentsStockholder sktAgentsStockholder);
	
	/**
	 * 查询用户字段
	 * @param userId
	 * @return
	 */
	public Map<String,Object> selectUserById(@Param(value="userId")Integer userId);
	/**
	 * 查询该地区有没有该职位
	 * @param province
	 * @param citys
	 * @param county
	 * @param alevel
	 * @param type
	 * @return
	 */
	public Map<String, Object> selectSktAgentsStockUser(@Param(value="province")Integer province, 
			@Param(value="citys")Integer citys,  @Param(value="county")Integer county, 
			@Param(value="alevel")Integer alevel, @Param(value="type")Integer type);

	public SktAgentsStockholder  selectByUserId( Integer userId);


	public Integer selectCountStcokManger(SktAgentsStockholder sktAgentsStockholder);

	public Integer selectCountNotHolder(Integer agentId);

	public List<SktAgentsStockholder> selectByAgentId(Integer agentId);

	


}
