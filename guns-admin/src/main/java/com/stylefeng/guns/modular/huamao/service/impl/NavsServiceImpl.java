package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.Navs;
import com.stylefeng.guns.modular.huamao.dao.NavsMapper;
import com.stylefeng.guns.modular.huamao.service.INavsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商城导航表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
@Service
public class NavsServiceImpl extends ServiceImpl<NavsMapper, Navs> implements INavsService {

}
