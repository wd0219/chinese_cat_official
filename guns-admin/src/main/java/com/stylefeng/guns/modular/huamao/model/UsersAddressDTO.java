package com.stylefeng.guns.modular.huamao.model;

public class UsersAddressDTO extends UsersAddress{
	
	private static final long serialVersionUID = 1L;
	/**
	 * 用户电话
	 */
	private String yonghuPhone;
	/**
	 * 用户姓名
	 */
	private String yonghuName;
	/**
	 * 省名字
	 */
	private String provinceName;
	/**
	 * 城市名字
	 */
	private String cityName ;
	/**
	 * 地区名字
	 */
	private String areaName;
	/**
	 *开始时间
	 */
	private String beginTime;
	/**
	 * 结束时间
	 */
	private String endTime;
	/**
	 * 用户类型
	 */
	private Integer userType;
	/**
	 * 省id
	 */
	private String province;
	/**
	 * 市id
	 */
	private String citys;
	/**
	 * 县id
	 */
	private String county;
	
	public String getYonghuPhone() {
		return yonghuPhone;
	}
	public void setYonghuPhone(String yonghuPhone) {
		this.yonghuPhone = yonghuPhone;
	}
	public String getYonghuName() {
		return yonghuName;
	}
	public void setYonghuName(String yonghuName) {
		this.yonghuName = yonghuName;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCitys() {
		return citys;
	}
	public void setCitys(String citys) {
		this.citys = citys;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	
}
