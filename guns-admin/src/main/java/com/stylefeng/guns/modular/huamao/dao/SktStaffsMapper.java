package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.*;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商城职员表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-25
 */
public interface SktStaffsMapper extends BaseMapper<SktStaffs> {
	
	public List<SktShopIndustryDTO> sktStaffsfindAll(@Param("page")Page<SktShopIndustryDTO> page,@Param("sktShopIndustryDTO") SktShopIndustryDTO sktShopIndustryDTO);
	
	public List<SktShopIndustryDTO> sktStaffsfindup(SktShopIndustryDTO sktShopIndustryDTO);
	
	public void sktStaffsupdate(SktShopIndustryDTO sktShopIndustryDTO);
	
	public void sktStaffsup1(SktShopIndustryDTO sktShopIndustryDTO);

	public Integer updataAccount(@Param(value="shopId")Integer shopId, @Param(value="userId")Integer userId);

	public Integer selectShops(@Param(value="userId")Integer userId);

	public SktAgents selectAgents(SktShopApprove shopApprove);

	public Integer updataAgentNum(SktAgents sktAgents);

	public Integer selectIsAgentsStockholder(@Param(value="userId")Integer userId);

	public Integer updateAgentsStockholder(@Param(value="shId")Integer shId);


    Integer sktStaffsfindAllCount(@Param("sktShopIndustryDTO") SktShopIndustryDTO sktShopIndustryDTO);

    public Integer selectShopsCount(@Param(value="usersId") Integer usersId);

	public Integer selectShopsAcountCount(@Param(value="usersId")Integer usersId);
}
