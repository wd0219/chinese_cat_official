package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktUsersDrawsMaobi;
import com.stylefeng.guns.modular.huamao.model.SktUsersDrawsMaobiDto;
import com.stylefeng.guns.modular.huamao.dao.SktUsersDrawsMaobiMapper;
import com.stylefeng.guns.modular.huamao.service.ISktUsersDrawsMaobiService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户提现表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-30
 */
@Service
public class SktUsersDrawsMaobiServiceImpl extends ServiceImpl<SktUsersDrawsMaobiMapper, SktUsersDrawsMaobi> implements ISktUsersDrawsMaobiService {

	@Autowired
	private SktUsersDrawsMaobiMapper sktUsersDrawsMaobiMapper;
	@Override
	public List<SktUsersDrawsMaobiDto> selectMaoBiList(Page<SktUsersDrawsMaobiDto> page,
			SktUsersDrawsMaobiDto sktUsersDrawsMaobiDto) {
		List<SktUsersDrawsMaobiDto> list = sktUsersDrawsMaobiMapper.selectMaoBiList(page,sktUsersDrawsMaobiDto);
		Integer total = sktUsersDrawsMaobiMapper.selectMaoBiListCount(sktUsersDrawsMaobiDto);
		page.setTotal(total);
		return list;
	}

}
