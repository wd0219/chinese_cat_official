package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsMapper;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;
import com.stylefeng.guns.modular.huamao.service.IGoodsService;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {
	@Autowired
	private GoodsMapper goodsMapper;
	
	@Override
	public List<Map<String, Object>> selectByConditions(Page<Map<String, Object>>page,GoodsCustm goodsCustm) {
		 List<Map<String, Object>> list = goodsMapper.selectByConditions(page,goodsCustm);
		 Integer total = goodsMapper.selectByConditionsCount(goodsCustm);
		 page.setTotal(total);
		 return list;
	}

	@Override
	public List<Goods> selectGoodsByGoodsCatId(int id) {
		// TODO Auto-generated method stub
		return baseMapper.selectGoodsByGoodsCatId(id);
	}

	@Override
	public int batchUpdate(List<Map<String, Object>> list) {
		// TODO Auto-generated method stub
		return baseMapper.batchUpdate(list);
	}

	@Override
	public int batchUpdatePo(List<Goods> list) {
		// TODO Auto-generated method stub
		return baseMapper.batchUpdatePo(list);
	}

	@Override
	public int selectisSaleCount() {
		// TODO Auto-generated method stub
		return baseMapper.selectisSaleCount();
	}

	@Override
	public int selectGoodsStatusCount() {
		// TODO Auto-generated method stub
		return baseMapper.selectGoodsStatusCount();
	}

	@Override
	public int selectisSaleTotal() {
		// TODO Auto-generated method stub
		return baseMapper.selectisSaleTotal();
	}

	@Override
	public int selectGoodsStatusTotal() {
		// TODO Auto-generated method stub
		return baseMapper.selectGoodsStatusTotal();
	}

	@Override
	public List<Map<String, Object>> TodayHotSale() {
		List<Map<String,Object>> list = goodsMapper.selectTodayHotSale();
		return list;
	}
	
	@Override
	public List<Map<String,Object>> selectTodayHotSaleAll(Integer bandId){
		List<Map<String,Object>> list = goodsMapper.selectTodayHotSaleAll(bandId);
		return list;
	}
@Override
	public  List<Map<String,Object>> selectNotRecomGoods(GoodsCustm goodsCustm){
		return goodsMapper.selectNotRecomGoods(goodsCustm);
	}

	@Override
	public  List<Map<String,Object>> selectRecomGoods(GoodsCustm goodsCustm){
		return goodsMapper.selectRecomGoods(goodsCustm);
	}
}
