package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 第三方补单记录扩展类
 * 
 * @author gxz
 *
 */
public class ThirdSupplementList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 内部订单号
	 */
	private String orderNo;
	/**
	 * 业务类型 BUY_GOODS:商城消费 BUY_SCORE:购买积分 RECHARGE:充值 UPGRADE:升级
	 */
	private String businessType;
	/**
	 * 支付类型 1-aipay微信 2-aipay支付宝 3-aipay快捷
	 */
	private Integer payType;
	/**
	 * 商户号
	 */
	private String ino;
	/**
	 * 支付金额
	 */
	private BigDecimal payMoney;
	/**
	 * 支付流水单号
	 */
	private String outTradeNo;
	/**
	 * 请求数据
	 */
	private String request;
	/**
	 * 银行流水号或支付渠道号
	 */
	private String payTypeOrderNo;
	/**
	 * 支付成功返回的数据
	 */
	private String response;
	/**
	 * 响应时间
	 */
	private Date responseTime;
	/**
	 * 状态1已获取支付链接或跳入快捷页面 2已接收到异步响应 3处理完回调方法 4处理失败
	 */
	private Integer payStatus;
	/**
	 * 手动补单
	 */
	private Integer handleStatus;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 删除标记 0删除 1显示）
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	public ThirdSupplementList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ThirdSupplementList(Integer userId, String userPhone, String orderNo, String businessType, Integer payType,
			String ino, BigDecimal payMoney, String outTradeNo, String request, String payTypeOrderNo, String response,
			Date responseTime, Integer payStatus, Integer handleStatus, String remark, Integer dataFlag,
			Date createTime, String beginTime, String endTime) {
		super();
		this.userId = userId;
		this.userPhone = userPhone;
		this.orderNo = orderNo;
		this.businessType = businessType;
		this.payType = payType;
		this.ino = ino;
		this.payMoney = payMoney;
		this.outTradeNo = outTradeNo;
		this.request = request;
		this.payTypeOrderNo = payTypeOrderNo;
		this.response = response;
		this.responseTime = responseTime;
		this.payStatus = payStatus;
		this.handleStatus = handleStatus;
		this.remark = remark;
		this.dataFlag = dataFlag;
		this.createTime = createTime;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public String getIno() {
		return ino;
	}

	public void setIno(String ino) {
		this.ino = ino;
	}

	public BigDecimal getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getPayTypeOrderNo() {
		return payTypeOrderNo;
	}

	public void setPayTypeOrderNo(String payTypeOrderNo) {
		this.payTypeOrderNo = payTypeOrderNo;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Date getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Integer getHandleStatus() {
		return handleStatus;
	}

	public void setHandleStatus(Integer handleStatus) {
		this.handleStatus = handleStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ThirdSupplementList [userId=" + userId + ", userPhone=" + userPhone + ", orderNo=" + orderNo
				+ ", businessType=" + businessType + ", payType=" + payType + ", ino=" + ino + ", payMoney=" + payMoney
				+ ", outTradeNo=" + outTradeNo + ", request=" + request + ", payTypeOrderNo=" + payTypeOrderNo
				+ ", response=" + response + ", responseTime=" + responseTime + ", payStatus=" + payStatus
				+ ", handleStatus=" + handleStatus + ", remark=" + remark + ", dataFlag=" + dataFlag + ", createTime="
				+ createTime + ", beginTime=" + beginTime + ", endTime=" + endTime + "]";
	}

}
