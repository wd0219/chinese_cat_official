package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.GoodsSpecs;
import com.stylefeng.guns.modular.huamao.service.IGoodsSpecsService;

/**
 * 商品规格控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:31:55
 */
@Controller
@RequestMapping("/goodsSpecs")
public class GoodsSpecsController extends BaseController {

    private String PREFIX = "/huamao/goodsSpecs/";

    @Autowired
    private IGoodsSpecsService goodsSpecsService;

    /**
     * 跳转到商品规格首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "goodsSpecs.html";
    }

    /**
     * 跳转到添加商品规格
     */
    @RequestMapping("/goodsSpecs_add")
    public String goodsSpecsAdd() {
        return PREFIX + "goodsSpecs_add.html";
    }

    /**
     * 跳转到修改商品规格
     */
    @RequestMapping("/goodsSpecs_update/{goodsSpecsId}")
    public String goodsSpecsUpdate(@PathVariable Integer goodsSpecsId, Model model) {
        GoodsSpecs goodsSpecs = goodsSpecsService.selectById(goodsSpecsId);
        model.addAttribute("item",goodsSpecs);
        LogObjectHolder.me().set(goodsSpecs);
        return PREFIX + "goodsSpecs_edit.html";
    }

    /**
     * 获取商品规格列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return goodsSpecsService.selectList(null);
    }

    /**
     * 新增商品规格
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(GoodsSpecs goodsSpecs) {
        goodsSpecsService.insert(goodsSpecs);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品规格
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer goodsSpecsId) {
        goodsSpecsService.deleteById(goodsSpecsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品规格
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(GoodsSpecs goodsSpecs) {
        goodsSpecsService.updateById(goodsSpecs);
        return SUCCESS_TIP;
    }

    /**
     * 商品规格详情
     */
    @RequestMapping(value = "/detail/{goodsSpecsId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsSpecsId") Integer goodsSpecsId) {
        return goodsSpecsService.selectById(goodsSpecsId);
    }
}
