package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.constant.factory.PageFactory;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.common.UrlUtils;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.model.UsersUpgradeDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersUpgradeService;

/**
 * 用户升级表控制器
 *
 * @author fengshuonan
 * @Date 2018-04-14 11:53:39
 */
@Controller
@RequestMapping("/usersUpgrade")
public class UsersUpgradeController extends BaseController {

    private String PREFIX = "/huamao/usersUpgrade/";

    @Autowired
    private IUsersUpgradeService usersUpgradeService;

    /**
     * 跳转到用户升级表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "usersUpgrade.html";
    }

    /**
     * 跳转到添加用户升级表
     */
    @RequestMapping("/usersUpgrade_add")
    public String usersUpgradeAdd() {
        return PREFIX + "usersUpgrade_add.html";
    }

    /**
     * 跳转到修改用户升级表
     */
    @RequestMapping("/usersUpgrade_update/{usersUpgradeId}")
    public String usersUpgradeUpdate(@PathVariable Integer usersUpgradeId, Model model) {
        UsersUpgrade usersUpgrade = usersUpgradeService.selectById(usersUpgradeId);
        model.addAttribute("item",usersUpgrade);
        LogObjectHolder.me().set(usersUpgrade);
        return PREFIX + "usersUpgrade_edit.html";
    }

    /**
     * 获取用户升级表列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(UsersUpgradeDTO usersUpgradeDTO) {
    	Page<UsersUpgradeDTO> page = new PageFactory<UsersUpgradeDTO>().defaultPage();
    	List<UsersUpgradeDTO> list= usersUpgradeService.showUsersShengJiInfo(page,usersUpgradeDTO);
    	page.setRecords(list);
        return super.packForBT(page);
    }

    /**
     * 新增用户升级表
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(UsersUpgrade usersUpgrade) {
        usersUpgradeService.insert(usersUpgrade);
        return SUCCESS_TIP;
    }

    /**
     * 删除用户升级表
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer usersUpgradeId) {
        usersUpgradeService.deleteById(usersUpgradeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户升级表
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(UsersUpgrade usersUpgrade) {
        usersUpgradeService.updateById(usersUpgrade);
        return SUCCESS_TIP;
    }
    
    /**
     * 修改用户升级状态
     */
    @RequestMapping(value = "/updateStatus/{orderId}/{status}")
    @ResponseBody
    public Object updateUpgradeStatus(@PathVariable Integer orderId, @PathVariable Integer status,
    		@RequestParam(value = "preRole") Integer preRole, @RequestParam(value="afterRole") Integer afterRole) {
    	String result = usersUpgradeService.updateUpgradeStatus(orderId,status,preRole,afterRole);
    	if("SUCCESS".equals(result)){
    		return SUCCESS_TIP;
    	}
    	return result;
    }

    /**
     * 用户升级表详情
     */
    @RequestMapping(value = "/detail/{usersUpgradeId}")
    @ResponseBody
    public Object detail(@PathVariable("usersUpgradeId") Integer usersUpgradeId) {
        return usersUpgradeService.selectById(usersUpgradeId);
    }
}
