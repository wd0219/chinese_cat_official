package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 代理股东分红记录表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-12
 */
@TableName("skt_agents_profit")
public class SktAgentsProfit extends Model<SktAgentsProfit> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "profitId", type = IdType.AUTO)
    private Integer profitId;
    /**
     * 分红用户ID
     */
    private Integer userId;
    /**
     * 拥有的股票分数
     */
    private Integer stockNum;
    /**
     * 发展的商家数量
     */
    private Integer storeNum;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 分红金额
     */
    private BigDecimal profitScore;
    /**
     * 分润比例(不含百分号)
     */
    private BigDecimal ratio;
    /**
     * 创建时间
     */
    private Date addTime;
    /**
     * 分红周期(201711,201712)
     */
    private String period;


    public Integer getProfitId() {
        return profitId;
    }

    public void setProfitId(Integer profitId) {
        this.profitId = profitId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getStoreNum() {
        return storeNum;
    }

    public void setStoreNum(Integer storeNum) {
        this.storeNum = storeNum;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getProfitScore() {
        return profitScore;
    }

    public void setProfitScore(BigDecimal profitScore) {
        this.profitScore = profitScore;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    protected Serializable pkVal() {
        return this.profitId;
    }

    @Override
    public String toString() {
        return "SktAgentsProfit{" +
        "profitId=" + profitId +
        ", userId=" + userId +
        ", stockNum=" + stockNum +
        ", storeNum=" + storeNum +
        ", orderNo=" + orderNo +
        ", profitScore=" + profitScore +
        ", ratio=" + ratio +
        ", addTime=" + addTime +
        ", period=" + period +
        "}";
    }
}
