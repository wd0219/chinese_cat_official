package com.stylefeng.guns.core.asyncTask;

import com.stylefeng.guns.modular.huamao.common.QueueUtils;
import com.stylefeng.guns.modular.huamao.service.ISktHuamaobtLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

@Component
public class MyAsyncTask {

    @Autowired
    @Lazy
    ISktHuamaobtLogService sktHuamaobtLogService;
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Async
    public void refreshMyDbAsync()  {
        BlockingQueue<Map<String,Object>> queue = QueueUtils.getQueue();
        //队列方式遍历，元素逐个被移除
        while (queue.peek() != null) {
            Map<String,Object> map = queue.poll();

            logger.info("userId:"+map.get("userId")+" amount:"+map.get("amount"));
            Object  o = sktHuamaobtLogService.recharge(Integer.parseInt(map.get("userId").toString()),Integer.parseInt(map.get("amount").toString()),Integer.parseInt(map.get("drawId").toString()));
            //logger.info("AAAAAAAAAAAAAAAAAAAAAAA："+o.toString());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
