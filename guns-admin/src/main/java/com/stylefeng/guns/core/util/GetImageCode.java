/* 1:  */ package com.stylefeng.guns.core.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;
import javax.imageio.ImageIO;
public class GetImageCode {

    public  void GetImageCode() throws Exception {
        create();
    }

    // 验证码
    private static String code = null;
    // 验证码图片Buffer
    private BufferedImage buffImg=null;

    public static void main(String[] args)throws Exception {
        GetImageCode i=new GetImageCode();
        i.create();
    }

    public void create() throws Exception {
        int []a = null;
        // 大小
        int width = 60;
        int height = 30;
        // 声明一个图片类型rgb
        BufferedImage bi = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_BGR);
        // 获取画笔
        Graphics g = bi.getGraphics();
        // 背景色
        int rad[]={225, 255, 255, 223};
        int green[]={225, 236, 237, 255};
        int blue[]={225, 236, 166, 125};
        Random random = new Random();
        int pick = random.nextInt(3);
        g.setColor(new Color(rad[pick],green[pick],blue[pick]));

        // 画

        g.fillRect(0, 0, width, height);
        // 字体
        g.setFont(new Font("黑体", Font.BOLD, 18));

        // 写一个字符到bi
        Random r = new Random();
        for (int i = 0; i < 4; i++) {
            // 生成随机数
            int codes = r.nextInt(10);// 0---9之间
            // 画笔随机色
            g.setColor(new Color(r.nextInt(200), r.nextInt(120), r.nextInt(120)));
            // 写出字符
            g.drawString("" + codes, i * 15, 10 + r.nextInt(20));
            code=String.valueOf(codes);

        }

        //干扰线

        for(int i=0;i<12;i++){

            g.setColor(new Color(r.nextInt(256), r.nextInt(256), r.nextInt(256)));
            //画线
            g.drawLine(r.nextInt(80), r.nextInt(50), r.nextInt(80), r.nextInt(50));
        }
        g.dispose();//图片生成

    }

    public void write(String path) throws IOException {
        OutputStream sos = new FileOutputStream(path);
        this.write(sos);
    }

    public void write(OutputStream sos) throws IOException {
        ImageIO.write(buffImg, "png", sos);
        sos.close();
    }

    public BufferedImage getBuffImg() {
        return buffImg;
    }

    public static String getCode() {
        return code;
    }
    
}