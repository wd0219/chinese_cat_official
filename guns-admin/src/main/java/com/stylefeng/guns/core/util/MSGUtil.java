package com.stylefeng.guns.core.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class MSGUtil {
    /** 记录日志的变量 */
    private static Logger logger = LoggerFactory.getLogger(MSGUtil.class);

    /**
     * <p>description:统一获取返回的json格式</p>
     * @param err
     * @param msg
     * @return
     */
    public static String getResponseJson(String err,String msg){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(Constants.ERR,err);
        map.put(Constants.MSG, msg);
        String json = JacksonUtil.mapToJson(map);
        return json;
    }

    /**
     * <p>description:统一获取返回的json格式</p>
     * @param statusCode
     * @param msg
     * @param content
     * @return
     */
    public static String getResponseJson(int status,String msg, Object content){
        return getResponseJson(status, msg, content, null);
    }

    /**
     * <p>description:统一获取返回的json格式</p>
     * @param statusCode
     * @param msg
     * @param content
     * @param callback
     * @return
     */
    public static String getResponseJson(int statusCode,String msg, Object content, String callback){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(Constants.STATUS, statusCode);
        map.put(Constants.MSG, msg);
        if(content == null){
            content = "null";
        }
        map.put(Constants.DATA, content);
        String json = JacksonUtil.mapToJson(map);
        if(!StringUtils.isEmpty(callback)){
            json = callback.concat("(").concat(json).concat(")");
        }
        return json;
    }



    /**
     * <p>description:获取忽略空值的json格式(忽略""和NULL)</p>
     * @param err
     * @param msg
     * @return
     */
    public static String getResponseJsonNotEmpty(String err,String msg){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(Constants.ERR, err);
        map.put(Constants.MSG, msg);
        String json = JacksonFilterUtil.mapToJson(map);
        return json;
    }

    /**
     * <p>description:获取忽略空值的json格式(忽略""和NULL)</p>
     * @param statusCode
     * @param msg
     * @param content
     * @return
     */
    public static String getResponseJsonNotEmpty(int statusCode,String msg, Object content){
        return getResponseJsonNotEmpty(statusCode, msg, content, null);
    }

    /**
     * <p>description:获取忽略空值的json格式(忽略""和NULL)</p>
     * @param statusCode
     * @param msg
     * @param content
     * @param callback
     * @return
     */
    public static String getResponseJsonNotEmpty(int statusCode,String msg, Object content, String callback){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(Constants.STATUS, statusCode);
        map.put(Constants.MSG, msg);
        map.put(Constants.DATA, content);
        String json = JacksonFilterUtil.mapToJson(map);
        if(!StringUtils.isEmpty(callback)){
            json = callback.concat("(").concat(json).concat(")");
        }
        return json;
    }


    /**
     * 验证码6位
     */
    public static String getCode() {
        String code = new Random().nextInt(1000000) + "";
        if (code.length() < 6) {
            return getCode();
        }
        return code;
    }

        public static boolean getResponsePhone(String phone){
        String reg = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";
        Pattern p = Pattern.compile(reg);
        Matcher m = p.matcher(phone);
        boolean isMatch = m.matches();
        return isMatch;
    }

}
