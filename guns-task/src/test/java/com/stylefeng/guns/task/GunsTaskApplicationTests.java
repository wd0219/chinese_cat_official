package com.stylefeng.guns.task;

import com.stylefeng.guns.task.modular.huamao.dao.TestMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GunsTaskApplicationTests {

	@Autowired
	TestMapper testMapper;

	@Test
	public void contextLoads() {
		Integer integer = testMapper.get();
		System.out.println(integer);
	}

}
