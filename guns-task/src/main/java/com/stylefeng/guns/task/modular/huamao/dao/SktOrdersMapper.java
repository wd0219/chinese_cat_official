package com.stylefeng.guns.task.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.task.modular.huamao.model.SktOrders;

import java.util.Map;

/**
 * <p>
 * 线上商城订单表 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-08-23
 */
public interface SktOrdersMapper extends BaseMapper<SktOrders> {

    public Integer updateOrderStatus(Map map);
}
