package com.stylefeng.guns.task.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.task.modular.huamao.model.SktOrders;

import java.util.Map;

/**
 * <p>
 * 线上商城订单表 服务类
 * </p>
 *
 * @author caody
 * @since 2018-08-23
 */
public interface ISktOrdersService extends IService<SktOrders> {
    /**
     * 订单过期时更新订单状态
     * @param map
     * @return
     */
    public Object updateOrderStatus(Map map);
}
