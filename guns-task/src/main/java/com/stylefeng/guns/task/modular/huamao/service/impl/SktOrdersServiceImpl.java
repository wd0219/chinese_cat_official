package com.stylefeng.guns.task.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.task.modular.huamao.dao.SktOrdersMapper;
import com.stylefeng.guns.task.modular.huamao.model.SktOrders;
import com.stylefeng.guns.task.modular.huamao.service.ISktOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 线上商城订单表 服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-08-23
 */
@Service
public class SktOrdersServiceImpl extends ServiceImpl<SktOrdersMapper, SktOrders> implements ISktOrdersService {

    @Autowired
    private SktOrdersMapper ordersMapper;

    @Override
    public Object updateOrderStatus(Map map) {
        ordersMapper.updateOrderStatus(map);

        return null;
    }
}
