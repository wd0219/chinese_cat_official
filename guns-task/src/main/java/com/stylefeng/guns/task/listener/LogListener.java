package com.stylefeng.guns.task.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import com.stylefeng.guns.task.constants.RabbitConstants;
import com.stylefeng.guns.task.modular.huamao.model.ReceiveMessage;
import com.stylefeng.guns.task.modular.huamao.mongodb.OperationLog;
import com.stylefeng.guns.task.modular.huamao.mongodb.OperationLog1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;

@Controller
@Slf4j
public class LogListener {
    @Autowired
    private MongoTemplate mongoTemplate;
    /**
     * 正常监听日志队列
     * @param sendMessage
     * @param channel
     * @param message
     * @throws Exception
     */
    @RabbitListener(queues = RabbitConstants.QUEUE_NAME_LOG)
    public void process(String sendMessage, Channel channel, Message message) throws Exception {
        log.info("[{}]处理正常队列消息队列接收数据，消息体：{}", RabbitConstants.QUEUE_NAME_LOG, JSON.toJSONString(sendMessage));
        try {
            // 参数校验
            Assert.notNull(sendMessage, "sendMessage 消息体不能为NULL");
            JSONObject jsonObject = JSONObject.parseObject(sendMessage);

            /**
             * json对象转换成java对象
             */
           // ReceiveMessage receiveMessage = (ReceiveMessage) JSONObject.toJavaObject(jsonObject.getJSONObject("sendMessage"),ReceiveMessage.class);
            OperationLog operationLog = (OperationLog) JSONObject.toJavaObject(jsonObject,OperationLog.class);
            mongoTemplate.save(operationLog);
            log.info("处理MQ消息");
            // 确认消息已经消费成功
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            log.error("MQ消息处理异常，消息ID：{}，消息体:{}", message.getMessageProperties().getCorrelationIdString(),
                    JSON.toJSONString(sendMessage), e);

            // 拒绝当前消息，并把消息返回原队列
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
        }
    }
}
