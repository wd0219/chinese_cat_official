package com.stylefeng.guns.task.config;

import com.stylefeng.guns.task.constants.RabbitConstants;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * RabbitMq配置类
 */
@Configuration
public class RabbitConfig {
    /**
     * 方法rabbitAdmin的功能描述:动态声明queue、exchange、routing
     *
     * @param connectionFactory
     * @return
     */
    @Bean
    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory){
        RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory);
        // 发放奖励队列交换机
        DirectExchange exchange = new DirectExchange(RabbitConstants.MQ_EXCHANGE_LOG);
        Queue couponQueue = logQueue(RabbitConstants.QUEUE_NAME_LOG);
        rabbitAdmin.declareQueue(couponQueue);
        rabbitAdmin.declareExchange(exchange);
        rabbitAdmin.declareBinding(BindingBuilder.bind(couponQueue).to(exchange).with(RabbitConstants.MQ_ROUTING_KEY_LOG));

        // 死信（延迟）交换机（Fanout类型的exchange）
        FanoutExchange orderDeadExchange = new FanoutExchange(RabbitConstants.ORDER_DEAD_QUEUE_EXCHANGE);
        //死信（延迟）队列
        Queue orderDeadQueue = new Queue(RabbitConstants.ORDER_DEAD_QUEUE);
        rabbitAdmin.declareExchange(orderDeadExchange);
        rabbitAdmin.declareQueue(orderDeadQueue);
        rabbitAdmin.declareBinding(BindingBuilder.bind(orderDeadQueue).to(orderDeadExchange));

        // 订单交换机（Direct类型的exchange）
        DirectExchange orderExchange = new DirectExchange(RabbitConstants.MQ_EXCHANGE_ORDER);
        //订单队列
        Queue orderQueue = orderQueue(RabbitConstants.QUEUE_NAME_ORDER);
        rabbitAdmin.declareExchange(orderExchange);
        rabbitAdmin.declareQueue(orderQueue);
        rabbitAdmin.declareBinding(BindingBuilder.bind(orderQueue).to(orderExchange).with(RabbitConstants.MQ_ROUTING_KEY_ORDER));

        return rabbitAdmin;
    }

    /**
     * 日志队列
     * @param name
     * @return
     */
    public Queue logQueue(String name) {
        // 是否持久化
        boolean durable = true;
        // 仅创建者可以使用的私有队列，断开后自动删除
        boolean exclusive = false;
        // 当所有消费客户端连接断开后，是否自动删除队列
        boolean autoDelete = false;
        return new Queue(name, durable, exclusive, autoDelete);
    }

    /**
     * 订单队列
     * @param name
     * @return
     */
    public Queue orderQueue(String name) {
        Map<String, Object> args = new HashMap<>();
        // 设置死信队列
        args.put("x-dead-letter-exchange", RabbitConstants.ORDER_DEAD_QUEUE_EXCHANGE);
        args.put("x-dead-letter-routing-key", RabbitConstants.ORDER_ROUTING_KEY_DEAD_QUEUE);
        // 设置消息的过期时间， 单位是毫秒
        args.put("x-message-ttl", 30*60*1000);

        // 是否持久化
        boolean durable = true;
        // 仅创建者可以使用的私有队列，断开后自动删除
        boolean exclusive = false;
        // 当所有消费客户端连接断开后，是否自动删除队列
        boolean autoDelete = false;
        return new Queue(name, durable, exclusive, autoDelete, args);
    }

}
