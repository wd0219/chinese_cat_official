package com.stylefeng.guns.task.modular.huamao.controller;

import com.stylefeng.guns.task.modular.huamao.mongodb.OperationLog1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/mongodb")
public class mongodbController {
    @Autowired
    private MongoTemplate mongoTemplate;

    @RequestMapping(value = "/getLog")
    public Object getLog(){
        Integer currentPage = 1;
        Integer pageSize = 10;
        Query query = new Query();
        query.with(new Sort(Sort.Direction.DESC,"createtime"));
        query.skip((currentPage-1)*pageSize);
        query.limit(pageSize);
        List<OperationLog1> logs = mongoTemplate.find(query,OperationLog1.class);
        return logs;
    }
}
