package com.stylefeng.guns.task.modular.huamao.model;

import lombok.Data;


@Data
public class ReceiveMessage {


    private long id;

    private String name;

    private int age;
}
