package com.stylefeng.guns.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = {"com.stylefeng.guns"})
public class GunsTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(GunsTaskApplication.class, args);
	}
}
