package com.stylefeng.guns.task.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

public interface TestMapper extends BaseMapper{

    Integer get();
}
