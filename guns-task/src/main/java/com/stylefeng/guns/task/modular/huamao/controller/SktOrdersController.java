package com.stylefeng.guns.task.modular.huamao.controller;


import com.stylefeng.guns.task.modular.huamao.model.SktOrders;
import com.stylefeng.guns.task.modular.huamao.service.ISktOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 线上商城订单表 前端控制器
 * </p>
 *
 * @author caody
 * @since 2018-08-23
 */
@Controller
@RequestMapping("/sktOrders")
public class SktOrdersController {

    @Autowired
    private ISktOrdersService sktOrdersService;

    @RequestMapping("/getValue")
    @ResponseBody
    public Object getValue(){
        SktOrders sktOrders = new SktOrders();
        sktOrders = sktOrdersService.selectById(1);
        return sktOrders;
    }
}

