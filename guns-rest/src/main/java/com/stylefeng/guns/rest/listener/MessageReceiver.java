package com.stylefeng.guns.rest.listener;

import com.stylefeng.guns.modular.huamao.service.ISktOrdersService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * redis监听触发的方法
 */
@Component
public class MessageReceiver {

    @Autowired
    private ISktOrdersService sktOrdersService;

    /**接收消息的方法*/
    public void receiveMessage(String message){
        System.out.println("收到定时任务消息："+message);
    }

    /**
     * redis的key过期触发的方法
     * 从定时触发改为key过期触发
     * @param message
     */
    public void receiveKeyMessage(String message){
        String[] str =  message.split(":");
        if (str[0].equals("redis_key")){
            if (str[1].equals("orderNo")){

            }
        }
        sktOrdersService.orderBatch();
    }
}
