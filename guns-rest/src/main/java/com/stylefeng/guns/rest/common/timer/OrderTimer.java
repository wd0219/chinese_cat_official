package com.stylefeng.guns.rest.common.timer;


import com.stylefeng.guns.modular.huamao.service.ISktOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;



@Component
public class OrderTimer {

    @Autowired
    private ISktOrdersService  sktOrdersService;

//    @Scheduled(cron = "0 0/30 * * * ?")
    private void orderBatch() {
        sktOrdersService.orderBatch();
        //System.out.println("执行定时任务的时间是："+new Date());
    }
}
