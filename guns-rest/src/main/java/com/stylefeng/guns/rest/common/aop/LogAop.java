package com.stylefeng.guns.rest.common.aop;

import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.mongodb.OperationLog;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.RabbitSender;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import com.stylefeng.guns.rest.common.constants.RabbitConstants;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.ArithmeticOperators;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Aspect
@Component
@Slf4j
public class LogAop {

    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private RabbitSender rabbitSender;



    @Pointcut("execution(public * com.stylefeng.guns.modular.huamao.controller.*.*(..))")
    public void logAop(){};

    @Before("logAop()")
    public void around(JoinPoint joinPoint){
        log.info("user:cdy");
        log.info("time:"+new Date());
        log.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature
                ().getName());
        log.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        String logname = null;
        if (user != null){
            logname = user.getLoginName();
        }
        OperationLog operationLog = new OperationLog();
        operationLog.setLogname(logname);
        operationLog.setLogTypeId("1");
        operationLog.setLogtype("业务日志");
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String s = simpleDateFormat.format(new Date());
        operationLog.setCreatetime(s);
        operationLog.setUserid(1);
        operationLog.setClassname(joinPoint.getSignature().getDeclaringTypeName());
        operationLog.setMethod(joinPoint.getSignature().getName());
        operationLog.setSucceed("成功");
        operationLog.setMessage("");
        JSONObject jsonObject = (JSONObject)JSONObject.toJSON(operationLog);
        String content = jsonObject.toJSONString();
        //rabbitSender.sendMessage(RabbitConstants.MQ_EXCHANGE_LOG, RabbitConstants.MQ_ROUTING_KEY_LOG, content);
        mongoTemplate.save(operationLog);
    }

//    @AfterReturning(returning = "object", pointcut = "logAop()")
//    public void after(Object object){
//        System.out.println(object);
//        log.info("RESPONSE : " + object);
//    }

    @AfterThrowing(pointcut = "logAop()", throwing="e")
    public  void  afterThrowing(JoinPoint joinPoint, Throwable e){
        log.info("user:cdy");
        log.info("time:"+new Date());
        log.info("path : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature
                ().getName());
        log.info("param : " + Arrays.toString(joinPoint.getArgs()));
        log.info("异常代码:" + e.getClass().getName());
        log.info("异常信息:" + e.getMessage());

        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        String logname = null;
        if (user != null){
            logname = user.getLoginName();
        }
        OperationLog operationLog = new OperationLog();
        operationLog.setLogname(logname);
        operationLog.setLogTypeId("2");
        operationLog.setLogtype("异常日志");
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String s = simpleDateFormat.format(new Date());
        operationLog.setCreatetime(s);
        operationLog.setUserid(1);
        operationLog.setClassname(joinPoint.getSignature().getDeclaringTypeName());
        operationLog.setMethod(joinPoint.getSignature().getName());
        operationLog.setSucceed("失败");
        operationLog.setMessage(e.getMessage());
        JSONObject jsonObject = (JSONObject)JSONObject.toJSON(operationLog);
        String content = jsonObject.toJSONString();
        //rabbitSender.sendMessage(RabbitConstants.MQ_EXCHANGE_LOG, RabbitConstants.MQ_ROUTING_KEY_LOG, content);
        mongoTemplate.save(operationLog);

    }
}
