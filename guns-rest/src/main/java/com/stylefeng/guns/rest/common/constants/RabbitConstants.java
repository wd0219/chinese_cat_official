package com.stylefeng.guns.rest.common.constants;

public class RabbitConstants {
    /**
     * 日志队列EXCHANGE名称
     */
    public static final String MQ_EXCHANGE_LOG = "mq-exchange-log";

    /**
     * 日志队列名称
     */
    public static final String QUEUE_NAME_LOG = "queue-name-log";

    /**
     * 日志路由key
     */
    public static final String MQ_ROUTING_KEY_LOG = "mq_routing_key_log";

    /*****************************延迟队列*****************************/
    /**
     * 死信(延迟)队列EXCHANGE名称
     */
    public static final String ORDER_DEAD_QUEUE_EXCHANGE = "order-dead-queue-exchange";

    /**
     * 死信（延迟）队列名称
     */
    public static final String ORDER_DEAD_QUEUE = "order-dead-queue";

    /**
     * 死信（延迟）队列路由名称
     */
    public static final String ORDER_ROUTING_KEY_DEAD_QUEUE = "order-routing-key-dead-queue";

    /**
     * 订单EXCHANGE名称
     */
    public static final String MQ_EXCHANGE_ORDER = "mq-exchange-order";

    /**
     * 订单队列名称
     */
    public static final String QUEUE_NAME_ORDER= "queue-name-order";

    /**
     * 订单路由key
     */
    public static final String MQ_ROUTING_KEY_ORDER = "mq-routing-key-order";

}
