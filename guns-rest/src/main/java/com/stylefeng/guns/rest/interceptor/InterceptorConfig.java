package com.stylefeng.guns.rest.interceptor;

import com.stylefeng.guns.modular.huamao.util.StringUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class InterceptorConfig implements HandlerInterceptor {
    private static final String auth_token ="IX9vaE1Z902Bs2k0";
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        String AUTH   = httpServletRequest.getHeader("AUTH");
        String[] split = AUTH.split("\\|");
        String algo = split[0];
        String expTime= split[1];
        String nonce=split[2];
        String signature = split[3];
        StringBuffer sb = new StringBuffer();
        StringBuffer append = sb.append(expTime).append(auth_token).append(nonce);
        String s = DigestUtils.sha1Hex(append.toString());
        return signature.equals(s);
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
