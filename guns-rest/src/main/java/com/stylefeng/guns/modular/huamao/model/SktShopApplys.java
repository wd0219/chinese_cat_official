package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 商城商家开店申请表
 * </p>
 *
 * @author ck123
 * @since 2018-04-10
 */
@TableName("skt_shop_applys")
public class SktShopApplys extends Model<SktShopApplys> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "applyId", type = IdType.AUTO)
    private Integer applyId;
    /**
     * 申请用户ID
     */
    private Integer userId;
    /**
     * 店铺Id
     */
    private Integer shopId;
    /**
     * 申请失败原因
     */
    private String applyDesc;
    /**
     * 申请状态：1未审核 2拒绝 3审核通过
     */
    private Integer applyStatus;
    /**
     * 处理结果说明
     */
    private String handleDesc;
    /**
     * 删除标志 -1:删除 1:有效
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 审核时间
     */
    private Date checkTime;
    /**
     * 审核人
     */
    private Integer checkStaffId;
    private String telephone;

    public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getApplyDesc() {
        return applyDesc;
    }

    public void setApplyDesc(String applyDesc) {
        this.applyDesc = applyDesc;
    }

    public Integer getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(Integer applyStatus) {
        this.applyStatus = applyStatus;
    }

    public String getHandleDesc() {
        return handleDesc;
    }

    public void setHandleDesc(String handleDesc) {
        this.handleDesc = handleDesc;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Integer getCheckStaffId() {
        return checkStaffId;
    }

    public void setCheckStaffId(Integer checkStaffId) {
        this.checkStaffId = checkStaffId;
    }

    @Override
    protected Serializable pkVal() {
        return this.applyId;
    }

	@Override
	public String toString() {
		return "SktShopApplys [applyId=" + applyId + ", userId=" + userId + ", shopId=" + shopId + ", applyDesc="
				+ applyDesc + ", applyStatus=" + applyStatus + ", handleDesc=" + handleDesc + ", dataFlag=" + dataFlag
				+ ", createTime=" + createTime + ", checkTime=" + checkTime + ", checkStaffId=" + checkStaffId
				+ ", telephone=" + telephone + "]";
	}

	public SktShopApplys(Integer applyId, Integer userId, Integer shopId, String applyDesc, Integer applyStatus,
			String handleDesc, Integer dataFlag, Date createTime, Date checkTime, Integer checkStaffId,
			String telephone) {
		super();
		this.applyId = applyId;
		this.userId = userId;
		this.shopId = shopId;
		this.applyDesc = applyDesc;
		this.applyStatus = applyStatus;
		this.handleDesc = handleDesc;
		this.dataFlag = dataFlag;
		this.createTime = createTime;
		this.checkTime = checkTime;
		this.checkStaffId = checkStaffId;
		this.telephone = telephone;
	}

	public SktShopApplys() {
		super();
		// TODO Auto-generated constructor stub
	}
    
}
