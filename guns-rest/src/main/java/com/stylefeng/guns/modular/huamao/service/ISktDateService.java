package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktDate;

public interface ISktDateService extends IService<SktDate> {
    SktDate selectDateByDate(int date);
}
