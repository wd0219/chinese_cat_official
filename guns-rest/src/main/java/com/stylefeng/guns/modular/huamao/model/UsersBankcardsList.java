package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户的银行卡表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public class UsersBankcardsList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	private Integer bankCardId;
	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 昵称
	 */
	private String loginName;
	/**
	 * 真名
	 */
	private String trueName;
	/**
	 * 银行简称
	 */
	private String bankName;
	/**
	 * 是否是默认银行卡 1默认 2不是
	 */
	private Integer type;
	/**
	 * 省id
	 */
	private Integer provinceId;
	/**
	 * 市id
	 */
	private Integer cityId;
	/**
	 * 区县id
	 */
	private Integer areaId;
	/**
	 * 银行ID
	 */
	private Integer bankId;
	/**
	 * 开户行
	 */
	private String accArea;
	/**
	 * 银行卡号
	 */
	private String accNo;
	/**
	 * 银行预留手机号码
	 */
	private String phone;
	/**
	 * 有效标志
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 身份证号
	 */
	private String cardID;
	public UsersBankcardsList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsersBankcardsList(Integer bankCardId, String userPhone, Integer userId, String loginName, String trueName,
			String bankName, Integer type, Integer provinceId, Integer cityId, Integer areaId, Integer bankId,
			String accArea, String accNo, String phone, Integer dataFlag, Date createTime) {
		super();
		this.bankCardId = bankCardId;
		this.userPhone = userPhone;
		this.userId = userId;
		this.loginName = loginName;
		this.trueName = trueName;
		this.bankName = bankName;
		this.type = type;
		this.provinceId = provinceId;
		this.cityId = cityId;
		this.areaId = areaId;
		this.bankId = bankId;
		this.accArea = accArea;
		this.accNo = accNo;
		this.phone = phone;
		this.dataFlag = dataFlag;
		this.createTime = createTime;
	}

	public Integer getBankCardId() {
		return bankCardId;
	}

	public void setBankCardId(Integer bankCardId) {
		this.bankCardId = bankCardId;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	public String getAccArea() {
		return accArea;
	}

	public void setAccArea(String accArea) {
		this.accArea = accArea;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCardID() {
		return cardID;
	}

	public void setCardID(String cardID) {
		this.cardID = cardID;
	}

	@Override
	public String toString() {
		return "UsersBankcardsList [bankCardId=" + bankCardId + ", userPhone=" + userPhone + ", userId=" + userId
				+ ", loginName=" + loginName + ", trueName=" + trueName + ", bankName=" + bankName + ", type=" + type
				+ ", provinceId=" + provinceId + ", cityId=" + cityId + ", areaId=" + areaId + ", bankId=" + bankId
				+ ", accArea=" + accArea + ", accNo=" + accNo + ", phone=" + phone + ", dataFlag=" + dataFlag
				+ ", createTime=" + createTime + "]";
	}

}
