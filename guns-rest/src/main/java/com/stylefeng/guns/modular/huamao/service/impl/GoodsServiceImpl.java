package com.stylefeng.guns.modular.huamao.service.impl;


import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.service.ISktShopsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.dao.AttributesMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsAttributesMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsScoresMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsSpecsMapper;
import com.stylefeng.guns.modular.huamao.dao.SktAccountSystemMapper;
import com.stylefeng.guns.modular.huamao.dao.SktSysConfigsMapper;
import com.stylefeng.guns.modular.huamao.dao.SpecCatsMapper;
import com.stylefeng.guns.modular.huamao.dao.SpecItemsMapper;
import com.stylefeng.guns.modular.huamao.service.IGoodsService;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {
	@Autowired
	private GoodsMapper goodsMapper;
	@Autowired
	private GoodsScoresMapper goodsScoresMapper;
	@Autowired
	private AttributesMapper attributesMapper;
	@Autowired
	private SpecCatsMapper specCatsMapper;
	@Autowired
	private SpecItemsMapper specItemsMapper;
	@Autowired
	private GoodsSpecsMapper goodsSpecsMapper;
	@Autowired
	private GoodsAttributesMapper goodsAttributesMapper;
	@Autowired
	private SktSysConfigsMapper sktSysConfigsMapper;
	@Autowired
	private ISktShopsService sktShopsService;
	/**
	 * 通过分类查询商品，商标信息
	 */
	@Override
	public List<Map<String, Object>> selectByCatName(List<String> item){
		List<Map<String, Object>>  temp=  goodsMapper.selectByCatName(item);
		return temp;
	}
	/**
	 * 通过商品id查询商品分类级别
	 */
	@Override
	public GoodsCatsLevel selectCatsLevelByGoodsid(Integer goodsId) {
		return goodsMapper.selectCatsLevelByGoodsid(goodsId);
	}
	
	@Override
	public List<Map<String, Object>> selectByConditions(GoodsCustm goodsCustm) {
		// TODO Auto-generated method stub
		return baseMapper.selectByConditions(goodsCustm);
	}

	@Override
	public List<Goods> selectGoodsByGoodsCatId(int id) {
		// TODO Auto-generated method stub
		return baseMapper.selectGoodsByGoodsCatId(id);
	}

	@Override
	public int batchUpdate(List<Map<String, Object>> list) {
		// TODO Auto-generated method stub
		return baseMapper.batchUpdate(list);
	}

	@Override
	public int batchUpdatePo(List<Goods> list) {
		// TODO Auto-generated method stub
		return baseMapper.batchUpdatePo(list);
	}

	@Override
	public int selectisSaleCount() {
		// TODO Auto-generated method stub
		return baseMapper.selectisSaleCount();
	}

	@Override
	public int selectGoodsStatusCount() {
		// TODO Auto-generated method stub
		return baseMapper.selectGoodsStatusCount();
	}

	@Override
	public int selectisSaleTotal() {
		// TODO Auto-generated method stub
		return baseMapper.selectisSaleTotal();
	}

	@Override
	public int selectGoodsStatusTotal() {
		// TODO Auto-generated method stub
		return baseMapper.selectGoodsStatusTotal();
	}

	@Override
	public List<Map<String, Object>> TodayHotSale() {
		List<Map<String,Object>> list = goodsMapper.selectTodayHotSale();
		return list;
	}
	
	@Override
	public List<Map<String,Object>> selectTodayHotSaleAll(Integer bandId){
		List<Map<String,Object>> list = goodsMapper.selectTodayHotSaleAll(bandId);
		return list;
	}
	@Override
	public List<Map<String, Object>> todayHotSaleGoods(Map<String, Object> map) {
		List<Map<String,Object>> list = goodsMapper.todayHotSaleGoods( map);
		return list;
	}
	@Override
	public List<GoodsSellStock> selectByIsSale(GoodsSellStock goodsSellStock) {
		List<GoodsSellStock> list = goodsMapper.selectByIsSale(goodsSellStock);
		return list;
	}
	@Override
	public List<GoodsSellStock> selectByIsGoodsStatus(GoodsSellStock goodsSellStock) {
		List<GoodsSellStock> list = goodsMapper.selectByIsGoodsStatus(goodsSellStock);
		return list;
	}
	@Override
	public List<GoodsSellStock> selectByIsWarnStock(GoodsSellStock goodsSellStock) {
		List<GoodsSellStock> list = goodsMapper.selectByIsWarnStock(goodsSellStock);
		return list;
	}
	@Override
	public List<GoodsAppraisesDTO> selectByAppraises(Integer shopId, Integer shopCatId1, Integer shopCatId2) {
		//查出多有订单
		List<GoodsAppraisesDTO> orderlist = goodsMapper.selectAllOrder(shopId,shopCatId1,shopCatId2);
		if(ToolUtil.isNotEmpty(orderlist)){
			Iterator<GoodsAppraisesDTO> it = orderlist.iterator();
			while(it.hasNext()){
				GoodsAppraisesDTO goodsAppraisesDTO =  it.next();
				List<Map<String,Object>> list = goodsMapper.selectAppraise(goodsAppraisesDTO.getOrderId());
//				if(ToolUtil.isEmpty(list)){
//					it.remove();
//				}
				goodsAppraisesDTO.setAppraises(list);
				
			}
		}
		
		return orderlist;
	}
	@Override
	public boolean updateStatus(Integer[] goodId, Integer isSale, Integer isBest, Integer isHot, Integer isNew,
			Integer isRecom, Integer isDelete) {
		Integer updateStatus = goodsMapper.updateStatus(goodId,isSale,isBest,isHot,isNew,isRecom,isDelete);
		return updateStatus==0?false:true;
	}
	@Override
	public List<Map<String, Object>> selectDefaultProperty(String[] defaultProperty) {
		List<Map<String,Object>> list = goodsMapper.selectDefaultProperty(defaultProperty);
		return list;
	}
	@Override
	public List<Map<String,Object>> selectByGoods(GoodsList goods) {
		List<Map<String,Object>> list = goodsMapper.selectByGoods(goods);
		return list;
	}
	@Override
	@Transactional
	public Integer insertAdd(Goods goods) {
        //判断商品编号、货号是否存在
//        Goods goods1 = new Goods();
//        goods1.setGoodsSn(goods.getGoodsSn());
//        EntityWrapper<Goods> ew = new EntityWrapper<Goods>(goods1);
//        List<Goods> goodsList = goods.selectList(ew);
//        if (goodsList.size() > 0) {
//            return 0;
//        }
//        Goods goods2 = new Goods();
//        goods2.setProductNo(goods.getProductNo());
//        EntityWrapper<Goods> ew2 = new EntityWrapper<Goods>(goods2);
//        List<Goods> goodsList2 = goods.selectList(ew2);
//        if (goodsList2.size() > 0) {
//            return 0;
//        }
		//查看是否审核
		SktSysConfigs sysConfig = new SktSysConfigs();
		sysConfig.setFieldCode("isGoodsVerify");
		SktSysConfigs selectOne = sktSysConfigsMapper.selectOne(sysConfig);
		if("0".equals(selectOne.getFieldValue())){
			goods.setGoodsStatus(0);
			goods.setIsSale(0);
		}else{
			goods.setGoodsStatus(1);
			goods.setIsSale(1);
		}
		
		if(ToolUtil.isEmpty(goods.getShopCatId1())){
			goods.setShopCatId1(0);
		}
		if(ToolUtil.isEmpty(goods.getShopCatId2())){
			goods.setShopCatId2(0);
		}
		goods.setSaleTime(new Date());
		goods.setCreateTime(new Date());
		Integer insert = goodsMapper.insert(goods);
		//添加商品评分
		GoodsScores goodsScores = new GoodsScores();
		goodsScores.setGoodsId(goods.getGoodsId());
		goodsScores.setShopId(goods.getShopId());
		Integer insert2 = goodsScoresMapper.insert(goodsScores);
		if(insert == 0 && insert2==0 ){
			return 0;
		}
		return goods.getGoodsId();
	}
	@Override
	public List<Attributes> selectGoodsAttr(GoodsList goods) {
		Attributes attributes = new Attributes();
		attributes.setGoodsCatId(goods.getGoodsCatId());
		EntityWrapper<Attributes> ewap=new EntityWrapper<Attributes>(attributes);
		List<Attributes> list = attributesMapper.selectList(ewap);
		return list;
	}
	@Override
	public List<SpecCats> selectGoodsCat(GoodsList goods) {
		SpecCats attributes = new SpecCats();
		attributes.setGoodsCatId(goods.getGoodsCatId());     
		EntityWrapper<SpecCats> ewap=new EntityWrapper<SpecCats>(attributes);
		ewap.orderBy("catSort");
		List<SpecCats> list = specCatsMapper.selectList(ewap);
		return list;
	}
	@Override
	@Transactional
	public boolean addAttrCat(Goods goods, List<SpecItems> specItemss, List<GoodsSpecs> goodsSpecss,
			List<GoodsAttributes> goodsAttributess) {
		try {
			//判断商品编号、货号是否存在
			Goods goods1 = new Goods();
			goods1.setGoodsSn(goods.getGoodsSn());
			EntityWrapper<Goods> ew = new EntityWrapper<Goods>(goods1);
			List<Goods> goodsList = goods.selectList(ew);
			if (goodsList.size() > 0) {
				return false;
			}
			Goods goods2 = new Goods();
			goods2.setProductNo(goods.getProductNo());
			EntityWrapper<Goods> ew2 = new EntityWrapper<Goods>(goods2);
			List<Goods> goodsList2 = goods.selectList(ew2);
			if (goodsList2.size() > 0) {
				return false;
			}
			//新增goods表
			Date date = new Date();
			goods.setDataFlag(1);
			goods.setCreateTime(date);
			Integer insert = goodsMapper.insert(goods);
			//添加商品评分
			GoodsScores goodsScores = new GoodsScores();
			goodsScores.setGoodsId(goods.getGoodsId());
			goodsScores.setShopId(goods.getShopId());
			Integer insert2 = goodsScoresMapper.insert(goodsScores);
			if(insert == 0 && insert2==0 ){
				return false;
			}
			
			if(specItemss.size()>0){
				//添加商品规格
				for(SpecItems specItems : specItemss){
					specItems.setGoodsId(goods.getGoodsId());
					specItems.setCreateTime(date);
					specItems.setDataFlag(1);
					specItemsMapper.insert(specItems);
				}
			}
			if(goodsSpecss.size()>0){
				//添加销售规格
				for(GoodsSpecs goodsSpecs : goodsSpecss){
					goodsSpecs.setGoodsId(goods.getGoodsId());
					goodsSpecs.setDataFlag(1);
					goodsSpecsMapper.insert(goodsSpecs);
				}
			}
			if(goodsAttributess.size()>0){
				//添加商品属性
				for(GoodsAttributes goodsAttributes : goodsAttributess){
					goodsAttributes.setGoodsId(goods.getGoodsId());
					goodsAttributes.setCreateTime(date);
					goodsAttributesMapper.insert(goodsAttributes);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public BigDecimal selectMax() {
		Integer randm = (int)((Math.random()*9+1)*1000);
		String max = goodsMapper.selectMax();
		BigDecimal ma1 = new BigDecimal(max);
		BigDecimal ma2 = new BigDecimal(randm);
		BigDecimal ma3 = ma1.add(ma2);
		System.out.println(ma3);
		return ma3;
	}

	@Override
	public List<Map<String, Object>> getCatBrands(Integer catId) {
		List<Map<String, Object>> list = goodsMapper.getCatBrands(catId);
		return list;
	}
	@Override
	public boolean addAttrCat2(Integer goodsId, List<SpecItems> specItemss, List<GoodsSpecs> goodsSpecss,
			List<GoodsAttributes> goodsAttributess) {
		try {
			//删除以前表里的数据
			DeleteTableData(goodsId);
			Date date = new Date();
			if(specItemss != null && specItemss.size()>0 ){
				//添加商品规格
				for(SpecItems specItems : specItemss){
					specItems.setGoodsId(goodsId);
					specItems.setCreateTime(date);
					if(ToolUtil.isEmpty(specItems.getItemImg())){
						specItems.setItemImg("");
					}
					specItems.setItemDesc("");
					specItems.setDataFlag(1);
					specItemsMapper.insert(specItems);
				}
			}
			if(goodsSpecss.size()>0){
				//添加销售规格
				for(GoodsSpecs goodsSpecs : goodsSpecss){
					goodsSpecs.setGoodsId(goodsId);
					goodsSpecs.setDataFlag(1);
					goodsSpecsMapper.insert(goodsSpecs);
				}
			}
			if(goodsAttributess.size()>0){
				//添加商品属性
				for(GoodsAttributes goodsAttributes : goodsAttributess){
					goodsAttributes.setGoodsId(goodsId);
					goodsAttributes.setCreateTime(date);
					goodsAttributesMapper.insert(goodsAttributes);
				}
			}
			Goods goods = new Goods();
			goods.setGoodsId(goodsId);
			goods.setIsSale(1);
			goods.setIsSpec(1);
			goods.setGoodsStatus(0);
			goodsMapper.updateById(goods);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public void DeleteTableData(Integer goodsId){
			EntityWrapper<GoodsAttributes> ew = new EntityWrapper<GoodsAttributes>();
			ew.where("goodsId={0}",goodsId);
			goodsAttributesMapper.delete(ew);
			EntityWrapper<GoodsSpecs> ew2 = new EntityWrapper<GoodsSpecs>();
			ew2.where("goodsId={0}",goodsId);
			goodsSpecsMapper.delete(ew2);
//			EntityWrapper<SpecItems> ew3 = new EntityWrapper<SpecItems>();
//			ew3.where("goodsId={0}",goodsId);
//			specItemsMapper.delete(ew3);
	}
	@Override
	public boolean updateAttrCat(List<SpecItems> specItemss, List<GoodsSpecs> goodsSpecss,
			List<GoodsAttributes> goodsAttributess) {
		try {
			if(specItemss.size()>0){
				//添加商品规格
				for(SpecItems specItems : specItemss){
					specItemsMapper.updateById(specItems);
				}
			}
			if(goodsSpecss.size()>0){
				//添加销售规格
				for(GoodsSpecs goodsSpecs : goodsSpecss){
					goodsSpecsMapper.updateById(goodsSpecs);
				}
			}
			if(goodsAttributess.size()>0){
				//添加商品属性
				for(GoodsAttributes goodsAttributes : goodsAttributess){
					goodsAttributesMapper.updateById(goodsAttributes);
				}
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Transactional
	@Override
	public boolean deleteGoods(Integer goodsId) {
		try {
			//删除商品
			Goods goods = new Goods();
			goods.setGoodsId(goodsId);
			goods.setDataFlag(-1);
			goodsMapper.updateById(goods);
			//删除商品规格
//			SpecItems specItems = new SpecItems();
//			specItems.setDataFlag(-1);
//			EntityWrapper<SpecItems> gIEntityWrapper = new EntityWrapper<SpecItems>();
//			gIEntityWrapper.where("goodsId={0}", goodsId);
//			specItemsMapper.update(specItems, gIEntityWrapper);
//			//删除销售规格
//			GoodsSpecs goodsSpecs = new GoodsSpecs();
//			goodsSpecs.setDataFlag(-1);
//			EntityWrapper<GoodsSpecs> gsEntityWrapper = new EntityWrapper<GoodsSpecs>();
//			gsEntityWrapper.where("goodsId={0}", goodsId);
//			goodsSpecsMapper.update(goodsSpecs, gsEntityWrapper);
//			//删除商品属性
//			GoodsAttributes goodsAttributes = new GoodsAttributes();
//			EntityWrapper<GoodsAttributes> gaEntityWrapper = new EntityWrapper<GoodsAttributes>();
//			gaEntityWrapper.where("goodsId={0}", goodsId);
//			goodsAttributesMapper.delete(gaEntityWrapper);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	@Override
	public List<Map<String, Object>> selectByGoodsId(Integer goodsId) {
		List<Map<String, Object>> list = goodsMapper.selectByGoodsId(goodsId);
		return list;
	}
	@Override
	public List<Map<String, Object>> selectAttrShow(Integer goodsId, Integer shopId) {
		List<Map<String,Object>> list = goodsMapper.selectAttrShow(goodsId,shopId);
		return list;
	}
	@Override
	public List<Map<String, Object>> selectSpecShow(Integer goodsId, Integer shopId) {
		List<Map<String,Object>> list = goodsMapper.selectSpecShow(goodsId,shopId);
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> selectCatShow(Integer goodsId, Integer shopId) {
		GoodsSpecs goodsSpecs = new GoodsSpecs();
		goodsSpecs.setGoodsId(goodsId);
		goodsSpecs.setShopId(shopId);
		goodsSpecs.setDataFlag(1);
		EntityWrapper<GoodsSpecs> ew = new EntityWrapper<>(goodsSpecs);
		List<Map<String,Object>> list = new ArrayList<>();
		List<GoodsSpecs> goodsSpecsList = goodsSpecsMapper.selectList(ew);
		for(GoodsSpecs goodsSpecs2 : goodsSpecsList){
//			String[] specIds = goodsSpecs2.getSpecIds().split(":");
//			List<Map<String,Object>> list2 = goodsMapper.selectMaps
			list.add(toMap(goodsSpecs2));
		}
		return list;
	}
	
	/**  
     * 将一个 JavaBean 对象转化为一个 Map  
     * @param bean 要转化的JavaBean 对象  
     * @return 转化出来的 Map 对象  
     * @throws IntrospectionException 如果分析类属性失败  
     * @throws IllegalAccessException 如果实例化 JavaBean 失败  
     * @throws InvocationTargetException 如果调用属性的 setter 方法失败  
     */    
    @SuppressWarnings("rawtypes")    
    public static Map toMap(Object bean) {    
        Class<? extends Object> clazz = bean.getClass();    
        Map<Object, Object> returnMap = new HashMap<Object, Object>();    
        BeanInfo beanInfo = null;    
        try {    
            beanInfo = Introspector.getBeanInfo(clazz);    
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();    
            for (int i = 0; i < propertyDescriptors.length; i++) {    
                PropertyDescriptor descriptor = propertyDescriptors[i];    
                String propertyName = descriptor.getName();    
                if (!propertyName.equals("class")) {    
                    Method readMethod = descriptor.getReadMethod();    
                    Object result = null;    
                    result = readMethod.invoke(bean, new Object[0]);    
                    if (null != propertyName) {    
                        propertyName = propertyName.toString();    
                    }    
                    if (null != result) {    
                        result = result.toString();    
                    }    
                    returnMap.put(propertyName, result);    
                }    
            }    
        } catch (IntrospectionException e) {    
            System.out.println("分析类属性失败");    
        } catch (IllegalAccessException e) {    
            System.out.println("实例化 JavaBean 失败");    
        } catch (IllegalArgumentException e) {    
            System.out.println("映射错误");    
        } catch (InvocationTargetException e) {    
            System.out.println("调用属性的 setter 方法失败");    
        }    
        return returnMap;    
    }
    /***************************************App*****************************/
    @Override
	public List<Map<String, Object>> selectAppByOrder(Map<String, Object> map){
//		Map<String, Object> map=new HashMap<>();
//		map.put("catId",goodsCatIdPath);
//		map.put("order",order);
//		map.put("shopId",shopId);
		return goodsMapper.selectAppByOrder(map);
	}
	@Override
	public List<Goods> selectByShopId(Integer shopId){
		Goods goods = new Goods();
		goods.setShopId(shopId);
		EntityWrapper<Goods> entityWrapper =new EntityWrapper<Goods>(goods);
		return this.selectList(entityWrapper);
	}
	@Override
	public boolean deleteBathGoods(Integer[] goodsId) {
		Integer deleteBatchIds = goodsMapper.deleteBatchIds(goodsId);
		if(ToolUtil.isEmpty(deleteBatchIds)){
			return false;
		}
		return true;
	}
	@Override
	public Map<String, Object> selectGoodsShopCat(Integer goodsId) {
		Map<String,Object> map = goodsMapper.selectGoodsShopCat(goodsId);
		return map;
	}
	@Override
	public boolean deleteAttrCat_two(Integer goodsId) {
		try {
			DeleteTableData(goodsId);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}
	@Override
	public List<GoodsAppraisesShow> selectGoodsAppraises(Integer goodsId) {
		List<GoodsAppraisesShow> list = goodsMapper.selectGoodsAppraises(goodsId);
		return list;
	}
	public List<Map<String,Object>> selectRecomGoods(Map<String,Object> map){
		return goodsMapper.selectRecomGoods(map);
	}

	@Override
	public Map<String, Object> selectGoodsScores(Integer goodsId) {
		Map<String, Object> map = goodsMapper.selectGoodsScores(goodsId);
		return map;
	}
	@Override
	public List<Map<String, Object>> selectGoodsHots(Integer shopId) {
		List<Map<String, Object>> list = goodsMapper.selectGoodsHots(shopId);
		return list;
	}
	@Override
	public List<Goods> guessLikeGoods(Integer shopId) {
		EntityWrapper<Goods> ew = new EntityWrapper<Goods>();
		ew.where("shopId={0}", shopId).and("isSale={0}",1).and("goodsStatus={0}",1);
		List<Goods> selectList = goodsMapper.selectList(ew);
		if(selectList.size()<=6){
			return selectList;
		}
		Set<Integer> set = rand(selectList.size());
		List<Goods> retList = new ArrayList<Goods>();
		Iterator<Integer> it = set.iterator();
		while(it.hasNext()){
			retList.add(selectList.get(it.next()));
		}
		return retList;
	}

	public Set<Integer> rand(Integer size){
		Set<Integer> set = new HashSet<Integer>();
		while(set.size() < 6){
			int number = new Random().nextInt(size);
			set.add(number);
		}
	    return set;
	}

	@Override
	public Set<Integer> appFindShopsByGoodsId(List<Integer> goodsIds) {
		return goodsMapper.appFindShopsByGoodsId(goodsIds);
	}
	@Override
	public List<Map<String, Object>> selectDownSelect(String gType) {
		List<Map<String, Object>> list = goodsMapper.selectDownSelect(gType);
		return list;
	}
	@Override
	public List<Map<String, Object>> selectRecomGoodsSelf(Map<String, Object> map) {
		List<Map<String, Object>> list = goodsMapper.selectRecomGoodsSelf(map);
		return list;
	}
	@Override
    public List<Map<String, Object>>  goodsShopSort (Map<String, Object> map){
        List<Map<String, Object>> list = goodsMapper.goodsShopSort(map);
        return list;
    }
	
	@SuppressWarnings({"rawtypes" })
	@Override
	@Transactional
	public List<Map<String, Object>> addGoodsCat(Integer shopId, Integer goodsId, String specItems) {
		
		List<Map> mapListJson = JSONArray.parseArray(specItems, Map.class);
		// 新增商品规格
		for(Map specItem : mapListJson){
			//更新
			if(specItem.containsKey("itemId") && ToolUtil.isNotEmpty(specItem.get("itemId"))){
				SpecItems sp = new SpecItems();
				sp.setItemId(Integer.parseInt(specItem.get("itemId").toString()));
				sp.setCatId(Integer.parseInt(specItem.get("catId").toString()));
				if(specItem.containsKey("val") && ToolUtil.isNotEmpty(specItem.get("val"))){
					sp.setItemName(specItem.get("val").toString());
				}else{
					sp.setItemName(" ");
				}
				if(specItem.containsKey("dataFlag") && ToolUtil.isNotEmpty(specItem.get("dataFlag"))){
					sp.setDataFlag(Integer.parseInt(specItem.get("dataFlag").toString()));
				}else{
					sp.setDataFlag(1);
				}
				specItemsMapper.updateById(sp);
			}else{//新增
				SpecItems sp = new SpecItems();
				sp.setCatId(Integer.parseInt(specItem.get("catId").toString()));
				if(specItem.containsKey("val") && ToolUtil.isNotEmpty(specItem.get("val"))){
					sp.setItemName(specItem.get("val").toString());
				}else{
					sp.setItemName(" ");
				}
				sp.setDataFlag(Integer.parseInt(specItem.get("dataFlag").toString()));
				sp.setShopId(shopId);
				sp.setGoodsId(goodsId);
				sp.setItemImg(" ");
				
				sp.setCreateTime(new Date());
				specItemsMapper.insert(sp);
			}
		}
		
		//根据商店id 商品id查询商品规格
		List<Map<String,Object>> list = goodsMapper.selectGoodsCat(goodsId,shopId);
		return list;
	}
	@Override
	public List<Goods> selectGoodsByMap(Map<String, Object> map){
    	return goodsMapper.selectGoodsByMap(map);
	}
    @Override
	public Map<String,Object> checkGoodsAndShop(Integer goodsId){
		Map<String,Object> map =new HashMap<>();
		Goods sktgoods= this.selectById(goodsId);
		String goodsName=sktgoods.getGoodsName();
		if (sktgoods.getIsSale()==0 ){
			map.put("code","0");
			map.put("msg","商品"+goodsName+"已下架");
			return  map;
		}
		if (sktgoods.getGoodsStatus()!=1){
			map.put("code","0");
			map.put("msg","商品"+goodsName+"违规或正在审核中");
			return  map;
		}
		if (sktgoods.getDataFlag()==-1){
			map.put("code","0");
			map.put("msg","商品"+goodsName+"已失效");
			return  map;
		}
		Integer shopId=sktgoods.getShopId();
		SktShops shops = sktShopsService.selectById(shopId);
		String shopName=shops.getShopName();
		if (shops.getShopStatus()==0){
			map.put("code","0");
			map.put("msg","商品"+goodsName+"所属商家"+shopName+"已关闭");
			return  map;
		}
		if (shops.getDataFlag()==-1) {
			map.put("code", "0");
			map.put("msg", "商品" + goodsName + "所属商家" + shopName + "已失效");
			return map;
		}
		map.put("code", "1");
		return map;
	}
}
