package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单日志
 */
@TableName("skt_log_orders")
public class LogOrders extends Model<LogOrders> {
    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer logId;
    /**
     * 订单ID
     */
    private Integer orderId;
    /**
     * 订单状态:-5:商家不同意拒收 -4:商家同意拒收 -3:用户拒收 -2:待支付  -1:已取消 0:待发货 1:待收货 2:已收货
     */
    private Integer orderStatus;
    /**
     * 操作日志
     */
    private String logContent;
    /**
     * 操作者ID
     */
    private Integer logUserId;
    /**
     * 操作者类型 0:顾客/门店 1:商城职员 2管理员工staff
     */
    private Integer logType;
    /**
     * 创建时间
     */
    private Date logTime;

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getLogContent() {
        return logContent;
    }

    public void setLogContent(String logContent) {
        this.logContent = logContent;
    }

    public Integer getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(Integer logUserId) {
        this.logUserId = logUserId;
    }

    public Integer getLogType() {
        return logType;
    }

    public void setLogType(Integer logType) {
        this.logType = logType;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    @Override
    protected Serializable pkVal()  {
        return this.logId;
    }

    @Override
    public String toString() {
        return "LogOrders{" +
                "logId=" + logId +
                ", orderId=" + orderId +
                ", orderStatus=" + orderStatus +
                ", logContent='" + logContent + '\'' +
                ", logUserId=" + logUserId +
                ", logType=" + logType +
                ", logTime=" + logTime +
                '}';
    }
}
