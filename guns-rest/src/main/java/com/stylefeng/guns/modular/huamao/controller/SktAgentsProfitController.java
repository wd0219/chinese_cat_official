package com.stylefeng.guns.modular.huamao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktAgentsProfit;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsProfitService;

/**
 * 分红控制器
 *
 * @author fengshuonan
 * @Date 2018-05-12 15:30:55
 */
@CrossOrigin
@Controller
@RequestMapping("/sktAgentsProfit")
public class SktAgentsProfitController extends BaseController {

    private String PREFIX = "/huamao/sktAgentsProfit/";

    @Autowired
    private ISktAgentsProfitService sktAgentsProfitService;

    /**
     * 跳转到分红首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktAgentsProfit.html";
    }

    /**
     * 跳转到添加分红
     */
    @RequestMapping("/sktAgentsProfit_add")
    public String sktAgentsProfitAdd() {
        return PREFIX + "sktAgentsProfit_add.html";
    }

    /**
     * 跳转到修改分红
     */
    @RequestMapping("/sktAgentsProfit_update/{sktAgentsProfitId}")
    public String sktAgentsProfitUpdate(@PathVariable Integer sktAgentsProfitId, Model model) {
        SktAgentsProfit sktAgentsProfit = sktAgentsProfitService.selectById(sktAgentsProfitId);
        model.addAttribute("item",sktAgentsProfit);
        LogObjectHolder.me().set(sktAgentsProfit);
        return PREFIX + "sktAgentsProfit_edit.html";
    }

    /**
     * 获取分红列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktAgentsProfitService.selectList(null);
    }

    /**
     * 新增分红
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktAgentsProfit sktAgentsProfit) {
        sktAgentsProfitService.insert(sktAgentsProfit);
        return SUCCESS_TIP;
    }

    /**
     * 删除分红
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktAgentsProfitId) {
        sktAgentsProfitService.deleteById(sktAgentsProfitId);
        return SUCCESS_TIP;
    }

    /**
     * 修改分红
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktAgentsProfit sktAgentsProfit) {
        sktAgentsProfitService.updateById(sktAgentsProfit);
        return SUCCESS_TIP;
    }

    /**
     * 分红详情
     */
    @RequestMapping(value = "/detail/{sktAgentsProfitId}")
    @ResponseBody
    public Object detail(@PathVariable("sktAgentsProfitId") Integer sktAgentsProfitId) {
        return sktAgentsProfitService.selectById(sktAgentsProfitId);
    }
}
