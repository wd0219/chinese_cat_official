package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.modular.huamao.model.SpecItems;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规格值表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface SpecItemsMapper extends BaseMapper<SpecItems> {

    List<Map<String, Object>> selectCatName(Integer goodsId);
    List<Map<String, Object>>  selectItemName (Map<String, Object> map);

    Map<String,Object> appFindSpecItems(String specId);
}
