package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.PaySceneMapper;
import com.stylefeng.guns.modular.huamao.model.PayScene;
import com.stylefeng.guns.modular.huamao.service.IAppPaySceneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppPaySceneServiceImpl extends ServiceImpl<PaySceneMapper,PayScene> implements IAppPaySceneService {
    @Autowired
    private PaySceneMapper paySceneMapper;


    @Override
    public PayScene selectPayBySceneCode(Map<String, Object> map) {
        return paySceneMapper.selectPayBySceneCode(map);
    }
}
