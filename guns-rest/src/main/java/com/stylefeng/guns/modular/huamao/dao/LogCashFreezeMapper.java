package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogCashFreeze;
import com.stylefeng.guns.modular.huamao.model.LogCashFreezeList;
import com.stylefeng.guns.modular.huamao.model.TradeDictDTD;

/**
 * <p>
 * 待发现金流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogCashFreezeMapper extends BaseMapper<LogCashFreeze> {

	List<LogCashFreezeList> selectLogCashFreezeAll(LogCashFreezeList logCashFreezeList);

    List<Map<String,Object>> selectUserCaseFreeze(LogCashFreezeList logCashFreezeList);

    List<LogCashFreeze> willCashRecord(Integer userId);


    TradeDictDTD selectTradeDictByType(Integer type);
}
