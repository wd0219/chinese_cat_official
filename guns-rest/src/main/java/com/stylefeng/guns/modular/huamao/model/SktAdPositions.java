package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 广告位置表
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@TableName("skt_ad_positions")
public class SktAdPositions extends Model<SktAdPositions> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "positionId", type = IdType.AUTO)
    private Integer positionId;
    /**
     * 广告类型
     */
    private Integer positionType;
    /**
     * 广告位置
     */
    private String positionName;
    /**
     * 建议宽度
     */
    private Integer positionWidth;
    /**
     * 建议高度
     */
    private Integer positionHeight;
    /**
     * 有效状态
     */
    private Integer dataFlag;
    /**
     * 广告位置代码
     */
    private String positionCode;
    /**
     * 排序号
     */
    private Integer apSort;


    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Integer getPositionType() {
        return positionType;
    }

    public void setPositionType(Integer positionType) {
        this.positionType = positionType;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Integer getPositionWidth() {
        return positionWidth;
    }

    public void setPositionWidth(Integer positionWidth) {
        this.positionWidth = positionWidth;
    }

    public Integer getPositionHeight() {
        return positionHeight;
    }

    public void setPositionHeight(Integer positionHeight) {
        this.positionHeight = positionHeight;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    public Integer getApSort() {
        return apSort;
    }

    public void setApSort(Integer apSort) {
        this.apSort = apSort;
    }

    @Override
    protected Serializable pkVal() {
        return this.positionId;
    }

    @Override
    public String toString() {
        return "SktAdPositions{" +
        "positionId=" + positionId +
        ", positionType=" + positionType +
        ", positionName=" + positionName +
        ", positionWidth=" + positionWidth +
        ", positionHeight=" + positionHeight +
        ", dataFlag=" + dataFlag +
        ", positionCode=" + positionCode +
        ", apSort=" + apSort +
        "}";
    }
}
