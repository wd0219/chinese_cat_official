package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;

public class SktShopAccredsDTO {
	
	private Integer shopId;
	private String loginName;
	private Integer isSelf;
	private String shopName;
	private String trueName;
	private String userPhone;
	private String address;
	private String companyName;
	private Integer storeStatus;
	private Integer shopStatus;
	private String createTime;
	private String checkTime;
	private BigDecimal businessQuota;
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Integer getIsSelf() {
		return isSelf;
	}
	public void setIsSelf(Integer isSelf) {
		this.isSelf = isSelf;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Integer getStoreStatus() {
		return storeStatus;
	}
	public void setStoreStatus(Integer storeStatus) {
		this.storeStatus = storeStatus;
	}
	public Integer getShopStatus() {
		return shopStatus;
	}
	public void setShopStatus(Integer shopStatus) {
		this.shopStatus = shopStatus;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}
	public BigDecimal getBusinessQuota() {
		return businessQuota;
	}
	public void setBusinessQuota(BigDecimal businessQuota) {
		this.businessQuota = businessQuota;
	}
	@Override
	public String toString() {
		return "SktShopAccredsDTO [shopId=" + shopId + ", loginName=" + loginName + ", isSelf=" + isSelf + ", shopName="
				+ shopName + ", trueName=" + trueName + ", userPhone=" + userPhone + ", address=" + address
				+ ", companyName=" + companyName + ", storeStatus=" + storeStatus + ", shopStatus=" + shopStatus
				+ ", createTime=" + createTime + ", checkTime=" + checkTime + ", businessQuota=" + businessQuota + "]";
	}
	public SktShopAccredsDTO(Integer shopId, String loginName, Integer isSelf, String shopName, String trueName,
			String userPhone, String address, String companyName, Integer storeStatus, Integer shopStatus,
			String createTime, String checkTime, BigDecimal businessQuota) {
		super();
		this.shopId = shopId;
		this.loginName = loginName;
		this.isSelf = isSelf;
		this.shopName = shopName;
		this.trueName = trueName;
		this.userPhone = userPhone;
		this.address = address;
		this.companyName = companyName;
		this.storeStatus = storeStatus;
		this.shopStatus = shopStatus;
		this.createTime = createTime;
		this.checkTime = checkTime;
		this.businessQuota = businessQuota;
	}
	public SktShopAccredsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
