package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.CatBrandsMapper;
import com.stylefeng.guns.modular.huamao.model.CatBrands;
import com.stylefeng.guns.modular.huamao.service.ICatBrandsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分类-品牌表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class CatBrandsServiceImpl extends ServiceImpl<CatBrandsMapper, CatBrands> implements ICatBrandsService {



}
