package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktOrdersStockscore;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商家购买库存积分订单表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
public interface SktOrdersStockscoreMapper extends BaseMapper<SktOrdersStockscore> {
	
	/*
	 * 展示全部
	 * */
	public List<SktOrdersStockscore> sktOrdersStockscorefindall(SktOrdersStockscore ordersStockscore);

    Integer updateStockScoreById(SktOrdersStockscore sktOrdersStockscore);

    BigDecimal findStockScore(Integer orderId);

    Map<String,Object> appFindOrderByNo(String orderNo);

    SktOrdersStockscore selectByOredrNo(Map<String, Object> orderWhere);

    void updateOrderStockScoreById(SktOrdersStockscore sktOrdersStockscore2);

    SktOrdersStockscore selectOrdersStockscoreByWhere(SktOrdersStockscore sktOrdersStockscore);
}
