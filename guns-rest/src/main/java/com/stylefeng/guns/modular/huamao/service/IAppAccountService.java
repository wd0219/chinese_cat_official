package com.stylefeng.guns.modular.huamao.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Account;
import com.stylefeng.guns.modular.huamao.model.MyAccountHuabao;
import com.stylefeng.guns.modular.huamao.model.UsersDraws;
import com.stylefeng.guns.modular.huamao.result.AppResult;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public  interface IAppAccountService extends IService<Account>{


    List<UsersDraws> presentProgress(Map<String, Object> map);

    MyAccountHuabao myHuaBao(Integer userId);

    Account selectAccountByUserId(Integer userId);

    Account scoreIndex(Integer userId);

    JSONObject recharge(Integer userId, int payType, BigDecimal cash, String remark, Integer status);

    AppResult isBuyAPPTimeScore(Integer shopId, BigDecimal payMoney);

    Map<String,Object> selectAccountAndAccountShopByUserId(Integer userId);

    boolean updateAccountById(Account account);
}
