package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogStockscoreMapper;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;
import com.stylefeng.guns.modular.huamao.service.IAppLogStockScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppLogStockScoreServiceImpl extends ServiceImpl<LogStockscoreMapper,LogStockscore> implements IAppLogStockScoreService {
    @Autowired
    private LogStockscoreMapper logStockscoreMapper;

    @Override
    public List<LogStockscore> selectLogStockScoreByUserId(Map<String, Object> map) {
        return logStockscoreMapper.selectLogStockScoreByUserId(map);
    }
}
