package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单退款记录表
 * </p>
 *
 * @author caody123
 * @since 2018-04-19
 */
@TableName("skt_order_refunds")
public class SktOrderRefunds extends Model<SktOrderRefunds> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 订单ID
     */
    private Integer orderId;
    /**
     * 接收退款用户
     */
    private Integer refundTo;
    /**
     * 用户申请退款原因ID
     */
    private Integer refundReson;
    /**
     * 用户申请退款原因
     */
    private String refundOtherReson;
    /**
     * 退款快递公司ID
     */
    private Integer expressId;
    /**
     * 退款快递号
     */
    private String expressNo;
    /**
     * 用户退款金额
     */
    private BigDecimal backMoney;
    /**
     * 管理员退款流水号
     */
    private String refundTradeNo;
    /**
     * 退款备注
     */
    private String refundRemark;
    /**
     * 退款时间
     */
    private Date refundTime;
    /**
     * 店铺不同意拒收原因
     */
    private String shopRejectReason;
    /**
     * 退款状态-2平台不同意退款-1商家不同意 0用户申请退款 1商家同意退款 2平台已经退款
     */
    private Integer refundStatus;
    /**
     * 用户申请退款时间
     */
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getRefundTo() {
        return refundTo;
    }

    public void setRefundTo(Integer refundTo) {
        this.refundTo = refundTo;
    }

    public Integer getRefundReson() {
        return refundReson;
    }

    public void setRefundReson(Integer refundReson) {
        this.refundReson = refundReson;
    }

    public String getRefundOtherReson() {
        return refundOtherReson;
    }

    public void setRefundOtherReson(String refundOtherReson) {
        this.refundOtherReson = refundOtherReson;
    }

    public Integer getExpressId() {
        return expressId;
    }

    public void setExpressId(Integer expressId) {
        this.expressId = expressId;
    }

    public String getExpressNo() {
        return expressNo;
    }

    public void setExpressNo(String expressNo) {
        this.expressNo = expressNo;
    }

    public BigDecimal getBackMoney() {
        return backMoney;
    }

    public void setBackMoney(BigDecimal backMoney) {
        this.backMoney = backMoney;
    }

    public String getRefundTradeNo() {
        return refundTradeNo;
    }

    public void setRefundTradeNo(String refundTradeNo) {
        this.refundTradeNo = refundTradeNo;
    }

    public String getRefundRemark() {
        return refundRemark;
    }

    public void setRefundRemark(String refundRemark) {
        this.refundRemark = refundRemark;
    }

    public Date getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(Date refundTime) {
        this.refundTime = refundTime;
    }

    public String getShopRejectReason() {
        return shopRejectReason;
    }

    public void setShopRejectReason(String shopRejectReason) {
        this.shopRejectReason = shopRejectReason;
    }

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktOrderRefunds{" +
        "id=" + id +
        ", orderId=" + orderId +
        ", refundTo=" + refundTo +
        ", refundReson=" + refundReson +
        ", refundOtherReson=" + refundOtherReson +
        ", expressId=" + expressId +
        ", expressNo=" + expressNo +
        ", backMoney=" + backMoney +
        ", refundTradeNo=" + refundTradeNo +
        ", refundRemark=" + refundRemark +
        ", refundTime=" + refundTime +
        ", shopRejectReason=" + shopRejectReason +
        ", refundStatus=" + refundStatus +
        ", createTime=" + createTime +
        "}";
    }
}
