package com.stylefeng.guns.modular.huamao.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;
import com.stylefeng.guns.modular.huamao.model.Users;

public interface IAppWorkorderServer extends IService<SktWorkorder>{

	public Map<String, Object> buyWorkorder(String orderNo, Integer payType, String payPwd, Users user);

	public Map<String, Object> getWorkorder(Users user);

	public Map<String, Object> getShare(Users user);

	public Map<String, Object> modifyShare(String preValue, String afterValue, String orderRemarks,String imgs, Integer money, Users user);

	public Map<String, Object> getPhone(Users user);

	public Map<String, Object> modifyPhone(String preValue, String afterValue, String code, String function,
			String orderRemarks, String imgs, Integer money, Users user);

}
