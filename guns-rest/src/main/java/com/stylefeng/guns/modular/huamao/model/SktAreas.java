package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 区域表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_areas")
public class SktAreas extends Model<SktAreas> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "areaId", type = IdType.AUTO)
    private Integer areaId;
    /**
     * 父ID
     */
    private Integer parentId;
    /**
     * 地区名称
     */
    private String areaName;
    /**
     * 是否显示
     */
    private Integer isShow;
    /**
     * 排序号
     */
    private Integer areaSort;
    /**
     * 地区首字母
     */
    private String areaKey;
    /**
     * 级别标志
     */
    private Integer areaType;
    /**
     * 删除标志
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getAreaSort() {
        return areaSort;
    }

    public void setAreaSort(Integer areaSort) {
        this.areaSort = areaSort;
    }

    public String getAreaKey() {
        return areaKey;
    }

    public void setAreaKey(String areaKey) {
        this.areaKey = areaKey;
    }

    public Integer getAreaType() {
        return areaType;
    }

    public void setAreaType(Integer areaType) {
        this.areaType = areaType;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.areaId;
    }

    @Override
    public String toString() {
        return "SktAreas{" +
        "areaId=" + areaId +
        ", parentId=" + parentId +
        ", areaName=" + areaName +
        ", isShow=" + isShow +
        ", areaSort=" + areaSort +
        ", areaKey=" + areaKey +
        ", areaType=" + areaType +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
