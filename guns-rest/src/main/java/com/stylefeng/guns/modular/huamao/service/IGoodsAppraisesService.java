package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesCustm;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品评价表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IGoodsAppraisesService extends IService<GoodsAppraises> {
	List<Map<String,Object>> selectAll(GoodsAppraisesCustm goodsAppraisesCustm);
	public int selectGoodsAppraisesTotal();

    Integer appReplyComments(String content, Integer goodsId, Integer orderId);

	public List<Map<String, Object>> readComment(Integer goodsId);
	
	boolean insertAdd(GoodsAppraises goodsAppraises);
}
