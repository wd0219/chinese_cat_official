package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktSpecApply;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规则申请表 服务类
 * </p>
 *
 * @author py
 * @since 2018-08-23
 */
public interface ISktSpecApplyService extends IService<SktSpecApply> {
    public Map<String,String> insertSktSpecApply(SktSpecApply sktSpecApply);
}
