package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 开元宝流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogKaiyuanServiceImpl extends ServiceImpl<LogKaiyuanMapper, LogKaiyuan> implements ILogKaiyuanService {

	@Autowired
	private LogKaiyuanMapper lkm;

	// 开元宝流水返回
	@Override
	public List<LogKaiyuanList> selectLogKaiyuanAll(LogKaiyuanList logKaiyuanList) {
		return lkm.selectLogKaiyuanAll(logKaiyuanList);
	}

    @Override
    public List<Map<String, Object>> selectUserKaiYuan(LogKaiyuanList logKaiyuanList) {
		List<Map<String, Object>> list  = lkm.selectUserKaiYuan(logKaiyuanList);
		return list;
    }
}
