package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcards;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgent;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgentList;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgent;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgentList;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsDTO;
import com.stylefeng.guns.modular.huamao.model.SktAgentsProfit;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderRet;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsService;
import com.stylefeng.guns.modular.huamao.util.StringUtils;

/**
 * 代理公司管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-20 13:09:35
 */
@CrossOrigin
@Controller
@RequestMapping("/sktAgents")
public class SktAgentsController extends BaseController {

    private String PREFIX = "/huamao/sktAgents/";

    @Autowired
    private ISktAgentsService sktAgentsService;

    /**
     * 跳转到代理公司管理首页
     */
    @RequestMapping("")
    public String index(@RequestParam(required = false) String agentId, Model model) {
    	model.addAttribute("agentId",agentId);
        return PREFIX + "sktAgents.html";
    }

    /**
     * 跳转到添加代理公司管理
     */
    @RequestMapping("/sktAgents_add")
    public String sktAgentsAdd() {
        return PREFIX + "sktAgents_add.html";
    }

    /**
     * 跳转到修改代理公司管理
     */
    @RequestMapping("/sktAgents_update/{sktAgentsId}")
    public String sktAgentsUpdate(@PathVariable Integer sktAgentsId, Model model) {
        SktAgents sktAgents = sktAgentsService.selectById(sktAgentsId);
        model.addAttribute("item",sktAgents);
        LogObjectHolder.me().set(sktAgents);
        return PREFIX + "sktAgents_edit.html";
    }

    /**
     * 获取代理公司管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktAgentsDTO sktAgentsDTO) {
        return sktAgentsService.showSktAgentsIno(sktAgentsDTO);
    }
    /**
     * 获取人员名称 电话id
     */
    @RequestMapping(value = "/selectNameOrPhone")
    @ResponseBody
    public Object selectNameOrPhone(@RequestParam String nameOrPhone){
    	Map<String, Object> map = sktAgentsService.selectNameOrPhone(nameOrPhone);
    	String jsonString = JSONObject.toJSONString(map);
    	return jsonString;
    }
    
    /**
     * 新增代理公司管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktAgents sktAgents) {
    	sktAgents.setCreateTime(new Date());
        sktAgentsService.insert(sktAgents);
        return SUCCESS_TIP;
    }

    /**
     * 删除代理公司管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktAgentsId) {
        sktAgentsService.deleteById(sktAgentsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改代理公司管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktAgents sktAgents) {
        sktAgentsService.updateById(sktAgents);
        return SUCCESS_TIP;
    }

    /**
     * 代理公司管理详情
     */
    @RequestMapping(value = "/detail/{sktAgentsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktAgentsId") Integer sktAgentsId) {
        return sktAgentsService.selectById(sktAgentsId);
    }
    /*********************首页*************************/
    /**
     * 代理公司登录
     */
    @RequestMapping(value = "/AgentsLogin")
    @ResponseBody
    public Result AgentsLogin(String loginNameOrPhone, String loginPwd) {
         Map<String,Object> map= sktAgentsService.agentsLogin(loginNameOrPhone,loginPwd);
         if(StringUtils.isEmpty(map)){
            return  Result.EEROR("代理公司登录失败！");
         }else{
             return Result.OK(map);
         }
    }
    /**
     * 根据id查询代理公司信息   注册信息
     */
    @RequestMapping(value = "/selectAgentsReg")
    @ResponseBody
    public Result selectAgentsReg(SktAgents sktAgents) {
        SktAgents sktAgents1 = sktAgentsService.selectById(sktAgents.getAgentId());
        if(StringUtils.isEmpty(sktAgents1)){
            return Result.EEROR("代理公司查询注册信息失败!");
        }
        return Result.OK(sktAgents1);
    }
    /**
     * 代理基本信息
     */
    @RequestMapping(value = "/selectAgentsInfo")
    @ResponseBody
    public Result selectAgentsInfo(SktAgents sktAgents) {
        Map<String,Object> map = sktAgentsService.selectAgentsInfo(sktAgents);
        if(ToolUtil.isEmpty(map)){
            return Result.EEROR("代理公司查询基础信息失败!");
        }
        return Result.OK(map);
    }
    /**
     * 代理公司积分华宝
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/selectAgentsHuabao")
    @ResponseBody
    public Result selectAgentsHuabao(LogKaiyuanAgentList logKaiyuanAgentList, Integer pageNum) {
    	PageHelper.startPage(pageNum, 12);
        List<LogKaiyuanAgent> list = sktAgentsService.selectAgentsHuabao(logKaiyuanAgentList);
        PageInfo info = new PageInfo(list);
        return Result.OK(list,info.getPages());
    }
    /**
     * 代理公司积分华宝
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/selectAgentsscore")
    @ResponseBody
    public Result selectAgentsscore(LogScoreAgentList logScoreAgentList, Integer pageNum) {
    	PageHelper.startPage(pageNum, 12, false);
    	List<LogScoreAgent> list = sktAgentsService.selectAgentsscore(logScoreAgentList);
    	PageInfo info = new PageInfo(list);
        return Result.OK(list,info.getPages());
    }
    /**
     * 股东管理
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/selectAgentsStockholder")
    @ResponseBody
    public Result selectAgentsStockholder(SktAgentsStockholderDTO sktAgentsStockholderDTO, Integer pageNum) {
    	PageHelper.startPage(pageNum, 12, false);
        List<SktAgentsStockholderRet> list = sktAgentsService.selectAgentsStockholder(sktAgentsStockholderDTO);
        PageInfo info = new PageInfo(list);
        return Result.OK(list,info.getPages());
    }
    /**
     * 分红记录 selectAgentsProfit
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/selectAgentsProfit")
    @ResponseBody
    public Result selectAgentsProfit(@RequestParam(value="userId") Integer userId,  Integer pageNum) {
    	PageHelper.startPage(pageNum, 12);
        List<SktAgentsProfit> list = sktAgentsService.selectAgentsProfit(userId);
        PageInfo info = new PageInfo(list);
        return Result.OK(list,info.getPages());
    }
    /**
     * 商家管理
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/selectAccountShop")
    @ResponseBody
    public Result selectAccountShop(@RequestParam(value="agentId")Integer agentId,  
    		@RequestParam(value="shopName",defaultValue = "", required = false) String shopName,
    		@RequestParam(value="pageNum",defaultValue = "1", required = false)Integer pageNum) {
    	//PageHelper.startPage(pageNum, 8);
        List<Map<String,Object>> list = sktAgentsService.selectAccountShop(agentId,shopName,pageNum);
        PageInfo info = new PageInfo(list);
        return Result.OK(list,info.getPages());
    }
    /**
     * 商家查看
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/selectShopInfo")
    @ResponseBody
    public Result selectShopInfo(Integer shopId, Integer pageNum) {
        PageHelper.startPage(pageNum, 12);
        List<Map<String,Object>> list = sktAgentsService.selectShopInfo(shopId);
        PageInfo info = new PageInfo(list);
        return Result.OK(list,info.getPages());
    }
    /**
     * 代理银行卡查询
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = "/selectAgentBankcards")
    @ResponseBody
    public Result selectAgentBankcards(AgentsBankcards agentsBankcards,  Integer pageNum) {
    	PageHelper.startPage(pageNum, 12);
        List<AgentsBankcards> list = sktAgentsService.selectAgentBankcards(agentsBankcards);
        PageInfo info = new PageInfo(list);
        return Result.OK(list,info.getPages());
    }
    
    /**
     * 修改登录密码
     */
    @RequestMapping(value = "/updateAgentPwd")
    @ResponseBody
    public Result updateAgentPwd(SktAgentsDTO sktAgents){
    	Integer i =  sktAgentsService.updateAgentPwd(sktAgents);
    	if(ToolUtil.isEmpty(i)){
    		return Result.EEROR("修改密码失败！");
    	}
        return Result.OK(null);
    }
    /**
     * 修改支付密码
     */
    @RequestMapping(value = "/updateAgentPayPwd")
    @ResponseBody
    public Result updateAgentPayPwd(SktAgentsDTO sktAgents){
    	Integer i =  sktAgentsService.updateAgentPayPwd(sktAgents);
    	if(ToolUtil.isEmpty(i)){
    		return Result.EEROR("修改密码失败！");
    	}
        return Result.OK("支付成功");
    }
}
