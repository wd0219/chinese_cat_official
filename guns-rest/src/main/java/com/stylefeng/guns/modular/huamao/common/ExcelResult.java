package com.stylefeng.guns.modular.huamao.common;

public class ExcelResult {
	/**
	 * json return
	 */
	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
