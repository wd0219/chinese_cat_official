package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcards;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcardsList;

import java.util.List;

/**
 * <p>
 * 代理公司的银行卡表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface IAgentsBankcardsService extends IService<AgentsBankcards> {

	// 代理公司的银行卡表展示
	public List<AgentsBankcardsList> selectAgentsBankcardsAll(AgentsBankcardsList agentsBankcardsList);

	// 通过传过来的ID进行逻辑删除
	public void deleteById2(Integer agentsDrawsId);

}
