package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 待发现金流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public class LogCashFreezeList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 现金类型 1下线升级奖励200元或2000元 2线上营业额获得 3线下营业额 31转正式现金账户支出 32购买库存积分
	 */
	private Integer type;
	/**
	 * 发起用户ID 0为平台
	 */
	private Integer fromId;
	/**
	 * 目标用户ID 0为平台
	 */
	private Integer userId;
	/**
	 * 对应订单号
	 */
	private String orderNo;
	/**
	 * 操作前的金额
	 */
	private BigDecimal preCash;
	/**
	 * 流水标志 -1减少 1增加
	 */
	private Integer cashType;
	/**
	 * 金额
	 */
	private BigDecimal cash;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String cashp;
	/**
	 * 变动后金额
	 */
	private BigDecimal acash;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 有效状态 1有效 0删除
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getFromId() {
		return fromId;
	}
	public void setFromId(Integer fromId) {
		this.fromId = fromId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getPreCash() {
		return preCash;
	}
	public void setPreCash(BigDecimal preCash) {
		this.preCash = preCash;
	}
	public Integer getCashType() {
		return cashType;
	}
	public void setCashType(Integer cashType) {
		this.cashType = cashType;
	}
	public BigDecimal getCash() {
		return cash;
	}
	public void setCash(BigDecimal cash) {
		this.cash = cash;
	}
	public String getCashp() {
		return cashp;
	}
	public void setCashp(String cashp) {
		this.cashp = cashp;
	}
	public BigDecimal getAcash() {
		return acash;
	}
	public void setAcash(BigDecimal acash) {
		this.acash = acash;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	

}
