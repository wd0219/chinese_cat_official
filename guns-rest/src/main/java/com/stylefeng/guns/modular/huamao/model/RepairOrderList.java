package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 第三方支付补单记录表扩展类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-26
 */
public class RepairOrderList implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 用户手机号
	 */
	private String userPhone;
	/**
	 * 内部业务订单号
	 */
	private String orderNo;
	/**
	 * 第三方流水号
	 */
	private String outTradeNo;
	/**
	 * 业务类型 BUY_GOODS:商城消费 BUY_SCORE:购买积分 RECHARGE:充值 UPGRADE:升级 BUY_PLAQUE：购买牌匾
	 * WORK_ORDER：工单
	 */
	private String businessType;
	/**
	 * 支付类型（1：微信；2：支付宝；3：快捷；4：网关）
	 */
	private Integer payType;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 操作人ID
	 */
	private Integer operateId;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	public RepairOrderList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RepairOrderList(Integer id, Integer userId, String userPhone, String orderNo, String outTradeNo,
			String businessType, Integer payType, Date createTime, Integer operateId, String beginTime,
			String endTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.userPhone = userPhone;
		this.orderNo = orderNo;
		this.outTradeNo = outTradeNo;
		this.businessType = businessType;
		this.payType = payType;
		this.createTime = createTime;
		this.operateId = operateId;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getOperateId() {
		return operateId;
	}

	public void setOperateId(Integer operateId) {
		this.operateId = operateId;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "RepairOrderList [id=" + id + ", userId=" + userId + ", userPhone=" + userPhone + ", orderNo=" + orderNo
				+ ", outTradeNo=" + outTradeNo + ", businessType=" + businessType + ", payType=" + payType
				+ ", createTime=" + createTime + ", operateId=" + operateId + ", beginTime=" + beginTime + ", endTime="
				+ endTime + "]";
	}

}
