package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktHuamaobtLog;

import java.math.BigDecimal;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author caody
 * @since 2018-06-20
 */
public interface ISktHuamaobtLogService extends IService<SktHuamaobtLog> {
    public Object recharge(Integer userId,BigDecimal huabao, BigDecimal amount, String orderNo);
    public Object ticker();
}
