package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.*;

/**
 * <p>
 * 开元宝营业额流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface LogKaiyuanTurnoverMapper extends BaseMapper<LogKaiyuanTurnover> {

	// 开元宝营业额流水表返回
	List<LogKaiyuanTurnoverList> selectLogKaiyuanTurnoverAll(LogKaiyuanTurnoverList logKaiyuanTurnoverList);

	public List<LogKaiyuanTurnover> selectTurnoverInfo(LogKaiyuanTurnoverList logKaiyuanTurnoverList);

	public List<LogKaiyuanFreeze> selectLogKaiyuanFreeze(LogKaiyuanFreezeList logKaiyuanTurnoverList);

    List<LogKaiyuanTurnover> huaBaoTurnoverList(Integer userId);
}
