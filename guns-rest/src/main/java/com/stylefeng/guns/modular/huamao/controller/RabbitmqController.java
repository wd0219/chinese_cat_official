package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.modular.huamao.mongodb.SendMessage;
import com.stylefeng.guns.modular.huamao.util.RabbitSender;
import com.stylefeng.guns.rest.common.constants.RabbitConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mq")
public class RabbitmqController {
    @Autowired
    private RabbitSender rabbitSender;

    @RequestMapping("/send")
    public Object sendMsg(String name) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setId(1);
        sendMessage.setAge(20);
        sendMessage.setName(name);
        JSONObject test= (JSONObject)JSONObject.toJSON(sendMessage);
        String aaa = test.toJSONString();
        //String aaa =  JSONObject.parseObject(sendMessage.toString()).toJSONString();
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("sendMessage",sendMessage);
        String ss = jsonObject.toJSONString();
        //rabbitSender.sendMessage(RabbitConstants.MQ_EXCHANGE_SEND_AWARD, RabbitConstants.MQ_ROUTING_KEY_SEND_COUPON, ss);
        rabbitSender.sendMessage(RabbitConstants.MQ_EXCHANGE_LOG, RabbitConstants.MQ_ROUTING_KEY_LOG, aaa);
        return "发送成";
    }
    @RequestMapping("/sendOrder")
    public Object  sendOrder(){
//        SendMessage sendMessage = new SendMessage();
//        sendMessage.setId(1);
//        sendMessage.setAge(20);
//        sendMessage.setName("cdy");
//        JSONObject test= (JSONObject)JSONObject.toJSON(sendMessage);
//        String aaa = test.toJSONString();
        String order = "22018011809063880771";
        rabbitSender.sendMessage(RabbitConstants.MQ_EXCHANGE_ORDER,RabbitConstants.MQ_ROUTING_KEY_ORDER,order);
        return "发送成功";
    }
}
