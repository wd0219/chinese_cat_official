package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SpecCats;
import com.stylefeng.guns.modular.huamao.model.SpecCatsCustem;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规格分类表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface ISpecCatsService extends IService<SpecCats> {
	List<Map<String, Object>> selectAll(SpecCatsCustem specCatsCustem);
	Integer insertSpecCats(SpecCatsCustem specCatsCustem);
	SpecCatsCustem selectSpecCatsCustemById(int id);
}
