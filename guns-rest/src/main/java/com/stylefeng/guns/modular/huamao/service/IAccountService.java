package com.stylefeng.guns.modular.huamao.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.*;

/**
 * <p>
 * 会员账户表 服务类
 * </p>
 *
 * @author gxz123
 * @author Administrator
 * @author Administrator
 * @author Administrator
 * @since 2018-04-11
 */
/**
 * @author Administrator
 *
 */

/**
 * @author Administrator
 *
 */
public interface IAccountService extends IService<Account> {

    // 展示用户账户列表
    public List<AccountList> selectAccountAll(AccountList accountList);

    /*
     * 根据用户id查询账户
     */
    public Account selectAccountByuserId(Integer userId);

    /**
     * 用户点击升级
     *
     * @param userId
     *            用户id
     *
     * @return
     */
    public Object getUseUpDate(Integer userId);

    /**
     *
     * @param usersUpgrade
     * @param payPwd
     * @return
     */


	public JSONObject updateByUp(UsersUpgrade usersUpgrade,String payPwd);
	/**
	 * 取消用户升级
	 *
	 * @param usersUpgrade  用户升级订单表
	 * @return
	 */
	public boolean resetUPGrade(UsersUpgrade usersUpgrade);


    /*用户下单支付*/
    public  Map<String,Object> userSale(List<String> orderIdList,Integer payType,BigDecimal payMoney,BigDecimal feeMoney,Integer userId);

    public boolean toPayGoods(List<Integer> orderIdList);

//    // 给用户添加相应的积分,并且判断是否升级的是经理，直接分享人，直接分享人类型
//    public String upAfterAdd(Integer userId, Boolean isManager, Users shareMan, Integer orderId, BigDecimal score,
//                             Integer type, String remark);

    String selectSpSorceByUserId(Integer userId, String type);

    //充值成功后
    public JSONObject rechargeAfter(UsersRecharge recharge);

    //申请库存积分前操作
    public String applyStockBefore();

    //查询是否在购买时间段内
    public JSONObject isBuyTime(Integer shopId, BigDecimal cash);

    //用户充值生成订单
    public JSONObject recharge(Integer userId, Integer payType, BigDecimal cash, String remark, Integer status);

    //购买成功，添加库存扣除相应类型的金额
    JSONObject buyStockScoreAfter(SktOrdersStockscore sktOrdersStockscore, String payPwd);

    //数据报表
    public BaseTable getDataTable(Integer userId);

    //用户积分转化华宝
//    public SktDateTransform scoreTransform(Integer agentId, BigDecimal ratio, Integer userId, Integer type);


    //股东分红
//    void shareBonus(Integer agentId);

    public String doUpAgent(Integer shaId, Integer staffId);

    public JSONObject getCashToUp(Integer userId, Integer userUpType);

    //线下支付
    Integer underLinePay(UsersRecharge recharge);

    public AccountList getAccountByUserId(Integer userId);

    public JSONObject sendScore(Integer userId, Integer shopId, BigDecimal score, String payPwd, BigDecimal totalMoney, Integer scoreRatio, String orderRemarks);

    public Map<String, Object> getAfterSevenWorkDay(Integer dates);

    public JSONObject buyplaque(SktBuyplaque sktBuyplaque);

    public boolean insertLogKaiyuan(Integer fromId, BigDecimal kaiyuan, Integer kaiyuanType, String firstNum, BigDecimal preKaiyuan, String remark, Integer type, Integer userId);

	/**
	 * 添加现金流水
	 * @param cashType
	 * @param fromId
	 * @param userId
	 * @param firstNum
	 * @param preCash
	 * @param remark
	 * @param type
	 * @param cash
	 * @return
	 */
	public boolean insertLogCash(Integer cashType,Integer fromId,Integer userId ,String firstNum,BigDecimal preCash,String remark,Integer type,BigDecimal cash);

    public Integer insertLogKaiYuanTurnover(Integer type, Integer fromId, Integer userId, String firstNum, BigDecimal preKaiyuan, Integer kaiyuanType, BigDecimal kaiyuan, String remark);

    //点击线下支付
    UnderLinePay clickUnderLinePay();
    public JSONObject personPay( String userPhone , BigDecimal payMoney,  Integer scoreRatio ,
                                 String payPwd,  Integer payType,Integer userId);
    public Integer saleAward(int inviteId,BigDecimal score,int usetId ,String orderNo);
    public Integer marketAward(int inviteId,BigDecimal score, int userId,String orderNo);
    public void specialAward(List<Integer> list,BigDecimal specialPer,BigDecimal score,Integer specialType,int inviteId,String orderNo);

    boolean uptateKaiyuanByUserId(Map<String, Object> map);

    boolean uptateCashByUserId(Map<String, Object> cashMap);
    public  Map<String,Object> payment(List<String> orderIdList,Integer userId,Integer payType,
                                       BigDecimal payMoney,BigDecimal payKaiyuan,BigDecimal feeKaiyuan,BigDecimal payShopping,
                                       String thirdNo,Integer thirdType,String payName);
    public Boolean enter(List<Integer> orderIdList);

    public Map<String, Object> addShopping(String userPhone,Integer type,BigDecimal money,Integer sceneType);

    public  Map<String,Object> compareShopping(BigDecimal shopping,String userIds);
    public Map<String,Object> checkShopping(String userIds, BigDecimal mailccMoney);
}
