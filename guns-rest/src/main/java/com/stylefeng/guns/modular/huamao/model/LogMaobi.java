package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 转化猫币订单记录表
 * </p>
 *
 * @author liuduan
 * @since 2018-06-24
 */
@TableName("skt_log_maobi")
public class LogMaobi extends Model<LogMaobi> {

    private static final long serialVersionUID = 1L;
    /**
     * id自增
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 提现类型
     */
    private Integer type;
    /**
     * 发起用户ID
     */
    private Integer userId;
    /**
     * userId
     */
    private Integer dataId;
    /**
     * 对应订单号
     */
    private String orderNo;
    /**
     * 操作前的金额
     */
    private BigDecimal preCash;
    /**
     * 流水标志 -1减少 1增加
     */
    private Integer cashType;
    /**
     * 到账猫币
     */
    private BigDecimal maobi;
    /**
     * 备注
     */
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreCash() {
        return preCash;
    }

    public void setPreCash(BigDecimal preCash) {
        this.preCash = preCash;
    }

    public Integer getCashType() {
        return cashType;
    }

    public void setCashType(Integer cashType) {
        this.cashType = cashType;
    }

    public BigDecimal getMaobi() {
        return maobi;
    }

    public void setMaobi(BigDecimal maobi) {
        this.maobi = maobi;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getDataId() {
        return dataId;
    }

    public void setDataId(Integer dataId) {
        this.dataId = dataId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogMaobi{" +
                "id=" + id +
                ", type=" + type +
                ", userId=" + userId +
                ", dataId=" + dataId +
                ", orderNo='" + orderNo + '\'' +
                ", preCash=" + preCash +
                ", cashType=" + cashType +
                ", maobi=" + maobi +
                ", remark='" + remark + '\'' +
                ", dataFlag=" + dataFlag +
                ", createTime=" + createTime +
                '}';
    }
}
