package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktBanks;
import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktBanksService;
import com.stylefeng.guns.modular.huamao.service.ISktExpressService;

/**
 * 银行管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 18:52:03
 */
@CrossOrigin
@Controller
@RequestMapping("/sktBanks")
public class SktBanksController extends BaseController {

    private String PREFIX = "/huamao/sktBanks/";

    @Autowired
    private ISktBanksService sktBanksService;
    /**
     * 跳转到银行管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktBanks.html";
    }

    /**
     * 跳转到添加银行管理
     */
    @RequestMapping("/sktBanks_add")
    public String sktBanksAdd() {
        return PREFIX + "sktBanks_add.html";
    }

    /**
     * 跳转到修改银行管理
     */
    @RequestMapping("/sktBanks_update/{sktBanksId}")
    public String sktBanksUpdate(@PathVariable Integer sktBanksId, Model model) {
        SktBanks sktBanks = sktBanksService.selectById(sktBanksId);
        model.addAttribute("item",sktBanks);
        LogObjectHolder.me().set(sktBanks);
        return PREFIX + "sktBanks_edit.html";
    }


    /**
     * 新增银行管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktBanks sktBanks) {
        sktBanksService.insert(sktBanks);
        return SUCCESS_TIP;
    }

    /**
     * 删除银行管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktBanksId) {
        sktBanksService.deleteById(sktBanksId);
        return SUCCESS_TIP;
    }

    /**
     * 修改银行管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktBanks sktBanks) {
        sktBanksService.updateById(sktBanks);
        return SUCCESS_TIP;
    }

    /**
     * 银行管理详情
     */
    @RequestMapping(value = "/detail/{sktBanksId}")
    @ResponseBody
    public Object detail(@PathVariable("sktBanksId") Integer sktBanksId) {
        return sktBanksService.selectById(sktBanksId);
    }
    /**********************************************/
    /**
     * 获取银行列表
     */
    @RequestMapping(value = "selectBanksAll")
    @ResponseBody
    public Result selectBanksAll() {
    	EntityWrapper<SktBanks> ew = new EntityWrapper<SktBanks>();
    	ew.where("dataFlag", 1);
    	List<SktBanks> list = sktBanksService.selectList(ew);
        return Result.OK(list);
    }
    
}
