package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.model.SktShops2;
import com.stylefeng.guns.modular.huamao.model.SktShops3;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商家信息表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
public interface ISktShopsService extends IService<SktShops> {
	/*
	 * 展示全部
	 * */
	public List<SktShops> sktShopsfindAll(SktShops sktShops);
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 查询商家总数
	 * @param 
	 * @return
	 */
	public int getTotal();
	/*
	 * 店铺街展示店铺
	 * */
	public List<Map<String, Object>> findShopsAll(Integer hyId, Integer provinceId, Integer totalScore, Integer onlineTurnover, Integer userId);
	/*
	 * 店铺街展示商品推荐
	 * */
	public List<Map<String, Object>> findShopsByIdtj(Integer shopId,Integer limit);
	/*
     * 店铺街查询行业
     * */
	public List<Map<String, Object>> findShopsHyName();
	/*
     * 店铺街根据行业ID点进行业查询该行业商品
     * */
	public List<Map<String, Object>> selectByHyIdShops(Integer hyId);
	/*
	 * 查询热门品牌findBrandId
	 * */
	public List<Map<String, Object>> findBrandId();
	  /*
     * 根据店铺ID查询商品
     * */
	public List<Map<String, Object>> selectShopsGoodsId(Integer shopId);
	/*
	 * 自营街展示店铺
	 * */
	public List<Map<String, Object>> selectShopsZy();
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectShopNameShops(SktShops2 sktShops2);
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectLogoShops(SktShops2 sktShops2);
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */ 
	public List<Map<String, Object>> selectLoginNameShops(SktShops2 sktShops2);
	  /*
      * 根据ZY店铺ID查询商品
      * */
 	public List<Map<String, Object>> selectShopsZyGoodsId(Integer shopId);
	  /*
     * 轮播图首页
     * */
	public List<Map<String, Object>> findWebLunbo();

	/**
	 *楼层广告图
	 * @param map
	 * @return
	 */
	public Map<String,Object> floorAd(Map<String, Object> map);

	/*
	 * 单个商品详情
	 * */
	public List<Map<String,Object>> selectGoods(Integer goodsId);
	/*
	 * 线下商家个人详情
	 * */
	public List<Map<String,Object>> findByUserId(Integer userId);
	/*
	 * 线下商家个人详情
	 * */
	public List<Map<String,Object>> findByUserIdMn(Integer userId);
	/*
	 * 线下商家个人详情 最近月份支入支出
	 * */
	public List<Map<String,Object>> findByUserIdZrZc(Integer userId);
	/*
	 * 线下商家个人详情 最近月份支入支出
	 * */
	public List<Map<String,Object>> findByShopsXinxi(Integer userId);
	/*
	 * 修改店铺设置
	 * */
	public Integer updateShops(SktShops sktShops);
	/*
	 * 线上商家店铺详情
	 * */
	public List<Map<String,Object>> findByXSShopsXinxi(Integer shopId);
	/*
	 * 回显店铺设置
	 * */
	public List<Map<String,Object>> findByXSShopsImg(Integer shopId);
	/*
	 * 修改线上店铺设置
	 * */
	public Integer updateByXSShopsImg(SktShops sktShops);
	/*
	 * 回显店铺运费
	 * */
	public List<Map<String,Object>> findByYunFei(Integer shopId);
	/*
	 * 修改店铺运费
	 * */
	public boolean updateByYunFei(List<Map<String, Object>> sktShops2);
	/*
	 * 代付款订单
	 * */
	public List<Map<String,Object>> findDaiOrders(Integer shopId, String orderNo, String beginTime, String endTime);
	/*
	 * 以付款订单
	 * */
	public List<Map<String,Object>> findYiOrders(Integer shopId, String orderNo, String beginTime, String endTime);
	/*
	 * 代发货订单
	 * */
	public List<Map<String,Object>> findDaiFaHuoOrders(Integer shopId, String orderNo, String beginTime, String endTime);
	/*
	 * 退款订单
	 * */
	public List<Map<String,Object>> findTuiOrders(Integer shopId, String orderNo, String beginTime, String endTime, Integer refundStatus);
	/*
	 * 待收货订单
	 * */
	public List<Map<String,Object>> findDaiShouOrders(Integer shopId, String orderNo, String beginTime, String endTime);
	/*
	 * 取消订单
	 * */
	public List<Map<String,Object>> findQuxiaoOrders(Integer shopId, String orderNo, String beginTime, String endTime);
	/*
	 * 根据分类查询品牌商品
	 * */
	public List<Map<String,Object>> findHPcats(SktShops2 sktShops2);
	/*
	 * 线下店铺回显
	 * */
	public List<Map<String,Object>> findShopsId(SktShops3 sktshops3);
	/*
	 * 线下店铺回显
	 * */
	public List<Map<String,Object>> selectShopsName(String shopName);
	//查询所有商家的userId
	List<Integer> selectAllId();
    /*
     * 根据goodsID查询shopId
     * */
	public List<Map<String,Object>> selectShops(Integer goodsId);
    /*
     * 根据articleId查询内容
     * */
	public List<Map<String,Object>> selectArticle(Integer articleId);

	public Map<String, Object> shopHome(Integer shopId);


	/****************APP*********************/
	/**
     * 线下商家主页
	 * @param shopId
	 * @return
	 */
	public Map<String, Object> shopsStore(Integer shopId);
	/**
     * 周边主页（包括商家名称搜索,点击行业搜索）
	 * @param lng
	 * @param lat
	 * @param shopName
	 * @param industry
	 * @return
	 */
	public List<Map<String, Object>> shopsStorePos(BigDecimal lng, BigDecimal lat, String shopName, String industry);

    Map<String,Object> appShopsDetailed(Integer shopId);
    
    
	public List<Map<String, Object>> shopsStorePosL(BigDecimal lng, BigDecimal lat);

    Map<String,Object> selectUsersByShopId(Integer shopId);

    List<Map<String,Object>> findCheckOrders(Integer shopId, String orderNo, String beginTime, String endTime);

    List<Map<String,Object>> findRefuseOrders(Integer shopId, String orderNo, String beginTime, String endTime);
}
