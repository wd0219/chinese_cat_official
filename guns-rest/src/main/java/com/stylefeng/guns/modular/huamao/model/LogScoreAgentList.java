package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 代理公司积分流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public class LogScoreAgentList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 代理公司手机
	 */
	private String phone;
	/**
	 * 积分类型 1运营积分获得 2待发运营积分获得 3后台升级补交代理费到的积分 31转换开元宝减少 32待发积分转正式积分
	 */
	private Integer type;
	/**
	 * 目标代理公司ID
	 */
	private Integer agentId;
	/**
	 * 对应订单号
	 */
	private String orderNo;
	/**
	 * 操作前的金额
	 */
	private BigDecimal preScore;
	/**
	 * 流水标志 -1减少 1增加
	 */
	private Integer scoreType;
	/**
	 * 金额
	 */
	private BigDecimal score;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String scorep;
	/**
	 * 变动后金额
	 */
	private BigDecimal ascore;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 有效状态 1有效 0删除
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getPreScore() {
		return preScore;
	}
	public void setPreScore(BigDecimal preScore) {
		this.preScore = preScore;
	}
	public Integer getScoreType() {
		return scoreType;
	}
	public void setScoreType(Integer scoreType) {
		this.scoreType = scoreType;
	}
	public BigDecimal getScore() {
		return score;
	}
	public void setScore(BigDecimal score) {
		this.score = score;
	}
	public String getScorep() {
		return scorep;
	}
	public void setScorep(String scorep) {
		this.scorep = scorep;
	}
	public BigDecimal getAscore() {
		return ascore;
	}
	public void setAscore(BigDecimal ascore) {
		this.ascore = ascore;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	

}
