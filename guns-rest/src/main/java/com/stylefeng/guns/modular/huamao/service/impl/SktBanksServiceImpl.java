package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktBanksMapper;
import com.stylefeng.guns.modular.huamao.model.SktBanks;
import com.stylefeng.guns.modular.huamao.service.ISktBanksService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 银行表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@Service
public class SktBanksServiceImpl extends ServiceImpl<SktBanksMapper, SktBanks> implements ISktBanksService {

}
