package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 代理公司的银行卡表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public class AgentsBankcardsList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	@TableId(value = "bankCardId", type = IdType.AUTO)
	private Integer bankCardId;
	/**
	 * 代理公司ID
	 */
	private Integer agentId;
	/**
	 * 代理公司名称
	 */
	private String agentName;
	/**
	 * 银行名称
	 */
	private String bankName;
	/**
	 * 省id
	 */
	private Integer provinceId;
	/**
	 * 市id
	 */
	private Integer cityId;
	/**
	 * 区县id
	 */
	private Integer areaId;
	/**
	 * 银行ID
	 */
	private Integer bankId;
	/**
	 * 开户行
	 */
	private String accArea;
	/**
	 * 银行卡号
	 */
	private String accNo;
	/**
	 * 持卡人
	 */
	private String accUser;
	/**
	 * 身份证号
	 */
	private String IDnumber;
	// 因为前台获取IDnumber是出错所以让它获取number
	private String number;
	/**
	 * 银行预留电话号码
	 */
	private String phone;
	/**
	 * 有效标志
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;

	public AgentsBankcardsList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AgentsBankcardsList(Integer bankCardId, Integer agentId, String agentName, String bankName,
			Integer provinceId, Integer cityId, Integer areaId, Integer bankId, String accArea, String accNo,
			String accUser, String iDnumber, String number, String phone, Integer dataFlag, Date createTime) {
		super();
		this.bankCardId = bankCardId;
		this.agentId = agentId;
		this.agentName = agentName;
		this.bankName = bankName;
		this.provinceId = provinceId;
		this.cityId = cityId;
		this.areaId = areaId;
		this.bankId = bankId;
		this.accArea = accArea;
		this.accNo = accNo;
		this.accUser = accUser;
		IDnumber = iDnumber;
		this.number = number;
		this.phone = phone;
		this.dataFlag = dataFlag;
		this.createTime = createTime;
	}

	public Integer getBankCardId() {
		return bankCardId;
	}

	public void setBankCardId(Integer bankCardId) {
		this.bankCardId = bankCardId;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	public String getAccArea() {
		return accArea;
	}

	public void setAccArea(String accArea) {
		this.accArea = accArea;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getAccUser() {
		return accUser;
	}

	public void setAccUser(String accUser) {
		this.accUser = accUser;
	}

	public String getIDnumber() {
		return IDnumber;
	}

	public void setIDnumber(String iDnumber) {
		IDnumber = iDnumber;
	}

	// 让number直接返回IDnumber，不需要set
	public String getNumber() {
		return IDnumber;
	}

	// public void setNumber(String number) {
	// this.number = number;
	// }

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AgentsBankcardsList [bankCardId=" + bankCardId + ", agentId=" + agentId + ", agentName=" + agentName
				+ ", bankName=" + bankName + ", provinceId=" + provinceId + ", cityId=" + cityId + ", areaId=" + areaId
				+ ", bankId=" + bankId + ", accArea=" + accArea + ", accNo=" + accNo + ", accUser=" + accUser
				+ ", IDnumber=" + IDnumber + ", number=" + number + ", phone=" + phone + ", dataFlag=" + dataFlag
				+ ", createTime=" + createTime + "]";
	}

}
