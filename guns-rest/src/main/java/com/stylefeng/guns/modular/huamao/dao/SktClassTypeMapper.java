package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktClassType;

import java.util.List;

/**
 * <p>
 * 视频类型表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface SktClassTypeMapper extends BaseMapper<SktClassType> {

    List<SktClassType> selectAllType();

}
