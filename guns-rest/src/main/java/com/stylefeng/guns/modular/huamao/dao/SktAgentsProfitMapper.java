package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktAgentsProfit;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 代理股东分红记录表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-12
 */
public interface SktAgentsProfitMapper extends BaseMapper<SktAgentsProfit> {

}
