package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;

import java.util.List;
import java.util.Map;

public interface IAppShopIndustryService extends IService<SktShopIndustry> {
    List<Map<String,Object>> selectShopsIndustryByHot();

    List<Map<String,Object>> selectAllList();
}
