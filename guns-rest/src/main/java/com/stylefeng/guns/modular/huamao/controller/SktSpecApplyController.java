package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.SktSpecApply;
import com.stylefeng.guns.modular.huamao.model.SpecCats;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktSpecApplyService;
import com.stylefeng.guns.modular.huamao.service.ISpecCatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 商品规格申请控制器
 *
 * @author py
 * @Date 2018-08-23 16:08:11
 */
@Controller
@RequestMapping("/sktSpecApply")
public class SktSpecApplyController extends BaseController {

    private String PREFIX = "/huamao/sktSpecApply/";

    @Autowired
    private ISktSpecApplyService sktSpecApplyService;
    @Autowired
    private ISpecCatsService specCatsService;

    /**
     * 新增商品规格申请
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Result add(SktSpecApply sktSpecApply) {
        Map<String, String> map = sktSpecApplyService.insertSktSpecApply(sktSpecApply);
        if("1".equals(map.get("code").toString())){
            return  Result.OK(map);
        }else{
            return Result.EEROR(map.get("msg").toString());
        }
    }
    /**
     * 查看商品规格
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Result list(SktSpecApply sktSpecApply) {
        EntityWrapper<SpecCats> entityWrapper=new  EntityWrapper<SpecCats>();
        List<SpecCats> list = specCatsService.selectList(entityWrapper);
        return Result.OK(list);
    }

}
