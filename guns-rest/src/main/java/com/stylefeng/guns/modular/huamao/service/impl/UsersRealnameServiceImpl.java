package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersRealnameMapper;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;
import com.stylefeng.guns.modular.huamao.model.UsersRealnameDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersRealnameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 实名认证信息表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
@Service
public class UsersRealnameServiceImpl extends ServiceImpl<UsersRealnameMapper, UsersRealname> implements IUsersRealnameService {

	@Autowired
	private UsersRealnameMapper usersRealnameMapper;
	@Autowired
	private UsersMapper usersMapper;
	@Override
	public List<UsersRealnameDTO> showRenZhengInfo(UsersRealnameDTO usersRealname) {
		List<UsersRealnameDTO> list = usersRealnameMapper.showRenZhengInfo(usersRealname);
		return list;
	}

	@Override
	public UsersRealnameDTO showRenZhengXiangQing(Integer realId) {
		UsersRealnameDTO usersRealnameDTO = usersRealnameMapper.showRenZhengXiangQing(realId);
		return usersRealnameDTO;
	}

	@Override
	public Map<String,Object> selectUserCardId(Integer userId) {
		Map<String,Object> map = new HashMap<String, Object>();
		map = usersRealnameMapper.selectUserCardId(userId);
		if(map != null && map.size() > 0){
			return map;
		}else{
			map.put("auditStatus","3");
			return map;
		}
	}

	@Override
	public String addUserRealName(UsersRealname usersRealname) {
		//先查询用户身份证是否超过3个
		String cardID = usersRealname.getCardID();
		Integer cardIdCount = usersRealnameMapper.selectUserCardIds(cardID);
		if(cardIdCount >= 10){
			return "LGCARD";
		}

		Users users = usersMapper.selectById(usersRealname.getUserId());
		usersRealname.setAddDatetime(new Date());
		usersRealname.setAuditStatus(0);
		usersRealname.setAuditDatetime(new Date());
		usersRealname.setPhone(users.getUserPhone());
		Integer i = usersRealnameMapper.insert(usersRealname);//返回影响行数
		return i.toString();
	}
}
