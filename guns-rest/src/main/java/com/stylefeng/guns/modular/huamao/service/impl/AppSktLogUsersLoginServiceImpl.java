package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktLogUsersLoginsMapper;
import com.stylefeng.guns.modular.huamao.model.SktLogUsersLogins;
import com.stylefeng.guns.modular.huamao.service.IAppSktLogUsersLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppSktLogUsersLoginServiceImpl extends ServiceImpl<SktLogUsersLoginsMapper,SktLogUsersLogins> implements IAppSktLogUsersLoginService {
    @Autowired
    private SktLogUsersLoginsMapper sktLogUsersLoginsMapper;
}
