package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktDateTransform;

import java.util.List;

public interface IAppDateTransformService extends IService<SktDateTransform> {
    List<SktDateTransform> selectDateTransform();
}
