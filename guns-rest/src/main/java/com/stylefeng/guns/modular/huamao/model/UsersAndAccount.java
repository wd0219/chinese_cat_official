package com.stylefeng.guns.modular.huamao.model;


import java.math.BigDecimal;

/**
 * 用户账户工具类
 * @author 刘端
 */
public class UsersAndAccount {
    //用户名
    private String loginName;
    //手机号
    private String userPhone;
    //用户类型
    private Integer userType;
    //线下商家标志
    private Integer isStore;
    //线上商家标志
    private Integer isShop;
    //商家ID
    private Integer shopId;
    //代理公司股东标志
    private Integer isAgent;
    //真实姓名
    private String trueName;
    //会员头像
    private String userPhoto;
    //支付密码
    private  String haspayPwd;
    //二维码图片地址
    private  String QRcode;
    //账号状态
    private Integer userStatus;
    //积分
    private BigDecimal score;
    //华宝
    private BigDecimal kaiyuan;
    //现金
    private BigDecimal cash;
    //累积获得分红(当登陆人是代理时显示)
    private Double totalDividend;
    //(当登录人是代理时，发展下线人数)
    private Integer inviteNum;
    //股东可分红需达到人数
    private Integer holderDividendNum;
    //购物券数量(元)
    private String shoppingCount  ;
    //购物券数量(张)
    /*private String shoppingPage;

    public String getShoppingPage() {
        return shoppingPage;
    }

    public void setShoppingPage(String shoppingPage) {
        this.shoppingPage = shoppingPage;
    }*/

    public String getShoppingCount() {
        return shoppingCount;
    }

    public void setShoppingCount(String shoppingCount) {
        this.shoppingCount = shoppingCount;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getIsStore() {
        return isStore;
    }

    public void setIsStore(Integer isStore) {
        this.isStore = isStore;
    }

    public Integer getIsShop() {
        return isShop;
    }

    public void setIsShop(Integer isShop) {
        this.isShop = isShop;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Integer isAgent) {
        this.isAgent = isAgent;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getHaspayPwd() {
        return haspayPwd;
    }

    public void setHaspayPwd(String haspayPwd) {
        this.haspayPwd = haspayPwd;
    }

    public String getQRcode() {
        return QRcode;
    }

    public void setQRcode(String QRcode) {
        this.QRcode = QRcode;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public BigDecimal getScore() {
    	if(score!=null) {
    		return score.setScale(3,BigDecimal.ROUND_HALF_UP);
    	}
    	return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public Double getTotalDividend() {
        return totalDividend;
    }

    public void setTotalDividend(Double totalDividend) {
        this.totalDividend = totalDividend;
    }

    public Integer getInviteNum() {
        return inviteNum;
    }

    public void setInviteNum(Integer inviteNum) {
        this.inviteNum = inviteNum;
    }

    public Integer getHolderDividendNum() {
        return holderDividendNum;
    }

    public void setHolderDividendNum(Integer holderDividendNum) {
        this.holderDividendNum = holderDividendNum;
    }
}
