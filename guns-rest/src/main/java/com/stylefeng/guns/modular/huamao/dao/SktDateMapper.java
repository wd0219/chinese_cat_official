package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktDate;


/**
 * <p>
 * 日期表 Mapper 接口
 * </p>
 */
public interface SktDateMapper extends BaseMapper<SktDate> {
    SktDate selectDateByDate(int date);
}
