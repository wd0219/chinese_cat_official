package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreAgentMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgent;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgentList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreAgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 代理公司积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogScoreAgentServiceImpl extends ServiceImpl<LogScoreAgentMapper, LogScoreAgent>
		implements ILogScoreAgentService {

	@Autowired
	private LogScoreAgentMapper lam;

	// 代理公司积分流水表返回
	@Override
	public List<LogScoreAgentList> selectLogScoreAgentAll(LogScoreAgentList logScoreAgentList) {
		return lam.selectLogScoreAgentAll(logScoreAgentList);
	}

}
