package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 企业认证表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
public interface ISktShopApproveService extends IService<SktShopApprove> {

	String insertApprove(SktShopApprove sktShopApprove);

}
