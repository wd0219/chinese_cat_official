package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.Articles;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppArticleService;
import com.stylefeng.guns.modular.huamao.util.CommonModel;
import com.stylefeng.guns.modular.huamao.util.EnumModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 文章
 */
@CrossOrigin
@Controller
@RequestMapping("/app")
public class AppArticleController extends BaseController{

    @Autowired
    private IAppArticleService iAppArticleService;

    /**
     * 分享规则  获取各类碎片文章
     * @param code
     * @return
     */
    @RequestMapping("/article/getArticle")
    @ResponseBody
    public AppResult getArticle(@ModelAttribute("code")String code){
        EnumModel enumModel=new EnumModel();
        Integer articlesId = enumModel.getArticlesId(code);
        if (articlesId==null){
            return AppResult.ERROR("查询失败",-1);
        }
        Articles articles=new Articles();
        articles.setArticleId(articlesId);
        Articles article = iAppArticleService.selectById(articles);
        if (-1!=article.getArticleContent().indexOf("/upload/")){
            CommonModel commonModel=new CommonModel();
            article.setArticleContent(article.getArticleContent().replace("/upload/",commonModel.getBASE_URL()+"upload/"));
        }
        return AppResult.OK("查询成功",article);
    }

    /**
     * 获取文章详情（ZJF）
     * @param articleId
     * @return
     */
    @RequestMapping("/article/companyDetail")
    @ResponseBody
    public AppResult articleDetail(@ModelAttribute("articleId")String articleId){
        Articles articles = iAppArticleService.selectArticlesById(Integer.parseInt(articleId));
        Map<Object, Object> map = new HashMap<>();
        map.put("articleContent",articles.getArticleContent());
        map.put("createTime",articles.getCreateTime());
        return AppResult.OK("查询成功",map);
    }





}
