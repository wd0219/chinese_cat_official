package com.stylefeng.guns.modular.huamao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopFreightsMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopFreights;
import com.stylefeng.guns.modular.huamao.service.ISktShopFreightsService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 店铺运费表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-06-08
 */
@Service
public class SktShopFreightsServiceImpl extends ServiceImpl<SktShopFreightsMapper, SktShopFreights> implements ISktShopFreightsService {

    @Autowired
    private SktShopFreightsMapper sktShopFreightsMapper;
    @Override
    public List<Map<String,Object>> getFreight(List<Integer> shopIds, Integer areaId2) {
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for (Integer shopId:
             shopIds) {
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("shopId",shopId);
            map.put("areaId2",areaId2);
            SktShopFreights sktShopFreights = sktShopFreightsMapper.getFreigth(map);
            if(sktShopFreights != null){
                map.put("freight",sktShopFreights.getFreight());
            }else{
                map.put("freight",0);
            }
            list.add(map);
        }
        return list;
//         List<SktShopFreights> list = sktShopFreightsMapper.getFreigth(map);
//        if(list != null && list.size() > 0){
//            for (SktShopFreights sktShopFreights:
//                 list) {
//                freights.add(sktShopFreights.getFreight());
//            }
//        }
//        return list;
    }
}
