package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktAreasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/areas")
@CrossOrigin
public class AreasController {
    @Autowired
    private ISktAreasService sktAreasService;
    @RequestMapping("/getAreas")
    public Result getAreas(Integer parentId){
        List<Map<String, Object>> maps = sktAreasService.selectByParentId(parentId);
         return   Result.OK(maps);
    }

}
