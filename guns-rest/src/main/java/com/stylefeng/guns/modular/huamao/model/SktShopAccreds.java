package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 店铺认证信息表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-13
 */
@TableName("skt_shop_accreds")
public class SktShopAccreds extends Model<SktShopAccreds> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 认证ID
     */
    private Integer accredId;
    /**
     * 店铺ID
     */
    private Integer shopId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccredId() {
        return accredId;
    }

    public void setAccredId(Integer accredId) {
        this.accredId = accredId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktShopAccreds{" +
        "id=" + id +
        ", accredId=" + accredId +
        ", shopId=" + shopId +
        "}";
    }
}
