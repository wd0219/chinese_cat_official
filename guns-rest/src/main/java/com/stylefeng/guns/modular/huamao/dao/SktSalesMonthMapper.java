package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktSalesMonth;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商家每月营业额统计表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
public interface SktSalesMonthMapper extends BaseMapper<SktSalesMonth> {
	
	/*
	 * 展示全部
	 * */
	public List<SktSalesMonth> sktSalesMonthfindAll(SktSalesMonth sktSalesMonth);

	BigDecimal getSalesMonth(Map<String, Object> sMap);
}
