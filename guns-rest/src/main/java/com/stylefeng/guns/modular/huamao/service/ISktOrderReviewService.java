package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrderReview;
import com.stylefeng.guns.modular.huamao.model.SktOrderReviewDTO;

import java.util.List;

/**
 * <p>
 * 商城订单审核表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-09-19
 */
public interface ISktOrderReviewService extends IService<SktOrderReview> {

    List<SktOrderReviewDTO> selectAll(Page<SktOrderReviewDTO> page, SktOrderReviewDTO sktOrderReviewDTO);
}
