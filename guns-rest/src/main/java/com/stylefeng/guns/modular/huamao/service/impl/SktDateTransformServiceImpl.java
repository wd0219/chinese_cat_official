package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktDateTransformMapper;
import com.stylefeng.guns.modular.huamao.model.SktDateTransform;
import com.stylefeng.guns.modular.huamao.service.ISktDateTransformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 积分转换开元宝每日记录表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
@Service
public class SktDateTransformServiceImpl extends ServiceImpl<SktDateTransformMapper, SktDateTransform> implements ISktDateTransformService {
	@Autowired
	private SktDateTransformMapper dateTransformMapper;
	@Override
	public List<SktDateTransform> sktDateTransformfindAll(SktDateTransform sktDateTransform) {
		return dateTransformMapper.sktDateTransformfindAll(sktDateTransform);
		
	}

}
