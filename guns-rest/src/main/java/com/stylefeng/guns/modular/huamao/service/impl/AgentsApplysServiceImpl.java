package com.stylefeng.guns.modular.huamao.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.stylefeng.guns.modular.huamao.dao.SktAgentsStockholderMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AgentsApplysMapper;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.service.IAgentsApplysService;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.StringUtils;

/**
 * <p>
 * 代理公司股东申请表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-17
 */
@Transactional
@Service
public class AgentsApplysServiceImpl extends ServiceImpl<AgentsApplysMapper, AgentsApplys> implements IAgentsApplysService {
	
	@Autowired
	private AgentsApplysMapper agentsApplysMapper;
	@Autowired
	private SktAgentsMapper sktAgentsMapper;
	@Autowired
	private UsersMapper usersMapper;
	@Autowired
	private SktAgentsStockholderMapper agentsStockholderMapper;
	@Override
	public List<AgentsApplysDTO> showDaiLiShenQingInfo(AgentsApplysDTO agentsApplysDTO) {
		List<AgentsApplysDTO> list = agentsApplysMapper.showDaiLiShenQingInfo(agentsApplysDTO);
		return list;
	}
	@SuppressWarnings("unused")
	@Override
	public Integer insertAgentsApplys(AgentsApplysDTO agentsApplys) {
		if(agentsApplys.getAlevel()==10){
			agentsApplys.setCitys(null);
			agentsApplys.setCounty(null);
		}
		if(agentsApplys.getAlevel()==11){
			agentsApplys.setCounty(null);
		}
		//查询出代理公司id
		SktAgents selectId = sktAgentsMapper.selectId(agentsApplys);
		Users users = usersMapper.selectById(agentsApplys.getUserId());
		agentsApplys.setPhone(users.getUserPhone());
		agentsApplys.setLegalPerson(users.getTrueName());
		agentsApplys.setAgentId(selectId.getAgentId());
		if(selectId==null){
			return 0;
		}
		AgentsApplys agentsApplys2 = new AgentsApplys();
		if(agentsApplys.getType()!=7){
			//查询是否有该类型
			agentsApplys2 = agentsApplysMapper.isSelectAgent(agentsApplys);
		}
		if(StringUtils.isEmpty(agentsApplys2)){
			return 0;
		}
		Integer i = insertApplys(agentsApplys);
		return i;
	}
	
	public Integer insertApplys(AgentsApplysDTO agentsApplys){
		AgentsApplys agentsApplys2 = new AgentsApplys();
		agentsApplys2.setAgentId(agentsApplys.getAgentId());
		agentsApplys2.setOrderNo(StringUtils.getOrderIdByTime("4"));
		agentsApplys2.setUserId(agentsApplys.getUserId());
		agentsApplys2.setType(agentsApplys.getType());
		agentsApplys2.setPhone(agentsApplys.getPhone());
		agentsApplys2.setMoneyOrderImg(agentsApplys.getMoneyOrderImg());
		Integer agentId=agentsApplys.getAgentId();
		Integer type=agentsApplys.getType();
		SktAgents agents = sktAgentsMapper.selectById(agentId);
		Integer agencyFee =agents.getAgencyFee();
		BigDecimal money=new BigDecimal(agencyFee);
		SktAgentsStockholder agentsStockholder = new SktAgentsStockholder();
		agentsStockholder.setAgentId(agentId);
		agentsStockholder.setType(type);
		Integer countStcokManger = agentsStockholderMapper.selectCountStcokManger(agentsStockholder);
		if (type == 1 ) {
			if(countStcokManger == 1){
				return 6;
			}
			agentsApplys2.setStockNum(5);
			agentsApplys2.setMoney(money.multiply(new BigDecimal(5)));
		} else if (type == 2 ) {
			if(countStcokManger == 1){
				return 2;
			}
			agentsApplys2.setStockNum(4);
			agentsApplys2.setMoney(money.multiply(new BigDecimal(4)));
		} else if (type == 3) {
			if(countStcokManger == 1){
				return 3;
			}
			agentsApplys2.setStockNum(3);
			agentsApplys2.setMoney(money.multiply(new BigDecimal(3)));
		} else if (type == 4) {
			if(countStcokManger == 1){
				return 4;
			}
			agentsApplys2.setStockNum(3);
			agentsApplys2.setMoney(money.multiply(new BigDecimal(3)));
		} else if (type == 5 ) {
			if(countStcokManger == 3){
				return 5;
			}
			agentsApplys2.setStockNum(2);
			agentsApplys2.setMoney(money.multiply(new BigDecimal(2)));
		} else if (type == 7 ) {
			if(agents.getLPsurplusStockNum() ==0){
				return 7;
			}
			agentsApplys2.setStockNum(1);
			agentsApplys2.setMoney(money.multiply(new BigDecimal(1)));
		}

		agentsApplys2.setCreateTime(new Date());
		agentsApplys2.setOneCheckTime(new Date());
		agentsApplys2.setTwoCheckTime(new Date());
		if (type==7){
			agentsApplys2.setBatFlag(0);
		}else{
			Date dNow = new Date();   //当前时间
			Date dAfter = new Date();
			Calendar calendar = Calendar.getInstance(); //得到日历
			calendar.setTime(dNow);//把当前时间赋给日历
			calendar.add(calendar.MONTH, 3);  //设置为后3月
			dAfter = calendar.getTime();   //得到后3月的时间
			agentsApplys2.setBatTime(dAfter);
			agentsApplys2.setTestTime(dAfter);
			agentsApplys2.setBatFlag(1);
		}
		Map<String,Object> map = new HashMap<String, Object>();
		int [] statu={-1,-2};
		map.put("userId", agentsApplys.getUserId());
		map.put("status",statu);
		//以前是否有记录 有就更新 没有新增
		AgentsApplys agentsApplys3 = agentsApplysMapper.selectAgentsApplysByUserIdAndStatus(map);
		if(MSGUtil.isBlank(agentsApplys3)){
			int i = agentsApplysMapper.insert(agentsApplys2);
			return i;
		}else{
			agentsApplys2.setShaId(agentsApplys3.getShaId());
			int i = agentsApplysMapper.updateById(agentsApplys2);
			return i;
		}
		
	}
	@Override
	public Map<String, Object> selectAgentsApplysInfo(Integer userId) {
		Map<String, Object> map = agentsApplysMapper.selectAgentsApplysInfo(userId);
		return map;
	}
}
