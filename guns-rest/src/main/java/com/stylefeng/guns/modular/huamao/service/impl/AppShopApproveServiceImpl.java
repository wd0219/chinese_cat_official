package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopApproveMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import com.stylefeng.guns.modular.huamao.service.IAppShopApproveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppShopApproveServiceImpl extends ServiceImpl<SktShopApproveMapper,SktShopApprove> implements IAppShopApproveService {
    @Autowired
    private SktShopApproveMapper sktShopApproveMapper;

    @Override
    public SktShopApprove selectShopApproveByUserId(Integer userId) {
        return sktShopApproveMapper.selectShopApproveByUserId(userId);
    }
}
