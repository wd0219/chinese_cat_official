package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.OptBing;
import com.stylefeng.guns.modular.huamao.service.IOptBingService;

/**
 * 操作并发限制控制器
 *
 * @author fengshuonan
 * @Date 2018-06-28 18:47:15
 */
@Controller
@RequestMapping("/optBing")
public class OptBingController extends BaseController {

    private String PREFIX = "/huamao/optBing/";

    @Autowired
    private IOptBingService optBingService;

    /**
     * 跳转到操作并发限制首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "optBing.html";
    }

    /**
     * 跳转到添加操作并发限制
     */
    @RequestMapping("/optBing_add")
    public String optBingAdd() {
        return PREFIX + "optBing_add.html";
    }

    /**
     * 跳转到修改操作并发限制
     */
    @RequestMapping("/optBing_update/{optBingId}")
    public String optBingUpdate(@PathVariable Integer optBingId, Model model) {
        OptBing optBing = optBingService.selectById(optBingId);
        model.addAttribute("item",optBing);
        LogObjectHolder.me().set(optBing);
        return PREFIX + "optBing_edit.html";
    }

    /**
     * 获取操作并发限制列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return optBingService.selectList(null);
    }

    /**
     * 新增操作并发限制
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(OptBing optBing) {
        optBingService.insert(optBing);
        return SUCCESS_TIP;
    }

    /**
     * 删除操作并发限制
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer optBingId) {
        optBingService.deleteById(optBingId);
        return SUCCESS_TIP;
    }

    /**
     * 修改操作并发限制
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(OptBing optBing) {
        optBingService.updateById(optBing);
        return SUCCESS_TIP;
    }

    /**
     * 操作并发限制详情
     */
    @RequestMapping(value = "/detail/{optBingId}")
    @ResponseBody
    public Object detail(@PathVariable("optBingId") Integer optBingId) {
        return optBingService.selectById(optBingId);
    }
}
