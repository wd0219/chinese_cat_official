package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktLogUsersLoginsMapper;
import com.stylefeng.guns.modular.huamao.model.SktLogUsersLogins;
import com.stylefeng.guns.modular.huamao.service.ISktLogUsersLoginsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 会员登陆记录表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
@Service
public class SktLogUsersLoginsServiceImpl extends ServiceImpl<SktLogUsersLoginsMapper, SktLogUsersLogins> implements ISktLogUsersLoginsService {
	@Autowired
	private SktLogUsersLoginsMapper SktLogUsersLoginsMapper;
	@Override
	public List<SktLogUsersLogins> sktLogUsersLoginsfindup(SktLogUsersLogins sktLogUsersLogins) {
		return SktLogUsersLoginsMapper.sktLogUsersLoginsfindup(sktLogUsersLogins);
	}

}
