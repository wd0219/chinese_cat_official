package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AgentsApplysMapper;
import com.stylefeng.guns.modular.huamao.model.AgentsApplys;
import com.stylefeng.guns.modular.huamao.service.IAppAgentsApplysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppAgentsApplysServiceImpl extends ServiceImpl<AgentsApplysMapper,AgentsApplys> implements IAppAgentsApplysService {
    @Autowired
    private AgentsApplysMapper agentsApplysMapper;

    @Override
    public AgentsApplys selectAgentsApplysByUserId(Integer userId) {
        return agentsApplysMapper.selectAgentsApplysByUserId(userId);
    }

    @Override
    public AgentsApplys selectAgentsApplysByUserIdAndStatus(Map<String, Object> map) {
        return agentsApplysMapper.selectAgentsApplysByUserIdAndStatus(map);
    }

    @Override
    public Map<String, Object> selectAgentsAppLysCheckApplyByUserId(Integer userId) {
        return agentsApplysMapper.selectAgentsAppLysCheckApplyByUserId(userId);
    }
}
