package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 手动补单记录返回
 * 
 * @author gxz
 *
 */
public class AccountOperationList implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 自增ID
	 */
	private Integer id;
	/**
	 * 用户id
	 */
	private Integer userId;
	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 登录名
	 */
	private String loginName;
	/**
	 * 订单编号
	 */
	private String orderNo;
	/**
	 * 操作对象 1积分 2现金 3华宝 4库存积分
	 */
	private Integer object;
	/**
	 * 变化金额
	 */
	private BigDecimal num;
	/**
	 * 标志 -1减少 1增加
	 */
	private Integer type;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String nump;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	public AccountOperationList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountOperationList(Integer id, Integer userId, String userPhone, String loginName, String orderNo,
			Integer object, BigDecimal num, Integer type, String nump, String remark, Date createTime, String beginTime,
			String endTime) {
		super();
		this.id = id;
		this.userId = userId;
		this.userPhone = userPhone;
		this.loginName = loginName;
		this.orderNo = orderNo;
		this.object = object;
		this.num = num;
		this.type = type;
		this.nump = nump;
		this.remark = remark;
		this.createTime = createTime;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Integer getObject() {
		return object;
	}

	public void setObject(Integer object) {
		this.object = object;
	}

	public BigDecimal getNum() {
		return num;
	}

	public void setNum(BigDecimal num) {
		this.num = num;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	public String getNump() {
		if (type == -1) {
			return "-" + num;
		}
		return "+" + num;
	}
	// public void setNump(String nump) {
	// this.nump = nump;
	// }

	@Override
	public String toString() {
		return "AccountOperationList [id=" + id + ", userId=" + userId + ", userPhone=" + userPhone + ", loginName="
				+ loginName + ", orderNo=" + orderNo + ", object=" + object + ", num=" + num + ", type=" + type
				+ ", nump=" + nump + ", remark=" + remark + ", createTime=" + createTime + ", beginTime=" + beginTime
				+ ", endTime=" + endTime + "]";
	}

}
