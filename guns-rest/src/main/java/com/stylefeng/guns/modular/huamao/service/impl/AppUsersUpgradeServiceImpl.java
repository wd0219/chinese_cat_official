package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersUpgradeMapper;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.service.IAppUsersUpgradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppUsersUpgradeServiceImpl extends ServiceImpl<UsersUpgradeMapper,UsersUpgrade> implements IAppUsersUpgradeService {
    @Autowired
    private UsersUpgradeMapper usersUpgradeMapper;

    @Override
    public UsersUpgrade selectUsersUpgradeByUserId(Integer userId) {
        return usersUpgradeMapper.selectUsersUpgradeByUserIdInt(userId);
    }

    @Override
    public boolean insertOptBing(Map<Object, Object> maps) {
        int i=usersUpgradeMapper.insertOptBing(maps);
        if (i==1){
            return true;
        }
        return false;
    }

    @Override
    public UsersUpgrade selectUsersUpgradeByUserIdAndStatus(Map<Object, Object> maps) {
        return usersUpgradeMapper.selectUsersUpgradeByUserIdAndStatus(maps);
    }

    @Override
    public void delectOptBingByUserIdAndCode(Map<Object, Object> maps) {
        usersUpgradeMapper.delectOptBingByUserIdAndCode(maps);
    }

    @Override
    public UsersUpgrade selectUsersUpgradeByOrderNo(Map<String, Object> orderWhere) {
        return usersUpgradeMapper.selectUsersUpgradeByOrderNo(orderWhere);
    }
}
