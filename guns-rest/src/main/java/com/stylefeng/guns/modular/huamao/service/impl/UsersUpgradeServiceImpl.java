package com.stylefeng.guns.modular.huamao.service.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersUpgradeMapper;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.model.UsersUpgradeDTO;
import com.stylefeng.guns.modular.huamao.service.IUsersUpgradeService;
import com.stylefeng.guns.modular.huamao.util.StringUtils;

/**
 * <p>
 * 用户升级角色订单表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
@Transactional
@Service
public class UsersUpgradeServiceImpl extends ServiceImpl<UsersUpgradeMapper, UsersUpgrade> implements IUsersUpgradeService {
	
	@Autowired
	private UsersUpgradeMapper usersUpgradeMapper;
	@Autowired
	private UsersMapper usersMapper;
	private static final String FINAL_ROLE = "FINAL_ROLE";
	private static final String SUCCESS = "SUCCESS";
	@Override
	public List<UsersUpgradeDTO> showUsersShengJiInfo(UsersUpgradeDTO usersUpgradeDTO) {
		List<UsersUpgradeDTO> list= usersUpgradeMapper.showUsersShengJiInfo(usersUpgradeDTO);
		return list;
	}
	
	@Override
	public String updateUpgradeStatus(Integer orderId, Integer status,Integer preRole, Integer afterRole) {
		if(afterRole == 2){//是不是经理
			return FINAL_ROLE;
		}
		int i = usersUpgradeMapper.updateUpgradeStatus(orderId,status,preRole,afterRole);
		//修改用户角色
		UsersUpgrade usersUpgrade = usersUpgradeMapper.selectById(orderId);
		Integer iUserId = usersUpgrade.getUserId(); 
		Users users = new Users();
		users.setUserId(iUserId);
		users.setUserType(afterRole);
		int j = usersMapper.updateById(users);
		//如果修改成
		if(i == 0 && j == 0){
			return FINAL_ROLE;
		}
		//获得上一级的实体
		//升级用户信息
		//Users users2 = usersMapper.selectById(iUserId);
		//获得上一级用户信息
		//Users usersUp = usersMapper.selectById(users2.getInviteId());
		//调用积分接口
		//(iUserId,orderId,usersUp);
		return SUCCESS;
	}
	/**
	 * 用户升级新增记录
	 * @param usersUpgrade
	 * @return
	 */
	public String addUserUpgradeInfo(UsersUpgrade usersUpgrade){
//		String max = usersUpgradeMapper.selectMaxValue();//查找最大值
//		BigInteger m = new BigInteger(max);
//		BigInteger randm = randomNum();
		//根据id获得升级前角色
		Users users = usersMapper.selectById(usersUpgrade.getUserId());
		usersUpgrade.setPreRole(users.getUserType());
		String no = StringUtils.getOrderIdByTime("3");//生成订单
		usersUpgrade.setOrderNo(no);
		usersUpgrade.setCreateTime(new Date());
		int i = usersUpgradeMapper.insert(usersUpgrade);
		if(i == 0 ){
			return "FAILS";
		}
		return "SUCCESS";
	}
	
	private BigInteger randomNum(){
		Random rd = new Random();  
        String[] radmon = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };  
        StringBuffer sb = new StringBuffer();  
        for (int i = 0; i < 9; i++) {  
            String s = radmon[rd.nextInt(10)];  
            sb.append(s);  
        }  
        return new BigInteger(sb.toString());
	}
	@Override
	/**
	 * 根据用户id查询最大的升级订单id  用于生成订单后返回前台
	 * @param userId
	 * @return
	 */
	public  Integer selectMaxOrderIdByUserId(Integer userId){
	return usersUpgradeMapper.selectMaxOrderIdByUserId(userId);
	}

    @Override
    public void insertOptBing(Map<Object, Object> map) {
		usersUpgradeMapper.insertOptBing(map);
    }

	@Override
	public UsersUpgrade selectUsersUpgradeByUserIdOrderNo(Map<Object, Object> map) {
		return usersUpgradeMapper.selectUsersUpgradeByUserIdOrderNo(map);
	}

	@Override
	public void updateByOrderId(UsersUpgrade usersUpgrade1) {
		usersUpgradeMapper.updateByOrderId(usersUpgrade1);
	}
}
