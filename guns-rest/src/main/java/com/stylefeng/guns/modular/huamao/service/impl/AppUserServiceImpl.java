package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersAndAccount;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppUserService;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author wangang
 * @since 2018-05-7
 */
@Service
public class AppUserServiceImpl extends ServiceImpl<UsersMapper, Users> implements IAppUserService {
	@Autowired
	private UsersMapper appUserMapper;

	// 查询用户账户列表
	@Override
	public Users findListByLoginName(String loginName) {
		Users users = appUserMapper.selectUserByLoginNameOrUserPhone(loginName);
		return users;
	}

	/**
	 * 查询分享人
	 * @param shareMan
	 * @return
	 */
	@Override
	public Users findUserByLoginNameOrUserPhone(String shareMan) {
		return appUserMapper.selectUserByLoginNameOrUserPhone(shareMan);
	}

	@Override
	public boolean addUserRegister(Users users){
		int usermsgs=appUserMapper.insert(users);
		if(usermsgs==0){
			return false;
		}
		return true;
	}

	@Override
	public Users findUsersByUserPhoneAndOldpayPwd(Users users) {
		return appUserMapper.selectUsersByUserPhoneAndPayPwd(users);
	}

	@Override
	public Users findUserByPhone(String userPhone) {
		return appUserMapper.selectUserByPhone(userPhone);
	}

	@Override
	public int updateUsersByUserPhone(Users users) {
		return appUserMapper.updateUserByUserPhone(users);
	}

    @Override
    public UsersAndAccount selectUserAccountDataById(Users user) {
        return appUserMapper.selectUserAccountDataById(user);
    }

	@Override
	public AppResult qrcode(Users user,String url) {
		Users users=appUserMapper.selectByUsersId(user);
		if (users==null){
			return AppResult.ERROR("获取失败",-1);
		}
        Map<Object, Object> map = new HashMap<>();
		map.put("loginName",users.getLoginName());
		map.put("userPhone",users.getUserPhone());
		map.put("userType",users.getUserType());
		map.put("trueName",users.getTrueName());
		map.put("userPhoto",users.getUserPhoto());
		map.put("userStatus",users.getUserStatus());
		map.put("qrCode",url);
        Map<Object, Object> maps = new HashMap<>();
        maps.put("user",map);
        System.out.println(maps.get("user").toString());
        return AppResult.OK("获取成功",maps);
	}

    @Override
    public List<Users> selectUsersInviteMan(Users user) {
		return appUserMapper.selectInvitesMan(user);
    }

	@Override
	public int selectInvitesCount(Users user) {
		return appUserMapper.selectCountInvites(user);
	}

	@Override
	public int selectChargeCount(Users user) {
		return appUserMapper.selectChargeCount(user);
	}

	@Override
	public int selectManagerCount(Users user) {
		return appUserMapper.selectManagerCount(user);
	}

    @Override
    public Users selectUsersByUserId(Integer userId) {
		return appUserMapper.selectUsersByUserId(userId
		);
    }

    @Override
    public Users selectUsersByUserIdAndUserStatus(Map<Object, Object> maps) {
		return appUserMapper.selectUsersByUserIdAndUserStatus(maps);
    }

    @Override
    public Map<String, Object> selectUsersAndUserRealNameByUserId(Integer userId) {
		return appUserMapper.selectUsersAndUserRealNameByUserId(userId);
    }

	@Override
	public Map<String, Object> selectUsersAccountByUserId(Integer userId) {
		return appUserMapper.selectUsersAccountByUserId(userId);
	}


}
