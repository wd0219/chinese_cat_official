package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 商城导航表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
@TableName("skt_navs")
public class Navs extends Model<Navs> {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 导航类型
	 */
	private Integer navType;
	/**
	 * 导航标题
	 */
	private String navTitle;
	/**
	 * 导航网址
	 */
	private String navUrl;
	/**
	 * 是否显示
	 */
	private Integer isShow;
	/**
	 * 是否新开窗口
	 */
	private Integer isOpen;
	/**
	 * 排序号
	 */
	private Integer navSort;
	/**
	 * 创建时间
	 */
	private Date createTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNavType() {
		return navType;
	}

	public void setNavType(Integer navType) {
		this.navType = navType;
	}

	public String getNavTitle() {
		return navTitle;
	}

	public void setNavTitle(String navTitle) {
		this.navTitle = navTitle;
	}

	public String getNavUrl() {
		return navUrl;
	}

	public void setNavUrl(String navUrl) {
		this.navUrl = navUrl;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	public Integer getNavSort() {
		return navSort;
	}

	public void setNavSort(Integer navSort) {
		this.navSort = navSort;
	}

	/**
	 * 后台直接设置时间不用操作
	 * 
	 * @return
	 */
	public Date getCreateTime() {
		return new Date();
	}

	// public void setCreateTime(Date createTime) {
	// this.createTime = createTime;
	// }

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Navs{" + "id=" + id + ", navType=" + navType + ", navTitle=" + navTitle + ", navUrl=" + navUrl
				+ ", isShow=" + isShow + ", isOpen=" + isOpen + ", navSort=" + navSort + ", createTime=" + createTime
				+ "}";
	}
}
