package com.stylefeng.guns.modular.huamao.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.modular.huamao.model.SktExchangeUser;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.ISktExchangeUserService;
import com.stylefeng.guns.modular.huamao.service.ISktHuamaobtLogService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;
import com.stylefeng.guns.modular.huamao.util.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author caody
 * @since 2018-06-20
 */
@Controller
@RequestMapping("/sktExchangeUser")
public class SktExchangeUserController {

    @Autowired
    ISktExchangeUserService sktExchangeUserService;

    @Autowired
    ISktHuamaobtLogService sktHuamaobtLogService;

    @Autowired
    IUsersService usersService;

    @Autowired
    IAccountService accountService;

    /**
     *获取天气接口
     * @param
     * @return
     */
    @RequestMapping(value = "/weather")
    @ResponseBody
    public Object  weather() throws UnsupportedEncodingException {


        /**
         * 测试HttpUtil接口
         */
        //参数url化
        String city = java.net.URLEncoder.encode("石家庄", "utf-8");
        //拼地址
        String apiUrl = String.format("https://www.sojson.com/open/api/weather/json.shtml?city=%s",city);
        return HttpUtil.doGet(apiUrl);

        //return null;
    }

    /**
     * 注册交易所账号
     * @param userId
     * @return
     */
    @RequestMapping(value = "/register")
    @ResponseBody
    public Result register(Integer userId) {

        /**
         * 判断用户是否已经注册
         */
        SktExchangeUser sktExchangeUser1 = new SktExchangeUser();
        sktExchangeUser1.setUserId(userId);
        SktExchangeUser rsktExchangeUser = new SktExchangeUser();
        EntityWrapper<SktExchangeUser> entityWrapper = new EntityWrapper<SktExchangeUser>(sktExchangeUser1);
        rsktExchangeUser =sktExchangeUserService.selectOne(entityWrapper);
        if (rsktExchangeUser !=  null){
            return Result.OK("用户已经注册，无需重复注册");
        }



        Users users = new Users();
        users = usersService.selectById(userId);


        String url = "https://api1.digsg.com/user/register";
        String companyID = "1";
        //String userMobile= "15308488113";
        String userMobile= users.getUserPhone();
        String secretKey = "iL34.#dnZ2ubJJFt";

        String key = userMobile+companyID+secretKey;

        String md5Info = MD5Util.encrypt(key);
        //String md5Info = "8ba6fb79659105c37a4b27b5af63d513";

        Map<String,Object> map = new HashMap<String, Object>();
        map.put("companyID",companyID);
        map.put("userMobile",userMobile);
        map.put("md5Info",md5Info);

        /**
         *调用注册接口
         */
        String json =  HttpUtil.doPost(url,map);
        JSONObject jsonObject = JSONObject.parseObject(json);
        if(jsonObject.getBoolean("isSuc")){

            JSONObject datas = jsonObject.getJSONObject("datas");

            SktExchangeUser sktExchangeUser = new SktExchangeUser();
            sktExchangeUser.setUserId(users.getUserId());
            sktExchangeUser.setUserPhone(users.getUserPhone());
            sktExchangeUser.setLoginName(users.getLoginName());
            sktExchangeUser.setExchangeUserId(datas.getInteger("userID"));
            sktExchangeUser.setCreateTime(new Date());
            try{
                sktExchangeUserService.insert(sktExchangeUser);
                return Result.OK("注册成功");
            }catch (Exception e){
                return  Result.EEROR("注册失败");
            }
        }else {
            return Result.EEROR(jsonObject.getString("des"));
        }

    }

    /**
     * 用户充值（华宝转猫币）
     * @param userId
     * @param amount
     * @return
     */
    @RequestMapping(value = "/recharge")
    @ResponseBody
    public Object recharge(Integer userId, BigDecimal huabao, BigDecimal amount, String orderNo){

        return sktHuamaobtLogService.recharge(userId,huabao,amount,orderNo);
    }

    @RequestMapping(value = "/ticker")
    @ResponseBody
    public Object ticker (){
        return sktHuamaobtLogService.ticker();
    }





}

