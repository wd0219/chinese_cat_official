package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustryDTO;
import com.stylefeng.guns.modular.huamao.model.SktStaffs;

/**
 * <p>
 * 商城职员表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-25
 */
public interface ISktStaffsService extends IService<SktStaffs> {
	
	public List<SktShopIndustryDTO> sktStaffsfindAll(SktShopIndustryDTO sktShopIndustryDTO);
	
	public List<SktShopIndustryDTO> sktStaffsfindup(SktShopIndustryDTO sktShopIndustryDTO);
	
	public void sktStaffsupdate(SktShopIndustryDTO sktShopIndustryDTO);
	
	public void sktStaffsup1(SktShopIndustryDTO sktShopIndustryDTO);
	
	/**
     * 分享链接
     * @param userid
	 * @param endTime 
	 * @param beginTime 
	 * @param userType 
	 * @param userPhone 
	 * @param loginName 
     * @return
     */
	public List<Map<String,Object>> sharingRecords(Integer userid, String loginName, String userPhone, Integer userType, String beginTime, String endTime);
	
	
}
