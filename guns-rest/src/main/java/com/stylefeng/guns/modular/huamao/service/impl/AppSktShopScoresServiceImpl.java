package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopScoresMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopScores;
import com.stylefeng.guns.modular.huamao.service.IAppSktShopScoresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppSktShopScoresServiceImpl extends ServiceImpl<SktShopScoresMapper,SktShopScores> implements IAppSktShopScoresService {
    @Autowired
    private SktShopScoresMapper sktShopScoresMapper;

    @Override
    public void updateSktShopScores(Map<String, Object> goodsShopScoresMap) {
        sktShopScoresMapper.updateSktShopScores(goodsShopScoresMap);
    }
}
