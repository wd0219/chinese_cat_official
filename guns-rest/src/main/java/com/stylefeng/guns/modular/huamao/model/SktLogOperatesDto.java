package com.stylefeng.guns.modular.huamao.model;

import java.util.Date;

public class SktLogOperatesDto {

	    private Integer operateId;
	    /**
	     * 职员ID
	     */
	    private Integer staffId;
	    /**
	     * 操作时间
	     */
	    private Date operateTime;
	    /**
	     * 所属菜单ID
	     */
	    private Integer menuId;
	    /**
	     * 操作说明
	     */
	    private String operateDesc;
	    /**
	     * 操作链接地址
	     */
	    private String operateUrl;
	    /**
	     * 请求内容
	     */
	    private String content;
	    /**
	     * 操作IP
	     */
	    private String operateIP;
	    /**
	     * 开始时间
	     */
	    private String beginTime;
	    /**
	     * 结束时间
	     */
	    private String endTime;
	    public Integer getOperateId() {
	        return operateId;
	    }

	    public void setOperateId(Integer operateId) {
	        this.operateId = operateId;
	    }

	    public Integer getStaffId() {
	        return staffId;
	    }

	    public void setStaffId(Integer staffId) {
	        this.staffId = staffId;
	    }

	    public Date getOperateTime() {
	        return operateTime;
	    }

	    public void setOperateTime(Date operateTime) {
	        this.operateTime = operateTime;
	    }

	    public Integer getMenuId() {
	        return menuId;
	    }

	    public void setMenuId(Integer menuId) {
	        this.menuId = menuId;
	    }

	    public String getOperateDesc() {
	        return operateDesc;
	    }

	    public void setOperateDesc(String operateDesc) {
	        this.operateDesc = operateDesc;
	    }

	    public String getOperateUrl() {
	        return operateUrl;
	    }

	    public void setOperateUrl(String operateUrl) {
	        this.operateUrl = operateUrl;
	    }

	    public String getContent() {
	        return content;
	    }

	    public void setContent(String content) {
	        this.content = content;
	    }

	    public String getOperateIP() {
	        return operateIP;
	    }

	    public void setOperateIP(String operateIP) {
	        this.operateIP = operateIP;
	    }

		public String getBeginTime() {
			return beginTime;
		}

		public void setBeginTime(String beginTime) {
			this.beginTime = beginTime;
		}

		public String getEndTime() {
			return endTime;
		}

		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}
	    
}
