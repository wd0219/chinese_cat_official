package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktAdPositions;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 广告位置表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface SktAdPositionsMapper extends BaseMapper<SktAdPositions> {
	
	public List<SktAdPositions> sktAdPositionsfinAll(SktAdPositions sktAdPositions);

	public List<SktAdPositions> sktAdPositionsfindLeft(Map<String,Object> map);

	public List<SktAdPositions>	sktAdPositionsfindBottom (Map<String,Object> map);
}
