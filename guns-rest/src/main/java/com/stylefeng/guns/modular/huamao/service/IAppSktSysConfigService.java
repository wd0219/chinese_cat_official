package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktSysConfigs;

import java.util.List;
import java.util.Map;

public interface IAppSktSysConfigService extends IService<SktSysConfigs> {
    String findSysConfigs(String str);

    List<SktSysConfigs> selectAllConf();

    SktSysConfigs selectSysConfByFieldCode(String confWhere);
}
