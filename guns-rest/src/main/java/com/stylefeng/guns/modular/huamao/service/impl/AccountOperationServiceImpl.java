package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AccountOperationMapper;
import com.stylefeng.guns.modular.huamao.model.AccountOperation;
import com.stylefeng.guns.modular.huamao.model.AccountOperationList;
import com.stylefeng.guns.modular.huamao.service.IAccountOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 修改账户数据记录 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-13
 */
@Service
public class AccountOperationServiceImpl extends ServiceImpl<AccountOperationMapper, AccountOperation>
		implements IAccountOperationService {

	@Autowired
	AccountOperationMapper aom;

	// 展示手动补单记录列表
	@Override
	public List<AccountOperationList> selectAccountOperationAll(AccountOperationList accountOperationList) {
		return aom.selectAccountOperationAll(accountOperationList);
	}

}
