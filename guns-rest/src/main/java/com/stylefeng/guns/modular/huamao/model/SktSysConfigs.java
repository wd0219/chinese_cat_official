package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 商城配置表
 * </p>
 *
 * @author slt123
 * @since 2018-04-26
 */
@TableName("skt_sys_configs")
public class SktSysConfigs extends Model<SktSysConfigs> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "configId", type = IdType.AUTO)
    private Integer configId;
    /**
     * 字段名称
     */
    private String fieldName;
    /**
     * 字段代码
     */
    private String fieldCode;
    /**
     * 字段值
     */
    private String fieldValue;
    /**
     * 类型1文本框 2单选 3 富文本 4 特殊下拉
     */
    private Integer type;
    /**
     * 页码
     */
    private Integer page;
    /**
     * 选项值 以“,”间隔
     */
    private String value;


    public Integer getConfigId() {
        return configId;
    }

    public void setConfigId(Integer configId) {
        this.configId = configId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldCode() {
        return fieldCode;
    }

    public void setFieldCode(String fieldCode) {
        this.fieldCode = fieldCode;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    protected Serializable pkVal() {
        return this.configId;
    }

    @Override
    public String toString() {
        return "SktSysConfigs{" +
        "configId=" + configId +
        ", fieldName=" + fieldName +
        ", fieldCode=" + fieldCode +
        ", fieldValue=" + fieldValue +
        ", type=" + type +
        ", page=" + page +
        ", value=" + value +
        "}";
    }
}
