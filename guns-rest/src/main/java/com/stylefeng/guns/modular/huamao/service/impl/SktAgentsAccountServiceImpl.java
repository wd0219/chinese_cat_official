package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsAccountMapper;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccount;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccountDTO;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 代理公司账户表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-18
 */
@Service
public class SktAgentsAccountServiceImpl extends ServiceImpl<SktAgentsAccountMapper, SktAgentsAccount> implements ISktAgentsAccountService {
	
	@Autowired
	private SktAgentsAccountMapper sktAgentsAccountMapper;
	@Override
	public List<SktAgentsAccountDTO> showAgentsAccountInfo(SktAgentsAccountDTO sktAgentsAccountDTO) {
		List<SktAgentsAccountDTO> list = sktAgentsAccountMapper.showAgentsAccountInfo(sktAgentsAccountDTO);
		return list;
	}
	
}
