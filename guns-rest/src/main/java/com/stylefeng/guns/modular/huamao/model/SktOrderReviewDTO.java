package com.stylefeng.guns.modular.huamao.model;

/**
 * <p>
 * 商城订单审核扩展类
 * </p>
 *
 * @author gxz123
 * @since 2018-09-19
 */
public class SktOrderReviewDTO extends SktOrderReview {

    private String loginName;

    private String userPhone;

    private String staffName;

    private String beginTime;

    private String endTime;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
