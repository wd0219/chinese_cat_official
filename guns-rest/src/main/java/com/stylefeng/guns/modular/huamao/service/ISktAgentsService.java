package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcards;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgent;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgentList;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgent;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgentList;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsDTO;
import com.stylefeng.guns.modular.huamao.model.SktAgentsProfit;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderRet;

/**
 * <p>
 * 代理公司表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
public interface ISktAgentsService extends IService<SktAgents> {
	/**
	 * 查询显示列表
	 * @param sktAgentsDTO
	 * @return
	 */
	public List<SktAgentsDTO> showSktAgentsIno(SktAgentsDTO sktAgentsDTO);
	
	/**
	 * 查询用户名和电话 id
	 * @param nameOrPhone
	 * @return
	 */
	public Map<String, Object> selectNameOrPhone(String nameOrPhone);
	/**
	 * 登录代理公司
	 * @param
	 * @return
	 */
	public Map<String,Object> agentsLogin(String loginNameOrPhone, String loginPwd);

    /**
     * 查询代理公司基础信息
     * @param sktAgents
     * @return
     */
    public Map<String,Object> selectAgentsInfo(SktAgents sktAgents);

	/**
	 * 查询华宝
	 * @param sktAgentsDTO
	 * @return
	 */
	public List<LogKaiyuanAgent> selectAgentsHuabao(LogKaiyuanAgentList sktAgentsDTO);
	/**
	 * 查询积分
	 * @param sktAgentsDTO
	 * @return
	 */
	public List<LogScoreAgent> selectAgentsscore(LogScoreAgentList logScoreAgentList);
	/**
	 * 查询股东
	 * @param sktAgentsStockholderDTO
	 * @return
	 */
	public List<SktAgentsStockholderRet> selectAgentsStockholder(SktAgentsStockholderDTO sktAgentsStockholderDTO);
	/**
	 * 查询商家
	 * @param shopName 
	 * @param pageNum 
	 * @param accountShop
	 * @return
	 */
	public List<Map<String,Object>> selectAccountShop(Integer agentId, String shopName, Integer pageNum);
	/**
	 * 查询代理银行卡
	 * @param agentsBankcards
	 * @return
	 */
	public List<AgentsBankcards> selectAgentBankcards(AgentsBankcards agentsBankcards);
	/**
	 * 修改密码
	 * @param agentsBankcards
	 * @return
	 */
	public Integer updateAgentPwd(SktAgentsDTO sktAgents);
	/**
	 * 修改支付
	 * @param sktAgents
	 * @return
	 */
	public Integer updateAgentPayPwd(SktAgentsDTO sktAgents);
	/**
	 * 查看分红记录
	 * @param userId
	 * @return
	 */
	public List<SktAgentsProfit> selectAgentsProfit(Integer userId);

	/**
	 * 商城查看
	 */
	public List<Map<String,Object>> selectShopInfo(Integer shopId);
}
