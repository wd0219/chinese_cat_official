package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustryDTO;
import com.stylefeng.guns.modular.huamao.model.SktStaffs;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商城职员表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-25
 */
public interface SktStaffsMapper extends BaseMapper<SktStaffs> {
	
	public List<SktShopIndustryDTO> sktStaffsfindAll(SktShopIndustryDTO sktShopIndustryDTO);
	
	public List<SktShopIndustryDTO> sktStaffsfindup(SktShopIndustryDTO sktShopIndustryDTO);
	
	public void sktStaffsupdate(SktShopIndustryDTO sktShopIndustryDTO);
	
	public void sktStaffsup1(SktShopIndustryDTO sktShopIndustryDTO);
	
	/**
	 * 查询现金分享链接
	 * @param userId
	 * @return
	 */
	public List<LogCash> selectLogCash(Integer userId);
	
	/**
	 * 查询积分分享链接
	 * @param userId
	 * @return
	 */
	public List<LogScore> selectLogScore(Integer userId);
	
	/**
	 * 查询分享人数
	 * @param userId
	 * @return
	 */
	public Integer selectShareNumber(Integer userId);
	
	
}
