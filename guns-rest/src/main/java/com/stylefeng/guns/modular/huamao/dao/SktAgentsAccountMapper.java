package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccount;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccountDTO;

/**
 * <p>
 * 代理公司账户表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-18
 */
public interface SktAgentsAccountMapper extends BaseMapper<SktAgentsAccount> {
	/**
	 * 页面显示
	 * @param sktAgentsAccountDTO
	 * @return
	 */
	public List<SktAgentsAccountDTO> showAgentsAccountInfo(SktAgentsAccountDTO sktAgentsAccountDTO);

    SktAgentsAccount findAgentAccountById(Integer agentId);

	Integer updateScoreToKaiyuan(SktAgentsAccount sktAgentsAccount);

	SktAgentsAccount selectAgentAccountByPhone(Map<String,Object> map);
}
