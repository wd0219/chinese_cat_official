package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.*;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.service.IMessagesService;
import com.stylefeng.guns.modular.huamao.service.ISktOrderRefundsService;
import com.stylefeng.guns.modular.huamao.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

/**
 * <p>
 * 订单退款记录表 服务实现类
 * </p>
 *
 * @author caody123
 * @since 2018-04-19
 */
@Service
public class SktOrderRefundsServiceImpl extends ServiceImpl<SktOrderRefundsMapper, SktOrderRefunds> implements ISktOrderRefundsService {
	
	@Autowired
	SktOrderRefundsMapper sorm;

	@Autowired
	ISktOrderRefundsService sktOrderRefundsService;
	
	@Autowired
	SktOrdersMapper som;
	
	@Autowired
	AccountMapper am;
	
	@Autowired
	AccountOperationMapper aom;
	@Autowired
	private SktShopsMapper sktShopsMapper;
	@Autowired
	private IMessagesService messagesService;
	@Autowired
	private SktOrdersMapper sktOrdersMapper;
	@Override
	public Object selectOrderRefundList(SktOrderRefundsList sktOrderRefundsList) {
		// TODO Auto-generated method stub
		Object test = sorm.selectOrderRefundsList(sktOrderRefundsList);
		return test;
	}

	@Override		
	public Object selectOrderRefundsToRefund(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return sorm.selectOrderRefundsToRefund(param);
	}

	@Override
	@Transactional
	public Object examineOrderRefundsToRefund(SktOrderRefunds sktOrderRefunds,Integer staffId,String orderNo) {
		
		
		try {
			 /**
	         *退单成功，更新 退单表相关状态		
	         */
			sorm.updateById(sktOrderRefunds);
	 		/**
	 		 * 退单成功，更新 订单表相关状态
	 		 */
			SktOrders sktOrders = new SktOrders();
			sktOrders.setOrderId(sktOrderRefunds.getOrderId());
			sktOrders.setIsRefund(1);
			som.updateById(sktOrders);
			/**
			 * 退单成功，更新 会员账户表金额
			 */
			Account account = new Account();
			account.setUserId(sktOrderRefunds.getRefundTo());
			Account result= am.selectOne(account);
			account.setAccountId(result.getAccountId());
			account.setCash(result.getCash().add(sktOrderRefunds.getBackMoney()));
			//am.updateOrderRefunds(account);
			am.updateById(account);
			/**
			 * 退单成功，增加修改账户记录
			 */
			AccountOperation accountOperation = new AccountOperation();
			accountOperation.setOrderNo(orderNo);
			accountOperation.setUserId(sktOrderRefunds.getRefundTo());
			accountOperation.setNum(sktOrderRefunds.getBackMoney());
			accountOperation.setStaffId(staffId);
			accountOperation.setRemark("退单金额");
			accountOperation.setObject(2);
			accountOperation.setType(1);
			accountOperation.setDataFlag(1);
			Date d = new Date();
			accountOperation.setCreateTime(d);
			aom.insert(accountOperation);
			
			return true; 
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return false;
		}
       
		
	}

	@Override
	public List<Map<String, Object>> selectOrdersDetail(Integer sktOrderRefundsId) {
		List<Map<String, Object>> list = sorm.selectOrdersDetail(sktOrderRefundsId);
		return list;
	}

	@Override
	public List<Map<String, Object>> sktOrderRefundsSelectGoodsDetail(Integer orderId) {
		List<Map<String, Object>> list = sorm.sktOrderRefundsSelectGoodsDetail(orderId);
		return list;
	}
/***********************前台******************************/
	/**
	 * 用户申请退款
	 * @param sktOrderRefunds

	 * @return
	 */
	@Override
	@Transactional
	public JSONObject applayRefundsOrders(SktOrderRefunds sktOrderRefunds){
		JSONObject json=new JSONObject();
		try{
			sktOrderRefunds.setRefundTradeNo(StringUtils.getOrderIdByTime("9"));
			sktOrderRefunds.setRefundTime(new Date());
			sktOrderRefunds.setRefundStatus(0);
			if(sktOrderRefunds.getRefundRemark()==null ||"".equals(sktOrderRefunds.getRefundRemark())){
				sktOrderRefunds.setRefundRemark("用户请求退货");
			}
			sktOrderRefunds.setCreateTime(new Date());
			sorm.insert(sktOrderRefunds);
			SktOrders sktOrders = som.selectById(sktOrderRefunds.getOrderId());
			sktOrders.setOrderStatus(-3);
			sktOrdersMapper.updateById(sktOrders);
			SktShops sktShops = sktShopsMapper.selectById(sktOrders.getShopId());
			List<Integer> usersIds=new ArrayList<Integer>();
			usersIds.add(sktShops.getUserId());
			messagesService.addMessages(1,1,usersIds,"订单【"+sktOrders.getOrderNo()+"】用户申请退款，请及时处理。",null);
			json.put("code","01");
			json.put("msg","操作成功");
		}catch (Exception e ){
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			json.put("code","00");
			json.put("msg","操作失败");
			e.printStackTrace();
		}
		return json;
	}

	/**
	 * 商家对退货请求的处理
	 * @param sktOrderRefunds
	 * @return
	 */
	@Override
	@Transactional
	public JSONObject echoRefundsOrders(SktOrderRefunds sktOrderRefunds){
		JSONObject json=new JSONObject();
		try{
			SktOrderRefunds sktOrderRefunds2 = new SktOrderRefunds();
			SktOrders sktOrders = som.selectById(sktOrderRefunds.getId());
			sktOrderRefunds2.setOrderId(sktOrders.getOrderId());
			EntityWrapper<SktOrderRefunds> ew = new EntityWrapper<SktOrderRefunds>(sktOrderRefunds2);
			SktOrderRefunds	sktOrderRefunds1=sktOrderRefundsService.selectOne(ew);
			sktOrderRefunds.setId(sktOrderRefunds1.getId());
			sorm.updateById(sktOrderRefunds);
			StringBuffer  msg=new StringBuffer();
			if (sktOrderRefunds.getRefundStatus()==-1){
				msg.append("商家不同意退款原因"+sktOrderRefunds.getShopRejectReason());
			}
			if (sktOrderRefunds.getRefundStatus()==1){
				msg.append("商家同意退款");
			}
			List<Integer> usersIds=new ArrayList<Integer>();
			usersIds.add(sktOrderRefunds1.getRefundTo());
			messagesService.addMessages(1,1,usersIds,"您的订单【"+sktOrders.getOrderNo()+"】,"+msg,null);
			json.put("code","01");
			json.put("msg","操作成功");
		}catch (Exception e ){
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			json.put("code","00");
			json.put("msg","操作失败");
		}
		return json;
	}

	@Override
	public Integer appShopRefund(String remark, Integer type, String orderNo) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("remark",remark);
		map.put("type",type);
		map.put("orderNo",orderNo);
		Integer update = sorm.appShopRefund(map);
		return update;
	}
}
