package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsStockholderMapper;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholder;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsService;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsStockholderService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代理公司的股东表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-19
 */
@Service
public class SktAgentsStockholderServiceImpl extends ServiceImpl<SktAgentsStockholderMapper, SktAgentsStockholder> implements ISktAgentsStockholderService {
	
	@Autowired
	private SktAgentsStockholderMapper sktAgentsStockholderMapper;
	@Autowired
	private IUsersService iUsersService;
	@Autowired
	private ISktAgentsService iSktAgentsService;
	private final String SUCCESS = "SUCCESS";
	private final String FALSE = "FALSE";
	private final String TYPE = "TYPE";//用户类型
	private final String ISAGENTS = "ISAGENTS";//是否是管理股东
	private final String AUDITSTATUS = "AUDITSTATUS";//是否实名认证
	private final String HASAGENT = "HASAGENT";// 已有该股东
	
	@Override
	public List<SktAgentsStockholderDTO> showSktStcokHolderInfo(SktAgentsStockholderDTO sktAgentsStockholderDTO) {
		List<SktAgentsStockholderDTO> list = sktAgentsStockholderMapper.showSktStcokHolderInfo(sktAgentsStockholderDTO);
		return list;
	}

	@Override
	public String selectSktStcokManger(Integer agentId, Integer type) {
		SktAgentsStockholder sktAgentsStockholder = sktAgentsStockholderMapper.selectSktStcokManger(agentId, type);
		if(ToolUtil.isEmpty(sktAgentsStockholder)){
			return SUCCESS;
		}
		return FALSE;
	}

	@Override
	public Map<String, Object> selectNameOrPhone(String nameOrPhone) {
		Map<String,Object> map = sktAgentsStockholderMapper.selectNameOrPhone(nameOrPhone);
		return map;
	}

	@Override
	public void updateSktAgentsStockHolder(SktAgentsStockholder sktAgentsStockholder) {
		sktAgentsStockholderMapper.updateSktAgentsStockHolder(sktAgentsStockholder);
	}

	@Override
	@Transactional
	public String insertSktStockHodler(SktAgentsStockholderDTO sktAgentsStockholder) {
		//判断用户信息 如实名认证 管理层
		Map<String,Object> usersMap = sktAgentsStockholderMapper.selectUserById(sktAgentsStockholder.getUserId());
		if(usersMap.containsKey("userType") && "0".equals(usersMap.get("userType"))){
			return TYPE;
		}
		if(usersMap.containsKey("isAgent") && !"false".equals(usersMap.get("isAgent").toString())){
			return ISAGENTS;
		}
		if(usersMap.containsKey("auditStatus") && !"1".equals(usersMap.get("auditStatus"))){
			return AUDITSTATUS;
		}
		//根据省市县判断地区
		Integer alevel = sktAgentsStockholder.getAlevel();
		//是否有该职位
		Map<String, Object> map = getMap(alevel,sktAgentsStockholder);
		if(map != null){
			return HASAGENT;
		}
		try {
			//获取代理公司id
			Map<String, Object> map2 = getMap2(alevel,sktAgentsStockholder);
			//新增股东申请表
			SktAgentsStockholder agentsStockholder = new SktAgentsStockholder();
			agentsStockholder.setAgentId(Integer.parseInt(map2.containsKey("agentId")?map2.get("agentId").toString():null));
			agentsStockholder.setUserId(sktAgentsStockholder.getUserId());
			agentsStockholder.setType(sktAgentsStockholder.getType());
			agentsStockholder.setStockNum(sktAgentsStockholder.getType()==1?5:1);
			agentsStockholder.setStoreNum(0);
			agentsStockholder.setCreateTime(DateUtil.parseTime(DateUtil.getTime()));
			sktAgentsStockholderMapper.insert(agentsStockholder);
			//修改用户表
			Users users = new Users();
			users.setUserId(sktAgentsStockholder.getUserId());
			users.setIsAgent(sktAgentsStockholder.getAlevel());
			iUsersService.updateById(users);
			//修改代理公司表
			SktAgents sktAgents = new SktAgents();
			sktAgents = iSktAgentsService.selectById(map2.containsKey("agentId")?map2.get("agentId").toString():null);
			sktAgents.setStockNum(sktAgents.getStockNum()+1);
			if(sktAgentsStockholder.getType()==1){
				sktAgents.setGPsurplusStockNum(sktAgents.getTotalStockNum()-5);
			}else{
				sktAgents.setGPsurplusStockNum(sktAgents.getTotalStockNum()-1);
			}
			iSktAgentsService.updateById(sktAgents);
			return SUCCESS;
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return FALSE;
		}
	}
	
	
	public Map<String,Object> getMap(Integer alevel, SktAgentsStockholderDTO sktAgentsStockholder){
		Map<String, Object> map = null;
		if(alevel==10){
			map  = 	sktAgentsStockholderMapper.selectSktAgentsStockUser(sktAgentsStockholder.getProvince(),
					null,null,sktAgentsStockholder.getAlevel(),sktAgentsStockholder.getType());
		}else if(alevel ==  11){
			map  = 	sktAgentsStockholderMapper.selectSktAgentsStockUser(sktAgentsStockholder.getProvince(),
					sktAgentsStockholder.getCitys(),null,sktAgentsStockholder.getAlevel(),sktAgentsStockholder.getType());
		}else if(alevel == 12){
			map  = 	sktAgentsStockholderMapper.selectSktAgentsStockUser(sktAgentsStockholder.getProvince(),
					sktAgentsStockholder.getCitys(),sktAgentsStockholder.getCounty(),sktAgentsStockholder.getAlevel(),sktAgentsStockholder.getType());
		}
		return map;
	}
	
	public Map<String,Object> getMap2(Integer alevel, SktAgentsStockholderDTO sktAgentsStockholder){
		Map<String, Object> map = null;
		if(alevel==10){
			map  = 	sktAgentsStockholderMapper.selectSktAgentsStockUser(sktAgentsStockholder.getProvince(),
					null,null,sktAgentsStockholder.getAlevel(),null);
		}else if(alevel ==  11){
			map  = 	sktAgentsStockholderMapper.selectSktAgentsStockUser(sktAgentsStockholder.getProvince(),
					sktAgentsStockholder.getCitys(),null,sktAgentsStockholder.getAlevel(),null);
		}else if(alevel == 12){
			map  = 	sktAgentsStockholderMapper.selectSktAgentsStockUser(sktAgentsStockholder.getProvince(),
					sktAgentsStockholder.getCitys(),sktAgentsStockholder.getCounty(),sktAgentsStockholder.getAlevel(),null);
		}
		return map;
	}
}
