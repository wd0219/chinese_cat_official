package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktSysConfigsMapper;
import com.stylefeng.guns.modular.huamao.model.SktSysConfigs;
import com.stylefeng.guns.modular.huamao.service.ISktSysConfigsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商城配置表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-26
 */
@Service
public class SktSysConfigsServiceImpl extends ServiceImpl<SktSysConfigsMapper, SktSysConfigs> implements ISktSysConfigsService {
	@Autowired
	private SktSysConfigsMapper sktSysConfigsMapper;
	@Override
	public List<Map<String, Object>> selectBaseSetInfo(Integer page) {
		List<Map<String, Object>> list = sktSysConfigsMapper.selectBaseSetInfo(page);
		return list;
	}
	@Override
	@Transactional
	public Object updateByIds(List<SktSysConfigs> sktSysConfigs) {
		try {
			for(SktSysConfigs s : sktSysConfigs){
				sktSysConfigsMapper.updateById(s);
			}
			return true; 
		} catch (Exception e) {
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return false;
		}
	}
	@Override
	public void updateFieldCode(String fieldCode, String fieldValue) {
		sktSysConfigsMapper.updateFieldCode(fieldCode,fieldValue);
	}
@Override
public  SktSysConfigs	selectSysConfigsByFieldCode  (String str){
	return 	sktSysConfigsMapper.selectSysConfigsByFieldCode(str);

}
	@Override
	public Map<String, Object> getQR() {
		Map<String, Object> map=new HashMap<>();
		String weChatQR ="";
		Integer weChatQRId = null;
		String alipayQR = "";
		Integer alipayQRId = null;
		SktSysConfigs weChatQRvalue=sktSysConfigsMapper.selectSysConfigsByFieldCode("weChatQR ");
		if (weChatQRvalue!=null){
			weChatQR = weChatQRvalue.getFieldValue();
			weChatQRId = weChatQRvalue.getConfigId();
		}
		SktSysConfigs alipayQRValue=sktSysConfigsMapper.selectSysConfigsByFieldCode("alipayQR ");
		if (alipayQRValue!=null){
			alipayQR = alipayQRValue.getFieldValue();
			alipayQRId = alipayQRValue.getConfigId();
		}
		map.put("alipayQR",alipayQR);
		map.put("weChatQR",weChatQR);;
		map.put("weChatQRId",weChatQRId);;
		map.put("alipayQRId",alipayQRId);
		return map;
	}
}
