package com.stylefeng.guns.modular.huamao.util;

import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import com.stylefeng.guns.modular.huamao.result.AppResult;

import java.util.HashMap;
import java.util.Map;

public class Validate {

    public static Map<String,Object> validate(SktShopApprove sktShopApprove){
        Map<String, Object> map = new HashMap<>();
        if(sktShopApprove.getIsLegal()==1 || sktShopApprove.getIsLegal()==0){
        }else {
            map.put("Msg","请选择是否法人");
            return map;
        }
        if (sktShopApprove.getLegalPerson().length()<1 && sktShopApprove.getLegalPerson().length()>20){
            map.put("Msg","必须填写法人姓名|法人姓名长度为1-20");
            return map;
        }
        if (sktShopApprove.getCompanyName().length()<1&&sktShopApprove.getCompanyName().length()>20){
            map.put("Msg","请输入公司名称|企业名称长度为1-20");
            return map;
        }
        if (sktShopApprove.getLicenseNo().length()>101){
            map.put("Msg","请输入营业执照号码|营业执照号码长度超出限制");
            return map;
        }
        if (sktShopApprove.getCompanyType()==1||sktShopApprove.getCompanyType()==0){
        }else {
            map.put("Msg","请选择企业类型|请选择企业类型");
            return map;
        }
        if (sktShopApprove.getShopName().length()<1&&sktShopApprove.getShopName().length()>50){
            map.put("Msg","请输入店铺名称|店铺名称长度为1-50");
            return map;
        }
        if (sktShopApprove.getAddress().length()<1&&sktShopApprove.getAddress().length()>100){
            map.put("Msg","请输入经营地址|经营地址长度为1-100");
            return  map;
        }
        if (sktShopApprove.getTelephone().length()<1&&sktShopApprove.getTelephone().length()>50){
            map.put("Msg","请输入经营电话|长度超出限制");
            return map;
        }
        if (sktShopApprove.getLicenseMerge()==1||sktShopApprove.getLicenseMerge()==0){
        }else {
            map.put("Msg","请选择上传类型|请选择上传类型");
            return map;
        }
        if (sktShopApprove.getLicenseImg().length()<6){
            map.put("Msg","请上传营业执照");
            return map;
        }
        if (sktShopApprove.getLegalPersonImg().length()<6){
            map.put("Msg","请上传手持身份证照片");
            return map;
        }
        if (sktShopApprove.getLogo().length()<6){
            map.put("Msg","请上传logo");
            return map;
        }
        map.put("Msg","1");
        return map;
    }

}
