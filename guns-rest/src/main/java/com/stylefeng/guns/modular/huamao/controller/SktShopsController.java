package com.stylefeng.guns.modular.huamao.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.result.Result;


/**
 * 商家信息控制器
 *
 * @author fengshuonan
 * @Date 2018-04-14 14:19:44
 */
@CrossOrigin()
@Controller
@RequestMapping("/sktShops")
public class SktShopsController extends BaseController {

    private String PREFIX = "/huamao/sktShops/";

    @Autowired
    private ISktShopsService sktShopsService;
    @Autowired
    private ISktOrdersService  sktOrdersService;
    @Autowired
    private IGoodsCatsService goodsCatsService;
    @Autowired
    private ISktSysConfigsService sktSysConfigsService;
    @Autowired
    private IShoppingService shoppingService;
    /**
     * 跳转到商家信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShops.html";
    }

    /**
     * 跳转到添加商家信息
     */
    @RequestMapping("/sktShops_add")
    public String sktShopsAdd() {
        return PREFIX + "sktShops_add.html";
    }

    /**
     * 跳转到修改商家信息
     */
    @RequestMapping("/sktShops_update/{sktShopsId}")
    public String sktShopsUpdate(@PathVariable Integer sktShopsId, Model model,SktShops sktShops) {
    	sktShops.setShopId(sktShopsId);
        List<SktShops> list = sktShopsService.sktShopsfindAll(sktShops);
        model.addAttribute("item",list.get(0));
        //LogObjectHolder.me().set(sktShops);
        return PREFIX + "sktShops_edit.html";
    }

    /**
     * 获取商家信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktShopsService.selectList(null);
    }
    /**
     * 获取商家信息列表
     */
    @RequestMapping(value = "/list2")
    @ResponseBody
    public Object list2(String condition) {
        return sktShopsService.selectList(null);
    }

    /**
     * 新增商家信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShops sktShops) {
        sktShopsService.insert(sktShops);
        return SUCCESS_TIP;
    }

    /**
     * 删除商家信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopsId) {
        sktShopsService.deleteById(sktShopsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商家信息
     */
    @RequestMapping(value = "/updateShops")
    @ResponseBody
    public Result updateShops(SktShops sktShops) {
    	Integer shops = sktShopsService.updateShops(sktShops);
        if(ToolUtil.isEmpty(shops)){
          	return Result.EEROR("店铺设置");
        }
        return Result.OK(null);
    }

    /**
     * 商家信息详情
     */
    @RequestMapping(value = "/detail/{sktShopsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopsId") Integer sktShopsId) {

        SktShops sktShops = sktShopsService.selectById(sktShopsId);
        if (sktShops.getShopStatus()==0){
            return Result.EEROR("该商家已关闭");
        }
        return sktShops;
    }
    /**
     * 店铺街展示
     * @param pageNum 第几页
     * @param hyId 行业id
     * @param provinceId 省id
     * @param totalScore 商铺总分 1升序 2降序
     * @param onlineTurnover 商铺销售量 1升序 2降序
     * @return
     */
    @RequestMapping(value="/findShopsAll")
    @ResponseBody
    public Result findShopsAll(@RequestParam(value ="pageNum",defaultValue="1",required=false)Integer pageNum,
    		@RequestParam(value ="hyId",required=false)Integer hyId,	
    		@RequestParam(value ="userId",required=false)Integer userId,	
    		@RequestParam(value ="provinceId",required=false)Integer provinceId,	
    		@RequestParam(value ="totalScore",required=false)Integer totalScore,	
    		@RequestParam(value ="onlineTurnover",required=false)Integer onlineTurnover	
    		){
   	 	PageHelper.startPage(pageNum,4);
    	List<Map<String, Object>> list = sktShopsService.findShopsAll(hyId,provinceId,totalScore,onlineTurnover,userId);
    	for(Map<String, Object> x : list){
    		List<Map<String,Object>> list2 = sktShopsService.findShopsByIdtj((Integer)x.get("shopId"),3);
    		x.put("isRecom",list2);
    	}
    	PageInfo<Map<String, Object>> info = new PageInfo<>(list);
    	list = info.getList();
    	return Result.OK(list,info.getPages());
    }
    /*
     * 店铺街店家推荐商品
     * */
    @RequestMapping(value="/findShopsByIdtj")
    @ResponseBody
    public Result findShopsByIdtj(Integer shopId){
    	List<Map<String, Object>> list = sktShopsService.findShopsByIdtj(shopId,null);
    	return Result.OK(list);
    }
    /*
     * 店铺街查询行业
     * */
    @RequestMapping(value="/findShopsHyName")
    @ResponseBody
    public Result findShopsHyName(){
    	List<Map<String, Object>> list = sktShopsService.findShopsHyName();
    	return Result.OK(list);
    }
    /*
     * 店铺街根据行业ID查询店铺
     * */
    @RequestMapping(value="/selectByHyIdShops")
    @ResponseBody
    public Result selectByHyIdShops(Integer hyId,Integer pageSize1,Integer pageNum1){
    	pageSize = pageSize1!=null?pageSize1:pageSize;
    	pageNum = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
   	 	PageHelper.startPage(pageNum,pageSize);
    	List<Map<String, Object>> map = sktShopsService.selectByHyIdShops(hyId);
    	PageInfo<Map<String, Object>> info = new PageInfo<>(map);
    	map = info.getList();
    	return Result.OK(map);
    }
    /*
     * 查询热门品牌
     * */
    @RequestMapping(value="/findBrandId")
    @ResponseBody
    public Result findBrandId(){
    	List<Map<String, Object>> findBrandId = sktShopsService.findBrandId();
    	return Result.OK(findBrandId);
    }
    /*
     * 根据店铺ID查询商品
     * */
    @RequestMapping(value="/selectShopsGoodsId")
    @ResponseBody
    public Result selectShopsGoodsId(Integer shopId,Integer pageSize1,Integer pageNum1){
    	pageSize = pageSize1!=null?pageSize1:pageSize;
    	pageNum = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
   	 	PageHelper.startPage(pageNum,pageSize);
    	List<Map<String, Object>> selectShopsGoodsId = sktShopsService.selectShopsGoodsId(shopId);
    	PageInfo<Map<String, Object>> info = new PageInfo<>(selectShopsGoodsId);
     	selectShopsGoodsId = info.getList();
     	return Result.OK(selectShopsGoodsId);
    }
    private Integer pageSize = 10;
    private Integer pageNum = 1;
    /*
     *自营超市展示 
     * */
     @RequestMapping(value="/selectShopsZy")
     @ResponseBody
     public Result selectShopsZy(Integer pageSize1,Integer pageNum1){
    	 pageSize = pageSize1!=null?pageSize1:pageSize;
    	 pageNum = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
    	PageHelper.startPage(pageNum,pageSize);
     	List<Map<String, Object>> list = sktShopsService.selectShopsZy();
     	for(Map<String, Object> x : list){
     		List<Map<String,Object>> list2 = sktShopsService.findShopsByIdtj((Integer)x.get("shopId"),null);
     		x.put("isRecom",list2);
     	}
     	PageInfo<Map<String, Object>> info = new PageInfo<>(list);
     	list = info.getList();
     	 return Result.OK(list);
     } 
     /*
      * 根据LOGO 店铺名 loginName查询店铺ID selectLogoShops
      * */
     @RequestMapping(value="/selectShopNameShops")
     @ResponseBody
     public Result selectShopNameShops(SktShops2 sktShops2){
    	 List<Map<String, Object>> list = sktShopsService.selectShopNameShops(sktShops2);
    	 return Result.OK(list);
     }
     
     /*
      * 根据LOGO 店铺名 loginName查询店铺ID 
      * */
     @RequestMapping(value="/selectLogoShops")
     @ResponseBody
     public Result selectLogoShops(SktShops2 sktShops2){
    	 List<Map<String, Object>> list = sktShopsService.selectLogoShops(sktShops2);
    	 return Result.OK(list);
     } 
     /*
      * 根据LOGO 店铺名 loginName查询店铺ID 
      * */
     @RequestMapping(value="/selectLoginNameShops")
     @ResponseBody
     public Result selectLoginNameShops(SktShops2 sktShops2){
    	 List<Map<String, Object>> list = sktShopsService.selectLoginNameShops(sktShops2);
    	 return Result.OK(list);
     }
     /*
      * 根据店铺ID查询商品
      * */ 
     @RequestMapping(value="/selectShopsZyGoodsId")
     @ResponseBody
     public Result selectShopsZyGoodsId(Integer shopId,Integer pageSize1,Integer pageNum1){
    	 pageSize = pageSize1!=null?pageSize1:pageSize;
    	 pageNum = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
    	PageHelper.startPage(pageNum,pageSize);
     	List<Map<String, Object>> selectShopsGoodsId = sktShopsService.selectShopsZyGoodsId(shopId);
     	PageInfo<Map<String, Object>> info = new PageInfo<>(selectShopsGoodsId);
     	selectShopsGoodsId = info.getList();
     	return Result.OK(selectShopsGoodsId);
     }
      
      /*
       * 首页轮播图
       * */ 
      @RequestMapping(value="/findWebLunbo")
      @ResponseBody
      public Result findWebLunbo(){
      	List<Map<String, Object>> findWebLunbo = sktShopsService.findWebLunbo();
      	return Result.OK(findWebLunbo);
      }

    /**
     * 楼层广告图
     * @param positionCode
     * @return
     */
      @RequestMapping(value = "/floorAd")
      @ResponseBody
      public Result  floorAd(String positionCode){
          Map<String,Object> map = new HashMap<String, Object>();
          map.put("positionCode",positionCode);
          try {
              Map<String,Object> map1 = new HashMap<String, Object>();
              map1 = sktShopsService.floorAd(map);
              return Result.OK(map1);
          }catch (Exception  e){
              return Result.EEROR("调用失败");
          }

      }

    /**
     * 楼层名称
     * @return
     */
      @RequestMapping(value = "/floorContent")
      @ResponseBody
      public Result floorContent(){

          GoodsCats goodsCats = new GoodsCats();
          goodsCats.setParentId(0);
          goodsCats.setIsShow(1);
          goodsCats.setDataFlag(1);
          EntityWrapper<GoodsCats> entityWrapper = new EntityWrapper<GoodsCats>(goodsCats);
          entityWrapper.orderAsc(Collections.singleton("catSort"));
          PageHelper.startPage(1, 10);
          List<GoodsCats> list = goodsCatsService.selectList(entityWrapper);
          PageInfo<GoodsCats> pageInfo = new PageInfo<>(list);
          return Result.OK(list);
      }

      /*
       * 单个商品详情
       * */
      @RequestMapping("/selectGoods")
      @ResponseBody
      public Result selectGoods(Integer goodsId){
    	  List<Map<String, Object>> list = sktShopsService.selectGoods(goodsId);
          if (list!=null&& list.size()>0){
              Map<String,Object> goods=list.get(0);
              if ("0".equals(goods.get("isSale").toString())){
                  return 	Result.EEROR("该商品已下架");
              }
              if (!"1".equals(goods.get("goodsStatus").toString())){
                  return 	Result.EEROR("该商品未通过审核");
              }

              if ("-1".equals(goods.get("dataFlag").toString())){
                  return 	Result.EEROR("该商品已失效");
              }
              Integer shopId=Integer.parseInt(goods.get("shopId").toString());
              SktShops sktShops = sktShopsService.selectById(shopId);
              if (sktShops.getShopStatus()==0){
                  return 	Result.EEROR("该商品所属商家已关闭");
              }
              if (sktShops.getDataFlag()==-1){
                  return 	Result.EEROR("该商家已失效");
              }

          }
    	  return Result.OK(list);
      }
      /*
       * 线下商家个人信息
       * */
      @RequestMapping("/findByUserId")
      @ResponseBody
      public Result findByUserId(Integer userId){
    	  List<Map<String,Object>> list = sktShopsService.findByUserId(userId);
    	  return Result.OK(list);
      }
      /*
       * 线下商家个人信息余额显示
       * */
      @RequestMapping("/findByUserIdMn")
      @ResponseBody
      public Result findByUserIdMn(Integer userId){
    	  List<Map<String,Object>> list = sktShopsService.findByUserIdMn(userId);
    	  return Result.OK(list);
      }
      /*
       * 线下商家个人信息支入支出
       * */
      @RequestMapping("/findByUserIdZrZc")
      @ResponseBody
      public Result findByUserIdZrZc(Integer userId){
    	  List<Map<String,Object>> list = sktShopsService.findByUserIdZrZc(userId);
    	  return Result.OK(list);
      }
      /*
       * 线下商家店铺信息
       * */
      @RequestMapping("/findByShopsXinxi")
      @ResponseBody
      public Result findByShopsXinxi(Integer userId){
    	  List<Map<String,Object>> list = sktShopsService.findByShopsXinxi(userId);
    	  return Result.OK(list);
      }
      /*
       * 线上商家店铺展示
       * */
      @RequestMapping("/findByXSShopsXinxi")
      @ResponseBody
      public Result findByXSShopsXinxi(Integer shopId){
    	  List<Map<String,Object>> list = sktShopsService.findByXSShopsXinxi(shopId);
    	  return Result.OK(list);
      }
      /*
       * 回显商家店铺展示
       * */
      @RequestMapping("/findByXSShopsImg")
      @ResponseBody
      public Result findByXSShopsImg(Integer shopId){
    	  List<Map<String,Object>> list = sktShopsService.findByXSShopsImg(shopId);
    	  return Result.OK(list);
      }
      /**
       * 修改线上商家信息
       */
      @RequestMapping(value = "/updateByXSShopsImg")
      @ResponseBody
      public Result updateByXSShopsImg(SktShops sktShops) {
      	Integer shops = sktShopsService.updateByXSShopsImg(sktShops);
          if(ToolUtil.isEmpty(shops)){
            	return Result.EEROR("店铺设置");
          }
          return Result.OK(null);
      }	
      /*
       * 查看运费
       * */
      @RequestMapping("/findByYunFei")
      @ResponseBody
      public Result findByYunFei(Integer shopId){
    	  List<Map<String,Object>> list = sktShopsService.findByYunFei(shopId);
    	  return Result.OK(list);
      }
      /**
       * 修改运费
       */
      @RequestMapping(value = "/updateByYunFei")
      @ResponseBody
      public Result updateByYunFei(@RequestParam(value="sktShops2", required = false ) String sktShops2) {
    	  
    	List<Map<String, Object>> listMap = JSON.parseObject(sktShops2, new TypeReference<List<Map<String,Object>>>(){});

          boolean b = sktShopsService.updateByYunFei(listMap);
          if(b==true){
            	return Result.OK(b);
          }else {
              return Result.EEROR("添加失败！");
          }

      }	
      /*
       * 查看代付款订单
       * */
      @RequestMapping("/findDaiOrders")
      @ResponseBody
      public Result findDaiOrders(@RequestParam(value="shopId")Integer shopId,
    		  @RequestParam(value="pageNum",defaultValue = "1",required = false)Integer pageNum,
    		  @RequestParam(value="orderNo",required = false)String orderNo,
    		  @RequestParam(value="beginTime",required = false)String beginTime,
    		  @RequestParam(value="endTime",required = false)String endTime){
//    	  PageHelper.startPage(pageNum, 12);
    	  List<Map<String,Object>> list = sktShopsService.findDaiOrders(shopId,orderNo,beginTime,endTime);
    	  PageInfo info = new PageInfo<>(list);
  		  return Result.OK(list,info.getPages());
      }
      /*
       * 查看以收货订单S
       * */
      @RequestMapping("/findYiOrders")
      @ResponseBody
      public Result findYiOrders(@RequestParam(value="shopId")Integer shopId,
    		  @RequestParam(value="pageNum",defaultValue = "1",required = false)Integer pageNum,
    		  @RequestParam(value="orderNo",required = false)String orderNo,
    		  @RequestParam(value="beginTime",required = false)String beginTime,
    		  @RequestParam(value="endTime",required = false)String endTime){
    	//  PageHelper.startPage(pageNum, 12);
    	  List<Map<String,Object>> list = sktShopsService.findYiOrders(shopId,orderNo,beginTime,endTime);
    	  PageInfo info = new PageInfo<>(list);
  		  return Result.OK(list,info.getPages());
      }
      /*
       * 待发货订单
       * */
      @RequestMapping("/findDaiFaHuoOrders")
      @ResponseBody
      public Result findDaiFaHuoOrders(@RequestParam(value="shopId")Integer shopId,
    		  @RequestParam(value="pageNum",defaultValue = "1",required = false)Integer pageNum,
    		  @RequestParam(value="orderNo",required = false)String orderNo,
    		  @RequestParam(value="beginTime",required = false)String beginTime,
    		  @RequestParam(value="endTime",required = false)String endTime){
//    	  PageHelper.startPage(pageNum, 12);
    	  List<Map<String,Object>> list = sktShopsService.findDaiFaHuoOrders(shopId,orderNo,beginTime,endTime);
    	  PageInfo info = new PageInfo<>(list);
  		  return Result.OK(list,info.getPages());
      }
    /*
     * 平台审核中商品
     * */
    @RequestMapping("/findCheckOrders")
    @ResponseBody
    public Result findCheckOrders(@RequestParam(value="shopId")Integer shopId,
                                  @RequestParam(value="pageNum",defaultValue = "1",required = false)Integer pageNum,
                                  @RequestParam(value="orderNo",required = false)String orderNo,
                                  @RequestParam(value="beginTime",required = false)String beginTime,
                                  @RequestParam(value="endTime",required = false)String endTime){
//    	  PageHelper.startPage(pageNum, 12);
        List<Map<String,Object>> list = sktShopsService.findCheckOrders(shopId,orderNo,beginTime,endTime);
        PageInfo info = new PageInfo<>(list);
        return Result.OK(list,info.getPages());
    }
    /*
     * 平台拒绝订单
     * */
    @RequestMapping("/findRefuseOrders")
    @ResponseBody
    public Result findRefuseOrders(@RequestParam(value="shopId")Integer shopId,
                                     @RequestParam(value="pageNum",defaultValue = "1",required = false)Integer pageNum,
                                     @RequestParam(value="orderNo",required = false)String orderNo,
                                     @RequestParam(value="beginTime",required = false)String beginTime,
                                     @RequestParam(value="endTime",required = false)String endTime){
//    	  PageHelper.startPage(pageNum, 12);
        List<Map<String,Object>> list = sktShopsService.findRefuseOrders(shopId,orderNo,beginTime,endTime);
        PageInfo info = new PageInfo<>(list);
        return Result.OK(list,info.getPages());
    }
      /*
       * 退款订单
       * */
      @RequestMapping("/findTuiOrders")
      @ResponseBody
      public Result findTuiOrders(@RequestParam(value="shopId")Integer shopId,
    		  @RequestParam(value="pageNum",defaultValue = "1",required = false)Integer pageNum,
    		  @RequestParam(value="orderNo",required = false)String orderNo,
    		  @RequestParam(value="beginTime",required = false)String beginTime,
    		  @RequestParam(value="endTime",required = false)String endTime,
    		  @RequestParam(value="refundStatus",required = false)Integer refundStatus
    		  ){
//    	  PageHelper.startPage(pageNum, 12);
    	  List<Map<String,Object>> list = sktShopsService.findTuiOrders(shopId,orderNo,beginTime,endTime,refundStatus);
    	  PageInfo info = new PageInfo<>(list);
  		  return Result.OK(list,info.getPages());
      }
      /*
       * 待收货订单
       * */
      @RequestMapping("/findDaiShouOrders")
      @ResponseBody
      public Result findDaiShouOrders(@RequestParam(value="shopId")Integer shopId,
    		  @RequestParam(value="pageNum",defaultValue = "1",required = false)Integer pageNum,
    		  @RequestParam(value="orderNo",required = false)String orderNo,
    		  @RequestParam(value="beginTime",required = false)String beginTime,
    		  @RequestParam(value="endTime",required = false)String endTime){
//    	  PageHelper.startPage(pageNum, 12);
    	  List<Map<String,Object>> list = sktShopsService.findDaiShouOrders(shopId,orderNo,beginTime,endTime);
    	  PageInfo info = new PageInfo<>(list);
  		  return Result.OK(list,info.getPages());
      }
      /*
       * 取消订单
       * */
      @RequestMapping("/findQuxiaoOrders")
      @ResponseBody
      public Result findQuxiaoOrders(@RequestParam(value="shopId")Integer shopId,
    		  @RequestParam(value="pageNum",defaultValue = "1",required = false)Integer pageNum,
    		  @RequestParam(value="orderNo",required = false)String orderNo,
    		  @RequestParam(value="beginTime",required = false)String beginTime,
    		  @RequestParam(value="endTime",required = false)String endTime){
    	  List<Map<String,Object>> list = sktShopsService.findQuxiaoOrders(shopId,orderNo,beginTime,endTime);
    	  PageInfo info = new PageInfo<>(list);
  		  return Result.OK(list,info.getPages());
      }
      /**
       * 加载订单显示内容
       */
      @RequestMapping(value = "/sktOrders_load")
      @ResponseBody
      public Result sktOrdersLoad(Integer sktOrdersId) {
      	 SktOrders orders = sktOrdersService.selectById(sktOrdersId);
      	 return Result.OK(orders);
      }
      /**
       * 加载商品显示内容
       */
      @RequestMapping(value = "/sktOrdersGoods_load")
      @ResponseBody
      public Result sktOrdersGoodsLoad(Integer sktOrdersId) {
      	 Object goods = sktOrdersService.selectOrderGoods(sktOrdersId);
      	 return Result.OK(goods);
      }
      /**
       * 加载商品参数显示内容
       */
      @RequestMapping(value = "/sktOrdersGoodsParameter_load")
      @ResponseBody
      public Result sktOrdersGoodsParameterLoad(Integer goodsId) {
      	Map<String,Object> param = new HashMap<String,Object>();
      	param.put("goodsId", goodsId);
      	Object parameter = sktOrdersService.selectOrderGoodsParameter(param);
      	return Result.OK(parameter);
      }
      /*
       * 根据分类查询品牌商品 
       * */
      @RequestMapping("/findHPcats")
      @ResponseBody
      public Result findHPcats(SktShops2 sktShops2,Integer pageSize1,Integer pageNum1){
    	  pageSize = pageSize1!=null?pageSize1:pageSize;
     	 pageNum = (pageNum1!=null&&pageNum!=0)?pageNum1:pageNum;
     	PageHelper.startPage(pageNum,pageSize);
    	  List<Map<String,Object>> list = sktShopsService.findHPcats(sktShops2);
    	  PageInfo<Map<String, Object>> info = new PageInfo<>(list);
    	  list = info.getList();
    	  return Result.OK(list);
      }
      /*
       * 回显线下店铺信息
       * */
      @RequestMapping("/findShopsId")
      @ResponseBody
      public Result findShopsId(SktShops3 sktshops3){
    	  List<Map<String,Object>> list = sktShopsService.findShopsId(sktshops3);
    	  return Result.OK(list);
      }
      /*
       * 回显线下店铺信息
       * */
      @RequestMapping("/selectShopsName")
      @ResponseBody
      public Result selectShopsName(String shopName){
    	  List<Map<String,Object>> list = sktShopsService.selectShopsName(shopName);
    	  return Result.OK(list);
      }
      /*
       * 
       * */
      @RequestMapping("/selectArticle")
      @ResponseBody
      public Result selectArticle(Integer articleId){
    	  List<Map<String,Object>> list = sktShopsService.selectArticle(articleId);
    	  return Result.OK(list);
      }


    /*
     *根据id查商家
     * */
    @RequestMapping("/selectShopById")
    @ResponseBody
    public Result selectShopById(String shopId){
        if (!"".equals(shopId)&& shopId!=null){
            SktShops sktShops = sktShopsService.selectById(shopId);
            Integer shopStatus = sktShops.getShopStatus();
            Integer dataFlag = sktShops.getDataFlag();
            if (shopStatus==0){
                return 	Result.EEROR("该商家已关闭");
            }
            if (dataFlag==-1){
                return 	Result.EEROR("该商家已失效");
            }
            return 	Result.OK(sktShops);
        }
        return Result.EEROR("");
    }

    /**
     * 获取收款二维码
     * @return
     */
    @RequestMapping("/getQR")
    @ResponseBody
    public Result getQR(){
//        Map<String,Object> map=new HashMap<>();
        Map<String, Object> map = sktSysConfigsService.getQR();
        if (map!=null && map.size()>0){
            String alipayQR="";
            String weChatQR ="";
            String alipayQRId ="";
            String weChatQRId = "";
            if(!"".equals(map.get("alipayQR").toString())&& map.get("alipayQR").toString()!=null){
                alipayQR = map.get("alipayQR").toString();
            }
            if(!"".equals( map.get("weChatQR").toString())&& map.get("weChatQR").toString()!=null){
                weChatQR = map.get("weChatQR").toString();
            }

            if(!"".equals(map.get("alipayQRId").toString())&& map.get("alipayQRId").toString()!=null){
                alipayQRId = map.get("alipayQRId").toString();
            }
            if(!"".equals( map.get("weChatQRId").toString())&& map.get("weChatQRId").toString()!=null){
                weChatQRId = map.get("weChatQRId").toString();
            }
            map.put("weChatQR",weChatQR);
            map.put("alipayQR",alipayQR);
        }

        return Result.OK(map);
    }
    @RequestMapping("/getShoppinglist")
    @ResponseBody
    public Result getShoppinglist(Integer userId){
//        SktShops sktShops = sktShopsService.selectById(shopId);
//        Integer userId = sktShops.getUserId();
        Map<String,Object> map=new HashMap<>();
        map.put("type",3);
        map.put("dataId",userId);
        List<Map<String, Object>> list = shoppingService.selectShoppingList(map);
        return Result.OK(list);
    }
}
