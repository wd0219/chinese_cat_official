package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.AppUserAddressDTO;
import com.stylefeng.guns.modular.huamao.model.SktAreas;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppSktAreaService;
import com.stylefeng.guns.modular.huamao.service.IAppUserService;
import com.stylefeng.guns.modular.huamao.service.IAppUsersAddressService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
@CrossOrigin
@SuppressWarnings("all")
@RestController
@RequestMapping("/app")
public class AppAddressController extends BaseController {

    @Autowired
    private IAppUserService appUserService;
    @Autowired
    private IAppUsersAddressService iAppUsersAddressService;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private IAppSktAreaService iAppSktAreaService;

    /**
     * 收货地址新增
     * @return
     */
    @RequestMapping("/Address/add")
    @ResponseBody
    public AppResult AddressAdd(@ModelAttribute("areaIdPath_one") String areaIdPath_one,@ModelAttribute("areaIdPath_two") String areaIdPath_two,@ModelAttribute("areaIdPath_thress") String areaIdPath_thress,
                             @ModelAttribute("isDefault") String isDefault,@ModelAttribute("userAddress") String userAddress,@ModelAttribute("userName") String userName,@ModelAttribute("userPhone") String userPhone,@ModelAttribute("postalcode")String postalcode){
        try {
            if (!MSGUtil.getResponsePhone(userPhone)){
                return AppResult.ERROR("请输入正确的手机号格式",-1);
            }
            Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user==null){
               return AppResult.ERROR("您未登录",-2);
            }
//            if (user == null) {
//                user.setUserId(0);
//            }
            UsersAddress usersAddress = new UsersAddress();
            usersAddress.setIsDefault(Integer.parseInt(isDefault));
            UsersAddress usersAddressData = iAppUsersAddressService.selectUserAddressByUserId(user.getUserId());
            if (usersAddressData == null) {
                usersAddress.setIsDefault(1);
            }
            if (isDefault == "1" || isDefault.equals("1")) {
                iAppUsersAddressService.updateUserAddressUsersIsDefault(user.getUserId());
            }
            usersAddress.setUserId(user.getUserId());
            usersAddress.setProvinceId(Integer.parseInt(areaIdPath_one));
            usersAddress.setCityId(Integer.parseInt(areaIdPath_two));
            usersAddress.setAreaId(Integer.parseInt(areaIdPath_thress));
            usersAddress.setUserAddress(userAddress);
            usersAddress.setPostalcode(postalcode);
            usersAddress.setUserName(userName);
            usersAddress.setUserPhone(userPhone);
            usersAddress.setCreateTime(new Date());
            boolean insertUserAddress = iAppUsersAddressService.insert(usersAddress);
            if (insertUserAddress) {
                return AppResult.OK("添加成功", 1);
            }
            return AppResult.ERROR("添加失败", -1);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("未登录",-2);
        }
    }

    /**
     * 修改收货地址
     * @param areaIdPath_one
     * @param areaIdPath_two
     * @param areaIdPath_thress
     * @param isDefault
     * @param userAddress
     * @param userName
     * @param userPhone
     * @return
     */
    @RequestMapping("/Address/toEdit")
    @ResponseBody
    public AppResult AddressToEdit(@ModelAttribute("addressId")String addressId,@ModelAttribute("postalcode")String postalcode,@ModelAttribute("areaIdPath_one")String areaIdPath_one,@ModelAttribute("areaIdPath_two")String areaIdPath_two,
                                   @ModelAttribute("areaIdPath_thress")String areaIdPath_thress,@ModelAttribute("isDefault")String isDefault,@ModelAttribute("userAddress")String userAddress,
                                   @ModelAttribute("userName")String userName,@ModelAttribute("userPhone")String userPhone){
        try {
            if (!MSGUtil.getResponsePhone(userPhone)){
                return AppResult.ERROR("请输入正确的手机号格式",-1);
            }
            Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
            if (user == null) {
                user.setUserId(0);
            }
            iAppUsersAddressService.updateUserAddressUsersIsDefault(user.getUserId());
            UsersAddress usersAddress = new UsersAddress();
            usersAddress.setAddressId(Integer.parseInt(addressId));
            usersAddress.setProvinceId(Integer.parseInt(areaIdPath_one));
            usersAddress.setCityId(Integer.parseInt(areaIdPath_two));
            usersAddress.setAreaId(Integer.parseInt(areaIdPath_thress));
            usersAddress.setUserPhone(userPhone);
            usersAddress.setUserName(userName);
            usersAddress.setUserAddress(userAddress);
            usersAddress.setPostalcode(postalcode);
            usersAddress.setIsDefault(Integer.parseInt(isDefault));
            boolean usersAddressMsg = iAppUsersAddressService.updateById(usersAddress);
            if (usersAddressMsg) {
                return AppResult.OK("修改成功");
            }
            return AppResult.ERROR("修改失败", -1);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("还未登陆",-2);
        }
    }

    /**
     * 收货地址
     * @param pageIndex
     * @return
     */
    @RequestMapping("/Address/listAddress")
    @ResponseBody
    public AppResult AddressListAddress(@ModelAttribute("pageIndex") String pageIndex){
        try {
            Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
            if (user == null) {
                return AppResult.ERROR("您未登录", -2);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageIndex", (Integer.parseInt(pageIndex) - 1) * 15);
            map.put("pageNum", 15);
            map.put("userId", user.getUserId());
            List<AppUserAddressDTO> appUserAddress = iAppUsersAddressService.selectUserAddressByPageUserId(map);
            if (appUserAddress == null || appUserAddress.size() <= 0) {
                return AppResult.OK("获取成功", new ArrayList<>());
            }
            List<Object> list = new ArrayList<>();
            for (int i = 0; i < appUserAddress.size(); i++) {
                SktAreas sktAreas = iAppSktAreaService.selectSktAreasById(appUserAddress.get(i).getAreaId());
                SktAreas sktAreas1 = iAppSktAreaService.selectSktAreasById(appUserAddress.get(i).getCityId());
                SktAreas sktAreas2 = iAppSktAreaService.selectSktAreasById(appUserAddress.get(i).getProvinceId());
                Map<Object, Object> maps = new HashMap<>();
                maps.put("addressId", appUserAddress.get(i).getAddressId());
                maps.put("userId", appUserAddress.get(i).getUserId());
                maps.put("userName", appUserAddress.get(i).getUserName());
                maps.put("userPhone", appUserAddress.get(i).getUserPhone());
                maps.put("provinceId", appUserAddress.get(i).getProvinceId());
                maps.put("cityId", appUserAddress.get(i).getCityId());
                maps.put("areaId", appUserAddress.get(i).getAreaId());
                maps.put("userAddress", appUserAddress.get(i).getUserAddress());
                maps.put("postalcode", appUserAddress.get(i).getPostalcode());
                maps.put("isDefault", appUserAddress.get(i).getIsDefault());
                maps.put("dataFlag", appUserAddress.get(i).getDataFlag());
                if (MSGUtil.isBlank(sktAreas) && MSGUtil.isBlank(sktAreas) && MSGUtil.isBlank(sktAreas)) {
                    maps.put("name", "");
                }else {
                    maps.put("name",sktAreas2.getAreaName() + sktAreas1.getAreaName() + sktAreas.getAreaName());
                }
                list.add(maps);
            }
            return AppResult.OK("获取成功", list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("出错了",-1);
        }
    }

    /**
     * 选择省市区
     * @param areaId
     * @return
     */
    @RequestMapping("/Address/area")
    @ResponseBody
    public AppResult AddressArea(@ModelAttribute("areaId") String areaId){
        return iAppUsersAddressService.AddressArea(areaId);
    }

    /**
     * 收货地址删除
     * @param addressId
     * @return
     */
    @RequestMapping("/Address/del")
    public AppResult AddressDel(@ModelAttribute("addressId") String addressId){
        return iAppUsersAddressService.AddressDel(addressId);
    }



}
