package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogCashList;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ILogCashService;

/**
 * 现金记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-16 14:32:01
 */
@CrossOrigin()
@Controller
@RequestMapping("/logCash")
//@CrossOrigin
public class LogCashController extends BaseController {

	private String PREFIX = "/huamao/logCash/";

	@Autowired
	private ILogCashService logCashService;

	/**
	 * 跳转到现金记录首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, Model model) {
		// 参数是为了在用户点击现金查看积分详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logCash.html";
	}

	/**
	 * 跳转到添加现金记录
	 */
	@RequestMapping("/logCash_add")
	public String logCashAdd() {
		return PREFIX + "logCash_add.html";
	}

	/**
	 * 跳转到修改现金记录
	 */
	@RequestMapping("/logCash_update/{logCashId}")
	public String logCashUpdate(@PathVariable Integer logCashId, Model model) {
		LogCash logCash = logCashService.selectById(logCashId);
		model.addAttribute("item", logCash);
		LogObjectHolder.me().set(logCash);
		return PREFIX + "logCash_edit.html";
	}

	/**
	 * 获取现金记录列表：{userPhone2}用户查看现金详情传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone, LogCashList logCashList) {
		// 判断如果不是‘手机号’并且logCashList.getUserPhone()不是null说明要看用户积分详情，是‘手机号’并且logCashList.getUserPhone()是''或是有数据说明是普通的查询所有的现金详情
		if (!phone.equals("手机号") && logCashList.getUserPhone() == null) {
			logCashList.setUserPhone(phone);
		}
		return logCashService.selectLogCashAll(logCashList);
		// return logCashService.selectList(null);
	}

	/**
	 * 新增现金记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogCash logCash) {
		logCashService.insert(logCash);
		return SUCCESS_TIP;
	}

	/**
	 * 删除现金记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logCashId) {
		logCashService.deleteById(logCashId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改现金记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogCash logCash) {
		logCashService.updateById(logCash);
		return SUCCESS_TIP;
	}

	/**
	 * 现金记录详情
	 */
	@RequestMapping(value = "/detail/{logCashId}")
	@ResponseBody
	public Object detail(@PathVariable("logCashId") Integer logCashId) {
		return logCashService.selectById(logCashId);
	}


	/******************首页*******************/
	/**
	 * 现金记录详情 type   orderNo  beginTime endTime  pageNum
	 */
	@RequestMapping(value = "/selectUserCase")
	@ResponseBody
	public Result selectUserCase(LogCashList logCash,Integer pageNum){
		PageHelper.startPage(pageNum, 12);
		List<Map<String,Object>> list = logCashService.selectLogCash(logCash);
		PageInfo pag = new PageInfo(list);
		return Result.OK(list,pag.getPages());
	}


}
