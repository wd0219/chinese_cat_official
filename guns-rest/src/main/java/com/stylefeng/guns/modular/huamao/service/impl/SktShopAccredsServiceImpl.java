package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopAccredsMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopAccreds;
import com.stylefeng.guns.modular.huamao.model.SktShopAccredsDTO;
import com.stylefeng.guns.modular.huamao.service.ISktShopAccredsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 店铺认证信息表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-13
 */
@Service
public class SktShopAccredsServiceImpl extends ServiceImpl<SktShopAccredsMapper, SktShopAccreds> implements ISktShopAccredsService {
	@Autowired
	private SktShopAccredsMapper sktShopAccredsMapper;
	/*
	 * 展示
	 * */
	@Override
	public List<SktShopAccredsDTO> sktShopAccredsfindall(SktShopAccredsDTO sktShopAccredsDTO) {
		List<SktShopAccredsDTO> list = sktShopAccredsMapper.sktShopAccredsfindall(sktShopAccredsDTO);
		return list;
	}
	/*
	 * 删除
	 * */
	@Override
	public void sktShopAccredsdelete(Integer shopId) {
		sktShopAccredsMapper.sktShopAccredsdelete(shopId);
	}
	/*
	 * 修改回显
	 * */
	@Override
	public List<SktShopAccredsDTO> sktShopAccredsfindup(SktShopAccredsDTO shopAccredsDTO) {
		List<SktShopAccredsDTO> list = sktShopAccredsMapper.sktShopAccredsfindup(shopAccredsDTO);
		return list;
	}

}
