package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktAreas;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 区域表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface SktAreasMapper extends BaseMapper<SktAreas> {
	List<Map<String,Object>> selectByParentId(Integer parentId);
	boolean deleteByParentId(Integer parentId);
	List<Map<String,Object>> selectByName(SktAreas sktAreas);
	Integer selectMaxAreaSort();

    List<SktAreas> selectAreasByParentId(@Param("parentId") Integer areaId);

	SktAreas selectSktAreasById(Integer areasId);

    SktAreas selectAreaByProvinceId(int alevel);

	SktAreas selectAreaByCityId(int alevel);

	SktAreas selectAreaByAreaId(int alevel);
}
