package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreFreezeMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreeze;
import com.stylefeng.guns.modular.huamao.service.IAppLogScoreFreezeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppLogScoreFreezeServiceImpl extends ServiceImpl<LogScoreFreezeMapper,LogScoreFreeze> implements IAppLogScoreFreezeService {

    @Autowired
    private LogScoreFreezeMapper logScoreFreezeMapper;

    @Override
    public List<LogScoreFreeze> selectLogScoreFreezeByUserId(Map<String, Object> map) {
        return logScoreFreezeMapper.selectLogScoreFreezeByUserId(map);
    }
}
