package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktShopApproveService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2018-05-10 16:36:36
 */
@CrossOrigin
@Controller
@RequestMapping("/sktShopApprove")
public class SktShopApproveController extends BaseController {

    private String PREFIX = "/huamao/sktShopApprove/";

    @Autowired
    private ISktShopApproveService sktShopApproveService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopApprove.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/sktShopApprove_add")
    public String sktShopApproveAdd() {
        return PREFIX + "sktShopApprove_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/sktShopApprove_update/{sktShopApproveId}")
    public String sktShopApproveUpdate(@PathVariable Integer sktShopApproveId, Model model) {
        SktShopApprove sktShopApprove = sktShopApproveService.selectById(sktShopApproveId);
        model.addAttribute("item",sktShopApprove);
        LogObjectHolder.me().set(sktShopApprove);
        return PREFIX + "sktShopApprove_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktShopApproveService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopApprove sktShopApprove) {

        sktShopApproveService.insert(sktShopApprove);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopApproveId) {
        sktShopApproveService.deleteById(sktShopApproveId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopApprove sktShopApprove) {
        sktShopApproveService.updateById(sktShopApprove);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{sktShopApproveId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopApproveId") Integer sktShopApproveId) {
        return sktShopApproveService.selectById(sktShopApproveId);
    }
    
    
    /*************首页***************/
    /**
     * 新增联盟商家企业
     */
    @RequestMapping(value = "/addShopApprove")
    @ResponseBody
    public Result addShopApprove(SktShopApprove sktShopApprove) {
        sktShopApprove.setCreateTime(new Date());
        String insert = sktShopApproveService.insertApprove(sktShopApprove);
        if ("1".equals(insert)) {
            return Result.OK("新增企业成功！");
        } else if ("2".equals(insert)) {
            return Result.OK("商家已经申请!");
        }else if ("3".equals(insert)) {
            return Result.OK("已经是联盟商家!");
        }else if ("4".equals(insert)) {
            return Result.OK("已经申请待后台审核!");
        }else if("5".equals(insert)){
            return Result.EEROR("线上商家申请中，请先通过申请");
        }
        return Result.EEROR("新增企业失败！");
    }

}
