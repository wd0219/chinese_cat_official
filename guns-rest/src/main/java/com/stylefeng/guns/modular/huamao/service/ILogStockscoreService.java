package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnoverList;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;
import com.stylefeng.guns.modular.huamao.model.LogStockscoreList;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 库存积分流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface ILogStockscoreService extends IService<LogStockscore> {

	// 库存积分流水表展示
	public List<LogStockscoreList> selectLogStockScoreAll(LogStockscoreList logStockscoreList);
	/**
	 * 查询库存积分
	 * @param userId
	 * @return
	 */
	public List<Map<String, Object>> selectStockscoref(LogStockscoreList logStockscoreList);

}
