package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktAdPositions;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 广告位置表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface ISktAdPositionsService extends IService<SktAdPositions> {
	
	public List<SktAdPositions> sktAdPositionsfinAll(SktAdPositions sktAdPositions);
	public List<SktAdPositions> sktAdPositionsfindLeft(Map<String,Object> map);
	public List<SktAdPositions>	sktAdPositionsfindBottom (Map<String,Object> map);
}
