package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;

public class UnderLinePay extends Model<UnderLinePay> {

    private static final long serialVersionUID = 1L;

    /**
     * 收款文字描述
     */
    private String rechargeRemark;

    /**
     * 收款银行
     */
    private String rechargeBankName;

    /**
     * 收款支行
     */
    private String rechargeBranchBank;

    /**
     * 收款人
     */
    private String rechargeReceiver;

    /**
     * 收款账号
     */
    private String rechargeNo;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRechargeRemark() {
        return rechargeRemark;
    }

    public void setRechargeRemark(String rechargeRemark) {
        this.rechargeRemark = rechargeRemark;
    }

    public String getRechargeBankName() {
        return rechargeBankName;
    }

    public void setRechargeBankName(String rechargeBankName) {
        this.rechargeBankName = rechargeBankName;
    }

    public String getRechargeBranchBank() {
        return rechargeBranchBank;
    }

    public void setRechargeBranchBank(String rechargeBranchBank) {
        this.rechargeBranchBank = rechargeBranchBank;
    }

    public String getRechargeReceiver() {
        return rechargeReceiver;
    }

    public void setRechargeReceiver(String rechargeReceiver) {
        this.rechargeReceiver = rechargeReceiver;
    }

    public String getRechargeNo() {
        return rechargeNo;
    }

    public void setRechargeNo(String rechargeNo) {
        this.rechargeNo = rechargeNo;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "UnderLinePay{" +
                "rechargeRemark='" + rechargeRemark + '\'' +
                ", rechargeBankName='" + rechargeBankName + '\'' +
                ", rechargeBranchBank='" + rechargeBranchBank + '\'' +
                ", rechargeReceiver='" + rechargeReceiver + '\'' +
                ", rechargeNo='" + rechargeNo + '\'' +
                '}';
    }
}
