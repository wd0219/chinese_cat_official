package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Maps;
import com.stylefeng.guns.core.util.BeanUtil;
import com.stylefeng.guns.modular.huamao.model.Account;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogCashList;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.ILogCashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/usersAccount")
@CrossOrigin
public class UsersAccountController {
    @Autowired
    private ILogCashService logCashService;
    @Autowired
    IAccountService IAccountService;


    @RequestMapping("/getUsersAccountByUserId")
    public Result getUsersAccountByUserId(Integer userId, String type) {
        Account account = IAccountService.selectAccountByuserId(userId);
        // 奖励
        String spSorce = IAccountService.selectSpSorceByUserId(userId, type);
        Map<String, Object> stringObjectMap = BeanUtil.beanToMap(account);
        stringObjectMap.put("spSorce", spSorce);
        return Result.OK(stringObjectMap);
    }

    @RequestMapping("/cashRecordByUserId")
    public Result cashRecordByUserId(Integer userId, LogCashList logCash) {
        Page<LogCashList> logCashListPage = new Page<>();
        EntityWrapper<LogCashList> logCashEntityWrapper = new EntityWrapper<>();
        logCashEntityWrapper.setEntity(logCash);
        logCashEntityWrapper.where("orderNo={0} and type ={1} ", logCash.getOrderNo(), logCash.getType()).between("createTime", logCash.getBeginTime(), logCash.getEndTime());
        return Result.OK(null);
    }
}

