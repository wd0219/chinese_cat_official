package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.TradeDictDTD;
import com.stylefeng.guns.modular.huamao.service.IAppLogKaiyuanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppLogKaiyuanServiceImpl extends ServiceImpl<LogKaiyuanMapper,LogKaiyuan> implements IAppLogKaiyuanService {
    @Autowired
    private LogKaiyuanMapper logKaiyuanMapper;

    @Override
    public List<LogKaiyuan> huaBaoList(Integer userId) {
        return logKaiyuanMapper.huaBaoList(userId);
    }

    @Override
    public TradeDictDTD selectTradeDict(Map<String, Object> map) {
        return logKaiyuanMapper.selectTradeDict(map);
    }
}
