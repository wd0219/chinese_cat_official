package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersBankcardsMapper;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.service.IAppUsersBankcardsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppUsersBankcardsServiceImpl extends ServiceImpl<UsersBankcardsMapper,UsersBankcards> implements IAppUsersBankcardsService {
    @Autowired
    private UsersBankcardsMapper usersBankcardsMapper;

    @Override
    public List<Map<String,Object>> selectUsersBankAndBankByUserId(Integer userId) {
        return usersBankcardsMapper.selectUsersBankAndBankByUserId(userId);
    }

    @Override
    public int selectCountByUserId(Integer userId) {
        return usersBankcardsMapper.selectCountByUserId(userId);
    }

    @Override
    public boolean deleteByIdAndUserId(Map<String, Object> map) {
        int i=usersBankcardsMapper.deleteByIdAndUserId(map);
        if (i==1){
            return true;
        }
        return false;
    }
}
