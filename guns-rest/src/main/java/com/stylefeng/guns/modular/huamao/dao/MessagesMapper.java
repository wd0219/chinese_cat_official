package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统消息表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface MessagesMapper extends BaseMapper<Messages> {

	// 通过传过来的ID进行逻辑删除
	public void deleteById2(Integer id);

	// 系统消息展示
	public List<MessagesList> selectMessagesAll(MessagesList messagesList);

	// 获取用户
	public List<MessagesList> getUsers(String userPhone);

	// 添加信息
	public void addMessages(MessagesList messagesList);

	List<Messages> message(Integer userId);
	/**
	 * 查询消息列表
	 * @param receiveUserId
	 * @return
	 */
	public List<MessagesList> getMessage(Messages messages);
	
	public List<MessagesList> getMessage1(@Param(value="receiveUserId")Integer receiveUserId);
	
	public Integer updateMessageStatus(@Param(value="msgStatus") Integer msgStatus,
									@Param(value="dataFlag") Integer dataFlag,
									@Param(value="ids") Integer[] ids);
	/**
	 * 用户未读消息数量
	 * @param messages
	 * @return
	 */
	public Integer getMessageCount(Messages messages);
	// 添加信息
	public Integer shopAddMessages(
			@Param("messages") Messages messages,
			@Param("userIds") List<Integer> userIds
	);
}
