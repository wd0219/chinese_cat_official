package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShops;

import java.util.List;
import java.util.Map;

public interface IAppShopsService extends IService<SktShops> {
    Map<String,Object> selectUsersShopsByUserShopId(String shopId);

    SktShops selectShopsByShopId(int i);

    List<Map<String,Object>> selectShopsAndAccredsByShopId(Map<String, Object> map1);

    int selectCountByShopId(int i);
}
