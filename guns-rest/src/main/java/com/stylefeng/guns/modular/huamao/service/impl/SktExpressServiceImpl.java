package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktExpressMapper;
import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.stylefeng.guns.modular.huamao.service.ISktExpressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 快递表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@Service
public class SktExpressServiceImpl extends ServiceImpl<SktExpressMapper, SktExpress> implements ISktExpressService {
    @Autowired
    private SktExpressMapper sktExpressMapper;
    @Override
    public List<Map<String,Object>> selectExpressAll() {
        List<Map<String,Object>> list = sktExpressMapper.selectExpressAll();
        return list;
    }

    @Override
    public List<SktExpress> appFindExpress() {
        SktExpress sktExpress = new SktExpress();
        sktExpress.setDataFlag(1);
        EntityWrapper<SktExpress> entityWrapper = new EntityWrapper<SktExpress>(sktExpress);
        List<SktExpress> sktExpresses = this.selectList(entityWrapper);
        return sktExpresses;
    }
}
