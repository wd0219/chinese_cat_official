package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Goods;

import java.util.List;
import java.util.Map;

public interface IAppGoodsService extends IService<Goods> {
    void updateGoodsSaleNum(Map<String, Object> map);

    void updateGoodsAppraiseNum(Map<String, Object> goodsShopScoresMap);

    List<Map<String,Object>> selectGoodsByShopId(Map<String, Object> maps);

    int selectCountByGoodsId(int i);
}
