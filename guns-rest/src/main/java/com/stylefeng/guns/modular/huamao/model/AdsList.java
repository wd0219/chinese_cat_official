package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 广告表扩展类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public class AdsList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	private Integer adId;
	/**
	 * 广告位置ID
	 */
	private Integer adPositionId;
	/**
	 * 广告位置
	 */
	private String positionName;
	/**
	 * 广告文件
	 */
	private String adFile;
	/**
	 * 广告名称
	 */
	private String adName;
	/**
	 * 广告网址
	 */
	private String adURL;
	/**
	 * 广告开始日期
	 */
	private String adStartDate;
	/**
	 * 广告结束日期
	 */
	private String adEndDate;
	/**
	 * 排序号
	 */
	private Integer adSort;
	/**
	 * 广告点击数
	 */
	private Integer adClickNum;
	/**
	 * 广告类型
	 */
	private Integer positionType;
	/**
	 * 删除标志
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;

	public AdsList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AdsList(Integer adId, Integer adPositionId, String positionName, String adFile, String adName, String adURL,
			String adStartDate, String adEndDate, Integer adSort, Integer adClickNum, Integer positionType,
			Integer dataFlag, Date createTime) {
		super();
		this.adId = adId;
		this.adPositionId = adPositionId;
		this.positionName = positionName;
		this.adFile = adFile;
		this.adName = adName;
		this.adURL = adURL;
		this.adStartDate = adStartDate;
		this.adEndDate = adEndDate;
		this.adSort = adSort;
		this.adClickNum = adClickNum;
		this.positionType = positionType;
		this.dataFlag = dataFlag;
		this.createTime = createTime;
	}

	public Integer getAdId() {
		return adId;
	}

	public void setAdId(Integer adId) {
		this.adId = adId;
	}

	public Integer getAdPositionId() {
		return adPositionId;
	}

	public void setAdPositionId(Integer adPositionId) {
		this.adPositionId = adPositionId;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getAdFile() {
		return adFile;
	}

	public void setAdFile(String adFile) {
		this.adFile = adFile;
	}

	public String getAdName() {
		return adName;
	}

	public void setAdName(String adName) {
		this.adName = adName;
	}

	public String getAdURL() {
		return adURL;
	}

	public void setAdURL(String adURL) {
		this.adURL = adURL;
	}

	public String getAdStartDate() {
		return adStartDate;
	}

	public void setAdStartDate(String adStartDate) {
		this.adStartDate = adStartDate;
	}

	public String getAdEndDate() {
		return adEndDate;
	}

	public void setAdEndDate(String adEndDate) {
		this.adEndDate = adEndDate;
	}

	public Integer getAdSort() {
		return adSort;
	}

	public void setAdSort(Integer adSort) {
		this.adSort = adSort;
	}

	public Integer getAdClickNum() {
		return adClickNum;
	}

	public void setAdClickNum(Integer adClickNum) {
		this.adClickNum = adClickNum;
	}

	public Integer getPositionType() {
		return positionType;
	}

	public void setPositionType(Integer positionType) {
		this.positionType = positionType;
	}

	public Integer getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AdsList [adId=" + adId + ", adPositionId=" + adPositionId + ", positionName=" + positionName
				+ ", adFile=" + adFile + ", adName=" + adName + ", adURL=" + adURL + ", adStartDate=" + adStartDate
				+ ", adEndDate=" + adEndDate + ", adSort=" + adSort + ", adClickNum=" + adClickNum + ", positionType="
				+ positionType + ", dataFlag=" + dataFlag + ", createTime=" + createTime + "]";
	}

}
