package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreeze;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreezeList;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnover;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnoverList;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanTurnoverService;

/**
 * 华宝货款记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 08:56:39
 */
@CrossOrigin
@Controller
@RequestMapping("/logKaiyuanTurnover")
public class LogKaiyuanTurnoverController extends BaseController {

	private String PREFIX = "/huamao/logKaiyuanTurnover/";

	@Autowired
	private ILogKaiyuanTurnoverService logKaiyuanTurnoverService;

	/**
	 * 跳转到华宝货款记录首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, Model model) {
		// 参数是为了在用户点击华宝货款查看华宝货款详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logKaiyuanTurnover.html";
	}

	/**
	 * 跳转到添加华宝货款记录
	 */
	@RequestMapping("/logKaiyuanTurnover_add")
	public String logKaiyuanTurnoverAdd() {
		return PREFIX + "logKaiyuanTurnover_add.html";
	}

	/**
	 * 跳转到修改华宝货款记录
	 */
	@RequestMapping("/logKaiyuanTurnover_update/{logKaiyuanTurnoverId}")
	public String logKaiyuanTurnoverUpdate(@PathVariable Integer logKaiyuanTurnoverId, Model model) {
		LogKaiyuanTurnover logKaiyuanTurnover = logKaiyuanTurnoverService.selectById(logKaiyuanTurnoverId);
		model.addAttribute("item", logKaiyuanTurnover);
		LogObjectHolder.me().set(logKaiyuanTurnover);
		return PREFIX + "logKaiyuanTurnover_edit.html";
	}

	/**
	 * 获取华宝货款记录列表：{userPhone2}商家华宝货款传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone,
			LogKaiyuanTurnoverList logKaiyuanTurnoverList) {
		// 判断如果不是‘手机号’并且logKaiyuanTurnoverList.getUserPhone()不是null说明要看商家待发华宝营业额详情，是‘手机号’并且logKaiyuanTurnoverList.getUserPhone()是''或是有数据说明是普通的查询所有的商家待发华宝营业额详情
		if (!phone.equals("手机号") && logKaiyuanTurnoverList.getUserPhone() == null) {
			logKaiyuanTurnoverList.setUserPhone(phone);
		}
		return logKaiyuanTurnoverService.selectLogKaiyuanTurnoverAll(logKaiyuanTurnoverList);
		// return logKaiyuanTurnoverService.selectList(null);
	}

	/**
	 * 新增华宝货款记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogKaiyuanTurnover logKaiyuanTurnover) {
		logKaiyuanTurnoverService.insert(logKaiyuanTurnover);
		return SUCCESS_TIP;
	}

	/**
	 * 删除华宝货款记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logKaiyuanTurnoverId) {
		logKaiyuanTurnoverService.deleteById(logKaiyuanTurnoverId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改华宝货款记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogKaiyuanTurnover logKaiyuanTurnover) {
		logKaiyuanTurnoverService.updateById(logKaiyuanTurnover);
		return SUCCESS_TIP;
	}

	/**
	 * 华宝货款记录详情
	 */
	@RequestMapping(value = "/detail/{logKaiyuanTurnoverId}")
	@ResponseBody
	public Object detail(@PathVariable("logKaiyuanTurnoverId") Integer logKaiyuanTurnoverId) {
		return logKaiyuanTurnoverService.selectById(logKaiyuanTurnoverId);
	}
	
	/*****************商城*******************/
	/**
	 * 商家华宝记录
	 */
	@RequestMapping(value = "/selectLogKaiyuanTurnover")
	@ResponseBody
	public Result selectLogKaiyuanTurnover(LogKaiyuanTurnoverList logKaiyuanTurnoverList,Integer pageNum) {
		PageHelper.startPage(pageNum,12);
		List<LogKaiyuanTurnover> list = logKaiyuanTurnoverService.selectTurnoverInfo(logKaiyuanTurnoverList);
		PageInfo info = new PageInfo<>(list);
		return Result.OK(list,info.getPages());
	}
	
	/**
	 * 商家华宝记录
	 */
	@RequestMapping(value = "/selectLogKaiyuanFreeze")
	@ResponseBody
	public Result selectLogKaiyuanFreeze (LogKaiyuanFreezeList logKaiyuanTurnoverList,Integer pageNum) {
		PageHelper.startPage(pageNum,12);
		List<LogKaiyuanFreeze> list = logKaiyuanTurnoverService.selectLogKaiyuanFreeze(logKaiyuanTurnoverList);
		PageInfo info = new PageInfo<>(list);
		return Result.OK(list,info.getPages());
	}
}
