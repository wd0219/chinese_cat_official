package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktFavoritesMapper;
import com.stylefeng.guns.modular.huamao.model.SktFavorites;
import com.stylefeng.guns.modular.huamao.service.IAppFavoritesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppFavoritesServiceImpl extends ServiceImpl<SktFavoritesMapper,SktFavorites> implements IAppFavoritesService {
    @Autowired
    private  SktFavoritesMapper sktFavoritesMapper;

    @Override
    public List<Map<String, Object>> selectFavoritesByUserId(Map<String, Object> map) {
        return sktFavoritesMapper.selectFavoritesByUserId(map);
    }

    @Override
    public List<Map<String, Object>> selectFavoritesAndShopsByUserId(Map<String, Object> map) {
        return sktFavoritesMapper.selectFavoritesAndShopsByUserId(map);
    }

    @Override
    public int selectCountByUserId(SktFavorites map) {
        return sktFavoritesMapper.selectCountByUserId(map);
    }

    @Override
    public boolean insertAndFindId(SktFavorites sktFavorites) {
        int sktFavorites1 = sktFavoritesMapper.insertAndFindId(sktFavorites);
        if (sktFavorites1==0){
            return false;
        }
        return true;
    }

    @Override
    public SktFavorites selectListByEntity(SktFavorites sktFavorites) {
        return sktFavoritesMapper.selectListByEntity(sktFavorites);
    }

    @Override
    public boolean deleteByUser(SktFavorites sktFavorites) {
        int i=sktFavoritesMapper.deleteByUser(sktFavorites);
        if (i>0){
            return true;
        }
        return false;
    }
}
