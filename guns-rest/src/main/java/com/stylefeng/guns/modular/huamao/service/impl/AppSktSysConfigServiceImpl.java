package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktSysConfigsMapper;
import com.stylefeng.guns.modular.huamao.model.SktSysConfigs;
import com.stylefeng.guns.modular.huamao.service.IAppSktSysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppSktSysConfigServiceImpl extends ServiceImpl<SktSysConfigsMapper,SktSysConfigs> implements IAppSktSysConfigService {
    @Autowired
    private SktSysConfigsMapper sktSysConfigsMapper;

    @Override
    public String findSysConfigs(String str) {
        return sktSysConfigsMapper.findFieldValue(str);
    }

    @Override
    public List<SktSysConfigs> selectAllConf() {
        return sktSysConfigsMapper.selectAllConf();
    }

    @Override
    public SktSysConfigs selectSysConfByFieldCode(String fieldCode ) {
        return sktSysConfigsMapper.selectSysConfigsByFieldCode(fieldCode);
    }
}
