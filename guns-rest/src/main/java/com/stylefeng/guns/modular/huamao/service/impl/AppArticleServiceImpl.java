package com.stylefeng.guns.modular.huamao.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.ArticlesMapper;
import com.stylefeng.guns.modular.huamao.model.Articles;
import com.stylefeng.guns.modular.huamao.service.IAppArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppArticleServiceImpl extends ServiceImpl<ArticlesMapper,Articles> implements IAppArticleService{
    @Autowired
    private ArticlesMapper articlesMapper;
    @Override
    public Articles selectArticlesById(Integer articlesId) {

        return articlesMapper.selectArticlesById(articlesId);
    }
}
