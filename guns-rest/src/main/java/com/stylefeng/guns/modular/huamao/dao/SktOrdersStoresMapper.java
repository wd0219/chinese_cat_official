package com.stylefeng.guns.modular.huamao.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStoresDto;

/**
 * <p>
 * 线下商家订单表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-24
 */
public interface SktOrdersStoresMapper extends BaseMapper<SktOrdersStores> {
	/**
	 * 页面显示列表
	 * @param sktOrdersStoresDto
	 * @return
	 */
	public List<SktOrdersStoresDto> showOrdersStoresInfo(SktOrdersStoresDto sktOrdersStoresDto);
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int selectOrdersStoresTotal();

	/**
	 * 显示首页
	 * @param sktOrdersStoresDto
	 * @return
	 */
    List<Map<String,Object>> selectUserOrdersStores(SktOrdersStoresDto sktOrdersStoresDto);
    
    
	public List<Map<String, Object>> selectShopOrdersStoresUp(SktOrdersStoresDto sktOrdersStoresDto);
	
	public List<Map<String, Object>> selectShopOrdersStoresCh(SktOrdersStoresDto sktOrdersStoresDto);
	
	public List<Map<String, Object>> selectShopOrdersStoresOk(SktOrdersStoresDto sktOrdersStoresDto);

    List<Map<String,Object>> appSelectAllOrders(Map<String,Object> map);

    Integer appOrderStoreCancel(Map<String,Object> map);

	SktOrdersStores appFindOrderStoreById(String orderNo);

    BigDecimal appFindTodayTurnover(Map<String,Object> map);

	Integer appFindNotPayOrders(Map<String, Object> map2);

	Map<String,Object> appFindOrderByid(Integer orderId);

    List<Map<String,Object>> selectOrderStoresByMap(Map<String, Object> map);
}
