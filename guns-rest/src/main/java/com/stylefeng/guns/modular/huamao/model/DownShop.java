package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DownShop extends Model<DownShop> {
    @Override
    protected Serializable pkVal() {
        return null;
    }

    /**
     * 登录名 昵称
     */
    private String loginName;
    /**
     * 手机
     */
    private String userPhone;
    /**
     * 用户类型 0普通 1主管 2经理
     */
    private Integer userType;
    /**
     * 线下商家标志 1是 0否
     */
    private Integer isStore;
    /**
     * 线上商家标志 1是 0否
     */
    private Integer isShop;
    /**
     * 代理公司股东标志:0不是股东 1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
     */
    private Integer isAgent;
    /**
     * 真实姓名
     */
    private String trueName;
    /**
     * 会员头像
     */
    private String userPhoto;
    /**
     * 二维码路径
     */
    private String QRcode;
    /**
     * 账号状态：0停用 1启用
     */
    private Integer userStatus;
    /**
     * 积分
     */
    private BigDecimal score;
    /**
     * 开元宝
     */
    private BigDecimal kaiyuan;
    /**
     * 现金
     */
    private BigDecimal cash;

    /**
     * 累积获得分红(当登陆人是代理时显示)
     */
    private BigDecimal totalDividendScore;

    public DownShop() {
    }

    public DownShop(String loginName, String userPhone, Integer userType, Integer isStore, Integer isShop, Integer isAgent, String trueName, String userPhoto, String QRcode, Integer userStatus, BigDecimal score, BigDecimal kaiyuan, BigDecimal cash, BigDecimal totalDividendScore) {
        this.loginName = loginName;
        this.userPhone = userPhone;
        this.userType = userType;
        this.isStore = isStore;
        this.isShop = isShop;
        this.isAgent = isAgent;
        this.trueName = trueName;
        this.userPhoto = userPhoto;
        this.QRcode = QRcode;
        this.userStatus = userStatus;
        this.score = score;
        this.kaiyuan = kaiyuan;
        this.cash = cash;
        this.totalDividendScore = totalDividendScore;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getIsStore() {
        return isStore;
    }

    public void setIsStore(Integer isStore) {
        this.isStore = isStore;
    }

    public Integer getIsShop() {
        return isShop;
    }

    public void setIsShop(Integer isShop) {
        this.isShop = isShop;
    }

    public Integer getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Integer isAgent) {
        this.isAgent = isAgent;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getQRcode() {
        return QRcode;
    }

    public void setQRcode(String QRcode) {
        this.QRcode = QRcode;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getTotalDividendScore() {
        return totalDividendScore;
    }

    public void setTotalDividendScore(BigDecimal totalDividendScore) {
        this.totalDividendScore = totalDividendScore;
    }

    @Override
    public String toString() {
        return "DownShop{" +
                "loginName='" + loginName + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userType=" + userType +
                ", isStore=" + isStore +
                ", isShop=" + isShop +
                ", isAgent=" + isAgent +
                ", trueName='" + trueName + '\'' +
                ", userPhoto='" + userPhoto + '\'' +
                ", QRcode='" + QRcode + '\'' +
                ", userStatus=" + userStatus +
                ", score=" + score +
                ", kaiyuan=" + kaiyuan +
                ", cash=" + cash +
                ", totalDividendScore=" + totalDividendScore +
                '}';
    }
}
