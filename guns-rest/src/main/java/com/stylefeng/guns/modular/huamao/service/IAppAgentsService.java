package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktAgents;

import java.util.Map;

public interface IAppAgentsService extends IService<SktAgents> {
    SktAgents selectAgentsProvinceId(Map<String, Object> map);

    SktAgents selectAgentsCityId(Map<String, Object> map);

    SktAgents selectAgentsAreaId(Map<String, Object> map);

}
