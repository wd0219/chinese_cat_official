package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.model.Account;
import com.stylefeng.guns.modular.huamao.model.AccountList;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;

/**
 * 用户账户控制器
 *
 * @author fengshuonan
 * @Date 2018-04-11 11:06:33
 */
@CrossOrigin
@Controller
@RequestMapping("/account")
public class AccountController extends BaseController {

	private String PREFIX = "/huamao/account/";

	@Autowired
	private IAccountService accountService;
	@Autowired
	private IUsersService iUsersService;
	/**
	 * 跳转到用户账户首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String loginName, Model model) {
		// 参数是为了在用户点击昵称查看用户账户详情使用。
		// 判断语句：如果有值说明是看用户账户详情
		if (loginName != null && loginName != "") {
			model.addAttribute("model", loginName);
		} else {
			model.addAttribute("model", "昵称");
		}
		return PREFIX + "account.html";
	}

	/**
	 * 跳转到添加用户账户
	 */
	@RequestMapping("/account_add")
	public String accountAdd() {
		return PREFIX + "account_add.html";
	}

	/**
	 * 跳转到修改用户账户
	 */
	@RequestMapping("/account_update/{accountId}")
	public String accountUpdate(@PathVariable Integer accountId, Model model) {
		Account account = accountService.selectById(accountId);
		model.addAttribute("item", account);
		LogObjectHolder.me().set(account);
		return PREFIX + "account_edit.html";
	}

	/**
	 * 获取用户账户列表 ：{loginName2}查看用户账户详情传来的昵称
	 */
	@RequestMapping(value = "/list/{loginName2}")
	@ResponseBody
	public Object list(@PathVariable(value = "loginName2", required = false) String loginName2,
			AccountList accountList) {
		// 判断如果不是‘昵称’并且accountList.getUserPhone()不是null说明要看用户积分详情，是‘手机号’并且accountList.getUserPhone()是''或是有数据说明是普通的查询所有的积分详情
		if (!loginName2.equals("昵称") && accountList.getUserPhone() == null) {
			accountList.setLoginName(loginName2);
		} else {
			accountList.setLoginName("");
		}
		return accountService.selectAccountAll(accountList);
		// return accountService.selectList(null);
	}


	/**
	 * 删除用户账户
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer accountId) {
		accountService.deleteById(accountId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改用户账户
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(Account account) {
		accountService.updateById(account);
		return SUCCESS_TIP;
	}

	/**
	 * 用户账户详情
	 */
	@RequestMapping(value = "/detail/{accountId}")
	@ResponseBody
	public Object detail(@PathVariable("accountId") Integer accountId) {
		return accountService.selectById(accountId);
	}
	
	/*********************首页*********************/
	
	/**
	 * 新增用户账户
	 */
	@RequestMapping(value = "/addAccout")
	@ResponseBody
	public Result addAccout(Account account) {
		Account selectAccountByuserId = accountService.selectAccountByuserId(account.getUserId());
		if(ToolUtil.isNotEmpty(selectAccountByuserId)){
			selectAccountByuserId.setCash(account.getCash().add(selectAccountByuserId.getCash()));
			boolean updateById = accountService.updateById(selectAccountByuserId);
			return updateById==true?Result.OK(null):Result.EEROR("用户充值失败！");
		}else{
			account.setCreateTime(new Date());
			boolean insert = accountService.insert(account);
			return insert==true?Result.OK(null):Result.EEROR("用户充值失败！");
		}
		
		
	}
}
