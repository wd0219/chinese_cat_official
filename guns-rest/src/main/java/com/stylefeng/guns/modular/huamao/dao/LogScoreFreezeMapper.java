package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreeze;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreezeList;

/**
 * <p>
 * 待发积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogScoreFreezeMapper extends BaseMapper<LogScoreFreeze> {

	// 展示代发积分列表
	List<LogScoreFreezeList> selectScoreFreezeList(LogScoreFreezeList logScoreFreezeList);

	/**
	 * 首页显示冻结积分
	 * @param logScoreFreezeList
	 * @return
	 */
    List<Map<String,Object>> selectUserScoreFreeze(LogScoreFreezeList logScoreFreezeList);

    List<LogScoreFreeze> selectLogScoreFreezeByUserId(Map<String, Object> map);

}
