package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.HomeMenus;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前台菜单表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-28
 */
public interface IHomeMenusService extends IService<HomeMenus> {
	List<Map<String,Object>> selectHomeMenus(HomeMenus homeMenus);
}
