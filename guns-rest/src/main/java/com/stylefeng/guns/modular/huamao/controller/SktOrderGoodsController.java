package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;
import com.stylefeng.guns.modular.huamao.service.ISktOrderGoodsService;

/**
 * 商品订单控制器
 *
 * @author fengshuonan
 * @Date 2018-05-11 10:23:53
 */
@Controller
@RequestMapping("/sktOrderGoods")
public class SktOrderGoodsController extends BaseController {

    private String PREFIX = "/huamao/sktOrderGoods/";

    @Autowired
    private ISktOrderGoodsService sktOrderGoodsService;

    /**
     * 跳转到商品订单首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktOrderGoods.html";
    }

    /**
     * 跳转到添加商品订单
     */
    @RequestMapping("/sktOrderGoods_add")
    public String sktOrderGoodsAdd() {
        return PREFIX + "sktOrderGoods_add.html";
    }

    /**
     * 跳转到修改商品订单
     */
    @RequestMapping("/sktOrderGoods_update/{sktOrderGoodsId}")
    public String sktOrderGoodsUpdate(@PathVariable Integer sktOrderGoodsId, Model model) {
        SktOrderGoods sktOrderGoods = sktOrderGoodsService.selectById(sktOrderGoodsId);
        model.addAttribute("item",sktOrderGoods);
        LogObjectHolder.me().set(sktOrderGoods);
        return PREFIX + "sktOrderGoods_edit.html";
    }

    /**
     * 获取商品订单列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktOrderGoodsService.selectList(null);
    }

    /**
     * 新增商品订单
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktOrderGoods sktOrderGoods) {
        sktOrderGoodsService.insert(sktOrderGoods);
        return SUCCESS_TIP;
    }

    /**
     * 删除商品订单
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktOrderGoodsId) {
        sktOrderGoodsService.deleteById(sktOrderGoodsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品订单
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktOrderGoods sktOrderGoods) {
        sktOrderGoodsService.updateById(sktOrderGoods);
        return SUCCESS_TIP;
    }

    /**
     * 商品订单详情
     */
    @RequestMapping(value = "/detail/{sktOrderGoodsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktOrderGoodsId") Integer sktOrderGoodsId) {
        return sktOrderGoodsService.selectById(sktOrderGoodsId);
    }
}
