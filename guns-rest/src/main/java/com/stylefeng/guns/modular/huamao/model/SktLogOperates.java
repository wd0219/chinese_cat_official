package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 操作记录表
 * </p>
 *
 * @author slt123
 * @since 2018-04-27
 */
@TableName("skt_log_operates")
public class SktLogOperates extends Model<SktLogOperates> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "operateId", type = IdType.AUTO)
    private Integer operateId;
    /**
     * 职员ID
     */
    private Integer staffId;
    /**
     * 操作时间
     */
    private Date operateTime;
    /**
     * 所属菜单ID
     */
    private Integer menuId;
    /**
     * 操作说明
     */
    private String operateDesc;
    /**
     * 操作链接地址
     */
    private String operateUrl;
    /**
     * 请求内容
     */
    private String content;
    /**
     * 操作IP
     */
    private String operateIP;


    public Integer getOperateId() {
        return operateId;
    }

    public void setOperateId(Integer operateId) {
        this.operateId = operateId;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getOperateDesc() {
        return operateDesc;
    }

    public void setOperateDesc(String operateDesc) {
        this.operateDesc = operateDesc;
    }

    public String getOperateUrl() {
        return operateUrl;
    }

    public void setOperateUrl(String operateUrl) {
        this.operateUrl = operateUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOperateIP() {
        return operateIP;
    }

    public void setOperateIP(String operateIP) {
        this.operateIP = operateIP;
    }

    @Override
    protected Serializable pkVal() {
        return this.operateId;
    }

    @Override
    public String toString() {
        return "SktLogOperates{" +
        "operateId=" + operateId +
        ", staffId=" + staffId +
        ", operateTime=" + operateTime +
        ", menuId=" + menuId +
        ", operateDesc=" + operateDesc +
        ", operateUrl=" + operateUrl +
        ", content=" + content +
        ", operateIP=" + operateIP +
        "}";
    }
}
