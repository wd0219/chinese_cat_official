package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreSpecialMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecial;
import com.stylefeng.guns.modular.huamao.service.IAppLogScoreSpecialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppLogScoreSpecialServiceImpl extends ServiceImpl<LogScoreSpecialMapper,LogScoreSpecial> implements IAppLogScoreSpecialService {
    @Autowired
    private LogScoreSpecialMapper logScoreSpecialMapper;

    @Override
    public List<LogScoreSpecial> selectLogScoreSpecialByUserId(Map<String, Object> map) {
        return logScoreSpecialMapper.selectLogScoreSpecialByUserId(map);
    }
}
