package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktSalesDate;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商家每日营业额统计表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
public interface SktSalesDateMapper extends BaseMapper<SktSalesDate> {
	
	/*
	 * 展示全部
	 * */
	public List<SktSalesDate> sktSalesDatefindAll(SktSalesDate sktSalesDate);
}
