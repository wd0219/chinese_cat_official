package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;

import java.util.List;

/**
 * <p>
 * 系统消息表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface IMessagesService extends IService<Messages> {

	// 通过传过来的ID进行逻辑删除
	public void deleteById2(Integer id);

	// 系统消息展示
	public List<MessagesList> selectMessagesAll(MessagesList messagesList);

	// 获取用户
	public List<MessagesList> getUsers(String userPhone);

	// 添加广告信息
	public boolean addMessages(String li, MessagesList messagesList);
	
	/**
	 * 查询用户消息
	 * @param messages
	 * @return
	 */
	public List<MessagesList> getMessage(Messages messages);
	
	public List<MessagesList> getMessage1(Integer messages);
	
	/**
	 * 修改用户状态
	 * @param msgStatus
	 * @param dataFlag
	 * @param ids
	 * @return
	 */
	public Integer updateMessageStatus(Integer msgStatus, Integer dataFlag, Integer[] ids);
	/**
	 * 用户未读消息数量
	 * @param messages
	 * @return
	 */
	public Integer getMessageCount(Messages messages);
	// 添加广告信息
	public boolean addMessages(Integer msgType,Integer sendUserId,List<Integer> usersId,String msgContent,String msgJson);


}
