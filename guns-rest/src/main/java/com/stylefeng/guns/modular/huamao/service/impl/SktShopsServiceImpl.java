package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import java.math.BigDecimal;
import java.util.*;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.dao.SktShopFreightsMapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopsMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.service.IGoodsService;
import com.stylefeng.guns.modular.huamao.service.ISktFavoritesService;
import com.stylefeng.guns.modular.huamao.model.SktShopFreights;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.model.SktShops2;
import com.stylefeng.guns.modular.huamao.model.SktShops3;
import com.stylefeng.guns.modular.huamao.service.ISktShopsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商家信息表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
@Service
public class SktShopsServiceImpl extends ServiceImpl<SktShopsMapper, SktShops> implements ISktShopsService {
	@Autowired
	private SktShopsMapper sktShopsMapper;
	@Autowired
	private IGoodsService goodsService;
	@Autowired
	private ISktFavoritesService sktFavoritesService;

	@Autowired
	private SktShopFreightsMapper sktShopFreightsMapper;
	@Override
	public List<SktShops> sktShopsfindAll(SktShops sktShops) {
		List<SktShops> list = sktShopsMapper.sktShopsfindAll(sktShops);
		return list;
	}
	@Override
	public int selectCount() {
		// TODO Auto-generated method stub
		return sktShopsMapper.selectCount();
	}
	@Override
	public int getTotal() {
		// TODO Auto-generated method stub
		return sktShopsMapper.getTotal();
	}
	/*
	 * 店铺街展示店铺
	 * */
	@Override
	public List<Map<String,Object>> findShopsAll(Integer hyId, Integer provinceId, Integer totalScore, Integer onlineTurnover,Integer userId) {
		List<Map<String, Object>> m = sktShopsMapper.findShopsAll(hyId,provinceId,totalScore,onlineTurnover,userId);
		 return m;
	}
	/*
	 * 店铺街展示店铺推荐
	 * */
	@Override
	public List<Map<String, Object>> findShopsByIdtj(Integer shopId,Integer limit) {
		List<Map<String, Object>> map = sktShopsMapper.findShopsByIdtj(shopId,limit);
		return map;
	}
	/*
	 * 店铺街查询行业
	 * */
	@Override
	public List<Map<String, Object>> findShopsHyName() {
		return sktShopsMapper.findShopsHyName();
	}
	@Override
	public List<Map<String, Object>> selectByHyIdShops(Integer hyId) {
		return sktShopsMapper.selectByHyIdShops(hyId);
		 
	}
	/*
	 * 查询热门品牌findBrandId
	 * */
	@Override
	public List<Map<String, Object>> findBrandId() {
		return sktShopsMapper.findBrandId();
	}
	  /*
     * 根据店铺ID查询商品
     * */
	@Override
	public List<Map<String, Object>> selectShopsGoodsId(Integer shopId) {
		return sktShopsMapper.selectShopsGoodsId(shopId);
	}
	/*
	 * 自营街店铺展示
	 * */
	@Override
	public List<Map<String, Object>> selectShopsZy() {
		return sktShopsMapper.selectShopsZy();
	}
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectShopNameShops(SktShops2 sktShops2){
		List<Map<String,Object>> list = sktShopsMapper.selectShopNameShops(sktShops2);
		return list;
	}
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	@Override
	public List<Map<String, Object>> selectLogoShops(SktShops2 sktShops2) {
		return sktShopsMapper.selectLogoShops(sktShops2);
	} 
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	@Override
	public List<Map<String, Object>> selectLoginNameShops(SktShops2 sktShops2) {
		return sktShopsMapper.selectLoginNameShops(sktShops2);
	}
	/*
     * 根据店铺ID查询ZY商品
     * */
	@Override
	public List<Map<String, Object>> selectShopsZyGoodsId(Integer shopId) {
		return sktShopsMapper.selectShopsZyGoodsId(shopId);
	}
	@Override
	public List<Map<String, Object>> findWebLunbo() {
		return sktShopsMapper.findWebLunbo();
	}
	@Override
	public List<Map<String, Object>> selectGoods(Integer goodsId) {
		// TODO Auto-generated method stub
		return sktShopsMapper.selectGoods(goodsId);
	}
	@Override
	public List<Map<String, Object>> findByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findByUserId(userId);
	}
	@Override
	public List<Map<String, Object>> findByUserIdMn(Integer userId) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findByUserIdMn(userId);
	}
	@Override
	public List<Map<String, Object>> findByUserIdZrZc(Integer userId) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findByUserIdZrZc(userId);
	}
	@Override
	public List<Map<String, Object>> findByShopsXinxi(Integer userId) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findByShopsXinxi(userId);
	}
	@Override
	public Integer updateShops(SktShops sktShops) {
		// TODO Auto-generated method stub
		return sktShopsMapper.updateShops(sktShops);
	}
	@Override
	public List<Map<String, Object>> findByXSShopsXinxi(Integer shopId) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findByXSShopsXinxi(shopId);
	}
	@Override
	public Integer updateByXSShopsImg(SktShops sktShops) {
		// TODO Auto-generated method stub
		return sktShopsMapper.updateByXSShopsImg(sktShops);
	}
	@Override
	public List<Map<String, Object>> findByXSShopsImg(Integer shopId) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findByXSShopsImg(shopId);
	}
	@Override
	public List<Map<String, Object>> findByYunFei(Integer shopId) {
		return sktShopsMapper.findByYunFei(shopId);
	}

	@Transactional
	public boolean updateByYunFei(List<Map<String,Object>> sktShops2) {
		try{
			for (Map<String, Object> map : sktShops2) {
//			EntityWrapper<SktShopFreights> ew = new EntityWrapper<SktShopFreights>();
//			SktShopFreights shops = new SktShopFreights();
//			shops.setFreight(Integer.parseInt(map.get("freight").toString()));
//			ew.where("shopId={0}", map.get("shopId").toString()).and("areaId2={0}",map.get("areaId2").toString());
//		   // this.update(shops, ew);
//			sktShopFreightsMapper.delete(ew);
//			sktShopFreightsMapper.update(shops, ew);
				SktShopFreights shopsFreights = new SktShopFreights();
				shopsFreights.setShopId(Integer.parseInt(map.get("shopId").toString()));
				shopsFreights.setAreaId2(Integer.parseInt(map.get("areaId2").toString()));
				EntityWrapper<SktShopFreights> ew = new EntityWrapper<SktShopFreights>(shopsFreights);
				sktShopFreightsMapper.delete(ew);
				shopsFreights.setFreight(Integer.parseInt(map.get("freight").toString()));
				shopsFreights.setCreateTime(new Date());
				sktShopFreightsMapper.insert(shopsFreights);
			}
			return  true;
		}catch (Exception e){
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return false;
		}

		//return sktShopsMapper.findByYunFei(Integer.parseInt(sktShops2.get(0).get("shopId").toString()));
//		return sktShopsMapper.findByYunFei(sktShops2.get(0).get("shopId"));
//		return sktShopsMapper.updateByYunFei(sktShops, shopId);
//		return sktShopsMapper.updateByYunFei(sktShops2);
	}
	@Override
	public List<Map<String, Object>> findDaiOrders(Integer shopId, String orderNo, String beginTime, String endTime) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findDaiOrders(shopId,orderNo,beginTime,endTime);
	}
	@Override
	public List<Map<String, Object>> findYiOrders(Integer shopId, String orderNo, String beginTime, String endTime) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findYiOrders(shopId,orderNo,beginTime,endTime);
	}
	@Override
	public List<Map<String, Object>> findDaiFaHuoOrders(Integer shopId, String orderNo, String beginTime, String endTime) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findDaiFaHuoOrders(shopId,orderNo,beginTime,endTime);
	}
	@Override
	public List<Map<String, Object>> findTuiOrders(Integer shopId, String orderNo, String beginTime, String endTime,Integer refundStatus) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findTuiOrders(shopId,orderNo,beginTime,endTime,refundStatus);
	}
	@Override
	public List<Map<String, Object>> findDaiShouOrders(Integer shopId, String orderNo, String beginTime, String endTime) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findDaiShouOrders(shopId,orderNo,beginTime,endTime);
	}
	@Override
	public List<Map<String, Object>> findQuxiaoOrders(Integer shopId, String orderNo, String beginTime, String endTime) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findQuxiaoOrders(shopId,orderNo,beginTime,endTime);
	}
	@Override
	public List<Map<String, Object>> findHPcats(SktShops2 sktShops2) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findHPcats(sktShops2);
	}
	@Override
	public List<Map<String, Object>> findShopsId(SktShops3 sktshops3) {
		// TODO Auto-generated method stub
		return sktShopsMapper.findShopsId(sktshops3);
	}
	@Override
	public List<Map<String, Object>> selectShopsName(String shopName) {
		// TODO Auto-generated method stub
		return sktShopsMapper.selectShopsName(shopName);
	}
	//查询所有商家的userId
	@Override
	public List<Integer> selectAllId() {
		return sktShopsMapper.selectAllId();
	}

	@Override
	public List<Map<String, Object>> selectShops(Integer goodsId) {
		return null;
	}

	@Override
	public Map<String, Object> shopHome(Integer shopId) {
		Map<String, Object> map=new HashMap<>();
//		Goods goods =new  Goods();
//		goods.setShopId(shopId);
//		goods.setIsRecom(1);
//		EntityWrapper<Goods> entityWrapper=new EntityWrapper<Goods>(goods);
//		List<Goods> goodsList = goodsService.selectList(entityWrapper);
		SktShops sktShops = this.selectById(shopId);
		List<Map<String ,Object>> list=new ArrayList<>();
		Map<String ,Object> map1=new HashMap<>();
		map1.put("shopImg1",sktShops.getShopImg1());
		map1.put("shopImg2",sktShops.getShopImg2());
		map1.put("shopImg3",sktShops.getShopImg3());
		map1.put("shopName",sktShops.getShopName());
		map1.put("createTime",sktShops.getCreateTime());
		map1.put("shopLogo",sktShops.getLogo());
		map1.put("isSelf",sktShops.getIsSelf());
	//	list.add(map1);
		map.put("shopImg",map1);
		SktFavorites sktFavorites=new SktFavorites();
		sktFavorites.setFavoriteType(1);
		sktFavorites.setTargetId(shopId);
		EntityWrapper<SktFavorites> entityWrapper=new EntityWrapper<SktFavorites>(sktFavorites);
		List<SktFavorites> sktFavorites1 = sktFavoritesService.selectList(entityWrapper);
		if (sktFavorites1!=null && sktFavorites1.size()>0){
			map.put("Favorites",sktFavorites1.size());
		}else {
			map.put("Favorites",0);
		}
		return map;
	}
	@Override
	public Map<String, Object> shopsStore(Integer shopId) {
		SktShops shops = sktShopsMapper.selectById(shopId);
		Map<String,Object> map = new HashMap<String, Object>();
		if(ToolUtil.isNotEmpty(shops)){
        	map.put("storeImg1", shops.getStoreImg1());
        	map.put("storeImg2", shops.getStoreImg2());
        	map.put("storeImg3", shops.getStoreImg3());
        	map.put("shopName", shops.getShopName());
        	map.put("address", shops.getAddress());
        	map.put("telephone", shops.getTelephone());
        	map.put("startTime", shops.getStartTime());
        	map.put("endTime", shops.getEndTime());
        	map.put("companyName", shops.getCompanyName());//商家名称
    	}
		return map;
	}
	@Override
	public List<Map<String, Object>> shopsStorePos(BigDecimal lng, BigDecimal lat, String shopName, String industry) {
		try {
			//List<Map<String,Object>> list = sktShopsMapper.shopsStorePos(lng,lat,shopName,industry);
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("lng",lng);
			map.put("lat",lat);
			map.put("shopName",shopName);
			map.put("industry",industry);
			List<Map<String,Object>> list = sktShopsMapper.shopsStorePos(map);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public Map<String, Object> appShopsDetailed(Integer shopId) {
		try {
			//查询店铺详情，所有商品
			Map<String,Object> map = sktShopsMapper.appFindShopsDetailed(shopId);
			//查询店铺热销个数
			Integer hotNum = sktShopsMapper.appFindHotNum(shopId);
			//新品个数
			Integer newNum = sktShopsMapper.appFindNewNum(shopId);
			map.put("hotNum",hotNum);
			map.put("newNum",newNum);
			map.put("code",1);
			return map;
		}catch (Exception e){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("code",0);
			map.put("msg","查询错误");
			return map;
		}
	}
	@Override
	public List<Map<String, Object>> shopsStorePosL(BigDecimal lng, BigDecimal lat) {
		List<Map<String,Object>> list = sktShopsMapper.shopsStorePosL(lng,lat);
		return list;
	}

    @Override
    public Map<String, Object> selectUsersByShopId(Integer shopId) {
		return sktShopsMapper.selectUsersByShopId(shopId);
    }

	@Override
	public List<Map<String, Object>> findCheckOrders(Integer shopId, String orderNo, String beginTime, String endTime) {
		return sktShopsMapper.findCheckOrders(shopId,orderNo,beginTime,endTime);
	}

    @Override
    public List<Map<String, Object>> findRefuseOrders(Integer shopId, String orderNo, String beginTime, String endTime) {
        return sktShopsMapper.findRefuseOrders(shopId,orderNo,beginTime,endTime);
    }

    /**
	 *楼层广告图
	 * @param map
	 * @return
	 */
	@Override
	public Map<String,Object> floorAd(Map<String, Object> map){
		return sktShopsMapper.floorAd(map);
	}
	@Override
	public List<Map<String, Object>> selectArticle(Integer articleId) {
		// TODO Auto-generated method stub
		return sktShopsMapper.selectArticle(articleId);
	}
}
