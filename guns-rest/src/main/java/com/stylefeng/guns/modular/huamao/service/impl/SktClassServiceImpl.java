package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktClassMapper;
import com.stylefeng.guns.modular.huamao.model.SktClass;
import com.stylefeng.guns.modular.huamao.service.ISktClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商学院表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
@Service
public class SktClassServiceImpl extends ServiceImpl<SktClassMapper, SktClass> implements ISktClassService {
    @Override
    public Map<String,Object> insertSktClass(SktClass sktClass ){
        Map<String,Object> map=new HashMap<>();
        sktClass.setCreatTime(new Date());
        boolean insert = this.insert(sktClass);
        if (insert){
            map.put("status",1);
        }else {
            map.put("status",-1);
        }
        return map;
    }

    @Autowired
    private SktClassMapper sktClassMapper;

    @Override
    public SktClass selectSktClassByClassName(String className) {
        return sktClassMapper.selectSktClassByClassName(className);
    }

    @Override
    public List<Map<String, Object>> selectClassByTypeOrAll(Map<String,Object> map) {
        return sktClassMapper.selectClassByTypeOrAll(map);
    }

    @Override
    public List<Map<String,Object>> selectSearchVideo(Map<String,Object> map) {
        return sktClassMapper.selectSearchVideo(map);
    }

    @Override
    public List<Map<String,Object>> selectPageClass(Map<String, Object> map) {
        return sktClassMapper.selectPageClass(map);
    }

    @Override
    public Map<String, Object> selectClassByClassId(SktClass sktClass) {
        return sktClassMapper.selectClassByClassId(sktClass);
    }

    @Override
    public List<Map<String, Object>> selectBatchClassByClassId(Map<String, Object> map) {
        return sktClassMapper.selectBatchClassByClassId(map);
    }

}
