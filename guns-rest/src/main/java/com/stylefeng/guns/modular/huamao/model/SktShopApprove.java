package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 企业认证表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@TableName("skt_shop_approve")
public class SktShopApprove extends Model<SktShopApprove> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "applyId", type = IdType.AUTO)
    private Integer applyId;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 营业执照所在省id
     */
    private Integer provinceId;
    /**
     * 营业执照所在市id
     */
    private Integer cityId;
    /**
     * 营业执照所在区县id
     */
    private Integer areaId;
    /**
     * 是否法人 1法人 0不是法人
     */
    private Integer isLegal;
    /**
     * 法人
     */
    private String legalPerson;
    /**
     * 法人身份证号
     */
    private String IDnumber;
    /**
     * 法人手持身份证照片
     */
    private String legalPersonImg;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 营业执照号码
     */
    private String licenseNo;
    /**
     * 企业类型：0个体户 1公司
     */
    private Integer companyType;
    /**
     * 是否三证合一：0未合并 1合并
     */
    private Integer licenseMerge;
    /**
     * 营业执照
     */
    private String licenseImg;
    /**
     * 组织机构代码证
     */
    private String codeImg;
    /**
     * 税务登记证
     */
    private String taxImg;
    /**
     * 授权书
     */
    private String authorizationImg;
    /**
     * 行业ID
     */
    private Integer industry;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 经营地址
     */
    private String address;
    /**
     * 经营电话
     */
    private String telephone;
    /**
     * logo
     */
    private String logo;
    /**
     * 门店状态：-1拒绝0未审核1已审核
     */
    private Integer shopStatus;
    /**
     * 状态说明(一般用于停止和拒绝说明)
     */
    private String statusDesc;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 审核时间
     */
    private Date checkTime;
    /**
     * 审核人
     */
    private Integer checkStaffId;


    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getIsLegal() {
        return isLegal;
    }

    public void setIsLegal(Integer isLegal) {
        this.isLegal = isLegal;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getIDnumber() {
        return IDnumber;
    }

    public void setIDnumber(String IDnumber) {
        this.IDnumber = IDnumber;
    }

    public String getLegalPersonImg() {
        return legalPersonImg;
    }

    public void setLegalPersonImg(String legalPersonImg) {
        this.legalPersonImg = legalPersonImg;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public Integer getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Integer companyType) {
        this.companyType = companyType;
    }

    public Integer getLicenseMerge() {
        return licenseMerge;
    }

    public void setLicenseMerge(Integer licenseMerge) {
        this.licenseMerge = licenseMerge;
    }

    public String getLicenseImg() {
        return licenseImg;
    }

    public void setLicenseImg(String licenseImg) {
        this.licenseImg = licenseImg;
    }

    public String getCodeImg() {
        return codeImg;
    }

    public void setCodeImg(String codeImg) {
        this.codeImg = codeImg;
    }

    public String getTaxImg() {
        return taxImg;
    }

    public void setTaxImg(String taxImg) {
        this.taxImg = taxImg;
    }

    public String getAuthorizationImg() {
        return authorizationImg;
    }

    public void setAuthorizationImg(String authorizationImg) {
        this.authorizationImg = authorizationImg;
    }

    public Integer getIndustry() {
        return industry;
    }

    public void setIndustry(Integer industry) {
        this.industry = industry;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(Integer shopStatus) {
        this.shopStatus = shopStatus;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Integer getCheckStaffId() {
        return checkStaffId;
    }

    public void setCheckStaffId(Integer checkStaffId) {
        this.checkStaffId = checkStaffId;
    }

    @Override
    protected Serializable pkVal() {
        return this.applyId;
    }

    @Override
    public String toString() {
        return "SktShopApprove{" +
        "applyId=" + applyId +
        ", userId=" + userId +
        ", provinceId=" + provinceId +
        ", cityId=" + cityId +
        ", areaId=" + areaId +
        ", isLegal=" + isLegal +
        ", legalPerson=" + legalPerson +
        ", IDnumber=" + IDnumber +
        ", legalPersonImg=" + legalPersonImg +
        ", companyName=" + companyName +
        ", licenseNo=" + licenseNo +
        ", companyType=" + companyType +
        ", licenseMerge=" + licenseMerge +
        ", licenseImg=" + licenseImg +
        ", codeImg=" + codeImg +
        ", taxImg=" + taxImg +
        ", authorizationImg=" + authorizationImg +
        ", industry=" + industry +
        ", shopName=" + shopName +
        ", address=" + address +
        ", telephone=" + telephone +
        ", logo=" + logo +
        ", shopStatus=" + shopStatus +
        ", statusDesc=" + statusDesc +
        ", createTime=" + createTime +
        ", checkTime=" + checkTime +
        ", checkStaffId=" + checkStaffId +
        "}";
    }
}
