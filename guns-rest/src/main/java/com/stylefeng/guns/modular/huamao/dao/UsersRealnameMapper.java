package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;
import com.stylefeng.guns.modular.huamao.model.UsersRealnameDTO;

/**
 * <p>
 * 实名认证信息表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
public interface UsersRealnameMapper extends BaseMapper<UsersRealname> {
	/**
	 * 查询认证列表
	 * @param usersRealname
	 * @return
	 */
	public List<UsersRealnameDTO> showRenZhengInfo(UsersRealname usersRealname);
	/**
	 * 详情
	 * @param realId
	 * @return
	 */
	public UsersRealnameDTO showRenZhengXiangQing(@Param(value = "realId") Integer realId);
	/**
	 * 根据用户id查询身份证
	 * @param userId
	 * @return
	 */
	public Map<String, Object> selectUserCardId(@Param(value = "userId") Integer userId);
	/**
	 * 根据id查询该身份证有多少个
	 * @param cardID
	 * @return
	 */
	public Integer selectUserCardIds(@Param(value = "cardID") String cardID);

    UsersRealname selectUsersRealNameByUserId(Integer userId);

    UsersRealname selectUsersRealNameByCardId(String id);
}
