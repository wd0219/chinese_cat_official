package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnover;

import java.util.List;

public interface IAppLogKaiyuanTurnoverService extends IService<LogKaiyuanTurnover> {
    List<LogKaiyuanTurnover> huaBaoTurnoverList(Integer userId);
}
