package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsStockholderMapper;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholder;
import com.stylefeng.guns.modular.huamao.service.IAppAgentsStockholderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppAgentsStockholderServiceImpl extends ServiceImpl<SktAgentsStockholderMapper,SktAgentsStockholder> implements IAppAgentsStockholderService {
    @Autowired
    private SktAgentsStockholderMapper sktAgentsStockholderMapper;

    @Override
    public List<SktAgentsStockholder> selectAgentsStockholderList(Map<String, Object> map) {
        return sktAgentsStockholderMapper.selectAgentsStockholderList(map);
    }
}
