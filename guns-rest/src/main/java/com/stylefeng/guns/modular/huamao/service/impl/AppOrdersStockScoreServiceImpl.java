package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersStockscoreMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStockscore;
import com.stylefeng.guns.modular.huamao.service.IAppOrdersStockScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppOrdersStockScoreServiceImpl extends ServiceImpl<SktOrdersStockscoreMapper,SktOrdersStockscore> implements IAppOrdersStockScoreService {
    @Autowired
    private SktOrdersStockscoreMapper sktOrdersStockscoreMapper;

    @Override
    public SktOrdersStockscore selectByOredrNo(Map<String, Object> orderWhere) {
        return sktOrdersStockscoreMapper.selectByOredrNo(orderWhere);
    }

    @Override
    public void updateOrderStockScoreById(SktOrdersStockscore sktOrdersStockscore2) {
        sktOrdersStockscoreMapper.updateOrderStockScoreById(sktOrdersStockscore2);
    }

    @Override
    public SktOrdersStockscore selectOrdersStockscoreByWhere(SktOrdersStockscore sktOrdersStockscore) {
        return sktOrdersStockscoreMapper.selectOrdersStockscoreByWhere(sktOrdersStockscore);
    }
}
