package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsAppraisesMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsMapper;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersMapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopScoresMapper;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesCustm;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktShopScores;
import com.stylefeng.guns.modular.huamao.service.IGoodsAppraisesService;

/**
 * <p>
 * 商品评价表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsAppraisesServiceImpl extends ServiceImpl<GoodsAppraisesMapper, GoodsAppraises>
		implements IGoodsAppraisesService {

	@Autowired
	private GoodsAppraisesMapper goodsAppraisesMapper;
	@Autowired
	private GoodsMapper goodsMapper;
	@Autowired
	private SktOrdersMapper sktOrdersMapper;
	@Autowired
	private SktShopScoresMapper sktShopScoresMapper;

	@Override
	public List<Map<String, Object>> selectAll(GoodsAppraisesCustm goodsAppraisesCustm) {
		// TODO Auto-generated method stub
		return baseMapper.selectAll(goodsAppraisesCustm);
	}

	@Override
	public int selectGoodsAppraisesTotal() {
		// TODO Auto-generated method stub
		return baseMapper.selectGoodsAppraisesTotal();
	}

	@Override
	public List<Map<String, Object>> readComment(Integer goodsId) {
		return baseMapper.selectAppGoodsAppraises(goodsId);
	}

	@Override
	public Integer appReplyComments(String content, Integer goodsId, Integer orderId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("content", content);
		map.put("goodsId", goodsId);
		map.put("orderId", orderId);
		map.put("replyTime", new Date());
		return goodsAppraisesMapper.appReplyComments(map);
	}

	@Override
	@Transactional
	public boolean insertAdd(GoodsAppraises goodsAppraises) {
		Integer insert = goodsAppraisesMapper.insert(goodsAppraises);
		Goods goods = goodsMapper.selectById(goodsAppraises.getGoodsId());
		goods.setAppraiseNum(goods.getAppraiseNum() + 1);
		Integer updateById = goodsMapper.updateById(goods);

		// 修改订单评价状态
		SktOrders sktOrders = sktOrdersMapper.selectById(goodsAppraises.getOrderId());
		sktOrders.setOrderId(goodsAppraises.getOrderId());
		sktOrders.setIsAppraise(1);
		Integer updateById2 = sktOrdersMapper.updateById(sktOrders);

		// 修改店铺评价表
		SktShopScores sktShopScores2 = new SktShopScores();
		sktShopScores2.setShopId(goodsAppraises.getShopId());
		SktShopScores sktShopScores = sktShopScoresMapper.selectOne(sktShopScores2);
		Integer total = goodsAppraises.getGoodsScore() + goodsAppraises.getServiceScore()
				+ goodsAppraises.getTimeScore() + sktShopScores.getTotalScore();
		sktShopScores.setTotalScore(total);
		sktShopScores.setTotalUsers(sktShopScores.getTotalUsers() + 1);
		sktShopScores.setGoodsScore(sktShopScores.getGoodsScore() + goodsAppraises.getGoodsScore());
		sktShopScores.setGoodsUsers(sktShopScores.getGoodsUsers() + 1);
		sktShopScores.setTimeScore(sktShopScores.getTimeScore() + goodsAppraises.getTimeScore());
		sktShopScores.setServiceUsers(sktShopScores.getServiceUsers() + 1);
		sktShopScores.setServiceScore(sktShopScores.getServiceScore() + goodsAppraises.getServiceScore());
		Integer updateById3 = sktShopScoresMapper.updateById(sktShopScores);

		if (insert == 0 || updateById == 0 || updateById2 == 0 || updateById3 == 0) {
			return false;
		}
		return true;
	}

}
