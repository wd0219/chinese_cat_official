package com.stylefeng.guns.modular.huamao.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Attributes;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesDTO;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesShow;
import com.stylefeng.guns.modular.huamao.model.GoodsAttributes;
import com.stylefeng.guns.modular.huamao.model.GoodsCatsLevel;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;
import com.stylefeng.guns.modular.huamao.model.GoodsList;
import com.stylefeng.guns.modular.huamao.model.GoodsSellStock;
import com.stylefeng.guns.modular.huamao.model.GoodsSpecs;
import com.stylefeng.guns.modular.huamao.model.SpecCats;
import com.stylefeng.guns.modular.huamao.model.SpecItems;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IGoodsService extends IService<Goods> {
	List<Map<String,Object>> selectByConditions(GoodsCustm goodsCustm);
	List<Goods> selectGoodsByGoodsCatId(int id);
	int batchUpdate(List<Map<String, Object>> list);
	int batchUpdatePo(List<Goods> list);
	public int selectisSaleCount();
	public int selectGoodsStatusCount();
	public int selectisSaleTotal();
	public int selectGoodsStatusTotal();
	/**
	 * 是否热销
	 * @return
	 */
	public List<Map<String,Object>> TodayHotSale();
	/**
	 * 根据品牌查询所有商品
	 * @return
	 */
	public List<Map<String,Object>> selectTodayHotSaleAll(Integer bandIds);
	/**
	 * 通过分类查询商品，商标信息
	 * @param item
	 * @return
	 */
	public List<Map<String, Object>> selectByCatName(List<String> item);
	/**
	 * 通过商品id查询商品分类级别
	 * @param goodsId
	 * @return
	 */
	public GoodsCatsLevel selectCatsLevelByGoodsid(Integer goodsId);
	/**
	 * 查询今日推荐商品
	 * @return
	 */
	public List<Map<String, Object>> todayHotSaleGoods(Map<String, Object> map);
	/**
	 * 是否库存 上架
	 * @param goodsSellStock
	 * @return
	 */
	public List<GoodsSellStock> selectByIsSale(GoodsSellStock goodsSellStock);
	/**
	 * 是否审核 违规
	 * @param goodsSellStock
	 * @return
	 */
	public List<GoodsSellStock> selectByIsGoodsStatus(GoodsSellStock goodsSellStock);
	
	/**
	 * 是或否预警
	 * @param goodsSellStock
	 * @return
	 */
	public List<GoodsSellStock> selectByIsWarnStock(GoodsSellStock goodsSellStock);
	/**
	 * 查询评论
	 * @param shopId
	 * @param shopCatId2 
	 * @param shopCatId1 
	 * @return
	 */
	public List<GoodsAppraisesDTO> selectByAppraises(Integer shopId, Integer shopCatId1, Integer shopCatId2);
	/**
	 * 更新商品状态
	 * @param goodId
	 * @param isSale
	 * @param isBest
	 * @param isHot
	 * @param isNew
	 * @param isRecom
	 * @param isDelete
	 * @return
	 */
	public boolean updateStatus(Integer[] goodId, Integer isSale, Integer isBest, Integer isHot, Integer isNew,
			Integer isRecom, Integer isDelete);
	/**
	 * 查询默认属性
	 * @param defaultProperty
	 * @return
	 */
	public List<Map<String, Object>> selectDefaultProperty(String[] defaultProperty);
	/**
	 * 首页查询商品
	 * @param goods
	 * @return
	 */
	public List<Map<String,Object>> selectByGoods(GoodsList goods);
	/**
	 * 添加商品信息
	 * @param goods
	 * @return
	 */
	public Integer insertAdd(Goods goods);
	/**
	 * 查询商品属性
	 * @param goods
	 * @return
	 */
	public List<Attributes> selectGoodsAttr(GoodsList goods);
	/**
	 * 查询商品分类
	 * @param goods
	 * @return
	 */
	public List<SpecCats> selectGoodsCat(GoodsList goods);
	/**
	 * 添加商品、属性、规格
	 * @param goods
	 * @param specItemss
	 * @param goodsSpecss
	 * @param goodsAttributess
	 * @return
	 */
	boolean addAttrCat(Goods goods, List<SpecItems> specItemss, List<GoodsSpecs> goodsSpecss,
			List<GoodsAttributes> goodsAttributess);

	/**
	 * 查找货号\编号
	 * @return
	 */
	public BigDecimal selectMax();
	/**
	 * 根据最后一级分类获得品牌
	 */
	public List<Map<String,Object>> getCatBrands(Integer catId);
	/**
	 * 添加商品、属性、规格
	 * @param goodsId 
	 * @param specItemss
	 * @param goodsSpecss
	 * @param goodsAttributess
	 * @return
	 */
	public boolean addAttrCat2(Integer goodsId, List<SpecItems> specItems, List<GoodsSpecs> goodsSpecs, List<GoodsAttributes> goodsAttributes);
	/**
	 * 修改商品属性、规格
	 * @param specItems
	 * @param goodsSpecs
	 * @param goodsAttributes
	 * @return
	 */
	public boolean updateAttrCat(List<SpecItems> specItems, List<GoodsSpecs> goodsSpecs,
			List<GoodsAttributes> goodsAttributes);
	/**
	 * 删除商品
	 * @param goodsId
	 * @return
	 */
	public boolean deleteGoods(Integer goodsId);
	/**
	 * 根据商品id查询信息回显
	 * @param goodsId
	 * @return
	 */
	public List<Map<String, Object>> selectByGoodsId(Integer goodsId);
	/**
	 * 根据shopId，goodsId商品属性回显
	 * @param goodsId
	 * @param shopId
	 * @return
	 */
	List<Map<String, Object>> selectAttrShow(Integer goodsId, Integer shopId);
	/**
	 * 根据shopId，goodsId商品规格回显
	 * @param goodsId
	 * @param shopId
	 * @return
	 */
	List<Map<String, Object>> selectSpecShow(Integer goodsId, Integer shopId);
	/**
	 * 根据shopId，goodsId商品分类回显
	 * @param goodsId
	 * @param shopId
	 * @return
	 */
	List<Map<String, Object>> selectCatShow(Integer goodsId, Integer shopId);

	public List<Map<String, Object>> selectAppByOrder(Map<String,Object> map);
	public List<Goods> selectByShopId(Integer shopId);
	/**
	 * 批量删除
	 */
	public boolean deleteBathGoods(Integer[] goodsId);
	/**
	 * 
	 * @param goodsId
	 * @return
	 */
	Map<String, Object> selectGoodsShopCat(Integer goodsId);
	
	boolean deleteAttrCat_two(Integer goodsId);
	
	/**
	 * 根据商品id查询评价
	 * @param goodsId
	 * @return
	 */
	public List<GoodsAppraisesShow> selectGoodsAppraises(Integer goodsId);
	public List<Map<String,Object>> selectRecomGoods(Map<String,Object> map);
	/**
	 * 根据商品id查询评分
	 * @param goodsId
	 * @return
	 */
	public Map<String, Object> selectGoodsScores(Integer goodsId);
	/**
	 * 根据店铺id查询 热销商品
	 * @param shopId
	 * @return
	 */
	public List<Map<String, Object>> selectGoodsHots(Integer shopId);
	/**
	 * 猜你喜欢商品
	 * @param shopId
	 * @return
	 */
	public List<Goods> guessLikeGoods(Integer shopId);

    Set<Integer> appFindShopsByGoodsId(List<Integer> goodsIds);
    /**
     * 查询日子下拉列表
     * @param gType
     * @return
     */
	public List<Map<String, Object>> selectDownSelect(String gType);
	/**
	 * 自营店
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> selectRecomGoodsSelf(Map<String, Object> map);

	public List<Map<String, Object>>  goodsShopSort (Map<String, Object> map);
	
	/**
	 * 新增商品规格
	 * @param shopId
	 * @param goodsId
	 * @param specItems
	 * @return
	 */
	List<Map<String, Object>> addGoodsCat(Integer shopId, Integer goodsId, String specItems);

	public List<Goods> selectGoodsByMap(Map<String, Object> map);
	Map<String,Object> checkGoodsAndShop(Integer goodsId);
}
