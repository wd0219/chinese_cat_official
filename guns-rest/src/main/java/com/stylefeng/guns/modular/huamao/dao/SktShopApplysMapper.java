package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商城商家开店申请表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-10
 */
public interface SktShopApplysMapper extends BaseMapper<SktShopApplys> {
	public List<MyDTO> shopApplysfindinfo(MyDTO myDto);
	
	public List<MyDTO> shopApplysfindup(MyDTO myDto);
	
	public void shopApplyupdate(MyDTO myDto);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();

    SktShopApplys selectShopApplysByUserId(Integer userId);

    SktShopApplys selectShopApplysByUserId2(Integer userId);

    List<SktShopApplys> selectShopApplysByUserId3(Map<String,Object> map);
}
