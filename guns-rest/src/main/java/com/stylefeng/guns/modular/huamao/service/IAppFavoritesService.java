package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktFavorites;

import java.util.List;
import java.util.Map;

public interface IAppFavoritesService extends IService<SktFavorites> {
    List<Map<String,Object>> selectFavoritesByUserId(Map<String, Object> map);

    List<Map<String,Object>> selectFavoritesAndShopsByUserId(Map<String, Object> map);

    int selectCountByUserId(SktFavorites map);

    boolean insertAndFindId(SktFavorites sktFavorites);

    SktFavorites selectListByEntity(SktFavorites sktFavorites);

    boolean deleteByUser(SktFavorites sktFavorites);
}
