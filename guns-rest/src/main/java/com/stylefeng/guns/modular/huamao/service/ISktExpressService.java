package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktExpress;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 快递表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface ISktExpressService extends IService<SktExpress> {
    /**
     * 查询所有快递公司
     * @return
     */
    public List<Map<String,Object>> selectExpressAll();

    List<SktExpress> appFindExpress();
}
