package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 待发开元宝营业额流水表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
@TableName("skt_log_kaiyuan_freeze")
public class LogKaiyuanFreeze extends Model<LogKaiyuanFreeze> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 开元宝类型 1线上营业额获得 2线下营业额获得 31转正式开元宝营业额账户支出 33购买库存积分支出 34购买库存积分的手续费和税率支出
     */
    private Integer type;
    @TableField(exist=false)
    private String stype;
    /**
     * 发起用户ID 0为平台
     */
    private Integer fromId;
    /**
     * 目标用户ID 0为平台
     */
    private Integer userId;
    /**
     * 对应订单号
     */
    private String orderNo;
    /**
     * 操作前的金额
     */
    private BigDecimal preKaiyuan;
    /**
     * 流水标志 -1减少 1增加
     */
    private Integer kaiyuanType;
    /**
     * 金额
     */
    private BigDecimal kaiyuan;
    @TableField(exist=false)
    private String skaiyuan;
    /**
     * 备注
     */
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreKaiyuan() {
        return preKaiyuan;
    }

    public void setPreKaiyuan(BigDecimal preKaiyuan) {
        this.preKaiyuan = preKaiyuan;
    }

    public Integer getKaiyuanType() {
        return kaiyuanType;
    }

    public void setKaiyuanType(Integer kaiyuanType) {
        this.kaiyuanType = kaiyuanType;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public String getSkaiyuan() {
		return skaiyuan;
	}

	public void setSkaiyuan(String skaiyuan) {
		this.skaiyuan = skaiyuan;
	}
	
	public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	@Override
    protected Serializable pkVal() {
        return this.id;
    }
    
    @Override
    public String toString() {
        return "LogKaiyuanFreeze{" +
        "id=" + id +
        ", type=" + type +
        ", fromId=" + fromId +
        ", userId=" + userId +
        ", orderNo=" + orderNo +
        ", preKaiyuan=" + preKaiyuan +
        ", kaiyuanType=" + kaiyuanType +
        ", kaiyuan=" + kaiyuan +
        ", remark=" + remark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
