package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.*;

@CrossOrigin
@SuppressWarnings("all")
@Controller
@RequestMapping("/app")
public class AppOrderController extends BaseController {
    @Autowired
    private IAppOrderService iAppOrderService;
    @Autowired
    private IAppShopsService iAppShopsService;
    @Autowired
    private IAppOrderGoodsService iAppOrderGoodsService;
    @Autowired
    private IAppAccountService iAppAccountService;
    @Autowired
    private IAppGoodsService iAppGoodsService;
    @Autowired
    private IAppGoodsSpecsService iAppGoodsSpecsService;
    @Autowired
    private IAppAccountShopService iAppAccountShopService;
    @Autowired
    private IAppMessageService iAppMessageService;
    @Autowired
    private IAppOrdersExtensionService iAppOrdersExtensionService;
    @Autowired
    private IAppLogOrdersService iAppLogOrdersService;
    @Autowired
    private IAppOrderRefundsService iAppOrderRefundsService;
    @Autowired
    private IAppSktShopScoresService iAppSktShopScoresService;
    @Autowired
    private IAppGoodsScoresService iAppGoodsScoresService;
    @Autowired
    private IAppGoodsAppraisesService iAppGoodsAppraisesService;
    @Autowired
    private IAppSktExpressService iAppSktExpressService;
    @Autowired
    private IAppOrderStoresService iAppOrderStoresService;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private IGoodsService goodsService;
    @Autowired
    private IAccountService accountService;

    /**
     * 我的订单(线上)
     * @param pageIndex
     * @return
     */
    @RequestMapping("/orders/allOrder")
    @ResponseBody
    @Transactional
    public AppResult allOrder(@ModelAttribute("pageIndex")String pageIndex){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<Object, Object> map = new HashMap<>();
            if (pageIndex.equals("")){
                map.put("pageIndex",(1-1)*50);
            }else {
                map.put("pageIndex",(Integer.parseInt(pageIndex)-1)*50);
            }
            map.put("pageNum",50);
            map.put("userId",user.getUserId());
            // 订单状态:-3:用户拒收 -2:未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
            List<Map<String,Object>> orderList=iAppOrderService.selectOrderAndOrderRefundsByUserId(map);
            List<Object> list = new ArrayList<>();
            for (Map<String,Object> maps:orderList) {

                //boolean 用来判断订单状态是否是未付款，如果是true否则false
                Boolean b = false;

                //payKaiyuan 一个订单总的支付华宝
                BigDecimal payKaiyuan = new BigDecimal(0);
                //payShopping 一个订单总的支付优惠券
                BigDecimal payShopping = new BigDecimal(0);
                String orderStatus = maps.get("orderStatus").toString();
                if("-2".equals(orderStatus)){
                    b = true;
                }
                SktShops sktShops=iAppShopsService.selectShopsByShopId(maps.get("shopId").toString()!=null?Integer.parseInt(maps.get("shopId").toString()):0);
                maps.put("catName",sktShops.getShopName());
                maps.put("telephone",sktShops.getTelephone());
                maps.put("orderStatus", RemarksGenerate.orderLineType(Integer.parseInt(orderStatus)));
                maps.put("orderStatus2",orderStatus);
                maps.put("countGoodsNum",0);
                List<Map<String,Object>> sktOrderGoodsList=iAppOrderGoodsService.selectOrderGoodsByOrderId(maps.get("orderId").toString()!=null?Integer.parseInt(maps.get("orderId").toString()):0);
                List<Object> objects = new ArrayList<>();
                CommonModel commonModel = new CommonModel();
                if (sktOrderGoodsList.size()>0) {
                    int goodsNum =0;
                    for (Map<String, Object> sktOrderGoodsMap : sktOrderGoodsList) {
                        sktOrderGoodsMap.put("goodsImg",commonModel.getOSS_DOM()+sktOrderGoodsMap.get("goodsImg").toString());
                        sktOrderGoodsMap.put("refundOtherReson",maps.get("refundOtherReson"));
                        sktOrderGoodsMap.put("goodsSpecNames",maps.get("goodsSpecNames")!=null?maps.get("goodsSpecNames"):"");
                        goodsNum += Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        maps.put("countGoodsNum",goodsNum);
                        Integer goodsId = (Integer)sktOrderGoodsMap.get("goodsId");
                        Goods goods = goodsService.selectById(goodsId);
                        sktOrderGoodsMap.put("payKaiyuan",goods.getPayKaiyuan().multiply(new BigDecimal(goodsNum)));
                        payKaiyuan = payKaiyuan.add(goods.getPayKaiyuan().multiply(new BigDecimal(goodsNum)));
                        sktOrderGoodsMap.put("payShopping",goods.getPayShopping().multiply(new BigDecimal(goodsNum)));
                        payShopping = payShopping.add(goods.getPayShopping().multiply(new BigDecimal(goodsNum)));
                        objects.add(sktOrderGoodsMap);
                    }
                }else {
                    objects.add(sktOrderGoodsList);
                }
                if (Integer.parseInt(orderStatus)==-3){
                    maps.put("refundOtherReson",RemarksGenerate.refundStatus(Integer.parseInt(maps.get("refundStatus").toString())));
                    if (MSGUtil.isBlank(maps.get("refundOtherReson"))){
                        maps.put("refundOtherReson","");
                    }
                }
                maps.put("goods",objects);
                if(b){
                    maps.put("payKaiyuan",payKaiyuan);
                }
                list.add(maps);
            }
            return AppResult.OK("全部订单",list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("获取失败",-1);
}
    }


/**
 *
 *待付款
 * @param pageIndex
 * @return
 */
@RequestMapping("/orders/pendingPayment")
@ResponseBody
@Transactional
    public AppResult pendingPayment(@ModelAttribute("pageIndex")String  pageIndex){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<Object, Object> map = new HashMap<>();
            if (pageIndex.equals("")){
                map.put("pageIndex",(1-1)*1);
            }else {
                map.put("pageIndex",(Integer.parseInt(pageIndex)-1)*1);
            }
            int status[]={-2};
            map.put("pageNum",10);
            map.put("orderStatus",status);
            map.put("isPay",0);
            map.put("userId",user.getUserId());
            // 订单状态:-3:用户拒收 -2:未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
            List<Map<String,Object>> orderList=iAppOrderService.selectOrderAndOrderRefundsByUserId(map);
            List<Object> list = new ArrayList<>();
            for (Map<String,Object> maps:orderList) {
                String orderStatus = maps.get("orderStatus").toString();
                SktShops sktShops=iAppShopsService.selectShopsByShopId(maps.get("shopId").toString()!=null?Integer.parseInt(maps.get("shopId").toString()):0);
                maps.put("catName",sktShops.getShopName());
                maps.put("telephone",sktShops.getTelephone());
                maps.put("orderStatus", RemarksGenerate.orderLineType(Integer.parseInt(orderStatus)));
                maps.put("orderStatus2",orderStatus);
                maps.put("countGoodsNum",0);
                List<Map<String,Object>> sktOrderGoodsList=iAppOrderGoodsService.selectOrderGoodsByOrderId(maps.get("orderId").toString()!=null?Integer.parseInt(maps.get("orderId").toString()):0);
                List<Object> objects = new ArrayList<>();
                CommonModel commonModel = new CommonModel();
                if (sktOrderGoodsList.size()>0) {
                    int goodsNum =0;
                    for (Map<String, Object> sktOrderGoodsMap : sktOrderGoodsList) {
                        sktOrderGoodsMap.put("goodsImg",commonModel.getOSS_DOM()+sktOrderGoodsMap.get("goodsImg").toString());
                        sktOrderGoodsMap.put("goodsSpecNames",maps.get("goodsSpecNames")!=null?maps.get("goodsSpecNames"):"");
                        goodsNum += Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        maps.put("countGoodsNum",goodsNum);
                        objects.add(sktOrderGoodsMap);
                    }
                }
                maps.put("goods",objects);
                list.add(maps);
            }
            return AppResult.OK("代付款",list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败",-1);
        }
    }

    /**
         * 待收货
     * @param pageIndex
     * @return
     */
    @RequestMapping("/orders/receiptOfGoods")
    @ResponseBody
    @Transactional
    public AppResult receiptOfGoods(@ModelAttribute("pageIndex")String  pageIndex){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<Object, Object> map = new HashMap<>();
            if (pageIndex.equals("")){
                map.put("pageIndex",(1-1)*50);
            }else {
                map.put("pageIndex",(Integer.parseInt(pageIndex)-1)*50);
            }
            int status[]={1};
            map.put("pageNum",50);
            map.put("orderStatus",status);
            map.put("isPay",1);
            map.put("userId",user.getUserId());
            // 订单状态:-3:用户拒收 -2:未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
            List<Map<String,Object>> orderList=iAppOrderService.selectOrderAndOrderRefundsByUserId(map);
            List<Object> list = new ArrayList<>();
            for (Map<String,Object> maps:orderList) {
                String orderStatus = maps.get("orderStatus").toString();
                SktShops sktShops=iAppShopsService.selectShopsByShopId(maps.get("shopId").toString()!=null?Integer.parseInt(maps.get("shopId").toString()):0);
                maps.put("catName",sktShops.getShopName());
                maps.put("telephone",sktShops.getTelephone());
                maps.put("orderStatus", RemarksGenerate.orderLineType(Integer.parseInt(orderStatus)));
                maps.put("orderStatus2",orderStatus);
                maps.put("countGoodsNum",0);
                List<Map<String,Object>> sktOrderGoodsList=iAppOrderGoodsService.selectOrderGoodsByOrderId(maps.get("orderId").toString()!=null?Integer.parseInt(maps.get("orderId").toString()):0);
                List<Object> objects = new ArrayList<>();
                CommonModel commonModel = new CommonModel();
                if (sktOrderGoodsList.size()>0) {
                    int goodsNum =0;
                    for (Map<String, Object> sktOrderGoodsMap : sktOrderGoodsList) {
                        sktOrderGoodsMap.put("goodsImg",commonModel.getOSS_DOM()+sktOrderGoodsMap.get("goodsImg").toString());
                        sktOrderGoodsMap.put("goodsSpecNames",maps.get("goodsSpecNames")!=null?maps.get("goodsSpecNames"):"");
                        goodsNum += Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        maps.put("countGoodsNum",goodsNum);
                        objects.add(sktOrderGoodsMap);
                    }
                }
                maps.put("goods",objects);
                list.add(maps);
            }
            return AppResult.OK("待收货",list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败",-1);
        }
    }

    /**
     * 确认收货
     * @param orderId
     * @return
     */
    @RequestMapping("/orders/confirmReceipt")
    @ResponseBody
    @Transactional
    public AppResult confirmReceipt(@ModelAttribute("orderId")String orderId){
        try{
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            if(user==null){
                  return AppResult.ERROR("您未登录",-2);
             }


            Map<String, Object> map = new HashMap<>();
            map.put("userId",user.getUserId());
            map.put("orderId",Integer.parseInt(orderId));
            map.put("orderStatus",1);
            Map<String,Object> ordersReceive=iAppOrderService.selectReceiveOrders(map);
            Account account = iAppAccountService.selectAccountByUserId(user.getUserId());
            if(ordersReceive!=null){
                List<Integer> list=new ArrayList<>();
                list.add(Integer.parseInt(orderId));
                boolean b = accountService.enter(list);
                if (b==true){
                    return AppResult.OK("操作成功",null);
                }else{
                    return AppResult.OK("操作失败",null);
                }
            }else {
                return AppResult.ERROR("订单信息不存在",-1);
            }
//            if (MSGUtil.isBlank(ordersReceive)) {

//                //修改orders信息
//                SktOrders sktOrders = new SktOrders();
//                sktOrders.setOrderStatus(2);
//                sktOrders.setReceiveTime(new Date());
//                sktOrders.setOrderId(Integer.parseInt(orderId));
//                boolean b = iAppOrderService.insertOrUpdate(sktOrders);
//                //修改商品销量
//                List<Map<String, Object>> goodsInfo = iAppOrderGoodsService.selectOrderGoodsByOrderId(Integer.parseInt(orderId));
//                for (Map<String,Object> goodsMap:goodsInfo) {
//                    map.put("goodsNum",goodsMap.get("goodsNum"));
//                    map.put("goodsId",goodsMap.get("goodsId"));
//                    iAppGoodsService.updateGoodsSaleNum(map);
//                    if (Integer.parseInt(goodsMap.get("goodsSpecId").toString())>1){
//                        map.put("goodsSpecId",goodsMap.get("goodsSpecId").toString());
//                        map.put("goodsNum",goodsMap.get("goodsNum"));
//                        iAppGoodsSpecsService.updateGoodsSpecsById(map);
//                    }
//                }
//                //修改拓展 如果积分库存标识为1 则库存积分足够  应收货时间和结算时间相同，标识为2 则库存积分不足，收货7工作日后结算
//                Date receiveDate = new Date();
//                Map<String, Object> maps = new HashMap<>();
//                maps.put("autoTakeTime",receiveDate);
//                maps.put("isAutoTake",2);//设为已手动收货
//                maps.put("orderId",Integer.parseInt(orderId));
//                if (Integer.parseInt(ordersReceive.get("scoreFlag").toString())==1){
//                    maps.put("settleTime",receiveDate);
//                    maps.put("shopUserId",ordersReceive.get("shopUserId"));
//                    maps.put("shopId",ordersReceive.get("shopId"));
//                    maps.put("score",ordersReceive.get("score"));
//                    //商家扣除冻结积分
//                    iAppAccountShopService.updateAccountShopsByUserId(maps);
//                }
//                if (Integer.parseInt(ordersReceive.get("scoreFlag").toString())==2){
//                    maps.put("settleTime", GetDateUtil.getNextDay(receiveDate,7));
//                }
//                iAppOrdersExtensionService.updateOrdersExtensionByOrderId(maps);
//                //新增订单日志
//                LogOrders logOrders = new LogOrders();
//                logOrders.setOrderId(Integer.parseInt(orderId));
//                logOrders.setLogContent("用户已收货");
//                logOrders.setOrderStatus(2);
//                logOrders.setLogUserId(user.getUserId());
//                logOrders.setLogType(0);
//                logOrders.setLogTime(new Date());
//                iAppLogOrdersService.insert(logOrders);
//                //发送一条商家信息
//                Messages messages = new Messages();
//                messages.setMsgType(1);
//                messages.setCreateTime(new Date());
//                messages.setSendUserId(1);
//                messages.setMsgStatus(0);
//                messages.setDataFlag(1);
//                messages.setReceiveUserId(Integer.parseInt(ordersReceive.get("shopUserId").toString()));
//                messages.setMsgContent("您的订单【"+ordersReceive.get("orderNo")+"");
//                Map<Object, Object> msgJson = new HashMap<>();
//                msgJson.put("from",1);
//                msgJson.put("dataId",orderId);
//                messages.setMsgJson(msgJson.toString());
//                boolean insert = iAppMessageService.insert(messages);
//                if (insert){
//                    return AppResult.OK("操作成功",null);
//                }
//            }else {
//                return AppResult.ERROR("订单信息不存在",-1);
//            }
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败",-1);
        }
    //    return AppResult.ERROR("操作失败，请检查订单状态是否已改变",-1);
    }


    /**
     * 待发货
     * @param pageIndex
     * @return
     */
    @RequestMapping("/orders/toBeShipped")
    @ResponseBody
    @Transactional
    public AppResult toBeShipped(@ModelAttribute("pageIndex")String  pageIndex){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<Object, Object> map = new HashMap<>();
            if (pageIndex.equals("")){
                map.put("pageIndex",(1-1)*50);
            }else {
                map.put("pageIndex",(Integer.parseInt(pageIndex)-1)*50);
            }
            int status[]={0};
            map.put("pageNum",50);
            map.put("orderStatus",status);
            map.put("isPay",1);
            map.put("userId",user.getUserId());
            // 订单状态:-3:用户拒收 -2:未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
            List<Map<String,Object>> orderList=iAppOrderService.selectOrderAndOrderRefundsByUserId(map);
            List<Object> list = new ArrayList<>();
            for (Map<String,Object> maps:orderList) {
                String orderStatus = maps.get("orderStatus").toString();
                SktShops sktShops=iAppShopsService.selectShopsByShopId(maps.get("shopId").toString()!=null?Integer.parseInt(maps.get("shopId").toString()):0);
                maps.put("catName",sktShops.getShopName());
                maps.put("telephone",sktShops.getTelephone());
                maps.put("orderStatus", RemarksGenerate.orderLineType(Integer.parseInt(orderStatus)));
                maps.put("orderStatus2",orderStatus);
                maps.put("countGoodsNum",0);
                List<Map<String,Object>> sktOrderGoodsList=iAppOrderGoodsService.selectOrderGoodsByOrderId(maps.get("orderId").toString()!=null?Integer.parseInt(maps.get("orderId").toString()):0);
                List<Object> objects = new ArrayList<>();
                CommonModel commonModel = new CommonModel();
                if (sktOrderGoodsList.size()>0) {
                    int goodsNum =0;
                    for (Map<String, Object> sktOrderGoodsMap : sktOrderGoodsList) {
                        sktOrderGoodsMap.put("goodsImg",commonModel.getOSS_DOM()+sktOrderGoodsMap.get("goodsImg").toString());
                        sktOrderGoodsMap.put("goodsSpecNames",maps.get("goodsSpecNames")!=null?maps.get("goodsSpecNames"):"");
                        goodsNum += Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        maps.put("countGoodsNum",goodsNum);
                        objects.add(sktOrderGoodsMap);
                    }
                }
                maps.put("goods",objects);
                list.add(maps);
            }
            return AppResult.OK("待发货",list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败",-1);
        }
    }
    /**
     * 待确认订单
     * @param pageIndex
     * @return
     */
    @RequestMapping("/orders/toBeEnter")
    @ResponseBody
    @Transactional
    public AppResult toBeEnter(@ModelAttribute("pageIndex")String  pageIndex){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<Object, Object> map = new HashMap<>();
            if (pageIndex.equals("")){
                map.put("pageIndex",(1-1)*50);
            }else {
                map.put("pageIndex",(Integer.parseInt(pageIndex)-1)*50);
            }
            int status[]={4};
            map.put("pageNum",50);
            map.put("orderStatus",status);
            map.put("isPay",1);
            map.put("userId",user.getUserId());
            // 订单状态:-3:用户拒收 -2:未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
            List<Map<String,Object>> orderList=iAppOrderService.selectOrderAndOrderRefundsByUserId(map);
            List<Object> list = new ArrayList<>();
            for (Map<String,Object> maps:orderList) {
                String orderStatus = maps.get("orderStatus").toString();
                SktShops sktShops=iAppShopsService.selectShopsByShopId(maps.get("shopId").toString()!=null?Integer.parseInt(maps.get("shopId").toString()):0);
                maps.put("catName",sktShops.getShopName());
                maps.put("telephone",sktShops.getTelephone());
                maps.put("orderStatus", RemarksGenerate.orderLineType(Integer.parseInt(orderStatus)));
                maps.put("orderStatus2",orderStatus);
                maps.put("countGoodsNum",0);
                List<Map<String,Object>> sktOrderGoodsList=iAppOrderGoodsService.selectOrderGoodsByOrderId(maps.get("orderId").toString()!=null?Integer.parseInt(maps.get("orderId").toString()):0);
                List<Object> objects = new ArrayList<>();
                CommonModel commonModel = new CommonModel();
                if (sktOrderGoodsList.size()>0) {
                    int goodsNum =0;
                    for (Map<String, Object> sktOrderGoodsMap : sktOrderGoodsList) {
                        sktOrderGoodsMap.put("goodsImg",commonModel.getOSS_DOM()+sktOrderGoodsMap.get("goodsImg").toString());
                        sktOrderGoodsMap.put("goodsSpecNames",maps.get("goodsSpecNames")!=null?maps.get("goodsSpecNames"):"");
                        goodsNum += Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        maps.put("countGoodsNum",goodsNum);
                        objects.add(sktOrderGoodsMap);
                    }
                }
                maps.put("goods",objects);
                list.add(maps);
            }
            return AppResult.OK("待确认",list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败",-1);
        }
    }

    /**
     * 待确认订单
     * @param pageIndex
     * @return
     */
    @RequestMapping("/orders/toBeNotEnter")
    @ResponseBody
    @Transactional
    public AppResult toBeNotEnter(@ModelAttribute("pageIndex")String  pageIndex){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<Object, Object> map = new HashMap<>();
            if (pageIndex.equals("")){
                map.put("pageIndex",(1-1)*50);
            }else {
                map.put("pageIndex",(Integer.parseInt(pageIndex)-1)*50);
            }
            int status[]={-4};
            map.put("pageNum",50);
            map.put("orderStatus",status);
            map.put("isPay",1);
            map.put("userId",user.getUserId());
            // 订单状态:-3:用户拒收 -2:未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
            List<Map<String,Object>> orderList=iAppOrderService.selectOrderAndOrderRefundsByUserId(map);
            List<Object> list = new ArrayList<>();
            for (Map<String,Object> maps:orderList) {
                String orderStatus = maps.get("orderStatus").toString();
                SktShops sktShops=iAppShopsService.selectShopsByShopId(maps.get("shopId").toString()!=null?Integer.parseInt(maps.get("shopId").toString()):0);
                maps.put("catName",sktShops.getShopName());
                maps.put("telephone",sktShops.getTelephone());
                maps.put("orderStatus", RemarksGenerate.orderLineType(Integer.parseInt(orderStatus)));
                maps.put("orderStatus2",orderStatus);
                maps.put("countGoodsNum",0);
                List<Map<String,Object>> sktOrderGoodsList=iAppOrderGoodsService.selectOrderGoodsByOrderId(maps.get("orderId").toString()!=null?Integer.parseInt(maps.get("orderId").toString()):0);
                List<Object> objects = new ArrayList<>();
                CommonModel commonModel = new CommonModel();
                if (sktOrderGoodsList.size()>0) {
                    int goodsNum =0;
                    for (Map<String, Object> sktOrderGoodsMap : sktOrderGoodsList) {
                        sktOrderGoodsMap.put("goodsImg",commonModel.getOSS_DOM()+sktOrderGoodsMap.get("goodsImg").toString());
                        sktOrderGoodsMap.put("goodsSpecNames",maps.get("goodsSpecNames")!=null?maps.get("goodsSpecNames"):"");
                        goodsNum += Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        maps.put("countGoodsNum",goodsNum);
                        objects.add(sktOrderGoodsMap);
                    }
                }
                maps.put("goods",objects);
                list.add(maps);
            }
            return AppResult.OK("待确认",list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败",-1);
        }
    }
    /**
     * 退款
     * @param orderNo
     * @param backMoney
     * @param expressId
     * @param expressNo
     * @param refundOtherReson
     * @return
     */
    @RequestMapping("/{orders}/applyRefund")
    @ResponseBody
    @Transactional
    public AppResult applyRefund(@ModelAttribute("orderNo")String orderNo,@ModelAttribute("backMoney")String backMoney,@ModelAttribute("expressId")String expressId,
                                  @ModelAttribute("expressNo")String expressNo,@ModelAttribute("refundOtherReson")String refundOtherReson){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            if (MSGUtil.isBlank(orderNo)){
                return AppResult.ERROR("订单号不能为空",-1);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("orderNo",orderNo);
            map.put("userId",user.getUserId());
            map.put("isPay",1);
            SktOrders sktOrders=iAppOrderService.selectOrderByUserId(map);
            if (MSGUtil.isBlank(sktOrders)){
                return AppResult.ERROR("此订单不存在",-1);
            }
            map.put("orderId",sktOrders.getOrderId());
            SktOrderRefunds sktOrderRefunds=iAppOrderRefundsService.selectOrderRefundsByOrderId(map);
            if (!MSGUtil.isBlank(sktOrderRefunds)){
                return AppResult.ERROR("此订单已申请退款不能重复提交",-1);
            }
            SktOrderRefunds orderRefunds = new SktOrderRefunds();
            orderRefunds.setOrderId(sktOrders.getOrderId());//订单ID
            orderRefunds.setRefundTo(user.getUserId());//接收退款用户
            orderRefunds.setRefundReson(10000);
            if(!"".equals(expressId) && expressId != null){
                orderRefunds.setExpressId(Integer.parseInt(expressId));//用户申请退款原因ID
            }//退款快递公司ID
            orderRefunds.setExpressNo(expressNo);//退款快递号
            orderRefunds.setRefundOtherReson(refundOtherReson);//用户申请退款原因
            orderRefunds.setBackMoney(new BigDecimal(backMoney));//用户退款金额
            orderRefunds.setRefundTradeNo(StringUtils.getOrderIdByTime("9"));//管理员退款流水号
            orderRefunds.setRefundRemark("用户申请退款");//退款备注
            orderRefunds.setShopRejectReason("");//店铺不同意拒收原因
            orderRefunds.setRefundStatus(0);//退款状态-1商家不同意 0用户申请退款 1商家同意退款 2平台已经退款
            orderRefunds.setCreateTime(new Date());//用户申请退款时间
            orderRefunds.setRefundTime(new Date());//退款时间(不设置报错)
            //建立订单记录
            LogOrders logOrders = new LogOrders();
            logOrders.setOrderId(sktOrders.getOrderId());
            logOrders.setOrderStatus(-1);
            logOrders.setLogContent("申请退款成功，等待商家审核");
            logOrders.setLogUserId(user.getUserId());
            logOrders.setLogType(0);
            logOrders.setLogTime(new Date());
            //插入订单日志表
            iAppLogOrdersService.insert(logOrders);
            //插入退款记录
            iAppOrderRefundsService.insert(orderRefunds);
            //修改订单状态
            map.put("orderStatus",-3);
            map.put("orderNo",orderNo);
            iAppOrderService.updateOrderStatusByOrderNo(map);
            return AppResult.OK("申请已提交",null);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("申请失败",-1);
        }
    }

    /**
     * 二次退款
     * @param orderNo
     * @param backMoney
     * @param expressId
     * @param expressNo
     * @param refundOtherReson
     * @return
     */
    @RequestMapping("/Orders/applyRefundAgain")
    @ResponseBody
    @Transactional
    public AppResult applyRefundAgain(@ModelAttribute("orderNo")String orderNo,@ModelAttribute("backMoney")String backMoney,@ModelAttribute("expressId")String expressId,
                                      @ModelAttribute("expressNo")String expressNo,@ModelAttribute("refundOtherReson")String refundOtherReson){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            if (MSGUtil.isBlank(orderNo)){
                return AppResult.ERROR("订单号不能为空",-1);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("orderNo",orderNo);
            map.put("userId",user.getUserId());
            map.put("isPay",1);
            SktOrders sktOrders=iAppOrderService.selectOrderByUserId(map);
            if (MSGUtil.isBlank(sktOrders)){
                return AppResult.ERROR("此订单不存在",-1);
            }
            map.put("orderId",sktOrders.getOrderId());
            map.put("refundStatus",-1);
            SktOrderRefunds sktOrderRefunds=iAppOrderRefundsService.selectOrderRefundsByOrderId(map);
            if (!MSGUtil.isBlank(sktOrderRefunds)){
                return AppResult.ERROR("未查询到商家拒绝退款订单",-1);
            }
            //建立订单记录
            LogOrders logOrders = new LogOrders();
            logOrders.setOrderId(sktOrders.getOrderId());
            logOrders.setOrderStatus(-1);
            logOrders.setLogContent("申请退款成功，等待商家审核");
            logOrders.setLogUserId(user.getUserId());
            logOrders.setLogType(0);
            logOrders.setLogTime(new Date());
            //插入订单日志表
            iAppLogOrdersService.insert(logOrders);
            SktOrderRefunds orderRefunds = new SktOrderRefunds();
            orderRefunds.setRefundStatus(0);
            orderRefunds.setExpressId(Integer.parseInt(expressId));
            orderRefunds.setExpressNo(expressNo);
            orderRefunds.setId(sktOrderRefunds.getId());
            iAppOrderRefundsService.updateById(orderRefunds);
            return AppResult.OK("申请已提交",null);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("申请失败",-1);
        }
    }


    /**
     * 待评价
     * @param pageIndex
     * @return
     */
    @RequestMapping("/orders/toBeEvaluated")
    @ResponseBody
    public AppResult toBeEvaluated(@ModelAttribute("pageIndex")String pageIndex){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<Object, Object> map = new HashMap<>();
            if (pageIndex.equals("")){
                map.put("pageIndex",(1-1)*50);
            }else {
                map.put("pageIndex",(Integer.parseInt(pageIndex)-1)*50);
            }
            int status[]={2,3};
            map.put("pageNum",50);
            map.put("orderStatus",status);
            map.put("isPay",1);
            map.put("userId",user.getUserId());
            map.put("isAppraise",0);
            // 订单状态:-3:用户拒收 -2:未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
            List<Map<String,Object>> orderList=iAppOrderService.selectOrderAndOrderRefundsByUserId(map);
            List<Object> list = new ArrayList<>();
            for (Map<String,Object> maps:orderList) {
                String orderStatus = maps.get("orderStatus").toString();
                SktShops sktShops=iAppShopsService.selectShopsByShopId(maps.get("shopId").toString()!=null?Integer.parseInt(maps.get("shopId").toString()):0);
                maps.put("catName",sktShops.getShopName());
                maps.put("telephone",sktShops.getTelephone());
                maps.put("orderStatus", RemarksGenerate.orderLineType(Integer.parseInt(orderStatus)));
                maps.put("orderStatus2",orderStatus);
                maps.put("countGoodsNum",0);
                List<Map<String,Object>> sktOrderGoodsList=iAppOrderGoodsService.selectOrderGoodsByOrderId(maps.get("orderId").toString()!=null?Integer.parseInt(maps.get("orderId").toString()):0);
                List<Object> objects = new ArrayList<>();
                CommonModel commonModel = new CommonModel();
                if (sktOrderGoodsList.size()>0) {
                    int goodsNum =0;
                    for (Map<String, Object> sktOrderGoodsMap : sktOrderGoodsList) {
                        sktOrderGoodsMap.put("goodsImg",commonModel.getOSS_DOM()+sktOrderGoodsMap.get("goodsImg").toString());
                        sktOrderGoodsMap.put("goodsSpecNames",maps.get("goodsSpecNames")!=null?maps.get("goodsSpecNames"):"");
                        goodsNum += Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        maps.put("countGoodsNum",goodsNum);
                        objects.add(sktOrderGoodsMap);
                    }
                }
                maps.put("goods",objects);
                list.add(maps);
            }
            return AppResult.OK("待评价",list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("操作失败",-1);
        }
    }

    /**
     * 提交评价
     * @param orderId
     * @param list
     * @return
     */
    @RequestMapping("/comment/reportComment")
    @ResponseBody
    @Transactional      
    public AppResult reportComment(@ModelAttribute("orderId")String orderId, @ModelAttribute("list")Object list){
        try{
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<String, Object> map2 = new HashMap<>();
            map2.put("userId",user.getUserId());
            map2.put("orderId",orderId);
            GoodsAppraises goodsAppraiseses=iAppGoodsAppraisesService.selectGoodsAppraisesBuUserId(map2);
            if (!MSGUtil.isBlank(goodsAppraiseses)){
                return AppResult.ERROR("评论已存在",-1);
            }
            List<GoodsAppraises> goodsAppraisesList = new ArrayList<>();
            JSONArray jsonArray = JSONArray.fromObject(list);
            for(int i = 0;i<jsonArray.size();i++){
                JSONObject map = jsonArray.getJSONObject(i);
                GoodsAppraises goodsAppraises = new GoodsAppraises();
                goodsAppraises.setUserId(user.getUserId());
                goodsAppraises.setOrderId(Integer.parseInt(orderId));
                goodsAppraises.setGoodsId(Integer.parseInt(map.get("goodsId").toString()));
                Map<String, Object> map1 = new HashMap<>();
                map1.put("goodsId",Integer.parseInt(map.get("goodsId").toString()));
                map1.put("orderId",Integer.parseInt(orderId));
                Map<String,Object> goodsOrder=iAppOrderGoodsService.selectOrderGoodsByGoodsId(map1);
                goodsAppraises.setShopId(goodsOrder.get("shopId")!=null?Integer.parseInt(goodsOrder.get("shopId").toString()):0);
                goodsAppraises.setGoodsSpecId(goodsOrder.get("goodsSpecId")!=null?Integer.parseInt(goodsOrder.get("goodsSpecId").toString()):0);
                goodsAppraises.setGoodsScore(map.get("goodsScore")!=null?Integer.parseInt(map.get("goodsScore").toString()):0);
                goodsAppraises.setServiceScore(Integer.parseInt(map.get("serviceScore").toString()));
                goodsAppraises.setTimeScore(Integer.parseInt(map.get("timeScore").toString()));
                goodsAppraises.setContent(map.get("content").toString());
                goodsAppraises.setImages(map.get("images").toString());
                goodsAppraises.setIsShow(1);
                goodsAppraises.setCreateTime(new Date());
                goodsAppraises.setDataFlag(1);
                goodsAppraisesList.add(goodsAppraises);
                //一个订单中每个商品/店铺对应的品评分累加
                int totalScore=Integer.parseInt(map.get("goodsScore").toString())+Integer.parseInt(map.get("serviceScore").toString())+Integer.parseInt(map.get("timeScore").toString());
                Map<String, Object>  goodsShopScoresMap= new HashMap<>();
                goodsShopScoresMap.put("totalScore",totalScore);
                goodsShopScoresMap.put("totalUsers",1);
                goodsShopScoresMap.put("goodsScore",map.get("goodsScore"));
                goodsShopScoresMap.put("goodsUsers",1);
                goodsShopScoresMap.put("serviceScore",map.get("serviceScore"));
                goodsShopScoresMap.put("serviceUsers",1);
                goodsShopScoresMap.put("timeScore",map.get("timeScore"));
                goodsShopScoresMap.put("timeUsers",1);
                goodsShopScoresMap.put("goodsId",map.get("goodsId"));
                goodsShopScoresMap.put("shopId",map.get("shopId"));
                //一个订单中每个商品/店铺对应的品评分累加
                iAppGoodsScoresService.updateGoodsScores(goodsShopScoresMap);
                iAppSktShopScoresService.updateSktShopScores(goodsShopScoresMap);
                //增加商品评价数
                iAppGoodsService.updateGoodsAppraiseNum(goodsShopScoresMap);
            }
            //插入商品评价内容
            boolean b = iAppGoodsAppraisesService.insertBatch(goodsAppraisesList);
            if (!b){
                return AppResult.ERROR("插入商品评价内容失败",-1);
            }
            //更改订单评价记录
            SktOrders sktOrders = new SktOrders();
            sktOrders.setOrderId(Integer.parseInt(orderId));
            sktOrders.setIsAppraise(1);
            boolean b1 = iAppOrderService.updateById(sktOrders);
            if (!b1){
                return AppResult.ERROR("更改订单评价记录失败",-1);
            }
            return AppResult.OK("评论成功",null);
        }catch (Exception e){
            e.printStackTrace();
            return  AppResult.ERROR("评论失败",-1);
        }
    }

    /**
     * 待退款
     * @param pageIndex
     * @return
     */
    @RequestMapping("/orders/refundOrder")
    @ResponseBody
    @Transactional
    public AppResult refundOrder(@ModelAttribute("pageIndex")String pageIndex){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<Object, Object> map = new HashMap<>();
            if (pageIndex.equals("")){
                map.put("pageIndex",(1-1)*50);
            }else {
                map.put("pageIndex",(Integer.parseInt(pageIndex)-1)*50);
            }
            map.put("pageNum",50);
            map.put("userId",user.getUserId());
            int status []={-3,-1};
            map.put("orderStatus",status);
            // 订单状态:-3:用户拒收 -2:未付款的订单 -1用户取消 0待发货 1配送中 2用户确认收货 3系统确认收货
            List<Map<String,Object>> orderList=iAppOrderService.selectOrderAndOrderRefundsByUserId(map);
            List<Object> list = new ArrayList<>();
            for (Map<String,Object> maps:orderList) {
                String orderStatus = maps.get("orderStatus").toString();
                SktShops sktShops=iAppShopsService.selectShopsByShopId(maps.get("shopId").toString()!=null?Integer.parseInt(maps.get("shopId").toString()):0);
                maps.put("catName",sktShops.getShopName());
                maps.put("telephone",sktShops.getTelephone());
                maps.put("orderStatus", RemarksGenerate.orderLineType(Integer.parseInt(orderStatus)));
                maps.put("orderStatus2",orderStatus);
                maps.put("countGoodsNum",0);
                List<Map<String,Object>> sktOrderGoodsList=iAppOrderGoodsService.selectOrderGoodsByOrderId(maps.get("orderId").toString()!=null?Integer.parseInt(maps.get("orderId").toString()):0);
                List<Object> objects = new ArrayList<>();
                CommonModel commonModel = new CommonModel();
                BigDecimal payShopping=BigDecimal.ZERO;
                if (sktOrderGoodsList.size()>0) {
                    int goodsNum =0;
                    for (Map<String, Object> sktOrderGoodsMap : sktOrderGoodsList) {
                        sktOrderGoodsMap.put("goodsImg",commonModel.getOSS_DOM()+sktOrderGoodsMap.get("goodsImg").toString());
                        sktOrderGoodsMap.put("refundOtherReson",maps.get("refundOtherReson"));
                        sktOrderGoodsMap.put("goodsSpecNames",maps.get("goodsSpecNames")!=null?maps.get("goodsSpecNames"):"");
                        goodsNum += Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        maps.put("countGoodsNum",goodsNum);
                        int goodsNums=Integer.parseInt(sktOrderGoodsMap.get("goodsNum").toString());
                        //查询商品详情
                        Integer goodsId = (Integer)sktOrderGoodsMap.get("goodsId");
                        Goods goods = goodsService.selectById(goodsId);
                        payShopping=payShopping.add(goods.getPayShopping().multiply(new BigDecimal(goodsNum)));
                        objects.add(sktOrderGoodsMap);
                    }
                }
                if (Integer.parseInt(orderStatus)==-3){
                    maps.put("refundOtherReson",RemarksGenerate.refundStatus(Integer.parseInt(maps.get("refundStatus").toString())));
                    if (MSGUtil.isBlank(maps.get("refundOtherReson"))){
                        maps.put("refundOtherReson","");
                    }
                }
                maps.put("goods",objects);
                maps.put("payShopping",payShopping);
                list.add(maps);
            }
            return AppResult.OK("待退款",list);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("获取失败",-1);
        }
    }


    /**
     * 订单详情
     * @param orderNo
     * @return
     */
    @RequestMapping("/Orders/detaiOrder")
    @ResponseBody
    public AppResult detaiOrder(@ModelAttribute("orderNo")String orderNo){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            if (MSGUtil.isBlank(orderNo)){
                return AppResult.ERROR("订单号不能为空",-1);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("userId",user.getUserId());
            map.put("orderNo",orderNo);
            SktOrders sktOrders = iAppOrderService.selectOrderByUserId(map);
            if (MSGUtil.isBlank(sktOrders)){
                return AppResult.ERROR("无此订单号数据",-1);
            }
            SktExpress sktExpress=new SktExpress();
            if (sktOrders.getExpressId()!=null){
               sktExpress= iAppSktExpressService.selectExpressById(sktOrders.getExpressId());
            }
            SktShops sktShops = iAppShopsService.selectShopsByShopId(sktOrders.getShopId());
            List<Map<String, Object>> list = iAppOrderGoodsService.selectOrderGoodsByOrderId(sktOrders.getOrderId());
            List<Object> lists = new ArrayList<>();
            CommonModel commonModel = new CommonModel();
            BigDecimal payShopping=BigDecimal.ZERO;
            for (Map<String,Object> orderGoods:list) {
                orderGoods.put("goodsImg",commonModel.getOSS_DOM()+orderGoods.get("goodsImg").toString());
                //查询商品详情
                Integer goodsId = (Integer)orderGoods.get("goodsId");
                Goods goods = goodsService.selectById(goodsId);
                //商品市场价
                orderGoods.put("marketPrice",goods.getMarketPrice());
                orderGoods.put("payKaiyuan",goods.getPayKaiyuan());
                Integer goodsNum=Integer.parseInt(orderGoods.get("goodsNum").toString());
                payShopping=payShopping.add(goods.getPayShopping().multiply(new BigDecimal(goodsNum)));
                lists.add(orderGoods);
            }
            Map<String, Object> dateMap = new HashMap<>();
            if (!MSGUtil.isBlank(sktOrders.getExpressId()) || !MSGUtil.isBlank(sktOrders.getExpressNo())){
                dateMap.put("expressId",sktOrders.getExpressId());
                dateMap.put("expressNo",sktOrders.getExpressNo());
                dateMap.put("expressName",sktExpress!=null?sktExpress.getExpressName():"");
            }
            dateMap.put("catName",sktShops.getShopName());
            //线上商城logo
            dateMap.put("shopLogo",sktShops.getLogo());
            dateMap.put("list",lists);
            dateMap.put("shopId",sktOrders.getShopId());
            dateMap.put("orderNo",sktOrders.getOrderNo());
            dateMap.put("orderId",sktOrders.getOrderId());
            dateMap.put("orderStatus",sktOrders.getOrderStatus());
            dateMap.put("goodsMoney",sktOrders.getGoodsMoney());
            dateMap.put("totalMoney",sktOrders.getTotalMoney());
            dateMap.put("createTime",sktOrders.getCreateTime());
            dateMap.put("isPay",sktOrders.getIsPay());
            dateMap.put("userName",sktOrders.getUserName());
            dateMap.put("userAddress",sktOrders.getUserAddress());
            dateMap.put("userPhone",sktOrders.getUserPhone());
            dateMap.put("isAppraise",sktOrders.getIsAppraise());
            dateMap.put("isRefund",sktOrders.getIsRefund());
            dateMap.put("isClosed",sktOrders.getIsClosed());
            dateMap.put("receiveTime",sktOrders.getReceiveTime());
            dateMap.put("deliveryTime",sktOrders.getDeliveryTime());
            dateMap.put("paymentTime",sktOrders.getPaymentTime());
            dateMap.put("deliverMoney",sktOrders.getDeliverMoney());
            dateMap.put("payShopping",payShopping);

            return AppResult.OK("订单详情",dateMap);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("查询失败",-1);
        }
    }

    /**
     * 我的订单线下(ZJF)
     * @param page
     * @param pageSize
     * @param orderStatus
     * @return
     */
    @RequestMapping("/orders/orderUser")
    @ResponseBody
    public AppResult orderUser(@ModelAttribute("page")String page,@ModelAttribute("pageSize")String pageSize,@ModelAttribute("orderStatus")String orderStatus){
        try {
            int pagesizes=MSGUtil.isBlank(pageSize)?10:Integer.parseInt(pageSize);
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user == null){
                return AppResult.ERROR("对不起您还未登录",-2);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("page",MSGUtil.isBlank(page)?(1-1)*pagesizes:(Integer.parseInt(page)-1)*pagesizes);
            map.put("pageSize",pagesizes);
            map.put("userId",user.getUserId());
            map.put("dataFlag",1);
            if (!MSGUtil.isBlank(orderStatus)){
                map.put("orderStatus",Integer.parseInt(orderStatus));
            }
            List<Map<String,Object>> list=iAppOrderStoresService.selectOrderStoresByMap(map);
            List<Object> lists = new ArrayList<>();
            if (!MSGUtil.isBlank(list)){
                for (Map<String,Object> maps:list) {
                    maps.put("orderStatus",RemarksGenerate.orderType(Integer.parseInt(maps.get("orderStatus").toString())));
                    lists.add(maps);
                }
            }
            return AppResult.OK("查询成功",lists);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("查询失败",-1);
        }
    }

}
