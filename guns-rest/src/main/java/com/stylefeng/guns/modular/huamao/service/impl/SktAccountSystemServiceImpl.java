package com.stylefeng.guns.modular.huamao.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAccountSystemMapper;
import com.stylefeng.guns.modular.huamao.model.SktAccountSystem;
import com.stylefeng.guns.modular.huamao.service.ISktAccountSystemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统账户表 服务实现类
 * </p>
 *
 * @author wd123
 * @since 2018-05-25
 */
@Service
public class SktAccountSystemServiceImpl extends ServiceImpl<SktAccountSystemMapper, SktAccountSystem> implements ISktAccountSystemService {
	
}
