package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktExchangeUserMapper;
import com.stylefeng.guns.modular.huamao.model.SktExchangeUser;
import com.stylefeng.guns.modular.huamao.service.ISktExchangeUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-06-20
 */
@Service
public class SktExchangeUserServiceImpl extends ServiceImpl<SktExchangeUserMapper, SktExchangeUser> implements ISktExchangeUserService {

}
