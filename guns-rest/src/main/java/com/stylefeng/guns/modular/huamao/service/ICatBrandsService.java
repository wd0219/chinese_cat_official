package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.CatBrands;

/**
 * <p>
 * 分类-品牌表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface ICatBrandsService extends IService<CatBrands> {
	 

}
