package com.stylefeng.guns.modular.huamao.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品分类表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IGoodsCatsService extends IService<GoodsCats> {
	List<GoodsCats> selectByParentId(int parentId);
	boolean deleteByPId(Integer pid);
	List<Map<String,Object>> selectMap(GoodsCats goodsCats);
	List<Brands> selectCatName(String sIds);
	public JSONObject getGoodscatsByGoodsId(Integer goodsId );
	public JSONObject getGoodscatsByGoodsCatId(Integer goodsCatId );
	public List<Map<String,Object>>  selectGoodsCats(Integer parentId);
	public List category(Integer parentId);
	List<Map<String, Object>> getGoodscatsSelGoods(StringBuffer sb, Integer shopPrice1, 
			Integer shopPrice2, Integer saleNum, Integer shopPrice, Integer visitNum, 
			Integer appraiseNum, Integer ssaleTime, Integer isNew, Integer goodsStock, String brandIds, String goodsName, Integer isSelf);
	
	List<Map<String, Object>> selectShopCatsGoods(Integer shopId, Integer shopCatId, Integer shopPrice1,
			Integer shopPrice2, Integer saleNum, Integer shopPrice, Integer visitNum, Integer appraiseNum,
			Integer ssaleTime, Integer isNew, Integer goodsStock, String goodsName);
	Map<String, Object> selectShopsCores(Integer shopId);

    public List<Map<String,Object>> showFloor();
}
