package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.BrandsCustem;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 品牌表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface BrandsMapper extends BaseMapper<Brands> {
 List<Map<String,Object>> selectAll(BrandsCustem brandsCustem);
  Integer insert(BrandsCustem brandsCustem);
  Map<String,Object> selectMapById(Integer brandsId);
  List<Map<String,Object>> selectRecommend(BrandsCustem brandsCustem);
  List<Map<String,Object>>  selectNotRecommend(BrandsCustem brandsCustem);
 int selectBrandsTotal();
  
}
