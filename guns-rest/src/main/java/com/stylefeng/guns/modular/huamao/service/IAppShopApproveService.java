package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShopApprove;

public interface IAppShopApproveService extends IService<SktShopApprove> {
    SktShopApprove selectShopApproveByUserId(Integer userId);

}
