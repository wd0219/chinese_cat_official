package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.stylefeng.guns.modular.huamao.dao.UsersDrawsMaobiMapper;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsMaobi;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppUserDrwasMaobiService;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.RemarksGenerate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AppUserDrwasMaobiServiceImpl extends ServiceImpl<UsersDrawsMaobiMapper,UsersDrawsMaobi> implements IAppUserDrwasMaobiService {
    @Autowired
    private UsersDrawsMaobiMapper usersDrawsMaobiMapper;

    @Override
    public AppResult AccountMaobiRecord(Integer pageNum, UsersDrawsMaobi usersDrawsMaobis) {
        PageHelper.startPage(Integer.valueOf(pageNum),15);
        List<Map<String,Object>> usersDrawsMaobiList=new ArrayList<>();
        usersDrawsMaobiList=usersDrawsMaobiMapper.selectRecordByUserId(usersDrawsMaobis);
        List<Object> list = new ArrayList<>();
        if (usersDrawsMaobiList.size()>0) {
            for (Map<String,Object> usersDrawsMaobiMap : usersDrawsMaobiList) {
                Map<String, Object> map = new HashMap<>();
                map.put("maobi", "+"+usersDrawsMaobiMap.get("maobi").toString());
                map.put("orderNo", usersDrawsMaobiMap.get("orderNo"));
                map.put("cashType",1);
                if (Integer.parseInt(usersDrawsMaobiMap.get("drawsType").toString())==0) {
                    map.put("type", "到帐猫币");
                }else {
                    map.put("type", "到帐HHTB");
                }
                map.put("createTime", usersDrawsMaobiMap.get("createTime"));
                list.add(map);
            }
        }
        return AppResult.OK("查询成功",list);
    }

    @Override
    public double selectAllMaobiByUserId(Integer userId) {
        return usersDrawsMaobiMapper.selectAllMaobiByUserId(userId);
    }

    @Override
    public Double selectAllhhtbByUserId(Integer userId) {
        return usersDrawsMaobiMapper.selectAllhhtbByUserId(userId);
    }
}
