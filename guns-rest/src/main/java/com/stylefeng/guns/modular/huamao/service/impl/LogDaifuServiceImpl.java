package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogDaifuMapper;
import com.stylefeng.guns.modular.huamao.model.LogDaifu;
import com.stylefeng.guns.modular.huamao.model.LogDaifuList;
import com.stylefeng.guns.modular.huamao.service.ILogDaifuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 代付记录表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
@Service
public class LogDaifuServiceImpl extends ServiceImpl<LogDaifuMapper, LogDaifu> implements ILogDaifuService {

	@Autowired
	private LogDaifuMapper ldm;

	// 代付记录表展示
	@Override
	public List<LogDaifuList> selectLogDaifuAll(LogDaifuList logDaifuList) {
		return ldm.selectLogDaifuAll(logDaifuList);
	}

}
