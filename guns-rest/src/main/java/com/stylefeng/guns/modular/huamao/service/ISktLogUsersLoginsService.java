package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktLogUsersLogins;

import java.util.List;

/**
 * <p>
 * 会员登陆记录表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
public interface ISktLogUsersLoginsService extends IService<SktLogUsersLogins> {
	
	public List<SktLogUsersLogins> sktLogUsersLoginsfindup(SktLogUsersLogins sktLogUsersLogins);
}
