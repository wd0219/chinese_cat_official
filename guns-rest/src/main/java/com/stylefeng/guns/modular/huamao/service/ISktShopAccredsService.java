package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShopAccreds;
import com.stylefeng.guns.modular.huamao.model.SktShopAccredsDTO;

import java.util.List;

/**
 * <p>
 * 店铺认证信息表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-13
 */
public interface ISktShopAccredsService extends IService<SktShopAccreds> {
	/*
	 * 展示全部
	 * */
	public List<SktShopAccredsDTO> sktShopAccredsfindall(SktShopAccredsDTO sktShopAccredsDTO);
	/*
	 * 删除
	 * */
	public void sktShopAccredsdelete(Integer shopId);
	/*
	 * 修改回显
	 * */
	public List<SktShopAccredsDTO> sktShopAccredsfindup(SktShopAccredsDTO shopAccredsDTO);
}
