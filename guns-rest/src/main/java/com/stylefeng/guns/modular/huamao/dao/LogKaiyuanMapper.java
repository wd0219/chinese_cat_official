package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanList;
import com.stylefeng.guns.modular.huamao.model.TradeDictDTD;

/**
 * <p>
 * 开元宝流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogKaiyuanMapper extends BaseMapper<LogKaiyuan> {

	// 开元宝流水返回
	List<LogKaiyuanList> selectLogKaiyuanAll(LogKaiyuanList logKaiyuanList);

	/**
	 * 首页显示
	 * @param logKaiyuanList
	 * @return
	 */
    List<Map<String,Object>> selectUserKaiYuan(LogKaiyuanList logKaiyuanList);

    List<LogKaiyuan> huaBaoList(Integer userId);

	TradeDictDTD selectTradeDict(Map<String, Object> map);

}
