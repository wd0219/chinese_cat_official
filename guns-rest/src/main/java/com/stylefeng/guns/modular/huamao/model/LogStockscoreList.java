package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 库存积分流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public class LogStockscoreList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 积分类型 1购买库存积分获得 2商城下单冻结库存积分 3商城订单退货返还 4线下订单发放库存积分
	 */
	private Integer type;
	/**
	 * 发起用户ID 0为平台
	 */
	private Integer fromId;
	/**
	 * 目标用户ID
	 */
	private Integer userId;
	/**
	 * 对应订单号
	 */
	private String orderNo;
	/**
	 * 操作前的金额
	 */
	private BigDecimal preScore;
	/**
	 * 流水标志 -1减少 1增加
	 */
	private Integer scoreType;
	/**
	 * 金额
	 */
	private BigDecimal score;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String scorep;
	/**
	 * 变动后金额
	 */
	private BigDecimal ascore;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 有效状态 1有效 0删除
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getFromId() {
		return fromId;
	}
	public void setFromId(Integer fromId) {
		this.fromId = fromId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getPreScore() {
		return preScore;
	}
	public void setPreScore(BigDecimal preScore) {
		this.preScore = preScore;
	}
	public Integer getScoreType() {
		return scoreType;
	}
	public void setScoreType(Integer scoreType) {
		this.scoreType = scoreType;
	}
	public BigDecimal getScore() {
		return score;
	}
	public void setScore(BigDecimal score) {
		this.score = score;
	}
	public String getScorep() {
		return scorep;
	}
	public void setScorep(String scorep) {
		this.scorep = scorep;
	}
	public BigDecimal getAscore() {
		return ascore;
	}
	public void setAscore(BigDecimal ascore) {
		this.ascore = ascore;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	

}
