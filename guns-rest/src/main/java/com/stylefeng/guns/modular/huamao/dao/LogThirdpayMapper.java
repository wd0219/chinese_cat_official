package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogThirdpay;
import com.stylefeng.guns.modular.huamao.model.LogThirdpayList;

/**
 * <p>
 * 第三方支付记录表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface LogThirdpayMapper extends BaseMapper<LogThirdpay> {

	// 第三方支付记录表展示
	List<LogThirdpayList> selectLogThirdpayAll(LogThirdpayList logThirdpayList);

}
