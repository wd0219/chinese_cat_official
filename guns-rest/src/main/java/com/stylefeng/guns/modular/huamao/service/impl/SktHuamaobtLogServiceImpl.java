package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.modular.huamao.dao.SktHuamaobtLogMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.HttpUtil;
import com.stylefeng.guns.modular.huamao.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-06-20
 */
@Service
public class SktHuamaobtLogServiceImpl extends ServiceImpl<SktHuamaobtLogMapper, SktHuamaobtLog> implements ISktHuamaobtLogService {

    @Autowired
    ISktExchangeUserService exchangeUserService;

    @Autowired
    IAccountService accountService;

    @Autowired
    ILogKaiyuanService logKaiyuanService;

    @Autowired
    IUsersService usersService;


    public JSONObject checkNumber(Integer userId){
        Users users = new Users();
        users = usersService.selectById(userId);

        String url = "https://api1.digsg.com/user/register";
        String companyID = "1";

        String userMobile= users.getUserPhone();
        String secretKey = "iL34.#dnZ2ubJJFt";
        String key = userMobile+companyID+secretKey;
        String md5Info = MD5Util.encrypt(key);

        Map<String,Object> map = new HashMap<String, Object>();
        map.put("companyID",companyID);
        map.put("userMobile",userMobile);
        map.put("md5Info",md5Info);

        String json =  HttpUtil.doPost(url,map);
        return JSONObject.parseObject(json);

    }

    /**
     *
     * @return
     */
    @Override
    @Transactional
    public Object ticker(){

        //String turl = "https://api1.digsg.com/ticker/ticker?market=maoc_usdt";
        String turl = "https://api1.digsg.com/ticker/ticker?market=maoc_hhtb";
        String json =  HttpUtil.doGet(turl);

        JSONObject  jsonObject  = JSONObject.parseObject(json);
        JSONObject datas = jsonObject.getJSONObject("datas");
        String cnyPrices = datas.getString("cnyPrice");
        BigDecimal divide = new BigDecimal(1).divide(new BigDecimal(cnyPrices), 4, BigDecimal.ROUND_HALF_UP);
        return divide.toString();
    }

    /**
     * 猫币转换
     * @param userId
     * @param amount
     * @param orderNo
     * @return
     */
    @Override
    @Transactional
    public Result recharge(Integer userId,BigDecimal huabao, BigDecimal amount,String orderNo){

        JSONObject result = this.checkNumber(userId);
        /**
         * 用来接收datas的值
         */
        JSONObject robject = new JSONObject();
        if(!result.getBoolean("isSuc")){
            return Result.EEROR("校验失败");
        }else{
            robject  = result.getJSONObject("datas");
            /**
             * 判断用户是否注册
             */
            if(robject.getString("userID").equals("0")){
                return Result.EEROR("用户未注册");
            }
        }

        String url = "https://api1.digsg.com/assets/recharge";

        /**
         * 用户交易所的ID
         */
        //Integer userID = rexchangeUser.getExchangeUserId();
        Integer userID = robject.getInteger("userID");
        String companyID = "1";
        String secretKey = "iL34.#dnZ2ubJJFt";

        /**
         * MD5加密
         */
        String key = Integer.toString(userID)+amount.toString()+companyID+secretKey;
        String md5Info = MD5Util.encrypt(key);

        Map<String,Object> map = new HashMap<String, Object>();
        map.put("userID",userID);
        map.put("amount",amount);
        map.put("companyID",companyID);
        map.put("md5Info",md5Info);

        /**
         *调用用户充值接口
         */
        String json =  HttpUtil.doPost(url,map);
        JSONObject jsonObject = JSONObject.parseObject(json);
        if(jsonObject.getBoolean("isSuc")){


            /**
             * 减少用户相应的华宝
             */
            Account account = new Account();
            //userId代表用户id，userID代表交易所id
            account.setUserId(userId);
            Account raccount  = new Account();
            EntityWrapper<Account> entityWrapper = new EntityWrapper<Account>(account);
            raccount = accountService.selectOne(entityWrapper);
            raccount.getKaiyuan();
            BigDecimal temp = huabao;
            raccount.setKaiyuan(raccount.getKaiyuan().subtract(temp));
            accountService.updateById(raccount);

            /**
             * 增加华宝交易记录
             */
            LogKaiyuan logKaiyuan = new LogKaiyuan();
            logKaiyuan.setType(33);
            logKaiyuan.setFromId(userId);
            logKaiyuan.setUserId(userID);
            logKaiyuan.setOrderNo(orderNo);
            logKaiyuan.setPreKaiyuan(account.getKaiyuan());
            logKaiyuan.setKaiyuanType(-1);
            logKaiyuan.setKaiyuan(temp);
            logKaiyuan.setRemark("赎回支出");
            logKaiyuan.setDataFlag(1);
            logKaiyuan.setCreateTime(new Date());
            logKaiyuanService.insert(logKaiyuan);

            /**
             * 增加交易记录
             */
            SktHuamaobtLog huamaobtLog  = new SktHuamaobtLog();
            //userId代表用户id，userID代表交易所id
            huamaobtLog.setExchangeUserId(userID);
            huamaobtLog.setAmount(amount);
            huamaobtLog.setCreateTime(new Date());
            huamaobtLog.setOrderNo(orderNo);
            this.insert(huamaobtLog);

            return Result.OK("转换成功");

        }else{
            return Result.EEROR(jsonObject.getString("des"));
        }

    }
}
