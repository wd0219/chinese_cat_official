package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AgentsApplys;

import java.util.Map;

public interface IAppAgentsApplysService extends IService<AgentsApplys> {
    AgentsApplys selectAgentsApplysByUserId(Integer userId);

    AgentsApplys selectAgentsApplysByUserIdAndStatus(Map<String, Object> map);

    Map<String,Object> selectAgentsAppLysCheckApplyByUserId(Integer userId);

}
