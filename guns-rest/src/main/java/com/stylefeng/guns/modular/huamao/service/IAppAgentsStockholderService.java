package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholder;

import java.util.List;
import java.util.Map;

public interface IAppAgentsStockholderService extends IService<SktAgentsStockholder> {
    List<SktAgentsStockholder> selectAgentsStockholderList(Map<String, Object> map);
}
