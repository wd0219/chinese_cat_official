package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.support.DateTime;
import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktShopApplysService;

/**
 * 商城商家开店申请控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 17:51:01
 */
@CrossOrigin
@Controller
@RequestMapping("/sktShopApplys")
public class SktShopApplysController extends BaseController {

    private String PREFIX = "/huamao/sktShopApplys/";
    @Autowired
    private ISktShopApplysService sktShopApplysService;

    /**
     * 跳转到商城商家开店申请首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopApplys.html";
    }

    /**
     * 跳转到添加商城商家开店申请
     */
    @RequestMapping("/sktShopApplys_add")
    public String sktShopApplysAdd() {
        return PREFIX + "sktShopApplys_add.html";
    }

    /**
     * 跳转到修改商城商家开店申请
     */
    @RequestMapping("/sktShopApplys_update/{sktShopApplysId}")
    public String sktShopApplysUpdate(@PathVariable Integer sktShopApplysId, Model model) {
        SktShopApplys sktShopApplys = sktShopApplysService.selectById(sktShopApplysId);
        model.addAttribute("item",sktShopApplys);
        LogObjectHolder.me().set(sktShopApplys);
        return PREFIX + "sktShopApplys_edit.html";
    }

    /**
     * 获取商城商家开店申请列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(MyDTO myDto) {
        return sktShopApplysService.shopApplysfindall(myDto);
    }
    /*
     * 搜索
     * */

    /**
     * 新增商城商家开店申请
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopApplys sktShopApplys) {
        sktShopApplysService.insert(sktShopApplys);
        return SUCCESS_TIP;
    }

    /**
     * 删除商城商家开店申请
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopApplysId) {
        sktShopApplysService.deleteById(sktShopApplysId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商城商家开店申请
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object shopApplyupdate(MyDTO myDto) {
    	sktShopApplysService.shopApplyupdate(myDto);
        return SUCCESS_TIP;
    }

    /**
     * 商城商家开店申请详情
     */
    @RequestMapping(value = "/detail/{sktShopApplysId}")
    public String detail(@PathVariable("sktShopApplysId") Integer sktShopApplysId,MyDTO myDto,Model model) {
    	myDto.setApplyId(sktShopApplysId);
    	List<MyDTO> list = sktShopApplysService.shopApplysfindup(myDto);
    	 model.addAttribute("item",list.get(0));
    	 return PREFIX +"sktShopApplys_edit.html";
    }
    
    /*
     *处理
     * */
    @RequestMapping(value = "/detail2/{sktShopApplysId}")
    public String detail1(@PathVariable("sktShopApplysId") Integer sktShopApplysId,MyDTO myDto,Model model) {
    	myDto.setApplyId(sktShopApplysId);
    	List<MyDTO> list = sktShopApplysService.shopApplysfindup(myDto);
    	myDto.setCheckTime(new DateTime().toString());
    	 model.addAttribute("item",list.get(0));
    	 return PREFIX +"sktShopApplys_edit1.html";
    }
    
    /*********首页**********/
//    /**
//     * 新增商城商家开店申请表
//     */
//    @RequestMapping(value = "/insertShopApplys")
//    @ResponseBody
//    public Result insertShopApplys(SktShopApplys sktShopApplys) {
//        boolean b = sktShopApplysService.insertShopApplys(sktShopApplys);
//        return b==true?Result.OK("新增线上商城成功！"):Result.EEROR("新增线上商城失败！");
//    }

    /**
     * 新增商城商家开店申请表修改
     */
    @RequestMapping(value = "/insertShopApplys")
    @ResponseBody
    public Result insertShopApplys2(SktShopApprove sktShopApprove) {
        String b = sktShopApplysService.insertShopApplys2(sktShopApprove);
        if("0".equals(b)){
            return Result.EEROR("线上商家申请失败");
        }else if("2".equals(b)){
            return Result.EEROR("已经申请线上商家");
        }else if("3".equals(b)){
            return Result.EEROR("已经是线上商家");
        }else if("5".equals(b)){
            return Result.EEROR("联盟商家已经申请，请先通过联盟申请");
        }
        //return b==true?Result.OK("新增线上商城成功！"):Result.EEROR("新增线上商城失败！");
        return Result.OK("申请线上商家成功");
    }
}
