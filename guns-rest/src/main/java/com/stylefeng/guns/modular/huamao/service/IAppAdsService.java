package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Ads;

import java.util.List;

public interface IAppAdsService extends IService<Ads> {

    List<Ads> findAds(int positionId, int num, String string);
}
