package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AccountShopMapper;
import com.stylefeng.guns.modular.huamao.model.AccountShop;
import com.stylefeng.guns.modular.huamao.service.IAppAccountShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppAccountShopServiceImpl extends ServiceImpl<AccountShopMapper,AccountShop> implements IAppAccountShopService {
   @Autowired
   private AccountShopMapper accountShopMapper;
    @Override
    public AccountShop scoreIndex(Integer userId, Integer shopId) {
        return accountShopMapper.selectAccountShopByUserIdAndShopId(userId,shopId);
    }

    @Override
    public Map<String, Object> selectScoreByShopId(Integer shopId) {
        return accountShopMapper.selectScoreByShopId(shopId);
    }

    @Override
    public AccountShop selectAccountShopByUserId(Integer userId) {
        return accountShopMapper.selectAccountShopByUserId(userId);
    }

    @Override
    public void updateAccountShopsByUserId(Map<String, Object> maps) {
        accountShopMapper.updateAccountShopsByUserId(maps);
    }

    @Override
    public boolean updateAccountShopById(AccountShop accountShop) {
        int i=accountShopMapper.updateAccountShopById(accountShop);
        if (i>0){
            return true;
        }
        return false;
    }
}
