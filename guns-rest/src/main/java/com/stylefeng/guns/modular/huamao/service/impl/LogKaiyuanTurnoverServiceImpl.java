package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanTurnoverMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreeze;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreezeList;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnover;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnoverList;
import com.stylefeng.guns.modular.huamao.service.ILogKaiyuanTurnoverService;

/**
 * <p>
 * 开元宝营业额流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
@Service
public class LogKaiyuanTurnoverServiceImpl extends ServiceImpl<LogKaiyuanTurnoverMapper, LogKaiyuanTurnover>
		implements ILogKaiyuanTurnoverService {

	@Autowired
	private LogKaiyuanTurnoverMapper ltm;

	// 开元宝营业额流水表展示
	@Override
	public List<LogKaiyuanTurnoverList> selectLogKaiyuanTurnoverAll(LogKaiyuanTurnoverList logKaiyuanTurnoverList) {
		return ltm.selectLogKaiyuanTurnoverAll(logKaiyuanTurnoverList);
	}
	
	@Override
	public List<LogKaiyuanTurnover> selectTurnoverInfo(LogKaiyuanTurnoverList logKaiyuanTurnoverList) {
		List<LogKaiyuanTurnover> list = ltm.selectTurnoverInfo(logKaiyuanTurnoverList);
		return list;
	}

	@Override
	public List<LogKaiyuanFreeze> selectLogKaiyuanFreeze(LogKaiyuanFreezeList logKaiyuanTurnoverList) {
		List<LogKaiyuanFreeze> list = ltm.selectLogKaiyuanFreeze(logKaiyuanTurnoverList);
		return list;
	}
}
