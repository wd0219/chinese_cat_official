package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Articles;

public interface IAppArticleService extends IService<Articles> {
    Articles selectArticlesById(Integer articlesId);
}
