package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuan;
import com.stylefeng.guns.modular.huamao.model.TradeDictDTD;

import java.util.List;
import java.util.Map;

public interface IAppLogKaiyuanService extends IService<LogKaiyuan> {
    List<LogKaiyuan> huaBaoList(Integer userId);

    TradeDictDTD selectTradeDict(Map<String, Object> s);
}
