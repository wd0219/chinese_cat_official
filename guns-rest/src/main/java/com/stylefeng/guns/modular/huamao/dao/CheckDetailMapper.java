package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.CheckDetail;
import com.stylefeng.guns.modular.huamao.model.CheckDetailList;

/**
 * <p>
 * 每日用户对账结果数据表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-13
 */
public interface CheckDetailMapper extends BaseMapper<CheckDetail> {

	// 展示用户每日对账列表
	List<CheckDetailList> selectCheckDetailAll(CheckDetailList checkDetailList);

	// 通过id查询用户详情
	CheckDetailList selectctDetailedById(Integer checkDetailId);

}
