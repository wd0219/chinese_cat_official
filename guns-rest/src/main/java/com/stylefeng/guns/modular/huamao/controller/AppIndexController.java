package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
@RestController
@RequestMapping("/app")
@CrossOrigin
public class AppIndexController extends BaseController {
    @Autowired
    private IAdsService adsService;
    @Autowired
    private IGoodsCatsService goodsCatsService;
    @Autowired
    private IGoodsService goodsService;
    @Autowired
    private IGoodsAppraisesService goodsAppraisesService;
    @Autowired
    private ISpecItemsService specItemsService;
    @Autowired
    private IGoodsSpecsService goodsSpecsService;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private ISktShopsService sktShopsService;
    @Autowired
    private IUsersService usersService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IArticlesService iArticlesService;
    @Autowired
    private ISktShopCatsService sktShopCatsService;
    @Autowired
    private IAppFavoritesService iAppFavoritesService;
    @Autowired
    private ISktShopScoresService sktShopScoresService;

    /**
     * 首页（商城）
     *
     * @return
     */
    @RequestMapping("/site/home")
    @ResponseBody
    public AppResult siteHome() {
        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        Map<String, Object> map = new HashMap<>();
        map.put("positionCode", "app-1-1");
        List<Map<String, Object>> adses = adsService.getSiteHome(map);
        List<Map<String, Object>> goodsCatses = goodsCatsService.selectGoodsCats(0);
        List<Map<String, Object>> list = iArticlesService.articleCompanyNew();
        PageHelper.startPage(1, 10);
//        Goods goods=new Goods();
//        goods.setDataFlag(1);
//        goods.setIsSale(1);
//        goods.setGoodsStatus(1);
        Map<String, Object> map1=new HashMap<>();
        map1.put("isSale",1);
        map1.put("goodsStatus",1);
        map1.put("shopStatus",1);
        map1.put("dataFlag",1);
        map1.put("dataFlags",1);
     //   EntityWrapper<Goods> entityWrapper = new EntityWrapper<Goods>(goods);
        List<Goods> goodses = goodsService.selectGoodsByMap(map1);
        json.put("ads", adses);
        json.put("goodsCat", goodsCatses);
        json.put("goods", goodses);
        json.put("news", list);
        return AppResult.OK(json);
    }

    /**
     * 下拉获取更多猜你喜欢商品
     *
     * @param pageIndex
     * @return
     */
    @RequestMapping("/Goods/goods")
    public AppResult goodsGoods(@ModelAttribute("pageIndex") String pageIndex) {
        Integer pageIndexs=1;
        if (!"".equals(pageIndex) && pageIndex!=null){
            pageIndexs=Integer.parseInt(pageIndex);
        }
//        Goods goods=new Goods();
//        goods.setDataFlag(1);
//        goods.setIsSale(1);
//        goods.setGoodsStatus(1);
//        EntityWrapper<Goods> entityWrapper = new EntityWrapper<Goods>(goods);
//        List<Goods> goodses = goodsService.selectList(entityWrapper);
        PageHelper.startPage(pageIndexs, 10);
        Map<String, Object> map1=new HashMap<>();
        map1.put("isSale",1);
        map1.put("goodsStatus",1);
        map1.put("shopStatus",1);
        map1.put("dataFlag",1);
        map1.put("dataFlags",1);
        //   EntityWrapper<Goods> entityWrapper = new EntityWrapper<Goods>(goods);
        List<Goods> goodses = goodsService.selectGoodsByMap(map1);

        JSONObject json = new JSONObject();
        if (goodses != null && goodses.size() > 0) {
            PageInfo pageInfo = new PageInfo(goodses);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", goodses);
        return AppResult.OK(json);
    }

    /**
     * 搜索某一商品
     *
     * @param goodsName
     * @return
     */
    @RequestMapping("/Goods/searchGoods")
    public AppResult searchGoods(@ModelAttribute("goodsName") String goodsName,
                                 @ModelAttribute("pageIndex") String  pageIndex) {
        Integer pageIndexs=1;
        if (!"".equals(pageIndex) && pageIndex!=null){
            pageIndexs=Integer.parseInt(pageIndex);
        }
//        EntityWrapper entityWrapper = new EntityWrapper();
//        entityWrapper.setEntity(new Goods());
//        entityWrapper.where("dataFlag={0}",1).and("isSale={0}",1).and("goodsStatus={0}",1).like("goodsName", goodsName);
//        List<Goods> goodses = goodsService.selectList(entityWrapper);
        PageHelper.startPage(pageIndexs, 10);
        Map<String, Object> map1=new HashMap<>();
        map1.put("isSale",1);
        map1.put("goodsStatus",1);
        map1.put("shopStatus",1);
        map1.put("dataFlag",1);
        map1.put("dataFlags",1);
        map1.put("goodsName",goodsName);
        List<Goods> goodses = goodsService.selectGoodsByMap(map1);

        JSONObject json = new JSONObject();
        if (goodses != null && goodses.size() > 0) {
            PageInfo pageInfo = new PageInfo(goodses);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", goodses);
        return AppResult.OK(json);
    }

    /**
     * 商品详情
     *
     * @param goodsId 商品ID
     * @return
     */
    @RequestMapping("/Goods/goodsDetail")
    public AppResult goodsDetail(@ModelAttribute("goodsId") String goodsId) {

        Goods goods = goodsService.selectById(Integer.parseInt(goodsId));
        if (goods.getIsSale()==0 ){
          return   AppResult.ERROR("该商品已下架",-1);
        }
        if ( goods.getGoodsStatus()!=1){
            return   AppResult.ERROR("该商品违规或审核未通过",-1);
        }
        if ( goods.getDataFlag()==-1){
            return   AppResult.ERROR("该商品已失效",-1);
        }

        SktShops sktShops = new SktShops();
        sktShops.setShopId(goods.getShopId());
        EntityWrapper<SktShops> entityWrapper = new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if (sktShops1.getShopStatus()==0){
            return   AppResult.ERROR("该商品所属商家已关闭",-1);
        }
        if (sktShops1.getDataFlag()==-1){
            return   AppResult.ERROR("该商品所属商家已失效",-1);
        }
        GoodsCustm goodsCustm = new GoodsCustm();
        BeanUtils.copyProperties(goods, goodsCustm);
        goodsCustm.setShopName(sktShops1.getShopName());
        SktFavorites sktFavorites = new SktFavorites();


        if (cacheUtil.isKeyInCache(SessionUtils.getSessionId())){
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            sktFavorites.setTargetId(Integer.parseInt(goodsId));
            sktFavorites.setUserId(user.getUserId());
            SktFavorites sktFavorites1 = iAppFavoritesService.selectListByEntity(sktFavorites);
            if (!MSGUtil.isBlank(sktFavorites1)) {
                goodsCustm.setFavorites(sktFavorites1.getFavoriteId());
            } else {
                goodsCustm.setFavorites(0);
            }
        }
        goodsCustm.setShopLog(sktShops1.getLogo());
        SktShopScores sktShopScores = new SktShopScores();
        sktShopScores.setShopId(goods.getShopId());
        EntityWrapper<SktShopScores> ewp = new EntityWrapper<SktShopScores>(sktShopScores);
        SktShopScores sktShopScores1 = sktShopScoresService.selectOne(ewp);
        if(sktShopScores1==null){
            goodsCustm.setShopScores(BigDecimal.ZERO);
        }else {
            int totalScore = sktShopScores1.getTotalScore();
            int totalUsers = sktShopScores1.getTotalUsers();
            if (totalUsers != 0) {
                int flag = totalUsers * 3;
                BigDecimal shopScores = new BigDecimal(totalScore).divide(new BigDecimal(flag), 2, BigDecimal.ROUND_HALF_UP);
                goodsCustm.setShopScores(shopScores);
            } else {
                goodsCustm.setShopScores(BigDecimal.ZERO);
            }
        }



        return AppResult.OK(goodsCustm);
    }

    /**
     * 商品评价
     *
     * @param goodsId 商品ID
     * @return
     */
    @RequestMapping("/martgoods/readComment")
    public AppResult readComment(@ModelAttribute("goodsId") Integer goodsId) {
        List<Map<String, Object>> goodsAppraises = goodsAppraisesService.readComment(goodsId);
        return AppResult.OK(goodsAppraises);
    }

    /**
     * 获取某一分类下商品列表(商家)
     *
     * @param goodsCatIdPath
     * @param pageIndex
     * @param order
     * @return
     */
    @RequestMapping("/Goods/goodsShopSort")
    public AppResult goodsShopSort(@ModelAttribute("goodsCatIdPath") String goodsCatIdPath,
                                   @ModelAttribute("pageIndex") String pageIndex,
                                   @ModelAttribute("order") String order,
                                   @ModelAttribute("shopId") Integer shopId) {
//        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
//        if(user==null){
//            return AppResult.ERROR("您未登录",-2);
//        }
//        SktShops sktShops=new SktShops();
//        sktShops.setUserId(user.getUserId());
//        EntityWrapper<SktShops> entityWrapper=new EntityWrapper<SktShops>(sktShops);
//        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
//        if (sktShops1==null){
//            return AppResult.ERROR("无商家信息");
//        }
//        int shopId =sktShops1.getShopId();
//        int shopId=185;
        Integer pageIndexs=1;
        if (!"".equals(pageIndex) && pageIndex!=null){
            pageIndexs=Integer.parseInt(pageIndex);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("catId", goodsCatIdPath);
        map.put("order", order);
        map.put("shopId", shopId);
        PageHelper.startPage(pageIndexs, 10);
        List<Map<String, Object>> maps = goodsService.goodsShopSort(map);
        JSONObject json = new JSONObject();
        if (maps != null && maps.size() > 0) {
            PageInfo pageInfo = new PageInfo(maps);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", maps);
        return AppResult.OK(json);


    }

    /**
     * 选择规格
     *
     * @param goodsId
     * @return
     */
    @RequestMapping("/Goods/goodsDetailType")
    public AppResult goodsDetailType(@ModelAttribute("goodsId") Integer goodsId) {
        Map<String, Object> map = specItemsService.goodsDetailType(goodsId);
        return AppResult.OK(map);
    }

    /**
     *
     *
     * @param goodsSpecId
     * @param goodsId根据规格组合获取价格和库存
     * @return
     */
    @RequestMapping("/Goods/goodsSpecPrice")
    public AppResult goodsSpecPrice(@ModelAttribute("goodsSpecId") String goodsSpecId,
                                    @ModelAttribute("goodsId") Integer goodsId) {
        GoodsSpecs goodsSpecs = new GoodsSpecs();
        goodsSpecs.setSpecIds(goodsSpecId);
        goodsSpecs.setGoodsId(goodsId);
        EntityWrapper<GoodsSpecs> entityWrapper = new EntityWrapper<GoodsSpecs>(goodsSpecs);
        GoodsSpecs goodsSpecs1 = goodsSpecsService.selectOne(entityWrapper);
        return AppResult.OK(goodsSpecs1);
    }

    /**
     * 线上商城主页
     *
     * @param shopId
     * @return
     */
    @RequestMapping("/Site/shopHome")
    public AppResult shopHome(@ModelAttribute("shopId") Integer shopId) {
//        Goods goods = new Goods();
//        goods.setShopId(shopId);
//        goods.setDataFlag(1);
//        goods.setIsRecom(1);
//        goods.setIsSale(1);
//        goods.setGoodsStatus(1);

//        EntityWrapper<Goods> entityWrapper = new EntityWrapper<Goods>(goods);
//        List<Goods> goodsList = goodsService.selectList(entityWrapper);

        PageHelper.startPage(1, 10);
        Map<String, Object> map1=new HashMap<>();
        map1.put("isSale",1);
        map1.put("goodsStatus",1);
        map1.put("shopStatus",1);
        map1.put("dataFlag",1);
        map1.put("dataFlags",1);
        List<Goods> goodsList = goodsService.selectGoodsByMap(map1);


        //  SktShops sktShops = sktShopsService.selectById(shopId);
        Map<String, Object> map = sktShopsService.shopHome(shopId);
        map.put("goods", AppResult.OK(goodsList));
        return AppResult.OK(map);
    }

    /**
     * 下拉获取更多商家推荐商品
     *
     * @param shopId
     * @param pageIndex
     * @return
     */
    @RequestMapping("/Goods/shopRecommend")
    public AppResult shopRecommend(@ModelAttribute("shopId") Integer shopId, @ModelAttribute("pageIndex") String pageIndex) {
        Integer pageIndexs=1;
        if (!"".equals(pageIndex) && pageIndex!=null){
            pageIndexs=Integer.parseInt(pageIndex);
        }
        Goods goods = new Goods();
        goods.setShopId(shopId);
        goods.setDataFlag(1);
        goods.setIsRecom(1);
        goods.setIsSale(1);
        goods.setGoodsStatus(1);
        EntityWrapper<Goods> entityWrapper = new EntityWrapper<Goods>(goods);

        PageHelper.startPage(pageIndexs, 10);
        List<Goods> goodsList = goodsService.selectList(entityWrapper);
        JSONObject json = new JSONObject();
        if (goodsList != null && goodsList.size() > 0) {
            PageInfo pageInfo = new PageInfo(goodsList);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", goodsList);
        return AppResult.OK(json);
    }

    /**
     * 查询商家所有商品
     *
     * @param shopId
     * @param pageIndex
     * @return
     */
    @RequestMapping("/Goods/shopGoods")
    public AppResult shopGoods(@ModelAttribute("shopId") Integer shopId,
                               @ModelAttribute("pageIndex") String pageIndex,
                               @ModelAttribute("order") String order,
                               @ModelAttribute("style") String style) {
        Integer pageIndexs=1;
        if (!"".equals(pageIndex) && pageIndex!=null){
            pageIndexs=Integer.parseInt(pageIndex);
        }
        Map<String, Object> map = new HashMap<>();
        if ("isNew".equals(style)) {
            map.put("isNew", 1);
        }
        if ("isHot".equals(style)) {
            map.put("isHot", 1);
        }
        if ("isRecom".equals(style)) {
            map.put("isRecom", 1);
        }
        if ("isBest".equals(style)) {
            map.put("isBest", 1);
        }
        map.put("order", order);
        map.put("shopId", shopId);

        PageHelper.startPage(pageIndexs, 10);
        List<Map<String, Object>> maps = goodsService.selectAppByOrder(map);
        JSONObject json = new JSONObject();
        if (maps != null && maps.size() > 0) {
            PageInfo pageInfo = new PageInfo(maps);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", maps);
        return AppResult.OK(json);
    }

    /**
     * 查询商家某一商品(根据商品名称)
     *
     * @param shopId
     * @param goodsName
     * @return
     */
    @RequestMapping("/Goods/searchShopGoods")
    public AppResult searchShopGoods(@ModelAttribute("shopId") Integer shopId,
                                     @ModelAttribute("goodsName") String goodsName,
                                     @ModelAttribute("pageIndex") String pageIndex) {
        Integer pageIndexs=1;
        if (!"".equals(pageIndex) && pageIndex!=null){
            pageIndexs=Integer.parseInt(pageIndex);
        }
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.setEntity(new Goods());
        entityWrapper.where("shopId={0}", shopId).and("isSale={0}",1).and("goodsStatus={0}",1).like("goodsName", goodsName);

        PageHelper.startPage(pageIndexs, 10);
        List<Goods> goodses = goodsService.selectList(entityWrapper);
        JSONObject json = new JSONObject();
        if (goodses != null && goodses.size() > 0) {
            PageInfo pageInfo = new PageInfo(goodses);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", goodses);
        return AppResult.OK(json);
    }

    /**
     * 商品分类  首页下面
     *
     * @return
     */
    @RequestMapping("/Goods/classify")
    public AppResult category() {
        List category = goodsCatsService.category(0);
        return AppResult.OK(category);
    }

    /**
     * 商品分类(商家)
     *
     * @param shopId
     * @return
     */
    @RequestMapping("/Goods/categoryShop")
    public AppResult categoryShop(@ModelAttribute("shopId") Integer shopId) {
        List list = sktShopCatsService.categoryShop(shopId);
        return AppResult.OK(list);
    }


    /**
     * 当面付页面显示数据
     *
     * @return
     */
    @RequestMapping("/score/gather")
    public AppResult gather() {
        Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
        //   Users user =usersService.selectById(6237);
        if (user == null) {
            return AppResult.ERROR("您未登录", -2);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("userPhoto", user.getUserPhoto());
        map.put("userPhone", user.getUserPhone());
        map.put("loginName", user.getLoginName());
        return AppResult.OK(map);
    }

    /**
     * 付款页面用户账户信息
     *
     * @return
     */
    @RequestMapping("/account/userAccount")
    public AppResult userAccount() {
        Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
        //   Users user =usersService.selectById(6237);
        if (user == null) {
            return AppResult.ERROR("您未登录", -2);
        }
        Account account = new Account();
        account.setUserId(user.getUserId());
        EntityWrapper<Account> entityWrapper = new EntityWrapper<Account>(account);
        Account account1 = accountService.selectOne(entityWrapper);
        Map<String, Object> map = new HashMap<>();
        map.put("cash", account1.getCash());
        map.put("kaiyuan", account1.getKaiyuan());
        return AppResult.OK(map);
    }

    /**
     * 付款提交后输入交易密码
     *
     * @param userPhone  收款人手机号码
     * @param payMoney   支付金额
     * @param scoreRatio 积分比例
     * @param payPwd     交易密码
     * @param payType    付款方式（2：现金；3：开元宝）
     * @return
     */
    @RequestMapping("/score/personPay")
    public AppResult personPay(@ModelAttribute("userPhone") String userPhone,
                               @ModelAttribute("payMoney") BigDecimal payMoney,
                               @ModelAttribute("scoreRatio") Integer scoreRatio,
                               @ModelAttribute("payPwd") String payPwd,
                               @ModelAttribute("payType") Integer payType) {
        Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
        //  Users user =usersService.selectById(6237);
        if (user == null) {
            return AppResult.ERROR("您未登录", -2);
        }
        Users users=new Users();
        users.setUserPhone(userPhone);
        EntityWrapper<Users> entityWrapper=new  EntityWrapper<Users>(users);
        Users users1 = usersService.selectOne(entityWrapper);
        if(users1.getIsOperation()!=1){
            return AppResult.ERROR("该功能已关闭",-1);
        }
        if (user.getUserId().equals(users1.getUserId())){
            return AppResult.ERROR("您不能给自己付款", -1);
        }
        JSONObject jsonObject = accountService.personPay(userPhone, payMoney, scoreRatio, payPwd, payType, user.getUserId());
        if ("00".equals(jsonObject.get("code").toString())){
            return AppResult.ERROR(jsonObject.get("msg").toString(), -1);
        }
        return AppResult.OK(jsonObject);
    }

    /**
     * 获取某一分类下商品列表(平台)
     *
     * @param goodsCatIdPath
     * @param pageIndex
     * @param order
     * @return
     */
    @RequestMapping("/Goods/goodsSort")
    public AppResult goodsSort(@ModelAttribute("goodsCatIdPath") String goodsCatIdPath,
                               @ModelAttribute("pageIndex") String pageIndex,
                               @ModelAttribute("order") String order) {
//      Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
//      if(user==null){
//          return AppResult.ERROR("您未登录",-2);
//      }
//        SktShops sktShops=new SktShops();
//        sktShops.setUserId(user.getUserId());
//        EntityWrapper<SktShops> entityWrapper=new EntityWrapper<SktShops>(sktShops);
//        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
//        if (sktShops1==null){
//            return AppResult.ERROR("无商家信息");
//        }
//        int shopId =sktShops1.getShopId();
        //       int shopId=185;
        Integer pageIndexs=1;
        if (!"".equals(pageIndex) && pageIndex!=null){
            pageIndexs=Integer.parseInt(pageIndex);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("catId", goodsCatIdPath);
        map.put("order", order);
//        map.put("shopId",shopId);
        PageHelper.startPage(pageIndexs, 10);
        List<Map<String, Object>> maps = goodsService.selectAppByOrder(map);
        JSONObject json = new JSONObject();
        if (maps != null && maps.size() > 0) {
            PageInfo pageInfo = new PageInfo(maps);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", maps);
        return AppResult.OK(json);
    }

    /**
     * 推荐/热销/新品/精品列表
     *
     * @param type
     * @param pageIndex
     * @param order
     * @return
     */
    @RequestMapping("/goods/okGoods")
    public AppResult okGoods(@ModelAttribute("type") String type,
                             @ModelAttribute("pageIndex") String pageIndex,
                             @ModelAttribute("order") String order
                            ) {
        Integer pageIndexs=1;
        if (!"".equals(pageIndex) && pageIndex!=null){
            pageIndexs=Integer.parseInt(pageIndex);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("order", order);
        PageHelper.startPage(pageIndexs, 10);
        List<Map<String, Object>> maps = goodsService.selectRecomGoodsSelf(map);
        JSONObject json = new JSONObject();
        if (maps != null && maps.size() > 0) {
            PageInfo pageInfo = new PageInfo(maps);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", maps);
        return AppResult.OK(json);
    }
//    @RequestMapping("/goods/okGoods")
//    public AppResult okGoods(@ModelAttribute("type") String type,
//                             @ModelAttribute("pageIndex") Integer pageIndex,
//                             @ModelAttribute("order") String order,
//                             @ModelAttribute("shopId") String shopId) {
//      Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
//      if(user==null){
//          return AppResult.ERROR("您未登录",-2);
//      }
//        SktShops sktShops=new SktShops();
//        sktShops.setUserId(user.getUserId());
//        EntityWrapper<SktShops> entityWrapper=new EntityWrapper<SktShops>(sktShops);
//        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
//        if (sktShops1==null){
//            return AppResult.ERROR("无商家信息");
//        }
//        int shopId =sktShops1.getShopId();
//        int shopId=185;
//        Map<String, Object> map = new HashMap<>();
//        String[] strings = type.split(",");
//        String style = "";
//        if (strings.length > 0) {
//            style = strings[0];
//            if ("isNew".equals(style)) {
//                map.put("dataType", 3);
//            }
//            if ("isHot".equals(style)) {
//                map.put("dataType", 1);
//            }
//            if ("isRecom".equals(style)) {
//                map.put("dataType", 0);
//            }
//            if ("isBest".equals(style)) {
//                map.put("dataType", 2);
//            }
//        }
//        if (strings.length == 2) {
//            map.put("isSelf", 1);
//        }
//
//
//        map.put("order", order);
//        map.put("shopId", shopId);
//        PageHelper.startPage(pageIndex, 10);
//        List<Map<String, Object>> maps = goodsService.selectRecomGoods(map);
//        JSONObject json = new JSONObject();
//        if (maps != null && maps.size() > 0) {
//            PageInfo pageInfo = new PageInfo(maps);
//            json.put("tatolPage", pageInfo.getPages());
//        } else {
//            json.put("tatolPage", 1);
//        }
//        json.put("maps", maps);
//        return AppResult.OK(json);
//    }
    /**
     * 自营店
     * @param type
     * @param pageIndex
     * @param order
     * @return
     */
    @RequestMapping("/goods/okGoodsSelf")
    public AppResult okGoods(@ModelAttribute("type") String type,
                             @ModelAttribute("pageIndex") String pageIndex,
                             @ModelAttribute("order") String order,
                             @ModelAttribute("shopId") String shopId){
    	 Map<String, Object> map = new HashMap<>();
    	 if (!"".equals(shopId) && shopId!=null){
             map.put("shopId", shopId);
         }
         if ("isSelf".equals(type)) {
             map.put("isSelf", 1);
         }
        int i =1;
         if(!"".equals(pageIndex) && pageIndex!=null){
             i=  Integer.parseInt(pageIndex);
         }
         map.put("order", order);
    	PageHelper.startPage(i, 10);
        List<Map<String, Object>> maps = goodsService.selectRecomGoodsSelf(map);
        JSONObject json = new JSONObject();
        if (maps != null && maps.size() > 0) {
            PageInfo pageInfo = new PageInfo(maps);
            json.put("tatolPage", pageInfo.getPages());
        } else {
            json.put("tatolPage", 1);
        }
        json.put("maps", maps);
        return AppResult.OK(json);
    }
    /**
     * 公司新闻
     *
     * @param page
     * @param pageSize
     * @return
     */
    @RequestMapping("/article/companyNew")
    public AppResult articleCompanyNew(@ModelAttribute(value = "page") String page,
                                       @ModelAttribute(value = "pageSize") String pageSize) {

        try {
            Integer pageIndexs=1;
            if (!"".equals(page) && page!=null){
                pageIndexs=Integer.parseInt(page);
            }
            Integer  pageSizes = 10;
            if (!"".equals(pageSize) && pageSizes!=null){
                pageSizes=Integer.parseInt(pageSize);
            }
            PageHelper.startPage(pageIndexs, pageSizes);
            Map<String, Object> retMap = new HashMap<String, Object>();
            List<Map<String, Object>> list = iArticlesService.articleCompanyNew();
            if (list != null && list.size() > 0) {
                PageInfo pg = new PageInfo<>(list);
                retMap.put("totalPage", pg.getPages());
            } else {
                retMap.put("totalPage", 1);
            }
            retMap.put("maps", list);
            return AppResult.OK(retMap);
        } catch (Exception e) {
            e.printStackTrace();
            return AppResult.ERROR("查询出错", -1);
        }
    }

    /**
     * 公司新闻id查询
     *
     * @return
     */
    @RequestMapping("/article/companyNewById")
    public AppResult companyNewById(@ModelAttribute(value = "articleId") Integer articleId) {
        Map<String, Object> map = iArticlesService.companyNewById(articleId);
        return AppResult.OK("查询成功", map);
    }

    /**
     * app弹窗
     * @return
     */
    @RequestMapping("/site/getAppPopUp")
    @ResponseBody
    public AppResult getAppPopUp(){
        Map<String, Object> map = new HashMap<>();
        map.put("pagesize", "1");
        map.put("positionCode", "app-2-1");
        List<Map<String, Object>> adses = adsService.getSiteHome(map);
        return   AppResult.OK(adses);

    }
}
