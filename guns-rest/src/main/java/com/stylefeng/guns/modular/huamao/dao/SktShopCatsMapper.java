package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopCats;

/**
 * <p>
 * 店铺分类表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-29
 */
public interface SktShopCatsMapper extends BaseMapper<SktShopCats> {
	
	public List<SktShopCats> selectByParentId(@Param(value="goodsCatsId")Integer goodsCatsId);

	public List<SktShopCats> selectShopIdList(@Param(value="shopId")Integer shopId,
			@Param(value="parentId")Integer parentId);

}
