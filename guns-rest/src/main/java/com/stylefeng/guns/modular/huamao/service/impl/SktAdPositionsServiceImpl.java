package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAdPositionsMapper;
import com.stylefeng.guns.modular.huamao.model.SktAdPositions;
import com.stylefeng.guns.modular.huamao.service.ISktAdPositionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 广告位置表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@Service
public class SktAdPositionsServiceImpl extends ServiceImpl<SktAdPositionsMapper, SktAdPositions> implements ISktAdPositionsService {
	@Autowired
	private SktAdPositionsMapper sktAdPositionsMapper;
	@Override
	public List<SktAdPositions> sktAdPositionsfinAll(SktAdPositions sktAdPositions) {
		return sktAdPositionsMapper.sktAdPositionsfinAll(sktAdPositions);
	}
	@Override
	public List<SktAdPositions> sktAdPositionsfindLeft(Map<String,Object> map){
		return sktAdPositionsMapper.sktAdPositionsfindLeft(map);
	}
@Override
	public List<SktAdPositions>	sktAdPositionsfindBottom (Map<String,Object> map){
		return sktAdPositionsMapper.sktAdPositionsfindBottom(map);
	}

}
