package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.HomeMenus;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 前台菜单表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-28
 */
public interface HomeMenusMapper extends BaseMapper<HomeMenus> {
	List<Map<String,Object>> selectHomeMenus(HomeMenus homeMenus);
}
