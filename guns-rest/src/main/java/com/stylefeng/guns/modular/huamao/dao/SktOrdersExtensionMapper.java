package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import com.stylefeng.guns.modular.huamao.model.SktOrdersExtension;

import java.util.Map;

/**
 * <p>
  * 线上商城订单扩展表 Mapper 接口
 * </p>
 *
 * @author caody123
 * @since 2018-05-18
 */
public interface SktOrdersExtensionMapper extends BaseMapper<SktOrdersExtension> {


    void updateOrdersExtensionByOrderId(Map<String, Object> maps);
}