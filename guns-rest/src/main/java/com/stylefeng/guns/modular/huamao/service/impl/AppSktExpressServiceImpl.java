package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktExpressMapper;
import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.stylefeng.guns.modular.huamao.service.IAppSktExpressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppSktExpressServiceImpl extends ServiceImpl<SktExpressMapper,SktExpress> implements IAppSktExpressService {
    @Autowired
    private SktExpressMapper sktExpressMapper;

    @Override
    public SktExpress selectExpressById(Integer expressId) {
        return sktExpressMapper.selectExpressById(expressId);
    }
}
