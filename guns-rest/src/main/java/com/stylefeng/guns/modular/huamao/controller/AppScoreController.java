package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.GetDateUtil;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@CrossOrigin
@SuppressWarnings("all")
@Controller
@RequestMapping("/app")
public class AppScoreController extends BaseController {

    @Autowired
    private IAppSktSysConfigService iAppSktSysConfigService;
    @Autowired
    private IAppUserService iAppUserService;
    @Autowired
    private IAppLogScoreService iAppLogScoreService;
    @Autowired
    private IAppLogStockScoreService iAppLogStockScoreService;
    @Autowired
    private IAppAccountService iAppAccountService;
    @Autowired
    private IAppAccountShopService iAppAccountShopService;
    @Autowired
    private IAppLogScoreFreezeService iAppLogScoreFreezeService;
    @Autowired
    private IAppLogScoreSpecialService iAppLogScoreSpecialService;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private ISktShopsService sktShopsService;
    @Autowired
    private IUsersService usersService;
    @Autowired
    private IAppOrdersStockScoreService iAppOrdersStockScoreService;
    @Autowired
    private IAppLogCashService iAppLogCashService;
    @Autowired
    private IAppLogKaiyuanService iAppLogKaiyuanService;
    @Autowired
    private IAppLogKaiyuanTurnoverService iAppLogKaiyuanTurnoverService;
    /**
     * 当日账单 OR 积分记录
     * @param page
     * @param pageSize
     * @param timeType
     * @return
     */
    @RequestMapping("/score/score")
    @ResponseBody
    public AppResult score(@ModelAttribute("page")String page,@ModelAttribute("pageSize")String pageSize,@ModelAttribute("timeType")String timeType){
        try{
            Integer pageSizes=pageSize.equals("")?10:Integer.parseInt(pageSize);
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            Map<String, Object> map = new HashMap<>();
            map.put("userId",user.getUserId());
            map.put("page",page.equals("")?(1-1)*pageSizes:(Integer.parseInt(page)-1)*pageSizes);
            map.put("pageSize",pageSizes);
            Double totalScore=0.00;
            Map<String,Object> mapss= new HashMap<>();
            List<LogScore> logScoreList=iAppLogScoreService.selectLogScoreByUserId(map);
            String gType="log_score";
            if (!timeType.equals("")){
                List list = new ArrayList<>();
                for (LogScore logScore:logScoreList) {
                    Map<String, Object> maps = new HashMap<>();
                    maps.put("scoreType",logScore.getScoreType()+"");
                    maps.put("score",logScore.getScoreType().intValue()==1? "+"+logScore.getScore():"-"+logScore.getScore());
                    maps.put("createTime",logScore.getCreateTime());
                    AppTradeDictDTD appTradeDictDTD=iAppLogScoreService.selectTradeDictBygType(gType,logScore.getType());
                    maps.put("type",appTradeDictDTD.gettName());
                    maps.put("orderNo",logScore.getOrderNo());
                    list.add(maps);
                }
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Map<String,Object> mapTime = GetDateUtil.getDateTime(timeType);
                map.put("beginTime",format.format(mapTime.get("beginTime")));
                map.put("endTime",format.format(mapTime.get("endTime")));
                totalScore= iAppLogScoreService.selectSumScoreByUserId(map);
                mapss.put("totalScore",totalScore==null?0.00:totalScore);
                mapss.put("data",list);
                return AppResult.OK("获取成功",mapss);
            }else {
                List list = new ArrayList<>();
                for (LogScore logScore:logScoreList) {
                    Map<String, Object> maps = new HashMap<>();
                    maps.put("scoreType",logScore.getScoreType()+"");
                    maps.put("score",logScore.getScoreType().intValue()==1? "+"+logScore.getScore():"-"+logScore.getScore());
                    maps.put("createTime",logScore.getCreateTime());
                    AppTradeDictDTD appTradeDictDTD=iAppLogScoreService.selectTradeDictBygType(gType,logScore.getType());
                    maps.put("type",appTradeDictDTD.gettName());
                    maps.put("orderNo",logScore.getOrderNo());
                    list.add(maps);
                }
                return AppResult.OK("获取成功",list);
            }
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("获取失败",-2);
        }
    }

    /**
     *  库存积分记录
     * @param page
     * @param pageSize
     * @return
     */
    @RequestMapping("/score/stockScore")
    @ResponseBody
    public AppResult stockScore(@ModelAttribute("page")String page,@ModelAttribute("pageSize")String pageSize){
        try {
        Integer pageSizes=pageSize.equals("")?10:Integer.parseInt(pageSize);
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        Map<String, Object> map = new HashMap<>();
        map.put("userId",user.getUserId());
        map.put("page",page.equals("")?(1-1)*pageSizes:(Integer.parseInt(page)-1)*pageSizes);
        map.put("pageSize",pageSizes);
        List<LogStockscore> logStockscoresList=iAppLogStockScoreService.selectLogStockScoreByUserId(map);
        String gType="log_stockscore";
        List<Object> list = new ArrayList<>();
        for (LogStockscore logStockscore:logStockscoresList) {
            Map<String, Object> maps = new HashMap<>();
            maps.put("scoreType",logStockscore.getScoreType()+"");
            maps.put("score",logStockscore.getScoreType().intValue()==1? "+"+logStockscore.getScore():"-"+logStockscore.getScore());
            maps.put("createTime",logStockscore.getCreateTime());
            AppTradeDictDTD appTradeDictDTD=iAppLogScoreService.selectTradeDictBygType(gType,logStockscore.getType());
            maps.put("type",appTradeDictDTD.gettName());
            maps.put("orderNo",logStockscore.getOrderNo());
            list.add(maps);
        }
        return AppResult.OK("获取成功",list);
        }catch (Exception e){
            e.printStackTrace();
            AppResult.ERROR("对不起，您还未登陆",-2);
        }
        return AppResult.ERROR("获取失败",-2);
    }

    /**
     * 分发积分（1）
     * @return
     */
    @RequestMapping("/score/distributeScore")
    @ResponseBody
    public AppResult distributeScore(){
        try{
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        Account account = iAppAccountService.selectAccountByUserId(user.getUserId());
        Map<String,Object> distributeScore=iAppAccountShopService.selectScoreByShopId(account.getShopId());
        distributeScore.put("stockScore",String.valueOf(distributeScore.get("stockScore")));
        distributeScore.put("scoreRatio",distributeScore.get("scoreRatio"));
        return  AppResult.OK("成功",distributeScore);
        }catch (Exception e){
            e.printStackTrace();
            AppResult.ERROR("对不起，您还未登录",-2);
        }
        return AppResult.ERROR("失败",-1);
    }

    /**
     * 待发积分明细
     * @param page
     * @param pageSize
     * @return
     */
    @RequestMapping("/score/freezeScore")
    @ResponseBody
    public AppResult freezeScore(@ModelAttribute("page")String page,@ModelAttribute("pageSize")String pageSize){
        try {
            Integer pageSizes=pageSize.equals("")?10:Integer.parseInt(pageSize);
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            Map<String, Object> map = new HashMap<>();
            map.put("userId",user.getUserId());
            map.put("page",page.equals("")?(1-1)*pageSizes:(Integer.parseInt(page)-1)*pageSizes);
            map.put("pageSize",pageSizes);
            List<LogScoreFreeze> logScoreFreezeList=iAppLogScoreFreezeService.selectLogScoreFreezeByUserId(map);
            String gType="log_score_freeze";
            List<Object> list = new ArrayList<>();
            for (LogScoreFreeze logScoreFreeze:logScoreFreezeList) {
                Map<String, Object> maps = new HashMap<>();
                maps.put("scoreType",logScoreFreeze.getScoreType()+"");
                maps.put("score",logScoreFreeze.getScoreType().intValue()==1? "+"+logScoreFreeze.getScore():"-"+logScoreFreeze.getScore());
                maps.put("createTime",logScoreFreeze.getCreateTime());
                AppTradeDictDTD appTradeDictDTD=iAppLogScoreService.selectTradeDictBygType(gType,logScoreFreeze.getType());
                maps.put("type",appTradeDictDTD.gettName());
                maps.put("orderNo",logScoreFreeze.getOrderNo());
                list.add(maps);
            }
            return AppResult.OK("获取成功",list);
        }catch (Exception e){
            e.printStackTrace();
            AppResult.ERROR("对不起，您还未登陆",-2);
        }
        return AppResult.ERROR("获取失败",-2);
    }


    /**
     * 待发特别积分记录
     * @param page
     * @param pageSize
     * @return
     */
    @RequestMapping("/score/specialScore")
    @ResponseBody
    public AppResult specialScore(@ModelAttribute("page")String page){
        try {
            Integer pageSizes=10;
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            Map<String, Object> map = new HashMap<>();
            map.put("userId",user.getUserId());
            map.put("page",page.equals("")?(1-1)*pageSizes:(Integer.parseInt(page)-1)*pageSizes);
            map.put("pageSize",pageSizes);
            List<LogScoreSpecial> logScoreSpecialsList=iAppLogScoreSpecialService.selectLogScoreSpecialByUserId(map);
            String gType="log_score_special";
            List<Object> list = new ArrayList<>();
            for (LogScoreSpecial logScoreSpecial:logScoreSpecialsList) {
                Map<String, Object> maps = new HashMap<>();
                maps.put("scoreType",logScoreSpecial.getScoreType()+"");
                maps.put("score",logScoreSpecial.getScoreType().intValue()==1? "+"+logScoreSpecial.getScore():"-"+logScoreSpecial.getScore());
                maps.put("createTime",logScoreSpecial.getCreateTime());
                AppTradeDictDTD appTradeDictDTD=iAppLogScoreService.selectTradeDictBygType(gType,logScoreSpecial.getType());
                maps.put("type",appTradeDictDTD.gettName());
                maps.put("orderNo",logScoreSpecial.getOrderNo());
                list.add(maps);
            }
            return AppResult.OK("获取成功",list);
        }catch (Exception e){
            e.printStackTrace();
            AppResult.ERROR("对不起，您还未登陆",-2);
        }
        return AppResult.ERROR("获取失败",-2);
    }

    /**
     * 购买积分（1）
     * @param payMoney
     * @return
     */
    @RequestMapping("/score/buySubmit")
    @ResponseBody
    public AppResult buySubmit( @ModelAttribute("payMoney") BigDecimal payMoney){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if (user==null){
         return    AppResult.ERROR("对不起，您还未登陆",-2);
        }else {
            Users users = usersService.selectById(user.getUserId());
            if(users.getIsOperation()!=1){
                return AppResult.ERROR("该功能已关闭",-1);
            }
            SktShops sktShops=new SktShops();
            sktShops.setUserId(user.getUserId());
            EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
            SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
            if(sktShops1==null){
                return   AppResult.ERROR("您还不是商家，无法申请");
            }
            AppResult json = iAppAccountService.isBuyAPPTimeScore(sktShops1.getShopId(), payMoney);
          return   json;
        }
    }

    /**
     * 分发积分（2）
     * @param payMoney  支付金额
     * @param scoreRatio  积分比例
     * @param checkData  买家信息(手机号)
     * @param orderRemarks  备注
     * @param payPwd  交易密码
     * @return
     */
    @RequestMapping("/score/distributePwd")
    @ResponseBody
    public AppResult distributePwd( @ModelAttribute("payMoney") BigDecimal payMoney,
            @ModelAttribute("scoreRatio") int scoreRatio,
            @ModelAttribute("checkData") String checkData,
            @ModelAttribute("orderRemarks") String orderRemarks,
            @ModelAttribute("payPwd") String payPwd){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if (user==null){
          return   AppResult.ERROR("对不起，您还未登陆",-2);
        }else {

            SktShops sktShops =new SktShops ();
            sktShops.setUserId(user.getUserId());
            EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
            SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
            EntityWrapper ew =new EntityWrapper();
            ew.setEntity(new Users());
            ew.where("userPhone={0}", checkData).or("loginName={0}", checkData);
            Users users = usersService.selectOne(ew);
            if(users.getIsOperation()!=1){
                return AppResult.ERROR("该功能已关闭",-1);
            }
            if(users==null){
                return   AppResult.ERROR("对不起，赠送买家不存在请核实买家信息");
            }
            if (users.getUserId()==user.getUserId()){
                return   AppResult.ERROR("对不起，您不能给自己赠送积分");
            }
            JSONObject json = accountService.sendScore(users.getUserId(), sktShops1.getShopId(), payMoney.multiply(new BigDecimal(scoreRatio)), payPwd, payMoney, scoreRatio, orderRemarks);
            return   AppResult.OK(json);
        }
    }


    /**
     * 购买积分（2）
     * @param payPwd
     * @param payType
     * @param orderNo
     * @return
     */
    @RequestMapping("/score/buyScore")
    @ResponseBody
    @Transactional
    public AppResult buyScore(@ModelAttribute("payPwd")String payPwd,@ModelAttribute("payType")String payType,@ModelAttribute("orderNo")String orderNo){
        try {
        int payTypes = Integer.parseInt(payType);
        if (payTypes==3 || payTypes==6 || payTypes==2){
            return AppResult.ERROR("该功能已关闭",-1);
        }
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user.getIsOperation()!=1){
            return AppResult.ERROR("该功能已关闭",-1);
        }
        if (user==null){
            return   AppResult.ERROR("对不起，您还未登陆",-2);
        }
        String type="BUY_SCORE";
        Users users = iAppUserService.selectUsersByUserId(user.getUserId());
        if (users.getUserStatus()!=1){
            return AppResult.ERROR("该账户已经被禁用",-1);
        }
        String checkPwd = payPwd + users.getLoginSecret();
        String encrypt = MD5Util.encrypt(checkPwd);
        if (!encrypt.equals(users.getPayPwd())){
            return AppResult.ERROR("支付密码错误",-1);
        }
        SktOrdersStockscore sktOrdersStockscore=new SktOrdersStockscore();
        sktOrdersStockscore.setOrderNo(orderNo);
        sktOrdersStockscore.setUserId(user.getUserId());
        SktOrdersStockscore sktOrdersStockscore1 = iAppOrdersStockScoreService.selectOrdersStockscoreByWhere(sktOrdersStockscore);
        if (MSGUtil.isBlank(sktOrdersStockscore1)){
            return AppResult.ERROR("该订单不存在",-1);
        }
        Map<String,Object> account_data= iAppAccountService.selectAccountAndAccountShopByUserId(user.getUserId());
        BigDecimal awardScore=sktOrdersStockscore1.getScore().setScale(3);
        Account account=new Account();
        SktOrdersStockscore sktOrdersStockscore2=new SktOrdersStockscore();
        LogCash logCash=new LogCash();
        LogKaiyuan logKaiyuan=new LogKaiyuan();
        LogKaiyuan logKaiyuanFreeze=new LogKaiyuan();
        AccountShop accountShop=new AccountShop();
        LogKaiyuanTurnover logKaiyuanTurnover=new LogKaiyuanTurnover();
        LogKaiyuanTurnover logKaiyuanTurnoverFee=new LogKaiyuanTurnover();
        BigDecimal payKaiyuan=new BigDecimal(0);
        BigDecimal payKaiyuanTurn=new BigDecimal(0);
        switch (payTypes){
            case 2:
                if (sktOrdersStockscore1.getTotalMoney().compareTo(new BigDecimal(account_data.get("cash").toString()))==1){
                    return AppResult.ERROR("现金余额不足",-1);
                }
                account.setAccountId(Integer.parseInt(account_data.get("accountId").toString()));
                account.setCash(sktOrdersStockscore1.getTotalMoney());
                account.setScore(awardScore);
                account.setTotalScore(awardScore);
                sktOrdersStockscore2.setOrderId(sktOrdersStockscore1.getOrderId());
                sktOrdersStockscore2.setOrderStatus(3);
                sktOrdersStockscore2.setPayType(2);
                sktOrdersStockscore2.setCash(sktOrdersStockscore1.getTotalMoney());
                sktOrdersStockscore2.setCheckTime(new Date());
                logCash.setType(32);
                logCash.setFromId(user.getUserId());
                logCash.setUserId(user.getUserId());
                logCash.setOrderNo(sktOrdersStockscore1.getOrderNo());
                logCash.setPreCash(new BigDecimal(account_data.get("cash").toString()));
                logCash.setCashType(-1);
                logCash.setCash(sktOrdersStockscore1.getTotalMoney());
                logCash.setRemark("商家现金余额申请积分");
                logCash.setCreateTime(new Date());
                break;
            case 3:
                SktSysConfigs feeRadio = iAppSktSysConfigService.selectSysConfByFieldCode("kaiyuanFee");
                SktSysConfigs taxRadio = iAppSktSysConfigService.selectSysConfByFieldCode("taxRatio");
                BigDecimal real=sktOrdersStockscore1.getTotalMoney().multiply(new BigDecimal(100)).setScale(3);
                BigDecimal fee=real.multiply(new BigDecimal(feeRadio.getFieldValue().toString()).divide(new BigDecimal(100))).setScale(3);
                BigDecimal tax=real.multiply(new BigDecimal(taxRadio.getFieldValue().toString()).divide(new BigDecimal(100))).setScale(3);
                BigDecimal kaiyuanFee=fee.add(tax);
                BigDecimal totalKaiYuan=real.add(kaiyuanFee);
                payKaiyuan=totalKaiYuan;
                if (totalKaiYuan.compareTo(new BigDecimal(account_data.get("kaiyuan").toString()))==1){
                    return AppResult.ERROR("华宝不足",-1);
                }
                account.setAccountId(Integer.parseInt(account_data.get("accountId").toString()));
                account.setKaiyuan(totalKaiYuan);
                account.setScore(awardScore);
                account.setTotalScore(awardScore);
                sktOrdersStockscore2.setOrderId(sktOrdersStockscore1.getOrderId());
                sktOrdersStockscore2.setOrderStatus(3);
                sktOrdersStockscore2.setPayType(3);
                sktOrdersStockscore2.setKaiyuan(real);
                sktOrdersStockscore2.setKaiyuanFee(kaiyuanFee);
                sktOrdersStockscore2.setCheckTime(new Date());
                logKaiyuan.setType(35);
                logKaiyuan.setFromId(user.getUserId());
                logKaiyuan.setUserId(user.getUserId());
                logKaiyuan.setOrderNo(orderNo);
                logKaiyuan.setPreKaiyuan(new BigDecimal(account_data.get("kaiyuan").toString()));
                logKaiyuan.setKaiyuanType(-1);
                logKaiyuan.setKaiyuan(real);
                logKaiyuan.setRemark("商家华宝申请积分");
                logKaiyuan.setCreateTime(new Date());
                logKaiyuanFreeze.setType(36);
                logKaiyuanFreeze.setFromId(user.getUserId());
                logKaiyuanFreeze.setUserId(user.getUserId());
                logKaiyuanFreeze.setOrderNo(orderNo);
                logKaiyuanFreeze.setPreKaiyuan(new BigDecimal(account_data.get("kaiyuan").toString()));
                logKaiyuanFreeze.setKaiyuanType(-1);
                logKaiyuanFreeze.setKaiyuan(kaiyuanFee);
                logKaiyuanFreeze.setRemark("商家华宝申请积分手续费和综合服务费率");
                logKaiyuanFreeze.setCreateTime(new Date());
                break;
            case 6:
                SktSysConfigs kaiyuanTurnRadio = iAppSktSysConfigService.selectSysConfByFieldCode("kaiyuanturnFee");
                BigDecimal real1=sktOrdersStockscore1.getTotalMoney().multiply(new BigDecimal(100)).setScale(3);
                BigDecimal fee1=real1.multiply(new BigDecimal(kaiyuanTurnRadio.getFieldValue().toString()).divide(new BigDecimal(100))).setScale(3);
                BigDecimal totalKaiyuanTurn=real1.add(fee1);
                payKaiyuanTurn=totalKaiyuanTurn;
                if (totalKaiyuanTurn.compareTo(new BigDecimal(account_data.get("kaiyuanTurnover").toString()))==1){
                    return AppResult.ERROR("华宝贷款不足",-1);
                }
                account.setAccountId(Integer.parseInt(account_data.get("accountId").toString()));
                account.setScore(awardScore);
                account.setTotalScore(awardScore);
                accountShop.setKaiyuanTurnover(new BigDecimal(account_data.get("kaiyuanTurnover").toString()).subtract(totalKaiyuanTurn).setScale(3,BigDecimal.ROUND_HALF_UP));
                sktOrdersStockscore2.setOrderId(sktOrdersStockscore1.getOrderId());
                sktOrdersStockscore2.setOrderStatus(3);
                sktOrdersStockscore2.setPayType(6);
                sktOrdersStockscore2.setKaiyuan(real1);
                sktOrdersStockscore2.setKaiyuanFee(fee1);
                sktOrdersStockscore2.setCheckTime(new Date());
                logKaiyuanTurnover.setType(33);
                logKaiyuanTurnover.setFromId(user.getUserId());
                logKaiyuanTurnover.setUserId(user.getUserId());
                logKaiyuanTurnover.setOrderNo(orderNo);
                logKaiyuanTurnover.setPreKaiyuan(new BigDecimal(account_data.get("kaiyuanTurnover").toString()));
                logKaiyuanTurnover.setKaiyuanType(-1);
                logKaiyuanTurnover.setKaiyuan(real1);
                logKaiyuanTurnover.setRemark("商家华宝营业额申请积分");
                logKaiyuanTurnover.setCreateTime(new Date());
                logKaiyuanTurnoverFee.setType(34);
                logKaiyuanTurnoverFee.setFromId(user.getUserId());
                logKaiyuanTurnoverFee.setUserId(user.getUserId());
                logKaiyuanTurnoverFee.setOrderNo(orderNo);
                logKaiyuanTurnoverFee.setPreKaiyuan(new BigDecimal(account_data.get("kaiyuanTurnover").toString()).subtract(totalKaiyuanTurn).setScale(3,BigDecimal.ROUND_HALF_UP));
                logKaiyuanTurnoverFee.setKaiyuanType(-1);
                logKaiyuanTurnoverFee.setKaiyuan(fee1);
                logKaiyuanTurnoverFee.setRemark("商家华宝营业额申请积分手续费");
                logKaiyuanTurnoverFee.setCreateTime(new Date());
                break;
        }
        LogStockscore logStockscore=new LogStockscore();
            logStockscore.setType(1);
            logStockscore.setFromId(user.getUserId());
            logStockscore.setUserId(user.getUserId());
            logStockscore.setOrderNo(orderNo);
            logStockscore.setPreScore(new BigDecimal(account_data.get("stockScore").toString()));
            logStockscore.setScoreType(1);
            logStockscore.setScore(sktOrdersStockscore1.getScore());
            logStockscore.setRemark("商家购买库存积分");
            logStockscore.setCreateTime(new Date());
        LogScore logScore=new LogScore();
            logScore.setType(3);
            logScore.setFromId(user.getUserId());
            logScore.setUserId(user.getUserId());
            logScore.setOrderNo(orderNo);
            logScore.setPreScore(new BigDecimal(account_data.get("score").toString()));
            logScore.setScoreType(1);
            logScore.setScore(awardScore);
            logScore.setRemark("商家购买库存积分获得积分");
            logScore.setCreateTime(new Date());
            accountShop.setAccountId(Integer.parseInt(account_data.get("accountShopId").toString()));
            accountShop.setStockScore(sktOrdersStockscore1.getScore());
            accountShop.setTotalStockScore(new BigDecimal(account_data.get("score").toString()));
            boolean b=iAppAccountService.updateAccountById(account);
            boolean b1=iAppAccountShopService.updateAccountShopById(accountShop);
            sktOrdersStockscore2.setCreateTime(new Date());
            iAppOrdersStockScoreService.updateById(sktOrdersStockscore2);
            iAppLogStockScoreService.insert(logStockscore);
            iAppLogScoreService.insert(logScore);
            switch (payTypes){
                case 2:
                    iAppLogCashService.insert(logCash);
                    break;
                case 3:
                    iAppLogKaiyuanService.insert(logKaiyuan);
                    iAppLogKaiyuanService.insert(logKaiyuanFreeze);
                    break;
                case 6:
                    iAppLogKaiyuanTurnoverService.insert(logKaiyuanTurnover);
                    iAppLogKaiyuanTurnoverService.insert(logKaiyuanTurnoverFee);
                    break;
            }
            Map<String, Object> map = new HashMap<>();
            map.put("orderNo",sktOrdersStockscore1.getOrderNo());
            map.put("payMoney",sktOrdersStockscore1.getTotalMoney());
            map.put("kaiyuan",payKaiyuan);
            map.put("kaiyuanTurn",payKaiyuanTurn);
            map.put("createTime",new Date());
            return AppResult.OK("购买成功",map);
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}
