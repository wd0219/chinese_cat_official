package com.stylefeng.guns.modular.huamao.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.*;

/**
 * <p>
 * 会员账户表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-11
 */
public interface AccountMapper extends BaseMapper<Account> {
    // 展示用户账户列表
    public List<AccountList> selectAccountAll(AccountList accountList);

    /**
     * 审核通过退款底单时，账户余额发生的改变
     *
     * @param AccountList
     * @return
     */
    public Object updateOrderRefunds(AccountList AccountList);

    /**
     * 根据用户id查询账户
     */
    public Account selectAccountByuserId(Integer userId);

    /**
     * 修改用户账户信息
     *
     * @param account
     * @return
     */
    public boolean updateAccount(Account account);

    // 操作前金额要到数据库查询account表中的实时金额
    public Account findScoreById(Integer userId);

    public void addScore(LogScore logscore);

    public Integer updateByUserId(Account account);

    // 根据userId查询待发
    public Account findFreezeScoreById(Integer userId);

    public Integer updateByUserId2(Account account);

    public Integer updateByUserId3(Account account);

    // 修改代发现金
    public Integer updateFreezeCash(Account account);

    /**
     * @param userId
     * @return
     */
    String selectSpSorceByUserIdline(Integer userId);

    String selectSpSorceByUserIdUm(Integer userId);


    //修改现金
    public Integer updateCash(Account account);


    Integer addFreezeScore(Account account);

    Integer updateScore(Account account);
    public BigDecimal getTodayRecharge(Integer userId);
    public BigDecimal getMouthRecharge(Integer userId);
    public BigDecimal getTotalRecharge(Integer userId);

    public BigDecimal getTodayDraws(Integer userId);
    public BigDecimal getMouthDraws(Integer userId);
    public BigDecimal getTotalDraws(Integer userId);

    public BigDecimal getTodayScore(Integer userId);
    public BigDecimal getMouthScore(Integer userId);
    public BigDecimal getTotalScore(Integer userId);

    public BigDecimal getTodayHuabao(Integer userId);
    public BigDecimal getMouthHuabao(Integer userId);
    public BigDecimal getTotalHuabao(Integer userId);

    public BigDecimal getTodayOrders(Integer userId);
    public BigDecimal getMouthOrders(Integer userId);
    public BigDecimal getTotalOrders(Integer userId);

    public BigDecimal getTodayOrdersStores(Integer userId);
    public BigDecimal getMouthOrdersStores(Integer userId);
    public BigDecimal getTotalOrdersStores(Integer userId);



    Integer updateKaiyuanById(Account account);

    Integer updateScoreToKaiyuan(Account account);
    /*前七个工作日*/
     List<SktDate> getSevenWorkDay(Integer date);

    Account getByUserId(Integer userId);

    Integer updateBonusScore(Account account);
    /*后七个工作日*/
    List<SktDate> getAfterSevenWorkDay(Map<String ,Object> map);


    MyAccountHuabao myHuaBao(Integer userId);

    Account scoreIndex(Integer userId);

    Map<String,Object> appFindAccountByUserId(Integer userId);

    int uptateKaiyuanByUserId(Map<String, Object> map);

    int uptateCashByUserId(Map<String, Object> cashMap);

    Map<String,Object> selectAccountAndAccountShopByUserId(Integer userId);

    int updateAccountById(Account account);
    Account selectAccountByUserPhone(Map<String,Object> map);
}
