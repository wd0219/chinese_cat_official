package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.AppUserAddressDTO;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.model.UsersAddressDTO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 会员地址表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
public interface UsersAddressMapper extends BaseMapper<UsersAddress> {
	/**
	 * 查询用户地址列表
	 * @return
	 */
	public List<UsersAddressDTO> showUserAddressInfo(UsersAddressDTO usersAddressDTO);

	AppUserAddressDTO selectUserAddressByUserId(@Param("userId") Integer userId);

	UsersAddress selectUserAddressByData(UsersAddress usersAddress);

	List<AppUserAddressDTO> selectUserAddressByPageUserId(Map<String, Object> map);

    void updateUserAddressUsersIsDefault(Integer userId);

    AppUserAddressDTO appFindAddress(Integer userId);
}
