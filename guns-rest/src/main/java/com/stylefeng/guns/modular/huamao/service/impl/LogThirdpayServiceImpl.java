package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogThirdpayMapper;
import com.stylefeng.guns.modular.huamao.model.LogThirdpay;
import com.stylefeng.guns.modular.huamao.model.LogThirdpayList;
import com.stylefeng.guns.modular.huamao.service.ILogThirdpayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 第三方支付记录表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
@Service
public class LogThirdpayServiceImpl extends ServiceImpl<LogThirdpayMapper, LogThirdpay> implements ILogThirdpayService {

	@Autowired
	private LogThirdpayMapper ltm;

	// 第三方支付记录表展示
	@Override
	public List<LogThirdpayList> slectLogThirdpayAll(LogThirdpayList logThirdpayList) {
		return ltm.selectLogThirdpayAll(logThirdpayList);
	}

}
