package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.stylefeng.guns.modular.huamao.dao.SktOrdersExtensionMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrdersExtension;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersExtensionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 线上商城订单扩展表 服务实现类
 * </p>
 *
 * @author caody123
 * @since 2018-05-18
 */
@Service
public class SktOrdersExtensionServiceImpl extends ServiceImpl<SktOrdersExtensionMapper, SktOrdersExtension> implements ISktOrdersExtensionService {
	
}
