package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.CheckResultMapper;
import com.stylefeng.guns.modular.huamao.model.CheckResult;
import com.stylefeng.guns.modular.huamao.model.CheckResultList;
import com.stylefeng.guns.modular.huamao.service.ICheckResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 每日总对账数据表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-14
 */
@Service
public class CheckResultServiceImpl extends ServiceImpl<CheckResultMapper, CheckResult> implements ICheckResultService {

	@Autowired
	CheckResultMapper crm;

	// 展示平台每日对账列表
	@Override
	public List<CheckResultList> selectCheckResultAll(CheckResultList checkResultList) {
		return crm.selectCheckResultAll(checkResultList);
	}

	// 通过id查询详情
	@Override
	public CheckResultList selectctDetailedById(Integer checkResultId) {
		return crm.selectctDetailedById(checkResultId);
	}

}
