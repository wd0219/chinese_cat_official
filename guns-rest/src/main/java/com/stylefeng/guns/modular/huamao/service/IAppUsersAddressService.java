package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AppUserAddressDTO;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.result.AppResult;

import java.util.List;
import java.util.Map;

public interface IAppUsersAddressService extends IService<UsersAddress> {

    AppUserAddressDTO selectUserAddressByUserId(Integer userId);

    UsersAddress selectUserAddressByData(UsersAddress usersAddress);

    List<AppUserAddressDTO> selectUserAddressByPageUserId(Map<String, Object> map);

    AppResult AddressArea(String areaId);

    AppResult AddressDel(String addressId);

    void updateUserAddressUsersIsDefault(Integer userId);


}
