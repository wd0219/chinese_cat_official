package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class SktAgentsDTO {
	
	 private Integer userId;
	 private String loginPwd;
	 private String newPwd;
	 private Integer agentId;
    /**
     * 代理名称
     */
    private String agentName;
    /**
     * 代理级别：10为省级 ，11为市级 ，12为区/县
     */
    private Integer alevel;
    /**
     * 地区
     */
    private String areas;
    /**
     * 省id
     */
    private Integer province;
    /**
     * 市id
     */
    private Integer citys;
    /**
     * 区县id
     */
    private Integer county;
    /**
     * 每股缴纳的代理费
     */
    private Integer agencyFee;
    /**
     * 代理公司总股份数量
     */
    private Integer totalStockNum;
    /**
     * 代理公司股东总人数
     */
    private Integer totalNum;
    /**
     * 完成目标辖区商家数量
     */
    private Integer totalStoreNum;
    /**
     * 股东完成度
     */
    private Integer gdNum;
    /**
     * 商家完成度
     */
    private Integer gxNum;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 成为股东的分红系数
     */
    private BigDecimal oneRatio;
    /**
     * 完成70%(落地)的分红系数
     */
    private BigDecimal twoRatio;
    /**
     * 全部完成的分红系数
     */
    private BigDecimal threeRatio;
    /**
     * 账号状态：0停用 1启用
     */
    private Integer agentStatus;
    /**
     * 创建时间
     */
    private Date createTime;
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public Integer getAlevel() {
		return alevel;
	}
	public void setAlevel(Integer alevel) {
		this.alevel = alevel;
	}
	public String getAreas() {
		return areas;
	}
	public void setAreas(String areas) {
		this.areas = areas;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	public Integer getCitys() {
		return citys;
	}
	public void setCitys(Integer citys) {
		this.citys = citys;
	}
	public Integer getCounty() {
		return county;
	}
	public void setCounty(Integer county) {
		this.county = county;
	}
	public Integer getAgencyFee() {
		return agencyFee;
	}
	public void setAgencyFee(Integer agencyFee) {
		this.agencyFee = agencyFee;
	}
	public Integer getTotalStockNum() {
		return totalStockNum;
	}
	public void setTotalStockNum(Integer totalStockNum) {
		this.totalStockNum = totalStockNum;
	}
	public Integer getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}
	public Integer getTotalStoreNum() {
		return totalStoreNum;
	}
	public void setTotalStoreNum(Integer totalStoreNum) {
		this.totalStoreNum = totalStoreNum;
	}
	public Integer getGdNum() {
		return gdNum;
	}
	public void setGdNum(Integer gdNum) {
		this.gdNum = gdNum;
	}
	public Integer getGxNum() {
		return gxNum;
	}
	public void setGxNum(Integer gxNum) {
		this.gxNum = gxNum;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public BigDecimal getOneRatio() {
		return oneRatio;
	}
	public void setOneRatio(BigDecimal oneRatio) {
		this.oneRatio = oneRatio;
	}
	public BigDecimal getTwoRatio() {
		return twoRatio;
	}
	public void setTwoRatio(BigDecimal twoRatio) {
		this.twoRatio = twoRatio;
	}
	public BigDecimal getThreeRatio() {
		return threeRatio;
	}
	public void setThreeRatio(BigDecimal threeRatio) {
		this.threeRatio = threeRatio;
	}
	public Integer getAgentStatus() {
		return agentStatus;
	}
	public void setAgentStatus(Integer agentStatus) {
		this.agentStatus = agentStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getLoginPwd() {
		return loginPwd;
	}
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}
	public String getNewPwd() {
		return newPwd;
	}
	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}
}
