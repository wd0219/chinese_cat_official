package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.CheckResult;
import com.stylefeng.guns.modular.huamao.model.CheckResultList;

import java.util.List;

/**
 * <p>
 * 每日总对账数据表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-14
 */
public interface ICheckResultService extends IService<CheckResult> {

	// 展示平台每日对账列表
	public List<CheckResultList> selectCheckResultAll(CheckResultList checkResultList);

	// 通过id查询详情
	public CheckResultList selectctDetailedById(Integer checkResultId);

}
