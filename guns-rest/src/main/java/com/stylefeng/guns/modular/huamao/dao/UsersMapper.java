package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.stylefeng.guns.modular.huamao.model.*;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-10
 */
public interface UsersMapper extends BaseMapper<Users> {
	/**
	 * 查询显示页面数据
	 * 
	 * @param usersDTO
	 * @return
	 */
	public List<UsersDTO> selectUsersInfo(UsersDTO usersDTO);

	/**
	 * 修改显示页面数据
	 * 
	 * @param usersDTO
	 * @return
	 */
	public UsersDTO selectOneUsersInfo(UsersDTO usersDTO);

	/**
	 * 更新用户数据
	 * 
	 * @param usersDTO
	 */
	public void updateUsersInfo(UsersDTO usersDTO);

	/**
	 * 查询推荐人列表
	 * 
	 * @param inviteId
	 * @return
	 */
	public List<UsersDTO> selectTuiUsersInfo(Integer inviteId);

	/**
	 * 查询推荐人用户电话
	 * 
	 * @param usersPhone
	 * @param usersId
	 * @return
	 */
	public List<Users> selectTuiPhone(@Param(value = "usersPhone") String usersPhone,
			@Param(value = "usersId") Integer usersId);

	/**
	 * 查询推荐人登录名
	 * 
	 * @param usersName
	 * @param usersId
	 * @return
	 */
	public List<Users> selectTuiName(@Param(value = "usersName") String usersName,
			@Param(value = "usersId") Integer usersId);

	/**
	 * 查询今日新增总数
	 * 
	 * @param
	 * @return
	 */
	public int selectCount();

	/**
	 * 查询总数
	 * 
	 * @param
	 * @return
	 */
	public int getTotal();

	/**
	 * 查询用户id电话 姓名身份证号 用户类型 推荐人 代理股东 线上线下商家 是不是特权账号 签约卡的手机号
	 * 
	 * @param users
	 * @return
	 */
	public Map<String, Object> selectUsersPhone(Users users);

	/**
	 * 用户登录
	 * 
	 * @param loginName
	 * @param loginPwd
	 * @return
	 */
	public UsersLoginDTO userLogin(Users users);

	/**
	 * 查询用户
	 * 
	 * @param shareMan
	 * @return
	 */
	Users selectUserByLoginNameOrUserPhone(@Param(value="loginName")String loginName);

	/**
	 * 查询用户根据支付密码和手机号
	 * @param users
	 * @return
	 */
	Users selectUsersByUserPhoneAndPayPwd(Users users);

	/**
	 * 核实用户身份
	 * 
	 * @param userPhoneOrloginName
	 * @return
	 */
	public Users checkUserInfo(@Param(value = "userPhoneOrloginName") String userPhoneOrloginName);

	/**
	 * 查询用户注册信息
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> selectByUsersInfo(Integer id);

	/**
	 * 查询用户注册信息
	 * 
	 * @param id
	 * @return
	 */
	public int updateUsersPwd(UsersDTO usersDTO);

	/**
	 * 用户名查询
	 * 
	 * @param loginName
	 * @return
	 */
	public List<Users> selectByLoginName(@Param(value = "loginName") String loginName,
			@Param(value = "userPhone") String userPhone);

	/**
	 * 根据用户名跟密码查询用户
	 * 
	 * @param users
	 * @return
	 */
	public Users selectByUserInfo(Users users);

	/**
	 * 查询用户下线用户
	 * 
	 * @param userId
	 * @return
	 */
	public List<Integer> findDownWires(Integer userId);

	/**
	 * 手机号查询用户
	 * @param userPhone
	 * @return
	 */
    Users selectUserByPhone(@Param("userPhone") String userPhone);

	/**
	 * 修改用户通过手机号
	 * @param users
	 * @return
	 */
    int updateUserByUserPhone(Users users);

	/**
	 * 查询账户
	 * @param user
	 * @return
	 */
	UsersAndAccount selectUserAccountDataById(Users user);

	public Integer selectCountIsStore(Integer inviteId);

	/**
	 * 查询用户通过id
	 * @param user
	 * @return
	 */
    public Users selectByUsersId(Users user);
    /**
     * 查看用户升级状态
     * @param users
     * @return
     */
	public Integer upStauts(Users users);


	/**
	 * 查询分享人
	 * @param user
	 * @return
	 */
	List<Users> selectInvitesMan(Users user);

	int selectCountInvites(Users user);

	int selectChargeCount(Users user);

	int selectManagerCount(Users user);
	/**
	 * 用户身份状态
	 * @param id
	 * @return
	 */
	public UserIdentity userIdentity(@Param(value="id")Integer id);
	/**
	 * 修改支付密码
	 * @param users
	 * @return
	 */
	public int updateUsersPayPwd(UsersDTO users);
	/**
	 * 发展数量
	 * @param users
	 * @return
	 */
	public Integer developmentUser(Users users);

    DownShop appFindUserById(Integer userId);

    Users selectUsersByUserId(Integer userId);

    Users selectUsersByUserIdAndUserStatus(Map<Object, Object> maps);

    Map<String,Object> selectUsersAndUserRealNameByUserId(Integer userId);


    Map<String,Object> selectUsersAccountByUserId(Integer userId);

	public Map<String, Object> selectIdLoginNameShareInfo(String afterValue);
}
