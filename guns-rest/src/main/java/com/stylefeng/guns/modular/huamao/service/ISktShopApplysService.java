package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;
import com.stylefeng.guns.modular.huamao.model.SktShopApprove;

import java.util.List;

/**
 * <p>
 * 商城商家开店申请表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-10
 */
public interface ISktShopApplysService extends IService<SktShopApplys> {
	/*
	 * 展示全部
	 * */
	public List<MyDTO> shopApplysfindall(MyDTO myDto);
	/*
	 * 查看详情
	 * */
	public List<MyDTO> shopApplysfindup(MyDTO myDto);
	/*
	 * 修改后台
	 * */
	public void shopApplyupdate(MyDTO myDto);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 新增线上商城成功
	 * @param sktShopApplys
	 * @return
	 */
	public boolean insertShopApplys(SktShopApplys sktShopApplys);

	public String insertShopApplys2(SktShopApprove sktShopApprove);
}
