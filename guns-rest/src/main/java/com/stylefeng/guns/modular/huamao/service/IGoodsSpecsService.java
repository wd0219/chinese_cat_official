package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.GoodsSpecs;

/**
 * <p>
 * 商品规格表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IGoodsSpecsService extends IService<GoodsSpecs> {

}
