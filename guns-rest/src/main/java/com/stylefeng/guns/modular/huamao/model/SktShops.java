package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 商家信息表
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
@TableName("skt_shops")
public class SktShops extends Model<SktShops> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "shopId", type = IdType.AUTO)
    private Integer shopId;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 是否自营：1自营0非自营
     */
    private Integer isSelf;
    /**
     * 积分赠送比例 默认100 线上线下都使用
     */
    private Integer scoreRatio;
    /**
     * 默认运费
     */
    private Integer freight;
    /**
     * 营业执照所在省id
     */
    private Integer provinceId;
    /**
     * 营业执照所在市id
     */
    private Integer cityId;
    /**
     * 营业执照所在区县id
     */
    private Integer areaId;
    /**
     * 是否法人 1法人 0不是法人
     */
    private Integer isLegal;
    /**
     * 授权书
     */
    private String authorizationImg;
    /**
     * 法人
     */
    private String legalPerson;
    /**
     * 法人身份证号
     */
    private String IDnumber;
    /**
     * 法人手持身份证照片
     */
    private String legalPersonImg;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 营业执照号码
     */
    private String licenseNo;
    /**
     * 企业类型：0个体户 1公司
     */
    private Integer companyType;
    /**
     * 是否三证合一：0未合并 1合并
     */
    private Integer licenseMerge;
    /**
     * 营业执照
     */
    private String licenseImg;
    /**
     * 组织机构代码证
     */
    private String codeImg;
    /**
     * 税务登记证
     */
    private String taxImg;
    /**
     * 行业ID
     */
    private Integer industry;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * logo
     */
    private String logo;
    /**
     * 联系人
     */
    private String linkman;
    /**
     * 经营电话
     */
    private String telephone;
    /**
     * 经营地址
     */
    private String address;
    /**
     * 经度
     */
    private BigDecimal lng;
    /**
     * 纬度
     */
    private BigDecimal lat;
    /**
     * 促销语
     */
    private String intro;
    /**
     * 企业内容
     */
    private String content;
    /**
     * 线下商家状态：1可用 0禁用
     */
    private Integer storeStatus;
    /**
     * 线上商家状态：1可用 0禁用
     */
    private Integer shopStatus;
    /**
     * 服务营业时间
     */
    private String startTime;
    /**
     * 服务结束时间
     */
    private String endTime;
    /**
     * 线下店铺展示图1
     */
    private String storeImg1;
    /**
     * 线下店铺展示图2
     */
    private String storeImg2;
    /**
     * 线下店铺展示图3
     */
    private String storeImg3;
    /**
     * 线上商城展示图1
     */
    private String shopImg1;
    /**
     * 线上商城展示图2
     */
    private String shopImg2;
    /**
     * 线上商城展示图3
     */
    private String shopImg3;
    /**
     * 有效状态 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     *	省市县别名
     * */
    @TableField(exist = false)
    private String areas;
    @TableField(exist = false)
    private String hyName;
    @TableField(exist=false)
    private BigDecimal businessQuota;
    @TableField(exist = false)
    private String goodsImg;
    public String getHyName() {
		return hyName;
	}

	public BigDecimal getBusinessQuota() {
		return businessQuota;
	}

	public void setBusinessQuota(BigDecimal businessQuota) {
		this.businessQuota = businessQuota;
	}

	public void setHyName(String hyName) {
		this.hyName = hyName;
	}


	public void setAreas(String areas) {
		this.areas = areas;
	}

	public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIsSelf() {
        return isSelf;
    }

    public void setIsSelf(Integer isSelf) {
        this.isSelf = isSelf;
    }

    public Integer getScoreRatio() {
        return scoreRatio;
    }

    public void setScoreRatio(Integer scoreRatio) {
        this.scoreRatio = scoreRatio;
    }

    public Integer getFreight() {
        return freight;
    }

    public void setFreight(Integer freight) {
        this.freight = freight;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getIsLegal() {
        return isLegal;
    }

    public void setIsLegal(Integer isLegal) {
        this.isLegal = isLegal;
    }

    public String getAuthorizationImg() {
        return authorizationImg;
    }

    public void setAuthorizationImg(String authorizationImg) {
        this.authorizationImg = authorizationImg;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getIDnumber() {
        return IDnumber;
    }

    public void setIDnumber(String IDnumber) {
        this.IDnumber = IDnumber;
    }

    public String getLegalPersonImg() {
        return legalPersonImg;
    }

    public void setLegalPersonImg(String legalPersonImg) {
        this.legalPersonImg = legalPersonImg;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public Integer getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Integer companyType) {
        this.companyType = companyType;
    }

    public Integer getLicenseMerge() {
        return licenseMerge;
    }

    public void setLicenseMerge(Integer licenseMerge) {
        this.licenseMerge = licenseMerge;
    }

    public String getLicenseImg() {
        return licenseImg;
    }

    public void setLicenseImg(String licenseImg) {
        this.licenseImg = licenseImg;
    }

    public String getCodeImg() {
        return codeImg;
    }

    public void setCodeImg(String codeImg) {
        this.codeImg = codeImg;
    }

    public String getTaxImg() {
        return taxImg;
    }

    public void setTaxImg(String taxImg) {
        this.taxImg = taxImg;
    }

    public Integer getIndustry() {
        return industry;
    }

    public void setIndustry(Integer industry) {
        this.industry = industry;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getLng() {
        return lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStoreStatus() {
        return storeStatus;
    }

    public void setStoreStatus(Integer storeStatus) {
        this.storeStatus = storeStatus;
    }

    public Integer getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(Integer shopStatus) {
        this.shopStatus = shopStatus;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStoreImg1() {
        return storeImg1;
    }

    public void setStoreImg1(String storeImg1) {
        this.storeImg1 = storeImg1;
    }

    public String getStoreImg2() {
        return storeImg2;
    }

    public void setStoreImg2(String storeImg2) {
        this.storeImg2 = storeImg2;
    }

    public String getStoreImg3() {
        return storeImg3;
    }

    public void setStoreImg3(String storeImg3) {
        this.storeImg3 = storeImg3;
    }

    public String getShopImg1() {
        return shopImg1;
    }

    public void setShopImg1(String shopImg1) {
        this.shopImg1 = shopImg1;
    }

    public String getShopImg2() {
        return shopImg2;
    }

    public void setShopImg2(String shopImg2) {
        this.shopImg2 = shopImg2;
    }

    public String getShopImg3() {
        return shopImg3;
    }

    public void setShopImg3(String shopImg3) {
        this.shopImg3 = shopImg3;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.shopId;
    }

	@Override
	public String toString() {
		return "SktShops [shopId=" + shopId + ", userId=" + userId + ", isSelf=" + isSelf + ", scoreRatio=" + scoreRatio
				+ ", freight=" + freight + ", provinceId=" + provinceId + ", cityId=" + cityId + ", areaId=" + areaId
				+ ", isLegal=" + isLegal + ", authorizationImg=" + authorizationImg + ", legalPerson=" + legalPerson
				+ ", IDnumber=" + IDnumber + ", legalPersonImg=" + legalPersonImg + ", companyName=" + companyName
				+ ", licenseNo=" + licenseNo + ", companyType=" + companyType + ", licenseMerge=" + licenseMerge
				+ ", licenseImg=" + licenseImg + ", codeImg=" + codeImg + ", taxImg=" + taxImg + ", industry="
				+ industry + ", shopName=" + shopName + ", logo=" + logo + ", linkman=" + linkman + ", telephone="
				+ telephone + ", address=" + address + ", lng=" + lng + ", lat=" + lat + ", intro=" + intro
				+ ", content=" + content + ", storeStatus=" + storeStatus + ", shopStatus=" + shopStatus
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", storeImg1=" + storeImg1 + ", storeImg2="
				+ storeImg2 + ", storeImg3=" + storeImg3 + ", shopImg1=" + shopImg1 + ", shopImg2=" + shopImg2
				+ ", shopImg3=" + shopImg3 + ", dataFlag=" + dataFlag + ", createTime=" + createTime + ", areas="
				+ areas + ", hyName=" + hyName + ", businessQuota=" + businessQuota + "]";
	}

	public SktShops(Integer shopId, Integer userId, Integer isSelf, Integer scoreRatio, Integer freight,
			Integer provinceId, Integer cityId, Integer areaId, Integer isLegal, String authorizationImg,
			String legalPerson, String iDnumber, String legalPersonImg, String companyName, String licenseNo,
			Integer companyType, Integer licenseMerge, String licenseImg, String codeImg, String taxImg,
			Integer industry, String shopName, String logo, String linkman, String telephone, String address,
			BigDecimal lng, BigDecimal lat, String intro, String content, Integer storeStatus, Integer shopStatus,
			String startTime, String endTime, String storeImg1, String storeImg2, String storeImg3, String shopImg1,
			String shopImg2, String shopImg3, Integer dataFlag, Date createTime, String areas, String hyName,
			BigDecimal businessQuota) {
		super();
		this.shopId = shopId;
		this.userId = userId;
		this.isSelf = isSelf;
		this.scoreRatio = scoreRatio;
		this.freight = freight;
		this.provinceId = provinceId;
		this.cityId = cityId;
		this.areaId = areaId;
		this.isLegal = isLegal;
		this.authorizationImg = authorizationImg;
		this.legalPerson = legalPerson;
		IDnumber = iDnumber;
		this.legalPersonImg = legalPersonImg;
		this.companyName = companyName;
		this.licenseNo = licenseNo;
		this.companyType = companyType;
		this.licenseMerge = licenseMerge;
		this.licenseImg = licenseImg;
		this.codeImg = codeImg;
		this.taxImg = taxImg;
		this.industry = industry;
		this.shopName = shopName;
		this.logo = logo;
		this.linkman = linkman;
		this.telephone = telephone;
		this.address = address;
		this.lng = lng;
		this.lat = lat;
		this.intro = intro;
		this.content = content;
		this.storeStatus = storeStatus;
		this.shopStatus = shopStatus;
		this.startTime = startTime;
		this.endTime = endTime;
		this.storeImg1 = storeImg1;
		this.storeImg2 = storeImg2;
		this.storeImg3 = storeImg3;
		this.shopImg1 = shopImg1;
		this.shopImg2 = shopImg2;
		this.shopImg3 = shopImg3;
		this.dataFlag = dataFlag;
		this.createTime = createTime;
		this.areas = areas;
		this.hyName = hyName;
		this.businessQuota = businessQuota;
	}

	public SktShops() {
		super();
		// TODO Auto-generated constructor stub
	}

    
}
