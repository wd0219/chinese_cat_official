package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IMessagesService;

/**
 * 商城消息控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 10:33:09
 */
@CrossOrigin
@Controller
@RequestMapping("/messages")
public class MessagesController extends BaseController {

	private String PREFIX = "/huamao/messages/";

	@Autowired
	private IMessagesService messagesService;

	/**
	 * 跳转到商城消息首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "messages.html";
	}

	/**
	 * 跳转到添加商城消息
	 */
	@RequestMapping("/messages_add")
	public String messagesAdd() {
		return PREFIX + "messages_add.html";
	}

	/**
	 * 跳转到修改商城消息
	 */
	@RequestMapping("/messages_update/{messagesId}")
	public String messagesUpdate(@PathVariable Integer messagesId, Model model) {
		Messages messages = messagesService.selectById(messagesId);
		model.addAttribute("item", messages);
		LogObjectHolder.me().set(messages);
		return PREFIX + "messages_edit.html";
	}

	/**
	 * 获取商城消息列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(MessagesList messagesList) {
		return messagesService.selectMessagesAll(messagesList);
	}

	/**
	 * 新增商城消息：li为空说明是给所有商家发送，不为空为给指定用户发送
	 */
	@RequestMapping(value = "/addMessages")
	@ResponseBody
	public Object addMessages(@RequestParam(required = false) String li, MessagesList messagesList) {
		// 如果到了这里说明是手动添加，直接将类型改为手动
//		messagesList.setMsgType(0);
//		// 发送者id为操作员id
//		ShiroUser shiroUser = (ShiroUser) super.getSession().getAttribute("shiroUser");
//		// 写入实体类发送者id
//		messagesList.setSendUserId(shiroUser.getId());
//		// 发送时间为当前是时间
//		messagesList.setCreateTime(new Date());
//		// 传入service层
//		boolean b = messagesService.addMessages(li, messagesList);
//		// messagesService.insert(messages);
//		// 如果是true说明成功，否则失败
//		if (b) {
//			return b;
//		}
		return null;
	}

	/**
	 * 通过ID删除代理公司银行卡
	 */
	@RequestMapping(value = "/deleteById")
	@ResponseBody
	public Object deleteById(@RequestParam Integer id) {
		// 通过传过来的ID进行逻辑删除
		messagesService.deleteById2(id);
		return SUCCESS_TIP;
	}

	/**
	 * 删除商城消息
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer messagesId) {
		messagesService.deleteById(messagesId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改商城消息
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(Messages messages) {
		messagesService.updateById(messages);
		return SUCCESS_TIP;
	}

	/**
	 * 跳转到消息列表页面
	 */
	@RequestMapping("/getList")
	public String getList() {
		return PREFIX + "messagesList.html";
	}

	/**
	 * 跳转到发送消息页面
	 */
	@RequestMapping("/sendMessage")
	public String sendMessage() {
		return PREFIX + "messages.html";
	}

	/**
	 * 跳转到消息内容详情页面
	 */
	@RequestMapping("/messageDetail")
	public String messageDetail(@RequestParam String msgContent, Model model) {
		model.addAttribute("msgContent", msgContent);
		return PREFIX + "messageDetail.html";
	}

	/**
	 * 商城消息详情
	 */
	@RequestMapping(value = "/detail/{messagesId}")
	@ResponseBody
	public Object detail(@PathVariable("messagesId") Integer messagesId) {
		return messagesService.selectById(messagesId);
	}

	/**
	 * 获取用户
	 */
	@RequestMapping(value = "/getUsers")
	@ResponseBody
	public Object getUsers(String userPhone) {
		// 获取用户
		List<MessagesList> users = messagesService.getUsers(userPhone);
		// 计数器
		// int cont = 0;
		// // 拼接成前台需要的格式
		// for (MessagesList messagesList : users) {
		// if (cont == users.size() - 1) {
		// list += messagesList.getReceiveName() + "',";
		// list += "id:'" + messagesList.getReceiveUserId() + "'}]";
		// } else {
		// list += messagesList.getReceiveName() + "',";
		// list += "id:'" + messagesList.getReceiveUserId() + "'},{name:'";
		// }
		// cont++;
		// }
		return users;
	}
	
	/*****************************首页*****************************/
	/**
	 * 查询用户消息
	 */
	@RequestMapping(value = "/getMessage")
	@ResponseBody
	public Result getMessage(@RequestParam(value="receiveUserId") Integer receiveUserId,
			@RequestParam(value="pageNum",defaultValue="1", required=false) Integer pageNum) {
		PageHelper.startPage(pageNum, 12);
		List<MessagesList> message = messagesService.getMessage1(receiveUserId);
		PageInfo info = new PageInfo<>(message);
		return Result.OK(message,info.getPages());
	}
	/**
	 * 查询用户未读消息数量
	 */
	@RequestMapping(value = "/getMessageCount")
	@ResponseBody
	public Result getMessageCount(Messages messages) {
		Integer message = messagesService.getMessageCount(messages);
		return Result.OK(message);
	}
	/**
	 * 修改消息状态 删除 已读状态
	 */
	@RequestMapping(value = "/updateMessageStatus")
	@ResponseBody
	public Result updateMessageStatus(@RequestParam(value="msgStatus" , required=false) Integer msgStatus,
								@RequestParam(value="dataFlag" , required=false) Integer dataFlag,
								@RequestParam(value="ids[]") Integer[] ids) {
		Integer message = messagesService.updateMessageStatus(msgStatus,dataFlag,ids);
		if(message==0){
			return Result.EEROR("更新状态失败！");
		}
		return Result.OK("更新状态成功！");
	}
	/**
	 * 删除商城消息
	 * @param messagesId 消息id
	 * @return
	 */
	@RequestMapping(value = "/deleteMessages")
	@ResponseBody
	public Result deleteMessages(@RequestParam(value="messagesId") Integer messagesId) {
		boolean deleteById = messagesService.deleteById(messagesId);
		return deleteById==true?Result.OK("删除消息成功"):Result.EEROR("删除消息失败");
	}
}
