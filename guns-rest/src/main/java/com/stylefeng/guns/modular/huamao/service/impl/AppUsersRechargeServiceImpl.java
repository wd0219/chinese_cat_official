package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersRechargeMapper;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;
import com.stylefeng.guns.modular.huamao.service.IAppUsersRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppUsersRechargeServiceImpl extends ServiceImpl<UsersRechargeMapper,UsersRecharge> implements IAppUsersRechargeService {
    @Autowired
    private UsersRechargeMapper usersRechargeMapper;

    @Override
    public UsersRecharge selectUsersRechargeByOrderNo(Map<String, Object> orderWhere) {
        return usersRechargeMapper.selectUsersRechargeByOrderNo(orderWhere);
    }

    @Override
    public UsersRecharge selectUsersRechargeByUserId(Map<String, Object> map) {
        return usersRechargeMapper.selectUsersRechargeByUserId(map);
    }
}
