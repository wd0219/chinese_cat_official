package com.stylefeng.guns.modular.huamao.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.UrlUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.result.Result;

/**
 * 商品控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 16:16:23
 */
@CrossOrigin()
@Controller
@RequestMapping("/goods")
public class GoodsController extends BaseController {

    private String PREFIX = "/huamao/goods/";

    @Autowired
	private IGoodsService goodsService;
    @Autowired
    private ISktAreasService sktAreasService;
    @Autowired
    private IGoodsCatsService goodsCatsService;
    @Autowired
    private IGoodsAttributesService goodsAttributesService;
    @Autowired
    private IGoodsSpecsService goodsSpecsService;
    @Autowired
    private ISpecItemsService iSpecItemsService;
    @Autowired
    private IGoodsAppraisesService iGoodsAppraisesService;
    @Autowired
	private ISktShopsService sktShopsService;
	@Autowired
	private  ISktAdPositionsService sktAdPositionsService;
	@Autowired
	private IAdsService adsService;
    /**
     * 跳转到商品首页
     */
    
    @RequestMapping("")

//  @ResponseBody
    public String index() {
    	
       return PREFIX + "goods.html";
    }
    /**
     * 跳转到待审核商品首页
     */ 
    @RequestMapping("/goodsWarting")
    public String index2() {
        
    	return PREFIX + "goods_warting.html";
     }
    /**
     * 跳转到违规商品首页
     */ 
    @RequestMapping("/goodsUnqualified")
    public String index3() {
        
    	return PREFIX + "goods_unqualified.html";
     }
    /**
     * 跳转到推荐商品首页
     */ 
    @RequestMapping("/goodsRecom")
    public String index4() {
        
    	return PREFIX + "goods_Recom.html";
     }
    /**
     * 跳转到添加商品
     */
    @RequestMapping("/goods_add")
    public String goodsAdd() {
        return PREFIX + "goods_add.html";
    }

    /**
     * 跳转到修改商品
     */
    @RequestMapping("/goods_update/{goodsId}")
    public String goodsUpdate(@PathVariable Integer goodsId, Model model) {
        Goods goods = goodsService.selectById(goodsId);
        model.addAttribute("item",goods);
        return PREFIX + "goods_edit.html";
    }

    /**
     * 跳转到修改商品(待审核详情)
     */
    @RequestMapping("/goods_update1/{goodsId}")
    public String goodsUpdate1(@PathVariable Integer goodsId, Model model) {
        Goods goods = goodsService.selectById(goodsId);
        model.addAttribute("item",goods);
        return PREFIX + "goods_warting_edit.html";
    }

    /**
     * 获取已上架商品列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(GoodsCustm goodsCustm ) { 
    	if(goodsCustm.getIsSale()==null ){
    		goodsCustm.setIsSale(1);
    	}
    	if(goodsCustm.getGoodsStatus()==null ){
    		goodsCustm.setGoodsStatus(1);
    	}
    	if(goodsCustm.getGoodsName()!=null && !"".equals(goodsCustm.getGoodsName())){
    		goodsCustm.setGoodsSn(goodsCustm.getGoodsName());
    	}
    	 List<Map<String, Object>> goodsCustemList = goodsService.selectByConditions(goodsCustm);
    	for(Map<String, Object> map:goodsCustemList ){
    		String goodsCatIdPath=(String) map.get("goodsCatIdPath");
    		String[]  catIdPathList=goodsCatIdPath.split("_");
    		String goodsCatNamePath = null;
    		for (int i = 0; i < catIdPathList.length; i++) {
    			GoodsCats goodsCats = goodsCatsService.selectById(catIdPathList[i]);
    			String catName = goodsCats.getCatName();
    			goodsCatNamePath=goodsCatNamePath+catName+"→";
			}
    		goodsCatNamePath=goodsCatNamePath.substring(4,goodsCatNamePath.lastIndexOf("→") );
    		map.put("goodsCatIdPath", goodsCatNamePath);
    		
    	}
    	 return goodsCustemList;
    }

    /**
     * 获取待审核商品列表
     */
    @RequestMapping(value = "/list2")
    @ResponseBody
    public Object list2(GoodsCustm goodsCustm ) { 
    	if(goodsCustm.getGoodsStatus()==null){
    		goodsCustm.setGoodsStatus(0);
    	}
    	if(goodsCustm.getGoodsName()!=null || !"".equals(goodsCustm.getGoodsName())){
    		goodsCustm.setGoodsSn(goodsCustm.getGoodsName());
    	}
    	 List<Map<String, Object>> goodsCustemList = goodsService.selectByConditions(goodsCustm);
     	for(Map<String, Object> map:goodsCustemList ){
     		String goodsCatIdPath=(String) map.get("goodsCatIdPath");
     		String[]  catIdPathList=goodsCatIdPath.split("_");
     		String goodsCatNamePath = null;
     		for (int i = 0; i < catIdPathList.length; i++) {
     			GoodsCats goodsCats = goodsCatsService.selectById(catIdPathList[i]);
     			String catName = goodsCats.getCatName();
     			goodsCatNamePath=goodsCatNamePath+catName+"→";
 			}
     		goodsCatNamePath=goodsCatNamePath.substring(4,goodsCatNamePath.lastIndexOf("→") );
     		map.put("goodsCatIdPath", goodsCatNamePath);
     		
     	}
     	 
     	 
     	 return goodsCustemList;
    }
    
    /**
     * 获取违规商品列表
     */
    @RequestMapping(value = "/list3")
    @ResponseBody
    public Object list3(GoodsCustm goodsCustm ) { 
    	if(goodsCustm.getGoodsStatus()==null){
    		goodsCustm.setGoodsStatus(-1);
    	}
    	if(goodsCustm.getGoodsName()!=null || !"".equals(goodsCustm.getGoodsName())){
    		goodsCustm.setGoodsSn(goodsCustm.getGoodsName());
    	}
    	List<Map<String, Object>> goodsCustemList = goodsService.selectByConditions(goodsCustm);
     	for(Map<String, Object> map:goodsCustemList ){
     		String goodsCatIdPath=(String) map.get("goodsCatIdPath");
     		String[]  catIdPathList=goodsCatIdPath.split("_");
     		String goodsCatNamePath = null;
     		for (int i = 0; i < catIdPathList.length; i++) {
     			GoodsCats goodsCats = goodsCatsService.selectById(catIdPathList[i]);
     			String catName = goodsCats.getCatName();
     			goodsCatNamePath=goodsCatNamePath+catName+"→";
 			}
     		goodsCatNamePath=goodsCatNamePath.substring(4,goodsCatNamePath.lastIndexOf("→") );
     		map.put("goodsCatIdPath", goodsCatNamePath);
     		
     	}
     	 
     	 
     	 return goodsCustemList;
    }
    /**
     * 通过商品id查询商品分类级别
     */
    
    @RequestMapping(value = "/selectCatsLevelByGoodsid")
    @ResponseBody
    public Object selectCatsLevelByGoodsid(Integer goodsId){
    	/*JSONObject data = new JSONObject();
    	data.put("success", true);
    	data.put("obj", goodsService.selectCatsLevelByGoodsid(goodsId));
    	return data;*/
    	
    	Result result =  new Result();
    	try {
    		Object data = goodsService.selectCatsLevelByGoodsid(goodsId);
    		result.setType(1);
    		result.setMessage("Success");
    		result.setMessageCode(200);
    		result.setEntity(data);
		} catch (Exception e) {
			result.setType(0);
    		result.setMessage("查询失败");
    		result.setMessageCode(500);
		}
    	
    	return result;
    }
    /**
     * 显示商城首页内容
     */
    @RequestMapping(value = "/selectByCatName")
    @ResponseBody
    public Object selectByCatName(String param,String param1,String param2){
    	JSONObject data = new JSONObject();
    	String msg = "";
    	Boolean temp = true;
    	List<String> item = new ArrayList<String>();
    	if(StringUtils.isEmpty(param)){
    		msg = "param不能为空";
    		temp = false;
    	}else{
    		item.add(param);
    	}
    	if(StringUtils.isEmpty(param1)){
    		msg =  "param1不能为空";
    		temp = false;
    	}else{
    		item.add(param1);
    	}
    	
    	if(StringUtils.isEmpty(param2)){
    		
    	}else{
    		item.add(param2);
    	}
    	
    	Result result =  new Result();
    	
    	if(!temp){
    		result.setType(3);
    		result.setMessage(msg);
    		result.setMessageCode(500);
    	}else{
    		try {
        		Object obj = goodsService.selectByCatName(item);
        		result.setType(1);
        		result.setMessage("Success");
        		result.setMessageCode(200);
        		result.setEntity(obj);
    		} catch (Exception e) {
    			result.setType(0);
        		result.setMessage("查询失败");
        		result.setMessageCode(500);
    		}
    	}
    	
    	
    	
    	
    	return result;
    }
    
    /**
     * 
     */
    @RequestMapping(value = "/selectGoodsByGoodsCatId")
    @ResponseBody
    public Object selectGoodsByGoodsCatId(@RequestParam(value ="id") Integer id ) { 
    	
    	 JSONArray jSONArray=new JSONArray();
    	 List<Goods> goodsList = goodsService.selectGoodsByGoodsCatId(id);
     	for (Goods good : goodsList) {
     		JSONObject json=new JSONObject();
     		json.put("goodsId", good.getGoodsId());
     		json.put("goodsName", good.getGoodsName());
     		jSONArray.add(json);	
 		}
     	return jSONArray;
    	 
    	 
    }
    
    /**
     * 批量修改商品
     */
    @RequestMapping(value = "/batchUpdate")
    @ResponseBody
    public Object batchUpdate(
    		@RequestParam(value="to_left[]", required = false ) List<Integer> to_left,
    		@RequestParam(value="to_right[]",required = false) List<Integer> to_right,
    		@RequestParam(value="style") String style
    		) {
    	List<Integer> list1= to_left;
    	List<Integer> list2=  to_right; 
//    	List<Integer> list1 = goodsCustm.getTo_left();
//    	List<Integer> list2 = goodsCustm.getTo_right();
//
//    	Integer style=goodsCustm.getStyle();
    	List<Goods> goodsCustemlist1=new ArrayList<>();

    	if(list1!=null && list1.size()>0){
    		for (int i = 0; i <list1.size(); i++) {
    			Goods goods = goodsService.selectById(list1.get(i));
				  if("1".equals(style)){
					  goods.setIsBest(0);  
				  }
				  if("2".equals(style)){
					  goods.setIsHot(0);  
				  }
				  if("3".equals(style)){
					  goods.setIsNew(0);  
				  }
				  if("4".equals(style)){
					  goods.setIsRecom(0); 
				  }
				  goodsService.updateById(goods);
    			//  goodsCustemlist1.add(goods);  
    			  
			}
    		//goodsService.batchUpdatePo(goodsCustemlist1);
    	}
    	if(list2!=null &&list2.size()>0){
    		for (int i = 0; i <list2.size() ; i++) {
    			  Goods goods = goodsService.selectById(list2.get(i));
    			  if("1".equals(style)){
    				  goods.setIsBest(1);  
    			  }
    			  if("2".equals(style)){
    				  goods.setIsHot(1);  
    			  }
    			  if("3".equals(style)){
    				  goods.setIsNew(1);  
    			  }
    			  if("4".equals(style)){
    				  goods.setIsRecom(1); 
    			  }
    			  goodsService.updateById(goods);
    			//  goodsCustemlist1.addAll(goodsCustemlist1);  
			}
    		
    	}
    //	goodsService.batchUpdatePo(goodsCustemlist1);
        return SUCCESS_TIP;
    }

    
    /**
     * 商品详情
     */
    @RequestMapping(value = "/detail/{goodsId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsId") Integer goodsId) {
        return goodsService.selectById(goodsId);
    }
    
    /********************首页***************************/
    
    /**
     * 查看今日推荐
     */
    @RequestMapping(value = "/todayHotSale")
    @ResponseBody
    public Result todayHotSale() {
    	List<Map<String,Object>> list = goodsService.TodayHotSale();
        return Result.OK(list);
    }
    /**
     * 查看今日推荐商品
     */
    @RequestMapping(value = "/todayHotSaleGoods")
    @ResponseBody
    public Result todayHotSaleGoods(){
//		Map<String,Object> result=new HashMap<>();
		List<Map<String,Object>> result=new ArrayList<>();
		List<Map<String,Object>> adsResult=new ArrayList<>();
		//分类下推荐商品
		List<Map<String, Object>> showFloorList = goodsCatsService.showFloor();
		if (showFloorList!=null && showFloorList.size()>0){

			//首页左侧广告分类
			Map<String,Object> adPositionsListMap=new HashMap<>();
			adPositionsListMap.put("limit",showFloorList.size());
			List<SktAdPositions> sktAdPositionsLeft = sktAdPositionsService.sktAdPositionsfindLeft(adPositionsListMap);
			List<SktAdPositions> sktAdPositionsBottom = sktAdPositionsService.sktAdPositionsfindBottom(adPositionsListMap);
			for (int i=0;i<showFloorList.size();i++){
				Map<String, Object> resultMap=new HashMap<>();
				Map<String, Object> showFloor=showFloorList.get(i);
				String catId=showFloor.get("catId").toString();
				GoodsCats goodsCats = goodsCatsService.selectById(catId);
				String catName=goodsCats.getCatName();
				resultMap.put("catNmae",catName);
				Map<String, Object> paraMap=new HashMap<>();
				paraMap.put("catId",catId);
				List<Map<String,Object>> goodslist = goodsService.todayHotSaleGoods(paraMap);
				resultMap.put("goodslist",goodslist);
				//左侧广告
				SktAdPositions SktAdPosition=sktAdPositionsLeft.get(i);
				Integer	adPositionId=SktAdPosition.getPositionId();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
				//df.format(new Date());// new Date()为获取当前系统时间
				Date date = new Date();
				String nowDate = df.format(date);
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("nowDate",nowDate);
				map.put("adPositionId",adPositionId);
				map.put("dataFlag",1);
				map.put("limt",1);
				List<Ads> list = adsService.selectPcTopAds(map);
				Map<String,Object> leftAdsMap=new HashMap<String,Object>();
				if(list != null && list.size()>0){
					Ads ads = list.get(0);
					String adFile = ads.getAdFile();
					adFile = UrlUtils.getAllUrl(adFile);
					leftAdsMap.put("adFile",adFile);
					leftAdsMap.put("adURL",ads.getAdURL());

				}
				resultMap.put("leftAdsMap",leftAdsMap);
				//底部广告
				SktAdPositions sktAdPositionBottom = sktAdPositionsBottom.get(i);
				Integer	positionId=sktAdPositionBottom.getPositionId();
				Map<String,Object> map2 = new HashMap<String,Object>();
				map2.put("nowDate",nowDate);
				map2.put("adPositionId",positionId);
				map2.put("dataFlag",1);
				map2.put("limt",1);
				List<Ads> list2 = adsService.selectPcTopAds(map2);
				Map<String,Object> bottomAdsMap=new HashMap<String,Object>();
				if(list2 != null && list2.size()>0){
					Ads ads = list2.get(0);
					String adFile = ads.getAdFile();
					adFile = UrlUtils.getAllUrl(adFile);
					bottomAdsMap.put("adFile",adFile);
					bottomAdsMap.put("adURL",ads.getAdURL());

				}
				resultMap.put("bottomAdsMap",bottomAdsMap);
				result.add(resultMap);
			}

		}



        return Result.OK(result);
    }
    /**
     * 猜你喜欢
     * @param shopId
     * @return
     */
    @RequestMapping(value = "/guessLikeGoods")
    @ResponseBody
    public Result guessLikeGoods(@RequestParam(value="shopId")Integer shopId){
    	List<Goods> list = goodsService.guessLikeGoods(shopId);
        return Result.OK(list);
    }
    /**
     * 查看今日推荐品牌列表商品
     */
    @RequestMapping(value = "/selectTodayHotSaleAll/{brandId}")
    @ResponseBody
    public Result selectTodayHotSaleAll(@PathVariable("brandId") Integer brandId) {
    	List<Map<String,Object>> list = goodsService.selectTodayHotSaleAll(brandId);
        return Result.OK(list);
    }
    
    /**
     * 新增商品
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Result add(Goods goods) {
        Integer insert = goodsService.insertAdd(goods);
        return insert==0?Result.EEROR("新增商品失败！"):Result.OK(insert);
    }
    
    /**
     * 新增商品、属性、规格?不确定
     */
    @Deprecated
    @RequestMapping(value = "/addAttrCat")
    @ResponseBody
    public Result addAttrCat(@RequestParam(value="goods",required=false)String goods,
    		@RequestParam(value="specItemss") String specItemss,
    		@RequestParam(value="goodsSpecss") String goodsSpecss,
    		@RequestParam(value="goodsAttributess") String goodsAttributess) {
    	
    	Goods good = new Goods();
    	List<GoodsSpecs> goodsSpecs= new ArrayList<>();
    	List<GoodsAttributes> goodsAttributes = new ArrayList<>();
    	if(ToolUtil.isNotEmpty(goods)){
    		good = JSONObject.parseObject(goods, Goods.class);
    	}
    	if(ToolUtil.isNotEmpty(goodsSpecss)){
    		 goodsSpecs = JSONArray.parseArray(goodsSpecss, GoodsSpecs.class);
    	}
    	if(ToolUtil.isNotEmpty(goodsAttributess)){
    		 goodsAttributes = JSONArray.parseArray(goodsAttributess, GoodsAttributes.class);
    	}
        boolean insert = goodsService.addAttrCat(good,null,goodsSpecs,goodsAttributes);
        return insert==true?Result.OK("新增商品成功！"):Result.EEROR("新增商品失败！");
    }
    
    /**
     * 根据id查询商品
     */
    @RequestMapping(value = "/getGoodsById")
    @ResponseBody
    public Result getGoods(@RequestParam Integer goodsId) {
        Goods goods = goodsService.selectById(goodsId);
        if(ToolUtil.isEmpty(goods)){
        	return Result.EEROR("查询商品失败！");
        }
        return Result.OK(null);
    }
    
    /**
     * 修改商品
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Result update(@RequestParam(value="goodsId[]") Integer[] goodId,
    		@RequestParam(value="isSale",required = false ) Integer isSale,
    		@RequestParam(value="isBest",required = false ) Integer isBest,
    		@RequestParam(value="isHot",required = false ) Integer isHot,
    		@RequestParam(value="isNew",required = false ) Integer isNew,
    		@RequestParam(value="isRecom",required = false ) Integer isRecom,
    		@RequestParam(value="isDelete",required = false ) Integer isDelete
    		) {
        boolean updateById = goodsService.updateStatus(goodId,isSale,isBest,isHot,isNew,isRecom,isDelete);
        return updateById==true?Result.OK(null):Result.EEROR("更新商品失败！");
    }
    /**
     * 查询默认属性
     */
    @RequestMapping(value = "/selectDefaultProperty")
    @ResponseBody
    public Result selectDefaultProperty(@RequestParam(value="defaultProperty[]") String[] defaultProperty) {
        List<Map<String,Object>> list = goodsService.selectDefaultProperty(defaultProperty);
        if(list.size()>0){
        	return Result.OK(list);
        }
        return Result.EEROR("没有默认属性!");
    }
    /**
     * 查询默认下拉框列表
     */
    @RequestMapping(value = "/selectDownSelect")
    @ResponseBody
    public Result selectDownSelect(@RequestParam(value="gType") String gType) {
        List<Map<String,Object>> list = goodsService.selectDownSelect(gType);
        if(list.size()>0){
        	return Result.OK(list);
        }
        return Result.EEROR("没有该下拉列表!");
    }
    /**
     * 是否出售 1 是否库存 0
     */
    @RequestMapping(value = "/sellStockGoods")
    @ResponseBody
    public Result sellShop(GoodsSellStock goodsSellStock,
    		@RequestParam (value="pageNum",defaultValue="1", required=false) Integer pageNum) {
    	PageHelper.startPage(pageNum, 12);
        List<GoodsSellStock> list = goodsService.selectByIsSale(goodsSellStock);
        PageInfo pag = new PageInfo<>(list);
        return Result.OK(list,pag.getPages());
    }
    /**
     * 是否审核 1 是否违规 0
     */
    @RequestMapping(value = "/selectByIsGoodsStatus")
    @ResponseBody
    public Result selectByIsGoodsStatus(GoodsSellStock goodsSellStock,
    		@RequestParam (value="pageNum",defaultValue="1", required=false) Integer pageNum) {
    	PageHelper.startPage(pageNum, 12);
        List<GoodsSellStock> list = goodsService.selectByIsGoodsStatus(goodsSellStock);
        PageInfo pag = new PageInfo<>(list);
        return Result.OK(list,pag.getPages());
    }
    /**
     * 是否预警库存
     */
    @RequestMapping(value = "/selectByIsWarnStock")
    @ResponseBody	
    public Result selectByIsWarnStock(GoodsSellStock goodsSellStock,
    		@RequestParam (value="pageNum",defaultValue="1", required=false) Integer pageNum) {
    	PageHelper.startPage(pageNum, 12);
        List<GoodsSellStock> list = goodsService.selectByIsWarnStock(goodsSellStock);
        PageInfo pag = new PageInfo<>(list);
        return Result.OK(list,pag.getPages());
    }
    
    /**
     * 查询评论
     */
    @RequestMapping(value = "/selectByAppraises")
    @ResponseBody	
    public Result selectByAppraises(@RequestParam(value="shopId") Integer shopId,
    		@RequestParam(value="shopCatId1",required=false) Integer shopCatId1,
    		@RequestParam(value="shopCatId2",required=false) Integer shopCatId2,
    		@RequestParam (value="pageNum",defaultValue="1", required=false) Integer pageNum) {
    	PageHelper.startPage(pageNum, 12);
        List<GoodsAppraisesDTO> list = goodsService.selectByAppraises(shopId,shopCatId1,shopCatId2);
        PageInfo pag = new PageInfo<>(list);
        return Result.OK(list,pag.getPages());
    }
    /**
     * 回复评论 replyTime,shopReply店铺回复,id 评价id,images图片
     */
    @RequestMapping(value = "/backByAppraises")
    @ResponseBody	
    public Result backByAppraises(GoodsAppraises goodsAppraises) {
        goodsAppraises.setReplyTime(new Date());
        boolean updateById = iGoodsAppraisesService.updateById(goodsAppraises);
        return updateById == true ?Result.OK("回复成功"):Result.EEROR("回复失败");
    }
    /**
     * 首页查询商品
     */
    @RequestMapping(value = "/selectByGoods")
    @ResponseBody	
    public Result selectByGoods(GoodsList goods,
    		@RequestParam (value="pageNum",defaultValue="1", required=false) Integer pageNum) {
    	PageHelper.startPage(pageNum, 12);
        List<Map<String,Object>> list = goodsService.selectByGoods(goods);
        PageInfo pag = new PageInfo<>(list);
        return Result.OK(list,pag.getPages());
    }
    
    /**
     * 查询商品属性
     */
    @RequestMapping(value = "/selectGoodsAttr")
    @ResponseBody	
    public Result selectGoodsAttr(GoodsList goods) {
        List<Attributes> list = goodsService.selectGoodsAttr(goods);
        return Result.OK(list);
    }
    /**
     * 查询商品分类
     */
    @RequestMapping(value = "/selectGoodsCat")
    @ResponseBody	
    public Result selectGoodsCat(GoodsList goods) {
        List<SpecCats> list = goodsService.selectGoodsCat(goods);
        return Result.OK(list);
    }
	/**
	 * 生成默认编号 货号
	 */
	@RequestMapping(value="generateNo")
	@ResponseBody
	public Result generateNo(){
		BigDecimal generate  = goodsService.selectMax();
		return Result.OK(generate);
	}
	/**
	 * 根据最后一级分类获得品牌
	 */
	@RequestMapping(value="getCatBrands")
	@ResponseBody
	public Result getCatBrands(@RequestParam(value="catId") Integer catId){
		List<Map<String,Object>> list = goodsService.getCatBrands(catId);
		if(list.size()>0){
			return Result.OK(list);
		}
		return  Result.EEROR("获取失败！");
	}

    /**
     * 根据商品id查询信息回显
     */
    @RequestMapping(value="selelctgGoodsById")
    @ResponseBody
    public Result selelctgGoodsById(@RequestParam(value = "goodsId") Integer goodsId){
        List<Map<String,Object>> goodses = goodsService.selectByGoodsId(goodsId);
		if (goodses!=null&& goodses.size()>0){
			Map<String,Object> goods=goodses.get(0);
			if ("0".equals(goods.get("isSale").toString())){
				return 	Result.EEROR("该商品已下架");
			}
			if (!"1".equals(goods.get("goodsStatus").toString())){
				return 	Result.EEROR("该商品未通过审核");
			}
			if ("-1".equals(goods.get("dataFlag").toString())){
				return 	Result.EEROR("该商品已失效");
			}

			Integer shopId=Integer.parseInt(goods.get("shopId").toString());
			SktShops sktShops = sktShopsService.selectById(shopId);
			if (sktShops.getShopStatus()==0){
				return 	Result.EEROR("该商品所属商家已关闭");
			}

		}
        return Result.OK(goodses);
    }
    
    /**
     * 根据shopId，goodsId商品属性回显
     */
    @RequestMapping(value="selectAttrShow")
    @ResponseBody
    public Result selectAttrShow(@RequestParam(value = "goodsId") Integer goodsId,
    		@RequestParam(value = "shopId", required = false) Integer shopId){
        List<Map<String,Object>> goods = goodsService.selectAttrShow(goodsId,shopId);
        return Result.OK(goods);
    }
    
    /**
     * 根据shopId，goodsId商品规格回显
     */
    @RequestMapping(value="selectSpecShow")
    @ResponseBody
    public Result selectSpecShow(@RequestParam(value = "goodsId") Integer goodsId,
    		@RequestParam(value = "shopId", required = false) Integer shopId){
        List<Map<String,Object>> goods = goodsService.selectSpecShow(goodsId,shopId);
        return Result.OK(goods);
    }
    
    /**
     * 根据shopId，goodsId商品分类回显
     */
    @RequestMapping(value="selectCatShow")
    @ResponseBody
    public Result selectCatShow(@RequestParam(value = "goodsId") Integer goodsId,
    		@RequestParam(value = "shopId", required = false) Integer shopId){
        List<Map<String,Object>> goods = goodsService.selectCatShow(goodsId,shopId);
        return Result.OK(goods);
    }
    /**
     * 新增商品、属性、规格
     * @throws JSONException 
     */
    @RequestMapping(value = "/addAttrCat_two")
    @ResponseBody
    public Result addAttrCat_two(@RequestParam(value="goodsId",required=false)Integer goodsId,
    		@RequestParam(value="specItemss",required=false) String specItemss,
    		@RequestParam(value="goodsSpecss",required=false) String goodsSpecss,
    		@RequestParam(value="goodsAttributess",required=false) String goodsAttributess) throws JSONException {
    	List<SpecItems> specItems = new ArrayList<>();
    	List<GoodsSpecs> goodsSpecs= new ArrayList<>();
    	List<GoodsAttributes> goodsAttributes = new ArrayList<>();
    	if(ToolUtil.isNotEmpty(specItemss)){
    		specItems = JSONArray.parseArray(specItemss, SpecItems.class);
    	}
    	if(ToolUtil.isNotEmpty(goodsSpecss)){
    		 goodsSpecs = JSONArray.parseArray(goodsSpecss, GoodsSpecs.class);
    	}
    	if(ToolUtil.isNotEmpty(goodsAttributess)){
    		 goodsAttributes = JSONArray.parseArray(goodsAttributess, GoodsAttributes.class);
    	}
        boolean insert = goodsService.addAttrCat2(goodsId,specItems,goodsSpecs,goodsAttributes);
        return insert==true?Result.OK("新增商品成功！"):Result.EEROR("新增商品失败！");
    }
    
    /**
     * 新增商铺规格修改
     */
    @RequestMapping(value = "/addGoodsCat")
    @ResponseBody
    public Result addGoodsCat(@RequestParam(value="shopId") Integer shopId,
    		@RequestParam(value="goodsId") Integer goodsId,
    		@RequestParam(value="specItems") String specItems
    		) {
    	List<Map<String,Object>> result = goodsService.addGoodsCat(shopId,goodsId,specItems);
    	return Result.OK(result);
    }
    /**
     * 删除商品规格修改
     */
    @RequestMapping(value = "/deleteGoodsCat")
    @ResponseBody
    public Result deleteGoodsCat(@RequestParam(value="itemId") Integer itemId ) {
    	boolean deleteById = iSpecItemsService.deleteById(itemId);
    	return deleteById == true?Result.OK(null):Result.EEROR("删除商品规格失败！");
    }
    /**
     * 删除商品、属性、规格
     */
    @RequestMapping(value = "/deleteAttrCat_two")
    @ResponseBody
    public Result addAttrCat_two(@RequestParam(value="goodsId")Integer goodsId){
    	 boolean del = goodsService.deleteAttrCat_two(goodsId);
    	 return del==true?Result.OK("删除成功！"):Result.EEROR("删除失败！");
    }
    /**
     * 修改商品
     */
    @RequestMapping(value = "/updateGoods")
    @ResponseBody
    public Result updateGoods(Goods goods) {
    //	goods.setGoodsStatus(0);
    	goods.setCreateTime(new Date());
    	boolean updateById = goodsService.updateById(goods);
        return updateById==true?Result.OK("修改商品成功！"):Result.EEROR("修改商品失败！");
    }
    
    /**
     * 修改商品、属性、规格
     */
    @RequestMapping(value = "/updateAttrCat")
    @ResponseBody
    public Result updateAttrCat(
    		@RequestParam(value="specItemss",required=false) String specItemss,
    		@RequestParam(value="goodsSpecss") String goodsSpecss,
    		@RequestParam(value="goodsAttributess") String goodsAttributess) {
    	List<SpecItems> specItems = new ArrayList<>();
    	List<GoodsSpecs> goodsSpecs= new ArrayList<>();
    	List<GoodsAttributes> goodsAttributes = new ArrayList<>();
    	if(ToolUtil.isNotEmpty(specItemss)){
    		specItems = JSONArray.parseArray(specItemss, SpecItems.class);
    	}
    	if(ToolUtil.isNotEmpty(goodsSpecss)){
    		 goodsSpecs = JSONArray.parseArray(goodsSpecss, GoodsSpecs.class);
    	}
    	if(ToolUtil.isNotEmpty(goodsAttributess)){
    		 goodsAttributes = JSONArray.parseArray(goodsAttributess, GoodsAttributes.class);
    	}
        boolean insert = goodsService.updateAttrCat(specItems,goodsSpecs,goodsAttributes);
        return insert==true?Result.OK("修改商品成功！"):Result.EEROR("修改商品失败！");
    }
    
    /**
     * 删除商品
     */
    @RequestMapping(value = "/deleteGoods")
    @ResponseBody
    public Result deleteGoods(@RequestParam Integer goodsId) {
        boolean deleteById = goodsService.deleteGoods(goodsId);
        return deleteById==true?Result.OK(null):Result.EEROR("删除商品失败！");
    }
    /**
     * 批量删除商品
     */
    @RequestMapping(value = "/deleteBathGoods")
    @ResponseBody
    public Result deleteBathGoods(@RequestParam(value="goodsId[]",required=false) Integer[] goodsId) {
        boolean deleteById = goodsService.deleteBathGoods(goodsId);
        return deleteById==true?Result.OK(null):Result.EEROR("删除商品失败！");
    }
    /**
     * 删除商品属性
     */
    @RequestMapping(value = "/deleteGoodsAttr")
    @ResponseBody
    public Result deleteGoodsAttr(@RequestParam Integer id) {
        boolean deleteById = goodsAttributesService.deleteById(id);
        return deleteById==true?Result.OK(null):Result.EEROR("删除商品属性失败！");
    }
    
    /**
     * 删除销售规格
     */
    @RequestMapping(value = "/deleteSpecItems")
    @ResponseBody
    public Result deleteSpecItems(@RequestParam Integer itemId) {
        boolean deleteById = goodsSpecsService.deleteById(itemId);
        return deleteById==true?Result.OK(null):Result.EEROR("删除销售规格失败！");
    }
    
    /**
     * 删除商品规格
     */
    @RequestMapping(value = "/deleteGoodsSpecs")
    @ResponseBody
    public Result deleteGoodsSpecs(@RequestParam Integer id) {
        boolean deleteById = iSpecItemsService.deleteById(id);
        return deleteById==true?Result.OK(null):Result.EEROR("删除商品规格失败！");
    }
    
    /**
     * 商品id 查询商品在店铺中的分类
     */
    @RequestMapping(value = "/selectGoodsShopCat")
    @ResponseBody
    public Result selectGoodsShopCat(@RequestParam Integer goodsId) {
        Map<String,Object> map = goodsService.selectGoodsShopCat(goodsId);
        return Result.OK(map);
    }
    
    /**
     * 根据商品id查询评价
     */
    @RequestMapping(value = "/selectGoodsAppraises")
    @ResponseBody
    public Result selectGoodsAppraises(@RequestParam Integer goodsId) {
        List<GoodsAppraisesShow> list = goodsService.selectGoodsAppraises(goodsId);
        return Result.OK(list);
    }
    /**
     * 根据商品id查询评分
     */
    @RequestMapping(value = "/selectGoodsScores")
    @ResponseBody
    public Result selectGoodsScores(@RequestParam Integer goodsId) {
    	Map<String,Object>  map = goodsService.selectGoodsScores(goodsId);
        return Result.OK(map);
    }
    /**
     * 根据店铺id查询 热销商品
     */
    @RequestMapping(value = "/selectGoodsHots")
    @ResponseBody
    public Result selectGoodsHots(@RequestParam Integer shopId) {

		List<Map<String,Object>> list = goodsService.selectGoodsHots(shopId);
        return Result.OK(list);
    }
    
}
