package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.model.SktShopCats;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 店铺分类表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-29
 */
public interface ISktShopCatsService extends IService<SktShopCats> {
	
	/**
	 * 查询父类id
	 * @param goodsCatsId
	 * @return
	 */
	public List<SktShopCats> selectByParentId(Integer goodsCatsId);
	/**
	 * 查询该店铺的分类列表
	 * @param shopId
	 * @return
	 */
	public List<SktShopCats> selectShopIdList(Integer shopId,Integer parentId);
	public List categoryShop(Integer shopId);

}
