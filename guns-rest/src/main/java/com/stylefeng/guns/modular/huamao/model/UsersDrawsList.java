package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户提现表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public class UsersDrawsList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	private Integer drawId;
	/**
	 * 手机号
	 */
	private String userPhone;
	/**
	 * 用户昵称
	 */
	private String loginName;
	/**
	 * 真实姓名
	 */
	private String trueName;
	/**
	 * 身份证
	 */
	private String cardID;
	/**
	 * 银行卡号
	 */
	private String accNo;
	/**
	 * 是否手动处理
	 */
	private Integer specialType;
	/**
	 * 提现单号
	 */
	private String drawNo;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 银行卡表ID
	 */
	private Integer bankCardId;
	/**
	 * 提现的类型：1开元宝提现 2现金提现 3开元宝营业额提现
	 */
	private Integer type;
	/**
	 * 到账金额
	 */
	private BigDecimal money;
	/**
	 * 手续费+税率
	 */
	private BigDecimal fee;
	/**
	 * 提现手续费比例 %
	 */
	private BigDecimal ratio;
	/**
	 * 提现税率比例 %
	 */
	private BigDecimal taxratio;
	/**
	 * 提现状态 -2受理失败 -1审核拒绝 0审核中 2受理中 3受理成功
	 */
	private Integer cashSatus;
	/**
	 * 预计到账时间
	 */
	private Date getTime;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 拒绝理由
	 */
	private String checkRemark;
	/**
	 * 审核员工ID
	 */
	private Integer checkStaffId;
	/**
	 * 审核时间
	 */
	private Date checkTime;
	/**
	 * 查询创建时的开始时间
	 */
	private String cbeginTime;
	/**
	 * 查询创建时的结束时间
	 */
	private String cendTime;
	/**
	 * 查询预计到账时的开始时间
	 */
	private String ybeginTime;
	/**
	 * 查询预计到账时的结束时间
	 */
	private String yendTime;

	public UsersDrawsList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsersDrawsList(Integer drawId, String userPhone, String loginName, String trueName, String cardID,
			String accNo, Integer specialType, String drawNo, Integer userId, Integer bankCardId, Integer type,
			BigDecimal money, BigDecimal fee, BigDecimal ratio, BigDecimal taxratio, Integer cashSatus, Date getTime,
			Date createTime, String checkRemark, Integer checkStaffId, Date checkTime, String cbeginTime,
			String cendTime, String ybeginTime, String yendTime) {
		super();
		this.drawId = drawId;
		this.userPhone = userPhone;
		this.loginName = loginName;
		this.trueName = trueName;
		this.cardID = cardID;
		this.accNo = accNo;
		this.specialType = specialType;
		this.drawNo = drawNo;
		this.userId = userId;
		this.bankCardId = bankCardId;
		this.type = type;
		this.money = money;
		this.fee = fee;
		this.ratio = ratio;
		this.taxratio = taxratio;
		this.cashSatus = cashSatus;
		this.getTime = getTime;
		this.createTime = createTime;
		this.checkRemark = checkRemark;
		this.checkStaffId = checkStaffId;
		this.checkTime = checkTime;
		this.cbeginTime = cbeginTime;
		this.cendTime = cendTime;
		this.ybeginTime = ybeginTime;
		this.yendTime = yendTime;
	}

	public Integer getDrawId() {
		return drawId;
	}

	public void setDrawId(Integer drawId) {
		this.drawId = drawId;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getCardID() {
		return cardID;
	}

	public void setCardID(String cardID) {
		this.cardID = cardID;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public Integer getSpecialType() {
		return specialType;
	}

	public void setSpecialType(Integer specialType) {
		this.specialType = specialType;
	}

	public String getDrawNo() {
		return drawNo;
	}

	public void setDrawNo(String drawNo) {
		this.drawNo = drawNo;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getBankCardId() {
		return bankCardId;
	}

	public void setBankCardId(Integer bankCardId) {
		this.bankCardId = bankCardId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public BigDecimal getRatio() {
		return ratio;
	}

	public void setRatio(BigDecimal ratio) {
		this.ratio = ratio;
	}

	public BigDecimal getTaxratio() {
		return taxratio;
	}

	public void setTaxratio(BigDecimal taxratio) {
		this.taxratio = taxratio;
	}

	public Integer getCashSatus() {
		return cashSatus;
	}

	public void setCashSatus(Integer cashSatus) {
		this.cashSatus = cashSatus;
	}

	public Date getGetTime() {
		return getTime;
	}

	public void setGetTime(Date getTime) {
		this.getTime = getTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCheckRemark() {
		return checkRemark;
	}

	public void setCheckRemark(String checkRemark) {
		this.checkRemark = checkRemark;
	}

	public Integer getCheckStaffId() {
		return checkStaffId;
	}

	public void setCheckStaffId(Integer checkStaffId) {
		this.checkStaffId = checkStaffId;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public String getCbeginTime() {
		return cbeginTime;
	}

	public void setCbeginTime(String cbeginTime) {
		this.cbeginTime = cbeginTime;
	}

	public String getCendTime() {
		return cendTime;
	}

	public void setCendTime(String cendTime) {
		this.cendTime = cendTime;
	}

	public String getYbeginTime() {
		return ybeginTime;
	}

	public void setYbeginTime(String ybeginTime) {
		this.ybeginTime = ybeginTime;
	}

	public String getYendTime() {
		return yendTime;
	}

	public void setYendTime(String yendTime) {
		this.yendTime = yendTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "UsersDrawsList [drawId=" + drawId + ", userPhone=" + userPhone + ", loginName=" + loginName
				+ ", trueName=" + trueName + ", cardID=" + cardID + ", accNo=" + accNo + ", specialType=" + specialType
				+ ", drawNo=" + drawNo + ", userId=" + userId + ", bankCardId=" + bankCardId + ", type=" + type
				+ ", money=" + money + ", fee=" + fee + ", ratio=" + ratio + ", taxratio=" + taxratio + ", cashSatus="
				+ cashSatus + ", getTime=" + getTime + ", createTime=" + createTime + ", checkRemark=" + checkRemark
				+ ", checkStaffId=" + checkStaffId + ", checkTime=" + checkTime + ", cbeginTime=" + cbeginTime
				+ ", cendTime=" + cendTime + ", ybeginTime=" + ybeginTime + ", yendTime=" + yendTime + "]";
	}

}
