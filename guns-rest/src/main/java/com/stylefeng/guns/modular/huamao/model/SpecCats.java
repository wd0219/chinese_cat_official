package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 商品规格分类表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_spec_cats")
public class SpecCats extends Model<SpecCats> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "catId", type = IdType.AUTO)
    private Integer catId;
    /**
     * 最后一级商品分类ID
     */
    private Integer goodsCatId;
    /**
     * 商品分类路径
     */
    private String goodsCatPath;
    /**
     * 类型名称
     */
    private String catName;
    /**
     * 是否允许上传图片：0不允许1允许
     */
    private Integer isAllowImg;
    /**
     * 是否显示：1是2否
     */
    private Integer isShow;
    /**
     * 排序号
     */
    private Integer catSort;
    /**
     * 有效状态：1有效-1无效
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getGoodsCatId() {
        return goodsCatId;
    }

    public void setGoodsCatId(Integer goodsCatId) {
        this.goodsCatId = goodsCatId;
    }

    public String getGoodsCatPath() {
        return goodsCatPath;
    }

    public void setGoodsCatPath(String goodsCatPath) {
        this.goodsCatPath = goodsCatPath;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Integer getIsAllowImg() {
        return isAllowImg;
    }

    public void setIsAllowImg(Integer isAllowImg) {
        this.isAllowImg = isAllowImg;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getCatSort() {
        return catSort;
    }

    public void setCatSort(Integer catSort) {
        this.catSort = catSort;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.catId;
    }

    @Override
    public String toString() {
        return "SpecCats{" +
        "catId=" + catId +
        ", goodsCatId=" + goodsCatId +
        ", goodsCatPath=" + goodsCatPath +
        ", catName=" + catName +
        ", isAllowImg=" + isAllowImg +
        ", isShow=" + isShow +
        ", catSort=" + catSort +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
