package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktUserAllOrders;

import java.util.List;
import java.util.Map;

public interface IAppOrderService extends IService<SktOrders> {
    List<SktUserAllOrders> allOrder(Integer userId);

    List<Map<String,Object>> selectOrderAndOrderRefundsByUserId(Map<Object, Object> map);

    Map<String,Object> selectReceiveOrders(Map<String, Object> map);

    SktOrders selectOrderByUserId(Map<String, Object> map);

    void updateOrderStatusByOrderNo(Map<String, Object> map);

    SktOrders selectOrderByIsPay(Map<String, Object> map1);

    Double selectSumTotalMoney(Map<String, Object> map1);
    public Double selectSumPayScore(Map<String, Object> map1);

    public List<SktOrders> findOrdersByOrderunique(String orderNo);
    public Double selectSumPayKaiyuan(Map<String, Object> map1);
    public Double selectSumPayShopping(Map<String, Object> map1);
}
