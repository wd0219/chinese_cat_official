package com.stylefeng.guns.modular.huamao.model;

public class AttributesCustem extends Attributes{
	private  Integer goods1;
	private  Integer goods2;
	private  Integer goods3;
	private  Integer goods;
	private String catName1;
	private String catName2;
	private String catName3;
	public String getCatName1() {
		return catName1;
	}
	public void setCatName1(String catName1) {
		this.catName1 = catName1;
	}
	public String getCatName2() {
		return catName2;
	}
	public void setCatName2(String catName2) {
		this.catName2 = catName2;
	}
	public String getCatName3() {
		return catName3;
	}
	public void setCatName3(String catName3) {
		this.catName3 = catName3;
	}
	public Integer getGoods() {
		return goods;
	}
	public void setGoods(Integer goods) {
		this.goods = goods;
	}
	public Integer getGoods1() {
		return goods1;
	}
	public void setGoods1(Integer goods1) {
		this.goods1 = goods1;
	}
	public Integer getGoods2() {
		return goods2;
	}
	public void setGoods2(Integer goods2) {
		this.goods2 = goods2;
	}
	public Integer getGoods3() {
		return goods3;
	}
	public void setGoods3(Integer goods3) {
		this.goods3 = goods3;
	}
}
