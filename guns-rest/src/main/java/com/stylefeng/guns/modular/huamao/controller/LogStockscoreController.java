package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnoverList;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;
import com.stylefeng.guns.modular.huamao.model.LogStockscoreList;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ILogStockscoreService;

/**
 * 库存积分记录控制器
 *
 * @author fengshuonan
 * @Date 2018-04-17 10:41:35
 */
@CrossOrigin
@Controller
@RequestMapping("/logStockscore")
public class LogStockscoreController extends BaseController {

	private String PREFIX = "/huamao/logStockscore/";

	@Autowired
	private ILogStockscoreService logStockscoreService;

	/**
	 * 跳转到库存积分记录首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, @RequestParam(required = false) String type,
			Model model) {
		// 参数是为了在用户点击库存积分查看冻结库存积分详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (type != null && type != "") {
			model.addAttribute("model2", type);
		} else {
			model.addAttribute("model2", "");
		}
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logStockscore.html";
	}

	/**
	 * 跳转到添加库存积分记录
	 */
	@RequestMapping("/logStockscore_add")
	public String logStockscoreAdd() {
		return PREFIX + "logStockscore_add.html";
	}

	/**
	 * 跳转到修改库存积分记录
	 */
	@RequestMapping("/logStockscore_update/{logStockscoreId}")
	public String logStockscoreUpdate(@PathVariable Integer logStockscoreId, Model model) {
		LogStockscore logStockscore = logStockscoreService.selectById(logStockscoreId);
		model.addAttribute("item", logStockscore);
		LogObjectHolder.me().set(logStockscore);
		return PREFIX + "logStockscore_edit.html";
	}

	/**
	 * 获取库存积分记录列表：{userPhone2}用户查看库存积分详情传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone,
			@RequestParam(required = false) String type2, LogStockscoreList logStockscoreList) {
		// 判断如果不是‘手机号’并且logStockscoreList.getUserPhone()不是null说明要看用户积分详情，是‘手机号’并且logStockscoreList.getUserPhone()是''或是有数据说明是普通的查询所有的现金详情
		if (!phone.equals("手机号") && logStockscoreList.getUserPhone() == null) {
			logStockscoreList.setUserPhone(phone);
		}
		if (type2 != null && type2 != "" && logStockscoreList.getType() == null) {
			logStockscoreList.setType(Integer.parseInt(type2));
		}
		return logStockscoreService.selectLogStockScoreAll(logStockscoreList);
		// return logStockscoreService.selectList(null);
	}

	/**
	 * 新增库存积分记录
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogStockscore logStockscore) {
		logStockscoreService.insert(logStockscore);
		return SUCCESS_TIP;
	}

	/**
	 * 删除库存积分记录
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logStockscoreId) {
		logStockscoreService.deleteById(logStockscoreId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改库存积分记录
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogStockscore logStockscore) {
		logStockscoreService.updateById(logStockscore);
		return SUCCESS_TIP;
	}

	/**
	 * 库存积分记录详情
	 */
	@RequestMapping(value = "/detail/{logStockscoreId}")
	@ResponseBody
	public Object detail(@PathVariable("logStockscoreId") Integer logStockscoreId) {
		return logStockscoreService.selectById(logStockscoreId);
	}
	/*************商城***************/
	/**
	 * 商城库存积分
	 */
	@RequestMapping(value = "/selectStockscore")
	@ResponseBody
	public Result selectStockscore(LogStockscoreList logStockscoreList,Integer pageNum) {
		PageHelper.startPage(pageNum,12);
		List<Map<String,Object>> list = logStockscoreService.selectStockscoref(logStockscoreList);
    	PageInfo<Map<String, Object>> info = new PageInfo<>(list);
		return Result.OK(list,info.getPages());
	}

}
