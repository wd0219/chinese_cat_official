package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 代理公司股东申请表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-17
 */
@TableName("skt_agents_applys")
public class AgentsApplys extends Model<AgentsApplys> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "shaId", type = IdType.AUTO)
    private Integer shaId;
    /**
     * 申请代理公司股东单号
     */
    private String orderNo;
    /**
     * 代理公司ID
     */
    private Integer agentId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 申请人手机号码
     */
    private String phone;
    /**
     * 股东类型 1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
     */
    private Integer type;
    /**
     * 拥有的股票分数
     */
    private Integer stockNum;
    /**
     * 缴纳代理费用
     */
    private BigDecimal money;
    /**
     * 打款备注
     */
    private String remark;
    /**
     * -2二审拒绝 -1一审拒绝 0审核中 1一审同意 2汇款单已上传 3二审完结
     */
    private Integer status;
    /**
     * 汇款单的图片
     */
    private String moneyOrderImg;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 拒绝理由
     */
    private String checkRemark;
    /**
     * 审核员工ID
     */
    private Integer oneCheckStaffId;
    /**
     * 审核时间
     */
    private Date oneCheckTime;
    /**
     * 二审员工ID
     */
    private Integer twoCheckStaffId;
    /**
     * 二审时间
     */
    private Date twoCheckTime;
    /**
     * 试用期结束时间
     */
    private Date testTime;
    /**
     * 处理状态 0无需处理 1未处理 2已处理 9处理中
     */
    private Integer batFlag;
    /**
     * 定时器处理时间（定时器 管理股东3个月到期，上线2倍积分由待发积分账户到正式积分账户）
     */
    private Date batTime;


    public Integer getShaId() {
        return shaId;
    }

    public void setShaId(Integer shaId) {
        this.shaId = shaId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMoneyOrderImg() {
        return moneyOrderImg;
    }

    public void setMoneyOrderImg(String moneyOrderImg) {
        this.moneyOrderImg = moneyOrderImg;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }

    public Integer getOneCheckStaffId() {
        return oneCheckStaffId;
    }

    public void setOneCheckStaffId(Integer oneCheckStaffId) {
        this.oneCheckStaffId = oneCheckStaffId;
    }

    public Date getOneCheckTime() {
        return oneCheckTime;
    }

    public void setOneCheckTime(Date oneCheckTime) {
        this.oneCheckTime = oneCheckTime;
    }

    public Integer getTwoCheckStaffId() {
        return twoCheckStaffId;
    }

    public void setTwoCheckStaffId(Integer twoCheckStaffId) {
        this.twoCheckStaffId = twoCheckStaffId;
    }

    public Date getTwoCheckTime() {
        return twoCheckTime;
    }

    public void setTwoCheckTime(Date twoCheckTime) {
        this.twoCheckTime = twoCheckTime;
    }

    public Date getTestTime() {
        return testTime;
    }

    public void setTestTime(Date testTime) {
        this.testTime = testTime;
    }

    public Integer getBatFlag() {
        return batFlag;
    }

    public void setBatFlag(Integer batFlag) {
        this.batFlag = batFlag;
    }

    public Date getBatTime() {
        return batTime;
    }

    public void setBatTime(Date batTime) {
        this.batTime = batTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.shaId;
    }

    @Override
    public String toString() {
        return "AgentsApplys{" +
        "shaId=" + shaId +
        ", orderNo=" + orderNo +
        ", agentId=" + agentId +
        ", userId=" + userId +
        ", phone=" + phone +
        ", type=" + type +
        ", stockNum=" + stockNum +
        ", money=" + money +
        ", remark=" + remark +
        ", status=" + status +
        ", moneyOrderImg=" + moneyOrderImg +
        ", createTime=" + createTime +
        ", checkRemark=" + checkRemark +
        ", oneCheckStaffId=" + oneCheckStaffId +
        ", oneCheckTime=" + oneCheckTime +
        ", twoCheckStaffId=" + twoCheckStaffId +
        ", twoCheckTime=" + twoCheckTime +
        ", testTime=" + testTime +
        ", batFlag=" + batFlag +
        ", batTime=" + batTime +
        "}";
    }
}
