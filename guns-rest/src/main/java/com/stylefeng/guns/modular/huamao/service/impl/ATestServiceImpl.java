package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.stylefeng.guns.modular.huamao.aliyun.OSSResultModel;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.util.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Service
public class ATestServiceImpl {

    @Autowired
    private FileUtils fileUtils;

    /**
     * 富文本编辑框图片上传阿里云
     * @return
     * @throws IOException
     */
    public Map<String,Object> upload(HttpServletRequest req){
        Map<String,Object> result = new HashMap<String, Object>();

        MultipartHttpServletRequest mReq  =  null;
        MultipartFile file = null;
        InputStream is = null ;
        OSSResultModel ossResult = null;
        String fileName = "";
        // 原始文件名   UEDITOR创建页面元素时的alt和title属性
        String originalFileName = "";

        try {
            mReq = (MultipartHttpServletRequest)req;
            // 从config.json中取得上传文件的ID
            file = mReq.getFile("file");
            // 取得文件的原
            fileName = file.getOriginalFilename();
            originalFileName = fileName;
            if(!StringUtils.isEmpty(fileName)){
                is = file.getInputStream();
                fileName = fileUtils.reName(fileName);
                ossResult = fileUtils.saveFile(fileName, is);
            } else {
                throw new IOException("文件名为空!");
            }

            String[] strings = new String[5];
            strings[0] = ossResult.getUrl();


            result.put("errno",0);
            result.put("data",strings);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            result.put("errno",1);
            result.put("data","");
            System.out.println("文件 "+fileName+" 上传失败!");
        }

        return result;
    }
    /**
     * 通用图片上传阿里云
     * @return
     * @throws IOException
     */
    public Result imageUpload(HttpServletRequest req){
        Map<String,Object> result = new HashMap<String, Object>();

        MultipartHttpServletRequest mReq  =  null;
        MultipartFile file = null;
        InputStream is = null ;
        OSSResultModel ossResult = null;
        String fileName = "";
        // 原始文件名   UEDITOR创建页面元素时的alt和title属性
        String originalFileName = "";

        try {
            mReq = (MultipartHttpServletRequest)req;
            // 从config.json中取得上传文件的ID
            file = mReq.getFile("file");
            // 取得文件的原
            fileName = file.getOriginalFilename();
            originalFileName = fileName;
            if(!StringUtils.isEmpty(fileName)){
                is = file.getInputStream();
                fileName = fileUtils.reName(fileName);
                ossResult = fileUtils.saveFile(fileName, is);
            } else {
                throw new IOException("文件名为空!");
            }

            return Result.OK(ossResult.getUrl());

        }
        catch (Exception e) {
            return Result.EEROR("文件 "+fileName+" 上传失败!");
        }

    }
}
