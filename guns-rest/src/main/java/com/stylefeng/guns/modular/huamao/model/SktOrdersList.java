package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class SktOrdersList extends SktOrders{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 点评名称
     */
    private String shopName;
    /**
     * 店铺手机
     */
    private String telephone;
    /**
     * 营业执照所在省id
     */
    private Integer provinceId;
    /**
     * 营业执照所在市id
     */
    private Integer cityId;
    /**
     * 营业执照所在区县id
     */
    private Integer areaId;
    /**
     * 查询时间开始
     */
    private String beginTime;
    private Integer isAppraise;
    private Integer orderStatus;
    private String sOrderStatus;
   // orderId,orderNO,createTime,totalMoney,totalMoney*100 as totalMoneyScore,orderStatus,orderRemarks
    private String endTime;
    private String orderNO;
    private Date createTime;
    private BigDecimal totalMoney;
    private BigDecimal totalMoneyScore;
    private String orderRemarks;
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Integer getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getAreaId() {
		return areaId;
	}
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getOrderNO() {
		return orderNO;
	}
	public void setOrderNO(String orderNO) {
		this.orderNO = orderNO;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}
	public BigDecimal getTotalMoneyScore() {
		return totalMoneyScore;
	}
	public void setTotalMoneyScore(BigDecimal totalMoneyScore) {
		this.totalMoneyScore = totalMoneyScore;
	}
	public String getOrderRemarks() {
		return orderRemarks;
	}
	public void setOrderRemarks(String orderRemarks) {
		this.orderRemarks = orderRemarks;
	}
	public Integer getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getsOrderStatus() {
		return sOrderStatus;
	}
	public void setsOrderStatus(String sOrderStatus) {
		this.sOrderStatus = sOrderStatus;
	}
	public Integer getIsAppraise() {
		return isAppraise;
	}
	public void setIsAppraise(Integer isAppraise) {
		this.isAppraise = isAppraise;
	}
	
	
}
