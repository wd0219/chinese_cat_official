package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.OptBing;
import com.stylefeng.guns.modular.huamao.dao.OptBingMapper;
import com.stylefeng.guns.modular.huamao.service.IOptBingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作并发限制 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-06-28
 */
@Service
public class OptBingServiceImpl extends ServiceImpl<OptBingMapper, OptBing> implements IOptBingService {

    @Autowired
    private OptBingMapper optBingMapper;

    @Override
    public void deleteByCodeAndUid(OptBing optBing) {
        optBingMapper.deleteByCodeAndUid(optBing);
    }
}
