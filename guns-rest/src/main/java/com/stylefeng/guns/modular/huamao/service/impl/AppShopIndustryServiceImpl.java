package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopIndustryMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;
import com.stylefeng.guns.modular.huamao.service.IAppShopIndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppShopIndustryServiceImpl extends ServiceImpl<SktShopIndustryMapper,SktShopIndustry> implements IAppShopIndustryService {
    @Autowired
    private SktShopIndustryMapper sktShopIndustryMapper;

    @Override
    public List<Map<String, Object>> selectShopsIndustryByHot() {
        return sktShopIndustryMapper.selectShopsIndustryByHot();
    }

    @Override
    public List<Map<String, Object>> selectAllList() {
        return sktShopIndustryMapper.selectAllList();
    }
}
