package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktCommentMapper;
import com.stylefeng.guns.modular.huamao.model.SktComment;
import com.stylefeng.guns.modular.huamao.service.IAppSktCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class IAppSktCommentServiceImpl extends ServiceImpl<SktCommentMapper,SktComment> implements IAppSktCommentService {
    @Autowired
    private SktCommentMapper sktCommentMapper;
    @Override
    public List<Map<String, Object>> selectListByClassId(Map<String, Object> map) {
        return sktCommentMapper.selectListByClassId(map);
    }

    @Override
    public List<Map<String, Object>> selectClassByComment(Map<String, Object> map) {
        return sktCommentMapper.selectClassByComment(map);
    }

}
