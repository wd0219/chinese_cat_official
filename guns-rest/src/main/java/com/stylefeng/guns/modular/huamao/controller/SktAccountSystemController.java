package com.stylefeng.guns.modular.huamao.controller;


import java.util.List;
import java.util.Date;


import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.SktAccountSystem;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktAccountSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;


/**
 * <p>
 * 系统账户表  前端控制器
 * </p>
 * @author wd123
 * @since 2018-05-25
 */
@Controller
public class SktAccountSystemController extends BaseController {

	@Autowired private ISktAccountSystemService sktAccountSystemService;

	/**
	 * 跳转列表页面
	 * @param model
	 * @return
	 */
	@RequestMapping("/sktAccountSystemIndex")
	public String index(Model model) {
        return "sktAccountSystemListIndex";
	}

	/**
	 * 跳转添加页面
	 * @param model
	 * @return
	 */
	@RequestMapping("/sktAccountSystemAdd")
	public String sktAccountSystemAdd(Model model) {
        return "sktAccountSystemAdd";
	}

	/**
	 * 跳转修改页面
	 * @param id  实体ID
	 * @return
	 */
	@RequestMapping("/sktAccountSystemUpdate/{id}")
	public String sktAccountSystemUpdate(@PathVariable Integer id,Model model) {
        model.addAttribute("id", id);
        return "sktAccountSystemUpd";
	}

	/**
	 * 分页查询数据
	 * @return
	 */
//	@RequestMapping("/getSktAccountSystemPageList")
//	@ResponseBody
//	public PageInfo dataGrid(SktAccountSystem sktAccountSystem, Integer page, Integer rows, String sort,String order) {
//        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
//        EntityWrapper<SktAccountSystem> ew = new EntityWrapper<SktAccountSystem>(sktAccountSystem);
//        Page<SktAccountSystem> pages = getPage(page, rows, sort, order);
//        pages = sktAccountSystemService.selectPage(pages, ew);
//        pageInfo.setRows(pages.getRecords());
//        pageInfo.setTotal(pages.getTotal());
//        return pageInfo;
//	}

	/**
	 * 添加
	 * @param
	 * @return
	 */
	@RequestMapping("/add")
    @ResponseBody
	public Object add(SktAccountSystem sktAccountSystem, BindingResult result) {
	//	sktAccountSystem.setCreateTime(new Date());
        boolean b = sktAccountSystemService.insert(sktAccountSystem);
        if (b) {
        	return Result.OK("添加成功！");
        } else {
        	return Result.EEROR("添加失败！");
        }
	}


	/**
	 * 根据id删除对象
	 * @param id  实体ID
	 * @return 0 失败  1 成功
	 */
	@RequestMapping("/sysRoleDelete")
    @ResponseBody
	public Object sysRoleDelete(Integer id){
        try {
			sktAccountSystemService.deleteById(id);
        	return Result.OK("添加成功！");
        }catch (Exception e){
        	return Result.EEROR("添加失败！");
        }
	}

	/**
	 * 批量删除对象
	 * @param ids 实体集合ID
	 * @return  0 失败  1 成功
	 */
	@RequestMapping("/sysRoleBatchDelete")
    @ResponseBody
	public Object deleteBatchIds(List  ids){
        try {
			sktAccountSystemService.deleteBatchIds(ids);
        	return Result.OK("添加成功！");
        }catch (Exception e){
        	return Result.EEROR("添加失败！");
        }
	}

	/**
	 * 编辑
	 * @param 
	 * @return
	 */
	@RequestMapping("/edit")
    @ResponseBody
	public Object edit(SktAccountSystem sktAccountSystem) {
		boolean b = sktAccountSystemService.updateById(sktAccountSystem);
		if (b) {
        	return Result.OK("添加成功！");
		} else {
        	return Result.EEROR("添加失败！");
		}
	}
	
}
