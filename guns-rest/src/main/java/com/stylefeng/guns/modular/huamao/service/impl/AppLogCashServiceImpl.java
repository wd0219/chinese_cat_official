package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.stylefeng.guns.modular.huamao.dao.LogCashMapper;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppLogCashService;
import com.stylefeng.guns.modular.huamao.util.RemarksGenerate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AppLogCashServiceImpl extends ServiceImpl<LogCashMapper,LogCash> implements IAppLogCashService {

    @Autowired
    private LogCashMapper logCashMapper;

    @Override
    public AppResult AccountCashRecord(Integer pageIndex,LogCash logCash) {
        PageHelper.startPage(Integer.valueOf(pageIndex),15);
        List<LogCash> logCashData=logCashMapper.selectAccountCashRecordByUserId(logCash);
        List<Object> list = new ArrayList<>();
        if (logCashData.size()>0) {
            for (LogCash logCash1 : logCashData) {
                Map<String, Object> map = new HashMap<>();
                map.put("orderNo", logCash1.getOrderNo());
                map.put("cash", logCash1.getCash().toString());
                map.put("cashType", logCash1.getCashType());
                map.put("type", RemarksGenerate.type(logCash1.getType()));
                map.put("createTime", logCash1.getCreateTime());
                list.add(map);
            }
        }
        return AppResult.OK("获取成功",list);
    }

    @Override
    public LogCash selectSumCash(Map<String, Object> map) {
        return logCashMapper.selectSumLogCash(map);
    }

}
