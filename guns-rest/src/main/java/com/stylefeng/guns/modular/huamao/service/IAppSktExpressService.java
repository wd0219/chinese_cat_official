package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktExpress;

public interface IAppSktExpressService extends IService<SktExpress> {
    SktExpress selectExpressById(Integer expressId);
}
