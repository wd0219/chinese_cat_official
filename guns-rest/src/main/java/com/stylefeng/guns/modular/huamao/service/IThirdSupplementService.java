package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.ThirdSupplementList;

import java.util.List;

/**
 * 第三方支付补单记录服务层
 * 
 * @author gxz
 *
 */
public interface IThirdSupplementService extends IService<ThirdSupplementList> {

	// 第三方支付补单表展示
	List<ThirdSupplementList> selectThirdSupplementAll(ThirdSupplementList thirdSupplementList);

}
