package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktSpecApply;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品规则申请表 Mapper 接口
 * </p>
 *
 * @author py
 * @since 2018-08-23
 */
public interface SktSpecApplyMapper extends BaseMapper<SktSpecApply> {

}
