package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.Attributes;
import com.stylefeng.guns.modular.huamao.model.AttributesCustem;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品属性表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface AttributesMapper extends BaseMapper<Attributes> {
	Integer insertAttributes(AttributesCustem attributesCustem);
	List<Map<String, Object>> selectAll(AttributesCustem attributesCustem); 
	AttributesCustem selectAttributesCustem(Integer id);
}
