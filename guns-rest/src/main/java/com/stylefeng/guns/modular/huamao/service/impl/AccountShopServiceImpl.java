package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AccountShopMapper;
import com.stylefeng.guns.modular.huamao.model.AccountShop;
import com.stylefeng.guns.modular.huamao.model.AccountShopList;
import com.stylefeng.guns.modular.huamao.service.IAccountShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商家账户表(包括线上和线下) 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-12
 */
@Service
public class AccountShopServiceImpl extends ServiceImpl<AccountShopMapper, AccountShop> implements IAccountShopService {
	@Autowired
	private AccountShopMapper asm;

	// 展示用户账户列表
	@Override
	public List<AccountShopList> selectAccountShopAll(AccountShopList accountShopList) {
		return asm.selectAccountShopAll(accountShopList);
	}

    @Override
    public AccountShop selectAccountShopByUserId(Integer userId) {
        return asm.selectAccountShopByUserId(userId);
    }

    @Override
    public boolean updateKaiyuanTurnoverByUserId(Map<String, Object> map) {
        int i=asm.updateKaiyuanTurnoverByUserId(map);
        if (i!=1){
            return false;
        }
	    return true;
    }

	@Override
	public AccountShop selectByUserId(Integer userId) {
		AccountShop accountShop = asm.selectByUserId(userId);
		return accountShop;
	}

}
