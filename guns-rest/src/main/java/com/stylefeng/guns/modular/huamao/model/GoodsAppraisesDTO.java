package com.stylefeng.guns.modular.huamao.model;

import java.util.List;
import java.util.Map;

public class GoodsAppraisesDTO {
	private Integer orderId;
	private String goodsSn;
	private String goodsName;
	private String goodsImg;
	private Integer goodsId;
	private List<Map<String,Object>> appraises;
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getGoodsSn() {
		return goodsSn;
	}
	public void setGoodsSn(String goodsSn) {
		this.goodsSn = goodsSn;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsImg() {
		return goodsImg;
	}
	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}
	public List<Map<String, Object>> getAppraises() {
		return appraises;
	}
	public void setAppraises(List<Map<String, Object>> appraises) {
		this.appraises = appraises;
	}
	public Integer getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	
	
}
