package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.GoodsAttributes;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品属性表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsAttributesMapper extends BaseMapper<GoodsAttributes> {

}
