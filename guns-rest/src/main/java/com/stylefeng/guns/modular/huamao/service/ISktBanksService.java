package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktBanks;

/**
 * <p>
 * 银行表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface ISktBanksService extends IService<SktBanks> {

}
