package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktDateTransformMapper;
import com.stylefeng.guns.modular.huamao.model.SktDateTransform;
import com.stylefeng.guns.modular.huamao.service.IAppDateTransformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppDateTransformServiceImpl extends ServiceImpl<SktDateTransformMapper, SktDateTransform>  implements IAppDateTransformService {
    @Autowired
    private SktDateTransformMapper sktDateTransformMapper;
    @Override
    public List<SktDateTransform> selectDateTransform() {
        return sktDateTransformMapper.selectDateTransform();
    }
}
