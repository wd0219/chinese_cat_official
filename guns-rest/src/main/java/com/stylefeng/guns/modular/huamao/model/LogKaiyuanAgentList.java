package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 代理公司开元宝流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public class LogKaiyuanAgentList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户手机
	 */
	private String phone;
	/**
	 * 开元宝类型1转换获得 2赎回拒绝退还 3赎回拒绝的手续费和税率的退还 32赎回支出 33赎回的手续费和税率
	 */
	private Integer type;
	/**
	 * 目标代理公司ID
	 */
	private Integer agentId;
	/**
	 * 对应订单号
	 */
	private String orderNo;
	/**
	 * 操作前的金额
	 */
	private BigDecimal preKaiyuan;
	/**
	 * 流水标志 -1减少 1增加
	 */
	private Integer kaiyuanType;
	/**
	 * 金额
	 */
	private BigDecimal kaiyuan;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String kaiyuanp;
	/**
	 * 变动后金额
	 */
	private BigDecimal akaiyuan;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 有效状态 1有效 0删除
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getPreKaiyuan() {
		return preKaiyuan;
	}
	public void setPreKaiyuan(BigDecimal preKaiyuan) {
		this.preKaiyuan = preKaiyuan;
	}
	public Integer getKaiyuanType() {
		return kaiyuanType;
	}
	public void setKaiyuanType(Integer kaiyuanType) {
		this.kaiyuanType = kaiyuanType;
	}
	public BigDecimal getKaiyuan() {
		return kaiyuan;
	}
	public void setKaiyuan(BigDecimal kaiyuan) {
		this.kaiyuan = kaiyuan;
	}
	public String getKaiyuanp() {
		return kaiyuanp;
	}
	public void setKaiyuanp(String kaiyuanp) {
		this.kaiyuanp = kaiyuanp;
	}
	public BigDecimal getAkaiyuan() {
		return akaiyuan;
	}
	public void setAkaiyuan(BigDecimal akaiyuan) {
		this.akaiyuan = akaiyuan;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	

}
