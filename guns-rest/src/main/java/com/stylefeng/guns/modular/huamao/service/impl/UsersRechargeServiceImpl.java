package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersRechargeMapper;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;
import com.stylefeng.guns.modular.huamao.model.UsersRechargeDTO;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IUsersRechargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户充值表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
@Service
public class UsersRechargeServiceImpl extends ServiceImpl<UsersRechargeMapper, UsersRecharge> implements IUsersRechargeService {
	
	@Autowired
	private UsersRechargeMapper usersRechargeMapper;
	@Autowired
	private  ATestServiceImpl aTestService;
	@Override
	public List<UsersRechargeDTO> showUserDengDanInfo(UsersRechargeDTO usersRechargeDTO) {
		List<UsersRechargeDTO> list = usersRechargeMapper.showUserDengDanInfo(usersRechargeDTO);
		return list;
	}
@Override
@Transactional
	public Map<String, Object> Recharge(HttpServletRequest request,String orderNO){
		Map<String, Object> resultMap =new HashMap<>();
		try{
			Result result = aTestService.imageUpload(request);
			if (result.getMessageCode()==200){
				UsersRecharge usersRecharge=new UsersRecharge();
				usersRecharge.setOrderNo(orderNO);
				EntityWrapper<UsersRecharge> entityWrapper=new EntityWrapper<UsersRecharge>(usersRecharge);
				UsersRecharge usersRecharge1 = this.selectOne(entityWrapper);
				usersRecharge1.setOrderImg(result.getEntity().toString());
				this.updateById(usersRecharge1);
				resultMap.put("code","01");
				resultMap.put("msg","修改成功 ");
				return resultMap;
			}else{
				resultMap.put("code","00");
				resultMap.put("msg","上传凭证失败");
				return resultMap;
			}
		}catch (Exception e){
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			resultMap.put("code","00");
			resultMap.put("msg","出错了");
			return resultMap;
		}
	}
}
