package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.stylefeng.guns.modular.huamao.controller.SktExchangeUserController;
import com.stylefeng.guns.modular.huamao.dao.SktSysConfigsMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersDrawsMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.GetDateUtil;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 用户提现表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
@Service
public class UsersDrawsServiceImpl extends ServiceImpl<UsersDrawsMapper, UsersDraws> implements IUsersDrawsService {

	@Autowired
	UsersDrawsMapper udm;
	@Autowired
	private UsersMapper usersMapper;
	@Autowired
	private SktSysConfigsMapper sktSysConfigsMapper;
	@Autowired
	private IAccountService  accountService;
	@Autowired
	private IAccountShopService accountShopService;
	@Autowired
	private IUsersService usersService;
	@Autowired
	ISktHuamaobtLogService sktHuamaobtLogService;
	@Autowired
    private ISktDateService iSktDateService;
	@Autowired
    private IAppLogKaiyuanService iAppLogKaiyuanService;
	@Autowired
    private IUsersDrawsMaobiService iUsersDrawsMaobiService;
	@Autowired
    private IAppLogCashService iAppLogCashService;
	@Autowired
    private ILogKaiyuanTurnoverService iLogKaiyuanTurnoverService;
    @Autowired
    ISktExchangeUserService exchangeUserService;

	// 用户提现表展示
	@Override
	public List<UsersDrawsList> selectUsersDrawsAll(UsersDrawsList usersDrawsList) {
		return udm.selectUsersDrawsAll(usersDrawsList);
	}

	// 修改审核状态
	@Override
	public void updateSatus(UsersDrawsList usersDrawsList) {
		udm.updateSatus(usersDrawsList);
	}

	/**
	 * 用户点击赎回
	 * @param userId  用户id
	 * @param type  赎回类型  1开元宝提现 2现金提现 3开元宝营业额提现
	 */
	public JSONObject clickRedeem(Integer userId, Integer type){
		JSONObject json=new JSONObject();
		json.put("type", type);
		SktSysConfigs taxRatio = sktSysConfigsMapper.selectSysConfigsByFieldCode("taxRatio");//综合服务费%
		String  taxRatios=taxRatio.getFieldValue();//综合服务费用
		BigDecimal ratio=new BigDecimal(taxRatios);
	//	json.put("ratio", ratio);
		SktSysConfigs drawsAmountMin = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMin");
		String  drawsAmountMins=drawsAmountMin.getFieldValue();//最小单笔赎回数量
		SktSysConfigs drawsAmountMax = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMax");
		String  drawsAmountMaxs=drawsAmountMax.getFieldValue();//最大单笔赎回数量
		SktSysConfigs drawsAmountDay = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountDay");
		String  drawsAmountDays=drawsAmountDay.getFieldValue();//每天最大赎回额度

		json.put("drawsAmountMin", drawsAmountMins);
		json.put("drawsAmountMax", drawsAmountMaxs);
		json.put("drawsAmountDay", drawsAmountDays);
		BigDecimal totalUsersDraws = udm.getTotalUsersDraws(userId);//今日已经提现总金额
        if (totalUsersDraws==null){
            json.put("totalUsersDraws", "0.00");
        }else{
            json.put("totalUsersDraws", totalUsersDraws.toString());
        }

		if (type==1) {
			SktSysConfigs kaiyuanbaoTXt0 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt0");//华宝赎回% T+1
			SktSysConfigs kaiyuanbaoTXt4 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt4");//华宝赎回% T+4
			SktSysConfigs kaiyuanbaoTXt7 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt7");//华宝赎回% T+7
		//	BigDecimal huaBaoTXt0=new BigDecimal(kaiyuanbaoTXt0.getFieldValue());
			String  huaBaoTXt0=kaiyuanbaoTXt0.getFieldValue();
			String  huaBaoTXt4=kaiyuanbaoTXt4.getFieldValue();
		//	BigDecimal huaBaoTXt4=new BigDecimal(kaiyuanbaoTXt4.getFieldValue());
			String  huaBaoTXt7=kaiyuanbaoTXt7.getFieldValue();
		//	BigDecimal huaBaoTXt7=new BigDecimal(kaiyuanbaoTXt7.getFieldValue());
			json.put("huaBaoTXt0",huaBaoTXt0);
			json.put("huaBaoTXt4",huaBaoTXt4);
			json.put("huaBaoTXt7",huaBaoTXt7);
            json.put("taxRatio", taxRatios);
//			json.put("huaBaoTXt0", huaBaoTXt0.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
//			json.put("huaBaoTXt4",  huaBaoTXt4 .add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
//			json.put("huaBaoTXt7", huaBaoTXt7.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
		}else if (type==2){
            json.put("taxRatio", "0");
			Users users = usersMapper.selectById(userId);
			if(users.getUserType()==0){
				SktSysConfigs commonCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("commonCashDraw");
				String commonCashDraws = commonCashDraw.getFieldValue();
				json.put("commonCashDraws", commonCashDraws);
//				BigDecimal  commonCashDraws=new BigDecimal(commonCashDraw.getFieldValue());
//				json.put("commonCashDraw", commonCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
			}else if(users.getUserType()==1){
				SktSysConfigs directorCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("directorCashDraw");
				String directorCashDraws=directorCashDraw.getFieldValue();
				json.put("commonCashDraws", directorCashDraws);
//				BigDecimal  directorCashDraws= new BigDecimal(directorCashDraw.getFieldValue());
//				json.put("directorCashDraw", directorCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
			}else if(users.getUserType()==2){
				SktSysConfigs managerCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("managerCashDraw");
				String managerCashDraws=managerCashDraw.getFieldValue();
				json.put("commonCashDraws", managerCashDraws);
//				BigDecimal  managerCashDraws=new BigDecimal(managerCashDraw.getFieldValue());
//				json.put("managerCashDraw", managerCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
			}
		}else if(type==3){
            json.put("taxRatio", "0");
			SktSysConfigs kaiyuanYingyeTX = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanYingyeTX");
			String kaiyuanYingyeTXs= kaiyuanYingyeTX.getFieldValue();
			json.put("kaiyuanYingyeTX", kaiyuanYingyeTXs);
//			BigDecimal  kaiyuanYingyeTXs=new BigDecimal(kaiyuanYingyeTX.getFieldValue());
//			json.put("kaiyuanYingyeTX", kaiyuanYingyeTXs.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
		}
		return json;
	}

    /**
     * 赎回申请
     * @param userId  用户ID
     * @param type  提现的类型：1开元宝提现 2现金提现 3开元宝营业额提现
     * @param money   提现金额
     * @param expenses  手续费(钱数）
     * @param expensesrate  提现手续费比例 %   （可变的那个  华宝T+1 T+4 T+7 ···）
     * @param taxRatio   提现税率比例% （10%）
     * @param ratio  提现税（钱数）
     * @param payPwd
     * @return
     */
    @Override
    public   JSONObject  applyRedeem (Integer userId,Integer type,BigDecimal money,BigDecimal expenses,BigDecimal expensesrate,BigDecimal taxRatio,BigDecimal ratio,String payPwd) {
        JSONObject json = new JSONObject();
        boolean b=true;
        if (b){
            json.put("code","-01");
            json.put("msg","请到花猫商城移动端赎回");
            return json;
        }
        Users user = usersMapper.selectById(userId);
        if (user.getUserType()==0){
            json.put("status",-1);
            json.put("msg","普通用户没有申请赎回权限,请升级用户");
            return json;
        }
        if (user.getIsWithdrawCash()!=1){
            json.put("status",-1);
            json.put("msg","您没有申请赎回权限");
            return json;
        }

        //判定今天是否为工作日
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String format1 = format.format(new Date());
        int date = Integer.parseInt(format1);

        SktDate sktDate=iSktDateService.selectDateByDate(date);
        if (sktDate.getStatus()!=0){
            if (sktDate.getStatus()==1){
                json.put("status",-1);
                json.put("msg","今日为休息日不可进行提现操作");
                return json;
            }
            SktSysConfigs sktSysConfigs = sktSysConfigsMapper.selectSysConfigsByFieldCode("isHolidaysCash");
            if (Integer.parseInt(sktSysConfigs.getFieldValue())!=1){
                json.put("status",-1);
                json.put("msg","今日不可进行提现操作");
                return json;
            }
        }

        SktSysConfigs sktSysConfigs = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountDay");
        Integer fieldValue = Integer.parseInt(sktSysConfigs.getFieldValue());
        BigDecimal moneyValue=money;
        JSONObject jsonObject = usersService.checkPayPassWorld(userId, payPwd);
        if ("00".equals(jsonObject.get("code"))){
            return jsonObject;
        }
        if (!MSGUtil.isBlank(user.getPrivilegeTime())) {
            long nextThreeMonth = GetDateUtil.getNextAnyMonth(3,user.getPrivilegeTime()).getTime();
            long privilegeTime = new Date().getTime();
//            long nextThreeMonth = GetDateUtil.getNextAnyMonth(3,new Date()).getTime();
//            long privilegeTime = user.getPrivilegeTime().getTime();
            if(nextThreeMonth>privilegeTime){
                json.put("status",-1);
                json.put("msg","特权账号三个月内不能提现！");
                return json;
            }
        }
        //获取用户账户信息
        Account account = accountService.selectAccountByuserId(userId);
        //获取商家账户信息
        AccountShop accountShop=accountShopService.selectAccountShopByUserId(userId);
        //校验赎回人状态
        if (MSGUtil.isBlank(user.getTrueName())){
            json.put("status",-1);
            json.put("msg","您还未实名！");
            return json;
        }
        SktSysConfigs drawsAmountMin = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMin");
        SktSysConfigs drawsAmountMax = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMax");
        Double min=Double.valueOf(drawsAmountMin.getFieldValue());
        Double max=Double.valueOf(drawsAmountMax.getFieldValue());
        //校验计算手续费
        BigDecimal feeMoney = money.multiply(expensesrate).multiply(new BigDecimal(0.01)).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal ratioMoney = money.multiply(taxRatio).multiply(new BigDecimal(0.01)).setScale(2, BigDecimal.ROUND_HALF_UP);
        if (type==2||type==3){
            ratioMoney=new BigDecimal(0);
        }
        if (money.doubleValue()<min){
            json.put("status",-1);
            json.put("msg","单笔最小赎回额度为:"+min.toString());
            return json;
        }
        if (money.doubleValue()>max){
            json.put("status",-1);
            json.put("msg","单笔最大赎回额度为:"+max.toString());
            return json;
        }
        BigDecimal allFee = feeMoney.add(ratioMoney);//手续费+综合服务费(总手续费)
        /*int compare = feeMoney.compareTo(expenses);
        if (compare!=0){
            json.put("status",-1);
            json.put("msg","手续费计算有误！");
            return json;
        }
        if (ratioMoney.compareTo(ratio)!=0){
            json.put("status",-1);
            json.put("msg","交综合服务费金额计算有误！");
            return json;
        }*/

        Date dayBegin = GetDateUtil.getDayBegin();
        Date dayEnd = GetDateUtil.getDayEnd();
        Map<String, Object> map = new HashMap<>();
        map.put("startTime",dayBegin);
        map.put("endTime",dayEnd);
        map.put("userId",userId);
        UsersDrawsMaobi dayHandle=iUsersDrawsMaobiService.selectSumMoneyByUserId(map);
        SktSysConfigs drawsAmountDays = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountDay");
        BigDecimal drawsAmountDay = new BigDecimal(drawsAmountDays.getFieldValue());
        BigDecimal biggest=BigDecimal.ZERO;
        if(dayHandle!=null){
            biggest = dayHandle.getMoney()==null?money:dayHandle.getMoney().add(money);
        }

        if (drawsAmountDay.compareTo(biggest)==-1){
            json.put("status",-1);
            json.put("msg","每日最大赎回额度为:"+drawsAmountDay+",本日还可提现"+drawsAmountDay.subtract(drawsAmountDay));
            return json;
        }
        //根据不同赎回类型判断对应余额是否足够 $param['type']  1华宝赎回 2现金赎回 3华宝营业额赎回
        BigDecimal countMoney=money;
        BigDecimal currentMoney=new BigDecimal(0);
        if (type==1){
            if ((money.divideAndRemainder(BigDecimal.valueOf(100))[1]).compareTo(BigDecimal.valueOf(0))!=0){
                json.put("status",-1);
                json.put("msg","华宝赎回所得金额必须为100的倍数！");
                return json;
            }
            if (account.getKaiyuan().compareTo(countMoney.multiply(BigDecimal.valueOf(100)))==-1){
                json.put("status",-1);
                json.put("msg","华宝余额不足！");
                return json;
            }
            currentMoney=account.getKaiyuan();
//            account.setKaiyuan(currentMoney.subtract(money.multiply(new BigDecimal(100))));
//            accountService.updateById(account);
        }
        if (type==2){
            if (account.getCash().compareTo(countMoney)==-1){
                json.put("status",-1);
                json.put("msg","现金余额不足！");
                return json;
            }
            currentMoney=account.getCash();
         //   account.setCash(currentMoney.subtract(money));
//            accountService.updateById(account);
        }
        if (type==3){
            if(accountShop.getKaiyuanTurnover().compareTo(countMoney.multiply(BigDecimal.valueOf(100)))==-1){
                json.put("status",-1);
                json.put("msg","华宝营业额余额不足！");
                return json;
            }
            currentMoney=accountShop.getKaiyuanTurnover();
//            accountShop.setKaiyuanTurnover(currentMoney.subtract(money.multiply(new BigDecimal(100))));
//            accountShopService.updateById(accountShop);
        }

        //计算 赎回基本业务逻辑处理
        String orderNo = StringUtils.getOrderIdByTime("F");
        boolean result = this.draw(userId, money, type, expensesrate, taxRatio, allFee, orderNo, currentMoney);
        if (result){
            json.put("status",1);
            json.put("msg","赎回申请提交成功！");
            return json;
        }
        json.put("status",-1);
        json.put("msg","赎回申请提交失败！");
        return json;
    }

    /**
     * 赎回   计算 赎回基本业务逻辑处理
     * @param type $uid 用户ID
     * @param type $money 赎回金额
     * @param type $type 赎回类型
     * @param type $expensesrate 手续费比例
     * @param type $taxRatio 综合服务费比例
     * @param type $allFee 手续费加综合服务费
     * @param type $bankCardId 用户赎回卡ID
     * @param type $orderNo 订单号
     * @param type $currentMoney 当前额度
     * @return boolean
     */
    public boolean draw(Integer userId, BigDecimal money, Integer type, BigDecimal expensesrate, BigDecimal taxRatio, BigDecimal allFee, String orderNo, BigDecimal currentMoney) {
        try {
            if (money.compareTo(BigDecimal.valueOf(0))==-1){
                return false;
            }
            BigDecimal allmoney=money;//需要从余额扣除的金额
            BigDecimal hasmoney=money.subtract(allFee);//预计得到金额
            Date time = new Date();
            //赎回记录
            UsersDrawsMaobi usersDrawsMaobi = new UsersDrawsMaobi();
            usersDrawsMaobi.setDrawNo(orderNo);
            usersDrawsMaobi.setUserId(userId);
            usersDrawsMaobi.setType(type);
            usersDrawsMaobi.setMoney(hasmoney);
            usersDrawsMaobi.setFee(allFee);
            usersDrawsMaobi.setRatio(expensesrate);
            usersDrawsMaobi.setTaxratio(taxRatio);
            usersDrawsMaobi.setCashSatus(0);
            usersDrawsMaobi.setCreateTime(new Date());
            //插入赎回记录表
            iUsersDrawsMaobiService.insert(usersDrawsMaobi);
            if (type==1){//华宝
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> map = new HashMap<>();
                map.put("userId",userId);
                map.put("allmoney",allmoney.multiply(BigDecimal.valueOf(100)));
                map.put("kaiyuan","kaiyuan");
                boolean b = accountService.uptateKaiyuanByUserId(map);
                if (!b){
                    return false;
                }
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //华宝赎回流水
                    LogKaiyuan logKaiyuan = new LogKaiyuan();
                    logKaiyuan.setType(33);
                    logKaiyuan.setFromId(0);
                    logKaiyuan.setUserId(userId);
                    logKaiyuan.setOrderNo(orderNo);
                    logKaiyuan.setPreKaiyuan(currentMoney);
                    logKaiyuan.setKaiyuanType(-1);
                    logKaiyuan.setKaiyuan(hasmoney.multiply(BigDecimal.valueOf(100)));
                    logKaiyuan.setRemark("赎回支出猫币");
                    logKaiyuan.setDataFlag(1);
                    logKaiyuan.setCreateTime(new Date());
                    boolean insert = iAppLogKaiyuanService.insert(logKaiyuan);
                    //华宝赎回手续费流水
                    logKaiyuan=new LogKaiyuan();
                    logKaiyuan.setType(34);
                    logKaiyuan.setFromId(0);
                    logKaiyuan.setUserId(userId);
                    logKaiyuan.setOrderNo(orderNo);
                    logKaiyuan.setPreKaiyuan(currentMoney.subtract(hasmoney.multiply(BigDecimal.valueOf(100))));
                    logKaiyuan.setKaiyuanType(-1);
                    logKaiyuan.setKaiyuan(allFee.multiply(BigDecimal.valueOf(100)));
                    logKaiyuan.setRemark("赎回支出手续费+综合服务费");
                    logKaiyuan.setDataFlag(1);
                    logKaiyuan.setCreateTime(new Date());
                    boolean inserts = iAppLogKaiyuanService.insert(logKaiyuan);
                }
            }
            if (type==2){//现金
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> map = new HashMap<>();
                map.put("userId",userId);
                map.put("allmoney",allmoney);
                map.put("cash","cash");
                boolean b = accountService.uptateKaiyuanByUserId(map);
                if (!b){
                    return false;
                }
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //现金赎回流水
                    LogCash logCash = new LogCash();
                    logCash.setType(31);
                    logCash.setFromId(0);
                    logCash.setUserId(userId);
                    logCash.setOrderNo(orderNo);
                    logCash.setPreCash(currentMoney);
                    logCash.setCashType(-1);
                    logCash.setCash(hasmoney);
                    logCash.setRemark("赎回支出猫币");
                    logCash.setDataFlag(1);
                    logCash.setCreateTime(new Date());
                    boolean insert = iAppLogCashService.insert(logCash);
                    //现金赎回手续费流水
                    logCash=new LogCash();
                    logCash.setType(36);
                    logCash.setFromId(0);
                    logCash.setUserId(userId);
                    logCash.setOrderNo(orderNo);
                    logCash.setPreCash(currentMoney.subtract(hasmoney));
                    logCash.setCashType(-1);
                    logCash.setCash(allFee);
                    logCash.setRemark("赎回支出手续费");
                    logCash.setDataFlag(1);
                    logCash.setCreateTime(new Date());
                    boolean inserts = iAppLogCashService.insert(logCash);
                }
            }
            if (type==3){//华宝营业额
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> map = new HashMap<>();
                map.put("userId",userId);
                map.put("allmoney",allmoney.multiply(BigDecimal.valueOf(100)));
                boolean b=accountShopService.updateKaiyuanTurnoverByUserId(map);
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //华宝营业额赎回流水
                    LogKaiyuanTurnover logKaiyuanTurnover = new LogKaiyuanTurnover();
                    logKaiyuanTurnover.setType(33);
                    logKaiyuanTurnover.setFromId(0);
                    logKaiyuanTurnover.setUserId(userId);
                    logKaiyuanTurnover.setOrderNo(orderNo);
                    logKaiyuanTurnover.setPreKaiyuan(currentMoney);
                    logKaiyuanTurnover.setKaiyuanType(-1);
                    logKaiyuanTurnover.setKaiyuan(hasmoney.multiply(BigDecimal.valueOf(100)));
                    logKaiyuanTurnover.setRemark("赎回支出猫币");
                    logKaiyuanTurnover.setDataFlag(1);
                    logKaiyuanTurnover.setCreateTime(new Date());
                    boolean insert = iLogKaiyuanTurnoverService.insert(logKaiyuanTurnover);
                    //华宝营业额赎回手续费流水
                    logKaiyuanTurnover=new LogKaiyuanTurnover();
                    logKaiyuanTurnover.setType(34);
                    logKaiyuanTurnover.setFromId(0);
                    logKaiyuanTurnover.setUserId(userId);
                    logKaiyuanTurnover.setOrderNo(orderNo);
                    logKaiyuanTurnover.setPreKaiyuan(currentMoney.subtract(hasmoney.multiply(BigDecimal.valueOf(100))));
                    logKaiyuanTurnover.setKaiyuanType(-1);
                    logKaiyuanTurnover.setKaiyuan(allFee.multiply(BigDecimal.valueOf(100)));
                    logKaiyuanTurnover.setRemark("赎回支出手续费");
                    logKaiyuanTurnover.setDataFlag(1);
                    logKaiyuanTurnover.setCreateTime(new Date());
                    boolean inserts = iLogKaiyuanTurnoverService.insert(logKaiyuanTurnover);
                }
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("失败");
        }
    }

	/***************************APP*********************/
	@Override

	public JSONObject cashDraw(Integer userId){
		JSONObject json = new JSONObject();
		Account account=new Account();
		account.setUserId(userId);
		EntityWrapper<Account> entityWrapper=new EntityWrapper<Account>(account);
		Account account1 = accountService.selectOne(entityWrapper);
		JSONObject userAccount = new JSONObject();
		userAccount.put("kaiyuan",account1.getKaiyuan().toString());
		userAccount.put("cash",account1.getCash().toString());
		json.put("userAccount",userAccount);
		AccountShop accountShop=new AccountShop();
		accountShop.setUserId(userId);
		EntityWrapper<AccountShop> entityWrapper1 =new EntityWrapper<AccountShop>(accountShop);
		AccountShop accountShop1 = accountShopService.selectOne(entityWrapper1);
		JSONObject shopAccount = new JSONObject();
		if (accountShop1!=null){

			if (accountShop1.getKaiyuanTurnover()!=null){
				shopAccount.put("kaiyuanTurnover",accountShop1.getKaiyuanTurnover().toString());
			}else {
				shopAccount.put("kaiyuanTurnover","0.00");
			}
		}else {
			shopAccount.put("kaiyuanTurnover","0.00");
		}
		json.put("shopAccount",shopAccount);
		Users users = usersService.selectById(userId);
		json.put("userType",users.getUserType());
		if(users.getUserType()==0){
			SktSysConfigs commonCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("commonCashDraw");
			String commonCashDraws = commonCashDraw.getFieldValue();
			json.put("commonCashDraw", commonCashDraws);
//				BigDecimal  commonCashDraws=new BigDecimal(commonCashDraw.getFieldValue());
//				json.put("commonCashDraw", commonCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
		}else if(users.getUserType()==1){
			SktSysConfigs directorCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("directorCashDraw");
			String directorCashDraws=directorCashDraw.getFieldValue();
			json.put("commonCashDraw", directorCashDraws);
//				BigDecimal  directorCashDraws= new BigDecimal(directorCashDraw.getFieldValue());
//				json.put("directorCashDraw", directorCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
		}else if(users.getUserType()==2){
			SktSysConfigs managerCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("managerCashDraw");
			String managerCashDraws=managerCashDraw.getFieldValue();
			json.put("commonCashDraw", managerCashDraws);
//				BigDecimal  managerCashDraws=new BigDecimal(managerCashDraw.getFieldValue());
//				json.put("managerCashDraw", managerCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
		}
        /**
         * 开始调用交易所接口获取利率
         */

		/*SktSysConfigs taxRatio = sktSysConfigsMapper.selectSysConfigsByFieldCode("taxRatio");//综合服务费%
		String  taxRatios=taxRatio.getFieldValue();//综合服务费用
		BigDecimal ratio=new BigDecimal(taxRatios);*/
		//	json.put("ratio", ratio);
		SktSysConfigs drawsAmountMin = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMin");
		String  drawsAmountMins=drawsAmountMin.getFieldValue();//最小单笔赎回数量
		SktSysConfigs drawsAmountMax = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMax");
		String  drawsAmountMaxs=drawsAmountMax.getFieldValue();//最大单笔赎回数量
		SktSysConfigs drawsAmountDay = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountDay");
		String  drawsAmountDays=drawsAmountDay.getFieldValue();//每天最大赎回额度
        SktSysConfigs taxRatio = sktSysConfigsMapper.selectSysConfigsByFieldCode("taxRatio");//综合服务费%
        String  taxRatios=taxRatio.getFieldValue();//综合服务费用
		json.put("taxRatio", new BigDecimal(taxRatios));//综合服务费

        json.put("moneyRate",1);

		json.put("drawsAmountMin", drawsAmountMins);//最小单笔赎回数量
		json.put("drawsAmountMax", drawsAmountMaxs);//最大单笔赎回数量
		json.put("drawsAmountDay", drawsAmountDays);//每天最大赎回额度
		BigDecimal totalUsersDraws = udm.getTotalUsersDraws(userId);//今日已经提现总金额(包括申请的）
		if (totalUsersDraws==null){
			json.put("totalUsersDraws", "0.00");//今日已经提现总金额（包括申请的)
		}else {
			json.put("totalUsersDraws", totalUsersDraws.toString());//今日已经提现总金额（包括申请的)
		}

		SktSysConfigs kaiyuanbaoTXt0 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt0");//华宝赎回% T+1
		SktSysConfigs kaiyuanbaoTXt4 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt4");//华宝赎回% T+4
		SktSysConfigs kaiyuanbaoTXt7 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt7");//华宝赎回% T+7
		//	BigDecimal huaBaoTXt0=new BigDecimal(kaiyuanbaoTXt0.getFieldValue());
		String  huaBaoTXt0=kaiyuanbaoTXt0.getFieldValue();
		String  huaBaoTXt4=kaiyuanbaoTXt4.getFieldValue();
		//	BigDecimal huaBaoTXt4=new BigDecimal(kaiyuanbaoTXt4.getFieldValue());
		String  huaBaoTXt7=kaiyuanbaoTXt7.getFieldValue();
		//	BigDecimal huaBaoTXt7=new BigDecimal(kaiyuanbaoTXt7.getFieldValue());
		json.put("kaiyuanbaoTXt0",huaBaoTXt0);//开元宝提现手续费(1天)
		json.put("kaiyuanbaoTXt4",huaBaoTXt4);//开元宝提现手续费(4天)
		json.put("kaiyuanbaoTXt7",huaBaoTXt7);//开元宝提现手续费(7天)
		SktSysConfigs kaiyuanYingyeTX = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanYingyeTX");
		String kaiyuanYingyeTXs= kaiyuanYingyeTX.getFieldValue();
		json.put("kaiyuanYingyeTX", kaiyuanYingyeTXs);//开元宝营业额余额提现手续费
		return json;
	}



}
