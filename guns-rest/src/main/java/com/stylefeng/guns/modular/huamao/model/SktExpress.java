package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 快递表
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@TableName("skt_express")
public class SktExpress extends Model<SktExpress> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "expressId", type = IdType.AUTO)
    private Integer expressId;
    /**
     * 快递名称
     */
    private String expressName;
    /**
     * 删除标志
     */
    private Integer dataFlag;
    /**
     * 快递代码
     */
    private String expressCode;


    public Integer getExpressId() {
        return expressId;
    }

    public void setExpressId(Integer expressId) {
        this.expressId = expressId;
    }

    public String getExpressName() {
        return expressName;
    }

    public void setExpressName(String expressName) {
        this.expressName = expressName;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public String getExpressCode() {
        return expressCode;
    }

    public void setExpressCode(String expressCode) {
        this.expressCode = expressCode;
    }

    @Override
    protected Serializable pkVal() {
        return this.expressId;
    }

    @Override
    public String toString() {
        return "SktExpress{" +
        "expressId=" + expressId +
        ", expressName=" + expressName +
        ", dataFlag=" + dataFlag +
        ", expressCode=" + expressCode +
        "}";
    }
}
