package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.Articles;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 文章记录表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
public interface ArticlesMapper extends BaseMapper<Articles> {
	List<Map<String,Object>>selectArticles(Articles articles);

    Articles selectArticlesById(Integer articlesId);
    /**
     *  公司新闻
     * @return
     */
	public List<Map<String, Object>> articleCompanyNew();
	/**
     *  公司新闻id查询
     * @return
     */
	public Map<String, Object> companyNewById(Integer articleId);

}
