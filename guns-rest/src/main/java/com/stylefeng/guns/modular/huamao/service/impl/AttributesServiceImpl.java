package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AttributesMapper;
import com.stylefeng.guns.modular.huamao.model.Attributes;
import com.stylefeng.guns.modular.huamao.model.AttributesCustem;
import com.stylefeng.guns.modular.huamao.service.IAttributesService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品属性表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class AttributesServiceImpl extends ServiceImpl<AttributesMapper, Attributes> implements IAttributesService {

	@Override
	public Integer insertAttributes(AttributesCustem attributesCustem) {
		// TODO Auto-generated method stub
		return baseMapper.insertAttributes(attributesCustem);
	}

	@Override
	public List<Map<String, Object>> selectAll(AttributesCustem attributesCustem) {
		// TODO Auto-generated method stub
		return baseMapper.selectAll(attributesCustem);
	}

	@Override
	public AttributesCustem selectAttributesCustem(Integer id) {
		// TODO Auto-generated method stub
		return baseMapper.selectAttributesCustem(id);
	}

}
