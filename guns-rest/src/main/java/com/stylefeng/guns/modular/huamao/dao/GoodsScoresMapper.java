package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.GoodsScores;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 商品评分表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsScoresMapper extends BaseMapper<GoodsScores> {

    void updateGoodsScores(Map<String, Object> goodsShopScoresMap);
}
