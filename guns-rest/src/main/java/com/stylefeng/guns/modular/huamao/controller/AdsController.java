package com.stylefeng.guns.modular.huamao.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.modular.huamao.model.SktAdPositions;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktAdPositionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.Ads;
import com.stylefeng.guns.modular.huamao.model.AdsList;
import com.stylefeng.guns.modular.huamao.service.IAdsService;
import com.stylefeng.guns.modular.huamao.util.UrlUtils;

/**
 * 广告管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 16:35:08
 */
@Controller
@RequestMapping("/ads")
@CrossOrigin
public class AdsController extends BaseController {

	private String PREFIX = "/huamao/ads/";

	@Autowired
	private IAdsService adsService;

	@Autowired
	private ISktAdPositionsService sktAdPositionsService;

	/**
	 * 跳转到广告管理首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "ads.html";
	}

	/**
	 * 跳转到添加广告管理
	 */
	@RequestMapping("/ads_add")
	public String adsAdd() {
		return PREFIX + "ads_add.html";
	}

	/**
	 * 跳转到修改广告管理:通过id查询广告位修改广告做准备
	 */
	@RequestMapping("/ads_update/{adId}")
	public String adsUpdate(@PathVariable Integer adId, Model model) {
		AdsList adslist = adsService.selectAdById(adId);
		model.addAttribute("item", adslist);
		// LogObjectHolder.me().set(ads);
		return PREFIX + "ads_edit.html";
	}

	/**
	 * 获取广告管理列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(AdsList adsList) {
		return adsService.selectAdsAll(adsList);
		// return adsService.selectList(null);
	}

	/**
	 * 新增广告管理
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(AdsList adsList) {
		// 创建时间
		adsList.setCreateTime(new Date());
		// 添加广告
		adsService.addAds(adsList);
		// adsService.insert(ads);
		return SUCCESS_TIP;
	}

	/**
	 * 删除广告管理
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer adsId) {
		adsService.deleteById(adsId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改广告管理
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(AdsList adsList) {
		// 通过id修改广告
		adsService.updateAdById(adsList);
		// adsService.updateById(ads);
		return SUCCESS_TIP;
	}

	/**
	 * 广告管理详情
	 */
	@RequestMapping(value = "/detail/{adsId}")
	@ResponseBody
	public Object detail(@PathVariable("adsId") Integer adsId) {
		return adsService.selectById(adsId);
	}

	/**
	 * 异步获取广告位置
	 */
	@PostMapping("/getPosition")
	@ResponseBody
	public Object getPosition(String positionType) {
		return adsService.getPosition(positionType);
	}

	/**
	 *广告显示
	 * @return
	 */
	@RequestMapping(value = "/showAds")
	@ResponseBody
	public Object showAds(){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		//df.format(new Date());// new Date()为获取当前系统时间
		Ads ads = new Ads();
		ads.setPositionType(1);
		ads.setDataFlag(1);
		EntityWrapper<Ads> ew = new EntityWrapper<Ads>(ads);
		ew.where("adEndDate > {0}",df.format(new Date()));
		List<Ads> adsList = adsService.selectList(ew);

		return adsList;
	}

	/**
	 *首页顶部广告展示
	 * @return
	 */
	@RequestMapping(value = "/topShowAds")
	@ResponseBody
	public Object topShopAds(){
		SktAdPositions sktAdPositions=new SktAdPositions();
		sktAdPositions.setPositionCode("index-top-ads");
		EntityWrapper<SktAdPositions> ew=new EntityWrapper<SktAdPositions>(sktAdPositions);
		SktAdPositions sktAdPositions1 = sktAdPositionsService.selectOne(ew);
		Integer	adPositionId=sktAdPositions1.getPositionId();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		//df.format(new Date());// new Date()为获取当前系统时间
		Date date = new Date();
		String nowDate = df.format(date);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("nowDate",nowDate);
		map.put("adPositionId",adPositionId);
		map.put("dataFlag",1);
		map.put("limt",2);
		List<Ads> list = adsService.selectPcTopAds(map);
		Map<String,Object> map1 = new HashMap<String,Object>();
		if(list == null ||  list.size()==0){
			map1.put("code","00");
			return map1;
		}
		Ads ads = list.get(0);
		String adFile = ads.getAdFile();
		adFile = UrlUtils.getAllUrl(adFile);
		map1.put("code","01");
		map1.put("adFile",adFile);
		map1.put("adURL",ads.getAdURL());
		return map1;
	}
	/**
	 *首页弹窗广告
	 * @return
	 */
	@RequestMapping(value = "/getPcPopUp")
	@ResponseBody
	public Result jumpAds(){
		SktAdPositions sktAdPositions=new SktAdPositions();
		sktAdPositions.setPositionCode("pcPopUp");
		EntityWrapper<SktAdPositions> ew=new EntityWrapper<SktAdPositions>(sktAdPositions);
		SktAdPositions sktAdPositions1 = sktAdPositionsService.selectOne(ew);
		Integer	adPositionId=sktAdPositions1.getPositionId();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		//df.format(new Date());// new Date()为获取当前系统时间
		Date date = new Date();
		String nowDate = df.format(date);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("nowDate",nowDate);
		map.put("adPositionId",adPositionId);
		map.put("dataFlag",1);
		map.put("limt",1);
		List<Ads> list = adsService.selectPcTopAds(map);
	//	Map<String,Object> map1 = new HashMap<String,Object>();
		JSONObject map1=new JSONObject();
		if(list == null|| list.size()==0){
			map1.put("code","00");
			return Result.OK(map1);
		}
		Ads ads = list.get(0);
		String adFile = ads.getAdFile();
		adFile = UrlUtils.getAllUrl(adFile);
		map1.put("code","01");
		map1.put("adFile",adFile);
		map1.put("adURL",ads.getAdURL());
		return Result.OK(map1);
	}

	/**
	 *首页弹窗左侧广告
	 * @return
	 */
	@RequestMapping(value = "/getLeftAds")
	@ResponseBody
	public Result getLeftAds(){
		SktAdPositions sktAdPositions=new SktAdPositions();
		sktAdPositions.setPositionCode("pcLeftFixUp");
		EntityWrapper<SktAdPositions> ew=new EntityWrapper<SktAdPositions>(sktAdPositions);
		SktAdPositions sktAdPositions1 = sktAdPositionsService.selectOne(ew);
		Integer	adPositionId=sktAdPositions1.getPositionId();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		//df.format(new Date());// new Date()为获取当前系统时间
		Date date = new Date();
		String nowDate = df.format(date);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("nowDate",nowDate);
		map.put("adPositionId",adPositionId);
		map.put("dataFlag",1);
		map.put("limt",1);
		List<Ads> list = adsService.selectPcTopAds(map);
		//	Map<String,Object> map1 = new HashMap<String,Object>();
		JSONObject map1=new JSONObject();
		if(list == null|| list.size()==0){
			map1.put("code","00");
			return Result.OK(map1);
		}
		Ads ads = list.get(0);
		String adFile = ads.getAdFile();
		adFile = UrlUtils.getAllUrl(adFile);
		map1.put("code","01");
		map1.put("adFile",adFile);
		map1.put("adURL",ads.getAdURL());
		return Result.OK(map1);
	}

	/**
	 *首页弹窗右侧广告
	 * @return
	 */
	@RequestMapping(value = "/getRightAds")
	@ResponseBody
	public Result getRigthAds(){
		SktAdPositions sktAdPositions=new SktAdPositions();
		sktAdPositions.setPositionCode("pcRightFixDown");
		EntityWrapper<SktAdPositions> ew=new EntityWrapper<SktAdPositions>(sktAdPositions);
		SktAdPositions sktAdPositions1 = sktAdPositionsService.selectOne(ew);
		Integer	adPositionId=sktAdPositions1.getPositionId();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		//df.format(new Date());// new Date()为获取当前系统时间
		Date date = new Date();
		String nowDate = df.format(date);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("nowDate",nowDate);
		map.put("adPositionId",adPositionId);
		map.put("dataFlag",1);
		map.put("limt",1);
		List<Ads> list = adsService.selectPcTopAds(map);
		//	Map<String,Object> map1 = new HashMap<String,Object>();
		JSONObject map1=new JSONObject();
		if(list == null|| list.size()==0){
			map1.put("code","00");
			return Result.OK(map1);
		}
		Ads ads = list.get(0);
		String adFile = ads.getAdFile();
		adFile = UrlUtils.getAllUrl(adFile);
		map1.put("code","01");
		map1.put("adFile",adFile);
		map1.put("adURL",ads.getAdURL());
		return Result.OK(map1);
	}

	/**
	 *首页资讯下方广告
	 * @return
	 */
	@RequestMapping(value = "/indexArtBottom")
	@ResponseBody
	public Result indexArtBottom(){
		SktAdPositions sktAdPositions=new SktAdPositions();
		sktAdPositions.setPositionCode("index-art-bottom");
		EntityWrapper<SktAdPositions> ew=new EntityWrapper<SktAdPositions>(sktAdPositions);
		SktAdPositions sktAdPositions1 = sktAdPositionsService.selectOne(ew);
		Integer	adPositionId=sktAdPositions1.getPositionId();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		//df.format(new Date());// new Date()为获取当前系统时间
		Date date = new Date();
		String nowDate = df.format(date);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("nowDate",nowDate);
		map.put("adPositionId",adPositionId);
		map.put("dataFlag",1);
		map.put("limt",1);
		List<Ads> list = adsService.selectPcTopAds(map);
		//	Map<String,Object> map1 = new HashMap<String,Object>();
		JSONObject map1=new JSONObject();
		if(list == null|| list.size()==0){
			map1.put("code","00");
			return Result.OK(map1);
		}
		Ads ads = list.get(0);
		String adFile = ads.getAdFile();
		adFile = UrlUtils.getAllUrl(adFile);
		map1.put("code","01");
		map1.put("adFile",adFile);
		map1.put("adURL",ads.getAdURL());
		return Result.OK(map1);
	}
	/**
	 *首页今日推荐左侧广告
	 * @return
	 */
	@RequestMapping(value = "/adsRecommendLeft")
	@ResponseBody
	public Result adsRecommendLeft(){
		SktAdPositions sktAdPositions=new SktAdPositions();
		sktAdPositions.setPositionCode("ads-recommend-1");
		EntityWrapper<SktAdPositions> ew=new EntityWrapper<SktAdPositions>(sktAdPositions);
		SktAdPositions sktAdPositions1 = sktAdPositionsService.selectOne(ew);
		Integer	adPositionId=sktAdPositions1.getPositionId();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		//df.format(new Date());// new Date()为获取当前系统时间
		Date date = new Date();
		String nowDate = df.format(date);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("nowDate",nowDate);
		map.put("adPositionId",adPositionId);
		map.put("dataFlag",1);
		map.put("limt",1);
		List<Ads> list = adsService.selectPcTopAds(map);
		//	Map<String,Object> map1 = new HashMap<String,Object>();
		JSONObject map1=new JSONObject();
		if(list == null|| list.size()==0){
			map1.put("code","00");
			return Result.OK(map1);
		}
		Ads ads = list.get(0);
		String adFile = ads.getAdFile();
		adFile = UrlUtils.getAllUrl(adFile);
		map1.put("code","01");
		map1.put("adFile",adFile);
		map1.put("adURL",ads.getAdURL());
		return Result.OK(map1);
	}
	/**
	 *首页今日推荐右侧广告
	 * @return
	 */
	@RequestMapping(value = "/adsRecommendRight")
	@ResponseBody
	public Result adsRecommendRight(){
		SktAdPositions sktAdPositions=new SktAdPositions();
		sktAdPositions.setPositionCode("ads-recommend-2");
		EntityWrapper<SktAdPositions> ew=new EntityWrapper<SktAdPositions>(sktAdPositions);
		SktAdPositions sktAdPositions1 = sktAdPositionsService.selectOne(ew);
		Integer	adPositionId=sktAdPositions1.getPositionId();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式
		//df.format(new Date());// new Date()为获取当前系统时间
		Date date = new Date();
		String nowDate = df.format(date);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("nowDate",nowDate);
		map.put("adPositionId",adPositionId);
		map.put("dataFlag",1);
		map.put("limt",1);
		List<Ads> list = adsService.selectPcTopAds(map);
		//	Map<String,Object> map1 = new HashMap<String,Object>();
		JSONObject map1=new JSONObject();
		if(list == null|| list.size()==0){
			map1.put("code","00");
			return Result.OK(map1);
		}
		Ads ads = list.get(0);
		String adFile = ads.getAdFile();
		adFile = UrlUtils.getAllUrl(adFile);
		map1.put("code","01");
		map1.put("adFile",adFile);
		map1.put("adURL",ads.getAdURL());
		return Result.OK(map1);
	}
}
