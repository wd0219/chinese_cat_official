package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktHuamaobtLog;

import java.math.BigDecimal;
import java.util.Map;

public interface IAppSktMaobiChangeService extends IService<SktHuamaobtLog> {
    Map<String, Object> recharge(Integer userId,BigDecimal currentMoney,BigDecimal amount, String orderNo, BigDecimal moneyRate, BigDecimal money,Integer dtype);

    Object ticker();

}
