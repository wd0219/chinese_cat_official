package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysql.fabric.xmlrpc.base.Array;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.dao.GoodsCatsMapper;
import com.stylefeng.guns.modular.huamao.dao.GoodsMapper;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.model.SktShopCats;
import com.stylefeng.guns.modular.huamao.service.IAccountShopService;
import com.stylefeng.guns.modular.huamao.service.IAdsService;
import com.stylefeng.guns.modular.huamao.service.IGoodsCatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品分类表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsCatsServiceImpl extends ServiceImpl<GoodsCatsMapper, GoodsCats> implements IGoodsCatsService {
@Autowired
private GoodsCatsMapper goodsCatsMapper;
	@Autowired
	private GoodsMapper goodsMapper;
    @Autowired
    private IAdsService adsService;
	@Override
	public List<GoodsCats> selectByParentId(int parentId) {
		// TODO Auto-generated method stub
		return goodsCatsMapper.selectByParentId(parentId);
	}
	@Override
	public boolean deleteByPId(Integer pid) {
		// TODO Auto-generated method stub
		return goodsCatsMapper.deleteByPId(pid);
	}
	@Override
	public List<Map<String, Object>> selectMap(GoodsCats goodsCats) {
		// TODO Auto-generated method stub
		return goodsCatsMapper.selectMap( goodsCats);
	}
	@Override
	public List<Brands> selectCatName(String sIds) {
		List<Brands> list = goodsCatsMapper.selectCatName(sIds);
		return list;
	}

	/**
	 * 根据商品id查询分类
	 * @param goodsId
	 * @return
	 */
	@Override
	public JSONObject getGoodscatsByGoodsId(Integer goodsId ){
		JSONObject json = new JSONObject();
		Goods goods = goodsMapper.selectById(goodsId);
		if(goods!=null){
			String goodsCatIdPath = goods.getGoodsCatIdPath();
			String[] strList = goodsCatIdPath.split("_");
			for (int i=0;i<strList.length;i++){
				GoodsCats goodsCats = goodsCatsMapper.selectById(strList[i]);
				json.put("GoodsCatId"+(i+1),goodsCats.getCatId());
				json.put("GoodsCatName"+(i+1),goodsCats.getCatName());
			}
		}


		return  json;
	}

	/**
	 * 根据分类子id查询父类
	 * @param goodsCatId
	 * @return
	 */
	@Override
	public JSONObject getGoodscatsByGoodsCatId(Integer goodsCatId ){
		JSONObject json = new JSONObject();
		Integer catId=0;
			for (int i=0;i<3;i++){
				GoodsCats goodsCats = goodsCatsMapper.selectById(goodsCatId);
				json.put("GoodsCatId"+(i+1),goodsCatId);
				json.put("GoodsCatName"+(i+1),goodsCats.getCatName());
				goodsCatId=goodsCats.getParentId();
				if (goodsCats.getParentId()==0){
					break;
				}

			}



		return  json;
	}

	/***************************************app***********************************/
	@Override
	public List<Map<String,Object>>  selectGoodsCats(Integer parentId){
//		if (parentId==null){
//			parentId=0;
//		}
		return  goodsCatsMapper.selectGoodsCats(parentId);
	}

//	@Override
//	public Map<String, Object> category(Integer parentId){
//		Map<String, Object>  map=new HashMap<>();
//		GoodsCats goodsCats=new GoodsCats();
//		goodsCats.setParentId(0);
//		EntityWrapper<GoodsCats> entityWrapper=new EntityWrapper<GoodsCats>(goodsCats);
//		List<Map<String, Object>> firstCatsList =goodsCatsMapper. selectGoodsCats(parentId);
//		map.put("firstColumn",firstCatsList);
//		List<Map<String, Object>> secondCatsList = goodsCatsMapper.selectSecondCats(parentId);
//		map.put("secondColumn",secondCatsList);
//		List<Map<String, Object>> thirdCatsList = goodsCatsMapper.selectThirdCats(parentId);
//		map.put("thirdColumn",thirdCatsList);
//		return  map;
//	}

	@Override
	public List category(Integer parentId) {
		GoodsCats goodsCats=new GoodsCats();
		goodsCats.setParentId(0);
		EntityWrapper<GoodsCats> entityWrapper=new EntityWrapper<GoodsCats>(goodsCats);
		List<Map<String, Object>> firstCatsList1 =goodsCatsMapper. selectGoodsCats(parentId);
		List list=new ArrayList();
		Map<String, Object> paraMap=new HashMap<>();
		paraMap.put("positionCode","ads_classify_top1");
		List<Map<String, Object>> adses = adsService.getSiteHome(paraMap);
		Map<String, Object> map=new HashMap<>();

		if(adses!=null && adses.size()>0){

			map.put("adses",adses.get(0));
		}else{
            map.put("adses",new HashMap<>());
		}
		list.add(map);
		if (firstCatsList1!=null && firstCatsList1.size()>0){
			for (Map<String, Object> sktgoodsCats1:firstCatsList1){
				Map<String, Object> map1=new HashMap<>();
				map1.put("catId",sktgoodsCats1.get("catId"));
				map1.put("parentId",sktgoodsCats1.get("parentId"));
				map1.put("catName",sktgoodsCats1.get("catName"));
				map1.put("catImg",sktgoodsCats1.get("catImg"));
				List<Map<String, Object>> firstCatsList2 =goodsCatsMapper. selectGoodsCats(Integer.parseInt(sktgoodsCats1.get("catId").toString()));
				if(firstCatsList2!=null && firstCatsList2.size()>0){
					map1.put("secondColumn",firstCatsList2);
					list.add(map1);
				}

			}
		}

		return  list;
	}
	@Override
	public List<Map<String, Object>> getGoodscatsSelGoods(StringBuffer sb, Integer shopPrice1, Integer shopPrice2, 
					Integer saleNum, Integer shopPrice, Integer visitNum, Integer appraiseNum, Integer ssaleTime,
					Integer isNew, Integer goodsStock,String brandIds,String goodsName,Integer isSelf) {
		String[] str = null;
		if(sb.length()>0){
			str = sb.toString().split(",");
		}
		ArrayList<String> arrList = new ArrayList<String>();
		if(ToolUtil.isNotEmpty(brandIds)){
			String[] brandIdss = brandIds.split(",");
			arrList.addAll(Arrays.asList(brandIdss));
		}
		List<Map<String, Object>> list = goodsCatsMapper.getGoodscatsSelGoods(str,shopPrice1,shopPrice2,
				saleNum,shopPrice,visitNum,appraiseNum,ssaleTime,isNew,goodsStock,arrList,goodsName,isSelf);
		return list;
	}
	@Override
	public List<Map<String, Object>> selectShopCatsGoods(Integer shopId, Integer shopCatId, Integer shopPrice1,
			Integer shopPrice2, Integer saleNum, Integer shopPrice, Integer visitNum, Integer appraiseNum,
			Integer ssaleTime, Integer isNew, Integer goodsStock,String goodsName) {
		List<Map<String, Object>> list = goodsCatsMapper.selectShopCatsGoods(shopId,shopCatId,shopPrice1,
    			shopPrice2,saleNum,shopPrice,visitNum,appraiseNum,ssaleTime,isNew,goodsStock,goodsName);
		return list;
	}
	@Override
	public Map<String, Object> selectShopsCores(Integer shopId) {
		Map<String,Object> map = goodsCatsMapper.selectShopsCores(shopId);
		return map;
	}

	@Override
	public List<Map<String, Object>> showFloor() {
		List<Map<String,Object>> list = goodsCatsMapper.showFloor();
		return list;
	}
}
