package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Attributes;
import com.stylefeng.guns.modular.huamao.model.AttributesCustem;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品属性表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IAttributesService extends IService<Attributes> {
	Integer insertAttributes(AttributesCustem attributesCustem);
	List<Map<String, Object>> selectAll(AttributesCustem attributesCustem);
	AttributesCustem selectAttributesCustem(Integer id);

}
