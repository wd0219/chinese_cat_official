package com.stylefeng.guns.modular.huamao.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.UsersDraws;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsList;

/**
 * <p>
 * 用户提现表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface UsersDrawsMapper extends BaseMapper<UsersDraws> {

	// 用户提现表展示
	List<UsersDrawsList> selectUsersDrawsAll(UsersDrawsList usersDrawsList);

	// 修改审核状态
	void updateSatus(UsersDrawsList usersDrawsList);

    List<UsersDraws> presentProgress(Map<String, Object> map);
	//当天申请总额
	BigDecimal getTotalUsersDraws(Integer userId);
}
