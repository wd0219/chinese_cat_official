package com.stylefeng.guns.modular.huamao.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogCashList;

/**
 * <p>
 * 现金流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogCashMapper extends BaseMapper<LogCash> {

	// 现金流水返回
	List<LogCashList> selectLogCashAll(LogCashList logCashList);

	/**
	 * 首页返回
	 * @param logCash
	 * @return
	 */
    public List<Map<String,Object>> selectLogCash(LogCashList logCash);

    List<LogCash> selectAccountCashRecordByUserId(LogCash logCash);

	LogCash selectSumLogCash(Map<String, Object> map);

}
