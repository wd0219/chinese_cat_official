package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.ArticlesMapper;
import com.stylefeng.guns.modular.huamao.model.Articles;
import com.stylefeng.guns.modular.huamao.service.IArticlesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章记录表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
@Service
public class ArticlesServiceImpl extends ServiceImpl<ArticlesMapper, Articles> implements IArticlesService {
	
	@Autowired
	private ArticlesMapper articlesMapper;
	@Override
	public List<Map<String, Object>> selectArticles(Articles articles) {
		// TODO Auto-generated method stub
		return baseMapper.selectArticles(articles);
	}

	@Override
	public List<Map<String, Object>> articleCompanyNew() {
		List<Map<String, Object>> list = articlesMapper.articleCompanyNew();
		return list;
	}

	@Override
	public Map<String, Object> companyNewById(Integer articleId) {
		Map<String,Object> map = articlesMapper.companyNewById(articleId);
		return map;
	}


}
