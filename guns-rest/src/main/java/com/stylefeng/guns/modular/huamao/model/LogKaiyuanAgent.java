package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 代理公司开元宝流水表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
@TableName("skt_log_kaiyuan_agent")
public class LogKaiyuanAgent extends Model<LogKaiyuanAgent> {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 开元宝类型1转换获得 2赎回拒绝退还 3赎回拒绝的手续费和税率的退还 32赎回支出 33赎回的手续费和税率
	 */
	private Integer type;
	@TableField(exist=false)
	private String stype;
	/**
	 * 目标代理公司ID
	 */
	private Integer agentId;
	/**
	 * 对应订单号
	 */
	private String orderNo;
	/**
	 * 操作前的金额
	 */
	private BigDecimal preKaiyuan;
	/**
	 * 流水标志 -1减少 1增加
	 */
	private Integer kaiyuanType;
	/**
	 * 金额
	 */
	private BigDecimal kaiyuan;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 有效状态 1有效 0删除
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getStype() {
		return stype;
	}
	public void setStype(String stype) {
		this.stype = stype;
	}
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public BigDecimal getPreKaiyuan() {
		return preKaiyuan;
	}
	public void setPreKaiyuan(BigDecimal preKaiyuan) {
		this.preKaiyuan = preKaiyuan;
	}
	public Integer getKaiyuanType() {
		return kaiyuanType;
	}
	public void setKaiyuanType(Integer kaiyuanType) {
		this.kaiyuanType = kaiyuanType;
	}
	public BigDecimal getKaiyuan() {
		return kaiyuan;
	}
	public void setKaiyuan(BigDecimal kaiyuan) {
		this.kaiyuan = kaiyuan;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
