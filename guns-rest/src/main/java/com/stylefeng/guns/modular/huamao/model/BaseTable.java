package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;

import java.math.BigDecimal;

public class BaseTable {
    private static final long serialVersionUID = 1L;

    private BigDecimal todayRecharge;
    private BigDecimal mouthRecharge;
    private BigDecimal totalRecharge;
    private BigDecimal todayDraws;
    private BigDecimal mouthDraws;
    private BigDecimal totalDraws;
    private BigDecimal todaySale;
    private BigDecimal mouthSale;
    private BigDecimal totalSale;
    private BigDecimal todayScore;
    private BigDecimal mouthScore;
    private BigDecimal totalScore;
    private BigDecimal todayHuabao;
    private BigDecimal mouthHuabao;
    private BigDecimal totalHuabao;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public BigDecimal getTodayRecharge() {
        return todayRecharge;
    }

    public void setTodayRecharge(BigDecimal todayRecharge) {
        this.todayRecharge = todayRecharge;
    }

    public BigDecimal getMouthRecharge() {
        return mouthRecharge;
    }

    public void setMouthRecharge(BigDecimal mouthRecharge) {
        this.mouthRecharge = mouthRecharge;
    }

    public BigDecimal getTotalRecharge() {
        return totalRecharge;
    }

    public void setTotalRecharge(BigDecimal totalRecharge) {
        this.totalRecharge = totalRecharge;
    }

    public BigDecimal getTodayDraws() {
        return todayDraws;
    }

    public void setTodayDraws(BigDecimal todayDraws) {
        this.todayDraws = todayDraws;
    }

    public BigDecimal getMouthDraws() {
        return mouthDraws;
    }

    public void setMouthDraws(BigDecimal mouthDraws) {
        this.mouthDraws = mouthDraws;
    }

    public BigDecimal getTotalDraws() {
        return totalDraws;
    }

    public void setTotalDraws(BigDecimal totalDraws) {
        this.totalDraws = totalDraws;
    }

    public BigDecimal getTodaySale() {
        return todaySale;
    }

    public void setTodaySale(BigDecimal todaySale) {
        this.todaySale = todaySale;
    }

    public BigDecimal getMouthSale() {
        return mouthSale;
    }

    public void setMouthSale(BigDecimal mouthSale) {
        this.mouthSale = mouthSale;
    }

    public BigDecimal getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(BigDecimal totalSale) {
        this.totalSale = totalSale;
    }

    public BigDecimal getTodayScore() {
        return todayScore;
    }

    public void setTodayScore(BigDecimal todayScore) {
        this.todayScore = todayScore;
    }

    public BigDecimal getMouthScore() {
        return mouthScore;
    }

    public void setMouthScore(BigDecimal mouthScore) {
        this.mouthScore = mouthScore;
    }

    public BigDecimal getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    public BigDecimal getTodayHuabao() {
        return todayHuabao;
    }

    public void setTodayHuabao(BigDecimal todayHuabao) {
        this.todayHuabao = todayHuabao;
    }

    public BigDecimal getMouthHuabao() {
        return mouthHuabao;
    }

    public void setMouthHuabao(BigDecimal mouthHuabao) {
        this.mouthHuabao = mouthHuabao;
    }

    public BigDecimal getTotalHuabao() {
        return totalHuabao;
    }

    public void setTotalHuabao(BigDecimal totalHuabao) {
        this.totalHuabao = totalHuabao;
    }
}
