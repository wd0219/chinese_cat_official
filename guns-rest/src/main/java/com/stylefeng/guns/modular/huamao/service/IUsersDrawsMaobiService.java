package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsMaobi;

import java.util.Map;

public interface IUsersDrawsMaobiService extends IService<UsersDrawsMaobi> {
    UsersDrawsMaobi selectSumMoneyByUserId(Map<String, Object> map);
}
