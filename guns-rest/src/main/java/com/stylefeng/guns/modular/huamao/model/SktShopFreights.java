package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 店铺运费表
 * </p>
 *
 * @author ck123123
 * @since 2018-06-08
 */
@TableName("skt_shop_freights")
public class SktShopFreights extends Model<SktShopFreights> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "freightId", type = IdType.AUTO)
    private Integer freightId;
    /**
     * 店铺ID
     */
    private Integer shopId;
    /**
     * 市级ID
     */
    private Integer areaId2;
    /**
     * 运费
     */
    private Integer freight;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getFreightId() {
        return freightId;
    }

    public void setFreightId(Integer freightId) {
        this.freightId = freightId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getAreaId2() {
        return areaId2;
    }

    public void setAreaId2(Integer areaId2) {
        this.areaId2 = areaId2;
    }

    public Integer getFreight() {
        return freight;
    }

    public void setFreight(Integer freight) {
        this.freight = freight;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.freightId;
    }

    @Override
    public String toString() {
        return "SktShopFreights{" +
        "freightId=" + freightId +
        ", shopId=" + shopId +
        ", areaId2=" + areaId2 +
        ", freight=" + freight +
        ", createTime=" + createTime +
        "}";
    }
}
