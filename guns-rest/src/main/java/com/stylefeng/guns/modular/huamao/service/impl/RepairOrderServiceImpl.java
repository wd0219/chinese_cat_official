package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.RepairOrderMapper;
import com.stylefeng.guns.modular.huamao.model.RepairOrder;
import com.stylefeng.guns.modular.huamao.model.RepairOrderList;
import com.stylefeng.guns.modular.huamao.service.IRepairOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-26
 */
@Service
public class RepairOrderServiceImpl extends ServiceImpl<RepairOrderMapper, RepairOrder> implements IRepairOrderService {

	@Autowired
	private RepairOrderMapper rpm;

	// 第三方支付补单记录展示
	@Override
	public List<RepairOrderList> selectRepairOrderAll(RepairOrderList repairOrderList) {
		return rpm.selectRepairOrderAll(repairOrderList);
	}

}
