package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktUserAllOrders;
import com.stylefeng.guns.modular.huamao.service.IAppOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppOrderServiceImpl extends ServiceImpl<SktOrdersMapper,SktOrders> implements IAppOrderService {
    @Autowired
    private SktOrdersMapper sktOrdersMapper;

    @Override
    public List<SktUserAllOrders> allOrder(Integer userId) {
        return null;
    }

    @Override
    public List<Map<String, Object>> selectOrderAndOrderRefundsByUserId(Map<Object, Object> map) {
        return sktOrdersMapper.selectOrderAndOrderRefundsByUserId(map);
    }

    @Override
    public Map<String, Object> selectReceiveOrders(Map<String, Object> map) {
        return sktOrdersMapper.selectReceiveOrders(map);
    }


    @Override
    public SktOrders selectOrderByUserId(Map<String, Object> map) {
        return sktOrdersMapper.selectOrderByUserId(map);
    }

    @Override
    public void updateOrderStatusByOrderNo(Map<String, Object> map) {
        sktOrdersMapper.updateOrderStatusByOrderNo(map);
    }

    @Override
    public SktOrders selectOrderByIsPay(Map<String, Object> map) {
        return sktOrdersMapper.selectOrderByIsPay(map);
    }

    @Override
    public Double selectSumTotalMoney(Map<String, Object> map1) {
        return sktOrdersMapper.selectSumTotalMoney(map1);
    }
    @Override
    public Double selectSumPayScore(Map<String, Object> map1) {
        return sktOrdersMapper.selectSumPayScore(map1);
    }
   @Override
    public List<SktOrders> findOrdersByOrderunique(String orderNo){
        return sktOrdersMapper.findOrdersByOrderunique(orderNo);
   }
    @Override
    public Double selectSumPayKaiyuan(Map<String, Object> map1) {
        return sktOrdersMapper.selectSumPayKaiyuan(map1);
    }
    @Override
    public Double selectSumPayShopping(Map<String, Object> map1) {
        return sktOrdersMapper.selectSumPayShopping(map1);
    }
}
