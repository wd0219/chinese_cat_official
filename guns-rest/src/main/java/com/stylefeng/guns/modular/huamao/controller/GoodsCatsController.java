package com.stylefeng.guns.modular.huamao.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.util.DecodeUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.model.AttributesCustem;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.model.SpecCatsCustem;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IAttributesService;
import com.stylefeng.guns.modular.huamao.service.IGoodsCatsService;
import com.stylefeng.guns.modular.huamao.service.ISpecCatsService;

/**
 * 商品分类控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:31:06
 */
@CrossOrigin()
@Controller
@RequestMapping("/goodsCats")
public class GoodsCatsController extends BaseController {

    private String PREFIX = "/huamao/goodsCats/";

    @Autowired
    private IGoodsCatsService goodsCatsService;
    @Autowired
    private IAttributesService attributesService;
    @Autowired
    private ISpecCatsService specCatsService;
    /**
     * 查询 根据父ID查询  返回json格式
     */
   @RequestMapping("/selectByParentId")
   @ResponseBody
    public JSONArray selectByParentId(@RequestParam(value ="id") Integer id,Model model) {
    	JSONArray jSONArray=new JSONArray();
    	
    	List<GoodsCats> goodscats = goodsCatsService.selectByParentId(id);
    	if(goodscats!=null&&goodscats.size()>0  ){
    		for (GoodsCats goodcat : goodscats) {
        		JSONObject json=new JSONObject();
        		json.put("catId", goodcat.getCatId());
        		json.put("catName", goodcat.getCatName());
        		jSONArray.add(json);	
    		}
    	}
    	
    	return jSONArray;
    }
   
   
   /**
    * 查询  返回json格式 修改商品属性时查询商品分类
    */
  @RequestMapping("/selectAllWhenAttributes")
  @ResponseBody
   public JSONObject selectAllWhenAttributes( Integer id) {
   	JSONArray jSONArray=new JSONArray();
   	List<GoodsCats> goodscats = goodsCatsService.selectList(null);
   	for (GoodsCats goodcat : goodscats) {
   		JSONObject json=new JSONObject();
   		json.put("catId", goodcat.getCatId());
   		json.put("catName", goodcat.getCatName());
   		jSONArray.add(json);	
	}
   	Integer goods1=null;
	Integer goods2=null;
	Integer goods3=null;
	JSONArray jSONArray1=new JSONArray();
	JSONArray jSONArray2=new JSONArray();
	JSONArray jSONArray3=new JSONArray();
//	AttributesCustem attributesCustem=new AttributesCustem();
    AttributesCustem attributesCustem =  attributesService.selectAttributesCustem(id);
    List <Integer> list1 =new ArrayList<Integer>();
  
	Integer goodsCatsId=attributesCustem.getGoodsCatId();
	GoodsCats goodsCats;
	goodsCats = goodsCatsService.selectById(goodsCatsId);
	while(goodsCats.getParentId()!=0){
		Integer catId = goodsCats.getCatId();
		list1.add(catId);
		goodsCats = goodsCatsService.selectById(goodsCats.getParentId());
		if(goodsCats.getParentId()==0){
			list1.add(goodsCats.getCatId());
		}
	}
	JSONObject jsons=new JSONObject();
	if(list1.size()==3){
		goods1=list1.get(2);
	//	JSONArray jSONArray1=new JSONArray();
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		jsons.put("goods1", goods1);
		goods2=list1.get(1);
		
		
		List<GoodsCats> goodscats2 = goodsCatsService.selectByParentId(goods1);
		for (GoodsCats goodcat : goodscats2) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray2.add(json);	
		}
		jsons.put("goods2", goods2);
		
		goods3=list1.get(0);
		
		List<GoodsCats> goodscats3 = goodsCatsService.selectByParentId(goods2);
		for (GoodsCats goodcat : goodscats3) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray3.add(json);	
		}
		jsons.put("goods3", goods3);
		
		
	}
	if(list1.size()==2){
		goods1=list1.get(1);
		jsons.put("goods1", goods1);
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		
		goods2=list1.get(0);
		jsons.put("goods2", goods2);
		List<GoodsCats> goodscats2 = goodsCatsService.selectByParentId(goods1);
		for (GoodsCats goodcat : goodscats2) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray2.add(json);	
		}
		jsons.put("goods3", "");
		
		
	}
	if(list1.size()==1){
		goods1=list1.get(0);
		jsons.put("goods1", goods1);
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		jsons.put("goods2", "");
		jsons.put("goods3", "");
		/*GoodsCats	goodsCats1 = goodsCatsService.selectById(goods1);
		attributesCustem.setGoods1(goods1);
		attributesCustem.setCatName1(goodsCats1.getCatName());*/
		/*goods2=list1.get(1);
		GoodsCats	goodsCats2 = goodsCatsService.selectById(goods2);
		attributesCustem.setGoods2(goods2);
		attributesCustem.setCatName2(goodsCats2.getCatName());
		goods3=list1.get(0);
		GoodsCats	goodsCats3 = goodsCatsService.selectById(goods1);
		attributesCustem.setGoods3(goods3);
		attributesCustem.setCatName3(goodsCats3.getCatName());*/
		
	}
	jsons.put("jSONArray1", jSONArray1);
	jsons.put("jSONArray2", jSONArray2);
	jsons.put("jSONArray3", jSONArray3);
	
	jsons.put("jSONArray", jSONArray);
   	return jsons;
   }
  /**
   * 查询  返回json格式 修改商品规格时查询商品分类
   */
 @RequestMapping("/selectAllWhenSpecCats")
 @ResponseBody
  public JSONObject selectAllWhenSpecCats( Integer id) {
  	JSONArray jSONArray=new JSONArray();
  	List<GoodsCats> goodscats = goodsCatsService.selectList(null);
  	for (GoodsCats goodcat : goodscats) {
  		JSONObject json=new JSONObject();
  		json.put("catId", goodcat.getCatId());
  		json.put("catName", goodcat.getCatName());
  		jSONArray.add(json);	
	}
  	Integer goods1=null;
	Integer goods2=null;
	Integer goods3=null;
	JSONArray jSONArray1=new JSONArray();
	JSONArray jSONArray2=new JSONArray();
	JSONArray jSONArray3=new JSONArray();
//	AttributesCustem attributesCustem=new AttributesCustem();
	SpecCatsCustem specCatsCustem =  specCatsService.selectSpecCatsCustemById(id);
   List <Integer> list1 =new ArrayList<Integer>();
 
	Integer goodsCatsId=specCatsCustem.getGoodsCatId();
	GoodsCats goodsCats;
	goodsCats = goodsCatsService.selectById(goodsCatsId);
	while(goodsCats.getParentId()!=0){
		Integer catId = goodsCats.getCatId();
		list1.add(catId);
		goodsCats = goodsCatsService.selectById(goodsCats.getParentId());
		if(goodsCats.getParentId()==0){
			list1.add(goodsCats.getCatId());
		}
	}
	JSONObject jsons=new JSONObject();
	if(list1.size()==3){
		goods1=list1.get(2);
	//	JSONArray jSONArray1=new JSONArray();
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		jsons.put("goods1", goods1);
		goods2=list1.get(1);
		
		
		List<GoodsCats> goodscats2 = goodsCatsService.selectByParentId(goods1);
		for (GoodsCats goodcat : goodscats2) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray2.add(json);	
		}
		jsons.put("goods2", goods2);
		
		goods3=list1.get(0);
		
		List<GoodsCats> goodscats3 = goodsCatsService.selectByParentId(goods2);
		for (GoodsCats goodcat : goodscats3) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray3.add(json);	
		}
		jsons.put("goods3", goods3);
		
		
	}
	if(list1.size()==2){
		goods1=list1.get(1);
		jsons.put("goods1", goods1);
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		
		goods2=list1.get(0);
		jsons.put("goods2", goods2);
		List<GoodsCats> goodscats2 = goodsCatsService.selectByParentId(goods1);
		for (GoodsCats goodcat : goodscats2) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray2.add(json);	
		}
		jsons.put("goods3", "");
		
		
	}
	if(list1.size()==1){
		goods1=list1.get(0);
		jsons.put("goods1", goods1);
		List<GoodsCats> goodscats1 = goodsCatsService.selectByParentId(0);
		for (GoodsCats goodcat : goodscats1) {
	   		JSONObject json=new JSONObject();
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		jSONArray1.add(json);	
		}
		jsons.put("goods2", "");
		jsons.put("goods3", "");
		/*GoodsCats	goodsCats1 = goodsCatsService.selectById(goods1);
		attributesCustem.setGoods1(goods1);
		attributesCustem.setCatName1(goodsCats1.getCatName());*/
		/*goods2=list1.get(1);
		GoodsCats	goodsCats2 = goodsCatsService.selectById(goods2);
		attributesCustem.setGoods2(goods2);
		attributesCustem.setCatName2(goodsCats2.getCatName());
		goods3=list1.get(0);
		GoodsCats	goodsCats3 = goodsCatsService.selectById(goods1);
		attributesCustem.setGoods3(goods3);
		attributesCustem.setCatName3(goodsCats3.getCatName());*/
		
	}
	jsons.put("jSONArray1", jSONArray1);
	jsons.put("jSONArray2", jSONArray2);
	jsons.put("jSONArray3", jSONArray3);
	
	jsons.put("jSONArray", jSONArray);
  	return jsons;
  }
    /**
     * 跳转到商品分类首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "goodsCats.html"; 
    }

    /**
     * 跳转到添加商品分类
     */
    @RequestMapping("/goodsCats_add")
    public String goodsCatsAdd() {
        return PREFIX + "goodsCats_add.html";
    }
    /**
     * 跳转到添加商品子类
     */
    @RequestMapping("/goodsCatsChild_add/{goodsCatsId}")
    public String goodsCatsChildAdd(@PathVariable Integer goodsCatsId, Model model) {
    	 model.addAttribute("goodsCatsId",goodsCatsId);
     //    LogObjectHolder.me().set(goodsCats);
        return PREFIX + "goodsCats_child_add.html";
    }
    /**
     * 跳转到修改商品分类
     */
    @RequestMapping("/goodsCats_update/{goodsCatsId}")
    public String goodsCatsUpdate(@PathVariable Integer goodsCatsId, Model model) {
        GoodsCats goodsCats = goodsCatsService.selectById(goodsCatsId);
        model.addAttribute("item",goodsCats);
        return PREFIX + "goodsCats_edit.html";
    }

    /**
     * 获取商品分类列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(GoodsCats goodsCats) {
    	 return	goodsCatsService.selectMap(goodsCats);
//      List<Map<String, Object>> list = goodsCatsService.selectMaps(null);
//       return new GoodsCatsWapper(list).warp();
    }
   
    
    /**
     * 获取商品分类列表
     */
    @RequestMapping(value = "/list1")
    @ResponseBody
    public Object list1() {
    //	String a="[{text: \"p1\", nodes: [{ text: \"p1-1\", id: '00001', nodeId: '00001' },{ text: \"p1-2\", id: '00002' }, { text: \"p1-3\", id: '00003' },{ text: \"p1-4\", id: '00004', nodes: [{ text: 'p1-1-1', id: '00005'}]}]}]";
    	
    	 List<GoodsCats> goodsCatslist = goodsCatsService.selectList(null);
    	 JSONArray jSONArray=new JSONArray();
    	 for(GoodsCats goodsCat:goodsCatslist){
    		 JSONObject json=new JSONObject();
    		 json.put("id", goodsCat.getCatId());
    		 json.put("pid", goodsCat.getParentId());
    		 json.put("catName", goodsCat.getCatName());
    		 String isFloorName=null;
    		 String isShowName=null;
    		 if(goodsCat.getIsFloor()==1){
    			 isFloorName="推荐";
    		 }else if(goodsCat.getIsFloor()==0){
    			 isFloorName="不推荐";
    		 }
    		 if(goodsCat.getIsShow()==1){
    			 isShowName="显示";
    		 }else if(goodsCat.getIsShow()==0){
    			 isShowName="不显示";
    		 }
    		 json.put("isFloor", isFloorName);
    		 json.put("isShow", isShowName);
    		 json.put("catSort", goodsCat.getCatSort());
    		 jSONArray.add(json);
    		 
    	 }
    	  return jSONArray;
	}
    /**
     * 新增商品分类
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(GoodsCats goodsCats) {
    	if(goodsCats.getParentId()==null || "".equals(goodsCats.getParentId())){
    		goodsCats.setParentId(0);
    	}    	
    	goodsCats.setCreateTime(new Date());
    	goodsCats.setDataFlag(1);    	
        goodsCatsService.insert(goodsCats);
        return SUCCESS_TIP;
    }
    
 

    /**
     * 删除商品分类
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer goodsCatsId) {
    	List<GoodsCats> goodsCatslist = goodsCatsService.selectByParentId(goodsCatsId);
    	 if(goodsCatslist!=null && goodsCatslist.size()>0){
    		 for(GoodsCats goodsCat:goodsCatslist){
    			 List<GoodsCats> goodsCatslist2 = goodsCatsService.selectByParentId(goodsCat.getCatId()); 
	    		 if(goodsCatslist2!=null && goodsCatslist2.size()>0){
	    			 goodsCatsService.deleteByPId(goodsCat.getCatId());
	    		 }
	    		 goodsCatsService.deleteById(goodsCat.getCatId()); 
    		 }    		 
    	 }
        goodsCatsService.deleteById(goodsCatsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品分类
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(GoodsCats goodsCats) {
        goodsCatsService.updateById(goodsCats);
        return SUCCESS_TIP;
    }

    /**
     * 商品分类详情
     */
    @RequestMapping(value = "/detail/{goodsCatsId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsCatsId") Integer goodsCatsId) {
        return goodsCatsService.selectById(goodsCatsId);
    }
    
    /**
     * 商品分类详情
     */
    @RequestMapping(value = "/selectByCatId/{goodsCatsId}")
    @ResponseBody
    public Object selectParentIdByCatId(@PathVariable("goodsCatsId") Integer goodsCatsId) {
    	JSONArray jSONArray=new JSONArray();
    	GoodsCats goodsCats;
    	goodsCats = goodsCatsService.selectById(goodsCatsId);
    	while (goodsCats.getParentId()!=0) {
    		goodsCats = goodsCatsService.selectById(goodsCats.getCatId());
    		
		}
        return goodsCats;
    }
    
    /**********************首页**************************/
    
    /**
     * 首页商品分类
     */
    @RequestMapping(value = "/selectByGoodsInfo")
    @ResponseBody
    public Result selectByGoodsInfo(){
    	JSONArray jSONArrays=new JSONArray();
    	jSONArrays = selectChilds(0);
    	return Result.OK(jSONArrays);
    }
    
    public JSONArray selectChilds(Integer id){
    	List<GoodsCats> goodscats = goodsCatsService.selectByParentId(id);
    	JSONArray jSONArray=new JSONArray();
    	if(goodscats.size()>0 && goodscats!=null){
    		for (GoodsCats goodcat : goodscats) {
        		JSONObject json=new JSONObject();
        		json.put("catId", goodcat.getCatId());
        		json.put("catName", goodcat.getCatName());
        		json.put("childs", selectChilds(goodcat.getCatId()));
        		jSONArray.add(json);
    		}	
    	}
    	return jSONArray;
    }
    
    /**
     * 查询 商品一级分类
     */
   @RequestMapping("/selectFirstCat")
   @ResponseBody
    public Result selectByParentId() {
    	JSONArray jSONArray=new JSONArray();
    	List<GoodsCats> goodscats = goodsCatsService.selectByParentId(0);
    	if(goodscats.size()>0 && goodscats!=null){
    		for (GoodsCats goodcat : goodscats) {
        		JSONObject json=new JSONObject();
        		json.put("catId", goodcat.getCatId());
        		json.put("catName", goodcat.getCatName());
        		json.put("catImg", goodcat.getCatImg());
        		jSONArray.add(json);	
    		}
    	}
    	return Result.OK(jSONArray);
    }
   
   /**
    * 查询商品分类子类 级品牌
    * @param catId
    * @return
    */
   @RequestMapping(value = "/selectByGoodsInfo/{catId}")
   @ResponseBody
   public Result selectByGoodsInfo(@PathVariable Integer catId){
	   StringBuffer sb = new StringBuffer();
	   JSONArray jSONArrays=new JSONArray();
	   //查询父类id
	  // GoodsCats gGoodsCats = goodsCatsService.selectById(catId);
	  //jSONArrays = selectChilds2(gGoodsCats.getParentId(),sb);
   	   jSONArrays = selectChilds2(catId,sb);
   	   List<Brands> list = new ArrayList<>();
   	   if(sb.length()>0){
	   		String sIds = sb.substring(0, sb.length()-1);
	 		list = goodsCatsService.selectCatName(sIds);
   	   }
   	   JSONObject json=new JSONObject();
	   json.put("brands", list);
	   jSONArrays.add(json);
	   return Result.OK(jSONArrays);
   }
   
   public JSONArray selectChilds2(Integer id,StringBuffer sb){
   	List<GoodsCats> goodscats = goodsCatsService.selectByParentId(id);
   	JSONArray jSONArray=new JSONArray();
   	if(goodscats.size()>0 && goodscats!=null){
		for (GoodsCats goodcat : goodscats) {
	   		JSONObject json=new JSONObject();
	   		sb.append(goodcat.getCatId()).append(",");
	   		json.put("catId", goodcat.getCatId());
	   		json.put("catName", goodcat.getCatName());
	   		json.put("catImg", goodcat.getCatImg());
	   		json.put("childs", selectChilds2(goodcat.getCatId(),sb));
	   		jSONArray.add(json);
		}
	}	
   	return jSONArray;
   }
   
	/**
	 * 新增商品分类
	 */
	@RequestMapping(value = "/addList")
	@ResponseBody
	public Result addList(@RequestParam(value = "goodsCats[]") List<GoodsCats> goodsCats) {
		for (GoodsCats goodsCats1 : goodsCats){
			goodsCats1.setCreateTime(new Date());
			goodsCats1.setDataFlag(1);
		}
		boolean i = goodsCatsService.insertOrUpdateBatch(goodsCats);
		return i==true?Result.OK(null):Result.EEROR("新增商品分类失败！");
	}
	
	/**
     * 删除商品分类
     */
    @RequestMapping(value = "/deleteCats")
    @ResponseBody
    public Result deleteCats(@RequestParam Integer goodsCatsId) {
    	List<GoodsCats> goodsCatslist = goodsCatsService.selectByParentId(goodsCatsId);
    	 if(goodsCatslist!=null && goodsCatslist.size()>0){
    		 for(GoodsCats goodsCat:goodsCatslist){
    			 List<GoodsCats> goodsCatslist2 = goodsCatsService.selectByParentId(goodsCat.getCatId()); 
	    		 if(goodsCatslist2!=null && goodsCatslist2.size()>0){
	    			 goodsCatsService.deleteByPId(goodsCat.getCatId());
	    		 }
	    		 goodsCatsService.deleteById(goodsCat.getCatId()); 
    		 }    		 
    	 }
        boolean deleteById = goodsCatsService.deleteById(goodsCatsId);
        return deleteById==true?Result.OK(null):Result.EEROR("删除商品分类失败！");
    }

	@RequestMapping(value = "/getGoodscatsByGoodsId")
	@ResponseBody
    public  Result getGoodscatsByGoodsId(Integer goodsId){
		JSONObject json = goodsCatsService.getGoodscatsByGoodsId(goodsId);
		return  Result.OK(json);
	}

	@RequestMapping(value = "/getGoodscatsByGoodsCatId")
	@ResponseBody
	public  Result getGoodscatsByGoodsCatId(Integer goodsCatId){
		JSONObject json = goodsCatsService.getGoodscatsByGoodsCatId(goodsCatId);
		return  Result.OK(json);
	}
	
	/**
	 * 根据分类查询商品
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value = "/getGoodscatsSelGoods")
	@ResponseBody
	public  Result getGoodscatsSelGoods(@RequestParam(value="goodsCatId",required = false)Integer goodsCatId,
			@RequestParam(value="shopPrice1",required = false ) Integer shopPrice1,
			@RequestParam(value="shopPrice2",required = false ) Integer shopPrice2,
    		@RequestParam(value="saleNum",required = false ) Integer saleNum,
    		@RequestParam(value="shopPrice",required = false ) Integer shopPrice,
    		@RequestParam(value="visitNum",required = false ) Integer visitNum,
    		@RequestParam(value="appraiseNum",required = false ) Integer appraiseNum,
    		@RequestParam(value="ssaleTime",required = false ) Integer ssaleTime,
    		@RequestParam(value="isNew",required = false ) Integer isNew,
    		@RequestParam(value="goodsStock",required = false ) Integer goodsStock,
    		@RequestParam(value="brandIds",required = false ) String brandIds,
    		@RequestParam(value="goodsName",required = false ) String goodsName,
    		@RequestParam(value="isSelf",required = false ) Integer isSelf,
			@RequestParam(value="pageNum",defaultValue="1", required=false)Integer pageNum) {
		StringBuffer sb = new StringBuffer();
		if(ToolUtil.isNotEmpty(goodsCatId)){
			selectChilds3(goodsCatId,sb);
			sb.append(goodsCatId);
		}
		if(ToolUtil.isNotEmpty(goodsName)){
			try {
				goodsName = URLDecoder.decode(goodsName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		PageHelper.startPage(pageNum, 12);
		List<Map<String,Object>> list = goodsCatsService.getGoodscatsSelGoods(sb,shopPrice1,shopPrice2,saleNum,
				shopPrice,visitNum,appraiseNum,ssaleTime,isNew,goodsStock,brandIds,goodsName,isSelf); 
		PageInfo pag = new PageInfo<>(list);
		return Result.OK(list,pag.getPages());
	}
	
	public void selectChilds3(Integer id,StringBuffer sb){
	    	List<GoodsCats> goodscats = goodsCatsService.selectByParentId(id);
	    	if(goodscats.size()>0 && goodscats!=null){
	    		for (GoodsCats goodcat : goodscats) {
	    			sb.append(goodcat.getCatId()).append(",");
	    			selectChilds3(goodcat.getCatId(),sb);
	    		}	
	    	}
	}
	
	/**
     * 根据商店分类查询
     */
    @RequestMapping(value = "/selectShopCatsGoods")
    @ResponseBody
    public Result selectShopCatsGoods(@RequestParam(value="shopId", required = false) Integer shopId,
    		@RequestParam(value="shopCatId", required = false) Integer shopCatId,
    		@RequestParam(value="shopPrice1",required = false ) Integer shopPrice1,
			@RequestParam(value="shopPrice2",required = false ) Integer shopPrice2,
    		@RequestParam(value="saleNum",required = false ) Integer saleNum,
    		@RequestParam(value="shopPrice",required = false ) Integer shopPrice,
    		@RequestParam(value="visitNum",required = false ) Integer visitNum,
    		@RequestParam(value="appraiseNum",required = false ) Integer appraiseNum,
    		@RequestParam(value="ssaleTime",required = false ) Integer ssaleTime,
    		@RequestParam(value="isNew",required = false ) Integer isNew,
    		@RequestParam(value="goodsStock",required = false ) Integer goodsStock,
    		@RequestParam(value="goodsName",required = false,defaultValue="" ) String goodsName,
    		@RequestParam(value="pageNum" ,defaultValue="1", required=false)Integer pageNum
    		) {
    	PageHelper.startPage(pageNum,12);
		String goodsNa = "";
		try {
			if(!"".equals(goodsName)){
				goodsNa = URLDecoder.decode(goodsName,"UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	List<Map<String,Object>> list = goodsCatsService.selectShopCatsGoods(shopId,shopCatId,shopPrice1,
    			shopPrice2,saleNum,shopPrice,visitNum,appraiseNum,ssaleTime,isNew,goodsStock,goodsNa);
    	PageInfo pageInfo = new PageInfo<>(list);
        return Result.OK(list,pageInfo.getPages());
    }

	
    /**
     * 查询商铺评分
     */
    @RequestMapping(value = "/selectShopsCores")
	@ResponseBody
    public  Result selectShopsCores(Integer shopId){
		Map<String,Object> map= goodsCatsService.selectShopsCores(shopId);
		return  Result.OK(map);
	}

	/**
	 * 商城首页 - 楼层显示
	 */
	@RequestMapping(value = "/showFloor")
	@ResponseBody
	public Result showFloor(){
		List<Map<String,Object>> list = goodsCatsService.showFloor();
		return Result.OK(list);
	}
}
