package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogCashMapper;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogCashList;
import com.stylefeng.guns.modular.huamao.service.ILogCashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 现金流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogCashServiceImpl extends ServiceImpl<LogCashMapper, LogCash> implements ILogCashService {

	@Autowired
	private LogCashMapper lcm;

	// 现金流水表返回
	@Override
	public List<LogCashList> selectLogCashAll(LogCashList logCashList) {
		return lcm.selectLogCashAll(logCashList);
	}

    @Override
    public List<Map<String, Object>> selectLogCash(LogCashList logCash) {
		List<Map<String,Object>> list = lcm.selectLogCash(logCash);
        return list;
    }
}
