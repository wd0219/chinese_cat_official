package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktBanksMapper;
import com.stylefeng.guns.modular.huamao.model.SktBanks;
import com.stylefeng.guns.modular.huamao.service.IAppBanksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppBanksServiceImpl extends ServiceImpl<SktBanksMapper,SktBanks> implements IAppBanksService {
    @Autowired
    private SktBanksMapper sktBanksMapper;

    @Override
    public List<Map<String, Object>> selectAllBanks() {
        return sktBanksMapper.selectAllBanks();
    }
}
