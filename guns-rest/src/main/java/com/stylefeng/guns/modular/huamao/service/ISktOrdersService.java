package com.stylefeng.guns.modular.huamao.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktOrdersList;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 线上商城订单表 服务类
 * </p>
 *
 * @author caody123
 * @since 2018-04-14
 */
public interface ISktOrdersService extends IService<SktOrders> {

	/**
	 * 查询订单列表
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderShops(SktOrdersList sktOrdersList);
	/**
	 * 查询订单积分信息
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderScore(Map<String, Object> param);
	/**
	 * 查询订单商品信息
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderGoods(Integer sktOrdersId);
	/**
	 * 查询订单商品参数信息
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderGoodsParameter(Map<String, Object> param);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	
	/**
	 * 查询订单总数
	 * @param 
	 * @return
	 */
	public int selectOrderTotal();

	/**
	 * 首页显示
	 * @param sktOrdersList
	 * @return
	 */
    List<Map<String,Object>> selectUserOrders(SktOrdersList sktOrdersList);
    
    /**
     * 购物车结算时生成的订单
     * @param sktOrders
     * @return
     */
    public int produceOrders(SktOrders sktOrders);
    
    /**
     * 通过用户id查询地址，默认与所有的地址
     * @param map 
     * 当isDefault为1时是默认地址，为0时是备用地址
     * @return
     */
    public List<Map<String,Object>> selectAddressByUserid(Map<String,Object> map);
    
    /**
	 * 重新设置默认地址
	 * @param map
	 * @return
	 */
    public Boolean changeIsDefault(Map<String, Object> map);
    
    /**
	 * 添加收货地址
	 * @param map
	 * @return
	 */
    public Boolean addAddress(UsersAddress usersAddress);
    /**
     * 查询代发货 待付款 待评价
     * @param sktOrdersList
     * @return
     */
	public Map<String, Object> selectUserOrdersInfo(SktOrdersList sktOrdersList);

	/**
	 * 提交订单
	 * @param sktOrdersIds
	 * @return
	 */
	public Boolean submitOrder(List<Map<String, Object>> listMap,SktOrders sktOrders);

	/**
	 * 添加快递单号
	 * @param sktOrders

	 * @return
	 */
	public JSONObject addExpressNo(SktOrders sktOrders);

	/**
	 * 定时任务清除过期订单
	 * @return
	 */
	public Integer orderBatch();

	List<Map<String,Object>> appFindAllOrders(Integer shopId,String orderStatus);

	Integer appUpdateStatus(Map<String, Object> map);

	Map<String,Object> appFindOrdersDetailed(String orderNo);

	List<Map<String,Object>> appFindToBeEvaluatedOrders(Map<String, Object> map);

    List<Map<String,Object>> appFindrefundOrder(int userId, Integer refundStatus);

	/**
	 * 订单过期时更新订单状态
	 * @param map
	 * @return
	 */
	public void updateOrderStatus(Map map);
}
