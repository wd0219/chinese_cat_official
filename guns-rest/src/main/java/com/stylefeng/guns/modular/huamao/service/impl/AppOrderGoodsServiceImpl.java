package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrderGoodsMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;
import com.stylefeng.guns.modular.huamao.service.IAppOrderGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppOrderGoodsServiceImpl extends ServiceImpl<SktOrderGoodsMapper,SktOrderGoods> implements IAppOrderGoodsService {
    @Autowired
    private SktOrderGoodsMapper sktOrderGoodsMapper;


    @Override
    public List<Map<String, Object>> selectOrderGoodsByOrderId(int orderId) {
        return sktOrderGoodsMapper.selectOrderGoodsByOrderId(orderId);
    }

    @Override
    public Map<String, Object> selectOrderGoodsByGoodsId(Map<String, Object> map1) {
        return sktOrderGoodsMapper.selectOrderGoodsByGoodsId(map1);
    }
}
