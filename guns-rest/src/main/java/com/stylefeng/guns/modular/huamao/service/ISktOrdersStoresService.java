package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStoresDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 线下商家订单表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-24
 */
public interface ISktOrdersStoresService extends IService<SktOrdersStores> {
	/**
	 * 页面显示列表
	 * @param sktOrdersStoresDto
	 * @return
	 */
	public List<SktOrdersStoresDto> showOrdersStoresInfo(SktOrdersStoresDto sktOrdersStoresDto);
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int selectOrdersStoresTotal();

	/**
	 * 首页显示
	 * @param sktOrdersStoresDto
	 * @return
	 */
    List<Map<String,Object>> selectUserOrdersStores(SktOrdersStoresDto sktOrdersStoresDto);
  
	public List<Map<String, Object>> selectShopOrdersStoresUp(SktOrdersStoresDto sktOrdersStoresDto);
	
	public List<Map<String, Object>> selectShopOrdersStoresCh(SktOrdersStoresDto sktOrdersStoresDto);
	
	public List<Map<String, Object>> selectShopOrdersStoresOk(SktOrdersStoresDto sktOrdersStoresDto);

	List<Map<String,Object>> appSelectAllOrders(Map<String,Object> map);

    Integer appOrderStoreCancel(String orderNo);

	Map<String,Object> appOrderStoreConfirm(String orderNo);

    Map<String,Object> apporderStore(Integer shopId, BigDecimal payMoney, Integer scoreRatio,Integer userId);

}
