package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * 商品扩展实体类
 */
public class GoodsCustm extends Goods{
	private  Integer province;
	private  Integer citys;
	private  Integer county;
	private  Integer goods1;
	private  Integer goods2;
	private  Integer goods3;
	private  String shopName;
	private  String shopLog;
	private  Integer Favorites;//0：未收藏 大于0则已收藏
	private BigDecimal shopScores;

	public String getShopLog() {
		return shopLog;
	}

	public void setShopLog(String shopLog) {
		this.shopLog = shopLog;
	}

	public BigDecimal getShopScores() {
		return shopScores;
	}

	public void setShopScores(BigDecimal shopScores) {
		this.shopScores = shopScores;
	}

	public Integer getFavorites() {
		return Favorites;
	}

	public void setFavorites(Integer favorites) {
		Favorites = favorites;
	}

	private Integer style;
	public Integer getStyle() {
		return style;
	}
	public void setStyle(Integer style) {
		this.style = style;
	}
//	private String to_left;
//	private String to_right;
	
	
//	private String[] to_left;
//	private String[] to_right;
	private List<Integer> to_left;
	private List<Integer> to_right;
	public List<Integer> getTo_left() {
		return to_left;
	}
	public void setTo_left(List<Integer> to_left) {
		this.to_left = to_left;
	}
	public List<Integer> getTo_right() {
		return to_right;
	}
	public void setTo_right(List<Integer> to_right) {
		this.to_right = to_right;
	}


//	public String[] getTo_left() {
//		return to_left;
//	}
//	public void setTo_left(String[] to_left) {
//		this.to_left = to_left;
//	}
//	public String[] getTo_right() {
//		return to_right;
//	}
//	public void setTo_right(String[] to_right) {
//		this.to_right = to_right;
//	}


	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	public Integer getCitys() {
		return citys;
	}
	public void setCitys(Integer citys) {
		this.citys = citys;
	}
	public Integer getCounty() {
		return county;
	}
	public void setCounty(Integer county) {
		this.county = county;
	}
	public Integer getGoods1() {
		return goods1;
	}
	public void setGoods1(Integer goods1) {
		this.goods1 = goods1;
	}
	public Integer getGoods2() {
		return goods2;
	}
	public void setGoods2(Integer goods2) {
		this.goods2 = goods2;
	}
	public Integer getGoods3() {
		return goods3;
	}
	public void setGoods3(Integer goods3) {
		this.goods3 = goods3;
	}

}
