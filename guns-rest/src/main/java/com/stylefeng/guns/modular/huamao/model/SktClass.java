package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 商学院表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
@TableName("skt_class")
public class SktClass extends Model<SktClass> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "classId", type = IdType.AUTO)
    private Integer classId;
    /**
     * 视频课程名称
     */
    private String className;
    /**
     * 视频图片
     */
    private String classImg;
    /**
     * 视频播放路径
     */
    private String classUrl;
    /**
     * 视频标题
     */
    private String classTitle;
    /**
     * 视频点击率(预留字段)
     */
    private Integer clickRate;
    /**
     * 视频点赞数(预留字段)
     */
    private Integer pointNumber;
    /**
     * 视频评论id
     */
    private Integer commentId;
    /**
     * 视频作者
     */
    private String creatWriter;
    /**
     * 视频介绍
     */
    private String introduce;
    /**
     * 视频备注(不通过写出原因)
     */
    private String remark;
    /**
     * 课程状态:0未通过,1通过,2不通过
     */
    private Integer istatus;
    /**
     * 视频类型
     */
    private Integer classType;
    /**
     * 上传时间
     */
    private Date creatTime;
    /**
     * 审核时间
     */
    private Date checkTime;

    private String videoId;

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassImg() {
        return classImg;
    }

    public void setClassImg(String classImg) {
        this.classImg = classImg;
    }

    public String getClassUrl() {
        return classUrl;
    }

    public void setClassUrl(String classUrl) {
        this.classUrl = classUrl;
    }

    public String getClassTitle() {
        return classTitle;
    }

    public void setClassTitle(String classTitle) {
        this.classTitle = classTitle;
    }

    public Integer getClickRate() {
        return clickRate;
    }

    public void setClickRate(Integer clickRate) {
        this.clickRate = clickRate;
    }

    public Integer getPointNumber() {
        return pointNumber;
    }

    public void setPointNumber(Integer pointNumber) {
        this.pointNumber = pointNumber;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getCreatWriter() {
        return creatWriter;
    }

    public void setCreatWriter(String creatWriter) {
        this.creatWriter = creatWriter;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIstatus() {
        return istatus;
    }

    public void setIstatus(Integer istatus) {
        this.istatus = istatus;
    }

    public Integer getClassType() {
        return classType;
    }

    public void setClassType(Integer classType) {
        this.classType = classType;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.classId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
