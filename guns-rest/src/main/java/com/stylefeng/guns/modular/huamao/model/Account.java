package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 会员账户表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-11
 */
@TableName("skt_account")
public class Account extends Model<Account> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "accountId", type = IdType.AUTO)
    private Integer accountId;
    /**
     * 会员ID
     */
    private Integer userId;
    /**
     * 商家ID(shops表)
     */
    private Integer shopId;
    /**
     * 积分
     */
    private BigDecimal score;
    /**
     * 待发积分
     */
    private BigDecimal freezeScore;
    /**
     * 累计获得积分
     */
    private BigDecimal totalScore;
    /**
     * 开元宝
     */
    private BigDecimal kaiyuan;
    /**
     * 累计获得开元宝
     */
    private BigDecimal totalKaiyuan;
    /**
     * 现金
     */
    private BigDecimal cash;
    /**
     * 待发现金
     */
    private BigDecimal freezeCash;
    /**
     * 累计获得现金
     */
    private BigDecimal totalCash;
    /**
     * 线上消费额(商城消费金额)
     */
    private BigDecimal onlineExpenditure;
    /**
     * 线下消费额(商家补单 消费者主动下单 面对面付)
     */
    private BigDecimal offlineExpenditure;
    /**
     * 用户发展的主管或经理数量
     */
    private Integer inviteNum;
    /**
     * 特别奖励发展1个商家奖励的积分
     */
    private BigDecimal one;
    /**
     * 特别奖励发展2个商家奖励的积分
     */
    private BigDecimal two;
    /**
     * 特别奖励发展3个商家奖励的积分
     */
    private BigDecimal three;
    /**
     * 特别奖励发展4个商家奖励的积分
     */
    private BigDecimal four;
    /**
     * 特别奖励发展5个及其以上商家奖励的积分
     */
    private BigDecimal five;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 购物券总金额
     */
    private BigDecimal  shoppingMoney;

    public BigDecimal getShoppingMoney() {
        return shoppingMoney;
    }

    public void setShoppingMoney(BigDecimal shoppingMoney) {
        this.shoppingMoney = shoppingMoney;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public BigDecimal getFreezeScore() {
        return freezeScore;
    }

    public void setFreezeScore(BigDecimal freezeScore) {
        this.freezeScore = freezeScore;
    }

    public BigDecimal getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getTotalKaiyuan() {
        return totalKaiyuan;
    }

    public void setTotalKaiyuan(BigDecimal totalKaiyuan) {
        this.totalKaiyuan = totalKaiyuan;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getFreezeCash() {
        return freezeCash;
    }

    public void setFreezeCash(BigDecimal freezeCash) {
        this.freezeCash = freezeCash;
    }

    public BigDecimal getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(BigDecimal totalCash) {
        this.totalCash = totalCash;
    }

    public BigDecimal getOnlineExpenditure() {
        return onlineExpenditure;
    }

    public void setOnlineExpenditure(BigDecimal onlineExpenditure) {
        this.onlineExpenditure = onlineExpenditure;
    }

    public BigDecimal getOfflineExpenditure() {
        return offlineExpenditure;
    }

    public void setOfflineExpenditure(BigDecimal offlineExpenditure) {
        this.offlineExpenditure = offlineExpenditure;
    }

    public Integer getInviteNum() {
        return inviteNum;
    }

    public void setInviteNum(Integer inviteNum) {
        this.inviteNum = inviteNum;
    }

    public BigDecimal getOne() {
        return one;
    }

    public void setOne(BigDecimal one) {
        this.one = one;
    }

    public BigDecimal getTwo() {
        return two;
    }

    public void setTwo(BigDecimal two) {
        this.two = two;
    }

    public BigDecimal getThree() {
        return three;
    }

    public void setThree(BigDecimal three) {
        this.three = three;
    }

    public BigDecimal getFour() {
        return four;
    }

    public void setFour(BigDecimal four) {
        this.four = four;
    }

    public BigDecimal getFive() {
        return five;
    }

    public void setFive(BigDecimal five) {
        this.five = five;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.accountId;
    }

    @Override
    public String toString() {
        return "Account{" +
        "accountId=" + accountId +
        ", userId=" + userId +
        ", shopId=" + shopId +
        ", score=" + score +
        ", freezeScore=" + freezeScore +
        ", totalScore=" + totalScore +
        ", kaiyuan=" + kaiyuan +
        ", totalKaiyuan=" + totalKaiyuan +
        ", cash=" + cash +
        ", freezeCash=" + freezeCash +
        ", totalCash=" + totalCash +
        ", onlineExpenditure=" + onlineExpenditure +
        ", offlineExpenditure=" + offlineExpenditure +
        ", inviteNum=" + inviteNum +
        ", one=" + one +
        ", two=" + two +
        ", three=" + three +
        ", four=" + four +
        ", five=" + five +
        ", createTime=" + createTime +
        "}";
    }
}
