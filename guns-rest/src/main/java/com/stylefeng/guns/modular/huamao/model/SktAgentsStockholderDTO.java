package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class SktAgentsStockholderDTO {
	
	/**
	 * id
	 */
	private Integer shId;
	/**
	 * 代理公司id
	 */
	private Integer agentId;
	/**
	 * 用户id
	 */
	private Integer userId; 
	/**
	 * 登录名
	 */
	private String loginName;
	/**
	 * 用户姓名
	 */
	private String trueName;
	/**
	 * 用户电话
	 */
	private String userPhone;
	/**
	 * 管理股东类型
	 */
	private Integer type;
	/**
	 * 代理公司名称
	 */
	private String agentName;
	/**
	 * 代理级别
	 */
	private Integer alevel;
	/**
	 * 拥有的股票分数
	 */
	private Integer stockNum;
	/**
	 * storeNum
	 */
	private Integer storeNum;
	/**
	 * 累计分红
	 */
	private BigDecimal totalDividend;
	/**
	 * 上次分红时间
	 */
	private Date dividendTime; 
	/**
	 * 上次分红
	 */
	private BigDecimal lastDividend;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 省id
	 */
	private Integer province ; 
	/**
	 * 市id
	 */
	private Integer citys; 
	/**
	 * 县id
	 */
	private Integer county;
	
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public Integer getAlevel() {
		return alevel;
	}
	public void setAlevel(Integer alevel) {
		this.alevel = alevel;
	}
	public Integer getStockNum() {
		return stockNum;
	}
	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}
	public Integer getStoreNum() {
		return storeNum;
	}
	public void setStoreNum(Integer storeNum) {
		this.storeNum = storeNum;
	}
	public BigDecimal getTotalDividend() {
		return totalDividend;
	}
	public void setTotalDividend(BigDecimal totalDividend) {
		this.totalDividend = totalDividend;
	}
	public Date getDividendTime() {
		return dividendTime;
	}
	public void setDividendTime(Date dividendTime) {
		this.dividendTime = dividendTime;
	}
	public BigDecimal getLastDividend() {
		return lastDividend;
	}
	public void setLastDividend(BigDecimal lastDividend) {
		this.lastDividend = lastDividend;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getShId() {
		return shId;
	}
	public void setShId(Integer shId) {
		this.shId = shId;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	public Integer getCitys() {
		return citys;
	}
	public void setCitys(Integer citys) {
		this.citys = citys;
	}
	public Integer getCounty() {
		return county;
	}
	public void setCounty(Integer county) {
		this.county = county;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	
}
