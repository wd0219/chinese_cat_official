package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单商品表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-11
 */
public interface ISktOrderGoodsService extends IService<SktOrderGoods> {

    List<Map<String,Object>> appFindGoodById(Integer orderId);

    List<Map<String,Object>> appFindOrderMesById(Integer orderId);

}
