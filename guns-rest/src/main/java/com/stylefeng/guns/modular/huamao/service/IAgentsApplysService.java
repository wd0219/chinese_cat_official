package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AgentsApplys;
import com.stylefeng.guns.modular.huamao.model.AgentsApplysDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代理公司股东申请表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-17
 */
public interface IAgentsApplysService extends IService<AgentsApplys> {
	/**
	 * 查询列表
	 * @param agentsApplysDTO
	 * @return
	 */
	List<AgentsApplysDTO> showDaiLiShenQingInfo(AgentsApplysDTO agentsApplysDTO);
	/**
	 * 股东申请
	 * @param agentsApplys
	 * @return
	 */
	public Integer insertAgentsApplys(AgentsApplysDTO agentsApplys);
	/**
	 * 查询线上商城显示信息
	 * @param userId
	 * @return
	 */
	Map<String, Object> selectAgentsApplysInfo(Integer userId);
}
