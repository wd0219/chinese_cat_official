package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 代理公司表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
@TableName("skt_agents")
public class SktAgents extends Model<SktAgents> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "agentId", type = IdType.AUTO)
    private Integer agentId;
    /**
     * 代理名称
     */
    private String agentName;
    /**
     * 代理级别：10为省级 ，11为市级 ，12为区/县
     */
    private Integer alevel;
    /**
     * 省id
     */
    private Integer provinceId;
    /**
     * 市id
     */
    private Integer cityId;
    /**
     * 区县id
     */
    private Integer areaId;
    /**
     * 每股缴纳的代理费
     */
    private Integer agencyFee;
    /**
     * 当前公司股东人数
     */
    private Integer stockNum;
    /**
     * 代理公司股东总人数
     */
    private Integer totalNum;
    /**
     * 当前辖区商家数量
     */
    private Integer storeNum;
    /**
     * 完成目标辖区商家数量
     */
    private Integer totalStoreNum;
    /**
     * 代理公司总股份数量
     */
    private Integer totalStockNum;
    /**
     * 普通股东剩余股份数量
     */
    private Integer LPsurplusStockNum;
    /**
     * 管理股东剩余股份数量
     */
    private Integer GPsurplusStockNum;
    /**
     * 成为股东的分红系数
     */
    private BigDecimal oneRatio;
    /**
     * 完成70%(落地)的分红系数
     */
    private BigDecimal twoRatio;
    /**
     * 全部完成的分红系数
     */
    private BigDecimal threeRatio;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 安全码
     */
    private Integer loginSecret;
    /**
     * 密码
     */
    private String loginPwd;
    /**
     * 支付密码
     */
    private String payPwd;
    /**
     * 账号状态：0停用 1启用
     */
    private Integer agentStatus;
    /**
     * 删除标志：-1删除 1有效
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public Integer getAlevel() {
        return alevel;
    }

    public void setAlevel(Integer alevel) {
        this.alevel = alevel;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getAgencyFee() {
        return agencyFee;
    }

    public void setAgencyFee(Integer agencyFee) {
        this.agencyFee = agencyFee;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Integer getStoreNum() {
        return storeNum;
    }

    public void setStoreNum(Integer storeNum) {
        this.storeNum = storeNum;
    }

    public Integer getTotalStoreNum() {
        return totalStoreNum;
    }

    public void setTotalStoreNum(Integer totalStoreNum) {
        this.totalStoreNum = totalStoreNum;
    }

    public Integer getTotalStockNum() {
        return totalStockNum;
    }

    public void setTotalStockNum(Integer totalStockNum) {
        this.totalStockNum = totalStockNum;
    }

    public Integer getLPsurplusStockNum() {
        return LPsurplusStockNum;
    }

    public void setLPsurplusStockNum(Integer LPsurplusStockNum) {
        this.LPsurplusStockNum = LPsurplusStockNum;
    }

    public Integer getGPsurplusStockNum() {
        return GPsurplusStockNum;
    }

    public void setGPsurplusStockNum(Integer GPsurplusStockNum) {
        this.GPsurplusStockNum = GPsurplusStockNum;
    }

    public BigDecimal getOneRatio() {
        return oneRatio;
    }

    public void setOneRatio(BigDecimal oneRatio) {
        this.oneRatio = oneRatio;
    }

    public BigDecimal getTwoRatio() {
        return twoRatio;
    }

    public void setTwoRatio(BigDecimal twoRatio) {
        this.twoRatio = twoRatio;
    }

    public BigDecimal getThreeRatio() {
        return threeRatio;
    }

    public void setThreeRatio(BigDecimal threeRatio) {
        this.threeRatio = threeRatio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public Integer getLoginSecret() {
        return loginSecret;
    }

    public void setLoginSecret(Integer loginSecret) {
        this.loginSecret = loginSecret;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd;
    }

    public Integer getAgentStatus() {
        return agentStatus;
    }

    public void setAgentStatus(Integer agentStatus) {
        this.agentStatus = agentStatus;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.agentId;
    }

    @Override
    public String toString() {
        return "SktAgents{" +
        "agentId=" + agentId +
        ", agentName=" + agentName +
        ", alevel=" + alevel +
        ", provinceId=" + provinceId +
        ", cityId=" + cityId +
        ", areaId=" + areaId +
        ", agencyFee=" + agencyFee +
        ", stockNum=" + stockNum +
        ", totalNum=" + totalNum +
        ", storeNum=" + storeNum +
        ", totalStoreNum=" + totalStoreNum +
        ", totalStockNum=" + totalStockNum +
        ", LPsurplusStockNum=" + LPsurplusStockNum +
        ", GPsurplusStockNum=" + GPsurplusStockNum +
        ", oneRatio=" + oneRatio +
        ", twoRatio=" + twoRatio +
        ", threeRatio=" + threeRatio +
        ", phone=" + phone +
        ", loginName=" + loginName +
        ", loginSecret=" + loginSecret +
        ", loginPwd=" + loginPwd +
        ", payPwd=" + payPwd +
        ", agentStatus=" + agentStatus +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
