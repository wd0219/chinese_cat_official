package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktLogOperatesMapper;
import com.stylefeng.guns.modular.huamao.model.SktLogOperates;
import com.stylefeng.guns.modular.huamao.model.SktLogOperatesDto;
import com.stylefeng.guns.modular.huamao.service.ISktLogOperatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 操作记录表 服务实现类
 * </p>
 *
 * @author slt123
 * @since 2018-04-27
 */
@Service
public class SktLogOperatesServiceImpl extends ServiceImpl<SktLogOperatesMapper, SktLogOperates> implements ISktLogOperatesService {
	
	@Autowired
	private SktLogOperatesMapper sktLogOperatesMapper;
	@Override
	public List<SktLogOperatesDto> selectListDate(SktLogOperatesDto sktLogOperatesDto) {
		List<SktLogOperatesDto> list = sktLogOperatesMapper.selectListDate(sktLogOperatesDto);
		return list;
	}

}
