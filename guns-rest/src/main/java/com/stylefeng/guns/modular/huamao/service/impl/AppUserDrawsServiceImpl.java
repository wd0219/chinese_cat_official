package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktSysConfigsMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersDrawsMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
@Service
public class AppUserDrawsServiceImpl extends ServiceImpl<UsersDrawsMapper,UsersDraws> implements IAppUserDrwasService {
    @Autowired
    UsersDrawsMapper udm;
    @Autowired
    private UsersMapper usersMapper;
    @Autowired
    private IAppSktMaobiChangeService iAppSktMaobiChangeService;
    @Autowired
    private SktSysConfigsMapper sktSysConfigsMapper;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IAccountShopService accountShopService;
    @Autowired
    private IMessagesService messagesService;
    @Autowired
    private IUsersService usersService;
    @Autowired
    ISktHuamaobtLogService sktHuamaobtLogService;
    @Autowired
    private ISktDateService iSktDateService;
    @Autowired
    private IAppLogKaiyuanService iAppLogKaiyuanService;
    @Autowired
    private IUsersDrawsMaobiService iUsersDrawsMaobiService;
    @Autowired
    private IAppLogCashService iAppLogCashService;
    @Autowired
    private ILogKaiyuanTurnoverService iLogKaiyuanTurnoverService;
    @Autowired
    ISktExchangeUserService exchangeUserService;
    @Autowired
    private IAppUserDrwasService iAppUserDrwasService;
    @Autowired
    private CacheUtil cacheUtil;




    /**
     * 赎回申请
     * @param userId  用户ID
     * @param type  提现的类型：1开元宝提现 2现金提现 3开元宝营业额提现
     * @param money  提现金额
     * @param expenses  手续费+税率（平台扣除的费用）fee
     * @param expensesrate  提现手续费比例 %   ratio
     * @param taxRatio  提现税率比例%  （服务费用） 10
     * @param payPwd   支付密码
     * @return
     */
    @Override
    public AppResult applyRedeem (Integer userId,BigDecimal moneyRate,Integer type, BigDecimal money, BigDecimal expenses, BigDecimal expensesrate, BigDecimal taxRatio, BigDecimal ratio, String payPwd) {
        try {
//          if(type==1) {
//                /*SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                Date oldDate = dateFormat.parse("2018-8-6 11:00:00");
//                Date dates = new Date();
//                if (dates.getTime() > oldDate.getTime()) {
//                    if (user.getIsOperation() != 1) {
//                        return AppResult.ERROR("赎回功能已关闭", -1);
//                    }
//                }*/
//                return AppResult.ERROR("华宝赎回功能已关闭", -1);
//          }
            if (true) {
                return AppResult.ERROR("猫币赎回功能关闭",-1);
            }
            SimpleDateFormat setTime = new SimpleDateFormat("HH:mm:ss");
            String parse = setTime.format(new Date());
            Date parse1 = setTime.parse(parse);//当前时间时分秒
            Date parse2 = setTime.parse("11:00:00");//设定11点时间
            if (parse2.getTime()>parse1.getTime()){//如果设定时分秒大于当前时分秒
                return AppResult.ERROR("猫币赎回在11点开启",-1);
            }
        JSONObject json = new JSONObject();
        Users user = usersMapper.selectById(userId);
        if (user.getUserType()==0){
            return AppResult.ERROR("普通用户没有申请赎回权限,请升级用户",-1);
        }
        if (user.getIsWithdrawCash()!=1){
            return AppResult.ERROR("您没有申请赎回权限",-1);
        }
        if(type==1) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date oldDate = dateFormat.parse("2018-8-6 11:00:00");
            Date dates = new Date();
            if (dates.getTime() > oldDate.getTime()) {
                if (user.getIsOperation() != 1) {
                    return AppResult.ERROR("该用户不允许华宝赎回", -1);
                }
            }
        }
        //判定今天是否为工作日
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        String format1 = format.format(new Date());
        int date = Integer.parseInt(format1);

        SktDate sktDate=iSktDateService.selectDateByDate(date);
        if (sktDate.getStatus()!=0){
            if (sktDate.getStatus()==1){
                return AppResult.ERROR("今日为休息日不可进行提现操作",-1);
            }
            SktSysConfigs sktSysConfigs = sktSysConfigsMapper.selectSysConfigsByFieldCode("isHolidaysCash");
            if (Integer.parseInt(sktSysConfigs.getFieldValue())!=1){
                return AppResult.ERROR("今日不可进行提现操作",-1);
            }
        }

        //SktSysConfigs sktSysConfigs = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountDay");
        //Integer fieldValue = Integer.parseInt(sktSysConfigs.getFieldValue());
        BigDecimal moneyValue=money;
        JSONObject jsonObject = usersService.checkPayPassWorld(userId, payPwd);
        if ("00".equals(jsonObject.get("code"))){
            return AppResult.ERROR("交易密码不正确",-1);
        }
        if (!MSGUtil.isBlank(user.getPrivilegeTime())) {
            long nextThreeMonth = GetDateUtil.getNextAnyMonth(3,user.getPrivilegeTime()).getTime();
            long privilegeTime = new Date().getTime();
            if(nextThreeMonth>privilegeTime){
                return AppResult.ERROR("特权账号三个月内不能提现！",-1);
            }
        }
        //获取用户账户信息
        Account account = accountService.selectAccountByuserId(userId);
        //获取商家账户信息
        AccountShop accountShop=accountShopService.selectAccountShopByUserId(userId);
        //校验赎回人状态
        if (MSGUtil.isBlank(user.getTrueName())){
            return AppResult.ERROR("您还未实名！",-1);
        }
        SktSysConfigs drawsAmountMin = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMin");
        SktSysConfigs drawsAmountMax = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMax");
        Double min=Double.valueOf(drawsAmountMin.getFieldValue());
        Double max=Double.valueOf(drawsAmountMax.getFieldValue());
        //校验计算手续费
        BigDecimal feeMoney = money.multiply(expensesrate).multiply(new BigDecimal(0.01)).setScale(2,BigDecimal.ROUND_HALF_UP);
        BigDecimal ratioMoney = money.multiply(taxRatio).multiply(new BigDecimal(0.01)).setScale(2,BigDecimal.ROUND_HALF_UP);
        if (type==2){
            ratioMoney=new BigDecimal(0);
        }
        if (type==3){
            ratioMoney=new BigDecimal(0);
        }
        if (money.doubleValue()<min){
            return AppResult.ERROR("单笔最小赎回额度为:"+min.toString(),-1);
        }
        if (money.doubleValue()>max){
            return AppResult.ERROR("单笔最大赎回额度为:"+max.toString(),-1);
        }
        BigDecimal allFee = feeMoney.add(ratioMoney).setScale(3,BigDecimal.ROUND_UP);//手续费+综合服务费(总手续费)
        /*System.out.println(feeMoney+"____________________________");
        System.out.println(expenses+"______________________________");
        int compare = feeMoney.compareTo(expenses);
        if (compare!=0){
            return AppResult.ERROR("手续费计算有误！",-1);
        }
        if (ratioMoney.compareTo(ratio)!=0){
            return AppResult.ERROR("交综合服务费金额计算有误！",-1);
        }*/
        Date dayBegin = GetDateUtil.getDayBegin();
        Date dayEnd = GetDateUtil.getDayEnd();
        Map<String, Object> map = new HashMap<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        map.put("startTime",simpleDateFormat.format(dayBegin));
        map.put("endTime",simpleDateFormat.format(dayEnd));
        map.put("userId",userId);
        map.put("type",type);
        UsersDrawsMaobi sumMoney=iUsersDrawsMaobiService.selectSumMoneyByUserId(map);
        BigDecimal dayHandle=new BigDecimal(0.00);
        if (!MSGUtil.isBlank(sumMoney)){
            dayHandle= sumMoney.getMoney();
        }
        String tCode="drawsAmountDay";
        if(type==1){
            tCode="drawsKaiyuan";
        }
        if (type==3){
            tCode="drawsKaiyuanTrunover";
        }
        SktSysConfigs drawsAmountDays = sktSysConfigsMapper.selectSysConfigsByFieldCode(tCode);
        BigDecimal drawsAmountDay = new BigDecimal(drawsAmountDays.getFieldValue());
        BigDecimal biggest = dayHandle.add(money);
        if (drawsAmountDay.compareTo(biggest)==-1){
            return AppResult.ERROR("每日最大赎回额度为:"+drawsAmountDay+",本日还可提现"+drawsAmountDay.subtract(dayHandle),-1);
        }
        //根据不同赎回类型判断对应余额是否足够 $param['type']  1华宝赎回 2现金赎回 3华宝营业额赎回
        BigDecimal countMoney=money;
        BigDecimal currentMoney=new BigDecimal(0);
        if (type==1){
            if ((money.divideAndRemainder(BigDecimal.valueOf(100))[1]).compareTo(BigDecimal.valueOf(0))!=0){
                return AppResult.ERROR("华宝赎回所得金额必须为100的倍数！",-1);
            }
            if (account.getKaiyuan().compareTo(countMoney.multiply(BigDecimal.valueOf(100)))==-1){
                return AppResult.ERROR("华宝余额不足！",-1);
            }
            currentMoney=account.getKaiyuan();
        }
        if (type==2){
            if (account.getCash().compareTo(countMoney)==-1){
                return AppResult.ERROR("现金余额不足！",-1);
            }
            currentMoney=account.getCash();
        }
        if (type==3){
            if(accountShop.getKaiyuanTurnover().compareTo(countMoney.multiply(BigDecimal.valueOf(100)))==-1){
                return AppResult.ERROR("华宝营业额余额不足！");
            }
            currentMoney=accountShop.getKaiyuanTurnover();
        }

        //计算 赎回基本业务逻辑处理
        String orderNo = StringUtils.getOrderIdByTime("F");
        Map<String, Object> result = this.draw(userId, money, moneyRate, type, expensesrate, taxRatio, allFee, orderNo, currentMoney,ratio);
        int status = Integer.parseInt(result.get("status").toString());
        if (status==1){
            return AppResult.OK("赎回申请操作成功！",null);
        }
        return AppResult.ERROR(result.get("msg").toString(),-1);
        }catch (Exception e){
            e.printStackTrace();
        }
        return AppResult.ERROR("赎回猫币失败",-1);
    }

    /**
     * 赎回   计算 赎回基本业务逻辑处理
     * @param type $uid 用户ID
     * @param type $money 赎回金额
     * @param type $type 赎回类型
     * @param type $expensesrate 手续费比例
     * @param type $taxRatio 综合服务费比例
     * @param type $allFee 手续费加综合服务费
     * @param type $bankCardId 用户赎回卡ID
     * @param type $orderNo 订单号
     * @param type $currentMoney 当前额度
     *
     */
    public Map<String,Object> draw(Integer userId, BigDecimal money,BigDecimal moneyRate, Integer type, BigDecimal expensesrate, BigDecimal taxRatio, BigDecimal allFee, String orderNo, BigDecimal currentMoney,BigDecimal ratio) {
        Map<String, Object> result = new HashMap<>();
        try {
            if (money.compareTo(BigDecimal.valueOf(0))==-1){
                result.put("msg","金额不能为0");
                result.put("status",-1);
                return result;
            }
            BigDecimal allmoney=money;//需要从余额扣除的金额
            BigDecimal hasmoney=money.subtract(allFee);//预计得到金额
            Date time = new Date();
            //赎回记录
            UsersDrawsMaobi usersDrawsMaobi = new UsersDrawsMaobi();
            usersDrawsMaobi.setDrawNo(orderNo);
            usersDrawsMaobi.setUserId(userId);
            usersDrawsMaobi.setType(type);
            usersDrawsMaobi.setMoney(hasmoney);
            BigDecimal amount = hasmoney.multiply(moneyRate).setScale(4, BigDecimal.ROUND_DOWN);
            usersDrawsMaobi.setMaobi(amount);
            usersDrawsMaobi.setMoneyRate(moneyRate);
            usersDrawsMaobi.setCheckRemark("转化猫币成功");
            /**********************************************对接交易所开始***************************************/
            int cashSatus=1;
            Map<String, Object> recharge = iAppSktMaobiChangeService.recharge(userId, currentMoney,amount, orderNo,moneyRate,money,2);
            Integer status = Integer.parseInt(recharge.get("status").toString());
            if (status==-2){
                result.put("msg","该用户未注册交易所账户");
                result.put("status",-1);
                return result;
            }
            if(status!=1){
                usersDrawsMaobi.setCheckRemark("交易所转化失败");
                cashSatus=0;
            }
            //转为猫币的状态
            /**********************************************对接交易所结束***************************************/
            usersDrawsMaobi.setFee(allFee);
            usersDrawsMaobi.setRatio(expensesrate);
            usersDrawsMaobi.setTaxratio(taxRatio);
            usersDrawsMaobi.setCashSatus(cashSatus);
            usersDrawsMaobi.setCreateTime(new Date());
            UsersDraws usersDraws = new UsersDraws();
            usersDraws.setDrawNo(orderNo);
            usersDraws.setBankCardId(0);
            usersDraws.setUserId(userId);
            usersDraws.setType(type);
            usersDraws.setMoney(hasmoney);
            usersDraws.setFee(allFee);
            usersDraws.setRatio(ratio);
            usersDraws.setTaxratio(taxRatio);
            usersDraws.setCashSatus(3);
            usersDraws.setCreateTime(new Date());
            usersDraws.setSpecialType(0);
            if (status!=1){
                result.put("msg","交易所转化失败");
                result.put("status",-1);
                return result;
            }
            if (type==1){//华宝
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> kaiYuanMap = new HashMap<>();
                kaiYuanMap.put("userId",userId);
                kaiYuanMap.put("allmoney",allmoney.multiply(BigDecimal.valueOf(100)));
                kaiYuanMap.put("kaiyuan","kaiyuan");
                boolean b = accountService.uptateKaiyuanByUserId(kaiYuanMap);
                if (!b){
                    result.put("msg","转化失败");
                    result.put("status",-1);
                    return result;
                }
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //华宝赎回流水
                    LogKaiyuan logKaiyuan = new LogKaiyuan();
                    logKaiyuan.setType(41);
                    logKaiyuan.setFromId(0);
                    logKaiyuan.setUserId(userId);
                    logKaiyuan.setOrderNo(orderNo);
                    logKaiyuan.setPreKaiyuan(currentMoney);
                    logKaiyuan.setKaiyuanType(-1);
                    logKaiyuan.setKaiyuan(hasmoney.multiply(BigDecimal.valueOf(100)));
                    logKaiyuan.setRemark("赎回支出猫币");
                    logKaiyuan.setDataFlag(1);
                    logKaiyuan.setCreateTime(new Date());
                    boolean insert = iAppLogKaiyuanService.insert(logKaiyuan);
                    //华宝赎回手续费流水
                    logKaiyuan=new LogKaiyuan();
                    logKaiyuan.setType(34);
                    logKaiyuan.setFromId(0);
                    logKaiyuan.setUserId(userId);
                    logKaiyuan.setOrderNo(orderNo);
                    logKaiyuan.setPreKaiyuan(currentMoney.subtract(hasmoney.multiply(BigDecimal.valueOf(100))));
                    logKaiyuan.setKaiyuanType(-1);
                    logKaiyuan.setKaiyuan(allFee.multiply(BigDecimal.valueOf(100)));
                    logKaiyuan.setRemark("赎回支出手续费+综合服务费");
                    logKaiyuan.setDataFlag(1);
                    logKaiyuan.setCreateTime(new Date());
                    boolean inserts = iAppLogKaiyuanService.insert(logKaiyuan);
                }
            }
            if (type==2){//现金
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> cashMap = new HashMap<>();
                cashMap.put("userId",userId);
                cashMap.put("allmoney",allmoney);
                cashMap.put("cash","cash");
                boolean b = accountService.uptateCashByUserId(cashMap);
                if (!b){
                    result.put("msg","转化失败");
                    result.put("status",-1);
                    return result;
                }
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //现金赎回流水
                    LogCash logCash = new LogCash();
                    logCash.setType(39);
                    logCash.setFromId(0);
                    logCash.setUserId(userId);
                    logCash.setOrderNo(orderNo);
                    logCash.setPreCash(currentMoney);
                    logCash.setCashType(-1);
                    logCash.setCash(hasmoney);
                    logCash.setRemark("赎回支出猫币");
                    logCash.setDataFlag(1);
                    logCash.setCreateTime(new Date());
                    boolean insert = iAppLogCashService.insert(logCash);
                    //现金赎回手续费流水
                    logCash=new LogCash();
                    logCash.setType(36);
                    logCash.setFromId(0);
                    logCash.setUserId(userId);
                    logCash.setOrderNo(orderNo);
                    logCash.setPreCash(currentMoney.subtract(hasmoney));
                    logCash.setCashType(-1);
                    logCash.setCash(allFee);
                    logCash.setRemark("赎回支出手续费");
                    logCash.setDataFlag(1);
                    logCash.setCreateTime(new Date());
                    boolean inserts = iAppLogCashService.insert(logCash);
                }
            }
            if (type==3){//华宝营业额
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> map = new HashMap<>();
                map.put("userId",userId);
                map.put("allmoney",allmoney.multiply(BigDecimal.valueOf(100)));
                boolean b=accountShopService.updateKaiyuanTurnoverByUserId(map);
                if (!b){
                    result.put("msg","转化失败");
                    result.put("status",-1);
                    return result;
                }
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //华宝营业额赎回流水
                    LogKaiyuanTurnover logKaiyuanTurnover = new LogKaiyuanTurnover();
                    logKaiyuanTurnover.setType(41);
                    logKaiyuanTurnover.setFromId(0);
                    logKaiyuanTurnover.setUserId(userId);
                    logKaiyuanTurnover.setOrderNo(orderNo);
                    logKaiyuanTurnover.setPreKaiyuan(currentMoney);
                    logKaiyuanTurnover.setKaiyuanType(-1);
                    logKaiyuanTurnover.setKaiyuan(hasmoney.multiply(BigDecimal.valueOf(100)));
                    logKaiyuanTurnover.setRemark("赎回支出猫币");
                    logKaiyuanTurnover.setDataFlag(1);
                    logKaiyuanTurnover.setCreateTime(new Date());
                    boolean insert = iLogKaiyuanTurnoverService.insert(logKaiyuanTurnover);
                    //华宝营业额赎回手续费流水
                    logKaiyuanTurnover=new LogKaiyuanTurnover();
                    logKaiyuanTurnover.setType(34);
                    logKaiyuanTurnover.setFromId(0);
                    logKaiyuanTurnover.setUserId(userId);
                    logKaiyuanTurnover.setOrderNo(orderNo);
                    logKaiyuanTurnover.setPreKaiyuan(currentMoney.subtract(hasmoney.multiply(BigDecimal.valueOf(100))));
                    logKaiyuanTurnover.setKaiyuanType(-1);
                    logKaiyuanTurnover.setKaiyuan(allFee.multiply(BigDecimal.valueOf(100)));
                    logKaiyuanTurnover.setRemark("赎回支出手续费");
                    logKaiyuanTurnover.setDataFlag(1);
                    logKaiyuanTurnover.setCreateTime(new Date());
                    boolean inserts = iLogKaiyuanTurnoverService.insert(logKaiyuanTurnover);
                }
            }
            //插入现金账户记录表
            iAppUserDrwasService.insert(usersDraws);
            //插入赎回记录表
            iUsersDrawsMaobiService.insert(usersDrawsMaobi);
            result.put("msg","转化成功");
            result.put("status",1);
            return result;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("失败");
        }
    }

    /**
     * 赎回回显
     * @param userId
     * @return
     */
    @Override
    public JSONObject cashDraw(Integer userId){
        JSONObject json = new JSONObject();
        Account account=new Account();
        account.setUserId(userId);
        EntityWrapper<Account> entityWrapper=new EntityWrapper<Account>(account);
        Account account1 = accountService.selectOne(entityWrapper);
        JSONObject userAccount = new JSONObject();
        userAccount.put("kaiyuan",account1.getKaiyuan().toString());
        userAccount.put("cash",account1.getCash().toString());
        json.put("userAccount",userAccount);
        AccountShop accountShop=new AccountShop();
        accountShop.setUserId(userId);
        EntityWrapper<AccountShop> entityWrapper1 =new EntityWrapper<AccountShop>(accountShop);
        AccountShop accountShop1 = accountShopService.selectOne(entityWrapper1);
        JSONObject shopAccount = new JSONObject();
        if (accountShop1!=null){

            if (accountShop1.getKaiyuanTurnover()!=null){
                shopAccount.put("kaiyuanTurnover",accountShop1.getKaiyuanTurnover().toString());
            }else {
                shopAccount.put("kaiyuanTurnover","0.00");
            }
        }else {
            shopAccount.put("kaiyuanTurnover","0.00");
        }
        json.put("shopAccount",shopAccount);
        Users users = usersService.selectById(userId);
        json.put("userType",users.getUserType());
        if(users.getUserType()==0){
            SktSysConfigs commonCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("commonCashDraw");
            String commonCashDraws = commonCashDraw.getFieldValue();
            json.put("commonCashDraw", commonCashDraws);
//				BigDecimal  commonCashDraws=new BigDecimal(commonCashDraw.getFieldValue());
//				json.put("commonCashDraw", commonCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
        }else if(users.getUserType()==1){
            SktSysConfigs directorCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("directorCashDraw");
            String directorCashDraws=directorCashDraw.getFieldValue();
            json.put("commonCashDraw", directorCashDraws);
//				BigDecimal  directorCashDraws= new BigDecimal(directorCashDraw.getFieldValue());
//				json.put("directorCashDraw", directorCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
        }else if(users.getUserType()==2){
            SktSysConfigs managerCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("managerCashDraw");
            String managerCashDraws=managerCashDraw.getFieldValue();
            json.put("commonCashDraw", managerCashDraws);
//				BigDecimal  managerCashDraws=new BigDecimal(managerCashDraw.getFieldValue());
//				json.put("managerCashDraw", managerCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
        }
        SktSysConfigs drawsAmountMin = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMin");
        String  drawsAmountMins=drawsAmountMin.getFieldValue();//最小单笔赎回数量
        SktSysConfigs drawsAmountMax = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMax");
        String  drawsAmountMaxs=drawsAmountMax.getFieldValue();//最大单笔赎回数量
        SktSysConfigs drawsAmountDay = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountDay");
        String  drawsAmountDays=drawsAmountDay.getFieldValue();//每天最大赎回额度
        SktSysConfigs taxRatio = sktSysConfigsMapper.selectSysConfigsByFieldCode("taxRatio");//综合服务费%
        String  taxRatios=taxRatio.getFieldValue();//综合服务费用
        json.put("taxRatio", new BigDecimal(taxRatios));//综合服务费
        json.put("drawsAmountMin", drawsAmountMins);//最小单笔赎回数量
        json.put("drawsAmountMax", drawsAmountMaxs);//最大单笔赎回数量
        json.put("drawsAmountDay", drawsAmountDays);//每天最大赎回额度
        BigDecimal totalUsersDraws = udm.getTotalUsersDraws(userId);//今日已经提现总金额(包括申请的）
        if (totalUsersDraws==null){
            json.put("totalUsersDraws", "0.00");//今日已经提现总金额（包括申请的)
        }else {
            json.put("totalUsersDraws", totalUsersDraws.toString());//今日已经提现总金额（包括申请的)
        }

        SktSysConfigs kaiyuanbaoTXt0 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt0");//华宝赎回% T+1
        /*SktSysConfigs kaiyuanbaoTXt4 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt4");//华宝赎回% T+4
        SktSysConfigs kaiyuanbaoTXt7 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt7");//华宝赎回% T+7*/
        //	BigDecimal huaBaoTXt0=new BigDecimal(kaiyuanbaoTXt0.getFieldValue());
        String  huaBaoTXt0=kaiyuanbaoTXt0.getFieldValue();
        //String  huaBaoTXt4=kaiyuanbaoTXt4.getFieldValue();
        //	BigDecimal huaBaoTXt4=new BigDecimal(kaiyuanbaoTXt4.getFieldValue());
        //String  huaBaoTXt7=kaiyuanbaoTXt7.getFieldValue();
        //	BigDecimal huaBaoTXt7=new BigDecimal(kaiyuanbaoTXt7.getFieldValue());
        json.put("kaiyuanbaoTXt0",huaBaoTXt0);//开元宝提现手续费(1天)
        /*json.put("kaiyuanbaoTXt4",huaBaoTXt4);//开元宝提现手续费(4天)
        json.put("kaiyuanbaoTXt7",huaBaoTXt7);//开元宝提现手续费(7天)*/
        SktSysConfigs kaiyuanYingyeTX = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanYingyeTX");
        String kaiyuanYingyeTXs= kaiyuanYingyeTX.getFieldValue();
        json.put("kaiyuanYingyeTX", kaiyuanYingyeTXs);//开元宝营业额余额提现手续费
        Object ticker = iAppSktMaobiChangeService.ticker();
        BigDecimal cnyPrice = new BigDecimal(ticker.toString()).setScale(4,BigDecimal.ROUND_HALF_UP);
        json.put("moneyRate",cnyPrice.toString());
        json.put("status",1);
        json.put("msg","获取成功");
        return json;
    }


    /**
     * 赎回申请
     * @param userId  用户ID
     * @param type  提现的类型：1开元宝提现 2现金提现 3开元宝营业额提现
     * @param money  提现金额
     * @param expenses  手续费+税率（平台扣除的费用）fee
     * @param expensesrate  提现手续费比例 %   ratio
     * @param taxRatio  提现税率比例%  （服务费用） 10
     * @param payPwd   支付密码
     * @return
     */
    @Override
    public AppResult applyRedeemHHTB(Integer userId, BigDecimal moneyRate, Integer type, BigDecimal money, BigDecimal expenses, BigDecimal expensesrate, BigDecimal taxRatio, BigDecimal ratio, String payPwd) {
        try {
            /*String isDraws = sktSysConfigsMapper.findFieldValue("isDraws");
            if (Integer.parseInt(isDraws)!=1){
                return AppResult.ERROR("该功能暂时关闭",-1);
            }*/
            Users user = usersMapper.selectById(userId);
          /*if(type==3) {
                *//*SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date oldDate = dateFormat.parse("2018-8-6 11:00:00");
                Date dates = new Date();
                if (dates.getTime() > oldDate.getTime()) {
                    if (user.getIsOperation() != 1) {
                        return AppResult.ERROR("赎回功能已关闭", -1);
                    }
                }*//*
                return AppResult.ERROR("华宝货款赎回功能已关闭", -1);
          }*/
            SimpleDateFormat setTime = new SimpleDateFormat("HH:mm:ss");
            String parse = setTime.format(new Date());
            Date parse1 = setTime.parse(parse);//当前时间时分秒
            Date parse2 = setTime.parse("11:00:00");//设定11点时间
            if (parse2.getTime()>parse1.getTime()){//如果设定时分秒大于当前时分秒
                return AppResult.ERROR("hhtb赎回在11点开启",-1);
            }
            JSONObject json = new JSONObject();
            /*if (user.getUserType() == 0) {
                return AppResult.ERROR("普通用户没有申请赎回权限,请升级用户", -1);
            }*/
            if (user.getIsWithdrawCash() != 1) {
                return AppResult.ERROR("您没有申请赎回权限", -1);
            }
            //判定今天是否为工作日
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            String format1 = format.format(new Date());
            int date = Integer.parseInt(format1);
            SktSysConfigs sktSysConfigs = sktSysConfigsMapper.selectSysConfigsByFieldCode("isHolidaysCash");
            if (Integer.parseInt(sktSysConfigs.getFieldValue()) != 1) {
                SktDate sktDate = iSktDateService.selectDateByDate(date);
                if (sktDate.getStatus() != 0) {
                    if (sktDate.getStatus() == 1) {
                        return AppResult.ERROR("今日为休息日不可进行提现操作", -1);
                    }
                }
            }

            //SktSysConfigs sktSysConfigs = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountDay");
            //Integer fieldValue = Integer.parseInt(sktSysConfigs.getFieldValue());
            BigDecimal moneyValue = money;
            JSONObject jsonObject = usersService.checkPayPassWorld(userId, payPwd);
            if ("00".equals(jsonObject.get("code"))) {
                return AppResult.ERROR("交易密码不正确", -1);
            }
            /*if (!MSGUtil.isBlank(user.getPrivilegeTime())) {
                long nextThreeMonth = GetDateUtil.getNextAnyMonth(3, user.getPrivilegeTime()).getTime();
                long privilegeTime = new Date().getTime();
                if (nextThreeMonth > privilegeTime) {
                    return AppResult.ERROR("特权账号三个月内不能提现！", -1);
                }
            }*/
            //获取用户账户信息
            Account account = accountService.selectAccountByuserId(userId);
            //获取商家账户信息
            AccountShop accountShop = accountShopService.selectAccountShopByUserId(userId);
            //校验赎回人状态
            if (MSGUtil.isBlank(user.getTrueName())) {
                return AppResult.ERROR("您还未实名！", -1);
            }
            SktSysConfigs drawsAmountMin = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMin");
            SktSysConfigs drawsAmountMax = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMax");
            Double min = Double.valueOf(drawsAmountMin.getFieldValue());
            Double max = Double.valueOf(drawsAmountMax.getFieldValue());
            //校验计算手续费
            BigDecimal feeMoney = money.multiply(expensesrate).multiply(new BigDecimal(0.01)).setScale(2, BigDecimal.ROUND_HALF_UP);
            BigDecimal ratioMoney = money.multiply(taxRatio).multiply(new BigDecimal(0.01)).setScale(2, BigDecimal.ROUND_HALF_UP);
            if (type == 2) {
                ratioMoney = new BigDecimal(0);
            }
            if (type == 3) {
                ratioMoney = new BigDecimal(0);
            }
            if (money.doubleValue() < min) {
                return AppResult.ERROR("单笔最小赎回额度为:" + min.toString(), -1);
            }
            if(type == 1){
                if ((money.divideAndRemainder(BigDecimal.valueOf(100))[1]).compareTo(BigDecimal.valueOf(0)) != 0) {
                    return AppResult.ERROR("华宝赎回所得金额必须为100的倍数！", -1);
                }
                Account account1 = accountService.selectAccountByuserId(userId);
                BigDecimal kaiyuanMaxs = account1.getKaiyuan().setScale(3,BigDecimal.ROUND_HALF_UP);
                BigDecimal kaiyuanMax = kaiyuanMaxs.divide(new BigDecimal(100));
                max=Double.valueOf(kaiyuanMax.toString());
            }
            if (money.doubleValue() > max) {
                return AppResult.ERROR("单笔最大赎回额度为:" + max.toString(), -1);
            }
            BigDecimal allFee = feeMoney.add(ratioMoney).setScale(3, BigDecimal.ROUND_UP);//手续费+综合服务费(总手续费)
        /*System.out.println(feeMoney+"____________________________");
        System.out.println(expenses+"______________________________");
        int compare = feeMoney.compareTo(expenses);
        if (compare!=0){
            return AppResult.ERROR("手续费计算有误！",-1);
        }
        if (ratioMoney.compareTo(ratio)!=0){
            return AppResult.ERROR("交综合服务费金额计算有误！",-1);
        }*/
            Date dayBegin = GetDateUtil.getDayBegin();
            Date dayEnd = GetDateUtil.getDayEnd();
            Map<String, Object> map = new HashMap<>();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            map.put("startTime", simpleDateFormat.format(dayBegin));
            map.put("endTime", simpleDateFormat.format(dayEnd));
            map.put("userId", userId);
            map.put("type", type);
            UsersDrawsMaobi sumMoney = iUsersDrawsMaobiService.selectSumMoneyByUserId(map);
            BigDecimal dayHandle = new BigDecimal(0.00);
            if (!MSGUtil.isBlank(sumMoney)) {
                dayHandle = sumMoney.getMoney();
            }
            String tCode = "drawsAmountDay";
            if (type == 3) {
                tCode = "drawsKaiyuanTrunover";
            }
            SktSysConfigs drawsAmountDays = sktSysConfigsMapper.selectSysConfigsByFieldCode(tCode);
            BigDecimal drawsAmountDay = new BigDecimal(drawsAmountDays.getFieldValue());
            BigDecimal biggest = dayHandle.add(money);
            /*if (type == 1) {
                drawsAmountDay = new BigDecimal(max);
            }*/
            if(type!=1){
                if (drawsAmountDay.compareTo(biggest) == -1) {
                    return AppResult.ERROR("每日最大赎回额度为:" + drawsAmountDay + ",本日还可提现" + drawsAmountDay.subtract(dayHandle), -1);
                }
            }
            //根据不同赎回类型判断对应余额是否足够 $param['type']  1华宝赎回 2现金赎回 3华宝营业额赎回
            BigDecimal countMoney = money;
            BigDecimal currentMoney = new BigDecimal(0);
            if (type == 1) {
                if ((money.divideAndRemainder(BigDecimal.valueOf(100))[1]).compareTo(BigDecimal.valueOf(0)) != 0) {
                    return AppResult.ERROR("华宝赎回所得金额必须为100的倍数！", -1);
                }
                if (account.getKaiyuan().compareTo(countMoney.multiply(BigDecimal.valueOf(100))) == -1) {
                    return AppResult.ERROR("华宝余额不足！", -1);
                }
                currentMoney = account.getKaiyuan();
            }
            if (type == 2) {
                if (account.getCash().compareTo(countMoney) == -1) {
                    return AppResult.ERROR("现金余额不足！", -1);
                }
                currentMoney = account.getCash();
            }
            if (type == 3) {
                if (accountShop.getKaiyuanTurnover().compareTo(countMoney.multiply(BigDecimal.valueOf(100))) == -1) {
                    return AppResult.ERROR("华宝营业额余额不足！",-1);
                }
                currentMoney = accountShop.getKaiyuanTurnover();
            }

            //计算 赎回基本业务逻辑处理
            String orderNo = StringUtils.getOrderIdByTime("H");
            Map<String, Object> result = this.drawHHTB(userId, money, moneyRate, type, expensesrate, taxRatio, allFee, orderNo, currentMoney, ratio);
            int status = Integer.parseInt(result.get("status").toString());
            if (status == 1) {
                return AppResult.OK("赎回申请操作成功！", null);
            }
            return AppResult.ERROR(result.get("msg").toString(),-1);
        }catch (Exception e){
            e.printStackTrace();
        }
        return AppResult.ERROR("赎回HHTB失败",-1);
    }


    /**
     * 赎回 HHTB计算 赎回基本业务逻辑处理
     * @param type $uid 用户ID
     * @param type $money 赎回金额
     * @param type $type 赎回类型
     * @param type $expensesrate 手续费比例
     * @param type $taxRatio 综合服务费比例
     * @param type $allFee 手续费加综合服务费
     * @param type $bankCardId 用户赎回卡ID
     * @param type $orderNo 订单号
     * @param type $currentMoney 当前额度
     *
     */
    public Map<String,Object> drawHHTB(Integer userId, BigDecimal money,BigDecimal moneyRate, Integer type, BigDecimal expensesrate, BigDecimal taxRatio, BigDecimal allFee, String orderNo, BigDecimal currentMoney,BigDecimal ratio) {
        Map<String, Object> result = new HashMap<>();
        try {
            if (money.compareTo(BigDecimal.valueOf(0))==-1){
                result.put("msg","金额不能为0");
                result.put("status",-1);
                return result;
            }
            BigDecimal allmoney=money;//需要从余额扣除的金额
            BigDecimal hasmoney=money.subtract(allFee);//预计得到金额
            Date time = new Date();
            //赎回记录
            UsersDrawsMaobi usersDrawsMaobi = new UsersDrawsMaobi();
            usersDrawsMaobi.setDrawNo(orderNo);
            usersDrawsMaobi.setUserId(userId);
            usersDrawsMaobi.setType(type);
            usersDrawsMaobi.setMoney(hasmoney);
            BigDecimal amount = hasmoney.multiply(moneyRate).setScale(4, BigDecimal.ROUND_DOWN);
            usersDrawsMaobi.setMaobi(amount);
            usersDrawsMaobi.setMoneyRate(moneyRate);
            usersDrawsMaobi.setCheckRemark("转化HHTB成功");
            /**********************************************对接交易所开始***************************************/
            int cashSatus=1;
            Map<String, Object> recharge = iAppSktMaobiChangeService.recharge(userId, currentMoney,amount, orderNo,moneyRate,money,1);
            Integer status = Integer.parseInt(recharge.get("status").toString());
            if (status==-2){
                result.put("msg","该用户未注册交易所账户");
                result.put("status",-1);
                return result;
            }
            if(status!=1){
                usersDrawsMaobi.setCheckRemark("交易所转化失败");
                cashSatus=0;
            }
            //转为HHTB的状态
            /**********************************************对接交易所结束***************************************/
            usersDrawsMaobi.setFee(allFee);
            usersDrawsMaobi.setRatio(expensesrate);
            usersDrawsMaobi.setTaxratio(taxRatio);
            usersDrawsMaobi.setCashSatus(cashSatus);
            usersDrawsMaobi.setCreateTime(new Date());
            usersDrawsMaobi.setDrawsType(1);
            UsersDraws usersDraws = new UsersDraws();
            usersDraws.setDrawNo(orderNo);
            usersDraws.setBankCardId(0);
            usersDraws.setUserId(userId);
            usersDraws.setType(type);
            usersDraws.setMoney(hasmoney);
            usersDraws.setFee(allFee);
            usersDraws.setRatio(expensesrate);
            usersDraws.setTaxratio(taxRatio);
            usersDraws.setCashSatus(3);
            usersDraws.setCreateTime(new Date());
            usersDraws.setSpecialType(0);
            if (status!=1){
                result.put("msg","交易所转化失败");
                result.put("status",-1);
                return result;
            }
            if (type==1){//华宝
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> kaiYuanMap = new HashMap<>();
                kaiYuanMap.put("userId",userId);
                kaiYuanMap.put("allmoney",allmoney.multiply(BigDecimal.valueOf(100)));
                kaiYuanMap.put("kaiyuan","kaiyuan");
                boolean b = accountService.uptateKaiyuanByUserId(kaiYuanMap);
                if (!b){
                    result.put("msg","转化失败");
                    result.put("status",-1);
                    return result;
                }
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //华宝赎回流水
                    LogKaiyuan logKaiyuan = new LogKaiyuan();
                    logKaiyuan.setType(42);
                    logKaiyuan.setFromId(0);
                    logKaiyuan.setUserId(userId);
                    logKaiyuan.setOrderNo(orderNo);
                    logKaiyuan.setPreKaiyuan(currentMoney);
                    logKaiyuan.setKaiyuanType(-1);
                    logKaiyuan.setKaiyuan(hasmoney.multiply(BigDecimal.valueOf(100)));
                    logKaiyuan.setRemark("赎回支出HHTB");
                    logKaiyuan.setDataFlag(1);
                    logKaiyuan.setCreateTime(new Date());
                    boolean insert = iAppLogKaiyuanService.insert(logKaiyuan);
                    //华宝赎回手续费流水
                    logKaiyuan=new LogKaiyuan();
                    logKaiyuan.setType(34);
                    logKaiyuan.setFromId(0);
                    logKaiyuan.setUserId(userId);
                    logKaiyuan.setOrderNo(orderNo);
                    logKaiyuan.setPreKaiyuan(currentMoney.subtract(hasmoney.multiply(BigDecimal.valueOf(100))));
                    logKaiyuan.setKaiyuanType(-1);
                    logKaiyuan.setKaiyuan(allFee.multiply(BigDecimal.valueOf(100)));
                    logKaiyuan.setRemark("赎回支出手续费+综合服务费");
                    logKaiyuan.setDataFlag(1);
                    logKaiyuan.setCreateTime(new Date());
                    boolean inserts = iAppLogKaiyuanService.insert(logKaiyuan);
                }
            }
            if (type==2){//现金
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> cashMap = new HashMap<>();
                cashMap.put("userId",userId);
                cashMap.put("allmoney",allmoney);
                cashMap.put("cash","cash");
                boolean b = accountService.uptateCashByUserId(cashMap);
                if (!b){
                    result.put("msg","转化失败");
                    result.put("status",-1);
                    return result;
                }
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //现金赎回流水
                    LogCash logCash = new LogCash();
                    logCash.setType(40);
                    logCash.setFromId(0);
                    logCash.setUserId(userId);
                    logCash.setOrderNo(orderNo);
                    logCash.setPreCash(currentMoney);
                    logCash.setCashType(-1);
                    logCash.setCash(hasmoney);
                    logCash.setRemark("赎回支出HHTB");
                    logCash.setDataFlag(1);
                    logCash.setCreateTime(new Date());
                    boolean insert = iAppLogCashService.insert(logCash);
                    //现金赎回手续费流水
                    logCash=new LogCash();
                    logCash.setType(36);
                    logCash.setFromId(0);
                    logCash.setUserId(userId);
                    logCash.setOrderNo(orderNo);
                    logCash.setPreCash(currentMoney.subtract(hasmoney));
                    logCash.setCashType(-1);
                    logCash.setCash(allFee);
                    logCash.setRemark("赎回支出手续费");
                    logCash.setDataFlag(1);
                    logCash.setCreateTime(new Date());
                    boolean inserts = iAppLogCashService.insert(logCash);
                }
            }
            if (type==3){//华宝营业额
                //扣除因赎回减少的现金、华宝、华宝营业额额度
                Map<String, Object> map = new HashMap<>();
                map.put("userId",userId);
                map.put("allmoney",allmoney.multiply(BigDecimal.valueOf(100)));
                boolean b=accountShopService.updateKaiyuanTurnoverByUserId(map);
                if (!b){
                    result.put("msg","转化失败");
                    result.put("status",-1);
                    return result;
                }
                if (allFee.compareTo(BigDecimal.valueOf(0))==1){
                    //华宝营业额赎回流水
                    LogKaiyuanTurnover logKaiyuanTurnover = new LogKaiyuanTurnover();
                    logKaiyuanTurnover.setType(42);
                    logKaiyuanTurnover.setFromId(0);
                    logKaiyuanTurnover.setUserId(userId);
                    logKaiyuanTurnover.setOrderNo(orderNo);
                    logKaiyuanTurnover.setPreKaiyuan(currentMoney);
                    logKaiyuanTurnover.setKaiyuanType(-1);
                    logKaiyuanTurnover.setKaiyuan(hasmoney.multiply(BigDecimal.valueOf(100)));
                    logKaiyuanTurnover.setRemark("赎回支出HHTB");
                    logKaiyuanTurnover.setDataFlag(1);
                    logKaiyuanTurnover.setCreateTime(new Date());
                    boolean insert = iLogKaiyuanTurnoverService.insert(logKaiyuanTurnover);
                    //华宝营业额赎回手续费流水
                    logKaiyuanTurnover=new LogKaiyuanTurnover();
                    logKaiyuanTurnover.setType(34);
                    logKaiyuanTurnover.setFromId(0);
                    logKaiyuanTurnover.setUserId(userId);
                    logKaiyuanTurnover.setOrderNo(orderNo);
                    logKaiyuanTurnover.setPreKaiyuan(currentMoney.subtract(hasmoney.multiply(BigDecimal.valueOf(100))));
                    logKaiyuanTurnover.setKaiyuanType(-1);
                    logKaiyuanTurnover.setKaiyuan(allFee.multiply(BigDecimal.valueOf(100)));
                    logKaiyuanTurnover.setRemark("赎回支出手续费");
                    logKaiyuanTurnover.setDataFlag(1);
                    logKaiyuanTurnover.setCreateTime(new Date());
                    boolean inserts = iLogKaiyuanTurnoverService.insert(logKaiyuanTurnover);
                }
            }
            //插入现金账户记录表
            iAppUserDrwasService.insert(usersDraws);
            //插入赎回记录表
            iUsersDrawsMaobiService.insert(usersDrawsMaobi);
            //LogInfoManager.appendLog("DrawsLogInfo", "Date:" + new Date() + "  |","用户ID为："+userId+"赎回成功");
            result.put("msg","转化成功");
            result.put("status",1);
            return result;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("失败");
        }
    }





    /**
     * 赎回回显
     * @param userId
     * @return
     */
    @Override
    public JSONObject cashDrawHHTB(Integer userId){
        JSONObject json = new JSONObject();
        Account account=new Account();
        account.setUserId(userId);
        EntityWrapper<Account> entityWrapper=new EntityWrapper<Account>(account);
        Account account1 = accountService.selectOne(entityWrapper);
        JSONObject userAccount = new JSONObject();
        userAccount.put("kaiyuan",account1.getKaiyuan().toString());
        userAccount.put("cash",account1.getCash().toString());
        json.put("userAccount",userAccount);
        AccountShop accountShop=new AccountShop();
        accountShop.setUserId(userId);
        EntityWrapper<AccountShop> entityWrapper1 =new EntityWrapper<AccountShop>(accountShop);
        AccountShop accountShop1 = accountShopService.selectOne(entityWrapper1);
        JSONObject shopAccount = new JSONObject();
        if (accountShop1!=null){

            if (accountShop1.getKaiyuanTurnover()!=null){
                shopAccount.put("kaiyuanTurnover",accountShop1.getKaiyuanTurnover().toString());
            }else {
                shopAccount.put("kaiyuanTurnover","0.00");
            }
        }else {
            shopAccount.put("kaiyuanTurnover","0.00");
        }
        json.put("shopAccount",shopAccount);
        Users users = usersService.selectById(userId);
        json.put("userType",users.getUserType());
        if(users.getUserType()==0){
            SktSysConfigs commonCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("commonCashDraw");
            String commonCashDraws = commonCashDraw.getFieldValue();
            json.put("commonCashDraw", commonCashDraws);
//				BigDecimal  commonCashDraws=new BigDecimal(commonCashDraw.getFieldValue());
//				json.put("commonCashDraw", commonCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
        }else if(users.getUserType()==1){
            SktSysConfigs directorCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("directorCashDraw");
            String directorCashDraws=directorCashDraw.getFieldValue();
            json.put("commonCashDraw", directorCashDraws);
//				BigDecimal  directorCashDraws= new BigDecimal(directorCashDraw.getFieldValue());
//				json.put("directorCashDraw", directorCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
        }else if(users.getUserType()==2){
            SktSysConfigs managerCashDraw = sktSysConfigsMapper.selectSysConfigsByFieldCode("managerCashDraw");
            String managerCashDraws=managerCashDraw.getFieldValue();
            json.put("commonCashDraw", managerCashDraws);
//				BigDecimal  managerCashDraws=new BigDecimal(managerCashDraw.getFieldValue());
//				json.put("managerCashDraw", managerCashDraws.add(ratio).divide(new BigDecimal(100)).setScale(3,BigDecimal.ROUND_HALF_UP));
        }
        SktSysConfigs drawsAmountMin = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMin");
        String  drawsAmountMins=drawsAmountMin.getFieldValue();//最小单笔赎回数量
        SktSysConfigs drawsAmountMax = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountMax");
        String  drawsAmountMaxs=drawsAmountMax.getFieldValue();//最大单笔赎回数量
        SktSysConfigs drawsAmountDay = sktSysConfigsMapper.selectSysConfigsByFieldCode("drawsAmountDay");
        String  drawsAmountDays=drawsAmountDay.getFieldValue();//每天最大赎回额度
        SktSysConfigs taxRatio = sktSysConfigsMapper.selectSysConfigsByFieldCode("taxRatio");//综合服务费%
        String  taxRatios=taxRatio.getFieldValue();//综合服务费用
        json.put("taxRatio", new BigDecimal(taxRatios));//综合服务费
        json.put("drawsAmountMin", drawsAmountMins);//最小单笔赎回数量
        json.put("drawsAmountMax", drawsAmountMaxs);//最大单笔赎回数量
        json.put("drawsAmountDay", drawsAmountDays);//每天最大赎回额度
        BigDecimal totalUsersDraws = udm.getTotalUsersDraws(userId);//今日已经提现总金额(包括申请的）
        if (totalUsersDraws==null){
            json.put("totalUsersDraws", "0.00");//今日已经提现总金额（包括申请的)
        }else {
            json.put("totalUsersDraws", totalUsersDraws.toString());//今日已经提现总金额（包括申请的)
        }

        SktSysConfigs kaiyuanbaoTXt0 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt0");//华宝赎回% T+1
        /*SktSysConfigs kaiyuanbaoTXt4 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt4");//华宝赎回% T+4
        SktSysConfigs kaiyuanbaoTXt7 = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanbaoTXt7");//华宝赎回% T+7*/
        //	BigDecimal huaBaoTXt0=new BigDecimal(kaiyuanbaoTXt0.getFieldValue());
        String  huaBaoTXt0=kaiyuanbaoTXt0.getFieldValue();
        //String  huaBaoTXt4=kaiyuanbaoTXt4.getFieldValue();
        //	BigDecimal huaBaoTXt4=new BigDecimal(kaiyuanbaoTXt4.getFieldValue());
        //String  huaBaoTXt7=kaiyuanbaoTXt7.getFieldValue();
        //	BigDecimal huaBaoTXt7=new BigDecimal(kaiyuanbaoTXt7.getFieldValue());
        json.put("kaiyuanbaoTXt0",huaBaoTXt0);//开元宝提现手续费(1天)
        /*json.put("kaiyuanbaoTXt4",huaBaoTXt4);//开元宝提现手续费(4天)
        json.put("kaiyuanbaoTXt7",huaBaoTXt7);//开元宝提现手续费(7天)*/
        SktSysConfigs kaiyuanYingyeTX = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanYingyeTX");
        String kaiyuanYingyeTXs= kaiyuanYingyeTX.getFieldValue();
        json.put("kaiyuanYingyeTX", kaiyuanYingyeTXs);//开元宝营业额余额提现手续费
        Object ticker = iAppSktMaobiChangeService.ticker();
        SktSysConfigs cnyPrice = sktSysConfigsMapper.selectSysConfigsByFieldCode("HHTBRate");
        json.put("moneyRate",cnyPrice.getFieldValue().toString());
        json.put("status",1);
        json.put("msg","获取成功");
        return json;
    }

    @Test
    public void main(){
        String turl = "https://api1.digsg.com/ticker/ticker?market=maoc_hhtb";
        String json =  HttpUtil.doGet(turl);

        JSONObject  jsonObject  = JSONObject.parseObject(json);
        JSONObject datas = jsonObject.getJSONObject("datas");
        String cnyPrices = datas.getString("cnyPrice");
        double cnyPrice = 1 / Double.valueOf(cnyPrices);
        System.out.println(cnyPrice+"-----------");
    }
}
