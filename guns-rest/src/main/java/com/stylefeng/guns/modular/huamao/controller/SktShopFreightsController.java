package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.SktShopFreights;
import com.stylefeng.guns.modular.huamao.service.ISktShopFreightsService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2018-06-08 15:04:20
 */
@Controller
@RequestMapping("/sktShopFreights")
public class SktShopFreightsController extends BaseController {

    private String PREFIX = "/huamao/sktShopFreights/";

    @Autowired
    private ISktShopFreightsService sktShopFreightsService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopFreights.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/sktShopFreights_add")
    public String sktShopFreightsAdd() {
        return PREFIX + "sktShopFreights_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/sktShopFreights_update/{sktShopFreightsId}")
    public String sktShopFreightsUpdate(@PathVariable Integer sktShopFreightsId, Model model) {
        SktShopFreights sktShopFreights = sktShopFreightsService.selectById(sktShopFreightsId);
        model.addAttribute("item",sktShopFreights);
        LogObjectHolder.me().set(sktShopFreights);
        return PREFIX + "sktShopFreights_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktShopFreightsService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopFreights sktShopFreights) {
        sktShopFreightsService.insert(sktShopFreights);
        return SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopFreightsId) {
        sktShopFreightsService.deleteById(sktShopFreightsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopFreights sktShopFreights) {
        sktShopFreightsService.updateById(sktShopFreights);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{sktShopFreightsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopFreightsId") Integer sktShopFreightsId) {
        return sktShopFreightsService.selectById(sktShopFreightsId);
    }
}
