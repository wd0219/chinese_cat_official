package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 购买牌匾 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
public interface SktBuyplaqueMapper extends BaseMapper<SktBuyplaque> {
	
	/*
	 * 展示全部
	 * */
	public List<SktBuyplaque> sktBuyplaquefindall(SktBuyplaque buyplaque);
	
	public void sktBuyplaqueUpdate(SktBuyplaque buyplaque);
	
	public void sktBuyplaqueUpdate2(SktBuyplaque buyplaque);

    SktBuyplaque selectByOrderNo(Map<String, Object> orderWhere);
}
