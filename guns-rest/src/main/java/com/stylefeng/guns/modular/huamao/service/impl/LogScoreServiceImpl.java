package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreMapper;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.LogScoreList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogScoreServiceImpl extends ServiceImpl<LogScoreMapper, LogScore> implements ILogScoreService {

	@Autowired
	private LogScoreMapper lsm;

	// 展示积分列表
	@Override
	public List<LogScoreList> selectLogScoreAll(LogScoreList logScoreList) {
		return lsm.selectLogScoreAll(logScoreList);
	}

    @Override
    public List<Map<String, Object>> selectUserScore(LogScoreList logScoreList) {
		List<Map<String, Object>> list = lsm.selectUserScore(logScoreList);
		return list;
    }
    /*************************app**************************/
	@Override
	public List<Map<String, Object>> appDividendRecord() {
		List<Map<String, Object>> list = lsm.appDividendRecord();
		return list;
	}

	@Override
	public List<Map<String, Object>> selectLogScoreByUserId(Map<String, Object> map) {
		return lsm.selectLogScoreByUserMap(map);
	}
}
