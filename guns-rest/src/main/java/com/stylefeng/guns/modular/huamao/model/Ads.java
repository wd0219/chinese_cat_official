package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 广告表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
@TableName("skt_ads")
public class Ads extends Model<Ads> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "adId", type = IdType.AUTO)
    private Integer adId;
    /**
     * 广告位置ID
     */
    private Integer adPositionId;
    /**
     * 广告文件
     */
    private String adFile;
    /**
     * 广告名称
     */
    private String adName;
    /**
     * 广告网址
     */
    private String adURL;
    /**
     * 广告开始日期
     */
    private Date adStartDate;
    /**
     * 广告结束日期
     */
    private Date adEndDate;
    /**
     * 排序号
     */
    private Integer adSort;
    /**
     * 广告点击数
     */
    private Integer adClickNum;
    /**
     * 广告类型
     */
    private Integer positionType;
    /**
     * 删除标志
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getAdId() {
        return adId;
    }

    public void setAdId(Integer adId) {
        this.adId = adId;
    }

    public Integer getAdPositionId() {
        return adPositionId;
    }

    public void setAdPositionId(Integer adPositionId) {
        this.adPositionId = adPositionId;
    }

    public String getAdFile() {
        return adFile;
    }

    public void setAdFile(String adFile) {
        this.adFile = adFile;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public String getAdURL() {
        return adURL;
    }

    public void setAdURL(String adURL) {
        this.adURL = adURL;
    }

    public Date getAdStartDate() {
        return adStartDate;
    }

    public void setAdStartDate(Date adStartDate) {
        this.adStartDate = adStartDate;
    }

    public Date getAdEndDate() {
        return adEndDate;
    }

    public void setAdEndDate(Date adEndDate) {
        this.adEndDate = adEndDate;
    }

    public Integer getAdSort() {
        return adSort;
    }

    public void setAdSort(Integer adSort) {
        this.adSort = adSort;
    }

    public Integer getAdClickNum() {
        return adClickNum;
    }

    public void setAdClickNum(Integer adClickNum) {
        this.adClickNum = adClickNum;
    }

    public Integer getPositionType() {
        return positionType;
    }

    public void setPositionType(Integer positionType) {
        this.positionType = positionType;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.adId;
    }

    @Override
    public String toString() {
        return "Ads{" +
        "adId=" + adId +
        ", adPositionId=" + adPositionId +
        ", adFile=" + adFile +
        ", adName=" + adName +
        ", adURL=" + adURL +
        ", adStartDate=" + adStartDate +
        ", adEndDate=" + adEndDate +
        ", adSort=" + adSort +
        ", adClickNum=" + adClickNum +
        ", positionType=" + positionType +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
