package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.GoodsSpecs;

import java.util.Map;

public interface IAppGoodsSpecsService extends IService<GoodsSpecs> {
    void updateGoodsSpecsById(Map<String, Object> map);
}
