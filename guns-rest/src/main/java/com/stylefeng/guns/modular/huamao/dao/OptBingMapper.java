package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.OptBing;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 操作并发限制 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-06-28
 */
public interface OptBingMapper extends BaseMapper<OptBing> {

    void deleteByCodeAndUid(OptBing optBing);
}
