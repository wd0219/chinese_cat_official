package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAreasMapper;
import com.stylefeng.guns.modular.huamao.model.SktAreas;
import com.stylefeng.guns.modular.huamao.service.IAppSktAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppSktAreaServiceImpl extends ServiceImpl<SktAreasMapper,SktAreas> implements IAppSktAreaService {
    @Autowired
    private SktAreasMapper sktAreasMapper;

    @Override
    public SktAreas selectSktAreasById(Integer areasId) {
        return sktAreasMapper.selectSktAreasById(areasId);
    }

    @Override
    public SktAreas selectAreaByProvinceId(int alevel) {

        return sktAreasMapper.selectAreaByProvinceId(alevel);
    }

    @Override
    public SktAreas selectAreaByCityId(int alevel) {
        return sktAreasMapper.selectAreaByCityId(alevel);
    }

    @Override
    public SktAreas selectAreaByAreaId(int alevel) {
        return sktAreasMapper.selectAreaByAreaId(alevel);
    }
}
