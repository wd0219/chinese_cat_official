package com.stylefeng.guns.modular.huamao.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.AccountShop;
import com.stylefeng.guns.modular.huamao.model.AccountShopDTO;
import com.stylefeng.guns.modular.huamao.model.AccountShopList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商家账户表(包括线上和线下) Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-12
 */
public interface AccountShopMapper extends BaseMapper<AccountShop> {
	// 展示用户账户列表
	public List<AccountShopList> selectAccountShopAll(AccountShopList accountShopList);

	public AccountShop selectByShopId(Integer shopId);
	public boolean updateAccountShop(AccountShop accountShop);

	// 通过userId查询用户是否是商家
	public AccountShop findShopId(Integer userId);

	// 实时查询操作金额
	public AccountShop findKaiyuanTurnover(Integer userId);

	// 将商家待发华宝营业额转入正式
	public Integer updateKaiyuanByuserId(AccountShop findKaiyuanTurnover);

	//查询商家账户余额
    AccountShopDTO findAccountByUserId(Integer userId);

	public Integer updateStockScoreById(AccountShop accountShop);

    AccountShop selectAccountShopByUserIdAndShopId(@Param("userId") Integer userId, @Param("shopId") Integer shopId);

    Map<String,Object> selectScoreByShopId(Integer shopId);

    AccountShop selectAccountShopByUserId(Integer userId);

    void updateAccountShopsByUserId(Map<String, Object> maps);

    int updateKaiyuanTurnoverByUserId(Map<String, Object> map);

    int updateAccountShopById(AccountShop accountShop);

	public AccountShop selectByUserId(Integer userId);
}
