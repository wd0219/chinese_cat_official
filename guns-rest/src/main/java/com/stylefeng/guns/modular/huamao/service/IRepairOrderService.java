package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.RepairOrder;
import com.stylefeng.guns.modular.huamao.model.RepairOrderList;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-26
 */
public interface IRepairOrderService extends IService<RepairOrder> {

	// 第三方支付补单记录展示
	public List<RepairOrderList> selectRepairOrderAll(RepairOrderList repairOrderList);

}
