package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;

import java.util.List;
import java.util.Map;

public interface IAppSktWorkOrderService extends IService<SktWorkorder> {
    SktWorkorder selectByOrderNo(Map<String, Object> orderWhere);

	public List<Map<String, Object>> selectRecord();
}
