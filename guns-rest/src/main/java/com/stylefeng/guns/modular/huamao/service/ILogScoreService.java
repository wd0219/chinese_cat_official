package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.LogScoreList;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 积分流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface ILogScoreService extends IService<LogScore> {

	// 展示积分列表
	public List<LogScoreList> selectLogScoreAll(LogScoreList logScoreList);

	/**
	 * 首页显示
	 * @param logScoreList
	 * @return
	 */
    public  List<Map<String,Object>> selectUserScore(LogScoreList logScoreList);
    /************************app*************************/
    /**
     * 分红记录
     * @return
     */
	public List<Map<String, Object>> appDividendRecord();

	List<Map<String,Object>> selectLogScoreByUserId(Map<String, Object> map);
}
