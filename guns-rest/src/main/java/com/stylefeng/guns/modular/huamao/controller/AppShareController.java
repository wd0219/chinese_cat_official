package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppLogCashService;
import com.stylefeng.guns.modular.huamao.service.IAppLogScoreService;
import com.stylefeng.guns.modular.huamao.service.IAppUserService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.CommonModel;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/app")
public class AppShareController extends BaseController {

    @Autowired
    private IAppUserService iAppUserService;
    @Autowired
    private IAppLogCashService iAppLogCashService;
    @Autowired
    private IAppLogScoreService iAppLogScoreService;
    @Autowired
    private CacheUtil cacheUtil;

    /**
     * 我的分享 分享记录 详情
     * @return
     */
    @RequestMapping("/share/share")
    @ResponseBody
    public AppResult share(){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if (user == null) {
                return AppResult.ERROR("您未登录", -2);
            }
            HashMap<String, Object> map = new HashMap<>();
            CommonModel commonModel=new CommonModel();
            String urls= "http://www.izzht.com/home/users/register?phone="+user.getUserPhone();
            map.put("qrCode",urls);
            return AppResult.OK("查询成功",map);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("出错了",-1);
        }

    }

    /**
     * 我的分享人
     * @return
     */
    @RequestMapping("/share/myShareMan")
    @ResponseBody
    public AppResult myShareMan(){
        try {
            HashMap<String, Object> map = new HashMap<>();
            Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
            Integer inviteId = user.getInviteId();
            Users users = iAppUserService.selectUsersByUserId(inviteId);
            if (MSGUtil.isBlank(users)){
                return AppResult.ERROR("您没有分享人",-1);
            }
            map.put("userPhone", users.getUserPhone());
            map.put("loginName", users.getLoginName());
            map.put("userPhoto",users.getUserPhoto());
            return AppResult.OK("获取成功", map);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("查询失败",-2);
        }
    }

    /**
     * 分享记录
     * @return
     */
    @RequestMapping("/share/shareList")
    @ResponseBody
    public AppResult shareList(){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            //获取分享人列表
            List<Users> invite= iAppUserService.selectUsersInviteMan(user);
            //总分享人数
            int all=iAppUserService.selectInvitesCount(user);
            //主管个数
            int charge=iAppUserService.selectChargeCount(user);
            //经理个数
            int manager=iAppUserService.selectManagerCount(user);
            Map<String, Object> map = new HashMap<>();
            map.put("userId",user.getUserId());
            //累积现金奖励
            LogCash cash=iAppLogCashService.selectSumCash(map);
            //累积积分奖励
            LogScore score=iAppLogScoreService.selectSumLogScore(map);
            map=new  HashMap<>();
            map.put("invite",invite);
            map.put("all",all);
            map.put("charge",charge);
            map.put("manager",manager);
            map.put("cash",MSGUtil.isBlank(cash)?0.00:cash.getCash());
            map.put("score",MSGUtil.isBlank(score)?0.00:score.getScore());
            return AppResult.OK("获取成功",map);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("获取失败",-2);
        }
    }

    /**
     * 详情
     * @param userId
     * @param createTime
     * @return
     */
    @RequestMapping("/share/shareDetail")
    @ResponseBody
    public AppResult shareDetail(@ModelAttribute("userId") String userId, @ModelAttribute("createTime") String createTime){
        Map<String, Object> map = new HashMap<>();
        Integer userIds=Integer.parseInt(userId);
        Users user=new Users();
        user.setUserId(userIds);
        int invite = iAppUserService.selectInvitesCount(user);
        map.put("userId",userIds);
        LogCash cash = iAppLogCashService.selectSumCash(map);
        LogScore score=iAppLogScoreService.selectEverySumLogScore(map);
        map=new HashMap<>();
        map.put("invite",invite);
        map.put("createTime",createTime);
        map.put("cash",MSGUtil.isBlank(cash)?0.00:cash.getCash());
        map.put("score",MSGUtil.isBlank(score)?0.00:score.getScore());
        return AppResult.OK("获取成功",map);
    }

}
