package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public class GoodsSellStock implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private Integer goodsStatus;
    /**
     * 违规原因
     */
    private String illegalRemarks;
    private Integer goodsId;
    /**
     * 商品编号
     */
    private String goodsSn;
    /**
     * 商品货号
     */
    private String productNo;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 门店价
     */
    private BigDecimal shopPrice;
    /**
     * 预警库存
     */
    private Integer warnStock;
    /**
     * 商品总库存
     */
    private Integer goodsStock;
    /**
     * 是否上架 0：不上架 1：上架
     */
    private Integer isSale;
    private String sisSale;
    /**
     * 是否精品 0：否 1：是
     */
    private Integer isBest;
    private String sisBest;
    /**
     * 是否热销产品 0：否 1：是
     */
    private Integer isHot;
    private String sisHot;
    /**
     * 是否新品 0：否 1：是
     */
    private Integer isNew;
    private String sisNew;
    /**
     * 是否推荐 0：否 1：是
     */
    private Integer isRecom;
    private String sisRecom;
    /**
     * 品牌ID
     */
    private Integer brandId;
    /**
     * 销量
     */
    private Integer saleNum;
    private Integer shopCatId1;
    private Integer shopCatId2;
	public Integer getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	public String getGoodsSn() {
		return goodsSn;
	}
	public void setGoodsSn(String goodsSn) {
		this.goodsSn = goodsSn;
	}
	public String getProductNo() {
		return productNo;
	}
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsImg() {
		return goodsImg;
	}
	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	public BigDecimal getShopPrice() {
		return shopPrice;
	}
	public void setShopPrice(BigDecimal shopPrice) {
		this.shopPrice = shopPrice;
	}
	public Integer getWarnStock() {
		return warnStock;
	}
	public void setWarnStock(Integer warnStock) {
		this.warnStock = warnStock;
	}
	public Integer getGoodsStock() {
		return goodsStock;
	}
	public void setGoodsStock(Integer goodsStock) {
		this.goodsStock = goodsStock;
	}
	public Integer getIsSale() {
		return isSale;
	}
	public void setIsSale(Integer isSale) {
		this.isSale = isSale;
	}
	public Integer getIsBest() {
		return isBest;
	}
	public void setIsBest(Integer isBest) {
		this.isBest = isBest;
	}
	public Integer getIsHot() {
		return isHot;
	}
	public void setIsHot(Integer isHot) {
		this.isHot = isHot;
	}
	public Integer getIsNew() {
		return isNew;
	}
	public void setIsNew(Integer isNew) {
		this.isNew = isNew;
	}
	public Integer getIsRecom() {
		return isRecom;
	}
	public void setIsRecom(Integer isRecom) {
		this.isRecom = isRecom;
	}
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public Integer getSaleNum() {
		return saleNum;
	}
	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}
	public Integer getGoodsStatus() {
		return goodsStatus;
	}
	public void setGoodsStatus(Integer goodsStatus) {
		this.goodsStatus = goodsStatus;
	}
	public String getSisSale() {
		return sisSale;
	}
	public void setSisSale(String sisSale) {
		this.sisSale = sisSale;
	}
	public String getSisBest() {
		return sisBest;
	}
	public void setSisBest(String sisBest) {
		this.sisBest = sisBest;
	}
	public String getSisHot() {
		return sisHot;
	}
	public void setSisHot(String sisHot) {
		this.sisHot = sisHot;
	}
	public String getSisNew() {
		return sisNew;
	}
	public void setSisNew(String sisNew) {
		this.sisNew = sisNew;
	}
	public String getSisRecom() {
		return sisRecom;
	}
	public void setSisRecom(String sisRecom) {
		this.sisRecom = sisRecom;
	}
	public String getIllegalRemarks() {
		return illegalRemarks;
	}
	public void setIllegalRemarks(String illegalRemarks) {
		this.illegalRemarks = illegalRemarks;
	}
	public Integer getShopCatId1() {
		return shopCatId1;
	}
	public void setShopCatId1(Integer shopCatId1) {
		this.shopCatId1 = shopCatId1;
	}
	public Integer getShopCatId2() {
		return shopCatId2;
	}
	public void setShopCatId2(Integer shopCatId2) {
		this.shopCatId2 = shopCatId2;
	}
	
   
}
