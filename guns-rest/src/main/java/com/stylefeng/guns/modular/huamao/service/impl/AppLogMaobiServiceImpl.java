package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogMaobiMapper;
import com.stylefeng.guns.modular.huamao.model.LogMaobi;
import com.stylefeng.guns.modular.huamao.service.IAppLogMaobiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppLogMaobiServiceImpl extends ServiceImpl<LogMaobiMapper,LogMaobi> implements IAppLogMaobiService {
    @Autowired
    private LogMaobiMapper logMaobiMapper;
}
