package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 代理公司提现记录表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public class AgentsDrawsList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	@TableId(value = "drawId", type = IdType.AUTO)
	private Integer drawId;
	/**
	 * 代理公司名称
	 */
	private String agentName;
	/**
	 * 持卡人姓名
	 */
	private String accUser;
	/**
	 * 持卡人身份证号
	 */
	private String IDnumber;
	// 因为前台获取IDnumber是出错所以让它获取number
	private String number;
	/**
	 * 提现单号
	 */
	private String drawNo;
	/**
	 * 银行卡号
	 */
	private String accNo;
	/**
	 * 代理公司ID
	 */
	private Integer agentId;
	/**
	 * 银行卡表ID
	 */
	private Integer bankCardId;
	/**
	 * 到账金额
	 */
	private BigDecimal money;
	/**
	 * 手续费+税率
	 */
	private BigDecimal fee;
	/**
	 * 提现手续费比例 %
	 */
	private BigDecimal ratio;
	/**
	 * 提现税率比例 %
	 */
	private BigDecimal taxratio;
	/**
	 * 提现状态 -2受理失败 -1审核拒绝 0审核中 2受理中 3受理成功 4线下打款
	 */
	private Integer cashSatus;
	/**
	 * 预计到账时间
	 */
	private Date getTime;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 拒绝理由
	 */
	private String checkRemark;
	/**
	 * 审核员工ID
	 */
	private Integer checkStaffId;
	/**
	 * 审核时间
	 */
	private Date checkTime;
	/**
	 * 0：非手动处理；2：手动处理成功；3：手动处理失败
	 */
	private Integer specialType;
	/**
	 * 查询创建时的开始时间
	 */
	private String cbeginTime;
	/**
	 * 查询创建时的结束时间
	 */
	private String cendTime;
	/**
	 * 查询预计到账时的开始时间
	 */
	private String ybeginTime;
	/**
	 * 查询预计到账时的结束时间
	 */
	private String yendTime;

	public AgentsDrawsList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AgentsDrawsList(Integer drawId, String agentName, String accUser, String iDnumber, String drawNo,
			String accNo, Integer agentId, Integer bankCardId, BigDecimal money, BigDecimal fee, BigDecimal ratio,
			BigDecimal taxratio, Integer cashSatus, Date getTime, Date createTime, String checkRemark,
			Integer checkStaffId, Date checkTime, Integer specialType, String cbeginTime, String cendTime,
			String ybeginTime, String yendTime) {
		super();
		this.drawId = drawId;
		this.agentName = agentName;
		this.accUser = accUser;
		IDnumber = iDnumber;
		this.drawNo = drawNo;
		this.accNo = accNo;
		this.agentId = agentId;
		this.bankCardId = bankCardId;
		this.money = money;
		this.fee = fee;
		this.ratio = ratio;
		this.taxratio = taxratio;
		this.cashSatus = cashSatus;
		this.getTime = getTime;
		this.createTime = createTime;
		this.checkRemark = checkRemark;
		this.checkStaffId = checkStaffId;
		this.checkTime = checkTime;
		this.specialType = specialType;
		this.cbeginTime = cbeginTime;
		this.cendTime = cendTime;
		this.ybeginTime = ybeginTime;
		this.yendTime = yendTime;
	}

	// 让number直接返回IDnumber，不需要set
	public String getNumber() {
		return IDnumber;
	}

	// public void setNumber(String number) {
	// this.number = number;
	// }

	public Integer getDrawId() {
		return drawId;
	}

	public void setDrawId(Integer drawId) {
		this.drawId = drawId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAccUser() {
		return accUser;
	}

	public void setAccUser(String accUser) {
		this.accUser = accUser;
	}

	public String getIDnumber() {
		return IDnumber;
	}

	public void setIDnumber(String iDnumber) {
		IDnumber = iDnumber;
	}

	public String getDrawNo() {
		return drawNo;
	}

	public void setDrawNo(String drawNo) {
		this.drawNo = drawNo;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public Integer getBankCardId() {
		return bankCardId;
	}

	public void setBankCardId(Integer bankCardId) {
		this.bankCardId = bankCardId;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public BigDecimal getRatio() {
		return ratio;
	}

	public void setRatio(BigDecimal ratio) {
		this.ratio = ratio;
	}

	public BigDecimal getTaxratio() {
		return taxratio;
	}

	public void setTaxratio(BigDecimal taxratio) {
		this.taxratio = taxratio;
	}

	public Integer getCashSatus() {
		return cashSatus;
	}

	public void setCashSatus(Integer cashSatus) {
		this.cashSatus = cashSatus;
	}

	public Date getGetTime() {
		return getTime;
	}

	public void setGetTime(Date getTime) {
		this.getTime = getTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCheckRemark() {
		return checkRemark;
	}

	public void setCheckRemark(String checkRemark) {
		this.checkRemark = checkRemark;
	}

	public Integer getCheckStaffId() {
		return checkStaffId;
	}

	public void setCheckStaffId(Integer checkStaffId) {
		this.checkStaffId = checkStaffId;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public Integer getSpecialType() {
		return specialType;
	}

	public void setSpecialType(Integer specialType) {
		this.specialType = specialType;
	}

	public String getCbeginTime() {
		return cbeginTime;
	}

	public void setCbeginTime(String cbeginTime) {
		this.cbeginTime = cbeginTime;
	}

	public String getCendTime() {
		return cendTime;
	}

	public void setCendTime(String cendTime) {
		this.cendTime = cendTime;
	}

	public String getYbeginTime() {
		return ybeginTime;
	}

	public void setYbeginTime(String ybeginTime) {
		this.ybeginTime = ybeginTime;
	}

	public String getYendTime() {
		return yendTime;
	}

	public void setYendTime(String yendTime) {
		this.yendTime = yendTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AgentsDrawsList [drawId=" + drawId + ", agentName=" + agentName + ", accUser=" + accUser + ", IDnumber="
				+ IDnumber + ", drawNo=" + drawNo + ", accNo=" + accNo + ", agentId=" + agentId + ", bankCardId="
				+ bankCardId + ", money=" + money + ", fee=" + fee + ", ratio=" + ratio + ", taxratio=" + taxratio
				+ ", cashSatus=" + cashSatus + ", getTime=" + getTime + ", createTime=" + createTime + ", checkRemark="
				+ checkRemark + ", checkStaffId=" + checkStaffId + ", checkTime=" + checkTime + ", specialType="
				+ specialType + ", cbeginTime=" + cbeginTime + ", cendTime=" + cendTime + ", ybeginTime=" + ybeginTime
				+ ", yendTime=" + yendTime + "]";
	}

}
