package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogKaiyuanTurnoverMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnover;
import com.stylefeng.guns.modular.huamao.service.IAppLogKaiyuanTurnoverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppLogKaiyuanTurnoverServiceImpl extends ServiceImpl<LogKaiyuanTurnoverMapper,LogKaiyuanTurnover> implements IAppLogKaiyuanTurnoverService {
    @Autowired
    private LogKaiyuanTurnoverMapper logKaiyuanTurnoverMapper;

    @Override
    public List<LogKaiyuanTurnover> huaBaoTurnoverList(Integer userId) {
        return logKaiyuanTurnoverMapper.huaBaoTurnoverList(userId);
    }
}
