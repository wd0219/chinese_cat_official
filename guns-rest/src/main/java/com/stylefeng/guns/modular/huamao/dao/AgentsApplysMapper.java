package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.AgentsApplys;
import com.stylefeng.guns.modular.huamao.model.AgentsApplysDTO;

/**
 * <p>
 * 代理公司股东申请表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-17
 */
public interface AgentsApplysMapper extends BaseMapper<AgentsApplys> {
	/**
	 * 查询列表
	 * @param agentsApplysDTO
	 * @return
	 */
	List<AgentsApplysDTO> showDaiLiShenQingInfo(AgentsApplysDTO agentsApplysDTO);
	/**
	 * 查询是否有该类型
	 * @param agentsApplys
	 * @return
	 */
	public AgentsApplys isSelectAgent(AgentsApplysDTO agentsApplys);
	/**
	 * 查找最大数订单
	 * @return
	 */
	public String seletMax();
	/**
	 * 回显示信息
	 * @param userId
	 * @return
	 */
	public Map<String, Object> selectAgentsApplysInfo(@Param(value="userId")Integer userId);

	List <AgentsApplys> getAgentsApplysSevenAgo(Map<String,Object> map);

    AgentsApplys selectAgentsApplysByUserId(Integer userId);

    AgentsApplys selectAgentsApplysByUserIdAndStatus(Map<String, Object> map);

    Map<String,Object> selectAgentsAppLysCheckApplyByUserId(Integer userId);
}
