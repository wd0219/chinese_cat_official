package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SpecCats;
import com.stylefeng.guns.modular.huamao.model.SpecCatsCustem;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品规格分类表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface SpecCatsMapper extends BaseMapper<SpecCats> {
	Integer insertSpecCats(SpecCatsCustem specCatsCustem);
	List<Map<String, Object>> selectAll(SpecCatsCustem specCatsCustem);
	SpecCatsCustem selectSpecCatsCustemById(int id);
}
