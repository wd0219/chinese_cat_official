package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsAttributesMapper;
import com.stylefeng.guns.modular.huamao.model.GoodsAttributes;
import com.stylefeng.guns.modular.huamao.service.IGoodsAttributesService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品属性表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class GoodsAttributesServiceImpl extends ServiceImpl<GoodsAttributesMapper, GoodsAttributes> implements IGoodsAttributesService {

}
