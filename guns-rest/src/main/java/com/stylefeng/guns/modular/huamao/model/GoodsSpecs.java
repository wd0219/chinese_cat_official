package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 商品规格表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_goods_specs")
public class GoodsSpecs extends Model<GoodsSpecs> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增Id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 店铺Id
     */
    private Integer shopId;
    /**
     * 商品Id
     */
    private Integer goodsId;
    /**
     * 商品货号
     */
    private String productNo;
    /**
     * 规格ID格式
     */
    private String specIds;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 商品价
     */
    private BigDecimal specPrice;
    /**
     * 库存
     */
    private Integer specStock;
    /**
     * 预警值
     */
    private Integer warnStock;
    /**
     * 销量
     */
    private Integer saleNum;
    /**
     * 默认规格
     */
    private Integer isDefault;
    /**
     * 有效状态
     */
    private Integer dataFlag;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getSpecIds() {
        return specIds;
    }

    public void setSpecIds(String specIds) {
        this.specIds = specIds;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getSpecPrice() {
        return specPrice;
    }

    public void setSpecPrice(BigDecimal specPrice) {
        this.specPrice = specPrice;
    }

    public Integer getSpecStock() {
        return specStock;
    }

    public void setSpecStock(Integer specStock) {
        this.specStock = specStock;
    }

    public Integer getWarnStock() {
        return warnStock;
    }

    public void setWarnStock(Integer warnStock) {
        this.warnStock = warnStock;
    }

    public Integer getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

	@Override
    public String toString() {
        return "GoodsSpecs{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", goodsId=" + goodsId +
        ", productNo=" + productNo +
        ", specIds=" + specIds +
        ", marketPrice=" + marketPrice +
        ", specPrice=" + specPrice +
        ", specStock=" + specStock +
        ", warnStock=" + warnStock +
        ", saleNum=" + saleNum +
        ", isDefault=" + isDefault +
        ", dataFlag=" + dataFlag +
        "}";
    }
}
