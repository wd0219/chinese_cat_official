package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersDrawsMaobiMapper;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsMaobi;
import com.stylefeng.guns.modular.huamao.service.IUsersDrawsMaobiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UsersDrawsMaobiServiceImpl extends ServiceImpl<UsersDrawsMaobiMapper,UsersDrawsMaobi> implements IUsersDrawsMaobiService {
    @Autowired
    private UsersDrawsMaobiMapper usersDrawsMaobiMapper;

    @Override
    public UsersDrawsMaobi selectSumMoneyByUserId(Map<String, Object> map) {
        return usersDrawsMaobiMapper.selectSumMoneyByUserId(map);
    }
}
