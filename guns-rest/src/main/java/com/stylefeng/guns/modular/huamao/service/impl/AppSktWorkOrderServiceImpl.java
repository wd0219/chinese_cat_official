package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktWorkorderMapper;
import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;
import com.stylefeng.guns.modular.huamao.service.IAppSktWorkOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppSktWorkOrderServiceImpl extends ServiceImpl<SktWorkorderMapper,SktWorkorder> implements IAppSktWorkOrderService {
    @Autowired
    private SktWorkorderMapper sktWorkorderMapper;

    @Override
    public SktWorkorder selectByOrderNo(Map<String, Object> orderWhere) {
        return sktWorkorderMapper.selectByOrderNo(orderWhere);
    }

	@Override
	public List<Map<String, Object>> selectRecord() {
		List<Map<String,Object>> list = sktWorkorderMapper.selectRecord();
		return list;
	}
}
