package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.model.UsersUpgradeDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户升级角色订单表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
public interface IUsersUpgradeService extends IService<UsersUpgrade> {
	/**
	 * 显示用户升级列表
	 * @param usersUpgradeDTO
	 * @return
	 */
	public List<UsersUpgradeDTO> showUsersShengJiInfo(UsersUpgradeDTO usersUpgradeDTO);
	/**
	 * 更新升级状态
	 * @param orderId
	 * @param status
	 * @param preRole
	 * @param afterRole
	 */
	public String updateUpgradeStatus(Integer orderId, Integer status, Integer preRole, Integer afterRole);

	/**
	 * 用户升级新增记录
	 * @param usersUpgrade
	 * @return
	 */
	public String addUserUpgradeInfo(UsersUpgrade usersUpgrade);

	/**
	 * 根据用户id查询最大的升级订单id  用于生成订单后返回前台
	 * @param userId
	 * @return
	 */
	public  Integer selectMaxOrderIdByUserId(Integer userId);

    void insertOptBing(Map<Object, Object> map);

	UsersUpgrade selectUsersUpgradeByUserIdOrderNo(Map<Object, Object> map);

	void updateByOrderId(UsersUpgrade usersUpgrade1);
}
