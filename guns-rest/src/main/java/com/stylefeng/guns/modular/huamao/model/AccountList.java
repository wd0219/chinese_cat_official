package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员账户返回
 */

public class AccountList  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer userId; 
	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 用户昵称
	 */
	private String loginName;
	/**
	 * 用户类型
	 */
	private Integer userType;
	/**
	 * 积分
	 */
	private BigDecimal score;
	/**
	 * 待发积分
	 */
	private BigDecimal freezeScore;
	/**
	 * 累计获得积分
	 */
	private BigDecimal totalScore;
	/**
	 * 开元宝
	 */
	private BigDecimal kaiyuan;
	/**
	 * 累计获得开元宝
	 */
	private BigDecimal totalKaiyuan;
	/**
	 * 现金
	 */
	private BigDecimal cash;
	/**
	 * 待发现金
	 */
	private BigDecimal freezeCash;
	/**
	 * 累计获得现金
	 */
	private BigDecimal totalCash;
	/**
	 * 线上消费额(商城消费金额)
	 */
	private BigDecimal onlineExpenditure;
	/**
	 * 线下消费额(商家补单 消费者主动下单 面对面付)
	 */
	private BigDecimal offlineExpenditure;
	/**
	 * 用户发展的主管或经理数量
	 */
	private Integer inviteNum;
	/**
	 * 特别奖励发展1个商家奖励的积分
	 */
	private BigDecimal one;
	/**
	 * 特别奖励发展2个商家奖励的积分
	 */
	private BigDecimal two;
	/**
	 * 特别奖励发展3个商家奖励的积分
	 */
	private BigDecimal three;
	/**
	 * 特别奖励发展4个商家奖励的积分
	 */
	private BigDecimal four;
	/**
	 * 特别奖励发展5个及其以上商家奖励的积分
	 */
	private BigDecimal five;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;
	private BigDecimal sumScoreSpecial;
	private Integer isStore;
	private Integer isShop;
	private Integer isAgent;
	private Integer auditStatus;
	private Integer upgradeStatus;
	private BigDecimal shoppingMoney;

	public BigDecimal getShoppingMoney() {
		return shoppingMoney;
	}

	public void setShoppingMoney(BigDecimal shoppingMoney) {
		this.shoppingMoney = shoppingMoney;
	}

	public Integer getUpgradeStatus() {
		return upgradeStatus;
	}

	public void setUpgradeStatus(Integer upgradeStatus) {
		this.upgradeStatus = upgradeStatus;
	}

	public Integer getIsStore() {
		return isStore;
	}

	public void setIsStore(Integer isStore) {
		this.isStore = isStore;
	}

	public Integer getIsShop() {
		return isShop;
	}

	public void setIsShop(Integer isShop) {
		this.isShop = isShop;
	}

	public Integer getIsAgent() {
		return isAgent;
	}

	public void setIsAgent(Integer isAgent) {
		this.isAgent = isAgent;
	}

	public Integer getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}

	public BigDecimal getSumScoreSpecial() {
		return sumScoreSpecial;
	}

	public void setSumScoreSpecial(BigDecimal sumScoreSpecial) {
		this.sumScoreSpecial = sumScoreSpecial;
	}

	public AccountList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountList(String userPhone, String loginName, Integer userType, BigDecimal score, BigDecimal freezeScore,
			BigDecimal totalScore, BigDecimal kaiyuan, BigDecimal totalKaiyuan, BigDecimal cash, BigDecimal freezeCash,
			BigDecimal totalCash, BigDecimal onlineExpenditure, BigDecimal offlineExpenditure, Integer inviteNum,
			BigDecimal one, BigDecimal two, BigDecimal three, BigDecimal four, BigDecimal five, String beginTime,
			String endTime) {
		super();
		this.userPhone = userPhone;
		this.loginName = loginName;
		this.userType = userType;
		this.score = score;
		this.freezeScore = freezeScore;
		this.totalScore = totalScore;
		this.kaiyuan = kaiyuan;
		this.totalKaiyuan = totalKaiyuan;
		this.cash = cash;
		this.freezeCash = freezeCash;
		this.totalCash = totalCash;
		this.onlineExpenditure = onlineExpenditure;
		this.offlineExpenditure = offlineExpenditure;
		this.inviteNum = inviteNum;
		this.one = one;
		this.two = two;
		this.three = three;
		this.four = four;
		this.five = five;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public BigDecimal getFreezeScore() {
		return freezeScore;
	}

	public void setFreezeScore(BigDecimal freezeScore) {
		this.freezeScore = freezeScore;
	}

	public BigDecimal getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(BigDecimal totalScore) {
		this.totalScore = totalScore;
	}

	public BigDecimal getKaiyuan() {
		return kaiyuan;
	}

	public void setKaiyuan(BigDecimal kaiyuan) {
		this.kaiyuan = kaiyuan;
	}

	public BigDecimal getTotalKaiyuan() {
		return totalKaiyuan;
	}

	public void setTotalKaiyuan(BigDecimal totalKaiyuan) {
		this.totalKaiyuan = totalKaiyuan;
	}

	public BigDecimal getCash() {
		return cash;
	}

	public void setCash(BigDecimal cash) {
		this.cash = cash;
	}

	public BigDecimal getFreezeCash() {
		return freezeCash;
	}

	public void setFreezeCash(BigDecimal freezeCash) {
		this.freezeCash = freezeCash;
	}

	public BigDecimal getTotalCash() {
		return totalCash;
	}

	public void setTotalCash(BigDecimal totalCash) {
		this.totalCash = totalCash;
	}

	public BigDecimal getOnlineExpenditure() {
		return onlineExpenditure;
	}

	public void setOnlineExpenditure(BigDecimal onlineExpenditure) {
		this.onlineExpenditure = onlineExpenditure;
	}

	public BigDecimal getOfflineExpenditure() {
		return offlineExpenditure;
	}

	public void setOfflineExpenditure(BigDecimal offlineExpenditure) {
		this.offlineExpenditure = offlineExpenditure;
	}

	public Integer getInviteNum() {
		return inviteNum;
	}

	public void setInviteNum(Integer inviteNum) {
		this.inviteNum = inviteNum;
	}

	public BigDecimal getOne() {
		return one;
	}

	public void setOne(BigDecimal one) {
		this.one = one;
	}

	public BigDecimal getTwo() {
		return two;
	}

	public void setTwo(BigDecimal two) {
		this.two = two;
	}

	public BigDecimal getThree() {
		return three;
	}

	public void setThree(BigDecimal three) {
		this.three = three;
	}

	public BigDecimal getFour() {
		return four;
	}

	public void setFour(BigDecimal four) {
		this.four = four;
	}

	public BigDecimal getFive() {
		return five;
	}

	public void setFive(BigDecimal five) {
		this.five = five;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AccountList [userPhone=" + userPhone + ", loginName=" + loginName + ", userType=" + userType
				+ ", score=" + score + ", freezeScore=" + freezeScore + ", totalScore=" + totalScore + ", kaiyuan="
				+ kaiyuan + ", totalKaiyuan=" + totalKaiyuan + ", cash=" + cash + ", freezeCash=" + freezeCash
				+ ", totalCash=" + totalCash + ", onlineExpenditure=" + onlineExpenditure + ", offlineExpenditure="
				+ offlineExpenditure + ", inviteNum=" + inviteNum + ", one=" + one + ", two=" + two + ", three=" + three
				+ ", four=" + four + ", five=" + five + ", beginTime=" + beginTime + ", endTime=" + endTime + "]";
	}

}
