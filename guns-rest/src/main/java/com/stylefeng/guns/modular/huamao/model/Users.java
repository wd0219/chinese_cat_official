package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author slt123
 * @since 2018-04-10
 */
@TableName("skt_users")
public class Users extends Model<Users> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "userId", type = IdType.AUTO)
    private Integer userId;
    /**
     * 手机
     */
    private String userPhone;
    /**
     * 登录名 昵称
     */
    private String loginName;
    /**
     * 安全码
     */
    private Integer loginSecret;
    /**
     * 密码
     */
    private String loginPwd;
    /**
     * 支付密码
     */
    private String payPwd;
    /**
     * 用户类型 0普通 1主管 2经理
     */
    private Integer userType;
    /**
     * 线下商家标志 1是 0否
     */
    private Integer isStore;
    /**
     * 线上商家标志 1是 0否
     */
    private Integer isShop;
    /**
     * 代理公司股东标志:0不是股东 1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
     */
    private Integer isAgent;
    /**
     * 真实姓名
     */
    private String trueName;
    /**
     * 推荐人id
     */
    private Integer inviteId;
    /**
     * 会员头像
     */
    private String userPhoto;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 注册来源：1pc 2H5 3ios 4 android
     */
    private Integer regTerminal;
    /**
     * 账号状态：0停用 1启用
     */
    private Integer userStatus;
    /**
     * 特权账号 0不是 1是
     */
    private Integer isPrivilege;
    /**
     * 成为特权账号的时间
     */
    private Date privilegeTime;

    /**
     * 是否允许提现
     */
    private Integer isWithdrawCash;
    /**
     * 是否允许操作 赎回、转化、购物、获取积分
     */
    private Integer isOperation;

    public Integer getIsOperation() {
        return isOperation;
    }

    public void setIsOperation(Integer isOperation) {
        this.isOperation = isOperation;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public Integer getLoginSecret() {
        return loginSecret;
    }

    public void setLoginSecret(Integer loginSecret) {
        this.loginSecret = loginSecret;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getIsStore() {
        return isStore;
    }

    public void setIsStore(Integer isStore) {
        this.isStore = isStore;
    }

    public Integer getIsShop() {
        return isShop;
    }

    public void setIsShop(Integer isShop) {
        this.isShop = isShop;
    }

    public Integer getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Integer isAgent) {
        this.isAgent = isAgent;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public Integer getInviteId() {
        return inviteId;
    }

    public void setInviteId(Integer inviteId) {
        this.inviteId = inviteId;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getRegTerminal() {
        return regTerminal;
    }

    public void setRegTerminal(Integer regTerminal) {
        this.regTerminal = regTerminal;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getIsPrivilege() {
        return isPrivilege;
    }

    public void setIsPrivilege(Integer isPrivilege) {
        this.isPrivilege = isPrivilege;
    }

    public Date getPrivilegeTime() {
        return privilegeTime;
    }

    public void setPrivilegeTime(Date privilegeTime) {
        this.privilegeTime = privilegeTime;
    }

    public Integer getIsWithdrawCash() {
        return isWithdrawCash;
    }

    public void setIsWithdrawCash(Integer isWithdrawCash) {
        this.isWithdrawCash = isWithdrawCash;
    }

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", userPhone='" + userPhone + '\'' +
                ", loginName='" + loginName + '\'' +
                ", loginSecret=" + loginSecret +
                ", loginPwd='" + loginPwd + '\'' +
                ", payPwd='" + payPwd + '\'' +
                ", userType=" + userType +
                ", isStore=" + isStore +
                ", isShop=" + isShop +
                ", isAgent=" + isAgent +
                ", trueName='" + trueName + '\'' +
                ", inviteId=" + inviteId +
                ", userPhoto='" + userPhoto + '\'' +
                ", createTime=" + createTime +
                ", regTerminal=" + regTerminal +
                ", userStatus=" + userStatus +
                ", isPrivilege=" + isPrivilege +
                ", privilegeTime=" + privilegeTime +
                ", isWithdrawCash=" + isWithdrawCash +
                ", isOperation=" + isOperation +
                '}';
    }
}
