package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.model.UsersAddressDTO;

import java.util.List;

/**
 * <p>
 * 会员地址表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
public interface IUsersAddressService extends IService<UsersAddress> {
	/**
	 * 查询用户地址列表
	 * @return
	 */
	public List<UsersAddressDTO> showUserAddressInfo(UsersAddressDTO usersAddressDTO);
}
