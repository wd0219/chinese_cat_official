package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopIndustryMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;
import com.stylefeng.guns.modular.huamao.service.ISktShopIndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 行业管理表 服务实现类
 * </p>
 *
 * @author ck123123
 * @since 2018-04-19
 */
@Service
public class SktShopIndustryServiceImpl extends ServiceImpl<SktShopIndustryMapper, SktShopIndustry> implements ISktShopIndustryService {
	@Autowired
	private SktShopIndustryMapper shopIndustryMapper;
	@Override
	public List<SktShopIndustry> shopIndustryfindinfo(SktShopIndustry shopIndustry) {
		List<SktShopIndustry> list = shopIndustryMapper.shopIndustryfindinfo(shopIndustry);
		return list;
	}

}
