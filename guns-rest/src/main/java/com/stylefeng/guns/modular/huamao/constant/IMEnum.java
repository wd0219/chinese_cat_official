package com.stylefeng.guns.modular.huamao.constant;

public enum IMEnum {

    CREATE("https://api.netease.im/nimserver/user/create.action");


    private String url;

    private IMEnum(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
    @Override
    public String toString() {
        return this.url;
    }
}
