package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopApplysMapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopApproveMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;
import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.service.ISktShopApproveService;

/**
 * <p>
 * 企业认证表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@Service
public class SktShopApproveServiceImpl extends ServiceImpl<SktShopApproveMapper, SktShopApprove> implements ISktShopApproveService {
	@Autowired
	private SktShopApproveMapper sktShopApproveMapper;
	@Autowired
	private SktShopApplysMapper sktShopApplysMapper;
	@Autowired
	private UsersMapper usersMapper;
	@Override
	@Transactional
	public String insertApprove(SktShopApprove sktShopApprove) {

		//查询有没有营业执照
		SktShopApprove sktShopApprove1 = new SktShopApprove();
		sktShopApprove1.setLicenseNo(sktShopApprove.getLicenseNo());
		sktShopApprove1.setShopStatus(1);
		EntityWrapper<SktShopApprove> ew = new EntityWrapper<SktShopApprove>(sktShopApprove1);

		List<SktShopApprove> list = sktShopApproveMapper.selectList(ew);
		if(list.size()>0){//判断营业执照是否重复
			return "2";
		}

        //线上商家是否申请
        Map<String,Object> map1 = new HashMap<>();
        map1 = selectApply(sktShopApprove);
        if(map1.size()>0 && "1".equals(map1.get("code"))){
            return "5";
        }

		//查询该用户是否申请
        Map<String,Object> map = new HashMap<>();
		map = selectShopAppr(sktShopApprove.getUserId());
		if (map.size() > 0 && "1".equals(map.get("code").toString())){
			return "3";//已经申请成功
		}else if(map.size() > 0 && "0".equals(map.get("code").toString())){
			return  "4"; //未审核状态
		}

		//SktShopApprove sktShopApprove2 = sktShopApproveMapper.selectShopApproveByUserId2(sktShopApprove.getUserId());
		SktShopApprove sktShopApprove2 = new SktShopApprove();
		if(!MSGUtil.isBlank(map.get("result"))){//获取拒绝的列表 方便修改 最新状态
			 List<SktShopApprove> listAppr =  (List<SktShopApprove>)map.get("result");
			 sktShopApprove2 = listAppr.get(0);
		}

		Integer insertUpdateSktShop2 = 0;
		if (MSGUtil.isBlank(sktShopApprove2.getApplyId())){

			sktShopApprove.setShopStatus(0);
			sktShopApprove.setCreateTime(new Date());

			insertUpdateSktShop2 = sktShopApproveMapper.insert(sktShopApprove);
		}else{
			//添加前台值
			sktShopApprove.setApplyId(sktShopApprove2.getApplyId());
			sktShopApprove.setShopStatus(0);
			sktShopApprove.setCheckStaffId(null);
			sktShopApprove.setCheckTime(null);

			insertUpdateSktShop2 = sktShopApproveMapper.updateById(sktShopApprove);
		}

//		Integer insert = sktShopApproveMapper.insert(sktShopApprove);
//		SktShopApplys  sktShopApplys = new SktShopApplys();
//		sktShopApplys.setUserId(sktShopApplys.getUserId());
//		sktShopApplys.setApplyStatus(1);
//		sktShopApplys.setApplyDesc(" ");
//		sktShopApplys.setCreateTime(new Date());
//		Integer insert2 = sktShopApplysMapper.insert(sktShopApplys);
//		if(insert==0 && insert2==0){
//			return "0";
//		}
		if(insertUpdateSktShop2.intValue() == 0){
			return "0";
		}
		return "1";
	}

    /**
     * 查找联盟商家
     * @param userId
     * @return
     */
	public Map<String,Object> selectShopAppr(Integer userId){
		Map<String,Object> map = new HashMap<>();
		Map<String,Object> mapRet = new HashMap<>();
		map.put("userId",userId);
		map.put("shopStatus",1);
		List<SktShopApprove> list1 = sktShopApproveMapper.selectShopApproveByUserId3(map);
		if(list1.size() > 0){
			mapRet.put("code","1");
			mapRet.put("result", list1);
			return mapRet;
		}
		map.put("shopStatus",0);
		List<SktShopApprove> list2 = sktShopApproveMapper.selectShopApproveByUserId3(map);
		if(list2.size() > 0){
			mapRet.put("code","0");
			mapRet.put("result", list2);
			return mapRet;
		}
		map.put("shopStatus",-1);
		List<SktShopApprove> list3 = sktShopApproveMapper.selectShopApproveByUserId3(map);
		if(list3.size() > 0){
			mapRet.put("code","-1");
			mapRet.put("result", list3);
		}
		return mapRet;
	}

    /**
     * 查询线上商城
     * @param sktShopApprove
     * @return
     */
    public Map<String,Object> selectApply(SktShopApprove sktShopApprove){
        Map<String,Object> mapRet = new HashMap<>();
        Map<String,Object> map = new HashMap<>();
        map.put("userId",sktShopApprove.getUserId());
        map.put("applyStatus",3);//通过
        List<SktShopApplys> list = sktShopApplysMapper.selectShopApplysByUserId3(map);
        if(list.size() > 0){
            mapRet.put("code","3");
            mapRet.put("result",list);
            return mapRet;
        }
        map.put("applyStatus",2);//拒绝
        List<SktShopApplys> list2 = sktShopApplysMapper.selectShopApplysByUserId3(map);
        if(list2.size() > 0){
            mapRet.put("code","2");
            mapRet.put("result",list2);
            return mapRet;
        }

        map.put("applyStatus",1);//未审核
        List<SktShopApplys> list3 = sktShopApplysMapper.selectShopApplysByUserId3(map);
        if(list3.size() > 0){
            mapRet.put("code","1");
            mapRet.put("result",list3);
        }
        return mapRet;

    }

}
