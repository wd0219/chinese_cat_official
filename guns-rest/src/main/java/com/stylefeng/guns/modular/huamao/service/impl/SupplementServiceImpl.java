package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SupplementMapper;
import com.stylefeng.guns.modular.huamao.model.SupplementList;
import com.stylefeng.guns.modular.huamao.service.ISupplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 手动补单 服务实现类
 * 
 * @author gxz
 *
 */
@Service
public class SupplementServiceImpl extends ServiceImpl<SupplementMapper, SupplementList> implements ISupplementService {

	@Autowired
	private SupplementMapper sm;

	// 校验用户名或手机号是否存在
	@Override
	public SupplementList checkUser(SupplementList supplementList) {
		return sm.checkUser(supplementList);
	}

	// 添加记录
	@Override
	public void add(SupplementList supplementList) {
		Boolean b = sm.add(supplementList);
	}

}
