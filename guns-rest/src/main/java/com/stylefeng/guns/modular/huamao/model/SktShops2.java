package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 商家信息表
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
@TableName("skt_shops")
public class SktShops2 extends Model<SktShops> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "shopId", type = IdType.AUTO)
    private Integer shopId;
    
    private String loginName;
    
    private String shopName;
    
    private String logo;
    
    private Integer freight;
    
    private Integer areaId2;
    
    private Integer brandId;
    
    private Integer catId;
    
    private String brandName;
    
    private String brandImg;
    
    private String brandDesc;
    
    private String goodsName;
    
    private String goodsImg;
    
    private Integer shopPrice1;
    private Integer shopPrice2;
    private String createTime;
    private Integer saleNum;
    private Integer visitNum;
    private Integer appraiseNum;
    private Integer saleTime;
    public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}

	public Integer getVisitNum() {
		return visitNum;
	}

	public void setVisitNum(Integer visitNum) {
		this.visitNum = visitNum;
	}

	public Integer getAppraiseNum() {
		return appraiseNum;
	}

	public void setAppraiseNum(Integer appraiseNum) {
		this.appraiseNum = appraiseNum;
	}

	public Integer getSaleTime() {
		return saleTime;
	}

	public void setSaleTime(Integer saleTime) {
		this.saleTime = saleTime;
	}

	public Integer getSsaleTime() {
		return ssaleTime;
	}

	public void setSsaleTime(Integer ssaleTime) {
		this.ssaleTime = ssaleTime;
	}

	private Integer ssaleTime;
    
    public Integer getShopPrice1() {
		return shopPrice1;
	}

	public void setShopPrice1(Integer shopPrice1) {
		this.shopPrice1 = shopPrice1;
	}

	public Integer getShopPrice2() {
		return shopPrice2;
	}

	public void setShopPrice2(Integer shopPrice2) {
		this.shopPrice2 = shopPrice2;
	}

	private BigDecimal shopPrice;

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public Integer getCatId() {
		return catId;
	}

	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandImg() {
		return brandImg;
	}

	public void setBrandImg(String brandImg) {
		this.brandImg = brandImg;
	}

	public String getBrandDesc() {
		return brandDesc;
	}

	public void setBrandDesc(String brandDesc) {
		this.brandDesc = brandDesc;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsImg() {
		return goodsImg;
	}

	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}

	public BigDecimal getShopPrice() {
		return shopPrice;
	}

	public void setShopPrice(BigDecimal shopPrice) {
		this.shopPrice = shopPrice;
	}

	public Integer getFreight() {
		return freight;
	}

	public void setFreight(Integer freight) {
		this.freight = freight;
	}

	public Integer getAreaId2() {
		return areaId2;
	}

	public void setAreaId2(Integer areaId2) {
		this.areaId2 = areaId2;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return "SktShops2 [shopId=" + shopId + ", loginName=" + loginName + ", shopName=" + shopName + ", logo=" + logo
				+ ", freight=" + freight + ", areaId2=" + areaId2 + ", brandId=" + brandId + ", catId=" + catId
				+ ", brandName=" + brandName + ", brandImg=" + brandImg + ", brandDesc=" + brandDesc + ", goodsName="
				+ goodsName + ", goodsImg=" + goodsImg + ", shopPrice=" + shopPrice + "]";
	}

	public SktShops2(Integer shopId, String loginName, String shopName, String logo, Integer freight, Integer areaId2,
			Integer brandId, Integer catId, String brandName, String brandImg, String brandDesc, String goodsName,
			String goodsImg, BigDecimal shopPrice) {
		super();
		this.shopId = shopId;
		this.loginName = loginName;
		this.shopName = shopName;
		this.logo = logo;
		this.freight = freight;
		this.areaId2 = areaId2;
		this.brandId = brandId;
		this.catId = catId;
		this.brandName = brandName;
		this.brandImg = brandImg;
		this.brandDesc = brandDesc;
		this.goodsName = goodsName;
		this.goodsImg = goodsImg;
		this.shopPrice = shopPrice;
	}

	public SktShops2() {
		super();
		// TODO Auto-generated constructor stub
	}



    
}
