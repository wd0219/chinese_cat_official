package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsMapper;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.service.IAppAgentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppAgentsServiceImpl extends ServiceImpl<SktAgentsMapper,SktAgents> implements IAppAgentsService {
    @Autowired
    private SktAgentsMapper sktAgentsMapper;


    @Override
    public SktAgents selectAgentsProvinceId(Map<String, Object> map) {
        return sktAgentsMapper.selectAgentsProvinceId(map);
    }

    @Override
    public SktAgents selectAgentsCityId(Map<String, Object> map) {
        return sktAgentsMapper.selectAgentsCityId(map);
    }

    @Override
    public SktAgents selectAgentsAreaId(Map<String, Object> map) {
        return sktAgentsMapper.selectAgentsAreaId(map);
    }
}
