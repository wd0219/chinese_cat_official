package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktClassType;

import java.util.List;

/**
 * <p>
 * 视频类型表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface ISktClassTypeService extends IService<SktClassType> {

    List<SktClassType> selectAllType();
}
