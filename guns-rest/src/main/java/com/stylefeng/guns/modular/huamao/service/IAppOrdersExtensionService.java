package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrdersExtension;

import java.util.Map;

public interface IAppOrdersExtensionService extends IService<SktOrdersExtension> {
    void updateOrdersExtensionByOrderId(Map<String, Object> maps);
}
