package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户每日对账返回
 * 
 * @author gxz
 *
 */
public class CheckDetailList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 用户名
	 */
	private String loginName;
	/**
	 * 账户明细ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 日期
	 */
	@TableField("check_date")
	private Integer checkDate;
	/**
	 * 资金对账差异
	 */
	@TableField("zj_checkDiff")
	private BigDecimal zjCheckdiff;
	/**
	 * 积分对账差异
	 */
	@TableField("jf_checkDiff")
	private BigDecimal jfCheckdiff;
	/**
	 * 开元宝对账差异
	 */
	@TableField("kyb_checkDiff")
	private BigDecimal kybCheckdiff;
	/**
	 * 昨日资金
	 */
	@TableField("zj_yesterday")
	private BigDecimal zjYesterday;
	/**
	 * 今日资金
	 */
	@TableField("zj_today")
	private BigDecimal zjToday;
	/**
	 * 今日业务增量
	 */
	@TableField("zj_change")
	private BigDecimal zjChange;
	/**
	 * 门店订单支付金额
	 */
	@TableField("zj_storePay")
	private BigDecimal zjStorepay;
	/**
	 * 商城订单支付金额
	 */
	@TableField("zj_mallPay")
	private BigDecimal zjMallpay;
	/**
	 * 提现申请
	 */
	private BigDecimal zjTixianapply;
	/**
	 * 提现手续费
	 */
	private BigDecimal zjTixianfee;
	/**
	 * 提现不通过
	 */
	private BigDecimal zjTixiannopass;
	/**
	 * 退款
	 */
	private BigDecimal zjTuikuan;
	/**
	 * 充值
	 */
	private BigDecimal zjChongzhi;
	/**
	 * 昨日积分
	 */
	private BigDecimal jfYesterday;
	/**
	 * 今日积分
	 */
	private BigDecimal jfToday;
	/**
	 * 今日业务增量
	 */
	private BigDecimal jfChange;
	/**
	 * 积分转换
	 */
	private BigDecimal jfZhuanhuan;
	/**
	 * 门店消费获得
	 */
	private BigDecimal jfStorepay;
	/**
	 * 商城消费获得
	 */
	private BigDecimal jfMallpay;
	/**
	 * 邀请奖励
	 */
	private BigDecimal jfInvitereward;
	/**
	 * 其它
	 */
	private BigDecimal jfOther;
	/**
	 * 昨日开元宝
	 */
	private BigDecimal kybYesterday;
	/**
	 * 今日开元宝
	 */
	private BigDecimal kybToday;
	/**
	 * 今日业务增量
	 */
	private BigDecimal kybChange;
	/**
	 * 积分转换
	 */
	private BigDecimal kybZhuanhuan;
	/**
	 * 门店支付
	 */
	private BigDecimal kybStorepay;
	/**
	 * 门店支付手续费
	 */
	private BigDecimal kybStorepayfee;
	/**
	 * 商城支付
	 */
	private BigDecimal kybMallpay;
	/**
	 * 商城支付手续费
	 */
	private BigDecimal kybMallpayfee;
	/**
	 * 提现
	 */
	private BigDecimal kybTixianapply;
	/**
	 * 提现手续费
	 */
	private BigDecimal kybTixianfee;
	/**
	 * 提现不通过
	 */
	private BigDecimal kybTixiannopass;
	/**
	 * 开元宝退款
	 */

	private BigDecimal kybTuikuan;
	/**
	 * 其它
	 */
	private BigDecimal kybOther;
	/**
	 * 查询时的开始时间
	 */
	private Integer beginTime;
	/**
	 * 查询时的结束时间
	 */
	private Integer endTime;

	public CheckDetailList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CheckDetailList(String loginName, Integer id, Integer userId, Integer checkDate, BigDecimal zjCheckdiff,
			BigDecimal jfCheckdiff, BigDecimal kybCheckdiff, BigDecimal zjYesterday, BigDecimal zjToday,
			BigDecimal zjChange, BigDecimal zjStorepay, BigDecimal zjMallpay, BigDecimal zjTixianapply,
			BigDecimal zjTixianfee, BigDecimal zjTixiannopass, BigDecimal zjTuikuan, BigDecimal zjChongzhi,
			BigDecimal jfYesterday, BigDecimal jfToday, BigDecimal jfChange, BigDecimal jfZhuanhuan,
			BigDecimal jfStorepay, BigDecimal jfMallpay, BigDecimal jfInvitereward, BigDecimal jfOther,
			BigDecimal kybYesterday, BigDecimal kybToday, BigDecimal kybChange, BigDecimal kybZhuanhuan,
			BigDecimal kybStorepay, BigDecimal kybStorepayfee, BigDecimal kybMallpay, BigDecimal kybMallpayfee,
			BigDecimal kybTixianapply, BigDecimal kybTixianfee, BigDecimal kybTixiannopass, BigDecimal kybTuikuan,
			BigDecimal kybOther, Integer beginTime, Integer endTime) {
		super();
		this.loginName = loginName;
		this.id = id;
		this.userId = userId;
		this.checkDate = checkDate;
		this.zjCheckdiff = zjCheckdiff;
		this.jfCheckdiff = jfCheckdiff;
		this.kybCheckdiff = kybCheckdiff;
		this.zjYesterday = zjYesterday;
		this.zjToday = zjToday;
		this.zjChange = zjChange;
		this.zjStorepay = zjStorepay;
		this.zjMallpay = zjMallpay;
		this.zjTixianapply = zjTixianapply;
		this.zjTixianfee = zjTixianfee;
		this.zjTixiannopass = zjTixiannopass;
		this.zjTuikuan = zjTuikuan;
		this.zjChongzhi = zjChongzhi;
		this.jfYesterday = jfYesterday;
		this.jfToday = jfToday;
		this.jfChange = jfChange;
		this.jfZhuanhuan = jfZhuanhuan;
		this.jfStorepay = jfStorepay;
		this.jfMallpay = jfMallpay;
		this.jfInvitereward = jfInvitereward;
		this.jfOther = jfOther;
		this.kybYesterday = kybYesterday;
		this.kybToday = kybToday;
		this.kybChange = kybChange;
		this.kybZhuanhuan = kybZhuanhuan;
		this.kybStorepay = kybStorepay;
		this.kybStorepayfee = kybStorepayfee;
		this.kybMallpay = kybMallpay;
		this.kybMallpayfee = kybMallpayfee;
		this.kybTixianapply = kybTixianapply;
		this.kybTixianfee = kybTixianfee;
		this.kybTixiannopass = kybTixiannopass;
		this.kybTuikuan = kybTuikuan;
		this.kybOther = kybOther;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Integer checkDate) {
		this.checkDate = checkDate;
	}

	public BigDecimal getZjCheckdiff() {
		return zjCheckdiff;
	}

	public void setZjCheckdiff(BigDecimal zjCheckdiff) {
		this.zjCheckdiff = zjCheckdiff;
	}

	public BigDecimal getJfCheckdiff() {
		return jfCheckdiff;
	}

	public void setJfCheckdiff(BigDecimal jfCheckdiff) {
		this.jfCheckdiff = jfCheckdiff;
	}

	public BigDecimal getKybCheckdiff() {
		return kybCheckdiff;
	}

	public void setKybCheckdiff(BigDecimal kybCheckdiff) {
		this.kybCheckdiff = kybCheckdiff;
	}

	public BigDecimal getZjYesterday() {
		return zjYesterday;
	}

	public void setZjYesterday(BigDecimal zjYesterday) {
		this.zjYesterday = zjYesterday;
	}

	public BigDecimal getZjToday() {
		return zjToday;
	}

	public void setZjToday(BigDecimal zjToday) {
		this.zjToday = zjToday;
	}

	public BigDecimal getZjChange() {
		return zjChange;
	}

	public void setZjChange(BigDecimal zjChange) {
		this.zjChange = zjChange;
	}

	public BigDecimal getZjStorepay() {
		return zjStorepay;
	}

	public void setZjStorepay(BigDecimal zjStorepay) {
		this.zjStorepay = zjStorepay;
	}

	public BigDecimal getZjMallpay() {
		return zjMallpay;
	}

	public void setZjMallpay(BigDecimal zjMallpay) {
		this.zjMallpay = zjMallpay;
	}

	public BigDecimal getZjTixianapply() {
		return zjTixianapply;
	}

	public void setZjTixianapply(BigDecimal zjTixianapply) {
		this.zjTixianapply = zjTixianapply;
	}

	public BigDecimal getZjTixianfee() {
		return zjTixianfee;
	}

	public void setZjTixianfee(BigDecimal zjTixianfee) {
		this.zjTixianfee = zjTixianfee;
	}

	public BigDecimal getZjTixiannopass() {
		return zjTixiannopass;
	}

	public void setZjTixiannopass(BigDecimal zjTixiannopass) {
		this.zjTixiannopass = zjTixiannopass;
	}

	public BigDecimal getZjTuikuan() {
		return zjTuikuan;
	}

	public void setZjTuikuan(BigDecimal zjTuikuan) {
		this.zjTuikuan = zjTuikuan;
	}

	public BigDecimal getZjChongzhi() {
		return zjChongzhi;
	}

	public void setZjChongzhi(BigDecimal zjChongzhi) {
		this.zjChongzhi = zjChongzhi;
	}

	public BigDecimal getJfYesterday() {
		return jfYesterday;
	}

	public void setJfYesterday(BigDecimal jfYesterday) {
		this.jfYesterday = jfYesterday;
	}

	public BigDecimal getJfToday() {
		return jfToday;
	}

	public void setJfToday(BigDecimal jfToday) {
		this.jfToday = jfToday;
	}

	public BigDecimal getJfChange() {
		return jfChange;
	}

	public void setJfChange(BigDecimal jfChange) {
		this.jfChange = jfChange;
	}

	public BigDecimal getJfZhuanhuan() {
		return jfZhuanhuan;
	}

	public void setJfZhuanhuan(BigDecimal jfZhuanhuan) {
		this.jfZhuanhuan = jfZhuanhuan;
	}

	public BigDecimal getJfStorepay() {
		return jfStorepay;
	}

	public void setJfStorepay(BigDecimal jfStorepay) {
		this.jfStorepay = jfStorepay;
	}

	public BigDecimal getJfMallpay() {
		return jfMallpay;
	}

	public void setJfMallpay(BigDecimal jfMallpay) {
		this.jfMallpay = jfMallpay;
	}

	public BigDecimal getJfInvitereward() {
		return jfInvitereward;
	}

	public void setJfInvitereward(BigDecimal jfInvitereward) {
		this.jfInvitereward = jfInvitereward;
	}

	public BigDecimal getJfOther() {
		return jfOther;
	}

	public void setJfOther(BigDecimal jfOther) {
		this.jfOther = jfOther;
	}

	public BigDecimal getKybYesterday() {
		return kybYesterday;
	}

	public void setKybYesterday(BigDecimal kybYesterday) {
		this.kybYesterday = kybYesterday;
	}

	public BigDecimal getKybToday() {
		return kybToday;
	}

	public void setKybToday(BigDecimal kybToday) {
		this.kybToday = kybToday;
	}

	public BigDecimal getKybChange() {
		return kybChange;
	}

	public void setKybChange(BigDecimal kybChange) {
		this.kybChange = kybChange;
	}

	public BigDecimal getKybZhuanhuan() {
		return kybZhuanhuan;
	}

	public void setKybZhuanhuan(BigDecimal kybZhuanhuan) {
		this.kybZhuanhuan = kybZhuanhuan;
	}

	public BigDecimal getKybStorepay() {
		return kybStorepay;
	}

	public void setKybStorepay(BigDecimal kybStorepay) {
		this.kybStorepay = kybStorepay;
	}

	public BigDecimal getKybStorepayfee() {
		return kybStorepayfee;
	}

	public void setKybStorepayfee(BigDecimal kybStorepayfee) {
		this.kybStorepayfee = kybStorepayfee;
	}

	public BigDecimal getKybMallpay() {
		return kybMallpay;
	}

	public void setKybMallpay(BigDecimal kybMallpay) {
		this.kybMallpay = kybMallpay;
	}

	public BigDecimal getKybMallpayfee() {
		return kybMallpayfee;
	}

	public void setKybMallpayfee(BigDecimal kybMallpayfee) {
		this.kybMallpayfee = kybMallpayfee;
	}

	public BigDecimal getKybTixianapply() {
		return kybTixianapply;
	}

	public void setKybTixianapply(BigDecimal kybTixianapply) {
		this.kybTixianapply = kybTixianapply;
	}

	public BigDecimal getKybTixianfee() {
		return kybTixianfee;
	}

	public void setKybTixianfee(BigDecimal kybTixianfee) {
		this.kybTixianfee = kybTixianfee;
	}

	public BigDecimal getKybTixiannopass() {
		return kybTixiannopass;
	}

	public void setKybTixiannopass(BigDecimal kybTixiannopass) {
		this.kybTixiannopass = kybTixiannopass;
	}

	public BigDecimal getKybTuikuan() {
		return kybTuikuan;
	}

	public void setKybTuikuan(BigDecimal kybTuikuan) {
		this.kybTuikuan = kybTuikuan;
	}

	public BigDecimal getKybOther() {
		return kybOther;
	}

	public void setKybOther(BigDecimal kybOther) {
		this.kybOther = kybOther;
	}

	public Integer getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Integer beginTime) {
		this.beginTime = beginTime;
	}

	public Integer getEndTime() {
		return endTime;
	}

	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "CheckDetailList [loginName=" + loginName + ", id=" + id + ", userId=" + userId + ", checkDate="
				+ checkDate + ", zjCheckdiff=" + zjCheckdiff + ", jfCheckdiff=" + jfCheckdiff + ", kybCheckdiff="
				+ kybCheckdiff + ", zjYesterday=" + zjYesterday + ", zjToday=" + zjToday + ", zjChange=" + zjChange
				+ ", zjStorepay=" + zjStorepay + ", zjMallpay=" + zjMallpay + ", zjTixianapply=" + zjTixianapply
				+ ", zjTixianfee=" + zjTixianfee + ", zjTixiannopass=" + zjTixiannopass + ", zjTuikuan=" + zjTuikuan
				+ ", zjChongzhi=" + zjChongzhi + ", jfYesterday=" + jfYesterday + ", jfToday=" + jfToday + ", jfChange="
				+ jfChange + ", jfZhuanhuan=" + jfZhuanhuan + ", jfStorepay=" + jfStorepay + ", jfMallpay=" + jfMallpay
				+ ", jfInvitereward=" + jfInvitereward + ", jfOther=" + jfOther + ", kybYesterday=" + kybYesterday
				+ ", kybToday=" + kybToday + ", kybChange=" + kybChange + ", kybZhuanhuan=" + kybZhuanhuan
				+ ", kybStorepay=" + kybStorepay + ", kybStorepayfee=" + kybStorepayfee + ", kybMallpay=" + kybMallpay
				+ ", kybMallpayfee=" + kybMallpayfee + ", kybTixianapply=" + kybTixianapply + ", kybTixianfee="
				+ kybTixianfee + ", kybTixiannopass=" + kybTixiannopass + ", kybTuikuan=" + kybTuikuan + ", kybOther="
				+ kybOther + ", beginTime=" + beginTime + ", endTime=" + endTime + "]";
	}

}
