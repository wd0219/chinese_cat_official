package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktLogAgentsLogins;

import java.util.List;

/**
 * <p>
 * 代理公司登陆记录表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
public interface ISktLogAgentsLoginsService extends IService<SktLogAgentsLogins> {
	
	public List<SktLogAgentsLogins> sktLogAgentsLoginfindup(SktLogAgentsLogins sktLogAgentsLogins);
}
