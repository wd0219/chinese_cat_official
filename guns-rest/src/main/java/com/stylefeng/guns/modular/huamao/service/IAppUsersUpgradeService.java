package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;

import java.util.Map;

public interface IAppUsersUpgradeService extends IService<UsersUpgrade> {
    UsersUpgrade selectUsersUpgradeByUserId(Integer userId);

    boolean insertOptBing(Map<Object, Object> maps);

    UsersUpgrade selectUsersUpgradeByUserIdAndStatus(Map<Object, Object> maps);

    void delectOptBingByUserIdAndCode(Map<Object, Object> maps);

    UsersUpgrade selectUsersUpgradeByOrderNo(Map<String, Object> orderWhere);
}
