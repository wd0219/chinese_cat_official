package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktAreas;

public interface IAppSktAreaService extends IService<SktAreas> {
    SktAreas selectSktAreasById(Integer provinceId);

    SktAreas selectAreaByProvinceId(int alevel);

    SktAreas selectAreaByCityId(int alevel);

    SktAreas selectAreaByAreaId(int alevel);

}
