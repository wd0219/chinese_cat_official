package com.stylefeng.guns.modular.huamao.result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统提示信息封装类
 *
 * @author SuLong
 */
public class Result {
    public static final int TYPE_RESULT_FAIL = 0;//失败
    public static final int TYPE_RESULT_SUCCESS = 1;//成功
    public static final int TYPE_RESULT_WARN = 2;//警告
    public static final int TYPE_RESULT_INFO = 3;//提示信息

    public Result() {
    }

    /**
     * 消息提示类型
     */
    private int type;
    private String url;

    private Integer error;
    /**
     * 提示代码
     */
    private int messageCode;


    /**
     * 提示信息
     */
    private String message;


    /**
     * 返给前台的实体
     */
    private Object entity;
    private Integer page;
    /**
     * 构造函数,根据提交信息代码messageCode获取提示信息
     *
     * @param
     */
    public Result(final int type, int messageCode, String message) {
        this.type = type;
        this.messageCode = messageCode;
        this.message = message;
    }
    public Result(int type, int messageCode, String message, Object entity) {
        this.type = type;
        this.messageCode = messageCode;
        this.message = message;
        this.entity = entity;
    }
    public Result(int type, int messageCode, String message, Object entity,Integer page) {
        this.type = type;
        this.messageCode = messageCode;
        this.message = message;
        this.entity = entity;
        this.page = page;
    }
    public static Result OK(Object object) {
        return new Result(1, 200, "操作成功", object);
    }
    public static Result OK(Object object,Integer page) {
        return new Result(1, 200, "操作成功", object, page);
    }
    public static Result OK(Object object, String message) {
        return new Result(1, 200, message, object);
    }
    
    
    public static Result EEROR(String message) {
        return new Result(0, 500, message);
    }


    public int getMessageCode() {
        return messageCode;
    }


    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }

    public Object getEntity() {
        return entity;
    }

    public void setEntity(Object entity) {
        this.entity = entity;
    }

    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isSuccess() {
        if (this.type == TYPE_RESULT_SUCCESS) {
            return true;
        }
        return false;
    }

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
    
}