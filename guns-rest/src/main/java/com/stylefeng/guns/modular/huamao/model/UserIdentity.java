package com.stylefeng.guns.modular.huamao.model;

public class UserIdentity {
	public String userId;
	public String suserType;//用户类型
	public String sauditStatus;//认证状态
	public String auditRemark;//失败说明
	public String sstatus;//升级状态
	public String orderRemarks;//升级失败说明
	public String sastatus;//代理状态
	public String checkRemark;//代理失败说明
	public String applyStatus;//线上状态
	public String applyDesc;//线上失败说明
	public String shopStatus;//线下状态
	public String statusDesc;//线下说明
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSuserType() {
		return suserType;
	}
	public void setSuserType(String suserType) {
		this.suserType = suserType;
	}
	public String getSauditStatus() {
		return sauditStatus;
	}
	public void setSauditStatus(String sauditStatus) {
		this.sauditStatus = sauditStatus;
	}
	public String getAuditRemark() {
		return auditRemark;
	}
	public void setAuditRemark(String auditRemark) {
		this.auditRemark = auditRemark;
	}
	public String getSstatus() {
		return sstatus;
	}
	public void setSstatus(String sstatus) {
		this.sstatus = sstatus;
	}
	public String getOrderRemarks() {
		return orderRemarks;
	}
	public void setOrderRemarks(String orderRemarks) {
		this.orderRemarks = orderRemarks;
	}
	public String getSastatus() {
		return sastatus;
	}
	public void setSastatus(String sastatus) {
		this.sastatus = sastatus;
	}
	public String getCheckRemark() {
		return checkRemark;
	}
	public void setCheckRemark(String checkRemark) {
		this.checkRemark = checkRemark;
	}
	public String getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}
	public String getApplyDesc() {
		return applyDesc;
	}
	public void setApplyDesc(String applyDesc) {
		this.applyDesc = applyDesc;
	}
	public String getShopStatus() {
		return shopStatus;
	}
	public void setShopStatus(String shopStatus) {
		this.shopStatus = shopStatus;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
}
