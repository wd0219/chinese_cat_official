package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.SktClass;
import com.stylefeng.guns.modular.huamao.model.SktClassType;
import com.stylefeng.guns.modular.huamao.model.SktComment;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppSktCommentService;
import com.stylefeng.guns.modular.huamao.service.ISktClassService;
import com.stylefeng.guns.modular.huamao.service.ISktClassTypeService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.Pager;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.swing.text.html.parser.Entity;
import java.util.*;

/**
 * 商学院评论控制
 *
 * @author liuduan
 * @Date 2018-8-11 10:53:48
 */
@Controller
@RequestMapping("/app")
@CrossOrigin
public class AppSktCommentController extends BaseController {

    @Autowired
    private IAppSktCommentService iSktCommentService;
    @Autowired
    private CacheUtil cacheUtil;

    /**
     * 查询视频的评论内容
     * @param pageIndex
     * @param classId
     * @return
     */
    @RequestMapping("/comment/getList")
    @ResponseBody
    public AppResult getClassComment(@ModelAttribute("pageIndex")String pageIndex, @ModelAttribute("classId")String classId){
        Map<String,Object> map=new HashMap<>();
        map.put("classId",Integer.parseInt(classId));
        Integer pageNum=10;
        int pageIndeses = MSGUtil.isBlank(pageIndex) ? 0 : (Integer.parseInt(pageIndex) - 1) * pageNum;
        map.put("pageIndex",pageIndeses);
        map.put("pageNum",pageNum);
        List<Map<String,Object>> sktCommentList=iSktCommentService.selectListByClassId(map);
        SktComment sktComment=new SktComment();
        sktComment.setClassId(Integer.parseInt(classId));
        EntityWrapper<SktComment> entityWrapper=new EntityWrapper<SktComment>(sktComment);
        int totalPage = iSktCommentService.selectCount(entityWrapper);
        Pager<SktComment> pager=new Pager(totalPage,pageIndeses,pageNum);
        Map<String,Object> maps=new HashMap<>();
        maps.put("maps",sktCommentList);
        maps.put("totalPage",pager.getPages());
        if(sktCommentList.size()<1){
            return AppResult.ERROR("该视频无评论",-1);
        }
        return AppResult.OK("获取成功",maps);
    }

    /**
     * 视频评论提交
     * @param classId
     * @param content
     * @return
     */
    @RequestMapping("/comment/commentSubmit")
    @ResponseBody
    public AppResult commentSubmit(@ModelAttribute("classId")String classId,@ModelAttribute("content")String content){
        Users users = (Users) cacheUtil.get(SessionUtils.getSessionId());
        if (MSGUtil.isBlank(users)){
            return AppResult.ERROR("请先登陆",-1);
        }
        SktComment sktComment=new SktComment();
        sktComment.setContent(content);
        sktComment.setClassId(Integer.parseInt(classId));
        sktComment.setUserId(users.getUserId());
        sktComment.setCreatTime(new Date());
        boolean insert = iSktCommentService.insert(sktComment);
        if (insert){
            return AppResult.OK("评论成功",null);
        }
        return AppResult.ERROR("评论失败",-1);
    }


}
