package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoodsList;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoodsParameter;
import com.stylefeng.guns.modular.huamao.model.SktOrderScore;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.model.SktOrdersList;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 线上商城订单表 Mapper 接口
 * </p>
 *
 * @author caody123
 * @since 2018-04-14
 */
public interface SktOrdersMapper extends BaseMapper<SktOrders> {
	/**
	 * 查询订单列表
	 * @param paramMap
	 * @return
	 */
	public List<SktOrdersList> selectOrderShops(SktOrdersList sktOrdersList);
	/**
	 * 订单积分详情数据
	 * @param param
	 * @return
	 */
	public SktOrderScore selectOrderScore(Map<String, Object> param);
	/**
	 * 订单商品列表
	 * @param param
	 * @return
	 */
	public List<Map<String,Object>> selectOrderGoods(Integer sktOrdersId);
	/**
	 * 订单商品参数列表
	 * @param param
	 * @return
	 */
	public List<SktOrderGoodsParameter> selectOrderGoodsParameter(Map<String, Object> param);
	
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int selectOrderTotal();
	
	/**  根据订单id查询订单商品表
	 * @param orderId
	 * @return
	 */
	public List<SktOrderGoods> getOrderGoodsByOrderId(int orderId);

	/**
	 * 首页显示
	 * @param sktOrdersList
	 * @return
	 */
    List<Map<String,Object>> selectUserOrders(SktOrdersList sktOrdersList);
    
    /**
     * 购物车结算时生成的订单
     * @param sktOrders
     * @return
     */
    public int produceOrders(SktOrders sktOrders);
    
    /**
     * 通过用户id查询地址，默认与所有的地址
     * @param map 
     * 当isDefault为1时是默认地址，为0时是备用地址
     * @return
     */
    public List<Map<String,Object>> selectAddressByUserid(Map<String,Object> map);
    /**
     * 查询代发货 待付款 待评价
     * @param sktOrdersList
     * @return
     */
	public Map<String, Object> selectUserOrdersInfo(SktOrdersList sktOrdersList);

	/**
	 * 监听(定时任务)清除过期订单
	 * @return
	 */
	public Integer orderBatch();

	List<Map<String,Object>> appFindAllOrders(Map<String,Object> para);

	Integer appUpdateStatus(Map<String, Object> map);

	Map<String,Object> appFindOrdersDetailed(String orderNo);

	List<Map<String,Object>> appFindToBeEvaluatedOrders(Map<String, Object> map);

    List<Map<String,Object>> appFindrefundOrder(Map<String, Integer> map);

    List<Map<String,Object>> selectOrderAndOrderRefundsByUserId(Map<Object, Object> map);

    Map<String,Object> selectReceiveOrders(Map<String, Object> map);

    SktOrders selectOrderByUserId(Map<String, Object> map);

    void updateOrderStatusByOrderNo(Map<String, Object> map);

    SktOrders selectOrderByIsPay(Map<String, Object> map);

	Double selectSumTotalMoney(Map<String, Object> map1);

	Double selectSumPayScore(Map<String, Object> map1);

    SktOrders appFindOrderByOrderNo(String orderNo);

    List<SktOrders> findOrdersByOrderunique(String orderNo);
	Double selectSumPayKaiyuan(Map<String, Object> map1);
	Double selectSumPayShopping(Map<String, Object> map1);
	public Integer updateOrderStatus(Map<String, Object> map);
}
