package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 推荐记录表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-24
 */
@TableName("skt_recommends")
public class Recommends extends Model<Recommends> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品分类ID
     */
    private Integer goodsCatId;
    /**
     * 数据类型
     */
    private Integer dataType;
    /**
     * 数据来源
     */
    private Integer dataSrc;
    /**
     * 数据再其表中的主键
     */
    private Integer dataId;
    /**
     * 数据排序号
     */
    private Integer dataSort;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsCatId() {
        return goodsCatId;
    }

    public void setGoodsCatId(Integer goodsCatId) {
        this.goodsCatId = goodsCatId;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public Integer getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(Integer dataSrc) {
        this.dataSrc = dataSrc;
    }

    public Integer getDataId() {
        return dataId;
    }

    public void setDataId(Integer dataId) {
        this.dataId = dataId;
    }

    public Integer getDataSort() {
        return dataSort;
    }

    public void setDataSort(Integer dataSort) {
        this.dataSort = dataSort;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Recommends{" +
        "id=" + id +
        ", goodsCatId=" + goodsCatId +
        ", dataType=" + dataType +
        ", dataSrc=" + dataSrc +
        ", dataId=" + dataId +
        ", dataSort=" + dataSort +
        "}";
    }
}
