package com.stylefeng.guns.modular.huamao.service.impl;

import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;
import com.stylefeng.guns.modular.huamao.dao.SktOrderGoodsMapper;
import com.stylefeng.guns.modular.huamao.service.ISktOrderGoodsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单商品表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-11
 */
@Service
public class SktOrderGoodsServiceImpl extends ServiceImpl<SktOrderGoodsMapper, SktOrderGoods> implements ISktOrderGoodsService {

    @Autowired
    private SktOrderGoodsMapper sktOrderGoodsMapper;
    @Override
    public List<Map<String, Object>> appFindGoodById(Integer orderId) {
        return sktOrderGoodsMapper.appFindGoodById(orderId);
    }

    @Override
    public List<Map<String, Object>> appFindOrderMesById(Integer orderId) {
        return sktOrderGoodsMapper.appFindOrderMesById(orderId);
    }
}
