package com.stylefeng.guns.modular.huamao.model;

import java.util.Date;

public class AppTradeDictDTD {
    /**
     * id
     */
    private Integer id;
    /**
     * 业务类型
     */
    private String gType;
    /**
     * 业务tCode
     */
    private String tCode;
    /**
     * 业务名称
     */
    private String tName;
    /**
     * 描述
     */
    private String remark;
    /**
     * 1 正常 0禁用
     */
    private Integer dataFlag;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 创建日期
     */
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getgType() {
        return gType;
    }

    public void setgType(String gType) {
        this.gType = gType;
    }

    public String gettCode() {
        return tCode;
    }

    public void settCode(String tCode) {
        this.tCode = tCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

}
