package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Recommends;

import java.util.Map;

/**
 * <p>
 * 推荐记录表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-24
 */
public interface IRecommendsService extends IService<Recommends> {
	Map<String,Object> selectMax();
	boolean deleteByDataId(Integer dataId);
	Map<String,Object>selectBrand(Integer dataId);
}
