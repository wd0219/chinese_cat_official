package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;

import java.util.Map;

public interface IAppUsersRechargeService extends IService<UsersRecharge> {
    UsersRecharge selectUsersRechargeByOrderNo(Map<String, Object> orderWhere);

    UsersRecharge selectUsersRechargeByUserId(Map<String, Object> map);
}
