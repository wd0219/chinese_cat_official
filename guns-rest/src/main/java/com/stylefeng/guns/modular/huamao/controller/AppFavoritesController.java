package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.SktFavorites;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppFavoritesService;
import com.stylefeng.guns.modular.huamao.service.IAppGoodsService;
import com.stylefeng.guns.modular.huamao.service.IAppShopsService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.CommonModel;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@SuppressWarnings("all")
@Controller
@CrossOrigin
@RequestMapping("/app")
public class AppFavoritesController extends BaseController {
    @Autowired
    private IAppFavoritesService iAppFavoritesService;
    @Autowired
    private IAppShopsService iAppShopsService;
    @Autowired
    private IAppGoodsService iAppGoodsService;
    @Autowired
    private CacheUtil cacheUtil;


    /**
     * 收藏的商品列表
     * @param page
     * @return
     */
    @RequestMapping("/Favorites/listGoodsQueryApp")
    @ResponseBody
    public AppResult listGoodsQueryApp(@ModelAttribute("page")String page){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            Map<String, Object> map = new HashMap<>();
            map.put("userId",user.getUserId());
            map.put("pagesize",10);
            map.put("page",(Integer.parseInt(page)-1)*10);
            List<Map<String,Object>> list=iAppFavoritesService.selectFavoritesByUserId(map);
            CommonModel commonModel=new CommonModel();
            List<Object> listGoods = new ArrayList<>();
            if (!MSGUtil.isBlank(list)) {
                for (Map<String, Object> map1 : list) {
                    List<Map<String, Object>> list1 = iAppShopsService.selectShopsAndAccredsByShopId(map1);
                    map1.put("accreds", list1);
                    map1.put("goodsImg", commonModel.getOSS_DOM() + map1.get("goodsImg"));
                    listGoods.add(map1);
                }
            }/*else {
                Map<Object, Object> map1 = new HashMap<>();
                map1.put("accreds","");
                map1.put("goodsImg","");
                listGoods.add(map1);
            }*/
            return AppResult.OK("查询成功",listGoods);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("您还未登陆",-2);
        }
    }

    /**
     * 收藏的店铺列表
     * @param page
     * @return
     */
    @RequestMapping("/Favorites/listShopQueryApp")
    @ResponseBody
    public AppResult listShopQueryApp(@ModelAttribute("page")String page){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            Map<String, Object> map = new HashMap<>();
            map.put("userId",user.getUserId());
            map.put("pagesize",10);
            map.put("page",(Integer.parseInt(page)-1)*10);
            List<Map<String,Object>> list=iAppFavoritesService.selectFavoritesAndShopsByUserId(map);
            List<Object> objects = new ArrayList<>();
            if (!MSGUtil.isBlank(list)) {
                CommonModel commonModel = new CommonModel();
                for (Map<String, Object> maps : list) {
                    //商品列表
                    List<Map<String, Object>> list1 = iAppGoodsService.selectGoodsByShopId(maps);
                    ArrayList<Object> objects1 = new ArrayList<>();
                    for (Map<String, Object> map1 : list1) {
                        map1.put("goodsImg", commonModel.getOSS_DOM() + map1.get("goodsImg"));
                        objects1.add(map1);
                    }
                    maps.put("goods", objects1);
                    maps.put("shopImg", commonModel.getOSS_DOM() + maps.get("shopImg"));
                    objects.add(maps);
                }
            }
            return AppResult.OK("查询成功",objects);
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("获取错误",-2);
        }
    }

    /**
     * 新增关注
     * @param id
     * @param type
     * @return
     */
    @RequestMapping("/Favorites/add")
    @ResponseBody
    public AppResult add(@ModelAttribute("id")String id,@ModelAttribute("type")String type){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if(user==null){
            return AppResult.ERROR("您未登录",-2);
        }
            //判断记录是否存在
            boolean b=false;
            if (Integer.parseInt(type)==0) {
                int goodsCount = iAppGoodsService.selectCountByGoodsId(Integer.parseInt(id));
                b=goodsCount>0;
            }else {
                int shopCount=iAppShopsService.selectCountByShopId(Integer.parseInt(id));
                b=shopCount>0;
            }
            if (!b){
                return AppResult.ERROR("关注失败，无效的关注对象",-1);
            }
            //判断是否已关注
            SktFavorites sktFavorites = new SktFavorites();
            sktFavorites.setUserId(user.getUserId());
            sktFavorites.setFavoriteType(Integer.parseInt(type));
            sktFavorites.setTargetId(Integer.parseInt(id));
            int favoritesCount=iAppFavoritesService.selectCountByUserId(sktFavorites);
            if (favoritesCount>0){
                return AppResult.ERROR("已收藏",-1);
            }
            sktFavorites.setCreateTime(new Date());
            iAppFavoritesService.insertAndFindId(sktFavorites);
            Map<String, Object> map = new HashMap<>();
            map.put("fId",sktFavorites.getFavoriteId());
            return AppResult.OK("收藏成功",map);

        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("新增失败",-2);
        }
    }

    /**
     * 删除关注
     * @param ids
     * @param type
     * @return
     */
    @RequestMapping("/Favorites/del")
    @ResponseBody
    public AppResult del(@ModelAttribute("ids")String ids,@ModelAttribute("type")String type){
        try {
            Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
            if (MSGUtil.isBlank(ids)){
                return AppResult.ERROR("取消收藏失败",-1);
            }
            boolean b = iAppFavoritesService.deleteById(Integer.parseInt(ids));
            if (b){
                return AppResult.OK("取消收藏成功",null);
            }
        }catch (Exception e){
            e.printStackTrace();
            return AppResult.ERROR("取消收藏失败",-2);
        }
        return  AppResult.ERROR("取消收藏失败",-1);
    }

}
