package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktShopIndustry;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 行业管理表 Mapper 接口
 * </p>
 *
 * @author ck123123
 * @since 2018-04-19
 */
public interface SktShopIndustryMapper extends BaseMapper<SktShopIndustry> {
	/*
	 * 展示全部+sort排序
	 * */
	public List<SktShopIndustry> shopIndustryfindinfo(SktShopIndustry shopIndustry);

    List<Map<String,Object>> selectShopsIndustryByHot();

	List<Map<String,Object>> selectAllList();
}
