package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;

public interface IAppShopApplysService extends IService<SktShopApplys> {
    SktShopApplys selectShopApplysByUserId(Integer userId);
}
