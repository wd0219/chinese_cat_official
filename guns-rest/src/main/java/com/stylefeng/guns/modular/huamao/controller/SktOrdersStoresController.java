package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStoresDto;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersStoresService;

/**
 * 线下订单管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 09:56:55
 */
@CrossOrigin
@Controller
@RequestMapping("/sktOrdersStores")
public class SktOrdersStoresController extends BaseController {

    private String PREFIX = "/huamao/sktOrdersStores/";

    @Autowired
    private ISktOrdersStoresService sktOrdersStoresService;

    /**
     * 跳转到线下订单管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktOrdersStores.html";
    }

    /**
     * 跳转到添加线下订单管理
     */
    @RequestMapping("/sktOrdersStores_add")
    public String sktOrdersStoresAdd() {
        return PREFIX + "sktOrdersStores_add.html";
    }

    /**
     * 跳转到修改线下订单管理
     */
    @RequestMapping("/sktOrdersStores_update/{sktOrdersStoresId}")
    public String sktOrdersStoresUpdate(@PathVariable Integer sktOrdersStoresId, Model model) {
    	SktOrdersStoresDto  sktOrdersStoresDto = new SktOrdersStoresDto();
    	sktOrdersStoresDto.setOrderId(sktOrdersStoresId);
        List<SktOrdersStoresDto> sktOrdersStores = sktOrdersStoresService.showOrdersStoresInfo(sktOrdersStoresDto);
        model.addAttribute("item",sktOrdersStores.size()>0?sktOrdersStores.get(0):null);
        LogObjectHolder.me().set(sktOrdersStores);
        return PREFIX + "sktOrdersStores_edit.html";
    }

    /**
     * 获取线下订单管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktOrdersStoresDto  sktOrdersStoresDto) {
        return sktOrdersStoresService.showOrdersStoresInfo(sktOrdersStoresDto);
    }

    /**
     * 新增线下订单管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktOrdersStores sktOrdersStores) {
        sktOrdersStoresService.insert(sktOrdersStores);
        return SUCCESS_TIP;
    }

    /**
     * 删除线下订单管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktOrdersStoresId) {
        sktOrdersStoresService.deleteById(sktOrdersStoresId);
        return SUCCESS_TIP;
    }

    /**
     * 修改线下订单管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktOrdersStores sktOrdersStores) {
        sktOrdersStoresService.updateById(sktOrdersStores);
        return SUCCESS_TIP;
    }

    /**
     * 线下订单管理详情
     */
    @RequestMapping(value = "/detail/{sktOrdersStoresId}")
    @ResponseBody
    public Object detail(@PathVariable("sktOrdersStoresId") Integer sktOrdersStoresId) {
        return sktOrdersStoresService.selectById(sktOrdersStoresId);
    }

    /******************首页*******************/
    /**
     * 线下商城
     */
    @RequestMapping(value = "/selectUserOrdersStores", method = RequestMethod.GET)
    @ResponseBody
    public Result selectUserOrdersStores(SktOrdersStoresDto sktOrdersStoresDto,Integer pageNum){
    	PageHelper.startPage(pageNum, 10);
        List<Map<String,Object>> list = sktOrdersStoresService.selectUserOrdersStores(sktOrdersStoresDto);
        PageInfo<Map<String,Object>> info = new PageInfo<>(list);
		list = info.getList();
		return Result.OK(list,info.getPages());
    }
    
    /**
     * 商家订单查询下单
     */
    @RequestMapping(value = "/selectShopOrdersStoresUp", method = RequestMethod.GET)
    @ResponseBody
    public Result selectShopOrdersStoresUp(SktOrdersStoresDto sktOrdersStoresDto,Integer pageNum){
    	PageHelper.startPage(pageNum, 12);
        List<Map<String,Object>> list = sktOrdersStoresService.selectShopOrdersStoresUp(sktOrdersStoresDto);
        PageInfo info = new PageInfo<>(list);
		return Result.OK(list,info.getPages());
    }
    /**
     * 商家订单查询取消
     */
    @RequestMapping(value = "/selectShopOrdersStoresCh", method = RequestMethod.GET)
    @ResponseBody
    public Result selectShopOrdersStoresCh(SktOrdersStoresDto sktOrdersStoresDto,Integer pageNum){
    	PageHelper.startPage(pageNum, 12);
        List<Map<String,Object>> list = sktOrdersStoresService.selectShopOrdersStoresCh(sktOrdersStoresDto);
        PageInfo info = new PageInfo<>(list);
		return Result.OK(list,info.getPages());
    }
    /**
     * 商家订单查询完成
     */
    @RequestMapping(value = "/selectShopOrdersStoresOk", method = RequestMethod.GET)
    @ResponseBody
    public Result selectShopOrdersStoresOk(SktOrdersStoresDto sktOrdersStoresDto,Integer pageNum){
    	PageHelper.startPage(pageNum, 12);
        List<Map<String,Object>> list = sktOrdersStoresService.selectShopOrdersStoresOk(sktOrdersStoresDto);
        PageInfo info = new PageInfo<>(list);
		return Result.OK(list,info.getPages());
    }
}
