package com.stylefeng.guns.modular.huamao.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.Ads;
import com.stylefeng.guns.modular.huamao.model.AdsList;
import com.stylefeng.guns.modular.huamao.model.SktClass;
import com.stylefeng.guns.modular.huamao.model.SktClassType;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAdsService;
import com.stylefeng.guns.modular.huamao.service.IAppSktCommentService;
import com.stylefeng.guns.modular.huamao.service.ISktClassService;
import com.stylefeng.guns.modular.huamao.service.ISktClassTypeService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.Pager;
import com.stylefeng.guns.modular.huamao.util.UrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 商学院控制器
 *
 * @author liuduan
 * @Date 2018-8-11 10:53:48
 */
@Controller
@RequestMapping("/app")
@CrossOrigin
public class AppSktClassController extends BaseController {

    @Autowired
    private ISktClassService iSktClassService;
    @Autowired
    private ISktClassTypeService iSktClassTypeService;
    @Autowired
    private IAppSktCommentService iSktCommentService;
    @Autowired
    private CacheUtil cacheUtil;

    /**
     * 获取视频信息
     *
     * @param typeId
     * @return
     */
    @RequestMapping("/video/getVideo")
    @ResponseBody
    public AppResult getVideoClass(@ModelAttribute("pageIndex") String pageIndex, @ModelAttribute("typeId") String typeId) {
        SktClass sktClass = new SktClass();
        Map<String, Object> map = new HashMap<>();
        Integer pageNum = 10;
        int pageIndeses = MSGUtil.isBlank(pageIndex) ? 0 : (Integer.parseInt(pageIndex) - 1) * pageNum;
        map.put("pageIndex", pageIndeses);
        map.put("pageNum", pageNum);
        if (!MSGUtil.isBlank(typeId)) {
            map.put("classType", Integer.parseInt(typeId));
            sktClass.setClassType(Integer.parseInt(typeId));
        }
        List<Map<String, Object>> list = iSktClassService.selectClassByTypeOrAll(map);
            EntityWrapper<SktClass> entityWrapper = new EntityWrapper<SktClass>(sktClass);
            int totalPage = iSktClassService.selectCount(entityWrapper);
            Pager<SktClass> pager = new Pager(totalPage, pageIndeses, pageNum);
            Map<String, Object> maps = new HashMap<>();
            maps.put("maps", list);
            maps.put("totalPage", pager.getPages());
            return AppResult.OK("获取成功", maps);
    }

    /**
     * 获取视频类型
     *
     * @return
     */
    @RequestMapping("/video/getVideoType")
    @ResponseBody
    public AppResult getVideoType() {
        List<SktClassType> sktClassTypesList = iSktClassTypeService.selectAllType();
        List<Object> list = new ArrayList<>();
        for (SktClassType sktClassType : sktClassTypesList) {
//            if (sktClassType.getTypeName().equals("今日推荐") || sktClassType.getTypeName().equals("热门视频")) {
//                continue;
//            }
            list.add(sktClassType);
        }
        return AppResult.OK("获取成功", list);
    }


    /**
     * 根据条件查询视频
     *
     * @param className
     * @param classType
     * @return
     */
    @RequestMapping("/video/videoSearch")
    @ResponseBody
    public AppResult videoSearch(@ModelAttribute("pageIndex") String pageIndex, @ModelAttribute("className") String className, @ModelAttribute("classType") String classType) {
        Map<String, Object> map = new HashMap<>();
        Integer pageNum = 10;
        int pageIndeses = MSGUtil.isBlank(pageIndex) ? 0 : (Integer.parseInt(pageIndex) - 1) * pageNum;
        map.put("pageIndex", pageIndeses);
        map.put("pageNum", pageNum);
        map.put("className", className);
        if (Integer.parseInt(classType) == 1) {
            List<SktClassType> sktClassTypesList = iSktClassTypeService.selectAllType();
            for (SktClassType sktClassType : sktClassTypesList) {
                if (sktClassType.getTypeName().indexOf("推荐") > 1) {
                    map.put("classType", sktClassType.getTypeId());
                }
            }
            List<Map<String,Object>> sktClassList = iSktClassService.selectSearchVideo(map);
            Map<String, Object> maps = new HashMap<>();
            maps.put("maps", sktClassList);
            maps.put("totalPage",0);
            return AppResult.OK("获取成功", maps);
        }
        if (Integer.parseInt(classType) == 2) {
            List<Map<String,Object>> sktClassList = iSktClassService.selectSearchVideo(map);
            if (sktClassList.size() > 0) {
                Map<String, Object> maps = new HashMap<>();
                maps.put("maps", sktClassList);
                maps.put("totalPage",0);
                return AppResult.OK("获取成功", maps);
            }
        }
        if (Integer.parseInt(classType) == 3){
            List<Map<String,Object>> list=iSktCommentService.selectClassByComment(map);
            if (list.size()<1){
                return AppResult.ERROR("没有视频被评论",-1);
            }
            List<Object> sktClassList=new ArrayList<>();
            for (Map<String,Object> mapss:list){
                sktClassList.add(mapss.get("classId"));
            }
            map.put("classId",sktClassList.toArray());
            /*for (Map<String,Object> maps:list){
                Integer classId = Integer.parseInt(maps.get("classId").toString());
                SktClass sktClass=new SktClass();
                sktClass.setClassId(classId);*/
            List<Map<String,Object>> sktClassData=iSktClassService.selectBatchClassByClassId(map);
          /*      sktClassData.put("commentCount",map.get("classCount"));
                sktClassList.add(sktClassData);
            }*/
            Map<String,Object> maps=new HashMap<>();
            maps.put("maps",sktClassData);
            maps.put("totalPage",0);
            return AppResult.OK("获取成功",maps);
        }
            return AppResult.ERROR("没有该视频信息", -1);
    }


    /**
     * 视频首页
     *
     * @return
     */
    @RequestMapping("/video/videoList")
    @ResponseBody
    public AppResult videoList() {
        List<SktClassType> sktClassTypesList = iSktClassTypeService.selectAllType();
        List<Object> list = new ArrayList<>();
        for (SktClassType sktClassType : sktClassTypesList) {
            Map<String, Object> map = new HashMap<>();
            map.put("classType", sktClassType.getTypeId());
            map.put("pageNum", 4);
            map.put("pageIndex", 0);
            List<Map<String, Object>> sktClassList = iSktClassService.selectPageClass(map);
            if (sktClassList.size() < 1) {
                continue;
            }
            Map<String, Object> maps = new HashMap<>();
            maps.put("list", sktClassList);
            maps.put("typeName", sktClassType.getTypeName());
            list.add(maps);
        }
        return AppResult.OK("获取成功", list);
    }

    /**
     * 根据视频Id获取视频信息
     *
     * @param classId
     * @return
     */
    @RequestMapping("/video/getVideoData")
    @ResponseBody
    public AppResult getVideoData(@ModelAttribute("classId") String classId) {
        SktClass sktClass = new SktClass();
        sktClass.setClassId(Integer.parseInt(classId));
        Map<String, Object> sktClassData = iSktClassService.selectClassByClassId(sktClass);
        if (MSGUtil.isBlank(sktClassData)) {
            return AppResult.ERROR("获取失败", -1);
        }
        return AppResult.OK("获取成功", sktClassData);
    }
}
