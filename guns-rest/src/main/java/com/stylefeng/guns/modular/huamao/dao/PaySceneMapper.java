package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.Ads;
import com.stylefeng.guns.modular.huamao.model.AdsList;
import com.stylefeng.guns.modular.huamao.model.PayScene;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 支付表 Mapper 接口
 * </p>
 *
 * @author liuduan
 * @since 2018-06-20
 */
public interface PaySceneMapper extends BaseMapper<PayScene> {


	PayScene selectPayBySceneCode(Map<String, Object> map);

    Map<String,Object> appFindPayScene(String code);
}
