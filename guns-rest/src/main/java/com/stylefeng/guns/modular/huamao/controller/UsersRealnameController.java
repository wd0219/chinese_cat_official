package com.stylefeng.guns.modular.huamao.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.UsersDTO;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;
import com.stylefeng.guns.modular.huamao.model.UsersRealnameDTO;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IUsersRealnameService;

/**
 * 个人认证控制器
 *
 * @author fengshuonan
 * @Date 2018-04-14 11:55:15
 */
@CrossOrigin()
@Controller
@RequestMapping("/usersRealname")
public class UsersRealnameController extends BaseController {

    private String PREFIX = "/huamao/usersRealname/";

    @Autowired
    private IUsersRealnameService usersRealnameService;

    /**
     * 跳转到个人认证首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "usersRealname.html";
    }

    /**
     * 跳转到添加个人认证
     */
    @RequestMapping("/usersRealname_add")
    public String usersRealnameAdd() {
        return PREFIX + "usersRealname_add.html";
    }

    /**
     * 跳转到详情
     */
    @RequestMapping("/usersRealname_XiangQing/{usersRealnameId}")
    public String usersRealnameXiangQing(@PathVariable(value="usersRealnameId") Integer usersRealnameId, Model model) {
        UsersRealnameDTO usersRealname = usersRealnameService.showRenZhengXiangQing(usersRealnameId);
        model.addAttribute("item",usersRealname);
        return PREFIX + "usersRealname_xiangqing.html";
    }
    
    /**
     * 跳转到详情
     */
    @RequestMapping("/usersRealname_Update/{usersRealnameId}")
    public String usersRealnameUpdate(@PathVariable(value="usersRealnameId") Integer usersRealnameId, Model model) {
        UsersRealnameDTO usersRealname = usersRealnameService.showRenZhengXiangQing(usersRealnameId);
        model.addAttribute("item",usersRealname);
        return PREFIX + "usersRealname_edit.html";
    }

    /**
     * 获取个人认证列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(UsersRealnameDTO usersRealname) {
        return usersRealnameService.showRenZhengInfo(usersRealname);
    }

    /**
     * 新增个人认证
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(UsersRealname usersRealname) {
        usersRealnameService.insert(usersRealname);
        return SUCCESS_TIP;
    }

    /**
     * 删除个人认证
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer usersRealnameId) {
        usersRealnameService.deleteById(usersRealnameId);
        return SUCCESS_TIP;
    }

    /**
     * 修改个人认证
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(@RequestParam(value="realId") Integer realId,@RequestParam(value="auditStatus") Integer auditStatus,
    		@RequestParam(value="auditRemark") String auditRemark) {
    	UsersRealname usersRealname = new UsersRealname();
    	usersRealname.setRealId(realId);
    	usersRealname.setAuditStatus(auditStatus);
    	usersRealname.setAuditRemark(auditRemark);
        usersRealnameService.updateById(usersRealname);
        return SUCCESS_TIP;
    }

    /**
     * 个人认证详情
     */
    @RequestMapping(value = "/detail/{usersRealnameId}")
    @ResponseBody
    public Object detail(@PathVariable("usersRealnameId") Integer usersRealnameId) {
        return usersRealnameService.selectById(usersRealnameId);
    }
    
    /********************首页************************/
    /**
     * 个人认证详情
     */
    @RequestMapping(value = "/selectUserCardId")
    @ResponseBody
    public Result selectUserCardId(@RequestParam("userId") Integer userId) {
    	Map<String, Object> selectUserCardId = usersRealnameService.selectUserCardId(userId);
        return Result.OK(selectUserCardId);
        
    }
    
    /**
     * 个人认证
     */
    @RequestMapping(value = "/addUserRealName")
    @ResponseBody
    public Result addUserRealName(UsersRealname usersRealname) {
    	String quickApproveBank = usersRealnameService.addUserRealName(usersRealname);
    	return ("1".equals(quickApproveBank))?Result.OK("成功"):Result.EEROR("人工认证失败！");
    }
}