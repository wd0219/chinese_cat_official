package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysql.fabric.xmlrpc.base.Array;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.dao.SktStaffsMapper;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustryDTO;
import com.stylefeng.guns.modular.huamao.model.SktStaffs;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ILogCashService;
import com.stylefeng.guns.modular.huamao.service.ISktStaffsService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商城职员表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-25
 */
@Service
public class SktStaffsServiceImpl extends ServiceImpl<SktStaffsMapper, SktStaffs> implements ISktStaffsService {
	@Autowired
	private SktStaffsMapper sktStaffsMapper;
	
	@Autowired
	private IUsersService usersService;
	
	@Autowired
	private ILogCashService logCashService;
	
	@Override
	public List<SktShopIndustryDTO> sktStaffsfindAll(SktShopIndustryDTO sktShopIndustryDTO) {
		return sktStaffsMapper.sktStaffsfindAll(sktShopIndustryDTO);
	}
	@Override
	public List<SktShopIndustryDTO> sktStaffsfindup(SktShopIndustryDTO sktShopIndustryDTO) {
		return sktStaffsMapper.sktStaffsfindup(sktShopIndustryDTO);
	}
	@Override
	public void sktStaffsupdate(SktShopIndustryDTO sktShopIndustryDTO) {
		sktStaffsMapper.sktStaffsupdate(sktShopIndustryDTO);	
	}
	@Override
	public void sktStaffsup1(SktShopIndustryDTO sktShopIndustryDTO) {
		sktStaffsMapper.sktStaffsup1(sktShopIndustryDTO);		
	}
	/**
     * 分享链接
     * @param userid
     * @return
     */
	@Transactional
	@Override
	public List<Map<String,Object>> sharingRecords(Integer userid, String loginName, String userPhone, Integer userType, String beginTime, String endTime) {
		// TODO Auto-generated method stub
		
		List<Map<String, Object>> rlist = new ArrayList<Map<String, Object>>();
		Users users = new Users();
		users.setInviteId(userid);
		if(ToolUtil.isNotEmpty(userType)){
			users.setUserType(userType);
		}
		EntityWrapper<Users> ew=new EntityWrapper<Users>(users);
		if(ToolUtil.isNotEmpty(loginName)){
			ew.like("loginName", loginName);
		}
		if(ToolUtil.isNotEmpty(userPhone)){
			ew.like("userPhone", userPhone);
		}
		if(ToolUtil.isNotEmpty(beginTime)){
			ew.gt("createTime", beginTime);
		}
		if(ToolUtil.isNotEmpty(endTime)){
			ew.lt("createTime", endTime);
		}
		ew.orderBy("createTime", false);
		List<Users> list =  usersService.selectList(ew);
		for(Users luser:list){
			Map<String, Object> map = new HashMap<String, Object>();
			List<LogCash> lLogCash = sktStaffsMapper.selectLogCash(luser.getUserId());
			List<LogScore> lLogScore = sktStaffsMapper.selectLogScore(luser.getUserId());
			if(lLogCash.isEmpty()){
				map.put("cash", "");
			}else{
				LogCash logCash = lLogCash.get(0);
				map.put("cash", logCash.getCash());
			}
			
			if(lLogScore.isEmpty()){
				map.put("score", "");
			}else{
				LogScore logScore = lLogScore.get(0);
				map.put("score", logScore.getScore());
			}
			
			
			int shareNumber = sktStaffsMapper.selectShareNumber(luser.getUserId());
			map.put("loginName", luser.getLoginName());
			map.put("userPhone", luser.getUserPhone());
			map.put("createTime", luser.getCreateTime());
			map.put("userType", luser.getUserType());
			
			
			map.put("shareNumber", shareNumber);
			rlist.add(map);
		}
		return rlist;
	}

}
