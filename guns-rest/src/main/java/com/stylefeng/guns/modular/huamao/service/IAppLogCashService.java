package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.result.AppResult;

import java.math.BigDecimal;
import java.util.Map;

public interface IAppLogCashService extends IService<LogCash> {

    AppResult AccountCashRecord(Integer pageIndex,LogCash logCash);

    LogCash selectSumCash(Map<String, Object> map);

}
