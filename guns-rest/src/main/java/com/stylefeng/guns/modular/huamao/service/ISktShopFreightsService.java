package com.stylefeng.guns.modular.huamao.service;


import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShopFreights;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 店铺运费表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-06-08
 */
public interface ISktShopFreightsService extends IService<SktShopFreights> {

    List<Map<String,Object>> getFreight(List<Integer> shopIds, Integer areaId2);
}
