package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SpecItems;

import java.util.Map;

/**
 * <p>
 * 商品规格值表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface ISpecItemsService extends IService<SpecItems> {
    public Map<String, Object> goodsDetailType(Integer goodsId );
}
