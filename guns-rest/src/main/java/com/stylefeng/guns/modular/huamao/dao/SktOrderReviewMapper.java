package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.modular.huamao.model.SktOrderReview;
import com.stylefeng.guns.modular.huamao.model.SktOrderReviewDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商城订单审核表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-09-19
 */
public interface SktOrderReviewMapper extends BaseMapper<SktOrderReview> {

    List<SktOrderReviewDTO> selectAll(@Param("page") Page<SktOrderReviewDTO> page, @Param("sktOrderReviewDTO") SktOrderReviewDTO sktOrderReviewDTO);

    Integer selectAllCount(SktOrderReviewDTO sktOrderReviewDTO);
}
