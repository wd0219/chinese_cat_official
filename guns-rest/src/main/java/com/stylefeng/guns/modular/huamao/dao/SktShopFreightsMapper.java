package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopFreights;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 店铺运费表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-06-08
 */
public interface SktShopFreightsMapper extends BaseMapper<SktShopFreights> {

    SktShopFreights getFreigth(Map<String, Object> map);
}
