package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogCashFreeze;
import com.stylefeng.guns.modular.huamao.model.TradeDictDTD;

import java.util.List;

public interface IAppLogCashFreezeService extends IService<LogCashFreeze>{

    List<LogCashFreeze> AccountWillCashRecord(Integer userId);

    TradeDictDTD selectTradeDictByType(Integer type);
}
