package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersStoresMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;
import com.stylefeng.guns.modular.huamao.service.IAppOrderStoresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppOrderStoresServiceImpl extends ServiceImpl<SktOrdersStoresMapper,SktOrdersStores> implements IAppOrderStoresService {
    @Autowired
    private SktOrdersStoresMapper sktOrdersStoresMapper;

    @Override
    public List<Map<String, Object>> selectOrderStoresByMap(Map<String, Object> map) {
        return sktOrdersStoresMapper.selectOrderStoresByMap(map);
    }
}
