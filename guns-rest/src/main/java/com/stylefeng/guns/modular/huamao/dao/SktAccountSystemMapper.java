package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktAccountSystem;


/**
 * <p>
  * 系统账户表 Mapper 接口
 * </p>
 *
 * @author wd123
 * @since 2018-05-25
 */
public interface SktAccountSystemMapper extends BaseMapper<SktAccountSystem> {

}