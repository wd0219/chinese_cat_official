package com.stylefeng.guns.modular.huamao.model;

public class SktOrderScore extends SktOrders{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8560536450625189493L;
	private String score;

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}
}
