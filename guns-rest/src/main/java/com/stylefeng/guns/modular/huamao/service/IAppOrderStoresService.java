package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStores;

import java.util.List;
import java.util.Map;

public interface IAppOrderStoresService extends IService<SktOrdersStores> {
    List<Map<String,Object>> selectOrderStoresByMap(Map<String, Object> map);
}
