package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktSalesMonthMapper;
import com.stylefeng.guns.modular.huamao.model.SktSalesMonth;
import com.stylefeng.guns.modular.huamao.service.ISktSalesMonthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商家每月营业额统计表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
@Service
public class SktSalesMonthServiceImpl extends ServiceImpl<SktSalesMonthMapper, SktSalesMonth> implements ISktSalesMonthService {
	@Autowired
	private SktSalesMonthMapper sktSalesMonthMapper;
	@Override
	public List<SktSalesMonth> sktSalesMonthfindAll(SktSalesMonth sktSalesMonth) {
		return sktSalesMonthMapper.sktSalesMonthfindAll(sktSalesMonth);
		 
	}

}
