package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsMapper;
import com.stylefeng.guns.modular.huamao.model.AgentsBankcards;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgent;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgentList;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgent;
import com.stylefeng.guns.modular.huamao.model.LogScoreAgentList;
import com.stylefeng.guns.modular.huamao.model.SktAgents;
import com.stylefeng.guns.modular.huamao.model.SktAgentsDTO;
import com.stylefeng.guns.modular.huamao.model.SktAgentsProfit;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderDTO;
import com.stylefeng.guns.modular.huamao.model.SktAgentsStockholderRet;
import com.stylefeng.guns.modular.huamao.service.ISktAgentsService;
import com.stylefeng.guns.modular.huamao.util.StringUtils;

/**
 * <p>
 * 代理公司表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
@Service
public class SktAgentsServiceImpl extends ServiceImpl<SktAgentsMapper, SktAgents> implements ISktAgentsService {
	
	@Autowired
	private SktAgentsMapper sktAgentsMapper;
	@Override
	public List<SktAgentsDTO> showSktAgentsIno(SktAgentsDTO sktAgentsDTO) {
		List<SktAgentsDTO> list = sktAgentsMapper.showSktAgentsIno(sktAgentsDTO);
		return list;
	}
	@Override
	public Map<String, Object> selectNameOrPhone(String nameOrPhone) {
		Map<String, Object> map = sktAgentsMapper.selectNameOrPhone(nameOrPhone);
		return map;
	}

	@Override
	public Map<String, Object> agentsLogin(String loginNameOrPhone, String loginPwd) {
		//先查出用户名密码
		SktAgents sktAgents = sktAgentsMapper.selectSecret(loginNameOrPhone);
		if(StringUtils.isEmpty(sktAgents)){
			return null;
		}
		String pwd = loginPwd + sktAgents.getLoginSecret();
		String sysPwd = MD5Util.encrypt(pwd);
		if(!sktAgents.getLoginPwd().equals(sysPwd)){
			return  null;
		}
		loginPwd = sysPwd;
		//加盐处理
		Map<String,Object> map = sktAgentsMapper.agentsLogin(loginNameOrPhone,loginPwd);
		return map;
	}

    @Override
    public Map<String, Object> selectAgentsInfo(SktAgents sktAgents) {
        Map<String,Object> map = sktAgentsMapper.selectAgentsInfo(sktAgents);
        return map;
	}

	@Override
	public List<LogKaiyuanAgent> selectAgentsHuabao(LogKaiyuanAgentList logKaiyuanAgentList) {
		List<LogKaiyuanAgent> list = sktAgentsMapper.selectAgentsHuabao(logKaiyuanAgentList);
		return list;
	}

	@Override
	public List<LogScoreAgent> selectAgentsscore(LogScoreAgentList logScoreAgentList) {
		List<LogScoreAgent> list = sktAgentsMapper.selectAgentsscore(logScoreAgentList);
		return list;
	}
	@Override
	public List<SktAgentsStockholderRet> selectAgentsStockholder(SktAgentsStockholderDTO sktAgentsStockholderDTO) {
		List<SktAgentsStockholderRet> list = sktAgentsMapper.selectAgentsStockholder(sktAgentsStockholderDTO);
		return list;
	}

	@Override
	public List<Map<String, Object>> selectAccountShop(Integer agentId, String shopName,Integer pageNum) {
			// 根据ID差出代理公司
			SktAgents sktAgents = sktAgentsMapper.selectById(agentId);
			if(ToolUtil.isEmpty(sktAgents)){
				return  new ArrayList<>();
			}
			List<Integer> list = sktAgentsMapper.selectShopIdList(sktAgents);
			PageHelper.startPage(pageNum, 12);
			List<Map<String,Object>> list2 = sktAgentsMapper.selectAccountShop(list,shopName);
			return list2;
	}
	@Override
	public List<AgentsBankcards> selectAgentBankcards(AgentsBankcards agentsBankcards) {
		List<AgentsBankcards> list = sktAgentsMapper.selectAgentBankcards(agentsBankcards);
		return list;
	}
	@Override
	public Integer updateAgentPwd(SktAgentsDTO sktAgents) {
		//查询登录名密码 盐
		SktAgents sktAgents2 = sktAgentsMapper.selectById(sktAgents.getAgentId());
		if(ToolUtil.isEmpty(sktAgents2)){
			return null;
		}
		//比对密码
		String saltPwd = MD5Util.encrypt(sktAgents.getLoginPwd()+sktAgents2.getLoginSecret());
		if(!saltPwd.equals(sktAgents2.getLoginPwd())){
			return null;
		}
		String newSaltPwd = MD5Util.encrypt(sktAgents.getNewPwd()+sktAgents2.getLoginSecret());
		sktAgents2.setLoginPwd(newSaltPwd);
		Integer i = sktAgentsMapper.updateById(sktAgents2);
		return i;
	}
	@Override
	public Integer updateAgentPayPwd(SktAgentsDTO sktAgents) {
		//查询登录名密码 盐
		SktAgents sktAgents2 = sktAgentsMapper.selectById(sktAgents.getAgentId());
		if(ToolUtil.isEmpty(sktAgents2)){
			return null;
		}
		String newSaltPwd = MD5Util.encrypt(sktAgents.getNewPwd()+sktAgents2.getLoginSecret());
		sktAgents2.setPayPwd(newSaltPwd);
		Integer i = sktAgentsMapper.updateById(sktAgents2);
		return i;
	}
	@Override
	public List<SktAgentsProfit> selectAgentsProfit(Integer userId) {
		List<SktAgentsProfit> list = sktAgentsMapper.selectAgentsProfit(userId);
		return list;
	}

	@Override
	public List<Map<String, Object>> selectShopInfo(Integer shopId) {
		List<Map<String,Object>> list = sktAgentsMapper.selectShopInfo(shopId);
		return list;
	}

}
