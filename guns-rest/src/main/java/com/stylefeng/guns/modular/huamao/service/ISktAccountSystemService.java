package com.stylefeng.guns.modular.huamao.service;


import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktAccountSystem;

/**
 * <p>
 * 系统账户表 服务类
 * </p>
 *
 * @author wd123
 * @since 2018-05-25
 */
public interface ISktAccountSystemService extends IService<SktAccountSystem> {
	
}
