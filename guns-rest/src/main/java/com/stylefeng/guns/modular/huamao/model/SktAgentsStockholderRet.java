package com.stylefeng.guns.modular.huamao.model;

public class SktAgentsStockholderRet {
	private String shId;
	private String userId;
	private String loginName;
	private String trueName;
	private String userPhone;
	private String stype; 
	private String stockNum; 
	private String storeNum; 
	private String lastDividend; 
	private String dividendTime;
	public String getShId() {
		return shId;
	}
	public void setShId(String shId) {
		this.shId = shId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getStype() {
		return stype;
	}
	public void setStype(String stype) {
		this.stype = stype;
	}
	public String getStockNum() {
		return stockNum;
	}
	public void setStockNum(String stockNum) {
		this.stockNum = stockNum;
	}
	public String getStoreNum() {
		return storeNum;
	}
	public void setStoreNum(String storeNum) {
		this.storeNum = storeNum;
	}
	public String getLastDividend() {
		return lastDividend;
	}
	public void setLastDividend(String lastDividend) {
		this.lastDividend = lastDividend;
	}
	public String getDividendTime() {
		return dividendTime;
	}
	public void setDividendTime(String dividendTime) {
		this.dividendTime = dividendTime;
	}
	
}
