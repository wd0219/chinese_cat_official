package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统消息表扩展类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public class MessagesList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增ID
	 */
	private Integer id;
	/**
	 * 消息类型
	 */
	private Integer msgType;
	/**
	 * 发送者Id
	 */
	private Integer sendUserId;
	/**
	 * 发送者
	 */
	private String sendName;
	/**
	 * 接受者Id
	 */
	private Integer receiveUserId;
	/**
	 * 接受者
	 */
	private String receiveName;
	/**
	 * 消息标题
	 */
	private String msgTitle;
	/**
	 * 消息内容
	 */
	private String msgContent;
	/**
	 * 阅读状态
	 */
	private Integer msgStatus;
	/**
	 * 存放json数据
	 */
	private String msgJson;
	/**
	 * 有效状态
	 */
	private Integer dataFlag;
	/**
	 * 发送时间
	 */
	private Date createTime;

	public MessagesList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MessagesList(Integer id, Integer msgType, Integer sendUserId, String sendName, Integer receiveUserId,
			String receiveName, String msgTitle, String msgContent, Integer msgStatus, String msgJson, Integer dataFlag,
			Date createTime) {
		super();
		this.id = id;
		this.msgType = msgType;
		this.sendUserId = sendUserId;
		this.sendName = sendName;
		this.receiveUserId = receiveUserId;
		this.receiveName = receiveName;
		this.msgTitle = msgTitle;
		this.msgContent = msgContent;
		this.msgStatus = msgStatus;
		this.msgJson = msgJson;
		this.dataFlag = dataFlag;
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMsgType() {
		return msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}

	public Integer getSendUserId() {
		return sendUserId;
	}

	public void setSendUserId(Integer sendUserId) {
		this.sendUserId = sendUserId;
	}

	public String getSendName() {
		return sendName;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public Integer getReceiveUserId() {
		return receiveUserId;
	}

	public void setReceiveUserId(Integer receiveUserId) {
		this.receiveUserId = receiveUserId;
	}

	public String getReceiveName() {
		return receiveName;
	}

	public void setReceiveName(String receiveName) {
		this.receiveName = receiveName;
	}

	public String getMsgTitle() {
		return msgTitle;
	}

	public void setMsgTitle(String msgTitle) {
		this.msgTitle = msgTitle;
	}

	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public Integer getMsgStatus() {
		return msgStatus;
	}

	public void setMsgStatus(Integer msgStatus) {
		this.msgStatus = msgStatus;
	}

	public String getMsgJson() {
		return msgJson;
	}

	public void setMsgJson(String msgJson) {
		this.msgJson = msgJson;
	}

	public Integer getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "MessagesList [id=" + id + ", msgType=" + msgType + ", sendUserId=" + sendUserId + ", sendName="
				+ sendName + ", receiveUserId=" + receiveUserId + ", receiveName=" + receiveName + ", msgTitle="
				+ msgTitle + ", msgContent=" + msgContent + ", msgStatus=" + msgStatus + ", msgJson=" + msgJson
				+ ", dataFlag=" + dataFlag + ", createTime=" + createTime + "]";
	}

}
