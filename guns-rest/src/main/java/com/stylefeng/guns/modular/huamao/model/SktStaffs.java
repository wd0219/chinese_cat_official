package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 商城职员表
 * </p>
 *
 * @author ck123
 * @since 2018-04-25
 */
@TableName("skt_staffs")
public class SktStaffs extends Model<SktStaffs> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "staffId", type = IdType.AUTO)
    private Integer staffId;
    /**
     * 账号
     */
    private String loginName;
    /**
     * 密码
     */
    private String loginPwd;
    /**
     * 安全码（安全码用于和用户密码混合在一起加密）
     */
    private Integer secretKey;
    /**
     * 职员名称
     */
    private String staffName;
    /**
     * 职员编号
     */
    private String staffNo;
    /**
     * 职员头像
     */
    private String staffPhoto;
    /**
     * 职员角色ID
     */
    private Integer staffRoleId;
    /**
     * 工作状态：0离职1在职
     */
    private Integer workStatus;
    /**
     * 职员状态：1正常0停用
     */
    private Integer staffStatus;
    /**
     * 删除标志：1有效-1删除
     */
    private Integer dataFlag;
    /**
     * 建立时间
     */
    private Date createTime;
    /**
     * 最后登陆时间
     */
    private Date lastTime;
    /**
     * 最后登陆IP
     */
    private String lastIP;


    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public Integer getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(Integer secretKey) {
        this.secretKey = secretKey;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public String getStaffPhoto() {
        return staffPhoto;
    }

    public void setStaffPhoto(String staffPhoto) {
        this.staffPhoto = staffPhoto;
    }

    public Integer getStaffRoleId() {
        return staffRoleId;
    }

    public void setStaffRoleId(Integer staffRoleId) {
        this.staffRoleId = staffRoleId;
    }

    public Integer getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(Integer workStatus) {
        this.workStatus = workStatus;
    }

    public Integer getStaffStatus() {
        return staffStatus;
    }

    public void setStaffStatus(Integer staffStatus) {
        this.staffStatus = staffStatus;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastIP() {
        return lastIP;
    }

    public void setLastIP(String lastIP) {
        this.lastIP = lastIP;
    }

    @Override
    protected Serializable pkVal() {
        return this.staffId;
    }

    @Override
    public String toString() {
        return "SktStaffs{" +
        "staffId=" + staffId +
        ", loginName=" + loginName +
        ", loginPwd=" + loginPwd +
        ", secretKey=" + secretKey +
        ", staffName=" + staffName +
        ", staffNo=" + staffNo +
        ", staffPhoto=" + staffPhoto +
        ", staffRoleId=" + staffRoleId +
        ", workStatus=" + workStatus +
        ", staffStatus=" + staffStatus +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", lastTime=" + lastTime +
        ", lastIP=" + lastIP +
        "}";
    }
}
