package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.*;


/**
 * <p>
 * 订单记录表 Mapper 接口
 * </p>
 *
 * @author liuduan
 * @since 2018-06-19
 */
public interface LogOrdersMapper extends BaseMapper<LogOrders> {

}
