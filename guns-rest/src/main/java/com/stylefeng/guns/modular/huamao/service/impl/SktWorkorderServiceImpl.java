package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktWorkorderMapper;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;
import com.stylefeng.guns.modular.huamao.service.ISktWorkorderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 工单表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
@Service
public class SktWorkorderServiceImpl extends ServiceImpl<SktWorkorderMapper, SktWorkorder> implements ISktWorkorderService {
	@Autowired
	private SktWorkorderMapper sktWorkorderMapper;
	@Override
	public List<SktWorkorder> sktWorkorderfindAll(SktWorkorder sktWorkorder) {

		return sktWorkorderMapper.sktWorkorderfindAll(sktWorkorder);
	}
	@Override
	public List<SktWorkorder> sktWorkorderfindup(SktWorkorder sktWorkorder) {
		return sktWorkorderMapper.sktWorkorderfindup(sktWorkorder);
	}

}
