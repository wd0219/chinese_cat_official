package com.stylefeng.guns.modular.huamao.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktClass;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商学院表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-07-26
 */
public interface SktClassMapper extends BaseMapper<SktClass> {

    SktClass selectSktClassByClassName(String className);

    List<Map<String,Object>> selectClassByTypeOrAll(Map<String,Object> map);

    List<Map<String,Object>> selectSearchVideo(Map<String,Object> sktClass);

    List<Map<String,Object>> selectPageClass(Map<String, Object> map);

    Map<String,Object> selectClassByClassId(SktClass sktClass);

    List<Map<String,Object>> selectBatchClassByClassId(Map<String, Object> map);
}
