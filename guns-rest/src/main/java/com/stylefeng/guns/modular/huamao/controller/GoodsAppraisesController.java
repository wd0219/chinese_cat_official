package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesCustm;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IGoodsAppraisesService;

/**
 * 商品评价控制器
 *
 * @author fengshuonan
 * @Date 2018-04-10 21:30:06
 */
@CrossOrigin
@Controller
@RequestMapping("/goodsAppraises")
public class GoodsAppraisesController extends BaseController {

    private String PREFIX = "/huamao/goodsAppraises/";

    @Autowired
    private IGoodsAppraisesService goodsAppraisesService;

    /**
     * 跳转到商品评价首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "goodsAppraises.html";
    }

    /**
     * 跳转到添加商品评价
     */
    @RequestMapping("/goodsAppraises_add")
    public String goodsAppraisesAdd() {
        return PREFIX + "goodsAppraises_add.html";
    }

    /**
     * 跳转到修改商品评价
     */
    @RequestMapping("/goodsAppraises_update/{goodsAppraisesId}")
    public String goodsAppraisesUpdate(@PathVariable Integer goodsAppraisesId, Model model) {
        GoodsAppraises goodsAppraises = goodsAppraisesService.selectById(goodsAppraisesId);
        model.addAttribute("item",goodsAppraises);
        LogObjectHolder.me().set(goodsAppraises);
        return PREFIX + "goodsAppraises_edit.html";
    }

    /**
     * 获取商品评价列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(GoodsAppraisesCustm goodsAppraisesCustm) {
        List<Map<String, Object>> goodsAppraiseslist = goodsAppraisesService.selectAll(goodsAppraisesCustm);
        return goodsAppraiseslist;
       // return new GoodsAppraisesWapper(goodsAppraiseslist).warp();
    }

    /**
     * 删除商品评价
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer goodsAppraisesId) {
        goodsAppraisesService.deleteById(goodsAppraisesId);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品评价
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(GoodsAppraises goodsAppraises) {
        goodsAppraisesService.updateById(goodsAppraises);
        return SUCCESS_TIP;
    }

    /**
     * 商品评价详情
     */
    @RequestMapping(value = "/detail/{goodsAppraisesId}")
    @ResponseBody
    public Object detail(@PathVariable("goodsAppraisesId") Integer goodsAppraisesId) {
        return goodsAppraisesService.selectById(goodsAppraisesId);
    }
    
    /*******************首页*******************/
    /**
     * 新增商品评价
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Result add(GoodsAppraises goodsAppraises) {
    	goodsAppraises.setCreateTime(new Date());
        boolean insert = goodsAppraisesService.insertAdd(goodsAppraises);
        return insert==true?Result.OK(null):Result.EEROR("新增评论失败！");
    }
}
