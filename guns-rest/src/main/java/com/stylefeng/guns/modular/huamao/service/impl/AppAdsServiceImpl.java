package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AdsMapper;
import com.stylefeng.guns.modular.huamao.model.Ads;
import com.stylefeng.guns.modular.huamao.service.IAppAdsService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppAdsServiceImpl extends ServiceImpl<AdsMapper,Ads> implements IAppAdsService {
    @Autowired
    private AdsMapper adsMapper;
    @Autowired
    private CacheUtil cacheUtil;
    @Override
    public List<Ads> findAds(int positionId, int num, String string) {
        if (positionId == 0) {
            positionId = 4;
        }
        if (num == 0) {
            num = 3;
        }
        List<Ads> ads=adsMapper.findAds(positionId,num);
        cacheUtil.set(string,ads);
        return ads;
    }
}
