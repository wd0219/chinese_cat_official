package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktComment;

import java.util.List;
import java.util.Map;

public interface IAppSktCommentService extends IService<SktComment> {
    List<Map<String,Object>> selectListByClassId(Map<String, Object> map);

    List<Map<String,Object>> selectClassByComment(Map<String, Object> map);
}
