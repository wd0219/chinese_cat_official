package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 行业管理表
 * </p>
 *
 * @author ck123123
 * @since 2018-04-19
 */
@TableName("skt_shop_industry")
public class SktShopIndustry extends Model<SktShopIndustry> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "hyId", type = IdType.AUTO)
    private Integer hyId;
    /**
     * 行业名称
     */
    private String hyName;
    /**
     * 状态：0隐藏1显示
     */
    private Integer status;
    /**
     * 是否常用： 1常用 2不常用
     */
    private Integer hot;
    /**
     * 首字母
     */
    private String initial;
    /**
     * 图标
     */
    private String icon;
    /**
     * 排序
     */

    private Integer sort;


    public Integer getHyId() {
        return hyId;
    }

    public void setHyId(Integer hyId) {
        this.hyId = hyId;
    }

    public String getHyName() {
        return hyName;
    }

    public void setHyName(String hyName) {
        this.hyName = hyName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getHot() {
        return hot;
    }

    public void setHot(Integer hot) {
        this.hot = hot;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    protected Serializable pkVal() {
        return this.hyId;
    }

    @Override
    public String toString() {
        return "SktShopIndustry{" +
        "hyId=" + hyId +
        ", hyName=" + hyName +
        ", status=" + status +
        ", hot=" + hot +
        ", initial=" + initial +
        ", icon=" + icon +
        ", sort=" + sort +
        "}";
    }
}
