package com.stylefeng.guns.modular.huamao.service;

import com.alibaba.fastjson.JSONObject;

/**
 * 阿里云短信发送接口
 */
public interface ISmsService {

    /**
     * 短信验证码接口
     */
    public JSONObject send_register_message(String phone, String ip, Integer userId);

}
