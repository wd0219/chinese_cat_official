package com.stylefeng.guns.modular.huamao.model;

public class ArticlesCustem extends Articles{
	private Integer article1;
	private Integer article2;
	private Integer article3;
	public Integer getArticle1() {
		return article1;
	}
	public void setArticle1(Integer article1) {
		this.article1 = article1;
	}
	public Integer getArticle2() {
		return article2;
	}
	public void setArticle2(Integer article2) {
		this.article2 = article2;
	}
	public Integer getArticle3() {
		return article3;
	}
	public void setArticle3(Integer article3) {
		this.article3 = article3;
	}
	
	
}
