package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.AdsMapper;
import com.stylefeng.guns.modular.huamao.model.Ads;
import com.stylefeng.guns.modular.huamao.model.AdsList;
import com.stylefeng.guns.modular.huamao.service.IAdsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 广告表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
@Service
public class AdsServiceImpl extends ServiceImpl<AdsMapper, Ads> implements IAdsService {

	@Autowired
	private AdsMapper am;

	// 广告表展示
	@Override
	public List<AdsList> selectAdsAll(AdsList adsList) {
		return am.selectAdsAll(adsList);
	}

	// 异步获取广告位置
	@Override
	public List<AdsList> getPosition(String positionType) {
		return am.getPosition(positionType);
	}

	// 添加广告
	@Override
	public void addAds(AdsList adsList) {
		am.addAds(adsList);
	}

	// 通过id查询广告为修改作准备
	@Override
	public AdsList selectAdById(Integer adId) {
		return am.selectAdById(adId);
	}

	// 通过id修改广告
	@Override
	public void updateAdById(AdsList adsList) {
		am.updateAdById(adsList);
	}
/*************************************App接口*****************************************/
	@Override
	public List<Map<String,Object>> getSiteHome(Map<String,Object> map){
		return  am.selectAppAds(map);
	}

	@Override
	public List<Ads> selectPcTopAds(Map<String, Object> map) {
		return am.selectPcTopAds(map);
	}

	@Override
	public List<Map<String, Object>> siteWelcome(Integer positionId, Integer num) {
		List<Map<String, Object>> list = am.siteWelcome(positionId,num);
		return list;
	}

}
