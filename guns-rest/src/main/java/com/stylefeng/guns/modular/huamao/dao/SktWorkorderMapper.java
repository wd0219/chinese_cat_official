package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 工单表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
public interface SktWorkorderMapper extends BaseMapper<SktWorkorder> {
	
	public List<SktWorkorder> sktWorkorderfindAll(SktWorkorder sktWorkorder);
	
	public List<SktWorkorder> sktWorkorderfindup(SktWorkorder sktWorkorder);

	SktWorkorder selectByOrderNo(Map<String, Object> orderWhere);

	public List<Map<String, Object>> selectRecord();

	public Map<String, Object> selWorkordeData(Map<String, Object> map);

	public String getWorkorder(Map<String, Object> map);

	public String getShare(Map<String, Object> map);

	public Map<String, Object> selectTuiJianInfo(Map<String, Object> map);

	public SktWorkorder getPhone(Map<String, Object> map);
}
