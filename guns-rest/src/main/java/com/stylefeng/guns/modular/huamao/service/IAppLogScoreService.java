package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AppTradeDictDTD;
import com.stylefeng.guns.modular.huamao.model.LogScore;

import java.util.List;
import java.util.Map;

public interface IAppLogScoreService extends IService<LogScore> {
    LogScore selectSumLogScore(Map<String, Object> map);

    LogScore selectEverySumLogScore(Map<String, Object> map);

    Double selectSumScoreByUserId(Map<String, Object> map);

    List<LogScore> selectLogScoreByUserId(Map<String, Object> map);

    AppTradeDictDTD selectTradeDictBygType(String gType,Integer type);
}
