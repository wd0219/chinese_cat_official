package com.stylefeng.guns.modular.huamao.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.UUID;

public class SessionUtils {


    public static String getSessionId(){
     HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("token");
        if(StringUtils.isEmpty(token)){
            token=TokenGenerate.GetGUID();
        }
        return token;
    }
}
