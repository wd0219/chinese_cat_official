package com.stylefeng.guns.modular.huamao.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;

/**
 * <p>
 * 商品分类表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsCatsMapper extends BaseMapper<GoodsCats> {
	List<GoodsCats> selectByParentId(int parentId);
	boolean deleteByPId(Integer pid);
	List<Map<String,Object>> selectMap(GoodsCats goodsCats);
	List<Brands> selectCatName(@Param(value = "sIds") String sIds);
	List<Map<String,Object>>  selectGoodsCats(Integer parentId);
	List<Map<String,Object>>	selectSecondCats(Integer parentId);
//	List<Map<String,Object>>	selectThirdCats(Integer parentId);
	List<Map<String, Object>> getGoodscatsSelGoods(@Param(value="stre")String[] stre,
			@Param(value="shopPrice1")Integer shopPrice1, 
			@Param(value="shopPrice2")Integer shopPrice2, 
			@Param(value="saleNum")Integer saleNum, 
			@Param(value="shopPrice")Integer shopPrice, 
			@Param(value="visitNum")Integer visitNum, 
			@Param(value="appraiseNum")Integer appraiseNum, 
			@Param(value="ssaleTime")Integer ssaleTime,
			@Param(value="isNew")Integer isNew,
			@Param(value="goodsStock")Integer goodsStock, 
			@Param(value="arrList")ArrayList<String> arrList, 
			@Param(value="goodsName") String goodsName, 
			@Param(value="isSelf") Integer isSelf);
	
	List<Map<String, Object>> selectShopCatsGoods(@Param(value="shopId")Integer shopId, 
			@Param(value="shopCatId")Integer shopCatId, 
			@Param(value="shopPrice1")Integer shopPrice1, 
			@Param(value="shopPrice2")Integer shopPrice2, 
			@Param(value="saleNum")Integer saleNum, 
			@Param(value="shopPrice")Integer shopPrice, 
			@Param(value="visitNum")Integer visitNum, 
			@Param(value="appraiseNum")Integer appraiseNum, 
			@Param(value="ssaleTime")Integer ssaleTime,
			@Param(value="isNew")Integer isNew,
			@Param(value="goodsStock")Integer goodsStock, 
			@Param(value="goodsName")String goodsName);
	
	Map<String, Object> selectShopsCores(@Param(value="shopId")Integer shopId);

    public List<Map<String,Object>> showFloor();
}
