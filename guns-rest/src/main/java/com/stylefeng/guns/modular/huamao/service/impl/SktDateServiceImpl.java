package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktDateMapper;
import com.stylefeng.guns.modular.huamao.model.SktDate;
import com.stylefeng.guns.modular.huamao.service.ISktDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SktDateServiceImpl extends ServiceImpl<SktDateMapper,SktDate> implements ISktDateService {
    @Autowired
    private SktDateMapper sktDateMapper;

    @Override
    public SktDate selectDateByDate(int date) {
        return sktDateMapper.selectDateByDate(date);
    }
}
