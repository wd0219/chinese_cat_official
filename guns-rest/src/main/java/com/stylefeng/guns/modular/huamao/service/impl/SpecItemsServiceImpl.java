package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SpecItemsMapper;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.SpecItems;
import com.stylefeng.guns.modular.huamao.service.ISpecItemsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规格值表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class SpecItemsServiceImpl extends ServiceImpl<SpecItemsMapper, SpecItems> implements ISpecItemsService {


    List<Map<String, Object>> selectCatName(Integer goodsId){
        return  baseMapper.selectCatName(goodsId);
    }
@Override
    public Map<String, Object> goodsDetailType(Integer goodsId ){
        List<Map<String, Object>> catName = baseMapper.selectCatName(goodsId);
        Map<String, Object> map1=new HashMap<>();
        map1.put("catName",catName);
        List list=new ArrayList<>();
        List lists=new ArrayList<>();
        List listss=new ArrayList<>();
        if (catName!=null && catName.size()>0){
            for (Map<String, Object> cats:catName){
                Map<String, Object> map2=new HashMap<>();
                List<String> list2=new ArrayList<>();
                List<Integer> list3=new ArrayList<>();
                List<Integer> list4=new ArrayList<>();
                Map<String, Object> parameterMap=new HashMap<>();
                parameterMap.put("goodsId",goodsId);
                parameterMap.put("catId",Integer.parseInt(cats.get("catId").toString()));
                List<Map<String, Object>> itemNames = baseMapper.selectItemName(parameterMap);
                if (itemNames!=null && itemNames.size()>0){
                  for (Map<String, Object> itemName:itemNames){
                      list2.add(itemName.get("itemName").toString());
                      list3.add(Integer.parseInt(itemName.get("itemId").toString()));
                      list4.add(goodsId);
                    //  list.add(list2);
                  }
                    list.add(list2);
                    lists.add(list3);
                    listss.add(list4);
              }

                map1.put("itemName",list);
                map1.put("itemId",lists);
                map1.put("goodsId",listss);
            }
        }
      return  map1;
    }


}
