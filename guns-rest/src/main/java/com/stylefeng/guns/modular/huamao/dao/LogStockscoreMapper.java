package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanTurnoverList;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;
import com.stylefeng.guns.modular.huamao.model.LogStockscoreList;

/**
 * <p>
 * 库存积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface LogStockscoreMapper extends BaseMapper<LogStockscore> {

	// 库存积分流水表展示
	List<LogStockscoreList> selectLogStockScoreAll(LogStockscoreList logStockscoreList);
	/**
	 * 查询库存积分
	 * @param userId
	 * @return
	 */
	List<Map<String, Object>> selectStockscoref(LogStockscoreList logStockscoreList);
	/**
	 * 开元宝
	 * @param logKaiyuanTurnoverList
	 * @return
	 */
	List<Map<String, Object>> selectLogKaiyuanTurnover(LogKaiyuanTurnoverList logKaiyuanTurnoverList);

    List<LogStockscore> selectLogStockScoreByUserId(Map<String, Object> map);
}
