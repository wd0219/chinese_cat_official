package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrdersExtension;


/**
 * <p>
 * 线上商城订单扩展表 服务类
 * </p>
 *
 * @author caody123
 * @since 2018-05-18
 */
public interface ISktOrdersExtensionService extends IService<SktOrdersExtension> {
	
}
