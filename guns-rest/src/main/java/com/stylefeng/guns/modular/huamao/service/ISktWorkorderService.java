package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktWorkorder;

import java.util.List;

/**
 * <p>
 * 工单表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
public interface ISktWorkorderService extends IService<SktWorkorder> {
	
	public List<SktWorkorder> sktWorkorderfindAll(SktWorkorder sktWorkorder);
	
	public List<SktWorkorder> sktWorkorderfindup(SktWorkorder sktWorkorder);
}
