package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreFreezeMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreeze;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreezeList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreFreezeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 待发积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogScoreFreezeServiceImpl extends ServiceImpl<LogScoreFreezeMapper, LogScoreFreeze>
		implements ILogScoreFreezeService {

	@Autowired
	private LogScoreFreezeMapper lsfm;

	// 展示代发积分列表
	@Override
	public List<LogScoreFreezeList> selectLogScoreFreezeAll(LogScoreFreezeList logScoreFreezeList) {
		return lsfm.selectScoreFreezeList(logScoreFreezeList);
	}

    @Override
    public List<Map<String, Object>> selectUserScoreFreeze(LogScoreFreezeList logScoreFreezeList) {
		List<Map<String, Object>> list  = lsfm.selectUserScoreFreeze(logScoreFreezeList);
        return list;
    }
}
