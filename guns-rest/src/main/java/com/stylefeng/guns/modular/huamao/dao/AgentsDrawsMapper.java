package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.AgentsDraws;
import com.stylefeng.guns.modular.huamao.model.AgentsDrawsList;

/**
 * <p>
 * 代理公司提现记录表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface AgentsDrawsMapper extends BaseMapper<AgentsDraws> {

	// 代理公司提现记录表展示
	List<AgentsDrawsList> selectAgentsDrawsAll(AgentsDrawsList agentsDrawsList);

}
