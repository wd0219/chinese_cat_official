package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktBanks;

import java.util.List;
import java.util.Map;

public interface IAppBanksService extends IService<SktBanks> {

    List<Map<String,Object>> selectAllBanks();
}
