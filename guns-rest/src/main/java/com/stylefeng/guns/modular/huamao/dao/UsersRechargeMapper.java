package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;
import com.stylefeng.guns.modular.huamao.model.UsersRechargeDTO;

/**
 * <p>
 * 用户充值表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
public interface UsersRechargeMapper extends BaseMapper<UsersRecharge> {
	/**
	 * 显示用户订单
	 * @param usersRechargeDTO
	 * @return
	 */
	public List<UsersRechargeDTO> showUserDengDanInfo(UsersRechargeDTO usersRechargeDTO);

	//修改充值状态
    Integer updateStatusById(UsersRecharge recharge);

    UsersRecharge selectUsersRechargeByOrderNo(Map<String, Object> orderWhere);

	UsersRecharge selectUsersRechargeByUserId(Map<String, Object> map);
}
