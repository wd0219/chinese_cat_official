package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.modular.huamao.model.Ads;
import com.stylefeng.guns.modular.huamao.service.IAppAdsService;
import com.stylefeng.guns.modular.huamao.service.IAppSktSysConfigService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.GetImageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AppController {
    @Autowired
    private IAppAdsService appAdsService;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private IAppSktSysConfigService iAppSktSysConfigService;

    @RequestMapping("/api/app/{type}")
    public String toRediRect(@RequestBody(required = true) Map<String, Object>  map, @PathVariable("type") String type, RedirectAttributes model) {
        String code = (String) map.get("code");
        code = code.replace("-", "/");
        code = "app/" + code;
        if (map.get("data").toString().equals("")){
            map.put("data","[]");
        }
        if(!map.get("data").toString().equals("[]")){
            JSONObject data = (JSONObject) map.get("data");
            for (Map.Entry<String, Object> entry : data.entrySet()) {
                Object value = entry.getValue();
                if(value instanceof JSONArray){
                    String s = ((JSONArray) value).toJSONString();
                    value =s ;
                }
                model.addAttribute(entry.getKey(),value);
            }
        }
        return "redirect:/"+code;
    }


    /**
     * 获取请求头部信息
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/api/app/getSystemInfo")
    @ResponseBody
    public Map<String,Object> getSystemInfo(HttpServletRequest request, HttpServletResponse response){
        String str="androidVersion";
        String androidVersion = iAppSktSysConfigService.findSysConfigs(str);
        String serviceTel = iAppSktSysConfigService.findSysConfigs("serviceTel");
        String iosVersion = iAppSktSysConfigService.findSysConfigs("iosVersion");
        String isAudit = iAppSktSysConfigService.findSysConfigs("isAudit");
        int positionId=40;
        int num=1;//查询张数
        List<Ads> ads= appAdsService.findAds(positionId,num,"welcome");
        Map map =new HashMap();
            map.put("baseUrl","http://rest.izzht.cn/api");
        //   map.put("baseUrl","http://hm.qilewang.net/api");
        //map.put("baseUrl","http://192.168.3.133:8080/api");
        map.put("versionCode",androidVersion);
       map.put("url","http://rest.izzht.cn/download/app-release.apk");
        //map.put("url","http://192.168.3.145/download/app-release.apk");
        //map.put("url","http://hm.qilewang.net/download/app-release.apk");
        map.put("androidStatus","1");
        map.put("disableContent","老版本停止更新，请下载最新版");
        map.put("update","0");//1强制更新 0无所谓
        map.put("welcomeImg",ads);//引导页面对应图片
        map.put("iosVersion",iosVersion);//iso版本号
        map.put("iosStatus",isAudit);//审核中，或已审核
        map.put("serverPhone",serviceTel);
        Map<String, Object> maps = new HashMap<>();
        maps.put("data",map);
        maps.put("status",1);
        return maps;
    }

    /**
     * 图形验证码
     * @param deviceId
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/api/site/code")
    public void getImagerCode(String deviceId,HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("image/jpeg;charset=UTF-8");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        GetImageCode vCode=new GetImageCode();
        try {
            vCode.GetImageCodes(60,30);
            if(cacheUtil.isKeyInCache("imgCode")){
                cacheUtil.update("imgCode",vCode.getCode());
            }else{
                cacheUtil.set("imgCode",vCode.getCode());
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
        vCode.write(response.getOutputStream());

    }



}
