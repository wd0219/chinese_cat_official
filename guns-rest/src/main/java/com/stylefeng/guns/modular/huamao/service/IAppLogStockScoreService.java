package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogStockscore;

import java.util.List;
import java.util.Map;

public interface IAppLogStockScoreService extends IService<LogStockscore> {
    List<LogStockscore> selectLogStockScoreByUserId(Map<String, Object> map);
}
