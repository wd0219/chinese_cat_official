package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrderReviewMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrderReview;
import com.stylefeng.guns.modular.huamao.model.SktOrderReviewDTO;
import com.stylefeng.guns.modular.huamao.service.ISktOrderReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商城订单审核表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-09-19
 */
@Service
public class SktOrderReviewServiceImpl extends ServiceImpl<SktOrderReviewMapper, SktOrderReview> implements ISktOrderReviewService {

    @Autowired
    private SktOrderReviewMapper sktOrderReviewMapper;

    @Override
    public List<SktOrderReviewDTO> selectAll(Page<SktOrderReviewDTO> page, SktOrderReviewDTO sktOrderReviewDTO) {
        Integer total = sktOrderReviewMapper.selectAllCount(sktOrderReviewDTO);
        page.setTotal(total);
        List<SktOrderReviewDTO> list = sktOrderReviewMapper.selectAll(page,sktOrderReviewDTO);
        return list;
    }
}
