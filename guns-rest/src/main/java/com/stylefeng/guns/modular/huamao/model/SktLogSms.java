package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 短信发送记录表
 * </p>
 *
 * @author caody
 * @since 2018-06-08
 */
@TableName("skt_log_sms")
public class SktLogSms extends Model<SktLogSms> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
	@TableId(value="smsId", type= IdType.AUTO)
	private Integer smsId;
    /**
     * 消息类型
     */
	private Integer smsSrc;
    /**
     * 发送者ID
     */
	private Integer smsUserId;
    /**
     * 短信内容
     */
	private String smsContent;
    /**
     * 短信号码
     */
	private String smsPhoneNumber;
    /**
     * 短信返回值
     */
	private String smsReturnCode;
    /**
     * 短信中的验证码
     */
	private String smsCode;
    /**
     * 调用短信的接口
     */
	private String smsFunc;
    /**
     * IP地址
     */
	private String smsIP;
    /**
     * 创建时间
     */
	private Date createTime;


	public Integer getSmsId() {
		return smsId;
	}

	public void setSmsId(Integer smsId) {
		this.smsId = smsId;
	}

	public Integer getSmsSrc() {
		return smsSrc;
	}

	public void setSmsSrc(Integer smsSrc) {
		this.smsSrc = smsSrc;
	}

	public Integer getSmsUserId() {
		return smsUserId;
	}

	public void setSmsUserId(Integer smsUserId) {
		this.smsUserId = smsUserId;
	}

	public String getSmsContent() {
		return smsContent;
	}

	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}

	public String getSmsPhoneNumber() {
		return smsPhoneNumber;
	}

	public void setSmsPhoneNumber(String smsPhoneNumber) {
		this.smsPhoneNumber = smsPhoneNumber;
	}

	public String getSmsReturnCode() {
		return smsReturnCode;
	}

	public void setSmsReturnCode(String smsReturnCode) {
		this.smsReturnCode = smsReturnCode;
	}

	public String getSmsCode() {
		return smsCode;
	}

	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}

	public String getSmsFunc() {
		return smsFunc;
	}

	public void setSmsFunc(String smsFunc) {
		this.smsFunc = smsFunc;
	}

	public String getSmsIP() {
		return smsIP;
	}

	public void setSmsIP(String smsIP) {
		this.smsIP = smsIP;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.smsId;
	}

}
