package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopApproveMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopApprove;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopApplysMapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopScoresMapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopsMapper;
import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;
import com.stylefeng.guns.modular.huamao.model.SktShopScores;
import com.stylefeng.guns.modular.huamao.service.ISktShopApplysService;
import sun.rmi.server.InactiveGroupException;

/**
 * <p>
 * 商城商家开店申请表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-10
 */
@Service
public class SktShopApplysServiceImpl extends ServiceImpl<SktShopApplysMapper, SktShopApplys> implements ISktShopApplysService {
	@Autowired
	private SktShopApplysMapper SktShopApplys;
	@Autowired
	private SktShopsMapper sktShopsMapper;
	@Autowired
	private SktShopScoresMapper sktShopScoresMapper;
	@Autowired
	private SktShopApproveMapper sktShopApproveMapper;
	@Override
	public List<MyDTO> shopApplysfindall(MyDTO myDto) {
		List<MyDTO> list = SktShopApplys.shopApplysfindinfo(myDto);
		return list;
	}
	public List<MyDTO> shopApplysfindup(MyDTO myDto) {
		List<MyDTO> list = SktShopApplys.shopApplysfindup(myDto);
		return list;
	}
	@Override
	public void shopApplyupdate(MyDTO myDto) {
		SktShopApplys.shopApplyupdate(myDto);
	}
	@Override
	public int selectCount() {
		// TODO Auto-generated method stub
		return SktShopApplys.selectCount();
	}
	
	@Override
	@Transactional
	public boolean insertShopApplys(SktShopApplys sktShopApplys) {
		//Integer shopId = sktShopsMapper.selectShopIdByUserId(sktShopApplys.getUserId());
		//sktShopApplys.setShopId(shopId);
		try {
			sktShopApplys.setCreateTime(new Date());
			sktShopApplys.setApplyStatus(1);
			sktShopApplys.setApplyDesc(" ");
			Integer insert = SktShopApplys.insert(sktShopApplys);
			
			//新增商铺评分表	
			SktShopScores sktShopScores = new SktShopScores();
			sktShopScores.setShopId(sktShopApplys.getShopId());
			sktShopScores.setGoodsUsers(0);
			sktShopScores.setGoodsScore(0);
			sktShopScores.setServiceScore(0);
			sktShopScores.setServiceUsers(0);
			sktShopScores.setTimeScore(0);
			sktShopScores.setTimeUsers(0);
			sktShopScores.setTotalScore(0);
			sktShopScores.setTimeUsers(0);
			Integer insert2 = sktShopScoresMapper.insert(sktShopScores);
			
			if(insert==1 && insert2 == 1){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	@Transactional
	public String insertShopApplys2(SktShopApprove sktShopApprove) {
		//查询企业表 有没有联盟商家
		Map<String,Object> map = new HashMap<>();
		Integer insert = 0;
		Integer insertApplys = 0;
		//查询申请表有没有
		Map<String,Object> map1 = selectApply(sktShopApprove);
		if(map1.size() > 0 && "3".equals(map1.get("code").toString())){
			return "3";//已经申请线上
		}else if(map1.size() > 0 && "1".equals(map1.get("code").toString())){
			return "2";//已经申请线上 未审核
		}


		map = selectShopAppr(sktShopApprove.getUserId());
		if (map.size() > 0 && ("1".equals(map.get("code").toString())
				|| "3".equals(map.get("code").toString()))){//联盟商家申请成功

			//如果有拒绝 把拒绝的更新
			List<SktShopApplys> list = new ArrayList<>();
			if(map1.size()>0){
				list = (List<SktShopApplys>) map1.get("result");
			}
			Integer i = 0,j = 0,k = 0;
			if(list.size()>0) {
				SktShopApplys sktShopApplys = list.get(0);
				sktShopApplys.setApplyStatus(1);
				sktShopApplys.setCreateTime(new Date());
				sktShopApplys.setCheckStaffId(null);
				sktShopApplys.setCreateTime(null);
				i = SktShopApplys.updateById(sktShopApplys);

			}else {//新增线上申请表 ，评分表
				//根据用户查询店铺id
				Integer shopId = sktShopsMapper.selectShopIdByUserId(sktShopApprove.getUserId());
				i = insertApplyShopScore(sktShopApprove,shopId);
			}
			//删除联盟商家2状态
			Map<String,Object> map2 = new HashMap<>();
			map2.put("userId",sktShopApprove.getUserId());
			map2.put("shopStatus",2);
			List<SktShopApprove> list2 = sktShopApproveMapper.selectShopApproveByUserId3(map2);
			if(list2.size()>0) {
				sktShopApproveMapper.deleteById(list2.get(0).getApplyId());
			}
			//修改联盟商家状态
			Map<String,Object> map3 = new HashMap<>();
			map3.put("userId",sktShopApprove.getUserId());
			map3.put("shopStatus",3);
			List<SktShopApprove> list3 = sktShopApproveMapper.selectShopApproveByUserId3(map3);
			if(list3.size()>0) {
				SktShopApprove sktShopApplys3 = list3.get(0);
				sktShopApplys3.setShopStatus(1);
				sktShopApproveMapper.updateById(sktShopApplys3);
			}
			if(i.intValue() == 1){
				return "1";
			}
		}else if(map.size() > 0 && "0".equals(map.get("code").toString())){//联盟商家正在申请
		    return "5";
        }else if(map.size() > 0 && "-1".equals(map.get("code").toString())){//联盟商家正在拒绝

			//如果有拒绝 把拒绝的更新
			List<SktShopApplys> list = new ArrayList<>();
			if(map1.size()>0){
				list = (List<SktShopApplys>) map1.get("result");
			}
			if(list.size()>0) {
				SktShopApplys sktShopApplys = list.get(0);
				sktShopApplys.setApplyStatus(1);
				sktShopApplys.setCreateTime(new Date());
				sktShopApplys.setCheckStaffId(null);
				sktShopApplys.setCheckTime(null);
				insertApplys = SktShopApplys.updateById(sktShopApplys);

			}else {//新增线上申请表 ，评分表
				insertApplys = insertApplyShopScore(sktShopApprove,null);
			}

			//如果有更新 企业表 没有新增企业表
			map.put("userId",sktShopApprove.getUserId());
			map.put("shopStatus",2);
			List<SktShopApprove> list1 = sktShopApproveMapper.selectShopApproveByUserId3(map);
			if(list1.size()>0){
				sktShopApprove.setApplyId(list1.get(0).getApplyId());
				sktShopApprove.setCreateTime(new Date());
				sktShopApprove.setShopStatus(2);
				sktShopApprove.setCheckStaffId(null);
				sktShopApprove.setCheckTime(null);
				insert = sktShopApproveMapper.updateById(sktShopApprove);
			}else{
				sktShopApprove.setCreateTime(new Date());
				sktShopApprove.setShopStatus(2);
				insert = sktShopApproveMapper.insert(sktShopApprove);
			}
            if(insert.intValue() == 1 && insertApplys.intValue() == 1){
                return "1";
            }else{
                return "0";
            }
		}else{
			//如果企业表没有数据 新增企业表 新增线上申请表 评分表
			insertApplys = insertApplyShopScore(sktShopApprove,null);
			//如果有更新 企业表 没有新增企业表
			map.put("userId",sktShopApprove.getUserId());
			map.put("shopStatus",2);
			List<SktShopApprove> list1 = sktShopApproveMapper.selectShopApproveByUserId3(map);
			if(list1.size()>0){
				sktShopApprove.setApplyId(list1.get(0).getApplyId());
				sktShopApprove.setCreateTime(new Date());
				sktShopApprove.setShopStatus(2);
				sktShopApprove.setCheckStaffId(null);
				sktShopApprove.setCheckTime(null);
				insert = sktShopApproveMapper.updateById(sktShopApprove);
			}else{
				sktShopApprove.setCreateTime(new Date());
				sktShopApprove.setShopStatus(2);
				insert = sktShopApproveMapper.insert(sktShopApprove);
			}


		}

		if(insert.intValue() == 1 && insertApplys.intValue() == 1){
			return "1";//成功
		}


		return "0";
	}

	/**
	 * 查询联盟商家
	 * @param userId
	 * @return
	 */
	public Map<String,Object> selectShopAppr(Integer userId){
		Map<String,Object> map = new HashMap<>();
		Map<String,Object> mapRet = new HashMap<>();
		map.put("userId",userId);
		map.put("shopStatus",1);
		List<SktShopApprove> list1 = sktShopApproveMapper.selectShopApproveByUserId3(map);
		if(list1.size() > 0){
			mapRet.put("code","1");
			mapRet.put("result", list1);
			return mapRet;
		}
		map.put("shopStatus",0);
		List<SktShopApprove> list2 = sktShopApproveMapper.selectShopApproveByUserId3(map);
		if(list2.size() > 0){
			mapRet.put("code","0");
			mapRet.put("result", list2);
			return mapRet;
		}
		map.put("shopStatus",-1);
		List<SktShopApprove> list3 = sktShopApproveMapper.selectShopApproveByUserId3(map);
		if(list3.size() > 0){
			mapRet.put("code","-1");
			mapRet.put("result", list3);
		}
		map.put("shopStatus",3);
		List<SktShopApprove> list4 = sktShopApproveMapper.selectShopApproveByUserId3(map);
		if(list4.size() > 0){
			mapRet.put("code","3");
			mapRet.put("result", list4);
		}
		return mapRet;
	}

	/**
	 * 查询线上商城
	 * @param sktShopApprove
	 * @return
	 */
	public Map<String,Object> selectApply(SktShopApprove sktShopApprove){
		Map<String,Object> mapRet = new HashMap<>();
		Map<String,Object> map = new HashMap<>();
		map.put("userId",sktShopApprove.getUserId());
		map.put("applyStatus",3);//通过
		List<SktShopApplys> list = SktShopApplys.selectShopApplysByUserId3(map);
		if(list.size() > 0){
			mapRet.put("code","3");
			mapRet.put("result",list);
			return mapRet;
		}
		map.put("applyStatus",2);//拒绝
		List<SktShopApplys> list2 = SktShopApplys.selectShopApplysByUserId3(map);
		if(list2.size() > 0){
			mapRet.put("code","2");
			mapRet.put("result",list2);
			return mapRet;
		}

		map.put("applyStatus",1);//未审核
		List<SktShopApplys> list3 = SktShopApplys.selectShopApplysByUserId3(map);
		if(list3.size() > 0){
			mapRet.put("code","1");
			mapRet.put("result",list3);
		}
		return mapRet;

	}

	@Transactional
	public Integer insertApplyShopScore(SktShopApprove sktShopApprove,Integer shopId){
		Integer insertApplys = 0,insertSCores = 0;
		SktShopApplys sktShopApplys = new SktShopApplys();
		sktShopApplys.setUserId(sktShopApprove.getUserId());
		sktShopApplys.setCreateTime(new Date());
		sktShopApplys.setApplyStatus(1);
		sktShopApplys.setApplyDesc(" ");
		insertApplys = SktShopApplys.insert(sktShopApplys);


		//新增商铺评分表
        if(shopId != null){
                //看有没有该商铺评分
            SktShopScores sktShopScores2 =  new SktShopScores();
            sktShopScores2.setShopId(shopId);
            sktShopScores2 = sktShopScoresMapper.selectOne(sktShopScores2);

            if(sktShopScores2.getScoreId() == null){
                SktShopScores sktShopScores = new SktShopScores();
                sktShopScores.setShopId(shopId);
                sktShopScores.setGoodsUsers(0);
                sktShopScores.setTotalUsers(0);
                sktShopScores.setGoodsScore(0);
                sktShopScores.setServiceScore(0);
                sktShopScores.setServiceUsers(0);
                sktShopScores.setTimeScore(0);
                sktShopScores.setTimeUsers(0);
                sktShopScores.setTotalScore(0);
                sktShopScores.setTimeUsers(0);
                insertSCores = sktShopScoresMapper.insert(sktShopScores);
            }else{
                SktShopScores sktShopScores = new SktShopScores();
                sktShopScores.setShopId(shopId);
                sktShopScores.setScoreId(sktShopScores2.getScoreId());
                sktShopScores.setGoodsUsers(0);
                sktShopScores.setTotalUsers(0);
                sktShopScores.setGoodsScore(0);
                sktShopScores.setServiceScore(0);
                sktShopScores.setServiceUsers(0);
                sktShopScores.setTimeScore(0);
                sktShopScores.setTimeUsers(0);
                sktShopScores.setTotalScore(0);
                sktShopScores.setTimeUsers(0);
                insertSCores = sktShopScoresMapper.updateById(sktShopScores);
            }
        }


		if(insertApplys.intValue() == 1 && insertSCores.intValue() == 1){
			return 1;
		}
		if(insertApplys.intValue() == 1 ){
			return 1;
		}

		return 0;
	}
}
