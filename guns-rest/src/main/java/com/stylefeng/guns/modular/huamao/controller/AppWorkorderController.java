package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppSktSysConfigService;
import com.stylefeng.guns.modular.huamao.service.IAppSktWorkOrderService;
import com.stylefeng.guns.modular.huamao.service.IAppWorkorderServer;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;

@CrossOrigin
@SuppressWarnings("all")
@Controller
@RequestMapping("/app")
public class AppWorkorderController {

	@Autowired
	private CacheUtil cacheUtil;
	@Autowired
	private IAppSktSysConfigService iAppSktSysConfigService;
	@Autowired
	private IAppSktWorkOrderService iAppSktWorkOrderService;
	@Autowired
	private IAppWorkorderServer iAppWorkorderServer;

	/**
	 * 工单记录 返回值待核实？
	 * 
	 * @param pageIndex
	 * @return
	 */
	@RequestMapping("workorder/workorderRecord")
	@ResponseBody
	public AppResult workorderRecord(@ModelAttribute(value = "pageIndex") Integer pageIndex) {
		Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
		if (user == null) {
			return AppResult.ERROR("您未登录", -2);
		}
		PageHelper.startPage(pageIndex, 10);
		List<Map<String, Object>> workOrder = iAppSktWorkOrderService.selectRecord();
		JSONObject json = new JSONObject();
		if (workOrder != null && workOrder.size() > 0) {
			PageInfo pageInfo = new PageInfo(workOrder);
			json.put("tatolPage", pageInfo.getPages());
		} else {
			json.put("tatolPage", 1);
		}
		json.put("maps", workOrder);
		Long time = new Date().getTime();
		json.put("time", time.toString());
		return AppResult.OK(json);
	}

	/**
	 * 工单支付
	 * 
	 * @param orderNo
	 * @param payType
	 * @param payPwd
	 * @return
	 */
	@RequestMapping("workorder/buyWorkorder")
	@ResponseBody
	public AppResult workorderRecord(@ModelAttribute(value = "orderNo") String orderNo,
			@ModelAttribute(value = "payType") Integer payType, @ModelAttribute(value = "payPwd") String payPwd) {
		Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
		if (user == null) {
			return AppResult.ERROR("您未登录", -2);
		}
		if ("".equals(payType)) {
			return AppResult.ERROR("支付密码不能为空");
		}
		Map<String, Object> result = iAppWorkorderServer.buyWorkorder(orderNo, payType, payPwd, user);
		if (!MSGUtil.isBlank(result) && result.containsKey("msg")) {
			return AppResult.ERROR(result.get("msg").toString());
		}
		return AppResult.OK(result);
	}

	/**
	 * 获取工单状态
	 * 
	 * @return
	 */
	@RequestMapping("workorder/getWorkorder")
	@ResponseBody
	public AppResult getWorkorder() {
		Users user = (Users) cacheUtil.get(SessionUtils.getSessionId());
		if (user == null) {
			return AppResult.ERROR("您未登录", -2);
		}
		Map<String, Object> map = iAppWorkorderServer.getWorkorder(user);
		return AppResult.OK(map);
	}

	/**
     * 修改分享人，获取页面数据
     * @return
     */
    @RequestMapping("workorder/getShare")
    @ResponseBody
    public AppResult getShare(){
    	Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user==null){
            return AppResult.ERROR("您未登录",-2);
        }
        Map<String,Object> map = iAppWorkorderServer.getShare(user);
        if(!MSGUtil.isBlank(map) && map.containsKey("msg")){
    		return AppResult.ERROR(map.get("msg").toString());
    	}
    	return AppResult.OK(map);
    }
    
    /**
     * 修改分享人
     * @param preValue
     * @param afterValue
     * @param orderRemarks
     * @param imgs
     * @param money
     * @return
     */
	@RequestMapping("workorder/modifyShare")
    @ResponseBody
    public AppResult modifyShare(@ModelAttribute("preValue")String preValue,
    		@ModelAttribute("afterValue")String afterValue,
    		@ModelAttribute("orderRemarks")String orderRemarks,
    		@ModelAttribute("imgs")String imgs,
    		@ModelAttribute("money")Integer money){
    	Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user==null){
            return AppResult.ERROR("您未登录",-2);
        }
        Map<String,Object> map = iAppWorkorderServer.modifyShare(preValue,afterValue,orderRemarks,imgs,money,user);
        if(!MSGUtil.isBlank(map) && map.containsKey("msg")){
    		return AppResult.ERROR(map.get("msg").toString());
    	}
    	return AppResult.OK(map);
    }
	
	/**
     * 修改分享人，获取页面数据
     * @return
     */
    @RequestMapping("workorder/getPhone")
    @ResponseBody
    public AppResult getPhone(){
    	Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user==null){
            return AppResult.ERROR("您未登录",-2);
        }
        Map<String,Object> map = iAppWorkorderServer.getPhone(user);
        if(!MSGUtil.isBlank(map) && map.containsKey("msg")){
    		return AppResult.ERROR(map.get("msg").toString());
    	}
    	return AppResult.OK(map);
    }
    
    /**
     * 修改分享人
     * @param preValue
     * @param afterValue
     * @param orderRemarks
     * @param imgs
     * @param money
     * @return
     */
	@RequestMapping("workorder/modifyPhone")
    @ResponseBody
    public AppResult modifyPhone(@ModelAttribute(value="preValue")String preValue,
    		@ModelAttribute(value="afterValue")String afterValue,
    		@ModelAttribute(value="code")String code,
    		@ModelAttribute(value="function")String function,
    		@ModelAttribute(value="orderRemarks")String orderRemarks,
    		@ModelAttribute(value="imgs")String imgs,
    		@ModelAttribute(value="money")Integer money
    		){
    	Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user==null){
            return AppResult.ERROR("您未登录",-2);
        }
        Map<String,Object> map = iAppWorkorderServer.modifyPhone(preValue,afterValue,code,function,orderRemarks,imgs,money,user);
        if(!MSGUtil.isBlank(map) && map.containsKey("msg")){
    		return AppResult.ERROR(map.get("msg").toString());
    	}
    	return AppResult.OK(map);
    }
	
}
