package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecial;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecialList;

/**
 * <p>
 * 待发特别积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface LogScoreSpecialMapper extends BaseMapper<LogScoreSpecial> {

	// 待发特别积分流水表展示
	List<LogScoreSpecialList> selectLogScoreSpecialAll(LogScoreSpecialList logScoreSpecialList);

	/**
	 * 首页显示
	 * @param logScoreSpecialList
	 * @return
	 */
    List<Map<String,Object>> selectUserScoreSpecial(LogScoreSpecialList logScoreSpecialList);

    List<LogScoreSpecial> selectLogScoreSpecialByUserId(Map<String, Object> map);
}
