package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccount;
import com.stylefeng.guns.modular.huamao.model.SktAgentsAccountDTO;

import java.util.List;

/**
 * <p>
 * 代理公司账户表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-18
 */
public interface ISktAgentsAccountService extends IService<SktAgentsAccount> {
	/**
	 * 页面显示
	 * @param sktAgentsAccountDTO
	 * @return
	 */
	public List<SktAgentsAccountDTO> showAgentsAccountInfo(SktAgentsAccountDTO sktAgentsAccountDTO);
}
