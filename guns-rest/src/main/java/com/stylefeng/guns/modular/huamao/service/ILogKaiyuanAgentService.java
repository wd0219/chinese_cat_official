package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgent;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgentList;

import java.util.List;

/**
 * <p>
 * 代理公司开元宝流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface ILogKaiyuanAgentService extends IService<LogKaiyuanAgent> {

	// 代理公司划拨啊流水表展示
	public List<LogKaiyuanAgentList> selectLogKaiyuanAgentAll(LogKaiyuanAgentList logKaiyuanAgentList);

}
