package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktFriendlinksMapper;
import com.stylefeng.guns.modular.huamao.model.SktFriendlinks;
import com.stylefeng.guns.modular.huamao.service.ISktFriendlinksService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 友情连接表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@Service
public class SktFriendlinksServiceImpl extends ServiceImpl<SktFriendlinksMapper, SktFriendlinks> implements ISktFriendlinksService {

}
