package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreMapper;
import com.stylefeng.guns.modular.huamao.model.AppTradeDictDTD;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.service.IAppLogScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppLogScoreServiceImpl extends ServiceImpl<LogScoreMapper,LogScore> implements IAppLogScoreService {

    @Autowired
    private LogScoreMapper logScoreMapper;

    @Override
    public LogScore selectSumLogScore(Map<String, Object> map) {
        return logScoreMapper.selectSumLogScore(map);
    }

    @Override
    public LogScore selectEverySumLogScore(Map<String, Object> map) {
        return logScoreMapper.selectEverySumLogScore(map);
    }

    @Override
    public Double selectSumScoreByUserId(Map<String, Object> map) {
        return logScoreMapper.selectSumScoreByUserId(map);
    }

    @Override
    public List<LogScore> selectLogScoreByUserId(Map<String, Object> map) {
        return logScoreMapper.selectLogScoreByUserId(map);
    }

    @Override
    public AppTradeDictDTD selectTradeDictBygType(String gType,Integer type) {
        return logScoreMapper.selectTradeDictBygType(gType,type);
    }
}
