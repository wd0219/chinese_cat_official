package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAreasMapper;
import com.stylefeng.guns.modular.huamao.model.SktAreas;
import com.stylefeng.guns.modular.huamao.service.IAppUsersAreasService;
import org.springframework.stereotype.Service;

@Service
public class AppUsersAreasServiceImpl extends ServiceImpl<SktAreasMapper,SktAreas> implements IAppUsersAreasService {
}
