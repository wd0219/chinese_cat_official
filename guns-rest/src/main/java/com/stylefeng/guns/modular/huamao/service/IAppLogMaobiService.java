package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogMaobi;

public interface IAppLogMaobiService extends IService<LogMaobi> {
}
