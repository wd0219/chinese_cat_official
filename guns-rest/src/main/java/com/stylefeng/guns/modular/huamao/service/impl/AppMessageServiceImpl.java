package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.MessagesMapper;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;
import com.stylefeng.guns.modular.huamao.service.IAppMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppMessageServiceImpl extends ServiceImpl<MessagesMapper,Messages> implements IAppMessageService {
    @Autowired
    private MessagesMapper messagesMapper;
    @Override
    public List<Messages> message(Integer userId) {
        return messagesMapper.message(userId);
    }

    @Override
    public List<MessagesList> getMessage(Messages messages) {
        return messagesMapper.getMessage(messages);
    }
}
