package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.Shopping;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物券表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-06-19
 */
public interface IShoppingService extends IService<Shopping> {
    public Shopping getShoppingCount(Map<String,Object> map);
    public List<Map<String, Object>> selectShoppingList(Map<String,Object> map);
}
