package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersAndAccount;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author wangang
 * @since 2018-05-6
 */
public interface IAppUserService extends IService<Users> {

	//根据登录名查询会员集合
	public Users findListByLoginName(@Param("loginName") String LoginName);
	//查询分享人
	Users findUserByLoginNameOrUserPhone(String shareMan);
	//添加用户
	boolean addUserRegister(Users uses);

	Users findUsersByUserPhoneAndOldpayPwd(Users users);

    Users findUserByPhone(String userPhone);

    int updateUsersByUserPhone(Users users);

    UsersAndAccount selectUserAccountDataById(Users user);

    AppResult qrcode(Users user,String url);

    List<Users> selectUsersInviteMan(Users user);

	int selectInvitesCount(Users user);

	int selectChargeCount(Users user);

	int selectManagerCount(Users user);

    Users selectUsersByUserId(Integer userId);

    Users selectUsersByUserIdAndUserStatus(Map<Object, Object> maps);

    Map<String,Object> selectUsersAndUserRealNameByUserId(Integer userId);

	Map<String,Object> selectUsersAccountByUserId(Integer userId);
}
