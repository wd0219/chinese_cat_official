package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsAppraisesMapper;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.service.IAppGoodsAppraisesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppGoodsAppraisesServiceImpl extends ServiceImpl<GoodsAppraisesMapper,GoodsAppraises> implements IAppGoodsAppraisesService {
    @Autowired
    private GoodsAppraisesMapper goodsAppraisesMapper;

    @Override
    public GoodsAppraises selectGoodsAppraisesBuUserId(Map<String, Object> map2) {
        return goodsAppraisesMapper.selectGoodsAppraisesBuUserId(map2);
    }
}
