package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 转化猫币订单表
 * </p>
 *
 * @author liuduan
 * @since 2018-06-24
 */
@TableName("skt_users_draws_maobi")
public class UsersDrawsMaobi extends Model<UsersDrawsMaobi> {

    /**
     * 自增ID
     */
    @TableId(value = "drawId", type = IdType.AUTO)
    private Integer drawId;
    /**
     * 提现单号
     */
    private String drawNo;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 提现的类型：1开元宝提现 2现金提现 3开元宝营业额提现
     */
    private Integer type;
    /**
     * 到账金额
     */
    private BigDecimal money;
    /**
     * 手续费+税率
     */
    private BigDecimal fee;
    /**
     * 提现手续费比例 %
     */
    private BigDecimal ratio;
    /**
     * 提现税率比例 %
     */
    private BigDecimal taxratio;
    /**
     * 提现状态 -2受理失败 -1审核拒绝 0审核中 2受理中 3受理成功 4线下打款
     */
    private Integer cashSatus;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 拒绝理由
     */
    private String checkRemark;
    /**
     * 审核员工ID
     */
    private Integer checkStaffId;
    /**
     * 预计到账猫币
     */
    private BigDecimal maobi;
    /**
     * 0：非手动处理；2：手动处理成功；3：手动处理失败
     */

    private Integer specialType;

    /**
     *汇率
     */
    private BigDecimal moneyRate;

    /**
     * 赎回货币类型（1猫币，2hhtb）
     * @return
     */
    private Integer drawsType;

    public Integer getDrawsType() {
        return drawsType;
    }

    public void setDrawsType(Integer drawsType) {
        this.drawsType = drawsType;
    }

    public Integer getDrawId() {
        return drawId;
    }

    public void setDrawId(Integer drawId) {
        this.drawId = drawId;
    }

    public String getDrawNo() {
        return drawNo;
    }

    public void setDrawNo(String drawNo) {
        this.drawNo = drawNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public BigDecimal getTaxratio() {
        return taxratio;
    }

    public void setTaxratio(BigDecimal taxratio) {
        this.taxratio = taxratio;
    }

    public Integer getCashSatus() {
        return cashSatus;
    }

    public void setCashSatus(Integer cashSatus) {
        this.cashSatus = cashSatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }

    public Integer getCheckStaffId() {
        return checkStaffId;
    }

    public void setCheckStaffId(Integer checkStaffId) {
        this.checkStaffId = checkStaffId;
    }

    public BigDecimal getMaobi() {
        return maobi;
    }

    public void setMaobi(BigDecimal maobi) {
        this.maobi = maobi;
    }

    public Integer getSpecialType() {
        return specialType;
    }

    public void setSpecialType(Integer specialType) {
        this.specialType = specialType;
    }

    public BigDecimal getMoneyRate() {
        return moneyRate;
    }

    public void setMoneyRate(BigDecimal moneyRate) {
        this.moneyRate = moneyRate;
    }

    @Override
    protected Serializable pkVal() {
        return drawId;
    }

    @Override
    public String toString() {
        return "UsersDrawsMaobi{" +
                "drawId=" + drawId +
                ", drawNo='" + drawNo + '\'' +
                ", userId=" + userId +
                ", type=" + type +
                ", money=" + money +
                ", fee=" + fee +
                ", ratio=" + ratio +
                ", taxratio=" + taxratio +
                ", cashSatus=" + cashSatus +
                ", createTime=" + createTime +
                ", checkRemark='" + checkRemark + '\'' +
                ", checkStaffId=" + checkStaffId +
                ", maobi=" + maobi +
                ", specialType=" + specialType +
                ", moneyRate=" + moneyRate +
                '}';
    }
}
