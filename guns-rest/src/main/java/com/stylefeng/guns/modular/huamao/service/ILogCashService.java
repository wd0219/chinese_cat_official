package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.LogCashList;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 现金流水表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface ILogCashService extends IService<LogCash> {

	// 现金流水表返回
	public List<LogCashList> selectLogCashAll(LogCashList logCashList);

	/**
	 * 首页返回信息
	 * @param logCash
	 * @return
	 */
    public List<Map<String,Object>> selectLogCash(LogCashList logCash);
}
