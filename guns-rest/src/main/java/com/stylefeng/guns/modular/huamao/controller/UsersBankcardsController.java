package com.stylefeng.guns.modular.huamao.controller;

import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.model.UsersBankcardsList;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IUsersBankcardsService;

/**
 * 用户银行卡控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 16:01:08
 */	
@CrossOrigin()
@Controller
@RequestMapping("/usersBankcards")
public class UsersBankcardsController extends BaseController {

	private String PREFIX = "/huamao/usersBankcards/";

	@Autowired
	private IUsersBankcardsService usersBankcardsService;

	/**
	 * 跳转到用户银行卡首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "usersBankcards.html";
	}

	/**
	 * 跳转到添加用户银行卡
	 */
	@RequestMapping("/usersBankcards_add")
	public String usersBankcardsAdd() {
		return PREFIX + "usersBankcards_add.html";
	}

	/**
	 * 跳转到修改用户银行卡
	 */
	@RequestMapping("/usersBankcards_update/{usersBankcardsId}")
	public String usersBankcardsUpdate(@PathVariable Integer usersBankcardsId, Model model) {
		UsersBankcards usersBankcards = usersBankcardsService.selectById(usersBankcardsId);
		model.addAttribute("item", usersBankcards);
		return PREFIX + "usersBankcards_edit.html";
	}

	/**
	 * 获取用户银行卡列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(UsersBankcardsList usersBankcardsList) {
		return usersBankcardsService.selectUsersBankcardsAll(usersBankcardsList);
		// return usersBankcardsService.selectList(null);
	}

	/**
	 * 删除用户银行卡
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer usersBankcardsId) {
		usersBankcardsService.deleteById(usersBankcardsId);
		return SUCCESS_TIP;
	}


	/**
	 * 修改用户银行卡
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(UsersBankcards usersBankcards) {
		usersBankcardsService.updateById(usersBankcards);
		return SUCCESS_TIP;
	}

	/**
	 * 用户银行卡详情
	 */
	@RequestMapping(value = "/detail/{usersBankcardsId}")
	@ResponseBody
	public Object detail(@PathVariable("usersBankcardsId") Integer usersBankcardsId) {
		return usersBankcardsService.selectById(usersBankcardsId);
	}
	
	/*********************首页****************************/
	/**
	 * 根据用户id获取用户银行卡列表
	 */
	@RequestMapping(value = "/selectUserBranklist")
	@ResponseBody
	public Result selectUserBranklist(UsersBankcardsList usersBankcardsList) {
		List<Map<String,Object>> selectUserBranks = usersBankcardsService.selectUserBranks(usersBankcardsList);
		return Result.OK(selectUserBranks);
	}	
	/**
	 * 新增用户银行卡
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Result add(UsersBankcardsList usersBankcards) {
		String insert = usersBankcardsService.addBankcards(usersBankcards);
		if("5".equals(insert)){
			return Result.EEROR("银行卡不能重复绑定");
		}
		if("6".equals(insert)){
			return Result.EEROR("银行卡不能为空");
		}
		if("4".equals(insert)){
			return Result.OK("添加银行卡成功！");
		}
		if("3".equals(insert)){
			return Result.EEROR("最多添加5个银行卡！");
		}
		if("2".equals(insert)){
			return Result.EEROR("银行卡信息有误！请填写正确信息！");
		}
		return Result.EEROR("添加银行卡失败！");
	}
	
	
	/**
	 * 设为默认银行卡
	 */
	@RequestMapping(value = "/updateUserBrankStatus")
	@ResponseBody
	public Result updateUserBrankStatus(UsersBankcards usersBankcards) {
		Integer i = usersBankcardsService.updateUserBrankStatus(usersBankcards);
		if(ToolUtil.isEmpty(i)){
			return Result.EEROR("修改默认银行卡失败！");
		}
		return Result.OK("修改默认银行卡成功!");
	}
	
	/**
	 * 根据用户id银行卡快速认证
	 */
	@RequestMapping(value = "/quickApproveBrank")
	@ResponseBody
	public Result quickApprove(UsersBankcardsList usersBankcardsList) {
		String quickApproveBank = usersBankcardsService.quickApproveBank(usersBankcardsList);
		if("1".equals(quickApproveBank)){
			return Result.OK(null);
		}else if("FAILS".equals(quickApproveBank)){
			return Result.EEROR("快速认证失败");
		}else if("2".equals(quickApproveBank)) {
			return Result.EEROR("用户已经实名认证");
		}
		return Result.EEROR(quickApproveBank);
	}
	/**
	 * 通过ID删除用户银行卡
	 */
	@RequestMapping(value = "/deleteById")
	@ResponseBody
	public Result deleteById(@RequestParam Integer usersBankcardsId) {
		// 通过传过来的ID进行逻辑删除
		//usersBankcardsService.deleteById2(usersBankcardsId);
		 boolean deleteById = usersBankcardsService.deleteById(usersBankcardsId);
		return deleteById == true?Result.OK("删除银行卡成功"):Result.EEROR("删除银行卡失败");
	}
	
}
