package com.stylefeng.guns.modular.huamao.model;

/**
 * 
 * @author slt
 *
 */
public class UsersLoginDTO {
	private Integer userId;
	private String loginName;
	private String lastLoginTime;
	private String loginAdress;
	private Integer cash;
	private Integer kaiyuan;
	private Integer freezeCash;
	private Integer score;
	private Integer freezeScore;
	private String freezeSpecialAwardScore;
	private Integer isStore;
	private Integer isShop;
	private Integer isAgent;
	private Integer auditStatus;//实名认证状态
	private Integer stauts;//升级状态
	private Integer uporderStatus;//线上商城状态
	private Integer dwapplyStatus;//线下商城状态
	private String loginRegion;
	/**
	 * 用户类型 0普通 1主管 2经理
	 */
	private Integer userType;

	private  String userStatus;//锁定状态

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getLoginAdress() {
		return loginAdress;
	}
	public void setLoginAdress(String loginAdress) {
		this.loginAdress = loginAdress;
	}
	public Integer getCash() {
		return cash;
	}
	public void setCash(Integer cash) {
		this.cash = cash;
	}
	public Integer getKaiyuan() {
		return kaiyuan;
	}
	public void setKaiyuan(Integer kaiyuan) {
		this.kaiyuan = kaiyuan;
	}
	public Integer getFreezeCash() {
		return freezeCash;
	}
	public void setFreezeCash(Integer freezeCash) {
		this.freezeCash = freezeCash;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getFreezeScore() {
		return freezeScore;
	}
	public void setFreezeScore(Integer freezeScore) {
		this.freezeScore = freezeScore;
	}
	public String getFreezeSpecialAwardScore() {
		return freezeSpecialAwardScore;
	}
	public void setFreezeSpecialAwardScore(String freezeSpecialAwardScore) {
		this.freezeSpecialAwardScore = freezeSpecialAwardScore;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public Integer getIsStore() {
		return isStore;
	}
	public void setIsStore(Integer isStore) {
		this.isStore = isStore;
	}
	public Integer getIsShop() {
		return isShop;
	}
	public void setIsShop(Integer isShop) {
		this.isShop = isShop;
	}
	public Integer getIsAgent() {
		return isAgent;
	}
	public void setIsAgent(Integer isAgent) {
		this.isAgent = isAgent;
	}

	public Integer getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}

	public Integer getStauts() {
		return stauts;
	}

	public void setStauts(Integer stauts) {
		this.stauts = stauts;
	}

	public Integer getUporderStatus() {
		return uporderStatus;
	}

	public void setUporderStatus(Integer uporderStatus) {
		this.uporderStatus = uporderStatus;
	}

	public Integer getDwapplyStatus() {
		return dwapplyStatus;
	}

	public void setDwapplyStatus(Integer dwapplyStatus) {
		this.dwapplyStatus = dwapplyStatus;
	}
	public String getLoginRegion() {
		return loginRegion;
	}
	public void setLoginRegion(String loginRegion) {
		this.loginRegion = loginRegion;
	}
}
