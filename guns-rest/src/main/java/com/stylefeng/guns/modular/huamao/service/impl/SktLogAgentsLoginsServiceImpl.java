package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktLogAgentsLoginsMapper;
import com.stylefeng.guns.modular.huamao.model.SktLogAgentsLogins;
import com.stylefeng.guns.modular.huamao.service.ISktLogAgentsLoginsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 代理公司登陆记录表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
@Service
public class SktLogAgentsLoginsServiceImpl extends ServiceImpl<SktLogAgentsLoginsMapper, SktLogAgentsLogins> implements ISktLogAgentsLoginsService {
	@Autowired
	private SktLogAgentsLoginsMapper sktLogAgentsLoginsMapper;
	@Override
	public List<SktLogAgentsLogins> sktLogAgentsLoginfindup(SktLogAgentsLogins sktLogAgentsLogins){
		return sktLogAgentsLoginsMapper.sktLogAgentsLoginfindup(sktLogAgentsLogins);
	}
	
	
}
