package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;

import java.util.Map;

public interface IAppSktBuyplaqueSerivce extends IService<SktBuyplaque> {
    SktBuyplaque selectByOrderNo(Map<String, Object> orderWhere);
}
