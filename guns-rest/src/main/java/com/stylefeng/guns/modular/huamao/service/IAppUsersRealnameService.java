package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;

public interface IAppUsersRealnameService extends IService<UsersRealname> {
    UsersRealname selectUsersRealNameByUserId(Integer userId);

    UsersRealname selectUsersRealNameByCardId(String id);

}
