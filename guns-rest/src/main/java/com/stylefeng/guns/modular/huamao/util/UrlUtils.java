package com.stylefeng.guns.modular.huamao.util;

import org.springframework.stereotype.Component;

/**
 * 判断图片是否有前缀，没有就加一个返回
 */
@Component
public class UrlUtils {

    final static String  imgUrl = "http://zzhtwl.oss-cn-qingdao.aliyuncs.com/";

    public static String getAllUrl(String pic) {
        if (pic != null && !"".equals(pic)) {
            if (pic.indexOf("http") == -1) {
                pic = imgUrl + pic;
            }
        }
        return pic;
    }
}
