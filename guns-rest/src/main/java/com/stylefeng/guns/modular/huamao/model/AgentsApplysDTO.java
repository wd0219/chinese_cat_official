package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class AgentsApplysDTO{
	
	private Integer userId;
	private String legalPerson;
	private Integer shaId;
	/**
	 * 企业名称
	 */
	private String agentName;
	
	/**
	 * 代理类行
	 */
	private Integer alevel;
	/**
	 * 区域
	 */
	private String areas;
	/**
	 * 代理公司ID
	 */
	private Integer agentId;
	/**
	 * 申请人手机号码
	 */
	private String phone;
	/**
	 * 股东类型 1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
	 */
	private Integer type;
	/**
	 * 拥有的股票分数
	 */
	private Integer stockNum;
	/**
	 * 缴纳代理费用
	 */
	private BigDecimal money;
	/**
	 * 打款备注
	 */
	private String remark;
	/**
	 * -2二审拒绝 -1一审拒绝 0审核中 1一审同意 2汇款单已上传 3二审完结
	 */
	private Integer status;
	/**
	 * 汇款单的图片
	 */
	private String moneyOrderImg;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 拒绝理由
	 */
	private String checkRemark;
	/**
	 * 处理状态 0无需处理 1未处理 2已处理 9处理中
	 */
	private Integer batFlag;
	/**
	 * 用户昵称
	 */
	private String loginName;
	/**
	 * 股东类型
	 */
	private Integer isAgent;
	/**
	 * 省id
	 */
	private Integer province;
	/**
	 * 市id
	 */
	private Integer citys;
	/**
	 * 区域id
	 */
	private Integer county;
	/**
	 * 用户类型
	 */
	private Integer userType;
	
    public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getCitys() {
		return citys;
	}

	public void setCitys(Integer citys) {
		this.citys = citys;
	}

	public Integer getCounty() {
		return county;
	}

	public void setCounty(Integer county) {
		this.county = county;
	}

	public Integer getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Integer isAgent) {
        this.isAgent = isAgent;
    }

    public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getShaId() {
		return shaId;
	}

	public void setShaId(Integer shaId) {
		this.shaId = shaId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Integer getAlevel() {
		return alevel;
	}

	public void setAlevel(Integer alevel) {
		this.alevel = alevel;
	}

	public String getAreas() {
		return areas;
	}

	public void setAreas(String areas) {
		this.areas = areas;
	}

	public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStockNum() {
		return stockNum;
	}

	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMoneyOrderImg() {
		return moneyOrderImg;
	}

	public void setMoneyOrderImg(String moneyOrderImg) {
		this.moneyOrderImg = moneyOrderImg;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCheckRemark() {
		return checkRemark;
	}

	public void setCheckRemark(String checkRemark) {
		this.checkRemark = checkRemark;
	}

	public Integer getBatFlag() {
		return batFlag;
	}

	public void setBatFlag(Integer batFlag) {
		this.batFlag = batFlag;
	}

	public String getLegalPerson() {
		return legalPerson;
	}

	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}
	
}
