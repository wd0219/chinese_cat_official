package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktLogUsersLogins;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员登陆记录表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
public interface SktLogUsersLoginsMapper extends BaseMapper<SktLogUsersLogins> {
	
	public List<SktLogUsersLogins> sktLogUsersLoginsfindup(SktLogUsersLogins sktLogUsersLogins);
}
