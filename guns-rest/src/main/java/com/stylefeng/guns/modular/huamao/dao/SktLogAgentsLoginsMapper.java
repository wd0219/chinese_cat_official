package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktLogAgentsLogins;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 代理公司登陆记录表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
public interface SktLogAgentsLoginsMapper extends BaseMapper<SktLogAgentsLogins> {
	
	public List<SktLogAgentsLogins> sktLogAgentsLoginfindup(SktLogAgentsLogins sktLogAgentsLogins);

}
