package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.CheckResult;
import com.stylefeng.guns.modular.huamao.model.CheckResultList;

/**
 * <p>
 * 每日总对账数据表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-14
 */
public interface CheckResultMapper extends BaseMapper<CheckResult> {

	// 展示平台每日对账列表
	List<CheckResultList> selectCheckResultAll(CheckResultList checkResultList);

	// 通过id查询详情
	CheckResultList selectctDetailedById(Integer checkResultId);

}
