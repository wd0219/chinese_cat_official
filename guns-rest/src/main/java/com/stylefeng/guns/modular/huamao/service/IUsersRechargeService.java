package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersRecharge;
import com.stylefeng.guns.modular.huamao.model.UsersRechargeDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户充值表 服务类
 * </p>
 *
 * @author slt123
 * @since 2018-04-13
 */
public interface IUsersRechargeService extends IService<UsersRecharge> {
	/**
	 * 查询用户订单
	 * @param usersRechargeDTO
	 * @return
	 */
	public List<UsersRechargeDTO> showUserDengDanInfo(UsersRechargeDTO usersRechargeDTO);
	public Map<String, Object> Recharge(HttpServletRequest request, String orderNO);
}
