package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.HomeMenusMapper;
import com.stylefeng.guns.modular.huamao.model.HomeMenus;
import com.stylefeng.guns.modular.huamao.service.IHomeMenusService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前台菜单表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-28
 */
@Service
public class HomeMenusServiceImpl extends ServiceImpl<HomeMenusMapper, HomeMenus> implements IHomeMenusService {

	@Override
	public List<Map<String, Object>> selectHomeMenus(HomeMenus homeMenus) {
		// TODO Auto-generated method stub
		return baseMapper.selectHomeMenus(homeMenus);
	}

}
