package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.CheckDetailMapper;
import com.stylefeng.guns.modular.huamao.model.CheckDetail;
import com.stylefeng.guns.modular.huamao.model.CheckDetailList;
import com.stylefeng.guns.modular.huamao.service.ICheckDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 每日用户对账结果数据表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-13
 */
@Service
public class CheckDetailServiceImpl extends ServiceImpl<CheckDetailMapper, CheckDetail> implements ICheckDetailService {

	@Autowired
	CheckDetailMapper cdm;

	// 展示用户每日对账列表
	@Override
	public List<CheckDetailList> selectCheckDetailAll(CheckDetailList checkDetailList) {
		return cdm.selectCheckDetailAll(checkDetailList);
	}

	// 通过id查询详情
	@Override
	public CheckDetailList selectctDetailedById(Integer checkDetailId) {
		return cdm.selectctDetailedById(checkDetailId);
	}

}
