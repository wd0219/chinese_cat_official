package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.model.UsersBankcardsList;

/**
 * <p>
 * 用户的银行卡表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface UsersBankcardsMapper extends BaseMapper<UsersBankcards> {

	// 用户的银行卡表展示
	List<UsersBankcardsList> selectUsersBankcardsAll(UsersBankcardsList usersBankcardsList);

	// 通过id逻辑删除
	public void deleteById2(Integer usersBankcardsId);
	/**
	 * 通过用户id查询所有银行卡
	 * @param usersBankcardsList
	 * @return
	 */
	public List<Map<String,Object>> selectUserBranks(UsersBankcardsList usersBankcardsList);
	/**
	 * 修改用户银行卡状态
	 * @param usersBankcards
	 * @return
	 */
	public Integer updateUserId(UsersBankcards usersBankcards);

    List<Map<String,Object>> selectUsersBankAndBankByUserId(Integer userId);

    int selectCountByUserId(Integer userId);

	int deleteByIdAndUserId(Map<String, Object> map);
}
