package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.util.PhotoUtil;
import com.stylefeng.guns.modular.huamao.util.UploadUtil;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.sql.ResultSet;

@CrossOrigin
@RestController
public class ImageController {

    @RequestMapping("/upload")
    public Result upload(@RequestParam("file") MultipartFile file) throws Exception {

        String upload = UploadUtil.upload("/" + file.getOriginalFilename(), file.getBytes());
        Result rs = Result.OK(upload);
        rs.setUrl(upload);
        rs.setError(0);
        return rs;
    }
}
