package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 银行表
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
@TableName("skt_banks")
public class SktBanks extends Model<SktBanks> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "bankId", type = IdType.AUTO)
    private Integer bankId;
    /**
     * 银行简称
     */
    private String code;
    /**
     * 银行简称
     */
    private String bankName;
    /**
     * 银行卡背景图片
     */
    private String bankImge;
    /**
     * 是否使用 0无效 1有效
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getBankImge() {
        return bankImge;
    }

    public void setBankImge(String bankImge) {
        this.bankImge = bankImge;
    }

    @Override
    protected Serializable pkVal() {
        return this.bankId;
    }

    @Override
    public String toString() {
        return "SktBanks{" +
                "bankId=" + bankId +
                ", code='" + code + '\'' +
                ", bankName='" + bankName + '\'' +
                ", bankImge='" + bankImge + '\'' +
                ", dataFlag=" + dataFlag +
                ", createTime=" + createTime +
                '}';
    }
}
