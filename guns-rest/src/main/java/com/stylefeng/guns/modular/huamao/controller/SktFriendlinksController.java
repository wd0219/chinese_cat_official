package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.modular.huamao.model.SktFriendlinks;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktFriendlinksService;

/**
 * 友情链接控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 14:38:43
 */
@CrossOrigin()
@Controller
@RequestMapping("/sktFriendlinks")
public class SktFriendlinksController {
	   private String PREFIX = "/huamao/sktFriendlinks/";

	    @Autowired
	    private ISktFriendlinksService sktFriendlinksService;
	    /**
	     * 获取友情链接列表
	     */
	    @RequestMapping(value = "/list")
	    @ResponseBody
	    public Result list() {
	    	 List<SktFriendlinks> list = sktFriendlinksService.selectList(null);
	        return Result.OK(list);
	    }

}
