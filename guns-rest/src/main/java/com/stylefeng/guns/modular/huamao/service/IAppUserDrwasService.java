package com.stylefeng.guns.modular.huamao.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersDraws;
import com.stylefeng.guns.modular.huamao.result.AppResult;

import java.math.BigDecimal;

public interface IAppUserDrwasService extends IService<UsersDraws> {
    AppResult applyRedeem(Integer userId,BigDecimal moneyRate, Integer type, BigDecimal money, BigDecimal expenses, BigDecimal expensesrate, BigDecimal taxRatio, BigDecimal ratio, String payPwd);

    JSONObject cashDraw(Integer userId);

    AppResult applyRedeemHHTB(Integer userId, BigDecimal moneyRate, Integer type, BigDecimal money, BigDecimal expenses, BigDecimal expensesrate, BigDecimal taxRatio, BigDecimal ratio, String payPwd);

    JSONObject cashDrawHHTB(Integer userId);
}
