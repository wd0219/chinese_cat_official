package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 待发现金流水表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@TableName("skt_log_cash_freeze")
public class LogCashFreeze extends Model<LogCashFreeze> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 现金类型 1下线升级奖励200元或2000元 2线上营业额获得 3线下营业额 31转正式现金账户支出 32购买库存积分
     */
    private Integer type;
    /**
     * 发起用户ID 0为平台
     */
    private Integer fromId;
    /**
     * 目标用户ID 0为平台
     */
    private Integer userId;
    /**
     * 对应订单号
     */
    private String orderNo;
    /**
     * 操作前的金额
     */
    private BigDecimal preCash;
    /**
     * 流水标志 -1减少 1增加
     */
    private Integer cashType;
    /**
     * 金额
     */
    private BigDecimal cash;
    /**
     * 备注
     */
    private String remark;
    /**
     * 有效状态 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPreCash() {
        return preCash;
    }

    public void setPreCash(BigDecimal preCash) {
        this.preCash = preCash;
    }

    public Integer getCashType() {
        return cashType;
    }

    public void setCashType(Integer cashType) {
        this.cashType = cashType;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LogCashFreeze{" +
        "id=" + id +
        ", type=" + type +
        ", fromId=" + fromId +
        ", userId=" + userId +
        ", orderNo=" + orderNo +
        ", preCash=" + preCash +
        ", cashType=" + cashType +
        ", cash=" + cash +
        ", remark=" + remark +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
