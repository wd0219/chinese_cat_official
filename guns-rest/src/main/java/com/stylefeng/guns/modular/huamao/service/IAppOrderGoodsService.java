package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;

import java.util.List;
import java.util.Map;

public interface IAppOrderGoodsService extends IService<SktOrderGoods> {
    List<Map<String,Object>> selectOrderGoodsByOrderId(int orderId);

    Map<String,Object> selectOrderGoodsByGoodsId(Map<String, Object> map1);
}
