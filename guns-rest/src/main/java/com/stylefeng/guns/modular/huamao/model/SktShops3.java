package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 商家信息表
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
@TableName("skt_shops")
public class SktShops3 extends Model<SktShops> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "shopId", type = IdType.AUTO)
    private Integer shopId;
    /**
     * 经营地址
     */
    private String address;
    /**
     * 线下店铺展示图1
     */
    private String storeImg1;
    /**
     * 线下店铺展示图2
     */
    private String storeImg2;
    /**
     * 线下店铺展示图3
     */
    private String storeImg3;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 经营电话
     */
    private String telephone;
    /**
     * 联系人
     */
    private String linkman;
    /**
     * 企业内容
     */
    private String content;
    /**
     * 积分赠送比例 默认100 线上线下都使用
     */
    private Integer scoreRatio;
    /**
     * 促销语
     */
    private String intro;
    /**
     * 行业内容
     * */
    private String hyName;
    /**
     * 服务营业时间
     */
    private String startTime;
    /**
     * 服务结束时间
     */
    private String endTime;
    /**
     * logo
     * */
    private String logo;
	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStoreImg1() {
		return storeImg1;
	}

	public void setStoreImg1(String storeImg1) {
		this.storeImg1 = storeImg1;
	}

	public String getStoreImg2() {
		return storeImg2;
	}

	public void setStoreImg2(String storeImg2) {
		this.storeImg2 = storeImg2;
	}

	public String getStoreImg3() {
		return storeImg3;
	}

	public void setStoreImg3(String storeImg3) {
		this.storeImg3 = storeImg3;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getLinkman() {
		return linkman;
	}

	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getScoreRatio() {
		return scoreRatio;
	}

	public void setScoreRatio(Integer scoreRatio) {
		this.scoreRatio = scoreRatio;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getHyName() {
		return hyName;
	}

	public void setHyName(String hyName) {
		this.hyName = hyName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "SktShops3 [shopId=" + shopId + ", address=" + address + ", storeImg1=" + storeImg1 + ", storeImg2="
				+ storeImg2 + ", storeImg3=" + storeImg3 + ", shopName=" + shopName + ", telephone=" + telephone
				+ ", linkman=" + linkman + ", content=" + content + ", scoreRatio=" + scoreRatio + ", intro=" + intro
				+ ", hyName=" + hyName + ", startTime=" + startTime + ", endTime=" + endTime + ", logo=" + logo + "]";
	}

	public SktShops3(Integer shopId, String address, String storeImg1, String storeImg2, String storeImg3,
			String shopName, String telephone, String linkman, String content, Integer scoreRatio, String intro,
			String hyName, String startTime, String endTime, String logo) {
		super();
		this.shopId = shopId;
		this.address = address;
		this.storeImg1 = storeImg1;
		this.storeImg2 = storeImg2;
		this.storeImg3 = storeImg3;
		this.shopName = shopName;
		this.telephone = telephone;
		this.linkman = linkman;
		this.content = content;
		this.scoreRatio = scoreRatio;
		this.intro = intro;
		this.hyName = hyName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.logo = logo;
	}

	public SktShops3() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
