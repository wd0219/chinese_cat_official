package com.stylefeng.guns.modular.huamao.util;

public class RemarksGenerate {


    public static String realname(Integer auditStatus){
        String realname="";
        switch (auditStatus){
            case 0:
                realname="申请中";
                break;
            case 1:
                realname="审核通过";
                break;
            case 2:
                realname="审核不通过";
                break;
        }
        return realname;
    }


    public static String upgrade(Integer auditStatus){
        String upgrade="";
        switch (auditStatus){
            case 0:
                upgrade="未支付";
                break;
            case 1:
                upgrade="审核中";
                break;
            case 2:
                upgrade="已取消";
                break;
            case 3:
                upgrade="已审核";
                break;
        }
        return upgrade;
    }



    public static String approve(Integer auditStatus){
        String approve="";
        switch (auditStatus){
            case -1:
                approve="拒绝";
                break;
            case 0:
                approve="未审核";
                break;
            case 1:
                approve="已审核";
                break;
        }
        return approve;
    }


    public static String shop(Integer auditStatus){
        String shop="";
        switch (auditStatus){
            case 1:
                shop="未审核";
                break;
            case 2:
                shop="拒绝";
                break;
            case 3:
                shop="审核通过";
                break;
        }
        return shop;
    }


    public static String agent(Integer auditStatus){
        String agent="";
        switch (auditStatus){
            case -2:
                agent="二审拒绝";
                break;
            case -1:
                agent="一审拒绝";
                break;
            case 0:
                agent="审核中";
                break;
            case 1:
                agent="一审同意";
                break;
            case 2:
                agent="汇款单已上传";
                break;
            case 3:
                agent="二审完结";
                break;
        }
        return agent;
    }

    public static String position(Integer position){
        String str="";
        switch (position){
            case 2:
                str="总裁";
                break;
            case 3:
                str="行政总监";
                break;
            case 4:
                str="财务总监";
                break;
            case 5:
                str="部门经理";
                break;
            case 7:
                str="普通股东";
                break;
        }
        return str;
    }

    public static String status(Integer status){
        String str="";
        switch (status){
            case -2:
                str="二审拒绝";
                break;
            case -1:
                str="一审拒绝";
                break;
            case 0:
                str="审核中";
                break;
            case 1:
                str="一审同意";
                break;
            case 2:
                str="汇款单已上传";
                break;
            case 3:
                str="二审完结";
                break;
        }
        return str;
    }

    public static String alevel(Integer alevel){
        String str="";
        switch (alevel){
            case 10:
                str="省级";
                break;
            case 11:
                str="市级";
                break;
            case 12:
                str="区/县";
                break;
        }
        return str;
    }

    public static String orderLineType(int orderStatus){
        String str="";
        switch (orderStatus){
            case -3:
                str="用户拒收";
                break;
            case -2:
                str="未付款的订单";
                break;
            case -1:
                str="用户取消";
                break;
            case 0:
                str="待发货";
                break;
            case 1:
                str="配送中";
                break;
            case 2:
                str="用户确认收货";
                break;
            case 3:
                str="系统确认收货";
                break;
            case 4:
                str="待审核";
                break;
             case -4:
                str="审核拒绝";
                break;
        }
        return str;
    }

    public static String orderType(int orderStatus){
        String str="";
        switch (orderStatus){
            case 1:
                str="待确认";
                break;
            case 2:
                str="已取消";
                break;
            case 3:
                str="已完成";
                break;
        }
        return str;
    }


    public static String refundStatus(int refundStatus){
        String str="";
        switch (refundStatus){
            case -2:
                str="平台不同意退款";
                break;
            case -1:
                str="商家不同意";
                break;
            case 0:
                str="用户申请退款";
                break;
            case 1:
                str="商家同意退款";
                break;
            case 2:
                str="平台已经退款";
                break;
        }
        return str;
    }
    // 1充值 2下线升级奖励200元或2000元 3线上营业额获得 4待发现金账户转入 5提现拒绝退还 6后台增加现金 7商城购物退款 8提现拒绝手续费退还 9线下营业额获得 31提现支出 32购买库存积分 33线上商城消费 34后台减少金额 35用户升级 36提现手续费  37线下门店消费 38购买牌匾
    public static String type(Integer type){
        String str="";
        switch (type){
            case 1:
                str="充值";
                break;
            case 2:
                str="下线升级奖励200元或2000元";
                break;
            case 3:
                str="线上营业额获得";
                break;
            case 4:
                str="待发现金账户转入";
                break;
            case 5:
                str="提现拒绝退还";
                break;
            case 6:
                str="后台增加现金";
                break;
            case 7:
                str="商城购物退款";
                break;
            case 8:
                str="提现拒绝手续费退还";
                break;
            case 9:
                str="线下营业额获得";
                break;
            case 31:
                str="提现支出";
                break;
            case 32:
                str="购买库存积分";
                break;
            case 33:
                str="线上商城消费";
                break;
            case 34:
                str="后台减少金额";
                break;
            case 35:
                str="用户升级";
                break;
            case 36:
                str="提现手续费";
                break;
            case 37:
                str="线下门店消费";
                break;
            case 38:
                str="购买牌匾";
                break;
            case 39:
                str="转化猫币";
                break;
        }
        return str;
    }

    public static String RecordType(Integer type){
        String str="";
        switch (type){
            case 1:
                str="开元宝转化";
                break;
            case 2:
                str="现金转化";
                break;
            case 3:
                str="开元宝营业额转化";
                break;
        }
        return str;
    }

}
