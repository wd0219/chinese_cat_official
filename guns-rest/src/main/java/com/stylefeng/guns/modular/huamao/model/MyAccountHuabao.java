package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;

public class MyAccountHuabao {
    private Integer isStore;
    private Integer isShop;
    private BigDecimal kaiyuan;
    private BigDecimal kaiyuanTurnover;
    private BigDecimal freezeKaiyuan;

    public Integer getIsStore() {
        return isStore;
    }

    public void setIsStore(Integer isStore) {
        this.isStore = isStore;
    }

    public Integer getIsShop() {
        return isShop;
    }

    public void setIsShop(Integer isShop) {
        this.isShop = isShop;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getKaiyuanTurnover() {
        return kaiyuanTurnover;
    }

    public void setKaiyuanTurnover(BigDecimal kaiyuanTurnover) {
        this.kaiyuanTurnover = kaiyuanTurnover;
    }

    public BigDecimal getFreezeKaiyuan() {
        return freezeKaiyuan;
    }

    public void setFreezeKaiyuan(BigDecimal freezeKaiyuan) {
        this.freezeKaiyuan = freezeKaiyuan;
    }
}
