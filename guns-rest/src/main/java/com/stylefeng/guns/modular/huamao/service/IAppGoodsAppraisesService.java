package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;

import java.util.Map;

public interface IAppGoodsAppraisesService extends IService<GoodsAppraises> {
    GoodsAppraises selectGoodsAppraisesBuUserId(Map<String, Object> map2);
}
