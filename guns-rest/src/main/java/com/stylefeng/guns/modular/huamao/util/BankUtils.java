package com.stylefeng.guns.modular.huamao.util;

import net.sf.json.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

public class BankUtils {
    /**
     * @param accountNo 银行卡号
     * @param idCard    身份证号
     * @param name      真实姓名
     * @param mobile    预留手机号
     */
    public static JSONObject getBankInfoTo4(String accountNo, String idCard, String name, String mobile) {

        String host = "https://bcard3and4.market.alicloudapi.com";
        String path = "/bankCheck4";
        String method = "GET";
        String appcode = "afe7e4a7ea454b629fcaa17ac28a028e";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("accountNo", accountNo);
        querys.put("idCard", idCard);
        querys.put("mobile", mobile);
        querys.put("name", name);


        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            //System.out.println(response.toString());
            //获取response的body

            return JSONObject.fromObject(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 银行卡3元素(三要素)实名认证
     *
     * @param accountNo 银行卡账号
     * @param idCard    身份证号
     * @param name      真实姓名
     * @return
     */
    public static JSONObject getBankInfoTo3(String accountNo, String idCard, String name) {


        String host = "https://bcard3and4.market.alicloudapi.com";
        String path = "/bank3Check";
        String method = "GET";
        String appcode = "afe7e4a7ea454b629fcaa17ac28a028e";
//        String appcode = "8e684ffc445d430a94ea9f30b259e5a6";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("accountNo", accountNo);
        querys.put("idCard", idCard);
        querys.put("name", name);


        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            //System.out.println(response.toString());
            //获取response的body

            return JSONObject.fromObject(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }
}
