package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 代付记录表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public class LogDaifuList implements Serializable {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 日期
	 */
	private Date date;
	/**
	 * 批次
	 */
	private Integer batch;
	/**
	 * 代付公司id
	 */
	private Integer daifuId;
	/**
	 * 类型：1用户 2代理公司
	 */
	private Integer type;
	/**
	 * 提现内部业务单号
	 */
	private String drawNo;
	/**
	 * 提现第三方流水号
	 */
	private String outTradeNo;
	/**
	 * 请求数据
	 */
	private String request;
	/**
	 * 响应数据
	 */
	private String response;
	/**
	 * 状态：-2受理失败 1请求前 2受理中 3 受理成功
	 */
	private Integer status;
	/**
	 * 备注
	 */
	private String remarks;
	/**
	 * 响应时间
	 */
	private Date responseTime;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	public LogDaifuList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LogDaifuList(Integer id, Date date, Integer batch, Integer daifuId, Integer type, String drawNo,
			String outTradeNo, String request, String response, Integer status, String remarks, Date responseTime,
			Date createTime, String beginTime, String endTime) {
		super();
		this.id = id;
		this.date = date;
		this.batch = batch;
		this.daifuId = daifuId;
		this.type = type;
		this.drawNo = drawNo;
		this.outTradeNo = outTradeNo;
		this.request = request;
		this.response = response;
		this.status = status;
		this.remarks = remarks;
		this.responseTime = responseTime;
		this.createTime = createTime;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getBatch() {
		return batch;
	}

	public void setBatch(Integer batch) {
		this.batch = batch;
	}

	public Integer getDaifuId() {
		return daifuId;
	}

	public void setDaifuId(Integer daifuId) {
		this.daifuId = daifuId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getDrawNo() {
		return drawNo;
	}

	public void setDrawNo(String drawNo) {
		this.drawNo = drawNo;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Date responseTime) {
		this.responseTime = responseTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "LogDaifuList [id=" + id + ", date=" + date + ", batch=" + batch + ", daifuId=" + daifuId + ", type="
				+ type + ", drawNo=" + drawNo + ", outTradeNo=" + outTradeNo + ", request=" + request + ", response="
				+ response + ", status=" + status + ", remarks=" + remarks + ", responseTime=" + responseTime
				+ ", createTime=" + createTime + ", beginTime=" + beginTime + ", endTime=" + endTime + "]";
	}

}
