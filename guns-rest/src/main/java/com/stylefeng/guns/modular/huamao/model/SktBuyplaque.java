package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 购买牌匾
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
@TableName("skt_buyplaque")
public class SktBuyplaque extends Model<SktBuyplaque> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "orderId", type = IdType.AUTO)
    private Integer orderId;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 支付方式支付方式 1:线下现金支付 2 现金账户支付 3开元宝支付 4第三方支付 5混合支付 6开元宝营业额
     */
    private Integer payType;
    /**
     * 订单总金额 (realMoney+cash+kaiyuan)
     */
    private BigDecimal totalMoney;
    /**
     * 通过支付公司现金支付的金额
     */
    private BigDecimal realMoney;
    /**
     * 平台现金账户金额
     */
    private BigDecimal cash;
    /**
     * 开元宝金额 1个开元宝=1分钱
     */
    private BigDecimal kaiyuan;
    /**
     * 使用开元宝支付的手续费和税率
     */
    private BigDecimal kaiyuanFee;
    /**
     * 订单备注
     */
    private String orderRemarks;
    /**
     * 下单时间
     */
    private String createTime;
    /**
     * 有效标志 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 状态 0未支付 1已付款未处理 2已取消 3已处理
     */
    private Integer status;
    
//    private String userPhone;
//
//    private Integer userType;
    
//    private String checkTime;

//    public String getCheckTime() {
//		return checkTime;
//	}
//
//	public void setCheckTime(String checkTime) {
//		this.checkTime = checkTime;
//	}

//	public String getUserPhone() {
//		return userPhone;
//	}
//
//	public void setUserPhone(String userPhone) {
//		this.userPhone = userPhone;
//	}
//
//	public Integer getUserType() {
//		return userType;
//	}
//
//	public void setUserType(Integer userType) {
//		this.userType = userType;
//	}

	public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getKaiyuanFee() {
        return kaiyuanFee;
    }

    public void setKaiyuanFee(BigDecimal kaiyuanFee) {
        this.kaiyuanFee = kaiyuanFee;
    }

    public String getOrderRemarks() {
        return orderRemarks;
    }

    public void setOrderRemarks(String orderRemarks) {
        this.orderRemarks = orderRemarks;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

	@Override
	public String toString() {
		return "SktBuyplaque [orderId=" + orderId + ", orderNo=" + orderNo + ", userId=" + userId + ", payType="
				+ payType + ", totalMoney=" + totalMoney + ", realMoney=" + realMoney + ", cash=" + cash + ", kaiyuan="
				+ kaiyuan + ", kaiyuanFee=" + kaiyuanFee + ", orderRemarks=" + orderRemarks + ", createTime="
				+ createTime + ", dataFlag=" + dataFlag + ", status=" + status + ",  ]";
	}

	public SktBuyplaque(Integer orderId, String orderNo, Integer userId, Integer payType, BigDecimal totalMoney,
			BigDecimal realMoney, BigDecimal cash, BigDecimal kaiyuan, BigDecimal kaiyuanFee, String orderRemarks,
			String createTime, Integer dataFlag, Integer status) {
		super();
		this.orderId = orderId;
		this.orderNo = orderNo;
		this.userId = userId;
		this.payType = payType;
		this.totalMoney = totalMoney;
		this.realMoney = realMoney;
		this.cash = cash;
		this.kaiyuan = kaiyuan;
		this.kaiyuanFee = kaiyuanFee;
		this.orderRemarks = orderRemarks;
		this.createTime = createTime;
		this.dataFlag = dataFlag;
		this.status = status;


	}

	public SktBuyplaque() {
		super();
		// TODO Auto-generated constructor stub
	}
    

}
