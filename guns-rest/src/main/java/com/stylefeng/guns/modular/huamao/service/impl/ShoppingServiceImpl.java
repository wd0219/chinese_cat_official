package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.modular.huamao.model.Shopping;
import com.stylefeng.guns.modular.huamao.dao.ShoppingMapper;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IShoppingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物券表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-06-19
 */
@Service
public class ShoppingServiceImpl extends ServiceImpl<ShoppingMapper, Shopping> implements IShoppingService {
    @Override
    public Shopping getShoppingCount(Map<String,Object> map){
        Shopping maps = baseMapper.selectSumShopping(map);
        return maps;
    }
    @Override
    public   List<Map<String, Object>>  selectShoppingList(Map<String,Object> map){
        List<Map<String, Object>> maps = baseMapper.selectShopping(map);
        return  maps;
    }

    
}
