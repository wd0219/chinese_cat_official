package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktExchangeUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-06-20
 */
public interface SktExchangeUserMapper extends BaseMapper<SktExchangeUser> {

}
