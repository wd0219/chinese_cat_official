package com.stylefeng.guns.modular.huamao.model;

import java.util.Date;
import java.util.List;

public class SktUserAllOrders {

    private Integer shopId;
    private String orderNo;
    private Integer orderId;
    private String orderStatus;
    private Integer orderStatus2;
    private Double goodsMoney;
    private Double totalMoney;
    private Date createTime;
    private Integer isPay;
    private String userName;
    private String userAddress;
    private String userPhone;
    private Integer isAppraise;
    private Integer isRefund;
    private Integer isClosed;
    private Date receiveTime;
    private Date deliveryTime;
    private Date paymentTime;
    private String expressNo;
    private Double deliverMoney;
    private Integer expressId;
    private String catName;
    private Integer countGoodsNum;
    private String telephone;
    private String refundOtherReson;
    private Integer refundStatus;
    private List<Goods> goods;


}
