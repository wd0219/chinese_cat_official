package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsScoresMapper;
import com.stylefeng.guns.modular.huamao.model.GoodsScores;
import com.stylefeng.guns.modular.huamao.service.IAppGoodsScoresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppGoodsScoresServiceImpl extends ServiceImpl<GoodsScoresMapper,GoodsScores> implements IAppGoodsScoresService {
    @Autowired
    private GoodsScoresMapper goodsScoresMapper;

    @Override
    public void updateGoodsScores(Map<String, Object> goodsShopScoresMap) {
        goodsScoresMapper.updateGoodsScores(goodsShopScoresMap);
    }
}
