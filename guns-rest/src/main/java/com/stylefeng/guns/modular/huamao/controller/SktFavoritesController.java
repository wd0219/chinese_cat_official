package com.stylefeng.guns.modular.huamao.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.model.SktFavorites;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktFavoritesService;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2018-05-10 16:37:40
 */
@CrossOrigin
@Controller
@RequestMapping("/sktFavorites")
public class SktFavoritesController extends BaseController {

    private String PREFIX = "/huamao/sktFavorites/";

    @Autowired
    private ISktFavoritesService sktFavoritesService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktFavorites.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/sktFavorites_add")
    public String sktFavoritesAdd() {
        return PREFIX + "sktFavorites_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/sktFavorites_update/{sktFavoritesId}")
    public String sktFavoritesUpdate(@PathVariable Integer sktFavoritesId, Model model) {
        SktFavorites sktFavorites = sktFavoritesService.selectById(sktFavoritesId);
        model.addAttribute("item",sktFavorites);
        LogObjectHolder.me().set(sktFavorites);
        return PREFIX + "sktFavorites_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktFavoritesService.selectList(null);
    }

//    /**
//     * 新增
//     */
//    @RequestMapping(value = "/add")
//    @ResponseBody
//    public Object add(SktFavorites sktFavorites) {
//        sktFavoritesService.insert(sktFavorites);
//        return SUCCESS_TIP;
//    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktFavorites sktFavorites) {
        sktFavoritesService.updateById(sktFavorites);
        return SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{sktFavoritesId}")
    @ResponseBody
    public Object detail(@PathVariable("sktFavoritesId") Integer sktFavoritesId) {
        return sktFavoritesService.selectById(sktFavoritesId);
    }
    
    /**************首页****************/
    /**
     * 获取商家 还是商品收藏 
     */
    @RequestMapping(value = "/selectShopGoods")
    @ResponseBody
    public Result selectShopGoods(SktFavorites sktFavorites,Integer pageNum) {
    	PageHelper.startPage(pageNum, 15);
    	List<Map<String,Object>> list = sktFavoritesService.selectShopGoods(sktFavorites);
        PageInfo<Map<String,Object>> info = new PageInfo<Map<String,Object>>(list);
		return Result.OK(list,info.getPages());
      
    }
    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Result delete(@RequestParam Integer sktFavoritesId) {
        boolean deleteById = sktFavoritesService.deleteById(sktFavoritesId);
        if(deleteById==true){
        	return Result.EEROR("取消失败！");
        }
        return Result.OK("取消成功！");
    }
    
    /**
     * 新增收藏信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Result add(SktFavorites sktFavorites) {
    	sktFavorites.setCreateTime(new Date());
        boolean deleteById = sktFavoritesService.insert(sktFavorites);
        if(deleteById==true){
        	return Result.OK("添加收藏成功");
        }
        return Result.OK("添加收藏失败");
    }
    /**
     * 用户是否收藏店铺或者商品 favoriteType userId  targetId商品或者商店id
     */
    @RequestMapping(value = "/favoritesShopGoods")
    @ResponseBody
    public Result favoritesShopGoods(SktFavorites sktFavorites) {
    	EntityWrapper<SktFavorites> ew = new EntityWrapper<SktFavorites>();
    	ew.where("favoriteType={0}", sktFavorites.getFavoriteType());
    	ew.and("userId={0}", sktFavorites.getUserId()).andNew("targetId={0}", sktFavorites.getTargetId());
    	SktFavorites sktFavorites2 = sktFavoritesService.selectOne(ew);
    	if(ToolUtil.isEmpty(sktFavorites2)){
    		return Result.OK("0");
    	}
    	return Result.OK("1");
    }
    /**
     * 取消收藏 favoriteType userId  targetId
     */
    @RequestMapping(value = "/delFavoritesShopGoods")
    @ResponseBody
    public Result delFavoritesShopGoods(SktFavorites sktFavorites) {
    	EntityWrapper<SktFavorites> ew = new EntityWrapper<SktFavorites>();
    	ew.where("favoriteType={0}", sktFavorites.getFavoriteType());
    	ew.and("userId={0}", sktFavorites.getUserId()).andNew("targetId={0}", sktFavorites.getTargetId());
    	boolean delete = sktFavoritesService.delete(ew);
    	if(delete==false){
    		return Result.OK("0");
    	}
    	return Result.OK("1");
    }
}
