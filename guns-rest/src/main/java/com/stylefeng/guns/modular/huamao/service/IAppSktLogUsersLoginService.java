package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktLogUsersLogins;

public interface IAppSktLogUsersLoginService extends IService<SktLogUsersLogins> {
}
