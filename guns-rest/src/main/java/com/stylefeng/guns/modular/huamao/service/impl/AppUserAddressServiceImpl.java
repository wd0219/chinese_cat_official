package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktAreasMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersAddressMapper;
import com.stylefeng.guns.modular.huamao.model.AppUserAddressDTO;
import com.stylefeng.guns.modular.huamao.model.SktAreas;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersAddress;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.IAppUsersAddressService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.MSGUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AppUserAddressServiceImpl extends ServiceImpl<UsersAddressMapper,UsersAddress>  implements IAppUsersAddressService{
    @Autowired
    UsersAddressMapper usersAddressMapper;
    @Autowired
    SktAreasMapper sktAreasMapper;
    @Autowired
    private CacheUtil cacheUtil;

    @Override
    public AppUserAddressDTO selectUserAddressByUserId(Integer userId) {
        return usersAddressMapper.selectUserAddressByUserId(userId);
    }

    @Override
    public UsersAddress selectUserAddressByData(UsersAddress usersAddress) {
        return usersAddressMapper.selectUserAddressByData(usersAddress);
    }

    @Override
    public List<AppUserAddressDTO> selectUserAddressByPageUserId(Map<String, Object> map) {
        return usersAddressMapper.selectUserAddressByPageUserId(map);
    }

    @Override
    public AppResult AddressArea(String areaId) {
        if (areaId.equals("") || areaId.equals(null)){
            areaId="0";
        }
        List<SktAreas> sktAreas = sktAreasMapper.selectAreasByParentId(Integer.parseInt(areaId));
        return AppResult.OK("获取成功",sktAreas);
    }

    @Override
    public AppResult AddressDel(String addressId) {
        UsersAddress usersAddress=new UsersAddress();
        usersAddress.setAddressId(Integer.parseInt(addressId));
        int userMsg = usersAddressMapper.deleteById(usersAddress);
        if (userMsg==0){
            return AppResult.ERROR("删除失败",-1);
        }
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        Map<String, Object> map = new HashMap<>();
        map.put("pageIndex", 0);
        map.put("pageNum", 1);
        map.put("userId", user.getUserId());
        List<AppUserAddressDTO> appUserAddress = usersAddressMapper.selectUserAddressByPageUserId(map);
        if (MSGUtil.isBlank(appUserAddress)){
            return AppResult.OK("删除成功",null);
        }
        appUserAddress.get(0).setIsDefault(1);
        usersAddressMapper.updateById(appUserAddress.get(0));
        return AppResult.OK("删除成功",null);
    }

    @Override
    public void updateUserAddressUsersIsDefault(Integer userId) {
        usersAddressMapper.updateUserAddressUsersIsDefault(userId);
    }


}
