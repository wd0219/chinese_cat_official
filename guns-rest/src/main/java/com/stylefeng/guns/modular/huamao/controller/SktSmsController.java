package com.stylefeng.guns.modular.huamao.controller;


import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 短信发送记录表  前端控制器
 * </p>
 * @author caody
 * @since 2018-06-08
 */
@Controller
@CrossOrigin
public class SktSmsController {

	@Autowired private ISmsService smsService;


	@RequestMapping("/sendMessage")
	@ResponseBody
	public Object send_register_message(HttpServletRequest request,String phone,Integer userId){
		String ip = request.getServerName();
		try {
			JSONObject result = smsService.send_register_message(phone,ip,userId);
			if("OK".equals(result.getString("cod"))){
				return Result.OK(result.getString("code"));
			}else{
				return Result.EEROR("调用失败");
			}

		}catch (Exception e){
			return Result.EEROR("调用失败");
		}
	}
	
}
