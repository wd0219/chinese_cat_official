package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.AppTradeDictDTD;
import com.stylefeng.guns.modular.huamao.model.LogScore;
import com.stylefeng.guns.modular.huamao.model.LogScoreList;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 积分流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
public interface LogScoreMapper extends BaseMapper<LogScore> {

	// 展示积分类表
	List<LogScoreList> selectLogScoreAll(LogScoreList logScoreList);

	/**
	 * 首页显示
	 * @param logScoreList
	 * @return
	 */
    List<Map<String,Object>> selectUserScore(LogScoreList logScoreList);

    LogScore selectSumLogScore(Map<String, Object> map);

    LogScore selectEverySumLogScore(Map<String, Object> map);

    Double selectSumScoreByUserId(Map<String, Object> map);

    List<LogScore> selectLogScoreByUserId(Map<String, Object> map);

    AppTradeDictDTD selectTradeDictBygType(@Param("gType") String gType, @Param("tCode") Integer type);

    /*************************app**************************/
    /**
     * 分红记录
     * @return
     */
	public List<Map<String, Object>> appDividendRecord();

    List<Map<String,Object>> selectLogScoreByUserMap(Map<String, Object> map);
}
