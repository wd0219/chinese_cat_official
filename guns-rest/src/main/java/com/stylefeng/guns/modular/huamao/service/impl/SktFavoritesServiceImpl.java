package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktFavoritesMapper;
import com.stylefeng.guns.modular.huamao.model.SktFavorites;
import com.stylefeng.guns.modular.huamao.service.ISktFavoritesService;

/**
 * <p>
 * 关注(商品/店铺)表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
@Service
public class SktFavoritesServiceImpl extends ServiceImpl<SktFavoritesMapper, SktFavorites> implements ISktFavoritesService {
	@Autowired
	private SktFavoritesMapper sktFavoritesMapper;
	
	@Override
	public List<Map<String,Object>> selectShopGoods(SktFavorites sktFavorites){
		List<Map<String,Object>> list = sktFavoritesMapper.selectShopGoods(sktFavorites);
		return list;
	}
	
}	
