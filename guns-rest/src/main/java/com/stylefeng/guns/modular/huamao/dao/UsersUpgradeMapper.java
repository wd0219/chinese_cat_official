package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.stylefeng.guns.modular.huamao.model.Users;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.UsersUpgrade;
import com.stylefeng.guns.modular.huamao.model.UsersUpgradeDTO;

/**
 * <p>
 * 用户升级角色订单表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
public interface UsersUpgradeMapper extends BaseMapper<UsersUpgrade> {
	/**
	 * 显示用户升级列表
	 * 
	 * @param usersUpgradeDTO
	 * @return
	 */
	public List<UsersUpgradeDTO> showUsersShengJiInfo(UsersUpgradeDTO usersUpgradeDTO);

	/**
	 * 更新升级订单状态
	 * 
	 * @param orderId
	 * @param status
	 */
	public Integer updateUpgradeStatus(@Param(value = "orderId") Integer orderId,
			@Param(value = "status") Integer status, @Param(value = "preRole") Integer preRole,
			@Param(value = "afterRole") Integer afterRole);
	/**
	 * 查找最大值
	 * @return
	 */
	public String selectMaxValue();

	// 查询用户升级前后角色
	public UsersUpgrade findUserRole(Integer userId);

    Integer updateStatusById(UsersUpgrade usersUpgrade);

	/**
	 * 根据用户id查询最大的升级订单id  用于生成订单后返回前台
	 * @param userId
	 * @return
	 */
	public  Integer selectMaxOrderIdByUserId(Integer userId);

    UsersUpgrade selectUsersUpgradeByUserId(Map<String,Object> map);

    int insertOptBing(Map<Object, Object> maps);

	UsersUpgrade selectUsersUpgradeByUserIdAndStatus(Map<Object, Object> maps);

    void delectOptBingByUserIdAndCode(Map<Object, Object> maps);

	void updateByOrderId(UsersUpgrade usersUpgrade);

    UsersUpgrade selectUsersUpgradeByOrderNo(Map<String, Object> orderWhere);

    UsersUpgrade selectUsersUpgradeByUserIdOrderNo(Map<Object, Object> map);

	UsersUpgrade selectUsersUpgradeByUserIdInt(Integer userId);
}
