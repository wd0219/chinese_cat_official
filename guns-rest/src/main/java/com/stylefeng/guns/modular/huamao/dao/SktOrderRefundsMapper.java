package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktOrderRefunds;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefundsList;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefundsToRefund;

import java.util.List;
import java.util.Map;

import com.stylefeng.guns.modular.huamao.model.SktOrdersList;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单退款记录表 Mapper 接口
 * </p>
 *
 * @author caody123
 * @since 2018-04-19
 */
public interface SktOrderRefundsMapper extends BaseMapper<SktOrderRefunds> {

	/**
	 * 查询订单列表
	 * @param paramMap
	 * @return
	 */
	public List<SktOrderRefundsList> selectOrderRefundsList(SktOrderRefundsList sktOrderRefundsList);
	/**
	 * 查询退款订单信息
	 * @param paramMap
	 * @return
	 */
	public SktOrderRefundsToRefund selectOrderRefundsToRefund(Map<String, Object> param);
	/**
	 * 通过退款订单编号查询详情
	 * @param sktOrderRefundsId
	 * @return
	 */
	public List<Map<String, Object>> selectOrdersDetail(@Param(value = "sktOrderRefundsId") Integer sktOrderRefundsId);
	/**
	 * 根据订单id查询商品信息
	 * @param orderId
	 * @return
	 */
	public List<Map<String, Object>> sktOrderRefundsSelectGoodsDetail(@Param(value = "orderId") Integer orderId);

	Integer appShopRefund(Map<String,Object> map);

    SktOrderRefunds selectOrderRefundsByOrderId(Map<String,Object> map);

	public List<Map<String, Object>>  selectOrderRefunds(SktOrdersList sktOrdersList);
}
