package com.stylefeng.guns.modular.huamao.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.support.DateTime;

import org.springframework.web.bind.annotation.RequestParam;

import com.stylefeng.guns.modular.huamao.model.MyDTO;
import com.stylefeng.guns.modular.huamao.model.SktShopIndustryDTO;
import com.stylefeng.guns.modular.huamao.model.SktStaffs;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktStaffsService;

/**
 * lianmengshangjia2控制器
 *
 * @author fengshuonan
 * @Date 2018-04-25 14:20:31
 */
@CrossOrigin
@Controller
@RequestMapping("/sktStaffs")
public class SktStaffsController extends BaseController {

    private String PREFIX = "/huamao/sktStaffs/";

    @Autowired
    private ISktStaffsService sktStaffsService;

    /**
     * 跳转到lianmengshangjia2首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktStaffs.html";
    }

    /**
     * 跳转到添加lianmengshangjia2
     */
    @RequestMapping("/sktStaffs_add")
    public String sktStaffsAdd() {
        return PREFIX + "sktStaffs_add.html";
    }

    /**
     * 跳转到修改lianmengshangjia2
     */
    @RequestMapping("/sktStaffs_update/{sktStaffsId}")
    public String sktStaffsUpdate(@PathVariable Integer sktStaffsId, Model model,SktShopIndustryDTO sktShopIndustryDTO) {
    	sktShopIndustryDTO.setApplyId(sktStaffsId);
    	List<SktShopIndustryDTO> list = sktStaffsService.sktStaffsfindup(sktShopIndustryDTO);
        if(list.size()>0 && list!=null){
        	model.addAttribute("item",list.get(0));
        }else{
        	model.addAttribute("item",null);
        }
        return PREFIX + "sktStaffs_edit.html";
      
    }

    /**
     * 获取lianmengshangjia2列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SktShopIndustryDTO sktShopIndustryDTO) {
        return sktStaffsService.sktStaffsfindAll(sktShopIndustryDTO);
    }

    /**
     * 新增lianmengshangjia2
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktStaffs sktStaffs) {
        sktStaffsService.insert(sktStaffs);
        return SUCCESS_TIP;
    }

    /**
     * 删除lianmengshangjia2
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktStaffsId) {
        sktStaffsService.deleteById(sktStaffsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改lianmengshangjia2
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopIndustryDTO sktShopIndustryDTO) {
        sktStaffsService.sktStaffsupdate(sktShopIndustryDTO);
        return SUCCESS_TIP;
    }
    
    /**
     * 修改lianmengshangjia2
     */
    @RequestMapping(value = "/update2")
    @ResponseBody
    public Object update2(SktShopIndustryDTO sktShopIndustryDTO) {
        sktStaffsService.sktStaffsup1(sktShopIndustryDTO);
        return SUCCESS_TIP;
    }

    /**
     * lianmengshangjia2详情
     */
    @RequestMapping(value = "/detail/{sktStaffsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktStaffsId") Integer sktStaffsId) {
        return sktStaffsService.selectById(sktStaffsId);
    }
    
    @RequestMapping(value = "/detail2/{sktShopApplysId}")
    public String detail1(@PathVariable("sktShopApplysId") Integer sktStaffsId,SktShopIndustryDTO sktShopIndustryDTO,Model model,String legalPerson ) {
    	sktShopIndustryDTO.setApplyId(sktStaffsId);
    	sktShopIndustryDTO.setLegalPerson(legalPerson);
    	List<SktShopIndustryDTO> list = sktStaffsService.sktStaffsfindup(sktShopIndustryDTO);
    	sktShopIndustryDTO.setCheckTime(new DateTime().toString());
    	 model.addAttribute("item",list.get(0));
    	 return PREFIX +"sktStaffs_edit2.html";
    }
    /**
     * 分享链接
     * @param userid
     * @return
     */
    @RequestMapping(value = "/sharingRecords")
    @ResponseBody
    public Result sharingRecords(@RequestParam(value="userId")Integer userId,
    		@RequestParam(value="loginName",required=false)String loginName,
    		@RequestParam(value="userPhone",required=false)String userPhone,
    		@RequestParam(value="userType",required=false)Integer userType,
    		@RequestParam(value="beginTime",required=false)String beginTime,
    		@RequestParam(value="endTime",required=false)String endTime,
    		@RequestParam(value="pageNum",defaultValue="1",required=false)Integer pageNum){
    	try {
    		PageHelper.startPage(pageNum, 12);
    		List<Map<String,Object>> list = sktStaffsService.sharingRecords(userId,loginName,userPhone,userType,beginTime,endTime);
    		PageInfo info = new PageInfo<>(list);
    		return Result.OK(list,info.getPages());
		} catch (Exception e) {
			// TODO: handle exception
			return Result.EEROR("查询失败");
		}
    	
    }
}
