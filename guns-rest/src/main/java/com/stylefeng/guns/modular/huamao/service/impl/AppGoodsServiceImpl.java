package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsMapper;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.service.IAppGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppGoodsServiceImpl extends ServiceImpl<GoodsMapper,Goods> implements IAppGoodsService {
    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public void updateGoodsSaleNum(Map<String, Object> map) {
        goodsMapper.updateGoodsSaleNum(map);
    }

    @Override
    public void updateGoodsAppraiseNum(Map<String, Object> map) {
        goodsMapper.updateGoodsAppraiseNum(map);
    }

    @Override
    public List<Map<String, Object>> selectGoodsByShopId(Map<String, Object> maps) {
        return goodsMapper.selectGoodsByShopId(maps);
    }

    @Override
    public int selectCountByGoodsId(int i) {
        return goodsMapper.selectCountByGoodsId(i);
    }
}
