package com.stylefeng.guns.modular.huamao.model;

import java.util.Date;

public class UsersRealnameDTO extends UsersRealname {
	private static final long serialVersionUID = 1L;
	private Integer realId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 签约卡的手机号
     */
    private String phone;
    /**
     * 姓名
     */
    private String trueName;
    /**
     * 证件类型 1二代身份证 2香港 3澳门 4台湾 5新加坡'
     */
    private Integer cardType;
    /**
     * 身份证号码
     */
    private String cardID;
    /**
     * 身份证正面图片
     */
    private String cardUrl;
    /**
     * 身份证背面图片
     */
    private String cardBackUrl;
    /**
     * 手持身份证照片
     */
    private String handCardUrl;
    /**
     * 审核状态 0 申请中 1 审核通过 2 审核不通过
     */
    private Integer auditStatus;
    /**
     * 证件地址
     */
    private String cardAddress;
    /**
     * 审核人id
     */
    private Integer staffId;
    /**
     * 审核时间
     */
    private Date auditDatetime;
    /**
     * 审核备注
     */
    private String auditRemark;
    /**
     * 创建时间
     */
    private Date addDatetime;
    /**
     * 操作终端 1 PC 2 微信 3 IOS 4 安卓
     */
    private Integer optTerminal;
    /**
     * 操作IP
     */
    private String optIP;
	/**
	 * 开始时间
	 */
	private String beginTime;
	/**
	 * 结束时间
	 */
	private String endTime;
	/**
	 * 会员类型
	 */
	private Integer userType;
	
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public Integer getRealId() {
		return realId;
	}
	public void setRealId(Integer realId) {
		this.realId = realId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public Integer getCardType() {
		return cardType;
	}
	public void setCardType(Integer cardType) {
		this.cardType = cardType;
	}
	public String getCardID() {
		return cardID;
	}
	public void setCardID(String cardID) {
		this.cardID = cardID;
	}
	public String getCardUrl() {
		return cardUrl;
	}
	public void setCardUrl(String cardUrl) {
		this.cardUrl = cardUrl;
	}
	public String getCardBackUrl() {
		return cardBackUrl;
	}
	public void setCardBackUrl(String cardBackUrl) {
		this.cardBackUrl = cardBackUrl;
	}
	public String getHandCardUrl() {
		return handCardUrl;
	}
	public void setHandCardUrl(String handCardUrl) {
		this.handCardUrl = handCardUrl;
	}
	public Integer getAuditStatus() {
		return auditStatus;
	}
	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}
	public String getCardAddress() {
		return cardAddress;
	}
	public void setCardAddress(String cardAddress) {
		this.cardAddress = cardAddress;
	}
	public Integer getStaffId() {
		return staffId;
	}
	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}
	public Date getAuditDatetime() {
		return auditDatetime;
	}
	public void setAuditDatetime(Date auditDatetime) {
		this.auditDatetime = auditDatetime;
	}
	public String getAuditRemark() {
		return auditRemark;
	}
	public void setAuditRemark(String auditRemark) {
		this.auditRemark = auditRemark;
	}
	public Date getAddDatetime() {
		return addDatetime;
	}
	public void setAddDatetime(Date addDatetime) {
		this.addDatetime = addDatetime;
	}
	public Integer getOptTerminal() {
		return optTerminal;
	}
	public void setOptTerminal(Integer optTerminal) {
		this.optTerminal = optTerminal;
	}
	public String getOptIP() {
		return optIP;
	}
	public void setOptIP(String optIP) {
		this.optIP = optIP;
	}
	
}
