package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreeze;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanFreezeList;

/**
 * <p>
 * 待发开元宝营业额流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface LogKaiyuanFreezeMapper extends BaseMapper<LogKaiyuanFreeze> {

	// 代发华宝营业额流水表展示
	List<LogKaiyuanFreezeList> selectLogKaiyuanFreezeAll(LogKaiyuanFreezeList logKaiyuanFreezeList);

}
