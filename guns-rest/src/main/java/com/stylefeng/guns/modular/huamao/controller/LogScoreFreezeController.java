package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreeze;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreezeList;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ILogScoreFreezeService;

/**
 * 代发积分流水表控制器
 *
 * @author fengshuonan
 * @Date 2018-04-16 11:18:28
 */
@CrossOrigin()
@Controller
@RequestMapping("/logScoreFreeze")
public class LogScoreFreezeController extends BaseController {

	private String PREFIX = "/huamao/logScoreFreeze/";

	@Autowired
	private ILogScoreFreezeService logScoreFreezeService;

	/**
	 * 跳转到代发积分流水表首页
	 */
	@RequestMapping("")
	public String index(@RequestParam(required = false) String userPhone, Model model) {
		// 参数是为了在用户点击待发积分查看待发积分详情使用。
		// 判断语句：如果有值说明是用户看详情
		if (userPhone != null && userPhone != "") {
			model.addAttribute("model", userPhone);
		} else {
			model.addAttribute("model", "手机号");
		}
		return PREFIX + "logScoreFreeze.html";
	}

	/**
	 * 跳转到添加代发积分流水表
	 */
	@RequestMapping("/logScoreFreeze_add")
	public String logScoreFreezeAdd() {
		return PREFIX + "logScoreFreeze_add.html";
	}

	/**
	 * 跳转到修改代发积分流水表
	 */
	@RequestMapping("/logScoreFreeze_update/{logScoreFreezeId}")
	public String logScoreFreezeUpdate(@PathVariable Integer logScoreFreezeId, Model model) {
		LogScoreFreeze logScoreFreeze = logScoreFreezeService.selectById(logScoreFreezeId);
		model.addAttribute("item", logScoreFreeze);
		LogObjectHolder.me().set(logScoreFreeze);
		return PREFIX + "logScoreFreeze_edit.html";
	}

	/**
	 * 获取代发积分流水表列表：{userPhone2}用户查看积分详情传来的手机号
	 */
	@RequestMapping(value = "/list/{userPhone2}")
	@ResponseBody
	public Object list(@PathVariable(value = "userPhone2", required = false) String phone,
			LogScoreFreezeList logScoreFreezeList) {
		// 判断如果不是‘手机号’并且logScoreFreezeList.getUserPhone()不是null说明要看用户积分详情，是‘手机号’并且logScoreFreezeList.getUserPhone()是''或是有数据说明是普通的查询所有的积分详情
		if (!phone.equals("手机号") && logScoreFreezeList.getUserPhone() == null) {
			logScoreFreezeList.setUserPhone(phone);
		}
		return logScoreFreezeService.selectLogScoreFreezeAll(logScoreFreezeList);
		// return logScoreFreezeService.selectList(null);
	}

	/**
	 * 新增代发积分流水表
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(LogScoreFreeze logScoreFreeze) {
		logScoreFreezeService.insert(logScoreFreeze);
		return SUCCESS_TIP;
	}

	/**
	 * 删除代发积分流水表
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer logScoreFreezeId) {
		logScoreFreezeService.deleteById(logScoreFreezeId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改代发积分流水表
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(LogScoreFreeze logScoreFreeze) {
		logScoreFreezeService.updateById(logScoreFreeze);
		return SUCCESS_TIP;
	}

	/**
	 * 代发积分流水表详情
	 */
	@RequestMapping(value = "/detail/{logScoreFreezeId}")
	@ResponseBody
	public Object detail(@PathVariable("logScoreFreezeId") Integer logScoreFreezeId) {
		return logScoreFreezeService.selectById(logScoreFreezeId);
	}

	/******************首页*******************/
	/**
	 * 现金记录详情
	 */
	@RequestMapping(value = "/selectUserScoreFreeze", method = RequestMethod.GET)
	@ResponseBody
	public Result selectUserScoreFreeze(LogScoreFreezeList logScoreFreezeList,Integer pageNum){
    	PageHelper.startPage(pageNum, 12);
		List<Map<String,Object>> list = logScoreFreezeService.selectUserScoreFreeze(logScoreFreezeList);
		PageInfo<Map<String,Object>> info = new PageInfo<>(list);
		return Result.OK(list,info.getPages());
	}
}
