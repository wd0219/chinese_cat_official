package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.AccountShop;

import java.util.Map;

public interface IAppAccountShopService extends IService<AccountShop> {

    AccountShop scoreIndex(Integer userId, Integer shopId);

    Map<String,Object> selectScoreByShopId(Integer shopId);

    AccountShop selectAccountShopByUserId(Integer userId);

    void updateAccountShopsByUserId(Map<String, Object> maps);

    boolean updateAccountShopById(AccountShop accountShop);
}
