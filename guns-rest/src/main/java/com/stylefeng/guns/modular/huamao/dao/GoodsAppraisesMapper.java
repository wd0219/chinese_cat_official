package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.GoodsAppraises;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesCustm;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品评价表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsAppraisesMapper extends BaseMapper<GoodsAppraises> {
	List<Map<String,Object>> selectAll(GoodsAppraisesCustm goodsAppraisesCustm);
	public int selectGoodsAppraisesTotal();

    Integer appReplyComments(Map<String, Object> map);
	List<Map<String,Object>> selectAppGoodsAppraises(Integer goodsId);

    GoodsAppraises selectGoodsAppraisesBuUserId(Map<String, Object> map2);
}
