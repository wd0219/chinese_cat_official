package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.modular.huamao.domain.PublicMsg;
import com.stylefeng.guns.modular.huamao.model.Articles;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IArticlesService;
import com.stylefeng.guns.modular.huamao.service.impl.ATestServiceImpl;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@CrossOrigin
public class ATestController {

    @Autowired
    ATestServiceImpl aTestService;
    @Autowired
    CacheUtil cacheUtil;
    @Autowired
    IArticlesService articlesService;
    @Autowired
    StringRedisTemplate stringRedisTemplate;



    private Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/")
    public String  index(){
        //return "hm/view/reload";
        return "hm/start/HM";
    }


    @RequestMapping(value = "/dh")
    public String welcome(ModelMap modelMap) {

       // Object object = cacheUtil.get("15176437220");
        //log.info(object.toString());
        modelMap.addAttribute("msg","欢迎使用华猫接口");
        log.info("欢迎使用华猫接口");
        return "index";
    }

    /**
     * 富文本编辑框
     * @return
     */
    @RequestMapping("/show")
    public String show() {
        return "test";
    }

    /**
     * 获取配置信息
     * @param request
     * @return
     */
    @RequestMapping(value="/ueditor")
    @ResponseBody
    public String ueditor(HttpServletRequest request) {

        return PublicMsg.UEDITOR_CONFIG;
    }

    /**
     * 富文本编辑器图片上传阿里云
     *
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/aliyunUpload")
    @ResponseBody
    public Map<String, Object> upload(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        return aTestService.upload(request);
    }

    /**
     * 通用图片上传
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/imageUpload")
    @ResponseBody
    public Object imageUpload(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        return aTestService.imageUpload(request);
    }

    /**
     * redis缓存失效测试
     */
    @RequestMapping(value = "/testRedis")
    @ResponseBody
    public void testRedis(){
        cacheUtil.set("test","111", Long.valueOf(60));
    }

    /**
     * 测试监听事件
     */
    @RequestMapping(value = "/sendMessage1")
    @ResponseBody
    private void sendMessage1() {
        System.out.println("执行定时任务的时间是："+new Date());
        Date d = new Date();
        stringRedisTemplate.convertAndSend("chat", d.toString());
    }


    /**
     *
     *公告展示首页
     */
    @RequestMapping(value = "/firstNotice")
    @ResponseBody
    public Result  firstNotice(Integer catId){
        Articles articles = new Articles();
        articles.setCatId(catId);
        EntityWrapper<Articles> entityWrapper = new EntityWrapper<Articles>(articles);
        entityWrapper.orderDesc(Collections.singleton("articleId"));
        try {
            articles = articlesService.selectOne(entityWrapper);
            return Result.OK(articles);
        }catch (Exception e){
            return Result.EEROR("调用失败");
        }


    }


    /**
     * 公告展示
     */
    @RequestMapping(value = "/notice")
    @ResponseBody
    public Result  notice(Integer catId,String pageNum){
        Integer pageIndexs=1;
        if (!"".equals(pageNum) && pageNum!=null){
            pageIndexs=Integer.parseInt(pageNum);
        }
        Articles articles = new Articles();
        articles.setCatId(catId);
        PageHelper.startPage(pageIndexs, 10);
        EntityWrapper<Articles> entityWrapper = new EntityWrapper<Articles>(articles);
        entityWrapper.orderDesc(Collections.singleton("articleId"));
        try {
            List<Articles> list = articlesService.selectList(entityWrapper);
            JSONObject json = new JSONObject();
            if (list != null && list.size() > 0) {
                PageInfo pageInfo = new PageInfo(list);
                json.put("tatolPage", pageInfo.getPages());
            } else {
                json.put("tatolPage", 1);
            }
            json.put("maps", list);
            Result  result =  new Result();
            result.setPage(json.getInteger("tatolPage"));
            result.setEntity(json);
            result.setMessageCode(200);
            return result;
        }catch (Exception e){
            return Result.EEROR("展示错误");

        }


    }

}
