package com.stylefeng.guns.modular.huamao.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktLogSms;

/**
 * <p>
  * 短信发送记录表 Mapper 接口
 * </p>
 *
 * @author caody
 * @since 2018-06-08
 */
public interface SktLogSmsMapper extends BaseMapper<SktLogSms> {

}