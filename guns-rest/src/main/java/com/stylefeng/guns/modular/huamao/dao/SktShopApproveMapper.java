package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopApprove;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 企业认证表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */

public interface SktShopApproveMapper extends BaseMapper<SktShopApprove> {

    SktShopApprove selectShopApproveByUserId(Integer userId);

    SktShopApprove selectShopApproveByUserId2(Integer userId);

    List<SktShopApprove> selectShopApproveByUserId3(Map<String,Object> map);
}
