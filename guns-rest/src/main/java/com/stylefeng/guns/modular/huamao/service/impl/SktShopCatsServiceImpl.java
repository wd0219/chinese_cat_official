package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.Wrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopCatsMapper;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.model.SktShopCats;
import com.stylefeng.guns.modular.huamao.service.ISktShopCatsService;

/**
 * <p>
 * 店铺分类表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-29
 */
@Service
public class SktShopCatsServiceImpl extends ServiceImpl<SktShopCatsMapper, SktShopCats> implements ISktShopCatsService {
	@Autowired
	private SktShopCatsMapper sktShopCatsMapper;
	@Override
	public List<SktShopCats> selectByParentId(Integer goodsCatsId) {
		List<SktShopCats> list = sktShopCatsMapper.selectByParentId(goodsCatsId);
		return list;
	}
	@Override
	public List<SktShopCats> selectShopIdList(Integer shopId,Integer parentId) {
		List<SktShopCats> list = sktShopCatsMapper.selectShopIdList(shopId,parentId);
		return list;
	}
	 @Override
	 public List categoryShop(Integer shopId) {
		 Map<String, Object> map=new HashMap<>();
		 List list=new ArrayList();
		 List<SktShopCats> list1 = sktShopCatsMapper.selectShopIdList(shopId,0);
		 if (list1!=null && list1.size()>0){
		 	for (SktShopCats sktShopCats1:list1){
				Map<String, Object> map1=new HashMap<>();
		 		map1.put("catId",sktShopCats1.getCatId());
				map1.put("parentId",sktShopCats1.getParentId());
				map1.put("catName",sktShopCats1.getCatName());
				List<SktShopCats> list2 = sktShopCatsMapper.selectShopIdList(shopId,sktShopCats1.getCatId());
				map1.put("secondColumn",list2);
				list.add(map1);
			}
		 }
		 return  list;
	 }


}
