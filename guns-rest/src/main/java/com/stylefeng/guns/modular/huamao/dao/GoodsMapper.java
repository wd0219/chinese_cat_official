package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesDTO;
import com.stylefeng.guns.modular.huamao.model.GoodsAppraisesShow;
import com.stylefeng.guns.modular.huamao.model.GoodsAttributes;
import com.stylefeng.guns.modular.huamao.model.GoodsCatsLevel;
import com.stylefeng.guns.modular.huamao.model.GoodsCustm;
import com.stylefeng.guns.modular.huamao.model.GoodsList;
import com.stylefeng.guns.modular.huamao.model.GoodsSellStock;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsMapper extends BaseMapper<Goods> {
	List<Map<String,Object>> selectByConditions(GoodsCustm goodsCustm);
	List<Goods> selectGoodsByGoodsCatId(int id);
    int batchUpdate(List<Map<String, Object>> list);
    int batchUpdatePo(List<Goods> list);
	public int selectisSaleCount();
	public int selectGoodsStatusCount();
	public int selectisSaleTotal();
	public int selectGoodsStatusTotal();
	/**
	 * 今日热销
	 * @return
	 */
	List<Map<String, Object>> selectTodayHotSale();
	/**
	 * 查询品牌所有商品
	 * @param brandId
	 * @return
	 */
	List<Map<String,Object>> selectTodayHotSaleAll(@Param(value = "brandId") Integer brandId);
	/**
	 * 通过分类查询商品，商标信息
	 * @param item
	 * @return
	 */
	public List<Map<String, Object>> selectByCatName(List<String> item);
	/**
	 * 通过商品id查询商品分类级别
	 * @param goodsId
	 * @return
	 */
	public GoodsCatsLevel selectCatsLevelByGoodsid(Integer goodsId);
	/**
	 * 查询今日推荐商品
	 * @return
	 */
	public List<Map<String, Object>> todayHotSaleGoods(Map<String, Object> map);
	/**
	 * 查询商品库存 上架
	 * @param goodsSellStock
	 * @return
	 */
	public List<GoodsSellStock> selectByIsSale(GoodsSellStock goodsSellStock);
	/**
	 * 审核违规
	 * @param goodsSellStock
	 * @return
	 */
	public List<GoodsSellStock> selectByIsGoodsStatus(GoodsSellStock goodsSellStock);
	/**
	 * 是否预警库存
	 * @param goodsSellStock
	 * @return
	 */
	public List<GoodsSellStock> selectByIsWarnStock(GoodsSellStock goodsSellStock);
	/**
	 * 所有订单
	 * @param shopId
	 * @param shopCatId2 
	 * @param shopCatId1 
	 * @return
	 */
	public List<GoodsAppraisesDTO> selectAllOrder(@Param(value="shopId") Integer shopId,
			@Param(value="shopCatId1")Integer shopCatId1, 
			@Param(value="shopCatId2")Integer shopCatId2);
	/**
	 * 查询所有评论
	 * @param orderId
	 * @return
	 */
	public List<Map<String, Object>> selectAppraise(@Param(value="orderId")Integer orderId);
	/**
	 * 更新商品状态
	 * @param goodId
	 * @param isSale
	 * @param isBest
	 * @param isHot
	 * @param isNew
	 * @param isRecom
	 * @param isDelete
	 * @return
	 */
	public Integer updateStatus(@Param(value="goodId")Integer[] goodId, 
			@Param(value="isSale")Integer isSale, 
			@Param(value="isBest")Integer isBest, 
			@Param(value="isHot")Integer isHot, 
			@Param(value="isNew")Integer isNew, 
			@Param(value="isRecom")Integer isRecom,
			@Param(value="isDelete")Integer isDelete);
	/**
	 * 查询默认属性
	 * @param defaultProperty
	 * @return
	 */
	public List<Map<String, Object>> selectDefaultProperty(@Param(value="defaultProperty") String[] defaultProperty);
	/**
	 * 首页查询商品
	 * @param goods
	 * @return
	 */
	public List<Map<String,Object>> selectByGoods(GoodsList goods);
	/**
	 * 查询商品属性
	 * @param goods
	 * @return
	 */
	public List<GoodsAttributes> selectGoodsAttr(GoodsList goods);

    public String selectMax();
	/**
	 * 根据最后一级分类获得品牌
	 */
	public List<Map<String,Object>> getCatBrands(@Param(value="catId") Integer catId);
	
	/**
	 * 根据商品id查询信息回显
	 * @param goodsId
	 * @return
	 */
	public List<Map<String, Object>> selectByGoodsId(@Param(value="goodsId") Integer goodsId);
	/**
	 * 根据shopId，goodsId商品属性回显
	 * @param goodsId
	 * @param shopId
	 * @return
	 */
	List<Map<String, Object>> selectAttrShow(@Param(value="goodsId")Integer goodsId, 
			@Param(value="shopId")Integer shopId);
	/**
	 *  根据shopId，goodsId商品规格回显
	 * @param goodsId
	 * @param shopId
	 * @return
	 */
	List<Map<String, Object>> selectSpecShow(@Param(value="goodsId")Integer goodsId, 
			@Param(value="shopId")Integer shopId);

	/**
	 * 批量删除
	 * @param goodsId
	 * @return
	 */
	public Integer deleteBatchIds(@Param(value="goodsId")Integer[] goodsId);

	/*******************************App*****************/
	List<Map<String,Object>> selectAppByOrder(Map<String,Object> map);
	/**
	 *  商品id 查询在店铺中的分类
	 * @param goodsId
	 * @return
	 */
	public Map<String, Object> selectGoodsShopCat(@Param(value="goodsId")Integer goodsId);
	/**
	 * 根据商品id查询评价
	 * @param goodsId
	 * @return
	 */
	public List<GoodsAppraisesShow> selectGoodsAppraises(@Param(value="goodsId")Integer goodsId);

	public List<Map<String,Object>> selectRecomGoods(Map<String,Object> map);
	/**
	 * 根据商品id查询评分
	 * @param goodsId
	 * @return
	 */
	public Map<String, Object> selectGoodsScores(@Param(value="goodsId")Integer goodsId);
	/**
	 * 根据店铺id查询 热销商品
	 * @param shopId
	 * @return
	 */
	public List<Map<String, Object>> selectGoodsHots(@Param(value="shopId")Integer shopId);

    void updateGoodsSaleNum(Map<String, Object> map);

    void updateGoodsAppraiseNum(Map<String, Object> map);

    List<Map<String,Object>> selectGoodsByShopId(Map<String, Object> maps);

	int selectCountByGoodsId(int i);

    Set<Integer> appFindShopsByGoodsId(@Param("goodsIds") List<Integer> goodsIds);

	List<Map<String,Object>>  appFindGoodsByshopId(Map<String, Object> map3);
	/**
	 * 查询下拉列表
	 * @param gType
	 * @return
	 */
	List<Map<String, Object>> selectDownSelect(@Param(value="gType")String gType);
	/**
	 * 自营店
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectRecomGoodsSelf(Map<String, Object> map);
	public List<Map<String, Object>>  goodsShopSort (Map<String, Object> map);
	
	public List<Map<String, Object>> selectGoodsCat(@Param(value="goodsId")Integer goodsId,
			@Param(value="shopId")Integer shopId);



	public List<Goods> selectGoodsByMap(Map<String, Object> map);
}
