package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 代理公司的股东表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-19
 */
@TableName("skt_agents_stockholder")
public class SktAgentsStockholder extends Model<SktAgentsStockholder> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "shId", type = IdType.AUTO)
    private Integer shId;
    /**
     * 代理公司ID
     */
    private Integer agentId;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 管理股东类型  1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
     */
    private Integer type;
    @TableField(exist=false)
	private String stype;
    /**
     * 拥有的股票分数
     */
    private Integer stockNum;
    /**
     * 发展的商家数量
     */
    private Integer storeNum;
    /**
     * 完成70%(落地)之前冻结分红
     */
    private BigDecimal twoFreezeDividend;
    /**
     * 完成70%完成100%之间冻结分红
     */
    private BigDecimal threeFreezeDividend;
    /**
     * 未完成商家数量冻结分红
     */
    private BigDecimal storeFreezeDividend;
    /**
     * 上次分红
     */
    private BigDecimal lastDividend;
    /**
     * 上次分红时间
     */
    private Date dividendTime;
    /**
     * 累计分红
     */
    private BigDecimal totalDividend;
    /**
     * 创建时间
     */
    private Date createTime;


    public Integer getShId() {
        return shId;
    }

    public void setShId(Integer shId) {
        this.shId = shId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getStoreNum() {
        return storeNum;
    }

    public void setStoreNum(Integer storeNum) {
        this.storeNum = storeNum;
    }

    public BigDecimal getTwoFreezeDividend() {
        return twoFreezeDividend;
    }

    public void setTwoFreezeDividend(BigDecimal twoFreezeDividend) {
        this.twoFreezeDividend = twoFreezeDividend;
    }

    public BigDecimal getThreeFreezeDividend() {
        return threeFreezeDividend;
    }

    public void setThreeFreezeDividend(BigDecimal threeFreezeDividend) {
        this.threeFreezeDividend = threeFreezeDividend;
    }

    public BigDecimal getStoreFreezeDividend() {
        return storeFreezeDividend;
    }

    public void setStoreFreezeDividend(BigDecimal storeFreezeDividend) {
        this.storeFreezeDividend = storeFreezeDividend;
    }

    public BigDecimal getLastDividend() {
        return lastDividend;
    }

    public void setLastDividend(BigDecimal lastDividend) {
        this.lastDividend = lastDividend;
    }

    public Date getDividendTime() {
        return dividendTime;
    }

    public void setDividendTime(Date dividendTime) {
        this.dividendTime = dividendTime;
    }

    public BigDecimal getTotalDividend() {
        return totalDividend;
    }

    public void setTotalDividend(BigDecimal totalDividend) {
        this.totalDividend = totalDividend;
    }

    public Date getCreateTime() {
        return createTime;
    }
    
    public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.shId;
    }

    @Override
    public String toString() {
        return "SktAgentsStockholder{" +
        "shId=" + shId +
        ", agentId=" + agentId +
        ", userId=" + userId +
        ", type=" + type +
        ", stockNum=" + stockNum +
        ", storeNum=" + storeNum +
        ", twoFreezeDividend=" + twoFreezeDividend +
        ", threeFreezeDividend=" + threeFreezeDividend +
        ", storeFreezeDividend=" + storeFreezeDividend +
        ", lastDividend=" + lastDividend +
        ", dividendTime=" + dividendTime +
        ", totalDividend=" + totalDividend +
        ", createTime=" + createTime +
        "}";
    }
}
