package com.stylefeng.guns.modular.huamao.model;

public class GoodsShopsModel {

    private String content;
    private int goodsId;
    private int goodsScore;
    private String images;
    private int serviceScore;
    private int timeScore;
    private int goodsSpecId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public int getGoodsScore() {
        return goodsScore;
    }

    public void setGoodsScore(int goodsScore) {
        this.goodsScore = goodsScore;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public int getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(int serviceScore) {
        this.serviceScore = serviceScore;
    }

    public int getTimeScore() {
        return timeScore;
    }

    public void setTimeScore(int timeScore) {
        this.timeScore = timeScore;
    }

    public int getGoodsSpecId() {
        return goodsSpecId;
    }

    public void setGoodsSpecId(int goodsSpecId) {
        this.goodsSpecId = goodsSpecId;
    }

    @Override
    public String toString() {
        return "GoodsShopsModel{" +
                "content='" + content + '\'' +
                ", goodsId=" + goodsId +
                ", goodsScore=" + goodsScore +
                ", images='" + images + '\'' +
                ", serviceScore=" + serviceScore +
                ", timeScore=" + timeScore +
                ", goodsSpecId=" + goodsSpecId +
                '}';
    }
}
