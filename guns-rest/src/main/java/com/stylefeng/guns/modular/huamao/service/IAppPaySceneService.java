package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.PayScene;

import java.util.Map;

public interface IAppPaySceneService extends IService<PayScene> {
    PayScene selectPayBySceneCode(Map<String, Object> map);
}
