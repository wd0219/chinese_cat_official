package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.GoodsSpecs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 商品规格表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface GoodsSpecsMapper extends BaseMapper<GoodsSpecs> {

    void updateGoodsSpecsById(Map<String, Object> map);
}
