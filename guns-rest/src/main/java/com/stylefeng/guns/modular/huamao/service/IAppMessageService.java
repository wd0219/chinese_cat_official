package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;

import java.util.List;

public interface IAppMessageService extends IService<Messages> {

    List<Messages> message(Integer userId);


    List<MessagesList> getMessage(Messages messages);

}
