package com.stylefeng.guns.modular.huamao.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.modular.huamao.service.IUsersRealnameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.dao.UsersBankcardsMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersRealnameMapper;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.model.UsersBankcardsList;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;
import com.stylefeng.guns.modular.huamao.service.IUsersBankcardsService;
import com.stylefeng.guns.modular.huamao.util.BankUtils;

import net.sf.json.JSONObject;

/**
 * <p>
 * 用户的银行卡表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
@Service
public class UsersBankcardsServiceImpl extends ServiceImpl<UsersBankcardsMapper, UsersBankcards>
		implements IUsersBankcardsService {

	@Autowired
	private UsersBankcardsMapper ubm;
	@Autowired
	private UsersRealnameMapper usersRealnameMapper;
	@Autowired
	private UsersMapper usersMapper;
	@Autowired
	private IUsersRealnameService usersRealnameService;
	// 用户的银行卡表展示
	@Override
	public List<UsersBankcardsList> selectUsersBankcardsAll(UsersBankcardsList usersBankcardsList) {
		return ubm.selectUsersBankcardsAll(usersBankcardsList);
	}

	// 通过id进行逻辑删除
	@Override
	public void deleteById2(Integer usersBankcardsId) {
		ubm.deleteById2(usersBankcardsId);
	}

	@Override
	public List<Map<String,Object>> selectUserBranks(UsersBankcardsList usersBankcardsList) {
		List<Map<String,Object>> list = ubm.selectUserBranks(usersBankcardsList);
		return list;
	}

	@Override
	public String quickApproveBank(UsersBankcardsList usersBankcardsList) {
		String cardID = usersBankcardsList.getCardID();
		Integer countCardId = usersRealnameMapper.selectUserCardIds(cardID);
		if(countCardId > 0){
			return "2";
		}
		JSONObject json = BankUtils.getBankInfoTo3(usersBankcardsList.getAccNo(), usersBankcardsList.getCardID(), usersBankcardsList.getTrueName());
		System.out.println("____________________"+json+"____________________");
		String status = json.get("status").toString();
		if(!status.equals("01")){
			return "银行卡信息有误！请填写正确信息";
		}
		Integer i = insertUsersRealname(usersBankcardsList);//新增实名认证表
		Users users = new Users();
		users.setUserId(usersBankcardsList.getUserId());
		users.setTrueName(usersBankcardsList.getTrueName());
		Integer updateById = usersMapper.updateById(users);
		
		Integer j = insersUsersBankcards(usersBankcardsList);//新增银行卡表
		
		if(i == 0  || j == 0  || updateById == 0){
			return "FAILS";
		}else{
			return "1";
		}

	}
	public Integer insertUsersRealname(UsersBankcardsList usersBankcardsDTO){
		UsersRealname usersRealname = new UsersRealname();
		usersRealname.setUserId(usersBankcardsDTO.getUserId());
		usersRealname.setTrueName(usersBankcardsDTO.getTrueName());
		
		if(ToolUtil.isEmpty(usersBankcardsDTO.getPhone())){
			usersRealname.setPhone(" ");
		}else{
			usersRealname.setPhone(usersBankcardsDTO.getPhone());
		}
		usersRealname.setAuditStatus(1);
		usersRealname.setCardID(usersBankcardsDTO.getCardID());
		usersRealname.setCardType(1);
		usersRealname.setAuditRemark("银行卡快速认证");
		usersRealname.setAuditDatetime(new Date());
		usersRealname.setAddDatetime(new Date());
		int i = usersRealnameMapper.insert(usersRealname);
		return i;
	}

	public  Integer insersUsersBankcards(UsersBankcardsList usersBankcardsDTO){
		UsersBankcards usersBankcards = new UsersBankcards();
		usersBankcards.setUserId(usersBankcardsDTO.getUserId());
		usersBankcards.setType(2);
		usersBankcards.setProvinceId(usersBankcardsDTO.getProvinceId());
		usersBankcards.setCityId(usersBankcardsDTO.getCityId());
		usersBankcards.setAreaId(usersBankcardsDTO.getAreaId());
		usersBankcards.setAccArea(usersBankcardsDTO.getAccArea());
		usersBankcards.setAccNo(usersBankcardsDTO.getAccNo());
		
		if(ToolUtil.isEmpty(usersBankcardsDTO.getPhone())){
			usersBankcards.setPhone(" ");
		}else{
			usersBankcards.setPhone(usersBankcardsDTO.getPhone());
		}
		
		usersBankcards.setBankId(usersBankcardsDTO.getBankId());
		usersBankcards.setDataFlag(1);
		usersBankcards.setCreateTime(new Date());
		int i = ubm.insert(usersBankcards);
		return i;
	}

	@Override
	public Integer updateUserBrankStatus(UsersBankcards usersBankcards) {
		//修改用户银行卡默认状态
		ubm.updateUserId(usersBankcards);
		//更新为默认银行卡
		usersBankcards.setType(1);
		Integer updateById = ubm.updateById(usersBankcards);
		return updateById;
	}

	@Override
	public String addBankcards(UsersBankcardsList usersBankcards) {
		if(usersBankcards.getAccNo() == null || usersBankcards.getAccNo() == ""){
			return "6";
		}
		UsersBankcards ub = new UsersBankcards();
		ub.setUserId(usersBankcards.getUserId());
		ub.setDataFlag(1);
		ub.setAccNo(usersBankcards.getAccNo());
		EntityWrapper<UsersBankcards> ew1 = new EntityWrapper<UsersBankcards>(ub);
		List<UsersBankcards> usersBankcards1 = this.selectList(ew1);
		if(usersBankcards1 != null && usersBankcards1.size() > 0){
			return "5";
		}
		//先根据用户查询多少个5个返回 不能超过5过
		List<Map<String,Object>> list = ubm.selectUserBranks(usersBankcards);
		if(list.size()>5){
			return "3";
		}
		Integer userId = usersBankcards.getUserId();
		UsersRealname usersRealname = new UsersRealname();
		usersRealname.setUserId(userId);
		usersRealname.setAuditStatus(1);
		EntityWrapper<UsersRealname> ew = new EntityWrapper<UsersRealname>(usersRealname);
		UsersRealname usersRealname1 = usersRealnameService.selectOne(ew);
		JSONObject json = BankUtils.getBankInfoTo3(usersBankcards.getAccNo(), usersRealname1.getCardID(), usersRealname1.getTrueName());
		System.out.println("____________________"+json+"____________________");
		String status = json.get("status").toString();
		if(!status.equals("01")){
			return "2";
		}
		UsersBankcards usersBankcards2 = new UsersBankcards();
		usersBankcards2.setUserId(usersBankcards.getUserId());
		usersBankcards2.setType(usersBankcards2.getType());
		usersBankcards2.setProvinceId(usersBankcards.getProvinceId());
		usersBankcards2.setCityId(usersBankcards.getCityId());
		usersBankcards2.setAreaId(usersBankcards.getAreaId());
		usersBankcards2.setBankId(usersBankcards.getBankId());
		usersBankcards2.setAccArea(usersBankcards.getAccArea());
		usersBankcards2.setAccNo(usersBankcards.getAccNo());
		usersBankcards2.setPhone(usersBankcards.getPhone());
		usersBankcards2.setDataFlag(1);
		usersBankcards2.setCreateTime(new Date());
		Integer insert = ubm.insert(usersBankcards2);
		if(ToolUtil.isEmpty(insert)){
			return "1";
		}
		return "4";
	}
}
