package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgent;
import com.stylefeng.guns.modular.huamao.model.LogKaiyuanAgentList;

/**
 * <p>
 * 代理公司开元宝流水表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
public interface LogKaiyuanAgentMapper extends BaseMapper<LogKaiyuanAgent> {

	// 代理公司华宝流水表展示
	List<LogKaiyuanAgentList> selectLogKaiyuanAgentAll(LogKaiyuanAgentList logKaiyuanAgentList);

}
