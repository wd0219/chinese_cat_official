package com.stylefeng.guns.modular.huamao.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.model.SktShops2;
import com.stylefeng.guns.modular.huamao.model.SktShops3;

/**
 * <p>
 * 商家信息表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-14
 */
public interface SktShopsMapper extends BaseMapper<SktShops> {
	
	public List<SktShops> sktShopsfindAll(SktShops sktShops);
	/**
	 * 查询今日新增总数
	 * @param 
	 * @return
	 */
	public int selectCount();
	/**
	 * 查询总数
	 * @param 
	 * @return
	 */
	public int getTotal();
	/*
	 * 店铺街展示店铺
	 * */
	public List<Map<String, Object>> findShopsAll(@Param(value="hyId")Integer hyId,
			@Param(value="provinceId")Integer provinceId,
			@Param(value="totalScore")Integer totalScore,
			@Param(value="onlineTurnover")Integer onlineTurnover, 
			@Param(value="userId")Integer userId);
	/*
	 * 店铺街展示商品推荐
	 * */
	public List<Map<String, Object>> findShopsByIdtj(@Param(value="shopId")Integer shopId, @Param(value="limit")Integer limit);
	/*
     * 店铺街查询行业
     * */
	public List<Map<String, Object>> findShopsHyName();
	/*
     * 店铺街根据行业ID点进行业查询该行业店铺
     * */
	public List<Map<String, Object>> selectByHyIdShops(Integer hyId);
	
	/*
	 * 查询热门品牌findBrandId
	 * */
	public List<Map<String, Object>> findBrandId();
	  /*
     * 根据店铺ID查询商品
     * */
	public List<Map<String, Object>> selectShopsGoodsId(Integer shopId);
	/*
	 * 自营街展示店铺
	 * */
	public List<Map<String, Object>> selectShopsZy();
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectShopNameShops(SktShops2 sktShops2);
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectLogoShops(SktShops2 sktShops2); 
	/*
     * 根据LOGO 店铺名 loginName查询店铺ID
     * */
	public List<Map<String, Object>> selectLoginNameShops(SktShops2 sktShops2);
	  /*
     * 根据ZY店铺ID查询商品
     * */
	public List<Map<String, Object>> selectShopsZyGoodsId(Integer shopId);
	  /*
     * 轮播图首页
     * */
	public List<Map<String, Object>> findWebLunbo();

	/**
	 *楼层广告图
	 * @param map
	 * @return
	 */
	public Map<String,Object> floorAd(Map<String, Object> map);

	/*
	 * 单个商品详情
	 * */
	public List<Map<String,Object>> selectGoods(Integer goodsId);
	/*
	 * 线下商家个人详情
	 * */
	public List<Map<String,Object>> findByUserId(Integer userId);
	/*
	 * 线下商家个人详情 金额等
	 * */
	public List<Map<String,Object>> findByUserIdMn(Integer userId);
	/*
	 * 线下商家个人详情 最近月份支入支出
	 * */
	public List<Map<String,Object>> findByUserIdZrZc(Integer userId);
	/*
	 * 线下商家个人详情 最近月份支入支出
	 * */
	public List<Map<String,Object>> findByShopsXinxi(Integer userId);
	/*
	 * 修改店铺设置
	 * */
	public Integer updateShops(SktShops sktShops);
	/*
	 * 线上商家店铺详情
	 * */
	public List<Map<String,Object>> findByXSShopsXinxi(Integer shopId);
	/*
	 * 回显店铺设置
	 * */
	public List<Map<String,Object>> findByXSShopsImg(Integer shopId);
	/*
	 * 修改线上店铺设置
	 * */
	public Integer updateByXSShopsImg(SktShops sktShops);
	/*
	 * 回显店铺运费
	 * */
	public List<Map<String,Object>> findByYunFei(Integer shopId);
	/*
	 * 修改店铺运费
	 * */
	public Integer updateByYunFei(SktShops2 sktShops2);
	/*
	 * 代付款订单
	 * */
	public List<Map<String,Object>> findDaiOrders(@Param(value="shopId")Integer shopId, 
			@Param(value="orderNo")String orderNo, 
			@Param(value="beginTime")String beginTime, 
			@Param(value="endTime")String endTime);
	/*
	 * 以发货订单
	 * */
	public List<Map<String,Object>> findYiOrders(@Param(value="shopId")Integer shopId, 
			@Param(value="orderNo")String orderNo, 
			@Param(value="beginTime")String beginTime, 
			@Param(value="endTime")String endTime);
	/*
	 * 代发货订单
	 * */
	public List<Map<String,Object>> findDaiFaHuoOrders(@Param(value="shopId")Integer shopId, 
			@Param(value="orderNo")String orderNo, 
			@Param(value="beginTime")String beginTime, 
			@Param(value="endTime")String endTime);
	/*
	 * 退款订单
	 * */
	public List<Map<String,Object>> findTuiOrders(@Param(value="shopId")Integer shopId, 
			@Param(value="orderNo")String orderNo, 
			@Param(value="beginTime")String beginTime, 
			@Param(value="endTime")String endTime,
			@Param(value="refundStatus")Integer refundStatus);
	/*
	 * 待收货订单
	 * */
	public List<Map<String,Object>> findDaiShouOrders(@Param(value="shopId")Integer shopId, 
			@Param(value="orderNo")String orderNo, 
			@Param(value="beginTime")String beginTime, 
			@Param(value="endTime")String endTime);
	/*
	 * 取消订单
	 * */
	public List<Map<String,Object>> findQuxiaoOrders(@Param(value="shopId")Integer shopId, 
			@Param(value="orderNo")String orderNo, 
			@Param(value="beginTime")String beginTime, 
			@Param(value="endTime")String endTime);
	
    List<SktShops> getShopByLevelId(Map<String, Object> map);
	/*
	 * 根据分类查询品牌商品
	 * */
	public List<Map<String,Object>> findHPcats(SktShops2 sktShops2);
	
	/**
	 * 根据用户查询商店Id
	 * @param userId
	 * @return
	 */
	public Integer selectShopIdByUserId(@Param(value="userId")Integer userId);
	/*
	 * 线下店铺回显
	 * */
	public List<Map<String,Object>> findShopsId(SktShops3 sktshops3);
	/*
	 * 线下店铺回显
	 * */
	public List<Map<String,Object>> selectShopsName(String shopName);
	//查询所有商家的userId
	List<Integer> selectAllId();

    Map<String,Object> selectUsersShopsByUserShopId(Integer shopId);

    /**
     * 周边主页（包括商家名称搜索,点击行业搜索）
     * @param lng
     * @param lat
     * @param shopName
     * @param industry
     * @return
     */
    public List<Map<String, Object>> shopsStorePos(Map<String,Object> map);

    SktShops selectShopsByShopId(int shopId);

    List<Map<String,Object>> appFindShops(Integer userId);

    List<Map<String,Object>> selectShopsAndAccredsByShopId(Map<String, Object> map1);

    int selectCountByShopId(int i);

    Map<String,Object> appFindShopsDetailed(Integer shopId);

	Integer appFindHotNum(Integer shopId);

	Integer appFindNewNum(Integer shopId);
	
	public List<Map<String, Object>> shopsStorePosL(@Param(value="lng")BigDecimal lng,
            @Param(value="lat")BigDecimal lat);

    List<Map<String,Object>> appFindCheckShops(Integer userId);
    /*
     * 根据articleId查询内容
     * */
	public List<Map<String,Object>> selectArticle(Integer articleId);

    Map<String,Object> selectUsersByShopId(Integer shopId);

    List<Map<String,Object>> findCheckOrders(@Param(value="shopId")Integer shopId,
											 @Param(value="orderNo")String orderNo,
											 @Param(value="beginTime")String beginTime,
											 @Param(value="endTime")String endTime);

	List<Map<String,Object>> findRefuseOrders(@Param(value="shopId")Integer shopId,
											  @Param(value="orderNo")String orderNo,
											  @Param(value="beginTime")String beginTime,
											  @Param(value="endTime")String endTime);
}
