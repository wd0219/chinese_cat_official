package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 会员登陆记录表
 * </p>
 *
 * @author ck123
 * @since 2018-04-27
 */
@TableName("skt_log_users_logins")
public class SktLogUsersLogins extends Model<SktLogUsersLogins> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "loginId", type = IdType.AUTO)
    private Integer loginId;
    /**
     * 职员ID
     */
    private Integer userId;
    /**
     * 登陆时间
     */
    private Date loginTime;
    /**
     * 登陆IP
     */
    private String loginIp;
    /**
     * 登录终端 5扫码登录
     */
    private Integer loginTer;
    /**
     * 登录备注信息：默认记载token
     */
    private String loginRemark;
    
    private String loginName;
    private String userPhone;
    private String createTime;
    private String checkTime;

    public String getLoginName() {
		return loginName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public Integer getLoginTer() {
        return loginTer;
    }

    public void setLoginTer(Integer loginTer) {
        this.loginTer = loginTer;
    }

    public String getLoginRemark() {
        return loginRemark;
    }

    public void setLoginRemark(String loginRemark) {
        this.loginRemark = loginRemark;
    }

    @Override
    protected Serializable pkVal() {
        return this.loginId;
    }

	@Override
	public String toString() {
		return "SktLogUsersLogins [loginId=" + loginId + ", userId=" + userId + ", loginTime=" + loginTime
				+ ", loginIp=" + loginIp + ", loginTer=" + loginTer + ", loginRemark=" + loginRemark + ", loginName="
				+ loginName + ", userPhone=" + userPhone + ", createTime=" + createTime + ", checkTime=" + checkTime
				+ "]";
	}

	public SktLogUsersLogins(Integer loginId, Integer userId, Date loginTime, String loginIp, Integer loginTer,
			String loginRemark, String loginName, String userPhone, String createTime, String checkTime) {
		super();
		this.loginId = loginId;
		this.userId = userId;
		this.loginTime = loginTime;
		this.loginIp = loginIp;
		this.loginTer = loginTer;
		this.loginRemark = loginRemark;
		this.loginName = loginName;
		this.userPhone = userPhone;
		this.createTime = createTime;
		this.checkTime = checkTime;
	}

	public SktLogUsersLogins() {
		super();
		// TODO Auto-generated constructor stub
	}
    
}
