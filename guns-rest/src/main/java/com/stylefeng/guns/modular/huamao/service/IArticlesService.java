package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Articles;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 文章记录表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-26
 */
public interface IArticlesService extends IService<Articles> {
	List<Map<String,Object>>selectArticles(Articles articles);
	/**
	 *  公司新闻
	 * @return
	 */
	List<Map<String, Object>> articleCompanyNew();
	/**
	 * 公司新闻id查询
	 * @param articleId
	 * @return
	 */
	public Map<String, Object> companyNewById(Integer articleId);
}
