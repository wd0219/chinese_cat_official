package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class UsersUpgradeDTO {
	/**
     * 自增ID
     */
    private Integer orderId;
    /**
     * 订单编号
     */
    private String orderNo;
	/**
	 * 用户昵称
	 */
	private String loginName;
	/**
	 * 用户姓名
	 */
	private String trueName;
	/**
	 * 用户电话
	 */
	private String userPhone;
    /**
     * 用户Id
     */
    private Integer userId;
    /**
     * 升级前角色 0普通 1主管
     */
    private Integer preRole;
    /**
     * 升级后角色 1主管 2经理
     */
    private Integer afterRole;
    /**
     * 支付方式支付方式 1:线下现金支付 2 现金账户支付 3华宝支付 4第三方支付 5混合支付 6华宝营业额
     */
    private Integer payType;
    /**
     * 订单总金额 (realMoney+cash+kaiyuan)
     */
    private BigDecimal totalMoney;
    /**
     * 实际支付金额 通过支付公司现金支付的金额
     */
    private BigDecimal realMoney;
    /**
     * 下单时间
     */
    private Date createTime;
    /**
     * 升级时间 成为主管或经理的时间
     */
    private Date upgradeTime;
    /**
     * 有效标志 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 状态 0未支付 1已付款未处理 2已取消 3已处理 9处理中
     */
    private Integer status;
    
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getPreRole() {
		return preRole;
	}
	public void setPreRole(Integer preRole) {
		this.preRole = preRole;
	}
	public Integer getAfterRole() {
		return afterRole;
	}
	public void setAfterRole(Integer afterRole) {
		this.afterRole = afterRole;
	}
	public Integer getPayType() {
		return payType;
	}
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}
	public BigDecimal getRealMoney() {
		return realMoney;
	}
	public void setRealMoney(BigDecimal realMoney) {
		this.realMoney = realMoney;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpgradeTime() {
		return upgradeTime;
	}
	public void setUpgradeTime(Date upgradeTime) {
		this.upgradeTime = upgradeTime;
	}
	public Integer getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
    
}
