package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
@Controller
@CrossOrigin
@RequestMapping("/app")
public class AppTopShopController extends BaseController {

    @Autowired
    private ISktExpressService sktExpressService;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private ISktOrdersService sktOrdersService;
    @Autowired
    private ISktOrderGoodsService sktOrderGoodsService;
    @Autowired
    private IGoodsAppraisesService goodsAppraisesService;
    @Autowired
    private ISktOrderRefundsService sktOrderRefundsService;
    @Autowired
    private ISktShopsService sktShopsService;

    /**
     *全部订单（商家）
     * @param pageIndex
     * @return
     */
    @RequestMapping("/shops/allOrder")
    @ResponseBody
    public AppResult topOrders(@ModelAttribute("pageIndex")Integer pageIndex){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        PageHelper.startPage(pageIndex,5);
        List<Map<String,Object>> list = sktOrdersService.appFindAllOrders(sktShops1.getShopId(),null);
        JSONObject json=new JSONObject();
        json.put("maps", list);
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            json.put("tatolPage",pageInfo.getPages());
        }else{
            json.put("tatolPage",1);
        }
        return AppResult.OK(json);
    }

    /**
     * 商家订单(确认发货获取快递列表)
     * @return
     */
    @RequestMapping("/shops/express")
    @ResponseBody
    public AppResult express(){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        List<SktExpress> list = sktExpressService.appFindExpress();
        return AppResult.OK("查询成功！",list);
    }

    /**
     * 待付款(商家)
     * @param pageIndex
     * @return
     */
    @RequestMapping("/shops/pendingPayment")
    @ResponseBody
    public AppResult pendingPayment(@ModelAttribute("pageIndex")Integer pageIndex){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        PageHelper.startPage(pageIndex,5);
        List<Map<String,Object>> list = sktOrdersService.appFindAllOrders(shopId,"-2");
        JSONObject json=new JSONObject();
        json.put("maps", list);
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            json.put("tatolPage",pageInfo.getPages());
        }else{
            json.put("tatolPage",1);
        }
        return AppResult.OK(json);
//        if(list != null && list.size() > 0){
//            return AppResult.OK("全部订单！",list);
//        }
//        return AppResult.ERROR("商家无此订单！");
    }

    /**
     * 待发货（商家）
     * @param pageIndex
     * @return
     */
    @RequestMapping("/shops/toBeShipped")
    @ResponseBody
    public AppResult toBeShipped(@ModelAttribute("pageIndex")Integer pageIndex){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        PageHelper.startPage(pageIndex,5);
        List<Map<String,Object>> list = sktOrdersService.appFindAllOrders(shopId,"0");
        JSONObject json=new JSONObject();
        json.put("maps", list);
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            json.put("tatolPage",pageInfo.getPages());
        }else{
            json.put("tatolPage",1);
        }
        return AppResult.OK(json);
//        if(list != null && list.size() > 0){
//            return AppResult.OK("全部订单！",list);
//        }
//        return AppResult.ERROR("商家无此订单！");
    }

    /**
     * 发货（商家）
     * @param expressId
     * @param expressNo
     * @param orderNo
     * @return
     */
    @RequestMapping("/shops/confirmDelivery")
    @ResponseBody
    public AppResult confirmDelivery(@ModelAttribute("expressId")Integer expressId,@ModelAttribute("expressNo")String expressNo,@ModelAttribute("orderNo")String orderNo){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("expressId",expressId);
        map.put("expressNo",expressNo);
        map.put("orderNo",orderNo);
        map.put("orderStatus",1);
        map.put("deliveryTime",new Date());
        Integer update = sktOrdersService.appUpdateStatus(map);
        if(update != 0){
            return AppResult.OK("确认成功！");
        }
        return AppResult.ERROR("确认失败！");
    }

    /**
     * 待收货
     * @param pageIndex
     * @return
     */
    @RequestMapping("/shops/receiptOfGoods")
    @ResponseBody
    public AppResult receiptOfGoods(@ModelAttribute("pageIndex")Integer pageIndex){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        PageHelper.startPage(pageIndex,5);
        List<Map<String,Object>> list = sktOrdersService.appFindAllOrders(shopId,"1");
        JSONObject json=new JSONObject();
        json.put("maps", list);
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            json.put("tatolPage",pageInfo.getPages());
        }else{
            json.put("tatolPage",1);
        }
        return AppResult.OK(json);
//        if(list != null && list.size() > 0){
//            return AppResult.OK("全部订单！",list);
//        }
//        return AppResult.ERROR("商家无此订单！");
    }

    /**
     * 待评价
     * @param pageIndex
     * @return
     */
    @RequestMapping("/shops/toBeEvaluated")
    @ResponseBody
    public AppResult toBeEvaluated(@ModelAttribute("pageIndex")Integer pageIndex){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        Map<String,Object> map = new HashMap<String,Object>();
        //订单状态 2用户确认 3系统确认
        map.put("orderStatus",2);
        map.put("orderStatus2",3);
        // 0未评价 1评价
        map.put("isAppraise",0);
        //0未支付  1支付
        map.put("isPay",1);
        map.put("shopId",shopId);
        PageHelper.startPage(pageIndex,5);
        List<Map<String,Object>> list = sktOrdersService.appFindToBeEvaluatedOrders(map);
        JSONObject json=new JSONObject();
        json.put("maps", list);
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            json.put("tatolPage",pageInfo.getPages());
        }else{
            json.put("tatolPage",1);
        }
        return AppResult.OK(json);
//        if(list != null && list.size() > 0){
//            return AppResult.OK("全部订单！",list);
//        }
//        return AppResult.ERROR("商家无此订单！");
    }

    /**
     * 获取订单信息评论页信息(商家)
     * @param orderId
     * @return
     */
    @RequestMapping("/Martgoods/orderMes")
    @ResponseBody
    public AppResult orderMes(@ModelAttribute("orderId")Integer orderId){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
//        Integer userId = user.getUserId();
//        SktShops sktShops=new SktShops();
//        sktShops.setUserId(userId);
//        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
//        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
//        if(sktShops1==null){
//            return   AppResult.ERROR("对不起，您还不是商家");
//        }
     //   Integer shopId = sktShops1.getShopId();
        List<Map<String,Object>> list = sktOrderGoodsService.appFindOrderMesById(orderId);
        JSONObject json=new JSONObject();
        json.put("maps", list);
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            json.put("tatolPage",pageInfo.getPages());
        }else{
            json.put("tatolPage",1);
        }
        return AppResult.OK(json);
//        if (list != null && list.size() >0){
//            return AppResult.OK("查询成功！",list);
//        }
//        return AppResult.ERROR("此商品无评价！");
    }

    /**
     *商家回复评价
     * @param content 回复内容
     * @param goodsId 回复商品ID
     * @param orderId 订单ID
     * @return
     */
    @RequestMapping("/comment/replyComments")
    @ResponseBody
    public AppResult replyComments(@ModelAttribute("content")String content,@ModelAttribute("goodsId")Integer goodsId,@ModelAttribute("orderId")Integer orderId){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        Integer update = goodsAppraisesService.appReplyComments(content, goodsId, orderId);
        if(update != 0){
            return AppResult.OK("回复成功！");
        }
        return AppResult.ERROR("回复失败！");
    }

    /**
     * 待退款（商家）
     * @param pageIndex
     * @return
     */
    @RequestMapping("/shops/refundOrder")
    @ResponseBody
    public AppResult refundOrder(@ModelAttribute("pageIndex")Integer pageIndex){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        PageHelper.startPage(pageIndex,5);
        List<Map<String,Object>> list = sktOrdersService.appFindrefundOrder(shopId,0);
        JSONObject json=new JSONObject();
        json.put("maps", list);
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            json.put("tatolPage",pageInfo.getPages());
        }else{
            json.put("tatolPage",1);
        }
        return AppResult.OK(json);
//        if(list != null && list.size() > 0){
//            return AppResult.OK("全部订单！",list);
//        }
//        return AppResult.ERROR("商家无订单！");
    }

    /**
     * 退款、拒绝退款（商家）
     * @param remark
     * @param type
     * @param orderNo
     * @return
     */
    @RequestMapping("/shops/shopRefund")
    @ResponseBody
    public AppResult shopRefund(@ModelAttribute("remark")String remark,@ModelAttribute("type")Integer type,@ModelAttribute("orderNo")String orderNo){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        Integer update = sktOrderRefundsService.appShopRefund(remark, type, orderNo);
        if(update != 0){
            return AppResult.OK("操作成功！");
        }
        return AppResult.ERROR("操作失败！");
    }

    /**
     * 订单详情
     * @param orderNo
     * @return
     */
    @RequestMapping("/shops/detaiOrder")
    @ResponseBody
    public AppResult detaiOrder(@ModelAttribute("orderNo")String orderNo){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        Map<String,Object> map = sktOrdersService.appFindOrdersDetailed(orderNo);
        if(map != null && map.size() > 0){
            return AppResult.OK("订单详情",map);
        }
        return AppResult.ERROR("出错啦！");
    }
}
