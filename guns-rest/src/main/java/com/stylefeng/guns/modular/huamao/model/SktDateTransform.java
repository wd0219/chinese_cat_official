package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * <p>
 * 积分转换开元宝每日记录表
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
@TableName("skt_date_transform")
public class SktDateTransform extends Model<SktDateTransform> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 转换对象：1是消费者 2是代理公司
     */
    private Integer type;
    /**
     * 日期
     */
    private Integer date;
    /**
     * 比例（万分单位）
     */
    private BigDecimal ratio;
    /**
     * 转换的积分总数
     */
    private BigDecimal transformNum;
    /**
     * 转换的华宝总数
     */
    private BigDecimal kaiyuan;
    /**
     * 转换的总人数：默认-1 未执行是，其它值是实际执行人数
     */
    private Integer userNum;
    /**
     * 操作员工 0系统
     */
    private Integer staffId;
    /**
     * 创建时间
     */
    private String createTime;
    private Date handleStart;
    private Date handleEnd;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public BigDecimal getTransformNum() {
        return transformNum;
    }

    public void setTransformNum(BigDecimal transformNum) {
        this.transformNum = transformNum;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public Integer getUserNum() {
        return userNum;
    }

    public void setUserNum(Integer userNum) {
        this.userNum = userNum;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Date getHandleStart() {
        return handleStart;
    }

    public void setHandleStart(Date handleStart) {
        this.handleStart = handleStart;
    }

    public Date getHandleEnd() {
        return handleEnd;
    }

    public void setHandleEnd(Date handleEnd) {
        this.handleEnd = handleEnd;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktDateTransform{" +
        "id=" + id +
        ", type=" + type +
        ", date=" + date +
        ", ratio=" + ratio +
        ", transformNum=" + transformNum +
        ", kaiyuan=" + kaiyuan +
        ", userNum=" + userNum +
        ", staffId=" + staffId +
        ", createTime=" + createTime +
        ", handleStart=" + handleStart +
        ", handleEnd=" + handleEnd +
        "}";
    }

	public SktDateTransform(Integer id, Integer type, Integer date, BigDecimal ratio, BigDecimal transformNum,
			BigDecimal kaiyuan, Integer userNum, Integer staffId, String createTime, Date handleStart, Date handleEnd) {
		super();
		this.id = id;
		this.type = type;
		this.date = date;
		this.ratio = ratio;
		this.transformNum = transformNum;
		this.kaiyuan = kaiyuan;
		this.userNum = userNum;
		this.staffId = staffId;
		this.createTime = createTime;
		this.handleStart = handleStart;
		this.handleEnd = handleEnd;
	}

	public SktDateTransform() {
		super();
		// TODO Auto-generated constructor stub
	}
    
}
