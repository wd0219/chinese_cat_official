package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 快递表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-24
 */
public interface SktExpressMapper extends BaseMapper<SktExpress> {
    /**
     * 查询所有快递
     * @return
     */
    public List<Map<String,Object>> selectExpressAll();

    SktExpress selectExpressById(Integer expressId);
}
