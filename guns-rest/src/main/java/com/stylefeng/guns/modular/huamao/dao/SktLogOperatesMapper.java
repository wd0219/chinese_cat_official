package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktLogOperates;
import com.stylefeng.guns.modular.huamao.model.SktLogOperatesDto;

/**
 * <p>
 * 操作记录表 Mapper 接口
 * </p>
 *
 * @author slt123
 * @since 2018-04-27
 */
public interface SktLogOperatesMapper extends BaseMapper<SktLogOperates> {
	/**
	 * 按时间查询列表
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public List<SktLogOperatesDto> selectListDate(SktLogOperatesDto sktLogOperatesDto);

}
