package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktStyles;


/**
 * <p>
 * 风格表 Mapper 接口
 * </p>
 *
 * @author liuduan
 * @since 2018-06-16
 */
public interface SktStylesMapper extends BaseMapper<SktStyles> {

}
