package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

public class SktStyles extends Model<SktStyles> {

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 系统类型
     */
    private String styleSys;
    /**
     * 风格名称
     */
    private String styleName;
    /**
     * 风格开发者
     */
    private String styleAuthor;
    /**
     * 风格开发者站点
     */
    private String styleShopSite;
    /**
     * 店铺风格id
     */
    private Integer styleShopId;
    /**
     * 风格目录名称
     */
    private String stylePath;
    /**
     * 是否使用中：1使用中2未使用
     */
    private Integer isUse;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStyleSys() {
        return styleSys;
    }

    public void setStyleSys(String styleSys) {
        this.styleSys = styleSys;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    public String getStyleAuthor() {
        return styleAuthor;
    }

    public void setStyleAuthor(String styleAuthor) {
        this.styleAuthor = styleAuthor;
    }

    public String getStyleShopSite() {
        return styleShopSite;
    }

    public void setStyleShopSite(String styleShopSite) {
        this.styleShopSite = styleShopSite;
    }

    public Integer getStyleShopId() {
        return styleShopId;
    }

    public void setStyleShopId(Integer styleShopId) {
        this.styleShopId = styleShopId;
    }

    public String getStylePath() {
        return stylePath;
    }

    public void setStylePath(String stylePath) {
        this.stylePath = stylePath;
    }

    public Integer getIsUse() {
        return isUse;
    }

    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SktStyles{" +
                "id=" + id +
                ", styleSys='" + styleSys + '\'' +
                ", styleName='" + styleName + '\'' +
                ", styleAuthor='" + styleAuthor + '\'' +
                ", styleShopSite='" + styleShopSite + '\'' +
                ", styleShopId=" + styleShopId +
                ", stylePath='" + stylePath + '\'' +
                ", isUse=" + isUse +
                '}';
    }
}
