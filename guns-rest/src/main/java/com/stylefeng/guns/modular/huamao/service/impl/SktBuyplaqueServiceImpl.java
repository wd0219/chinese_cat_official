package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.DateUtil;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.modular.huamao.dao.AccountMapper;
import com.stylefeng.guns.modular.huamao.dao.SktBuyplaqueMapper;
import com.stylefeng.guns.modular.huamao.dao.SktSysConfigsMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IAccountShopService;
import com.stylefeng.guns.modular.huamao.service.ISktBuyplaqueService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;
import com.stylefeng.guns.modular.huamao.util.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 购买牌匾 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
@Service
public class SktBuyplaqueServiceImpl extends ServiceImpl<SktBuyplaqueMapper, SktBuyplaque> implements ISktBuyplaqueService {
	@Autowired
	private SktBuyplaqueMapper buyplaqueMapper;
	@Autowired
	private IAccountService accountService;

	@Autowired
	private IAccountShopService accountShopService;
	@Autowired
	private SktSysConfigsMapper sktSysConfigsMapper;
	@Autowired
	private UsersMapper usersMapper;

    @Autowired
    private IUsersService usersService;
	@Autowired
	private IAccountService  acountService;
	@Autowired
	private AccountMapper am;
	@Override
	public List<SktBuyplaque> sktBuyplaquefindall(SktBuyplaque buyplaque) {
		return	buyplaqueMapper.sktBuyplaquefindall(buyplaque);
	}
	@Override
	public void sktBuyplaqueUpdate(SktBuyplaque buyplaque) {
		buyplaqueMapper.sktBuyplaqueUpdate(buyplaque);
	}
	@Override
	public void sktBuyplaqueUpdate2(SktBuyplaque buyplaque) {
		buyplaqueMapper.sktBuyplaqueUpdate2(buyplaque);
		
	}

	/**
	 * 用户点击购买牌匾
	 * @param userId
	 * @return
	 */
	@Override
	public JSONObject clickBuyplaque(Integer userId,BigDecimal totalMoney){
		JSONObject json = new JSONObject();
		//查询该用户是否有购买牌匾还没有处理的
		SktBuyplaque sktBuyplaque1=new SktBuyplaque();
		sktBuyplaque1.setUserId(userId);
		sktBuyplaque1.setStatus(1);
		EntityWrapper<SktBuyplaque> wrapper =new EntityWrapper<SktBuyplaque>(sktBuyplaque1);
		SktBuyplaque sktBuyplaque2 = this.selectOne(wrapper);
		if (sktBuyplaque2!=null){
			json.put("code","00");
			json.put("msg","您有购买牌匾的订单等待处理");
			return json;
		}
		SktSysConfigs taxRatio = sktSysConfigsMapper.selectSysConfigsByFieldCode("taxRatio");//综合服务费%
		String  taxRatios=taxRatio.getFieldValue();//综合服务费用
		BigDecimal ratio=new BigDecimal(taxRatios);
		SktSysConfigs kaiyuanFee = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanFee");//华宝支付手续费%
		String  huabaoFee=kaiyuanFee.getFieldValue();//华宝支付费用
		BigDecimal hb=new BigDecimal(huabaoFee);
		BigDecimal hbFee=hb.add(ratio);
		json.put("hbFee",hbFee);//华宝支付所需总费用
		SktSysConfigs kaiyuanturnFee = sktSysConfigsMapper.selectSysConfigsByFieldCode("kaiyuanturnFee");//华宝货款支付手续费%
		String  huabaoturnFee=kaiyuanturnFee.getFieldValue();//华宝货款支付费用
		json.put("huabaoturnFee",huabaoturnFee);//华宝货款支付所需总费用
		Account account=new Account();
		account.setUserId(userId);
		EntityWrapper<Account> ew =new EntityWrapper<Account>(account);
		Account account1 = accountService.selectOne(ew);
		json.put("cash",account1.getCash());
		json.put("kaiyuan",account1.getKaiyuan());
		AccountShop accountShop=new AccountShop();
		accountShop.setUserId(userId);
		EntityWrapper<AccountShop> ewp =new EntityWrapper<AccountShop>(accountShop);
		AccountShop accountShop1 = accountShopService.selectOne(ewp);
		if (accountShop1!=null){
			json.put("kaiyuanTurnover",accountShop1.getKaiyuanTurnover());
		}
		SktBuyplaque sktBuyplaque=new SktBuyplaque();
		sktBuyplaque.setOrderNo(StringUtils.getOrderIdByTime("d"));
		sktBuyplaque.setCreateTime(DateUtil.getTime());
		sktBuyplaque.setUserId(userId);
		sktBuyplaque.setPayType(4);
		sktBuyplaque.setTotalMoney(totalMoney);
		sktBuyplaque.setCash(BigDecimal.ZERO);
		sktBuyplaque.setKaiyuan(BigDecimal.ZERO);
		sktBuyplaque.setKaiyuanFee(BigDecimal.ZERO);
		sktBuyplaque.setRealMoney(BigDecimal.ZERO);
		sktBuyplaque.setOrderRemarks("购买牌匾");
		sktBuyplaque.setDataFlag(1);
		sktBuyplaque.setStatus(0);
		Integer insert = buyplaqueMapper.insert(sktBuyplaque);
		if (insert==1){
			json.put("allowPayType","2,3,4,6");
			json.put("code","01");
			json.put("orderId",sktBuyplaque.getOrderId());

			json.put("msg","申请购买牌匾成功");
		}else {
			json.put("code","00");
			json.put("msg","申请购买牌匾失败");
		}
		return json;
	}

	/**
	 * 用户购买牌匾输入密码支付
	 * @param sktBuyplaque
	 * @param payPwd
	 * @return
	 */
	@Override
	@Transactional

	public JSONObject enterBuyplaque(SktBuyplaque sktBuyplaque,String payPwd){
		JSONObject json = new JSONObject();
        Integer userId=sktBuyplaque.getUserId();

        JSONObject jsonObject = usersService.checkPayPassWorld(userId, payPwd);
        if ("00".equals(jsonObject)){
            return jsonObject;
        }else{
            try{
				sktBuyplaque.setStatus(1);
                Integer integer = buyplaqueMapper.updateById(sktBuyplaque);
				JSONObject jsonBuyplaque = acountService.buyplaque(sktBuyplaque);
				if ("00".equals(jsonBuyplaque.get("code").toString())){
					json.put("code","00");
					json.put("msg",jsonBuyplaque.get("msg").toString());
					return  json;
				}
				json.put("code","01");
                json.put("msg","下单成功");
            }catch (Exception e){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                json.put("code","00");
                json.put("msg","下单失败");
            }
            return  json;
        }
	}
	/**
	 * 购买牌匾
	 * @param sktBuyplaque
	 * @return
	 */
	@Override
	public Integer insertBuyplaque(SktBuyplaque sktBuyplaque) {
		sktBuyplaque.setOrderNo(StringUtils.getOrderIdByTime("d"));
		sktBuyplaque.setCreateTime(DateUtil.getTime());
		Integer insert = buyplaqueMapper.insert(sktBuyplaque);
		return insert;
	}
	
}
