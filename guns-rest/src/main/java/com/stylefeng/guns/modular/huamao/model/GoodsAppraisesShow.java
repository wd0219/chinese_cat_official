package com.stylefeng.guns.modular.huamao.model;

public class GoodsAppraisesShow {
	private String loginName;
	private String userPhoto;
	private String id;
	private String goodsScore;
	private String content;
	private String images;
	private String shopReply;
	private String replyTime;
	private String createTime;
	private String shopId;
	private String shopName;
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getUserPhoto() {
		return userPhoto;
	}
	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGoodsScore() {
		return goodsScore;
	}
	public void setGoodsScore(String goodsScore) {
		this.goodsScore = goodsScore;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}
	public String getShopReply() {
		return shopReply;
	}
	public void setShopReply(String shopReply) {
		this.shopReply = shopReply;
	}
	public String getReplyTime() {
		return replyTime;
	}
	public void setReplyTime(String replyTime) {
		this.replyTime = replyTime;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
}
