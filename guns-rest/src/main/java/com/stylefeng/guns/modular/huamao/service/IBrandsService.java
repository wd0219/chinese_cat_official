package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.Brands;
import com.stylefeng.guns.modular.huamao.model.BrandsCustem;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 品牌表 服务类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
public interface IBrandsService extends IService<Brands> {
	List<Map<String,Object>> selectAll(BrandsCustem brandsCustem);
	public boolean insertBrands(BrandsCustem brandsCustem);
	Map<String,Object> selectMapById(Integer brandsId);
	 List<Map<String,Object>> selectRecommend(BrandsCustem brandsCustem);
	 List<Map<String,Object>>  selectNotRecommend(BrandsCustem brandsCustem);
	 int selectBrandsTotal();
}
