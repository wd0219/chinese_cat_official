package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersExtensionMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrdersExtension;
import com.stylefeng.guns.modular.huamao.service.IAppOrdersExtensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppOrdersExtensionServiceImpl extends ServiceImpl<SktOrdersExtensionMapper,SktOrdersExtension> implements IAppOrdersExtensionService {
    @Autowired
    private SktOrdersExtensionMapper sktOrdersExtensionMapper;

    @Override
    public void updateOrdersExtensionByOrderId(Map<String, Object> maps) {
        sktOrdersExtensionMapper.updateOrdersExtensionByOrderId(maps);
    }
}
