package com.stylefeng.guns.modular.huamao.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 购买牌匾 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
public interface ISktBuyplaqueService extends IService<SktBuyplaque> {
	
	/*
	 * 展示
	 * */
	public List<SktBuyplaque> sktBuyplaquefindall(SktBuyplaque buyplaque);
	/*
	 * 修改 
	 * */
	public void sktBuyplaqueUpdate(SktBuyplaque buyplaque);
	
	public void sktBuyplaqueUpdate2(SktBuyplaque buyplaque);
	
	public Integer insertBuyplaque(SktBuyplaque sktBuyplaque);

	public JSONObject clickBuyplaque(Integer userId,BigDecimal totalMoney);
	public JSONObject enterBuyplaque(SktBuyplaque sktBuyplaque,String payPwd);
	
}
