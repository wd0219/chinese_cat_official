package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktExchangeUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author caody
 * @since 2018-06-20
 */
public interface ISktExchangeUserService extends IService<SktExchangeUser> {

}
