package com.stylefeng.guns.modular.huamao.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 
 * @author slt
 *
 */
public class UsersDTO {
	
	private String payPwd;
	
	private Integer userId;
	/**
	 * 登录名 昵称
	 */
	private String loginName;
	/**
	 * 用户类型 0普通 1主管 2经理
	 */
	private Integer userType;
	/**
	 * 真实姓名
	 */
	private String trueName;
	/**
	 * 推荐人id
	 */
	private Integer inviteId;
	/**
	 * 推荐人名称
	 */
	private String tuiName;
	/**
	 * 推荐人电话
	 */
	private String tuiPhone;
	/**
	 * 手机
	 */
	private String userPhone;

	/**
	 * 线下商家标志 1是 0否
	 */
	private Integer isStore;
	/**
	 * 线上商家标志 1是 0否
	 */
	private Integer isShop;
	/**
	 * 代理公司股东标志:0不是股东 1董事长 2总裁 3行政总监 4财务总监 5部门经理 6监事 7普通股东
	 */
	private Integer isAgent;
	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date createTime;
	/**
	 * 账号状态：0停用 1启用
	 */
	private Integer userStatus;
	/**
	 * 特权账号 0不是 1是
	 */
	private Integer isPrivilege;
	/**
	 * 会员头像
	 */
	private String userPhoto;
	/**
	 * 用户密码
	 */
	private String loginPwd;
	
	
	public String getPayPwd() {
		return payPwd;
	}

	public void setPayPwd(String payPwd) {
		this.payPwd = payPwd;
	}
	/**
	 * 新密码
	 */
	private String newLoginPwd;
	
	public String getLoginPwd() {
		return loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getTuiName() {
		return tuiName;
	}

	public String getUserPhoto() {
		return userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	public void setTuiName(String tuiName) {
		this.tuiName = tuiName;
	}

	public String getTuiPhone() {
		return tuiPhone;
	}

	public void setTuiPhone(String tuiPhone) {
		this.tuiPhone = tuiPhone;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getIsStore() {
		return isStore;
	}

	public void setIsStore(Integer isStore) {
		this.isStore = isStore;
	}

	public Integer getIsShop() {
		return isShop;
	}

	public void setIsShop(Integer isShop) {
		this.isShop = isShop;
	}

	public Integer getIsAgent() {
		return isAgent;
	}

	public void setIsAgent(Integer isAgent) {
		this.isAgent = isAgent;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	public Integer getIsPrivilege() {
		return isPrivilege;
	}

	public void setIsPrivilege(Integer isPrivilege) {
		this.isPrivilege = isPrivilege;
	}

	public Integer getInviteId() {
		return inviteId;
	}

	public void setInviteId(Integer inviteId) {
		this.inviteId = inviteId;
	}

	public String getNewLoginPwd() {
		return newLoginPwd;
	}

	public void setNewLoginPwd(String newLoginPwd) {
		this.newLoginPwd = newLoginPwd;
	}
	
}
