package com.stylefeng.guns.modular.huamao.model;

public class SktOrderGoodsList extends SktOrderGoods{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shopName;
	private Integer freight;
	private Integer shopId;

	public Integer getFreight() {
		return freight;
	}

	public void setFreight(Integer freight) {
		this.freight = freight;
	}

	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
}
