package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.SktComment;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 评论表(预留表) Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-08-21
 */
public interface SktCommentMapper extends BaseMapper<SktComment> {

    List<Map<String,Object>> selectListByClassId(Map<String, Object> map);

    List<Map<String,Object>> selectClassByComment(Map<String, Object> map);
}
