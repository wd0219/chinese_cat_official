package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktFavorites;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 关注(商品/店铺)表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-05-10
 */
public interface SktFavoritesMapper extends BaseMapper<SktFavorites> {

	List<Map<String, Object>> selectShopGoods(SktFavorites sktFavorites);

    List<Map<String,Object>> selectFavoritesByUserId(Map<String, Object> map);

    List<Map<String,Object>> selectFavoritesAndShopsByUserId(Map<String, Object> map);

    int selectCountByUserId(SktFavorites sktFavorites);

    int insertAndFindId(SktFavorites sktFavorites);

    SktFavorites selectListByEntity(SktFavorites sktFavorites);

    int deleteByUser(SktFavorites sktFavorites);
}
