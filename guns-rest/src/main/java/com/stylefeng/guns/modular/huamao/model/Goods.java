package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_goods")
public class Goods extends Model<Goods> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "goodsId", type = IdType.AUTO)
    private Integer goodsId;
    /**
     * 商品编号
     */
    private String goodsSn;
    /**
     * 商品货号
     */
    private String productNo;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 积分赠送比例
     */
    private Integer scoreRatio;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 门店价
     */
    private BigDecimal shopPrice;
    /**
     * 自营店铺的商品支付的积分
     */
    private Integer payScore;
    /**
     * 预警库存
     */
    private Integer warnStock;
    /**
     * 商品总库存
     */
    private Integer goodsStock;
    /**
     * 单位
     */
    private String goodsUnit;
    /**
     * 促销信息
     */
    private String goodsTips;
    /**
     * 是否上架 0：不上架 1：上架
     */
    private Integer isSale;
    /**
     * 是否精品 0：否 1：是
     */
    private Integer isBest;
    /**
     * 是否热销产品 0：否 1：是
     */
    private Integer isHot;
    /**
     * 是否新品 0：否 1：是
     */
    private Integer isNew;
    /**
     * 是否推荐 0：否 1：是
     */
    private Integer isRecom;
    /**
     * 商品分类ID路径
     */
    private String goodsCatIdPath;
    /**
     * 最后一级商品分类ID
     */
    private Integer goodsCatId;
    /**
     * 门店商品分类第一级ID
     */
    private Integer shopCatId1;
    /**
     * 门店商品第二级分类ID
     */
    private Integer shopCatId2;
    /**
     * 品牌ID
     */
    private Integer brandId;
    /**
     * 商品描述
     */
    private String goodsDesc;
    /**
     * 商品状态 -1：违规 0：未审核 1：已审核
     */
    private Integer goodsStatus;
    /**
     * 总销售量
     */
    private Integer saleNum;
    /**
     * 上架时间
     */
    private Date saleTime;
    /**
     * 访问数
     */
    private Integer visitNum;
    /**
     * 评价数
     */
    private Integer appraiseNum;
    /**
     * 是否有规格 0：没有 1：有
     */
    private Integer isSpec;
    /**
     * 商品相册
     */
    private String gallery;
    /**
     * 商品SEO关键字
     */
    private String goodsSeoKeywords;
    /**
     * 状态说明(一般用于说明拒绝原因)
     */
    private String illegalRemarks;
    /**
     * 删除标志 -1：删除 1：有效
     */
    private Integer dataFlag;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 商品类型
     */
    private Integer goodsType;
    /**
     * 是否分销商品
     */
    private Integer isDistribut;
    /**
     * 分销佣金
     */
    private Integer commission;
    /**
     * 是否包邮
     */
    private Integer isFreeShipping;
    /**
     * 购买自营店商品所需的华宝或者华宝货款
     */
    private BigDecimal  payKaiyuan;

    /**
     * 购买自营店商品所需的华宝或者华宝货款
     */
    private BigDecimal  payShopping;

    public BigDecimal getPayShopping() {
        return payShopping;
    }

    public void setPayShopping(BigDecimal payShopping) {
        this.payShopping = payShopping;
    }

    public BigDecimal getPayKaiyuan() {
        return payKaiyuan;
    }

    public void setPayKaiyuan(BigDecimal payKaiyuan) {
        this.payKaiyuan = payKaiyuan;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsSn() {
        return goodsSn;
    }

    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }

    public String getProductNo() {
        return productNo;
    }

    public void setProductNo(String productNo) {
        this.productNo = productNo;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getScoreRatio() {
        return scoreRatio;
    }

    public void setScoreRatio(Integer scoreRatio) {
        this.scoreRatio = scoreRatio;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public Integer getPayScore() {
        return payScore;
    }

    public void setPayScore(Integer payScore) {
        this.payScore = payScore;
    }

    public Integer getWarnStock() {
        return warnStock;
    }

    public void setWarnStock(Integer warnStock) {
        this.warnStock = warnStock;
    }

    public Integer getGoodsStock() {
        return goodsStock;
    }

    public void setGoodsStock(Integer goodsStock) {
        this.goodsStock = goodsStock;
    }

    public String getGoodsUnit() {
        return goodsUnit;
    }

    public void setGoodsUnit(String goodsUnit) {
        this.goodsUnit = goodsUnit;
    }

    public String getGoodsTips() {
        return goodsTips;
    }

    public void setGoodsTips(String goodsTips) {
        this.goodsTips = goodsTips;
    }

    public Integer getIsSale() {
        return isSale;
    }

    public void setIsSale(Integer isSale) {
        this.isSale = isSale;
    }

    public Integer getIsBest() {
        return isBest;
    }

    public void setIsBest(Integer isBest) {
        this.isBest = isBest;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }

    public Integer getIsRecom() {
        return isRecom;
    }

    public void setIsRecom(Integer isRecom) {
        this.isRecom = isRecom;
    }

    public String getGoodsCatIdPath() {
        return goodsCatIdPath;
    }

    public void setGoodsCatIdPath(String goodsCatIdPath) {
        this.goodsCatIdPath = goodsCatIdPath;
    }

    public Integer getGoodsCatId() {
        return goodsCatId;
    }

    public void setGoodsCatId(Integer goodsCatId) {
        this.goodsCatId = goodsCatId;
    }

    public Integer getShopCatId1() {
        return shopCatId1;
    }

    public void setShopCatId1(Integer shopCatId1) {
        this.shopCatId1 = shopCatId1;
    }

    public Integer getShopCatId2() {
        return shopCatId2;
    }

    public void setShopCatId2(Integer shopCatId2) {
        this.shopCatId2 = shopCatId2;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public Integer getGoodsStatus() {
        return goodsStatus;
    }

    public void setGoodsStatus(Integer goodsStatus) {
        this.goodsStatus = goodsStatus;
    }

    public Integer getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum;
    }

    public Date getSaleTime() {
        return saleTime;
    }

    public void setSaleTime(Date saleTime) {
        this.saleTime = saleTime;
    }

    public Integer getVisitNum() {
        return visitNum;
    }

    public void setVisitNum(Integer visitNum) {
        this.visitNum = visitNum;
    }

    public Integer getAppraiseNum() {
        return appraiseNum;
    }

    public void setAppraiseNum(Integer appraiseNum) {
        this.appraiseNum = appraiseNum;
    }

    public Integer getIsSpec() {
        return isSpec;
    }

    public void setIsSpec(Integer isSpec) {
        this.isSpec = isSpec;
    }

    public String getGallery() {
        return gallery;
    }

    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    public String getGoodsSeoKeywords() {
        return goodsSeoKeywords;
    }

    public void setGoodsSeoKeywords(String goodsSeoKeywords) {
        this.goodsSeoKeywords = goodsSeoKeywords;
    }

    public String getIllegalRemarks() {
        return illegalRemarks;
    }

    public void setIllegalRemarks(String illegalRemarks) {
        this.illegalRemarks = illegalRemarks;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(Integer goodsType) {
        this.goodsType = goodsType;
    }

    public Integer getIsDistribut() {
        return isDistribut;
    }

    public void setIsDistribut(Integer isDistribut) {
        this.isDistribut = isDistribut;
    }

    public Integer getCommission() {
        return commission;
    }

    public void setCommission(Integer commission) {
        this.commission = commission;
    }

    public Integer getIsFreeShipping() {
        return isFreeShipping;
    }

    public void setIsFreeShipping(Integer isFreeShipping) {
        this.isFreeShipping = isFreeShipping;
    }

    @Override
    protected Serializable pkVal() {
        return this.goodsId;
    }

    @Override
    public String toString() {
        return "Goods{" +
        "goodsId=" + goodsId +
        ", goodsSn=" + goodsSn +
        ", productNo=" + productNo +
        ", goodsName=" + goodsName +
        ", goodsImg=" + goodsImg +
        ", shopId=" + shopId +
        ", scoreRatio=" + scoreRatio +
        ", marketPrice=" + marketPrice +
        ", shopPrice=" + shopPrice +
        ", payScore=" + payScore +
        ", warnStock=" + warnStock +
        ", goodsStock=" + goodsStock +
        ", goodsUnit=" + goodsUnit +
        ", goodsTips=" + goodsTips +
        ", isSale=" + isSale +
        ", isBest=" + isBest +
        ", isHot=" + isHot +
        ", isNew=" + isNew +
        ", isRecom=" + isRecom +
        ", goodsCatIdPath=" + goodsCatIdPath +
        ", goodsCatId=" + goodsCatId +
        ", shopCatId1=" + shopCatId1 +
        ", shopCatId2=" + shopCatId2 +
        ", brandId=" + brandId +
        ", goodsDesc=" + goodsDesc +
        ", goodsStatus=" + goodsStatus +
        ", saleNum=" + saleNum +
        ", saleTime=" + saleTime +
        ", visitNum=" + visitNum +
        ", appraiseNum=" + appraiseNum +
        ", isSpec=" + isSpec +
        ", gallery=" + gallery +
        ", goodsSeoKeywords=" + goodsSeoKeywords +
        ", illegalRemarks=" + illegalRemarks +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        ", goodsType=" + goodsType +
        ", isDistribut=" + isDistribut +
        ", commission=" + commission +
        ", isFreeShipping=" + isFreeShipping +
        "}";
    }
}
