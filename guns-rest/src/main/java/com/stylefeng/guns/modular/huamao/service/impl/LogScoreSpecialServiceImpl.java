package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogScoreSpecialMapper;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecial;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecialList;
import com.stylefeng.guns.modular.huamao.service.ILogScoreSpecialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 待发特别积分流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-17
 */
@Service
public class LogScoreSpecialServiceImpl extends ServiceImpl<LogScoreSpecialMapper, LogScoreSpecial>
		implements ILogScoreSpecialService {

	@Autowired
	private LogScoreSpecialMapper lssm;

	// 待发特别积分流水表展示
	@Override
	public List<LogScoreSpecialList> selectLogScoreSpecialAll(LogScoreSpecialList logScoreSpecialList) {
		return lssm.selectLogScoreSpecialAll(logScoreSpecialList);
	}

    @Override
    public List<Map<String, Object>> selectUserScoreSpecial(LogScoreSpecialList logScoreSpecialList) {
		List<Map<String, Object>> list  = lssm.selectUserScoreSpecial(logScoreSpecialList);
		return list;
    }
}
