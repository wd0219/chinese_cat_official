package com.stylefeng.guns.modular.huamao.util;

public class EnumModel {
    public Integer getArticlesId(String code){
        Integer articlesId=null;
        switch (code){
            case "withdraw":
                articlesId=77;
                break;
            case "share":
                articlesId=76;
                break;
            case "feeDescription":
                articlesId=78;
                break;
            case "serviceAgreement":
                articlesId=0;
                break;
            case "buyScore":
                articlesId=73;
                break;
            case "distributeScore":
                articlesId=74;
                break;
            case "aboutMe":
                articlesId=80;
                break;
            case "helpCenter":
                articlesId=81;
                break;
            case "userUpgrade":
                articlesId=79;
                break;
        }
        return articlesId;
    }
}
