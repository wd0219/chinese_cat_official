package com.stylefeng.guns.modular.huamao.service;

import com.stylefeng.guns.modular.huamao.model.SktCarts;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;
import com.stylefeng.guns.modular.huamao.result.AppResult;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 购物车表 服务类
 * </p>
 *
 * @author caody123
 * @since 2018-05-10
 */
public interface ISktCartsService extends IService<SktCarts> {

	/**  根据用户id查询订单商品表
	 * @param
	 * @return
	 */
	public List<Map<String,Object>> selectCartsByuserId(Map<String,Object> map);
	
	/**  
	 * 购物车结算时查询
	 * @param userId
	 * @return
	 */
	//public List<SktOrderGoods> selectCartsSettlementByuserId(int userId);
	
	/**
	 * 购物车结算时操作
	 * @param userId
	 * @param goodsMoney
	 * @return
	 */
	public JSONObject cartsSettlement(Integer userId, BigDecimal goodsMoney, List<Map<String,Object>> param );

	Map<String,Object> appCarts(Integer userId);

    Map<String,Object> appOrderPay(Integer userId, Integer addressId, Integer isInvoice, String invoiceClient, String orderRemarks);
    
    public Integer appnewAddressBuy(Integer shopId,Integer areaId2);
    
    public Map<String,Object> newAddressPrice(Integer userId,Integer areaId2);

    Map<String,Object> appAccountConfig(Integer userId, String code);

    Map<String,Object> appPayCashKaiyuan(Integer userId,Integer payType, String orderNo, Integer isBatch);

    Map<String,Object> appCheckCarts(Integer userId);

    Map<String,Object> updateCartNum(String cartId, String cartNum);

	Map<String,Object> h5appOrderPay(Integer userId, Integer addressId, Integer isInvoice, String invoiceClient, List<Map> orderRemarks);
	public Map<String, Object> newAppPayCashKaiyuan(Integer userId,Integer payType, String orderNo, Integer isBatch,String thirdNo,Integer thirdType,String payName);

}
