package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktLogSms;

/**
 * <p>
 * 短信发送记录表 服务类
 * </p>
 *
 * @author caody
 * @since 2018-06-08
 */
public interface ISktLogSmsService extends IService<SktLogSms> {
	
}
