package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStockscore;

import java.util.Map;

public interface IAppOrdersStockScoreService extends IService<SktOrdersStockscore> {
    SktOrdersStockscore selectByOredrNo(Map<String, Object> orderWhere);

    void updateOrderStockScoreById(SktOrdersStockscore sktOrdersStockscore2);

    SktOrdersStockscore selectOrdersStockscoreByWhere(SktOrdersStockscore sktOrdersStockscore);
}
