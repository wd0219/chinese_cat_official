package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;

public class AccountShopDTO extends AccountShop {

    /**
     * 华宝
     */
    private BigDecimal kaiyuan;
    /**
     * 现金
     */
    private BigDecimal cash;
    /**
     * 华宝货款
     */
    private BigDecimal kaiyuanTurnover;
    /**
     * 扣除现金
     * @return
     */
    private BigDecimal deductCash;

    /**
     * 扣除华宝
     * @return
     */
    private BigDecimal deductKaiyuan;

    /**
     * 扣除华宝货款
     * @return
     */
    private BigDecimal deductKaiyuanTurnover;

    /**
     * 华宝支付扣除手续费
     */
    private BigDecimal kaiyuanFee;

    /**
     * 华宝支付综合服务费
     */
    private BigDecimal taxRatio;
    /**
     *华宝货款手续费
     */
    private BigDecimal kaiyuanturnFee;

    /**
     * 订单id
     * @return
     */
    private Integer orderId;

    /**
     * 订单编号
     * @return
     */
    private String orderNo;

    /**
     * 如果不在购买时间段内的提示信息
     * @return
     */
    private String message;

    @Override
    public BigDecimal getKaiyuanTurnover() {
        return kaiyuanTurnover;
    }

    @Override
    public void setKaiyuanTurnover(BigDecimal kaiyuanTurnover) {
        this.kaiyuanTurnover = kaiyuanTurnover;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getDeductCash() {
        return deductCash;
    }

    public void setDeductCash(BigDecimal deductCash) {
        this.deductCash = deductCash;
    }

    public BigDecimal getDeductKaiyuan() {
        return deductKaiyuan;
    }

    public void setDeductKaiyuan(BigDecimal deductKaiyuan) {
        this.deductKaiyuan = deductKaiyuan;
    }

    public BigDecimal getDeductKaiyuanTurnover() {
        return deductKaiyuanTurnover;
    }

    public void setDeductKaiyuanTurnover(BigDecimal deductKaiyuanTurnover) {
        this.deductKaiyuanTurnover = deductKaiyuanTurnover;
    }

    public BigDecimal getKaiyuanFee() {
        return kaiyuanFee;
    }

    public void setKaiyuanFee(BigDecimal kaiyuanFee) {
        this.kaiyuanFee = kaiyuanFee;
    }

    public BigDecimal getTaxRatio() {
        return taxRatio;
    }

    public void setTaxRatio(BigDecimal taxRatio) {
        this.taxRatio = taxRatio;
    }

    public BigDecimal getKaiyuanturnFee() {
        return kaiyuanturnFee;
    }

    public void setKaiyuanturnFee(BigDecimal kaiyuanturnFee) {
        this.kaiyuanturnFee = kaiyuanturnFee;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
