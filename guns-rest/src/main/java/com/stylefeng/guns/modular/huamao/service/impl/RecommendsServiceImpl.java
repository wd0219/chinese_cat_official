package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.RecommendsMapper;
import com.stylefeng.guns.modular.huamao.model.CatBrands;
import com.stylefeng.guns.modular.huamao.model.Goods;
import com.stylefeng.guns.modular.huamao.model.Recommends;
import com.stylefeng.guns.modular.huamao.service.IRecommendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 推荐记录表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-24
 */
@Service
public class RecommendsServiceImpl extends ServiceImpl<RecommendsMapper, Recommends> implements IRecommendsService {


	@Override
	public 	Map<String,Object>  selectMax() {
		// TODO Auto-generated method stub
		return baseMapper.selectMax();
	}

	@Override
	public boolean deleteByDataId(Integer dataId) {
		// TODO Auto-generated method stub
		return baseMapper.deleteByDataId(dataId);
	}

	@Override
	public Map<String, Object> selectBrand(Integer dataId) {
		// TODO Auto-generated method stub
		return baseMapper.selectBrand(dataId);
	}

}
