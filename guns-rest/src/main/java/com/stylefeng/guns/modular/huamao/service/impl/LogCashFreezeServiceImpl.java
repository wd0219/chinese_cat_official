package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogCashFreezeMapper;
import com.stylefeng.guns.modular.huamao.model.LogCashFreeze;
import com.stylefeng.guns.modular.huamao.model.LogCashFreezeList;
import com.stylefeng.guns.modular.huamao.service.ILogCashFreezeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 待发现金流水表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-16
 */
@Service
public class LogCashFreezeServiceImpl extends ServiceImpl<LogCashFreezeMapper, LogCashFreeze>
		implements ILogCashFreezeService {

	@Autowired
	private LogCashFreezeMapper lcfm;

	@Override
	public List<LogCashFreezeList> selectLogCashFreezeAll(LogCashFreezeList logCashFreezeList) {
		return lcfm.selectLogCashFreezeAll(logCashFreezeList);
	}

    @Override
    public List<Map<String, Object>> selectUserCaseFreeze(LogCashFreezeList logCashFreezeList) {
		List<Map<String,Object>> list = lcfm.selectUserCaseFreeze(logCashFreezeList);
        return list;
    }
}
