package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.UsersRealnameMapper;
import com.stylefeng.guns.modular.huamao.model.UsersRealname;
import com.stylefeng.guns.modular.huamao.service.IAppUsersRealnameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppUsersRealnameServiceImpl extends ServiceImpl<UsersRealnameMapper,UsersRealname> implements IAppUsersRealnameService {
    @Autowired
    private UsersRealnameMapper usersRealnameMapper;

    @Override
    public UsersRealname selectUsersRealNameByUserId(Integer userId) {
        return usersRealnameMapper.selectUsersRealNameByUserId(userId);
    }

    @Override
    public UsersRealname selectUsersRealNameByCardId(String id) {
        return usersRealnameMapper.selectUsersRealNameByCardId(id);
    }
}
