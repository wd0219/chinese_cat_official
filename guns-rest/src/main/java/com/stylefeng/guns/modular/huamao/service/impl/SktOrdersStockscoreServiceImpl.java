package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.common.GetOrderNum;
import com.stylefeng.guns.modular.huamao.dao.SktOrdersStockscoreMapper;
import com.stylefeng.guns.modular.huamao.dao.SktShopsMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrdersStockscore;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersStockscoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商家购买库存积分订单表 服务实现类
 * </p>
 *
 * @author ck123
 * @since 2018-04-19
 */
@Service
public class SktOrdersStockscoreServiceImpl extends ServiceImpl<SktOrdersStockscoreMapper, SktOrdersStockscore> implements ISktOrdersStockscoreService {
	@Autowired
	private SktOrdersStockscoreMapper orderStockscoreMapper;
	@Autowired
	private SktShopsMapper sktShopsMapper;
	@Override
	public List<SktOrdersStockscore> sktOrdersStockscorefindall(SktOrdersStockscore ordersStockscore) {
		List<SktOrdersStockscore> list = orderStockscoreMapper.sktOrdersStockscorefindall(ordersStockscore);
		return list;
	}

}
