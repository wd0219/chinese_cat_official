package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 评论表(预留表)
 * </p>
 *
 * @author stylefeng123
 * @since 2018-08-21
 */
@TableName("skt_comment")
public class SktComment extends Model<SktComment> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "commentId", type = IdType.AUTO)
    private Integer commentId;
    /**
     * 评论id
     */
    private Integer userId;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 评论时间
     */
    private Date creatTime;
    /**
     * 视频Id
     */
    private Integer classId;


    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    @Override
    protected Serializable pkVal() {
        return this.commentId;
    }

    @Override
    public String toString() {
        return "SktComment{" +
                "commentId=" + commentId +
                ", userId=" + userId +
                ", content=" + content +
                ", creatTime=" + creatTime +
                ", classId=" + classId +
                "}";
    }
}
