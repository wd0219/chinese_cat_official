package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsSpecsMapper;
import com.stylefeng.guns.modular.huamao.model.GoodsSpecs;
import com.stylefeng.guns.modular.huamao.service.IAppGoodsSpecsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppGoodsSpecsServiceImpl extends ServiceImpl<GoodsSpecsMapper,GoodsSpecs> implements IAppGoodsSpecsService {
    @Autowired
    private GoodsSpecsMapper goodsSpecsMapper;

    @Override
    public void updateGoodsSpecsById(Map<String, Object> map) {
        goodsSpecsMapper.updateGoodsSpecsById(map);
    }
}
