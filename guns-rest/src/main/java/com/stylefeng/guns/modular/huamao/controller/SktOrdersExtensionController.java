package com.stylefeng.guns.modular.huamao.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;

import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.SktOrdersExtension;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersExtensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 线上商城订单扩展表  前端控制器
 * </p>
 * @author caody123
 * @since 2018-05-18
 */
@Controller
@RequestMapping("/SktOrdersExtension")
public class SktOrdersExtensionController extends BaseController {

	@Autowired private ISktOrdersExtensionService sktOrdersExtensionService;

	/**
	 * 跳转列表页面
	 * @param model
	 * @return
	 */
	@RequestMapping("/sktOrdersExtensionIndex")
	public String index(Model model) {
        return "sktOrdersExtensionListIndex";
	}

	/**
	 * 跳转添加页面
	 * @param model
	 * @return
	 */
	@RequestMapping("/sktOrdersExtensionAdd")
	public String sktOrdersExtensionAdd(Model model) {
        return "sktOrdersExtensionAdd";
	}

	/**
	 * 跳转修改页面
	 * @param id  实体ID
	 * @return
	 */
	@RequestMapping("/sktOrdersExtensionUpdate/{id}")
	public String sktOrdersExtensionUpdate(@PathVariable Integer id,Model model) {
        model.addAttribute("id", id);
        return "sktOrdersExtensionUpd";
	}

	/**
	 * 分页查询数据
	 * @return
	 */
//	@RequestMapping("/getSktOrdersExtensionPageList")
//	@ResponseBody
//	public PageInfo dataGrid(SktOrdersExtension sktOrdersExtension, Integer page, Integer rows, String sort, String order) {
//        PageInfo pageInfo = new PageInfo(page, rows, sort, order);
//        EntityWrapper<SktOrdersExtension> ew = new EntityWrapper<SktOrdersExtension>(sktOrdersExtension);
//        Page<SktOrdersExtension> pages = getPage(page, rows, sort, order);
//        pages = sktOrdersExtensionService.selectPage(pages, ew);
//        pageInfo.setRows(pages.getRecords());
//        pageInfo.setTotal(pages.getTotal());
//        return pageInfo;
//	}

	/**
	 * 添加
	 * @param
	 * @return
	 */
	@RequestMapping("/add")
    @ResponseBody
	public Object add(SktOrdersExtension sktOrdersExtension, BindingResult result) {
        boolean b = sktOrdersExtensionService.insert(sktOrdersExtension);
        if (b) {
        	return Result.OK("添加成功！");
        } else {
        	return Result.EEROR("添加失败！");
        }
	}


	/**
	 * 根据id删除对象
	 * @param id  实体ID
	 * @return 0 失败  1 成功
	 */
	@RequestMapping("/sysRoleDelete")
    @ResponseBody
	public Object sysRoleDelete(Integer id){
        try {
			sktOrdersExtensionService.deleteById(id);
        	return Result.OK("添加成功！");
        }catch (Exception e){
        	return Result.EEROR("添加失败！");
        }
	}

	/**
	 * 批量删除对象
	 * @param ids 实体集合ID
	 * @return  0 失败  1 成功
	 */
	@RequestMapping("/sysRoleBatchDelete")
    @ResponseBody
	public Object deleteBatchIds(List  ids){
        try {
			sktOrdersExtensionService.deleteBatchIds(ids);
        	return Result.OK("添加成功！");
        }catch (Exception e){
        	return Result.EEROR("添加失败！");
        }
	}

	/**
	 * 编辑
	 * @param 
	 * @return
	 */
	@RequestMapping("/edit")
    @ResponseBody
	public Object edit(SktOrdersExtension sktOrdersExtension) {
		boolean b = sktOrdersExtensionService.updateById(sktOrdersExtension);
		if (b) {
        	return Result.OK("添加成功！");
		} else {
        	return Result.EEROR("添加失败！");
		}
	}
	
}
