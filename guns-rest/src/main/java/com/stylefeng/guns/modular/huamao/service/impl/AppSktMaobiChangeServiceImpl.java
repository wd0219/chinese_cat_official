package com.stylefeng.guns.modular.huamao.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.core.util.MD5Util;
import com.stylefeng.guns.modular.huamao.dao.SktHuamaobtLogMapper;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.*;
import com.stylefeng.guns.modular.huamao.util.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AppSktMaobiChangeServiceImpl extends ServiceImpl<SktHuamaobtLogMapper, SktHuamaobtLog> implements IAppSktMaobiChangeService {


    @Autowired
    ISktExchangeUserService exchangeUserService;

    @Autowired
    IAccountService accountService;

    @Autowired
    ILogKaiyuanService logKaiyuanService;


    @Autowired
    ISktExchangeUserService sktExchangeUserService;

    @Autowired
    ISktHuamaobtLogService sktHuamaobtLogService;

    @Autowired
    IAppLogMaobiService iAppLogMaobiService;

    @Autowired
    IUsersService usersService;

    /**
     * 现金转化猫币
     * @param userId
     * @param amount
     * @param orderNo
     * @param moneyRate
     * @param money
     * @return
     */
    @Override
    @Transactional
    public Map<String, Object> recharge(Integer userId,BigDecimal currentMoney,BigDecimal amount, String orderNo, BigDecimal moneyRate, BigDecimal money,Integer dtype){
        Map<String, Object> maps = new HashMap<>();
        try {
            JSONObject result = this.checkNumber(userId);
            /**
             * 用来接收datas的值
             */
            JSONObject robject = new JSONObject();
            if(!result.getBoolean("isSuc")){
                maps.put("status",-1);
                maps.put("msg","校验失败");
                return maps;
            }else{
                robject  = result.getJSONObject("datas");
                /**
                 * 判断用户是否注册
                 */
                if(robject.getString("userID").equals("0")){
                    maps.put("status",-2);
                    maps.put("msg","用户未注册");
                    return maps;
                }
            }
            String url = "https://api1.digsg.com/assets/rechargehhtb";
            if (dtype==1) {
                url = "https://api1.digsg.com/assets/rechargehhtb";
            }else if (dtype==2){
                url = "https://api1.digsg.com/assets/recharge";
            }

            /**
             * 用户交易所的ID
             */
            //Integer userID = rexchangeUser.getExchangeUserId();
            Integer userID = robject.getInteger("userID");
            String companyID = "1";
            String secretKey = "iL34.#dnZ2ubJJFt";

            /**
             * MD5加密
             */
            String key = Integer.toString(userID)+amount.toString()+companyID+secretKey;
            String md5Info = MD5Util.encrypt(key);

            Map<String,Object> map = new HashMap<String, Object>();
            map.put("userID",userID);
            map.put("amount",amount);
            map.put("companyID",companyID);
            map.put("md5Info",md5Info);

            /**
             *调用用户充值接口
             */
            String json =  HttpUtil.doPost(url,map);
            JSONObject jsonObject = JSONObject.parseObject(json);
            System.out.println(jsonObject.toString()+"-------------------");
            if(jsonObject.getBoolean("isSuc")){
                /**
                 * 增加猫币交易记录
                 */
                LogMaobi logMaobi = new LogMaobi();
                logMaobi.setType(39);
                logMaobi.setUserId(userId);
                logMaobi.setDataId(userID);
                logMaobi.setOrderNo(orderNo);
                logMaobi.setPreCash(currentMoney);
                logMaobi.setMaobi(amount);
                logMaobi.setCashType(-1);
                if (dtype==1) {
                    logMaobi.setRemark("赎回支出HHTB");
                }else {
                    logMaobi.setRemark("赎回支出猫币");
                }
                logMaobi.setDataFlag(1);
                logMaobi.setCreateTime(new Date());
                iAppLogMaobiService.insert(logMaobi);
                /**
                 * 增加交易记录
                 */
                SktHuamaobtLog huamaobtLog  = new SktHuamaobtLog();
                //userId代表用户id，userID代表交易所id
                huamaobtLog.setExchangeUserId(userID);
                huamaobtLog.setAmount(amount);
                huamaobtLog.setCreateTime(new Date());
                huamaobtLog.setOrderNo(orderNo);
                this.insert(huamaobtLog);

                maps.put("status",1);
                maps.put("msg","转化成功");
                return maps;

            }else{
                maps.put("status",-1);
                maps.put("msg",jsonObject.getString("des"));
                return maps;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("转化失败");
        }
    }

    /**
     * 判断用户是否注册交易所
     * @param userId
     * @return
     */
    public JSONObject checkNumber(Integer userId){
        Users users = new Users();
        users = usersService.selectById(userId);

        String url = "https://api1.digsg.com/user/register";
        String companyID = "1";

        String userMobile= users.getUserPhone();
        String secretKey = "iL34.#dnZ2ubJJFt";
        String key = userMobile+companyID+secretKey;
        String md5Info = MD5Util.encrypt(key);

        Map<String,Object> map = new HashMap<String, Object>();
        map.put("companyID",companyID);
        map.put("userMobile",userMobile);
        map.put("md5Info",md5Info);

        String json =  HttpUtil.doPost(url,map);
        return JSONObject.parseObject(json);

    }


    /**
     * 获取交易所实时汇率
     * @return
     */
    @Override
    @Transactional
    public Object ticker(){

        String turl = "https://api1.digsg.com/ticker/ticker?market=maoc_hhtb";
        String json =  HttpUtil.doGet(turl);

        JSONObject  jsonObject  = JSONObject.parseObject(json);
        JSONObject datas = jsonObject.getJSONObject("datas");
        String cnyPrices = datas.getString("cnyPrice");
        double cnyPrice = 1 / Double.valueOf(cnyPrices);
        return cnyPrice;
    }


}
