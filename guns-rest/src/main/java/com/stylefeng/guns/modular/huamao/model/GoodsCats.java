package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 商品分类表
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@TableName("skt_goods_cats")
public class GoodsCats extends Model<GoodsCats> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增Id
     */
    @TableId(value = "catId", type = IdType.AUTO)
    private Integer catId;
    /**
     * 父ID
     */
    private Integer parentId;
    /**
     * 分类名称
     */
    private String catName;
    /**
     * 分类图片
     */
    private String catImg;
    /**
     * 是否显示
     */
    private Integer isShow;
    /**
     * 是否显示楼层
     */
    private Integer isFloor;
    /**
     * 排序号
     */
    private Integer catSort;
    /**
     * 删除标志
     */
    private Integer dataFlag;
    /**
     * 建立时间
     */
    private Date createTime;


    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatImg() {
        return catImg;
    }

    public void setCatImg(String catImg) {
        this.catImg = catImg;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getIsFloor() {
        return isFloor;
    }

    public void setIsFloor(Integer isFloor) {
        this.isFloor = isFloor;
    }

    public Integer getCatSort() {
        return catSort;
    }

    public void setCatSort(Integer catSort) {
        this.catSort = catSort;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.catId;
    }

    @Override
    public String toString() {
        return "GoodsCats{" +
        "catId=" + catId +
        ", parentId=" + parentId +
        ", catName=" + catName +
        ", catImg=" + catImg +
        ", isShow=" + isShow +
        ", isFloor=" + isFloor +
        ", catSort=" + catSort +
        ", dataFlag=" + dataFlag +
        ", createTime=" + createTime +
        "}";
    }
}
