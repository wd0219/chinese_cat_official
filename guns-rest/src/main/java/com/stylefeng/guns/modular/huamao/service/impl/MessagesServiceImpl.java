package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.MessagesMapper;
import com.stylefeng.guns.modular.huamao.model.Messages;
import com.stylefeng.guns.modular.huamao.model.MessagesList;
import com.stylefeng.guns.modular.huamao.service.IMessagesService;
import com.stylefeng.guns.modular.huamao.service.ISktShopsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统消息表 服务实现类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
@Service
public class MessagesServiceImpl extends ServiceImpl<MessagesMapper, Messages> implements IMessagesService {

	@Autowired
	private MessagesMapper mm;
	@Autowired
	private ISktShopsService sktShopsService;
	// 通过传过来的ID进行逻辑删除
	@Override
	public void deleteById2(Integer id) {
		mm.deleteById2(id);
	}

	// 系统消息展示
	@Override
	public List<MessagesList> selectMessagesAll(MessagesList messagesList) {
		return mm.selectMessagesAll(messagesList);
	}

	// 获取用户
	@Override
	public List<MessagesList> getUsers(String userPhone) {
		return mm.getUsers(userPhone);
	}

	/**
	 * 添加信息，传过来的两个值需要在这里判断，如果li为空说明是给所有商家发送，只需要插入一条sql语句，
	 * 不为空，说明是给指定用户发送，需要多条sql语句，开启事务
	 */
	@Override
	@Transactional
	public boolean addMessages(String li, MessagesList messagesList) {
		if (li == null || li == "") {
			mm.addMessages(messagesList);
		} else {
			// 走到这里说明li中有值是给指定用户发送，所以要遍历添加
			String[] split = li.split(",");
			try {
				for (String string : split) {
					// 将遍历出来的id存入messagesList中到数据库循环查询
					messagesList.setReceiveUserId(Integer.parseInt(string));
					mm.addMessages(messagesList);
				}
			} catch (Exception e) {
				// TODO: handle exception
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			}
		}
		return true;
	}
	@Override
	public List<MessagesList> getMessage(Messages messages) {
		List<MessagesList> list = mm.getMessage(messages);
		return list;
	}
	@Override
	public List<MessagesList> getMessage1(Integer receiveUserId) {
		List<MessagesList> list = mm.getMessage1(receiveUserId);
		return list;
	}

	@Override
	public Integer updateMessageStatus(Integer msgStatus, Integer dataFlag, Integer[] ids) {
		 Integer updateMessageStatus = mm.updateMessageStatus(msgStatus, dataFlag, ids);
		return updateMessageStatus;
	}

	@Override
	public Integer getMessageCount(Messages messages) {
		Integer count = mm.getMessageCount(messages);
		return count;
	}

	/**
	 * * 添加信息，传过来一个实体需要判断，如果messagesList.getUsersId()为空说明是给所有商家发送，需要先查出来所有的是商家的userId，
	 * 不为空，说明是给指定用户发送，直接写添加sql
	 *
	 * @param msgType  0手动 1 系统
	 * @param sendUserId 发送者id
	 * @param usersId 接受者id
	 * @param msgContent 消息内容
	 * @param msgJson 存放json数据
	 * @return
	 */

	@Override
	@Transactional
	public boolean addMessages(Integer msgType, Integer sendUserId, List<Integer> usersId, String msgContent, String msgJson) {
		Messages messages = null;
		//new一个list保存对象，用于批量添加
		List<Messages> list = new ArrayList<Messages>();
		Boolean b = false;
		Integer add = 0;
		if (usersId == null || usersId.size() == 0) {
			//给所有商家发消息,先查询所有的商家的userId
			List<Integer> userIds = sktShopsService.selectAllId();
			messages = new Messages();
			// 消息类型 0手动 1系统
			messages.setMsgType(msgType);
			// 写入实体类发送者id
			messages.setSendUserId(sendUserId);
			//消息内容
			messages.setMsgContent(msgContent);
			//存放json数据
			messages.setMsgJson(msgJson);
			// 发送时间为当前是时间
			messages.setCreateTime(new Date());
			mm.shopAddMessages(messages, userIds);
		} else {
			//遍历添加到list中
			for (Integer userId :
					usersId) {
				messages = new Messages();
				// 消息类型 0手动 1系统
				messages.setMsgType(msgType);
				// 写入实体类发送者id
				messages.setSendUserId(sendUserId);
				//接收者id
				messages.setReceiveUserId(userId);
				//消息内容
				messages.setMsgContent(msgContent);
				//存放json数据
				messages.setMsgJson(msgJson);
				// 发送时间为当前是时间
				messages.setCreateTime(new Date());
				list.add(messages);
			}
			//批量添加
			EntityWrapper<Messages> entityWrapper = new EntityWrapper<Messages>(messages);
			b = this.insertBatch(list);
		}
		return b;
	}

}
