package com.stylefeng.guns.modular.huamao.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersDraws;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsList;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 用户提现表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
public interface IUsersDrawsService extends IService<UsersDraws> {

	// 用户提现表展示
	public List<UsersDrawsList> selectUsersDrawsAll(UsersDrawsList usersDrawsList);

	// 修改审核状态
	public void updateSatus(UsersDrawsList usersDrawsList);

	public JSONObject clickRedeem(Integer userId, Integer type);

	public   JSONObject  applyRedeem(Integer userId, Integer type, BigDecimal money, BigDecimal expenses, BigDecimal expensesrate, BigDecimal taxratio, BigDecimal ratio,String payPwd);
	/************app****************/
	public JSONObject cashDraw(Integer userId);
}
