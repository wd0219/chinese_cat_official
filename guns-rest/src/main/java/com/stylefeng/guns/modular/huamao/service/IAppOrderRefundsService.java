package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefunds;

import java.util.Map;

public interface IAppOrderRefundsService extends IService<SktOrderRefunds> {
    SktOrderRefunds selectOrderRefundsByOrderId(Map<String,Object> map);
}
