package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktSalesMonth;

import java.util.List;

/**
 * <p>
 * 商家每月营业额统计表 服务类
 * </p>
 *
 * @author ck123
 * @since 2018-04-22
 */
public interface ISktSalesMonthService extends IService<SktSalesMonth> {
	/*
	 * 展示全部
	 * */
	public List<SktSalesMonth> sktSalesMonthfindAll(SktSalesMonth sktSalesMonth);
}
