package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.AgentsApplys;
import com.stylefeng.guns.modular.huamao.model.AgentsApplysDTO;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IAgentsApplysService;

/**
 * 代理股东申请控制器
 *
 * @author fengshuonan
 * @Date 2018-04-17 16:06:28
 */
@CrossOrigin
@Controller
@RequestMapping("/agentsApplys")
public class AgentsApplysController extends BaseController {

    private String PREFIX = "/huamao/agentsApplys/";

    @Autowired
    private IAgentsApplysService agentsApplysService;

    /**
     * 跳转到代理股东申请首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "agentsApplys.html";
    }

    /**
     * 跳转到添加代理股东申请
     */
    @RequestMapping("/agentsApplys_add")
    public String agentsApplysAdd() {
        return PREFIX + "agentsApplys_add.html";
    }
    
    /**
    * 跳转到通过
    */
   @RequestMapping("/agentsApplys_pass/{shaId}")
   public String agentsApplysPass(@PathVariable(value="shaId") Integer shaId, Model model) {
	   model.addAttribute("shaId",shaId);
       return PREFIX + "agentsApplys_pass.html";
   }
   /**
    * 跳转到刷新
    */
   @RequestMapping("/agentsApplys_refuse/{shaId}")
   public String agentsApplysRefuse(@PathVariable(value="shaId") Integer shaId, Model model) {
	   model.addAttribute("shaId",shaId);
       return PREFIX + "agentsApplys_refuse.html";
   }

    /**
     * 跳转到修改代理股东申请
     */
    @RequestMapping("/agentsApplys_update/{agentsApplysId}")
    public String agentsApplysUpdate(@PathVariable Integer agentsApplysId, Model model) {
        AgentsApplys agentsApplys = agentsApplysService.selectById(agentsApplysId);
        model.addAttribute("item",agentsApplys);
        LogObjectHolder.me().set(agentsApplys);
        return PREFIX + "agentsApplys_edit.html";
    }

    /**
     * 获取代理股东申请列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(AgentsApplysDTO agentsApplysDTO) {
        return agentsApplysService.showDaiLiShenQingInfo(agentsApplysDTO);
    }

    /**
     * 新增代理股东申请
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AgentsApplys agentsApplys) {
        agentsApplysService.insert(agentsApplys);
        return SUCCESS_TIP;
    }

    /**
     * 删除代理股东申请
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer agentsApplysId) {
        agentsApplysService.deleteById(agentsApplysId);
        return SUCCESS_TIP;
    }

    /**
     * 修改代理股东申请
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AgentsApplys agentsApplys) {
        agentsApplysService.updateById(agentsApplys);
        return SUCCESS_TIP;
    }

    /**
     * 代理股东申请详情
     */
    @RequestMapping(value = "/detail/{agentsApplysId}")
    @ResponseBody
    public Object detail(@PathVariable("agentsApplysId") Integer agentsApplysId) {
        return agentsApplysService.selectById(agentsApplysId);
    }
    
    /*********首页***********/
    /**
     * 新增代理股东申请
     */
    @RequestMapping(value = "/addAgentsApplys")
    @ResponseBody
    public Result addAgentsApplys(AgentsApplysDTO agentsApplys) {
    	//新增股东 
        Integer insert = agentsApplysService.insertAgentsApplys(agentsApplys);
        return (insert!=null && insert != 0) ?Result.OK(null):Result.EEROR("新增用户股东失败！");
    }
    
    /**
     * 申请线上商城查询信息
     */
    @RequestMapping(value = "/selectAgentsApplysInfo")
    @ResponseBody
    public Result selectAgentsApplysInfo(Integer userId) {
    	Map<String,Object> list = agentsApplysService.selectAgentsApplysInfo(userId);
    	return Result.OK(list);
    }
}
