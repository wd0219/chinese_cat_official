package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopsMapper;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.service.IAppShopsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class AppShopsServiceImpl extends ServiceImpl<SktShopsMapper,SktShops> implements IAppShopsService {
    @Autowired
    private SktShopsMapper sktShopsMapper;

    @Override
    public Map<String,Object> selectUsersShopsByUserShopId(String shopId) {
        return sktShopsMapper.selectUsersShopsByUserShopId(Integer.parseInt(shopId));
    }

    @Override
    public SktShops selectShopsByShopId(int shopId) {
        return sktShopsMapper.selectShopsByShopId(shopId);
    }

    @Override
    public List<Map<String, Object>> selectShopsAndAccredsByShopId(Map<String, Object> map1) {
        return sktShopsMapper.selectShopsAndAccredsByShopId(map1);
    }

    @Override
    public int selectCountByShopId(int i) {
        return sktShopsMapper.selectCountByShopId(i);
    }
}
