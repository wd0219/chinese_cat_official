package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.GoodsScores;

import java.util.Map;

public interface IAppGoodsScoresService extends IService<GoodsScores> {
    void updateGoodsScores(Map<String, Object> goodsShopScoresMap);
}
