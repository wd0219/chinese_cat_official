package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;

import java.util.List;
import java.util.Map;

public interface IAppUsersBankcardsService extends IService<UsersBankcards> {
    List<Map<String,Object>> selectUsersBankAndBankByUserId(Integer userId);

    int selectCountByUserId(Integer userId);

    boolean deleteByIdAndUserId(Map<String, Object> map);
}
