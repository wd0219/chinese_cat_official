package com.stylefeng.guns.modular.huamao.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktExpress;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktExpressService;

/**
 * 快递管理控制器
 *
 * @author fengshuonan
 * @Date 2018-04-24 11:19:12
 */
@CrossOrigin
@Controller
@RequestMapping("/sktExpress")
public class SktExpressController extends BaseController {

    private String PREFIX = "/huamao/sktExpress/";

    @Autowired
    private ISktExpressService sktExpressService;

    /**
     * 跳转到快递管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktExpress.html";
    }

    /**
     * 跳转到添加快递管理
     */
    @RequestMapping("/sktExpress_add")
    public String sktExpressAdd() {
        return PREFIX + "sktExpress_add.html";
    }

    /**
     * 跳转到修改快递管理
     */
    @RequestMapping("/sktExpress_update/{sktExpressId}")
    public String sktExpressUpdate(@PathVariable Integer sktExpressId, Model model) {
        SktExpress sktExpress = sktExpressService.selectById(sktExpressId);
        model.addAttribute("item",sktExpress);
        LogObjectHolder.me().set(sktExpress);
        return PREFIX + "sktExpress_edit.html";
    }

    /**
     * 获取快递管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktExpressService.selectList(null);
    }

    /**
     * 新增快递管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktExpress sktExpress) {
        sktExpressService.insert(sktExpress);
        return SUCCESS_TIP;
    }

    /**
     * 删除快递管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktExpressId) {
        sktExpressService.deleteById(sktExpressId);
        return SUCCESS_TIP;
    }

    /**
     * 修改快递管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktExpress sktExpress) {
        sktExpressService.updateById(sktExpress);
        return SUCCESS_TIP;
    }

    /**
     * 快递管理详情
     */
    @RequestMapping(value = "/detail/{sktExpressId}")
    @ResponseBody
    public Object detail(@PathVariable("sktExpressId") Integer sktExpressId) {
        return sktExpressService.selectById(sktExpressId);
    }
    /**************************首页*****************************/
    /**
     * 查询所有快递
     */
    @RequestMapping(value = "/selectExpressAll")
    @ResponseBody
    public Result selectExpressAll() {
    	EntityWrapper<SktExpress> ew = new EntityWrapper<SktExpress>();
    	ew.where("dataFlag", 1);
    	List<SktExpress> list = sktExpressService.selectList(ew);
        return Result.OK(list);
    }
    
}
