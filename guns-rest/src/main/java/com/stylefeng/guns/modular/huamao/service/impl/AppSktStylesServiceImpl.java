package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktStylesMapper;
import com.stylefeng.guns.modular.huamao.model.SktStyles;
import com.stylefeng.guns.modular.huamao.service.IAppSktStylesService;
import org.springframework.stereotype.Service;

@Service
public class AppSktStylesServiceImpl extends ServiceImpl<SktStylesMapper,SktStyles> implements IAppSktStylesService {
}
