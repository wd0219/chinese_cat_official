package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class SktOrderRefundsList{

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;
	
	
	private Integer id;
	
	/**
     * 用户退款金额
     */
    private BigDecimal backMoney;
    
    /**
     * 退款时间
     */
    private Date refundTime;
    
    
	/**
     * 退款备注
     */
    private String refundRemark;
    
    /**
     * 退款状态-2平台不同意退款-1商家不同意 0用户申请退款 1商家同意退款 2平台已经退款
     */
    private Integer refundStatus;
	
	private String orderNo;
	
	private String loginName;
	
	private String userPhone;
	
	private String shopName;
	
	 /**
     * 实际支付金额 通过支付公司现金支付的金额
     */
    private BigDecimal realMoney;
	
	private String isRefund;
	
	/**
	 * 接受页面查询参数
	 */
	/**
     * 营业执照所在省id
     */
    private Integer provinceId;
    /**
     * 营业执照所在市id
     */
    private Integer cityId;
    /**
     * 营业执照所在区县id
     */
    private Integer areaId;
    
    /**
     * 查询时间开始
     */
    private String beginTime;
    /**
     * 查询时间结束
     */
    private String endTime;
    
    
    public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public BigDecimal getBackMoney() {
		return backMoney;
	}
	public void setBackMoney(BigDecimal backMoney) {
		this.backMoney = backMoney;
	}
	public Date getRefundTime() {
		return refundTime;
	}
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	public String getRefundRemark() {
		return refundRemark;
	}
	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}
	public Integer getRefundStatus() {
		return refundStatus;
	}
	public void setRefundStatus(Integer refundStatus) {
		this.refundStatus = refundStatus;
	}
	
	public Integer getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getAreaId() {
		return areaId;
	}
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getIsRefund() {
		return isRefund;
	}
	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}
	public BigDecimal getRealMoney() {
		return realMoney;
	}
	public void setRealMoney(BigDecimal realMoney) {
		this.realMoney = realMoney;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}
