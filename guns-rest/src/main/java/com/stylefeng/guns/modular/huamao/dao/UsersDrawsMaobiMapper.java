package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会员账户表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-11
 */
public interface UsersDrawsMaobiMapper extends BaseMapper<UsersDrawsMaobi> {

    UsersDrawsMaobi selectSumMoneyByUserId(Map<String, Object> map);

    List<UsersDrawsMaobi> selectAccountCashRecordByUserId(UsersDrawsMaobi usersDrawsMaobis);

    double selectAllMaobiByUserId(Integer userId);

    List<Map<String,Object>> selectRecordByUserId(UsersDrawsMaobi usersDrawsMaobis);

    Double selectAllhhtbByUserId(Integer userId);
}
