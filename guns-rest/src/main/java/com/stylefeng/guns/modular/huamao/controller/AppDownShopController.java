package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.DownShop;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.result.AppResult;
import com.stylefeng.guns.modular.huamao.service.ISktOrdersStoresService;
import com.stylefeng.guns.modular.huamao.service.ISktShopsService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import com.stylefeng.guns.modular.huamao.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
@Controller
@CrossOrigin
@RequestMapping("/app")
public class AppDownShopController  extends BaseController {

    @Autowired
    private IUsersService usersService;
    @Autowired
    private ISktOrdersStoresService sktOrdersStoresService;
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private ISktShopsService sktShopsService;

    /**
     * 我（商家）首页
     * @param userId
     * @return
     */
    @RequestMapping("/user/account")
    @ResponseBody
    public AppResult index(){
       Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
       if(user == null){
           return AppResult.ERROR("对不起您还未登录",-2);
       }
        DownShop downShop = usersService.appFindUserById(user.getUserId());
        if(downShop ==null){
            return AppResult.ERROR("无此用户！");
        }
        return AppResult.OK("查询成功！",downShop);
    }

    /**
     *商家订单线下确定
     * @param orderNo
     * @return
     */
    @RequestMapping("/score/orderStoreConfirm")
    @ResponseBody
    public AppResult orderStoreConfirm(@ModelAttribute("orderNo")String orderNo){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        Map<String,Object> map = sktOrdersStoresService.appOrderStoreConfirm(orderNo);
        String mes = (String)map.get("mes");
        if(mes != null && mes != ""){
            return AppResult.ERROR(mes);
        }
        return AppResult.OK("成功！",map);
    }

    /**
     * 商家订单线下取消
     * @param orderNo
     * @return
     */
    @RequestMapping("/score/orderStoreCancel")
    @ResponseBody
    public AppResult orderStoreCancel(@ModelAttribute("orderNo")String orderNo){
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        Integer shopId = sktShops1.getShopId();
        Integer update = sktOrdersStoresService.appOrderStoreCancel(orderNo);
        if(update != 0){
            return AppResult.OK("成功！");
        }
        return AppResult.ERROR("失败！");
    }

    /**
     *商家订单线下
     * @param page页数
     * @param pageSize	每页数量
     * @param orderStatus订单状态（1：待确认；2：取消；3：已完成）
     * @return
     */
    @RequestMapping("/orders/orderShop")
    @ResponseBody
    public AppResult orders(@ModelAttribute("page")Integer page,
                            @ModelAttribute("pageSize")Integer pageSize,
                            @ModelAttribute("orderStatus")String orderStatus){
//        获取用户登录信息
        Users user =(Users) cacheUtil.get(SessionUtils.getSessionId());
        if(user == null){
            return AppResult.ERROR("对不起您还未登录",-2);
        }
        Integer userId = user.getUserId();
        SktShops sktShops=new SktShops();
        sktShops.setUserId(userId);
        EntityWrapper<SktShops> entityWrapper =new EntityWrapper<SktShops>(sktShops);
        SktShops sktShops1 = sktShopsService.selectOne(entityWrapper);
        if(sktShops1==null){
            return   AppResult.ERROR("对不起，您还不是商家");
        }
        if(pageSize == null){
            pageSize = 5;
        }
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("orderStatus",orderStatus);
        map.put("shopId",sktShops1.getShopId());
        PageHelper.startPage(page,pageSize);
        List<Map<String,Object>> list = sktOrdersStoresService.appSelectAllOrders(map);
        JSONObject json=new JSONObject();
        json.put("maps", list);
        if(list != null && list.size() > 0){
            PageInfo pageInfo=new PageInfo(list) ;
            json.put("tatolPage",pageInfo.getPages());
        }else{
            json.put("tatolPage",1);
          //  return AppResult.ERROR("无此订单！");
        }
        return AppResult.OK(json);
    }

}
