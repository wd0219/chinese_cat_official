package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class SktAgentsAccountDTO {
	/**
	 * 代理公司账户表id
	 */
	private Integer accountId;
	/**
	 * 代理公司id
	 */
	private Integer agentId;
	/**
	 * 代理公司名称
	 */
	private String agentName;
	/**
	 * 积分
	 */
	private BigDecimal score;
	/**
	 * 待发积分 运营未开启时 代理股东通过后 奖励代理公司积分到此账号
	 */
	private BigDecimal freezeScore;
	/**
	 * 累计获得积分
	 */
	private BigDecimal totalScore;
	/**
	 * 开元宝
	 */
	private BigDecimal kaiyuan; 
	/**
	 * 累计获得开元宝
	 */
	private BigDecimal totalKaiyuan;
	/**
	 * 成为股东的分红系数
	 */
	private BigDecimal oneRatio; 
	/**
	 * 完成70%(落地)的分红系数
	 */
	private BigDecimal twoRatio; 
	/**
	 * 全部完成的分红系数
	 */
	private BigDecimal threeRatio;
	/**
	 * 运营账号是否开启 开启后账户积分开始转化开元宝 1开启 0关闭
	 */
	private Integer  runFlag;
	/**
	 * 运营账号开启时间
	 */
	private Date runTime;	
	/**
	 * 省id
	 */
	private Integer province;
	/**
	 * 市id
	 */
	private Integer citys;
	/**
	 * 区域id
	 */
	private Integer county;
	/**
	 * 开始时间
	 */
	private String beginTime;
	/**
	 * 结束时间
	 */
	private String endTime;
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public Integer getAgentId() {
		return agentId;
	}
	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public BigDecimal getScore() {
		return score;
	}
	public void setScore(BigDecimal score) {
		this.score = score;
	}
	public BigDecimal getFreezeScore() {
		return freezeScore;
	}
	public void setFreezeScore(BigDecimal freezeScore) {
		this.freezeScore = freezeScore;
	}
	public BigDecimal getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(BigDecimal totalScore) {
		this.totalScore = totalScore;
	}
	public BigDecimal getKaiyuan() {
		return kaiyuan;
	}
	public void setKaiyuan(BigDecimal kaiyuan) {
		this.kaiyuan = kaiyuan;
	}
	public BigDecimal getTotalKaiyuan() {
		return totalKaiyuan;
	}
	public void setTotalKaiyuan(BigDecimal totalKaiyuan) {
		this.totalKaiyuan = totalKaiyuan;
	}
	public BigDecimal getOneRatio() {
		return oneRatio;
	}
	public void setOneRatio(BigDecimal oneRatio) {
		this.oneRatio = oneRatio;
	}
	public BigDecimal getTwoRatio() {
		return twoRatio;
	}
	public void setTwoRatio(BigDecimal twoRatio) {
		this.twoRatio = twoRatio;
	}
	public BigDecimal getThreeRatio() {
		return threeRatio;
	}
	public void setThreeRatio(BigDecimal threeRatio) {
		this.threeRatio = threeRatio;
	}
	public Integer getRunFlag() {
		return runFlag;
	}
	public void setRunFlag(Integer runFlag) {
		this.runFlag = runFlag;
	}
	public Date getRunTime() {
		return runTime;
	}
	public void setRunTime(Date runTime) {
		this.runTime = runTime;
	}
	public Integer getProvince() {
		return province;
	}
	public void setProvince(Integer province) {
		this.province = province;
	}
	public Integer getCitys() {
		return citys;
	}
	public void setCitys(Integer citys) {
		this.citys = citys;
	}
	public Integer getCounty() {
		return county;
	}
	public void setCounty(Integer county) {
		this.county = county;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	
}
