package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogCashFreezeMapper;
import com.stylefeng.guns.modular.huamao.model.LogCashFreeze;
import com.stylefeng.guns.modular.huamao.model.TradeDictDTD;
import com.stylefeng.guns.modular.huamao.service.IAppLogCashFreezeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppLogCashFreezeServiceImpl extends ServiceImpl<LogCashFreezeMapper,LogCashFreeze> implements IAppLogCashFreezeService {

    @Autowired
    private LogCashFreezeMapper logCashFreezeMapper;

    @Override
    public List<LogCashFreeze> AccountWillCashRecord(Integer userId) {
        return logCashFreezeMapper.willCashRecord(userId);
    }

    @Override
    public TradeDictDTD selectTradeDictByType(Integer type) {
        return logCashFreezeMapper.selectTradeDictByType(type);
    }
}
