package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 开元宝营业额流水表返回
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public class LogKaiyuanTurnoverList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 开元宝类型 1线上营业额获得 2营业额赎回退还 3营业额赎回手续费和税率退还 4待发开元宝营业额转入 5线下营业额获得 6消费退款
	 * 31线上商城消费支出 32消费的手续费和税率的支出 33赎回支出 34赎回的手续费和税率 35购买库存积分支出 36购买库存积分的手续费和税率支出
	 * 37线下商城消费支出 38购买牌匾 39购买牌匾的手续费和税率
	 */
	private Integer type;
	/**
	 * 发起用户ID 0为平台
	 */
	private Integer fromId;
	/**
	 * 目标用户ID 0为平台
	 */
	private Integer userId;
	/**
	 * 对应订单号
	 */
	private String orderNo;
	/**
	 * 操作前的金额
	 */
	private BigDecimal preKaiyuan;
	/**
	 * 流水标志 -1减少 1增加
	 */
	private Integer kaiyuanType;
	/**
	 * 金额
	 */
	private BigDecimal kaiyuan;
	/**
	 * 变化金额的升级版，在金额前边加上“+ —”号，为前台显示做准备
	 */
	private String kaiyuanp;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 有效状态 1有效 0删除
	 */
	private Integer dataFlag;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	public LogKaiyuanTurnoverList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getFromId() {
		return fromId;
	}

	public void setFromId(Integer fromId) {
		this.fromId = fromId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public BigDecimal getPreKaiyuan() {
		return preKaiyuan;
	}

	public void setPreKaiyuan(BigDecimal preKaiyuan) {
		this.preKaiyuan = preKaiyuan;
	}

	public Integer getKaiyuanType() {
		return kaiyuanType;
	}

	public void setKaiyuanType(Integer kaiyuanType) {
		this.kaiyuanType = kaiyuanType;
	}

	public BigDecimal getKaiyuan() {
		return kaiyuan;
	}

	public void setKaiyuan(BigDecimal kaiyuan) {
		this.kaiyuan = kaiyuan;
	}

	public String getKaiyuanp() {
		return kaiyuanp;
	}

	public void setKaiyuanp(String kaiyuanp) {
		this.kaiyuanp = kaiyuanp;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Integer dataFlag) {
		this.dataFlag = dataFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
