package com.stylefeng.guns.modular.huamao.dao;

import java.util.List;
import java.util.Map;

import com.stylefeng.guns.modular.huamao.model.*;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 代理公司表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-04-18
 */
public interface SktAgentsMapper extends BaseMapper<SktAgents> {
	/**
	 * 查询显示列表
	 * @param sktAgentsDTO
	 * @return
	 */
	public List<SktAgentsDTO> showSktAgentsIno(SktAgentsDTO sktAgentsDTO);
	/**
	 * 查询名字跟id 电话
	 * @param nameOrPhone
	 * @return
	 */
	public Map<String, Object> selectNameOrPhone(@Param(value = "nameOrPhone") String nameOrPhone);
	/**
	 * 根据地址 级别查询id
	 * @param agentsApplys
	 */
	public SktAgents selectId(AgentsApplysDTO agentsApplys);
	/**
	 * 代理公司登录
	 * @param
	 * @return
	 */
	public Map<String,Object> agentsLogin(@Param(value = "loginNameOrPhone") String loginNameOrPhone,
										  @Param(value = "loginPwd") String loginPwd);

	/**
	 * 查询是否有该代理公司
	 * @param loginNameOrPhone
	 * @return
	 */
	public SktAgents selectSecret(@Param(value = "loginNameOrPhone") String loginNameOrPhone);

    /**
     * 查询基础信息
     * @param sktAgents
     * @return
     */
    public Map<String,Object> selectAgentsInfo(SktAgents sktAgents);

	/**
	 * 查询华宝
	 * @param logKaiyuanAgentList
	 * @return
	 */
	List<LogKaiyuanAgent> selectAgentsHuabao(LogKaiyuanAgentList logKaiyuanAgentList);

	/**
	 * 查询积分
	 * @param logScoreAgentList
	 * @return
	 */
	public List<LogScoreAgent> selectAgentsscore(LogScoreAgentList logScoreAgentList);
	/**
	 * 查询股东
	 * @param sktAgentsStockholderDTO
	 * @return
	 */
	public List<SktAgentsStockholderRet> selectAgentsStockholder(SktAgentsStockholderDTO sktAgentsStockholderDTO);
	/**
	 * 商家查询
	 * @param shopName 
	 * @param accountShop
	 * @return
	 */
	public List<Map<String,Object>> selectAccountShop(@Param(value="list")List<Integer> list, @Param(value="shopName")String shopName);
	/**
	 * 
	 * @param agentsBankcards
	 * @return
	 */
	public List<AgentsBankcards> selectAgentBankcards(AgentsBankcards agentsBankcards);
	
	/**
	 * 查看分红记录
	 * @param userId
	 * @return
	 */
	public List<SktAgentsProfit> selectAgentsProfit(@Param(value="userId") Integer userId);

	//查询代理公司信息
    SktAgents findAgent(Integer agentId);
	public List<Integer> selectShopIdList(SktAgents sktAgents);
	public List<Map<String,Object>> selectShopInfo(@Param(value="shopId") Integer shopId);

    SktAgents selectAgentsProvinceId(Map<String, Object> map);

	SktAgents selectAgentsCityId(Map<String, Object> map);

	SktAgents selectAgentsAreaId(Map<String, Object> map);
}
