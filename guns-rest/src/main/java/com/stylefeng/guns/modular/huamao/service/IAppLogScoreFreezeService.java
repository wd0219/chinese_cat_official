package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogScoreFreeze;

import java.util.List;
import java.util.Map;

public interface IAppLogScoreFreezeService extends IService<LogScoreFreeze> {
    List<LogScoreFreeze> selectLogScoreFreezeByUserId(Map<String, Object> map);
}
