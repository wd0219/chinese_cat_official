package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktCarts;
import com.stylefeng.guns.modular.huamao.model.SktOrderGoods;
import com.stylefeng.guns.modular.huamao.result.AppResult;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author caody123
 * @since 2018-05-10
 */
public interface SktCartsMapper extends BaseMapper<SktCarts> {
	
	/**
	 * 查询购物车的内容
	 * @param
	 * @return
	 */
	public List<Map<String,Object>> selectCartsByuserId(Map<String,Object> map);
	
	/**
	 * 购物车结算时查询
	 * @return
	 */
	public SktOrderGoods selectCartsSettlementByuserId(Map<String,Integer> map);

	public SktOrderGoods selectCartsSpecsByuserId(Map<String,Integer> map);

	List<Map<String,Object>> appFindGoodsByShopId(Map<String, Object> map3);

    List<Integer> appFindIsCheck(Integer userId);
    
    public Integer appnewAddressBuy(Map<String,Integer> mmp);
    
    public Map<String,Integer> newAddressPrice(Map<String,Integer> mmp);

	/**.
	 * 查询订单积分
	 * @return
	 */
	public Integer selectSumScore(Map<String,Object> map);

    List<Map<String,Object>> appFindGoodsByShopIdAll(Map<String, Object> map3);
}
