package com.stylefeng.guns.modular.huamao.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefunds;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefundsList;
import com.stylefeng.guns.modular.huamao.model.SktOrders;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktOrderRefundsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 退款订单控制器
 *
 * @author fengshuonan
 * @Date 2018-04-19 16:37:14
 */
@Controller
@RequestMapping("/sktOrderRefunds")
@CrossOrigin
public class SktOrderRefundsController extends BaseController {

    private String PREFIX = "/huamao/sktOrderRefunds/";
    @Autowired
    private ISktOrderRefundsService sktOrderRefundsService;



/***************************前台****************************/


    /**
     *用户点击退款
     */
    @RequestMapping(value = "/applayRefundsOrders")
    @ResponseBody
    public Result applayRefundsOrders(SktOrderRefunds sktOrderRefunds) {
        JSONObject json = sktOrderRefundsService.applayRefundsOrders(sktOrderRefunds);
        return Result.OK(json);
    }

    /**
     * 商家对退货请求的处理
     * @param sktOrderRefunds
     * @return
     */
    @RequestMapping(value = "/echoRefundsOrders")
    @ResponseBody
    public Result echoRefundsOrders(SktOrderRefunds sktOrderRefunds){
        JSONObject json = sktOrderRefundsService.echoRefundsOrders(sktOrderRefunds);
        return Result.OK(json);
    }
}
