package com.stylefeng.guns.modular.huamao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.huamao.model.Users;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IUsersService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.huamao.model.Shopping;
import com.stylefeng.guns.modular.huamao.service.IShoppingService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 购物券控制器
 *
 * @author fengshuonan
 * @Date 2018-06-19 17:42:10
 */
@Controller
@RequestMapping("/shopping")
@CrossOrigin
public class ShoppingController extends BaseController {

    private String PREFIX = "/huamao/shopping/";

    @Autowired
    private IShoppingService shoppingService;
    @Autowired
    private IUsersService usersService;
    @Autowired
    private IAccountService accountService;
    /**
     * 跳转到购物券首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "shopping.html";
    }

    /**
     * 跳转到添加购物券
     */
    @RequestMapping("/shopping_add")
    public String shoppingAdd() {
        return PREFIX + "shopping_add.html";
    }

    /**
     * 跳转到修改购物券
     */
    @RequestMapping("/shopping_update/{shoppingId}")
    public String shoppingUpdate(@PathVariable Integer shoppingId, Model model) {
        Shopping shopping = shoppingService.selectById(shoppingId);
        model.addAttribute("item",shopping);
        LogObjectHolder.me().set(shopping);
        return PREFIX + "shopping_edit.html";
    }

    /**
     * 获取购物券列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return shoppingService.selectList(null);
    }

    /**
     * 新增购物券
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Shopping shopping) {
        shoppingService.insert(shopping);
        return SUCCESS_TIP;
    }

    /**
     * 删除购物券
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer shoppingId) {
        shoppingService.deleteById(shoppingId);
        return SUCCESS_TIP;
    }

    /**
     * 修改购物券
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Shopping shopping) {
        shoppingService.updateById(shopping);
        return SUCCESS_TIP;
    }

    /**
     * 购物券详情
     */
    @RequestMapping(value = "/detail/{shoppingId}")
    @ResponseBody
    public Object detail(@PathVariable("shoppingId") Integer shoppingId) {
        return shoppingService.selectById(shoppingId);
    }


    /**
     * 查询我的优惠券
     * @param status  优惠券状态
     * @param userId  用户id
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    @RequestMapping("/myShopping")
    @ResponseBody
    public Result myShopping(String status, Integer userId, String startTime, String endTime){
        Users user=   usersService.selectById(userId);
        if (user!=null){
            Map<String,Object> map=new HashMap<>();
            map.put("dataId",user.getUserId());
            map.put("status",status);
            map.put("dataFlag",1);
            map.put("startTime",startTime);
            map.put("endTime",endTime);
            List<Map<String, Object>> maps = shoppingService.selectShoppingList(map);
            Map<String, Object> map1=new HashMap<>();
            if (maps!=null && map.size()>0){
                map1.put("total",maps.size());
                map1.put("maps",maps);
            }
            return    Result.OK(map1);
        }else {
            return   Result.EEROR("无用户信息");
        }
    }
    /**
     *
     * @param userPhone  用户手机号
     * @param type 用户身份 1会员 2代理
     * @param money 金额
     * @param sceneType 购物券使用场景 1通用 2线上 3线下 4无人超市
     * @return
     */

//增加购物券
    @RequestMapping("/addShopping")
    @ResponseBody
    public  Result addShopping(String userPhone,Integer type,BigDecimal money,Integer sceneType){
        Map<String, Object> map = accountService.addShopping(userPhone, type, money, sceneType);
        if ("00".equals(map.get("code").toString())){
            return  Result.EEROR(map.get("msg").toString());
        }else{
            return   Result.OK(map);
        }

    }
}
