package com.stylefeng.guns.modular.huamao.mongodb;

import lombok.Data;


@Data
public class SendMessage {


    private long id;

    private String name;

    private int age;
}
