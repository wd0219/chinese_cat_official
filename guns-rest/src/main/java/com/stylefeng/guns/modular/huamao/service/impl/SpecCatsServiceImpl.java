package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SpecCatsMapper;
import com.stylefeng.guns.modular.huamao.model.SpecCats;
import com.stylefeng.guns.modular.huamao.model.SpecCatsCustem;
import com.stylefeng.guns.modular.huamao.service.ISpecCatsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规格分类表 服务实现类
 * </p>
 *
 * @author wudi123
 * @since 2018-04-10
 */
@Service
public class SpecCatsServiceImpl extends ServiceImpl<SpecCatsMapper, SpecCats> implements ISpecCatsService {

	@Override
	public List<Map<String, Object>> selectAll(SpecCatsCustem specCatsCustem) {
		return baseMapper.selectAll(specCatsCustem);
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer insertSpecCats(SpecCatsCustem specCatsCustem) {
		// TODO Auto-generated method stub
		return baseMapper.insertSpecCats(specCatsCustem);
	}

	@Override
	public SpecCatsCustem selectSpecCatsCustemById(int id) {
		// TODO Auto-generated method stub
		return baseMapper.selectSpecCatsCustemById(id);
	}

}
