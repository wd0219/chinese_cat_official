package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.SktDateTransform;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 积分转换开元宝每日记录表 Mapper 接口
 * </p>
 *
 * @author ck123
 * @since 2018-04-20
 */
public interface SktDateTransformMapper extends BaseMapper<SktDateTransform> {
	
	public List<SktDateTransform> sktDateTransformfindAll(SktDateTransform sktDateTransform);

    SktDateTransform findRatioByDate(Integer nowDate);

    List<SktDateTransform> selectDateTransform();
}
