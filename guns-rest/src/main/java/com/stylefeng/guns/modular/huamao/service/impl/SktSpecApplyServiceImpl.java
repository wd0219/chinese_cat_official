package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.GoodsCatsMapper;
import com.stylefeng.guns.modular.huamao.dao.SktSpecApplyMapper;
import com.stylefeng.guns.modular.huamao.dao.SpecCatsMapper;
import com.stylefeng.guns.modular.huamao.model.GoodsCats;
import com.stylefeng.guns.modular.huamao.model.SktSpecApply;
import com.stylefeng.guns.modular.huamao.model.SpecCats;
import com.stylefeng.guns.modular.huamao.service.ISktSpecApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规则申请表 服务实现类
 * </p>
 *
 * @author py
 * @since 2018-08-23
 */
@Service
public class SktSpecApplyServiceImpl extends ServiceImpl<SktSpecApplyMapper, SktSpecApply> implements ISktSpecApplyService {
    @Autowired
    private SktSpecApplyMapper sktSpecApplyMapper;
    @Autowired
    private SpecCatsMapper specCatsMapper;
    @Override
    public Map<String,String> insertSktSpecApply(SktSpecApply sktSpecApply) {
        Map<String, String> result = new HashMap<String, String>();
        List<SpecCats> specCatsList = specCatsMapper.selectList(new EntityWrapper<SpecCats>().eq("goodsCatId", sktSpecApply.getGoodsCatId()));
        if (!specCatsList.isEmpty()) {
            for (SpecCats specCats : specCatsList) {
                if (specCats.getCatName().equals(sktSpecApply.getCatName())) {
                    result.put("code", "-1");
                    result.put("msg", "申请失败,当商品分类该规格已存在");
                    return result;
                }
            }
        }
        sktSpecApply.setCreateTime(new Date());
        Integer insert = sktSpecApplyMapper.insert(sktSpecApply);
        if (insert != 1){
            result.put("code", "-2");
            result.put("msg", "提交申请失败");
            return result;
        }
        result.put("code", "1");
        result.put("msg", "申请成功");
        return result;

    }
}
