package com.stylefeng.guns.modular.huamao.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.modular.huamao.model.LogMaobi;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 转化猫币记录表 Mapper 接口
 * </p>
 *
 * @author gxz123
 * @since 2018-04-24
 */
public interface LogMaobiMapper extends BaseMapper<LogMaobi> {


}
