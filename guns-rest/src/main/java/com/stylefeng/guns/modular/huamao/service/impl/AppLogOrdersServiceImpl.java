package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.LogOrdersMapper;
import com.stylefeng.guns.modular.huamao.model.LogOrders;
import com.stylefeng.guns.modular.huamao.service.IAppLogOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppLogOrdersServiceImpl extends ServiceImpl<LogOrdersMapper,LogOrders> implements IAppLogOrdersService {
    @Autowired
    private LogOrdersMapper logOrdersMapper;
}
