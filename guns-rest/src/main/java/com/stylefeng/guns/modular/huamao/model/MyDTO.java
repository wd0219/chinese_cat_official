package com.stylefeng.guns.modular.huamao.model;

public class MyDTO{

	private Integer applyId;
	private String userName;
	private String legalPerson;
	private String companyName;
	private String telephone; 
	private String shopName;
	private Integer shopStatus;
	private String createTime;
	private Integer checkStaffId; 
	private String checkTime;
	private String statusDesc;
	public Integer getApplyId() {
		return applyId;
	}
	public void setApplyId(Integer applyId) {
		this.applyId = applyId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLegalPerson() {
		return legalPerson;
	}
	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Integer getShopStatus() {
		return shopStatus;
	}
	public void setShopStatus(Integer shopStatus) {
		this.shopStatus = shopStatus;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public Integer getCheckStaffId() {
		return checkStaffId;
	}
	public void setCheckStaffId(Integer checkStaffId) {
		this.checkStaffId = checkStaffId;
	}
	public String getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	@Override
	public String toString() {
		return "MyDTO [applyId=" + applyId + ", userName=" + userName + ", legalPerson=" + legalPerson
				+ ", companyName=" + companyName + ", telephone=" + telephone + ", shopName=" + shopName
				+ ", shopStatus=" + shopStatus + ", createTime=" + createTime + ", checkStaffId=" + checkStaffId
				+ ", checkTime=" + checkTime + ", statusDesc=" + statusDesc + "]";
	}
	public MyDTO(Integer applyId, String userName, String legalPerson, String companyName, String telephone,
			String shopName, Integer shopStatus, String createTime, Integer checkStaffId, String checkTime,
			String statusDesc) {
		super();
		this.applyId = applyId;
		this.userName = userName;
		this.legalPerson = legalPerson;
		this.companyName = companyName;
		this.telephone = telephone;
		this.shopName = shopName;
		this.shopStatus = shopStatus;
		this.createTime = createTime;
		this.checkStaffId = checkStaffId;
		this.checkTime = checkTime;
		this.statusDesc = statusDesc;
	}
	public MyDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
//	@Override
//	protected Serializable pkVal() {
//		return this.applyId;
//	}
	
}
