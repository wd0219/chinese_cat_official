package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktShopScores;

import java.util.Map;

public interface IAppSktShopScoresService extends IService<SktShopScores> {
    void updateSktShopScores(Map<String, Object> goodsShopScoresMap);
}
