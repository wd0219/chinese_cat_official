package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogCash;
import com.stylefeng.guns.modular.huamao.model.UsersDrawsMaobi;
import com.stylefeng.guns.modular.huamao.result.AppResult;

public interface IAppUserDrwasMaobiService extends IService<UsersDrawsMaobi> {
    AppResult AccountMaobiRecord(Integer pageNum, UsersDrawsMaobi usersDrawsMaobi);

    double selectAllMaobiByUserId(Integer userId);

    Double selectAllhhtbByUserId(Integer userId);
}
