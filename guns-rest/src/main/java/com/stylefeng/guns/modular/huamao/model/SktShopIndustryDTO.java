package com.stylefeng.guns.modular.huamao.model;

public class SktShopIndustryDTO {
	
	private Integer applyId;
	private String userName;
	private String areas;
	private String province;
	private String citys;
	private String county;
	private String hyName;
	private String legalPerson;
	private String companyName;
	private Integer companyType;
	private String address;
	private String telephone;
	private String createTime;
	private Integer shopStatus;
	private Integer checkStaffId;
	private String checkTime;
	private Integer applyStatus;
	private Integer userId;
	private String loginName;
	private Integer isLegal;
	private String IDnumber;
	private String legalPersonImg;
	private String licenseNo;
	private Integer licenseMerge;
	private String licenseImg;
	private String shopName;
	private String logo;
	public Integer getApplyId() {
		return applyId;
	}
	public void setApplyId(Integer applyId) {
		this.applyId = applyId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAreas() {
		return areas;
	}
	public void setAreas(String areas) {
		this.areas = areas;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCitys() {
		return citys;
	}
	public void setCitys(String citys) {
		this.citys = citys;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getHyName() {
		return hyName;
	}
	public void setHyName(String hyName) {
		this.hyName = hyName;
	}
	public String getLegalPerson() {
		return legalPerson;
	}
	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Integer getCompanyType() {
		return companyType;
	}
	public void setCompanyType(Integer companyType) {
		this.companyType = companyType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public Integer getShopStatus() {
		return shopStatus;
	}
	public void setShopStatus(Integer shopStatus) {
		this.shopStatus = shopStatus;
	}
	public Integer getCheckStaffId() {
		return checkStaffId;
	}
	public void setCheckStaffId(Integer checkStaffId) {
		this.checkStaffId = checkStaffId;
	}
	public String getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}
	public Integer getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(Integer applyStatus) {
		this.applyStatus = applyStatus;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Integer getIsLegal() {
		return isLegal;
	}
	public void setIsLegal(Integer isLegal) {
		this.isLegal = isLegal;
	}
	public String getIDnumber() {
		return IDnumber;
	}
	public void setIDnumber(String iDnumber) {
		IDnumber = iDnumber;
	}
	public String getLegalPersonImg() {
		return legalPersonImg;
	}
	public void setLegalPersonImg(String legalPersonImg) {
		this.legalPersonImg = legalPersonImg;
	}
	public String getLicenseNo() {
		return licenseNo;
	}
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}
	public Integer getLicenseMerge() {
		return licenseMerge;
	}
	public void setLicenseMerge(Integer licenseMerge) {
		this.licenseMerge = licenseMerge;
	}
	public String getLicenseImg() {
		return licenseImg;
	}
	public void setLicenseImg(String licenseImg) {
		this.licenseImg = licenseImg;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	@Override
	public String toString() {
		return "SktShopIndustryDTO [applyId=" + applyId + ", userName=" + userName + ", areas=" + areas + ", province="
				+ province + ", citys=" + citys + ", county=" + county + ", hyName=" + hyName + ", legalPerson="
				+ legalPerson + ", companyName=" + companyName + ", companyType=" + companyType + ", address=" + address
				+ ", telephone=" + telephone + ", createTime=" + createTime + ", shopStatus=" + shopStatus
				+ ", checkStaffId=" + checkStaffId + ", checkTime=" + checkTime + ", applyStatus=" + applyStatus
				+ ", userId=" + userId + ", loginName=" + loginName + ", isLegal=" + isLegal + ", IDnumber=" + IDnumber
				+ ", legalPersonImg=" + legalPersonImg + ", licenseNo=" + licenseNo + ", licenseMerge=" + licenseMerge
				+ ", licenseImg=" + licenseImg + ", shopName=" + shopName + ", logo=" + logo + "]";
	}
	public SktShopIndustryDTO(Integer applyId, String userName, String areas, String province, String citys,
			String county, String hyName, String legalPerson, String companyName, Integer companyType, String address,
			String telephone, String createTime, Integer shopStatus, Integer checkStaffId, String checkTime,
			Integer applyStatus, Integer userId, String loginName, Integer isLegal, String iDnumber,
			String legalPersonImg, String licenseNo, Integer licenseMerge, String licenseImg, String shopName,
			String logo) {
		super();
		this.applyId = applyId;
		this.userName = userName;
		this.areas = areas;
		this.province = province;
		this.citys = citys;
		this.county = county;
		this.hyName = hyName;
		this.legalPerson = legalPerson;
		this.companyName = companyName;
		this.companyType = companyType;
		this.address = address;
		this.telephone = telephone;
		this.createTime = createTime;
		this.shopStatus = shopStatus;
		this.checkStaffId = checkStaffId;
		this.checkTime = checkTime;
		this.applyStatus = applyStatus;
		this.userId = userId;
		this.loginName = loginName;
		this.isLegal = isLegal;
		IDnumber = iDnumber;
		this.legalPersonImg = legalPersonImg;
		this.licenseNo = licenseNo;
		this.licenseMerge = licenseMerge;
		this.licenseImg = licenseImg;
		this.shopName = shopName;
		this.logo = logo;
	}
	public SktShopIndustryDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
