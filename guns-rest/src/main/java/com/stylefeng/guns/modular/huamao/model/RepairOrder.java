package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author gxz123
 * @since 2018-04-26
 */
@TableName("skt_repair_order")
public class RepairOrder extends Model<RepairOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    private Integer userId;
    /**
     * 内部业务订单号
     */
    private String orderNo;
    /**
     * 第三方流水号
     */
    private String outTradeNo;
    /**
     * 业务类型 BUY_GOODS:商城消费 BUY_SCORE:购买积分 RECHARGE:充值 UPGRADE:升级 BUY_PLAQUE：购买牌匾 WORK_ORDER：工单
     */
    private String businessType;
    /**
     * 支付类型（1：微信；2：支付宝；3：快捷；4：网关）
     */
    private Integer payType;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 操作人ID
     */
    private Integer operateId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getOperateId() {
        return operateId;
    }

    public void setOperateId(Integer operateId) {
        this.operateId = operateId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "RepairOrder{" +
        "id=" + id +
        ", userId=" + userId +
        ", orderNo=" + orderNo +
        ", outTradeNo=" + outTradeNo +
        ", businessType=" + businessType +
        ", payType=" + payType +
        ", createTime=" + createTime +
        ", operateId=" + operateId +
        "}";
    }
}
