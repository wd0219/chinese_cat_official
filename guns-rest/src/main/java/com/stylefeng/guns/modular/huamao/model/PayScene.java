package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author slt123
 * @since 2018-04-10
 */
@TableName("skt_payscene")
public class PayScene extends Model<PayScene> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String sceneCode;

    private String sceneExplain;

    private Integer offline;

    private Integer cash;

    private Integer kaiyuan;

    private Integer kaiyuanturnover;

    private Integer alipay;

    private Integer weixin;

    private Integer shortcut;

    private Integer gateway;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSceneCode() {
        return sceneCode;
    }

    public void setSceneCode(String sceneCode) {
        this.sceneCode = sceneCode;
    }

    public String getSceneExplain() {
        return sceneExplain;
    }

    public void setSceneExplain(String sceneExplain) {
        this.sceneExplain = sceneExplain;
    }

    public Integer getOffline() {
        return offline;
    }

    public void setOffline(Integer offline) {
        this.offline = offline;
    }

    public Integer getCash() {
        return cash;
    }

    public void setCash(Integer cash) {
        this.cash = cash;
    }

    public Integer getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(Integer kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public Integer getKaiyuanturnover() {
        return kaiyuanturnover;
    }

    public void setKaiyuanturnover(Integer kaiyuanturnover) {
        this.kaiyuanturnover = kaiyuanturnover;
    }

    public Integer getAlipay() {
        return alipay;
    }

    public void setAlipay(Integer alipay) {
        this.alipay = alipay;
    }

    public Integer getWeixin() {
        return weixin;
    }

    public void setWeixin(Integer weixin) {
        this.weixin = weixin;
    }

    public Integer getShortcut() {
        return shortcut;
    }

    public void setShortcut(Integer shortcut) {
        this.shortcut = shortcut;
    }

    public Integer getGateway() {
        return gateway;
    }

    public void setGateway(Integer gateway) {
        this.gateway = gateway;

    }


    @Override
    public String toString() {
        return "PayScene{" +
                "id=" + id +
                ", sceneCode='" + sceneCode + '\'' +
                ", sceneExplain='" + sceneExplain + '\'' +
                ", offline=" + offline +
                ", cash=" + cash +
                ", kaiyuan=" + kaiyuan +
                ", kaiyuanturnover=" + kaiyuanturnover +
                ", alipay=" + alipay +
                ", weixin=" + weixin +
                ", shortcut=" + shortcut +
                ", gateway=" + gateway +
                '}';
    }
}
