package com.stylefeng.guns.modular.huamao.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.UsersBankcards;
import com.stylefeng.guns.modular.huamao.model.UsersBankcardsList;

/**
 * <p>
 * 用户的银行卡表 服务类
 * </p>
 *
 * @author gxz123
 * @since 2018-04-19
 */
public interface IUsersBankcardsService extends IService<UsersBankcards> {

	// 用户银行卡表展示
	public List<UsersBankcardsList> selectUsersBankcardsAll(UsersBankcardsList usersBankcardsList);

	// 通过id进行逻辑删除
	public void deleteById2(Integer usersBankcardsId);
	/**
	 * 根据用户查询用户银行卡
	 * @param usersBankcardsList
	 * @return
	 */
	public List<Map<String,Object>> selectUserBranks(UsersBankcardsList usersBankcardsList);
	/**
	 * 银行卡认证
	 * @param usersBankcardsList
	 * @return
	 */
	public  String quickApproveBank(UsersBankcardsList usersBankcardsList);
	/**
	 * 修改银行卡默认状态
	 * @param usersBankcardsList
	 * @return
	 */
	public Integer updateUserBrankStatus(UsersBankcards usersBankcards);
	/**
	 * 添加银行卡
	 * @param usersBankcards
	 * @return
	 */
	public String addBankcards(UsersBankcardsList usersBankcards);
}
