package com.stylefeng.guns.modular.huamao.model;

import java.math.BigDecimal;
import java.util.Date;

public class SktOrdersStoresDto {
	 private Integer orderId;
	    /**
	     * 订单号
	     */
	    private String orderNo;
	    /**
	     * 门店ID
	     */
	    private Integer shopId;
	    /**
	     * 用户Id
	     */
	    private Integer userId;
	    /**
	     * 用户昵称
	     */
	    private String loginName;
	    /**
	     * 用户电话
	     */
	    private String userPhone;
	    /**
	     * 商店名称
	     */
	    private String shopName;
	    /**
	     * 订单总金额 (realMoney+cash+kaiyuan)
	     */
	    private BigDecimal totalMoney;
	    /**
	     * 支付金额
	     */
	    private String payMoney;
	    /**
	     * 支付方式 1:线下现金支付 2 现金账户支付 3开元宝支付 4第三方支付 5混合支付
	     */
	    private Integer payType;
	    /**
	     * 订单状态 1:用户下单 2商家取消 3已完成
	     */
	    private Integer orderStatus;
	    private String sOrderStatus;
	    /**
	     * 订单来源 1商家补单 2消费者主动下单 3面对面付
	     */
	    private Integer orderType;
	    /**
	     * 订单备注
	     */
	    private String orderRemarks;
	    /**
	     * 下单时间
	     */
	    private Date createTime;
	    /**
	     * 开始时间
	     */
	    private String beginTime;
	    /**
	     * 结束时间
	     */
	    private String endTime;
	    /**
		 * 省id
		 */
		private String province;
		/**
		 * 市id
		 */
		private String citys;
		/**
		 * 县id
		 */
		private String county;
		public Integer getOrderId() {
			return orderId;
		}
		public void setOrderId(Integer orderId) {
			this.orderId = orderId;
		}
		public String getOrderNo() {
			return orderNo;
		}
		public void setOrderNo(String orderNo) {
			this.orderNo = orderNo;
		}
		public Integer getShopId() {
			return shopId;
		}
		public void setShopId(Integer shopId) {
			this.shopId = shopId;
		}
		public Integer getUserId() {
			return userId;
		}
		public void setUserId(Integer userId) {
			this.userId = userId;
		}
		public String getLoginName() {
			return loginName;
		}
		public void setLoginName(String loginName) {
			this.loginName = loginName;
		}
		public String getUserPhone() {
			return userPhone;
		}
		public void setUserPhone(String userPhone) {
			this.userPhone = userPhone;
		}
		public String getShopName() {
			return shopName;
		}
		public void setShopName(String shopName) {
			this.shopName = shopName;
		}
		public BigDecimal getTotalMoney() {
			return totalMoney;
		}
		public void setTotalMoney(BigDecimal totalMoney) {
			this.totalMoney = totalMoney;
		}
		public String getPayMoney() {
			return payMoney;
		}
		public void setPayMoney(String payMoney) {
			this.payMoney = payMoney;
		}
		public Integer getPayType() {
			return payType;
		}
		public void setPayType(Integer payType) {
			this.payType = payType;
		}
		public Integer getOrderStatus() {
			return orderStatus;
		}
		public void setOrderStatus(Integer orderStatus) {
			this.orderStatus = orderStatus;
		}
		public Integer getOrderType() {
			return orderType;
		}
		public void setOrderType(Integer orderType) {
			this.orderType = orderType;
		}
		public String getOrderRemarks() {
			return orderRemarks;
		}
		public void setOrderRemarks(String orderRemarks) {
			this.orderRemarks = orderRemarks;
		}
		public Date getCreateTime() {
			return createTime;
		}
		public void setCreateTime(Date createTime) {
			this.createTime = createTime;
		}
		public String getBeginTime() {
			return beginTime;
		}
		public void setBeginTime(String beginTime) {
			this.beginTime = beginTime;
		}
		public String getEndTime() {
			return endTime;
		}
		public void setEndTime(String endTime) {
			this.endTime = endTime;
		}
		public String getProvince() {
			return province;
		}
		public void setProvince(String province) {
			this.province = province;
		}
		public String getCitys() {
			return citys;
		}
		public void setCitys(String citys) {
			this.citys = citys;
		}
		public String getCounty() {
			return county;
		}
		public void setCounty(String county) {
			this.county = county;
		}
		public String getsOrderStatus() {
			return sOrderStatus;
		}
		public void setsOrderStatus(String sOrderStatus) {
			this.sOrderStatus = sOrderStatus;
		}
		
	    
}
