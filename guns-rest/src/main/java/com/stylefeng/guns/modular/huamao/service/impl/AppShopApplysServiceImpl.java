package com.stylefeng.guns.modular.huamao.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopApplysMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopApplys;
import com.stylefeng.guns.modular.huamao.service.IAppShopApplysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppShopApplysServiceImpl extends ServiceImpl<SktShopApplysMapper,SktShopApplys> implements IAppShopApplysService {
    @Autowired
    private SktShopApplysMapper sktShopApplysMapper;

    @Override
    public SktShopApplys selectShopApplysByUserId(Integer userId) {
        return sktShopApplysMapper.selectShopApplysByUserId(userId);
    }
}
