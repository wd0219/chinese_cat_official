package com.stylefeng.guns.modular.huamao.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 商家账户返回
 */
public class AccountShopList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 用户手机
	 */
	private String userPhone;
	/**
	 * 用户类型
	 */
	private Integer userType;
	/**
	 * 线上商城营业额
	 */
	private BigDecimal onlineTurnover;
	/**
	 * 线下商家营业额
	 */
	private BigDecimal offlineTurnover;
	/**
	 * 库存积分（商家专用）
	 */
	private BigDecimal stockScore;
	/**
	 * 冻结的积分(线上商家下单)
	 */
	private BigDecimal freezeStockScore;
	/**
	 * 累计充值库存积分
	 */
	private BigDecimal totalStockScore;
	/**
	 * 累计分发库存积分
	 */
	private BigDecimal totalGiveStockScore;
	/**
	 * 待发开元宝营业额
	 */
	private BigDecimal freezeKaiyuan;
	/**
	 * 门店获得开元宝支付的营业额
	 */
	private BigDecimal kaiyuanTurnover;
	/**
	 * 门店累计获得开元宝支付的营业额
	 */
	private BigDecimal totalKaiyuanTurnover;
	/**
	 * 查询时的开始时间
	 */
	private String beginTime;
	/**
	 * 查询时的结束时间
	 */
	private String endTime;

	public AccountShopList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountShopList(String userPhone, Integer userType, BigDecimal onlineTurnover, BigDecimal offlineTurnover,
			BigDecimal stockScore, BigDecimal freezeStockScore, BigDecimal totalStockScore,
			BigDecimal totalGiveStockScore, BigDecimal freezeKaiyuan, BigDecimal kaiyuanTurnover,
			BigDecimal totalKaiyuanTurnover, String beginTime, String endTime) {
		super();
		this.userPhone = userPhone;
		this.userType = userType;
		this.onlineTurnover = onlineTurnover;
		this.offlineTurnover = offlineTurnover;
		this.stockScore = stockScore;
		this.freezeStockScore = freezeStockScore;
		this.totalStockScore = totalStockScore;
		this.totalGiveStockScore = totalGiveStockScore;
		this.freezeKaiyuan = freezeKaiyuan;
		this.kaiyuanTurnover = kaiyuanTurnover;
		this.totalKaiyuanTurnover = totalKaiyuanTurnover;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public BigDecimal getOnlineTurnover() {
		return onlineTurnover;
	}

	public void setOnlineTurnover(BigDecimal onlineTurnover) {
		this.onlineTurnover = onlineTurnover;
	}

	public BigDecimal getOfflineTurnover() {
		return offlineTurnover;
	}

	public void setOfflineTurnover(BigDecimal offlineTurnover) {
		this.offlineTurnover = offlineTurnover;
	}

	public BigDecimal getStockScore() {
		return stockScore;
	}

	public void setStockScore(BigDecimal stockScore) {
		this.stockScore = stockScore;
	}

	public BigDecimal getFreezeStockScore() {
		return freezeStockScore;
	}

	public void setFreezeStockScore(BigDecimal freezeStockScore) {
		this.freezeStockScore = freezeStockScore;
	}

	public BigDecimal getTotalStockScore() {
		return totalStockScore;
	}

	public void setTotalStockScore(BigDecimal totalStockScore) {
		this.totalStockScore = totalStockScore;
	}

	public BigDecimal getTotalGiveStockScore() {
		return totalGiveStockScore;
	}

	public void setTotalGiveStockScore(BigDecimal totalGiveStockScore) {
		this.totalGiveStockScore = totalGiveStockScore;
	}

	public BigDecimal getFreezeKaiyuan() {
		return freezeKaiyuan;
	}

	public void setFreezeKaiyuan(BigDecimal freezeKaiyuan) {
		this.freezeKaiyuan = freezeKaiyuan;
	}

	public BigDecimal getKaiyuanTurnover() {
		return kaiyuanTurnover;
	}

	public void setKaiyuanTurnover(BigDecimal kaiyuanTurnover) {
		this.kaiyuanTurnover = kaiyuanTurnover;
	}

	public BigDecimal getTotalKaiyuanTurnover() {
		return totalKaiyuanTurnover;
	}

	public void setTotalKaiyuanTurnover(BigDecimal totalKaiyuanTurnover) {
		this.totalKaiyuanTurnover = totalKaiyuanTurnover;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AccountShopList [userPhone=" + userPhone + ", userType=" + userType + ", onlineTurnover="
				+ onlineTurnover + ", offlineTurnover=" + offlineTurnover + ", stockScore=" + stockScore
				+ ", freezeStockScore=" + freezeStockScore + ", totalStockScore=" + totalStockScore
				+ ", totalGiveStockScore=" + totalGiveStockScore + ", freezeKaiyuan=" + freezeKaiyuan
				+ ", kaiyuanTurnover=" + kaiyuanTurnover + ", totalKaiyuanTurnover=" + totalKaiyuanTurnover
				+ ", beginTime=" + beginTime + ", endTime=" + endTime + "]";
	}

}
