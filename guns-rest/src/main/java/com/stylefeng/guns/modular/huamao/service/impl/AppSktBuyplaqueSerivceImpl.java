package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktBuyplaqueMapper;
import com.stylefeng.guns.modular.huamao.model.SktBuyplaque;
import com.stylefeng.guns.modular.huamao.service.IAppSktBuyplaqueSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppSktBuyplaqueSerivceImpl extends ServiceImpl<SktBuyplaqueMapper,SktBuyplaque> implements IAppSktBuyplaqueSerivce {
    @Autowired
    private SktBuyplaqueMapper sktBuyplaqueMapper;

    @Override
    public SktBuyplaque selectByOrderNo(Map<String, Object> orderWhere) {
        return sktBuyplaqueMapper.selectByOrderNo(orderWhere);
    }
}
