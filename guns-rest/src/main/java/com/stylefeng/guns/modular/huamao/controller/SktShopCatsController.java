package com.stylefeng.guns.modular.huamao.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.huamao.model.SktShopCats;
import com.stylefeng.guns.modular.huamao.result.Result;
import com.stylefeng.guns.modular.huamao.service.ISktShopCatsService;

/**
 * 店铺分类表控制器
 *
 * @author fengshuonan
 * @Date 2018-05-29 17:28:10
 */
@CrossOrigin
@Controller
@RequestMapping("/sktShopCats")
public class SktShopCatsController extends BaseController {

    private String PREFIX = "/system/sktShopCats/";

    @Autowired
    private ISktShopCatsService sktShopCatsService;

    /**
     * 跳转到店铺分类表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "sktShopCats.html";
    }

    /**
     * 跳转到添加店铺分类表
     */
    @RequestMapping("/sktShopCats_add")
    public String sktShopCatsAdd() {
        return PREFIX + "sktShopCats_add.html";
    }

    /**
     * 跳转到修改店铺分类表
     */
    @RequestMapping("/sktShopCats_update/{sktShopCatsId}")
    public String sktShopCatsUpdate(@PathVariable Integer sktShopCatsId, Model model) {
        SktShopCats sktShopCats = sktShopCatsService.selectById(sktShopCatsId);
        model.addAttribute("item",sktShopCats);
        LogObjectHolder.me().set(sktShopCats);
        return PREFIX + "sktShopCats_edit.html";
    }

    /**
     * 获取店铺分类表列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return sktShopCatsService.selectList(null);
    }

    /**
     * 新增店铺分类表
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(SktShopCats sktShopCats) {
        sktShopCatsService.insert(sktShopCats);
        return SUCCESS_TIP;
    }

    /**
     * 删除店铺分类表
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer sktShopCatsId) {
        sktShopCatsService.deleteById(sktShopCatsId);
        return SUCCESS_TIP;
    }

    /**
     * 修改店铺分类表
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SktShopCats sktShopCats) {
        sktShopCatsService.updateById(sktShopCats);
        return SUCCESS_TIP;
    }

    /**
     * 店铺分类表详情
     */
    @RequestMapping(value = "/detail/{sktShopCatsId}")
    @ResponseBody
    public Object detail(@PathVariable("sktShopCatsId") Integer sktShopCatsId) {
        return sktShopCatsService.selectById(sktShopCatsId);
    }
    
    /*****************首页****************/
    /**
     * 查询商铺分类列表
     */
    @RequestMapping(value = "/selectList")
	@ResponseBody
	public Result selectByGoodsInfo(@RequestParam(value="shopId") Integer shopId){
    	List<SktShopCats> list = sktShopCatsService.selectShopIdList(shopId,0);
    	List<SktShopCats> list2 = new ArrayList<>();
    	for(SktShopCats shopCats : list){
    		list2.add(shopCats);
    		List<SktShopCats> selectShopIdList = sktShopCatsService.selectShopIdList(shopId,shopCats.getCatId());
    		list2.addAll(selectShopIdList);
    	}
    	if(ToolUtil.isEmpty(list)){
    		return Result.EEROR("没有查询数据!");
    	}
    	return Result.OK(list2);
    }
    
    
    /**
	 * 新增商品分类
	 */
	@RequestMapping(value = "/addList")
	@ResponseBody
	public Result addList(@RequestBody List<SktShopCats> sktShopCats) {
		for (SktShopCats sktShopCats1 : sktShopCats){
			sktShopCats1.setCreateTime(new Date());
			sktShopCats1.setDataFlag(1);
		}
		boolean i = sktShopCatsService.insertOrUpdateBatch(sktShopCats);
		return i==true?Result.OK(null):Result.EEROR("新增商品分类失败！");
	}
	
	
	
	/**
     * 删除商品分类
     */
    @RequestMapping(value = "/deleteCats")
    @ResponseBody
    public Result deleteCats(@RequestParam Integer shopCatsId) {
    	List<SktShopCats> goodsCatslist = sktShopCatsService.selectByParentId(shopCatsId);
    	 if(goodsCatslist!=null && goodsCatslist.size()>0){
    		 for(SktShopCats shopCat:goodsCatslist){
    			 List<SktShopCats> goodsCatslist2 = sktShopCatsService.selectByParentId(shopCat.getCatId()); 
	    		 if(goodsCatslist2!=null && goodsCatslist2.size()>0){
	    			 sktShopCatsService.deleteById(shopCat.getCatId());
	    		 }
	    		 sktShopCatsService.deleteById(shopCat.getCatId()); 
    		 }    		 
    	 }
        boolean deleteById = sktShopCatsService.deleteById(shopCatsId);
        return deleteById==true?Result.OK(null):Result.EEROR("删除商品分类失败！");
    }
}
