package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 代理公司提现记录表
 * </p>
 *
 * @author gxz123
 * @since 2018-04-20
 */
@TableName("skt_agents_draws")
public class AgentsDraws extends Model<AgentsDraws> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "drawId", type = IdType.AUTO)
    private Integer drawId;
    /**
     * 提现单号
     */
    private String drawNo;
    /**
     * 代理公司ID
     */
    private Integer agentId;
    /**
     * 银行卡表ID
     */
    private Integer bankCardId;
    /**
     * 到账金额
     */
    private BigDecimal money;
    /**
     * 手续费+税率
     */
    private BigDecimal fee;
    /**
     * 提现手续费比例 %
     */
    private BigDecimal ratio;
    /**
     * 提现税率比例 %
     */
    private BigDecimal taxratio;
    /**
     * 提现状态 -2受理失败 -1审核拒绝 0审核中 2受理中 3受理成功 4线下打款 
     */
    private Integer cashSatus;
    /**
     * 预计到账时间
     */
    private Date getTime;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 拒绝理由
     */
    private String checkRemark;
    /**
     * 审核员工ID
     */
    private Integer checkStaffId;
    /**
     * 审核时间
     */
    private Date checkTime;
    /**
     * 0：非手动处理；2：手动处理成功；3：手动处理失败
     */
    private Integer specialType;


    public Integer getDrawId() {
        return drawId;
    }

    public void setDrawId(Integer drawId) {
        this.drawId = drawId;
    }

    public String getDrawNo() {
        return drawNo;
    }

    public void setDrawNo(String drawNo) {
        this.drawNo = drawNo;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(Integer bankCardId) {
        this.bankCardId = bankCardId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public BigDecimal getTaxratio() {
        return taxratio;
    }

    public void setTaxratio(BigDecimal taxratio) {
        this.taxratio = taxratio;
    }

    public Integer getCashSatus() {
        return cashSatus;
    }

    public void setCashSatus(Integer cashSatus) {
        this.cashSatus = cashSatus;
    }

    public Date getGetTime() {
        return getTime;
    }

    public void setGetTime(Date getTime) {
        this.getTime = getTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }

    public Integer getCheckStaffId() {
        return checkStaffId;
    }

    public void setCheckStaffId(Integer checkStaffId) {
        this.checkStaffId = checkStaffId;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Integer getSpecialType() {
        return specialType;
    }

    public void setSpecialType(Integer specialType) {
        this.specialType = specialType;
    }

    @Override
    protected Serializable pkVal() {
        return this.drawId;
    }

    @Override
    public String toString() {
        return "AgentsDraws{" +
        "drawId=" + drawId +
        ", drawNo=" + drawNo +
        ", agentId=" + agentId +
        ", bankCardId=" + bankCardId +
        ", money=" + money +
        ", fee=" + fee +
        ", ratio=" + ratio +
        ", taxratio=" + taxratio +
        ", cashSatus=" + cashSatus +
        ", getTime=" + getTime +
        ", createTime=" + createTime +
        ", checkRemark=" + checkRemark +
        ", checkStaffId=" + checkStaffId +
        ", checkTime=" + checkTime +
        ", specialType=" + specialType +
        "}";
    }
}
