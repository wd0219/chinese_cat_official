package com.stylefeng.guns.modular.huamao.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 用户升级角色订单表
 * </p>
 *
 * @author slt123
 * @since 2018-04-14
 */
@TableName("skt_users_upgrade")
public class UsersUpgrade extends Model<UsersUpgrade> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "orderId", type = IdType.AUTO)
    private Integer orderId;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户Id
     */
    private Integer userId;
    /**
     * 升级前角色 0普通 1主管
     */
    private Integer preRole;
    /**
     * 升级后角色 1主管 2经理
     */
    private Integer afterRole;
    /**
     * 支付方式支付方式 1:线下现金支付 2 现金账户支付 3开元宝支付 4第三方支付 5混合支付 6开元宝营业额
     */
    private Integer payType;
    /**
     * 订单总金额 (realMoney+cash+kaiyuan)
     */
    private BigDecimal totalMoney;
    /**
     * 实际支付金额 通过支付公司现金支付的金额
     */
    private BigDecimal realMoney;
    /**
     * 平台现金账户金额
     */
    private BigDecimal cash;
    /**
     * 开元宝金额 1个开元宝=1分钱
     */
    private BigDecimal kaiyuan;
    /**
     * 使用开元宝支付的手续费和税率
     */
    private BigDecimal kaiyuanFee;
    /**
     * 订单备注
     */
    private String orderRemarks;
    /**
     * 下单时间
     */
    private Date createTime;
    /**
     * 升级时间 成为主管或经理的时间
     */
    private Date upgradeTime;
    /**
     * 有效标志 1有效 0删除
     */
    private Integer dataFlag;
    /**
     * 状态 0未支付 1已付款未处理 2已取消 3已处理 9处理中
     */
    private Integer status;
    /**
     * 定时器处理时间
     */
    private Date batTime;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPreRole() {
        return preRole;
    }

    public void setPreRole(Integer preRole) {
        this.preRole = preRole;
    }

    public Integer getAfterRole() {
        return afterRole;
    }

    public void setAfterRole(Integer afterRole) {
        this.afterRole = afterRole;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getKaiyuan() {
        return kaiyuan;
    }

    public void setKaiyuan(BigDecimal kaiyuan) {
        this.kaiyuan = kaiyuan;
    }

    public BigDecimal getKaiyuanFee() {
        return kaiyuanFee;
    }

    public void setKaiyuanFee(BigDecimal kaiyuanFee) {
        this.kaiyuanFee = kaiyuanFee;
    }

    public String getOrderRemarks() {
        return orderRemarks;
    }

    public void setOrderRemarks(String orderRemarks) {
        this.orderRemarks = orderRemarks;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpgradeTime() {
        return upgradeTime;
    }

    public void setUpgradeTime(Date upgradeTime) {
        this.upgradeTime = upgradeTime;
    }

    public Integer getDataFlag() {
        return dataFlag;
    }

    public void setDataFlag(Integer dataFlag) {
        this.dataFlag = dataFlag;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getBatTime() {
        return batTime;
    }

    public void setBatTime(Date batTime) {
        this.batTime = batTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

    @Override
    public String toString() {
        return "UsersUpgrade{" +
        "orderId=" + orderId +
        ", orderNo=" + orderNo +
        ", userId=" + userId +
        ", preRole=" + preRole +
        ", afterRole=" + afterRole +
        ", payType=" + payType +
        ", totalMoney=" + totalMoney +
        ", realMoney=" + realMoney +
        ", cash=" + cash +
        ", kaiyuan=" + kaiyuan +
        ", kaiyuanFee=" + kaiyuanFee +
        ", orderRemarks=" + orderRemarks +
        ", createTime=" + createTime +
        ", upgradeTime=" + upgradeTime +
        ", dataFlag=" + dataFlag +
        ", status=" + status +
        ", batTime=" + batTime +
        "}";
    }
}
