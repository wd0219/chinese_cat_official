package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktShopScoresMapper;
import com.stylefeng.guns.modular.huamao.model.SktShopScores;
import com.stylefeng.guns.modular.huamao.model.SktShops;
import com.stylefeng.guns.modular.huamao.service.ISktShopScoresService;
import org.springframework.stereotype.Service;

@Service
public class SktShopScoresServiceImpl extends ServiceImpl<SktShopScoresMapper, SktShopScores> implements ISktShopScoresService {
}
