package com.stylefeng.guns.modular.huamao.dao;

import com.stylefeng.guns.modular.huamao.model.Shopping;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物券表 Mapper 接口
 * </p>
 *
 * @author wudi123
 * @since 2018-06-19
 */
public interface ShoppingMapper extends BaseMapper<Shopping> {
    public List<Map<String,Object>> selectShopping(Map<String,Object> map);

    Shopping selectSumShopping(Map<String, Object> map);

}
