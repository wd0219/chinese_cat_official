package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktOrderRefundsMapper;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefunds;
import com.stylefeng.guns.modular.huamao.service.IAppOrderRefundsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AppOrderRefundsServiceImpl extends ServiceImpl<SktOrderRefundsMapper,SktOrderRefunds> implements IAppOrderRefundsService {
    @Autowired
    private SktOrderRefundsMapper sktOrderRefundsMapper;

    @Override
    public SktOrderRefunds selectOrderRefundsByOrderId(Map<String,Object> map) {
        return sktOrderRefundsMapper.selectOrderRefundsByOrderId(map);
    }
}
