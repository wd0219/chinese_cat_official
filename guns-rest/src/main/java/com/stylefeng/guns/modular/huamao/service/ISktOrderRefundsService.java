package com.stylefeng.guns.modular.huamao.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefunds;
import com.stylefeng.guns.modular.huamao.model.SktOrderRefundsList;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单退款记录表 服务类
 * </p>
 *
 * @author caody123
 * @since 2018-04-19
 */
public interface ISktOrderRefundsService extends IService<SktOrderRefunds> {
	/**
	 * 查询退款订单信息列表
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderRefundList(SktOrderRefundsList sktOrderRefundsList);
	/**
	 * 查询退款订单信息
	 * @param paramMap
	 * @return
	 */
	public Object selectOrderRefundsToRefund(Map<String, Object> param);
	/**
	 * 审核通过退款订单信息
	 * @param paramMap
	 * @return
	 */
	public Object examineOrderRefundsToRefund(SktOrderRefunds sktOrderRefunds, Integer staffId, String orderNo);
	/**
	 * 通过退款订单编号查询
	 * @param sktOrderRefundsId
	 * @return
	 */
	public List<Map<String, Object>> selectOrdersDetail(Integer sktOrderRefundsId);
	/**
	 * 根据订单id查询商品信息
	 * @param orderId
	 * @return
	 */
	public List<Map<String, Object>> sktOrderRefundsSelectGoodsDetail(Integer orderId);

	public JSONObject applayRefundsOrders(SktOrderRefunds sktOrderRefunds);

	public JSONObject echoRefundsOrders(SktOrderRefunds sktOrderRefunds);


    Integer appShopRefund(String remark, Integer type, String orderNo);
}
