package com.stylefeng.guns.modular.huamao.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.huamao.dao.SktLogSmsMapper;
import com.stylefeng.guns.modular.huamao.model.SktLogSms;
import com.stylefeng.guns.modular.huamao.service.ISktLogSmsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 短信发送记录表 服务实现类
 * </p>
 *
 * @author caody
 * @since 2018-06-08
 */
@Service
public class SktLogSmsServiceImpl extends ServiceImpl<SktLogSmsMapper, SktLogSms> implements ISktLogSmsService {
	
}
