package com.stylefeng.guns.modular.huamao.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.huamao.model.LogScoreSpecial;

import java.util.List;
import java.util.Map;

public interface IAppLogScoreSpecialService extends IService<LogScoreSpecial> {

    List<LogScoreSpecial> selectLogScoreSpecialByUserId(Map<String, Object> map);
}
