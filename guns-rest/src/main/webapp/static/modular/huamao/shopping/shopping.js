/**
 * 购物券管理初始化
 */
var Shopping = {
    id: "ShoppingTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Shopping.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '自增ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '用户身份  1会员   2代理', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '目标用户ID', field: 'dataId', visible: true, align: 'center', valign: 'middle'},
            {title: '对应订单号', field: 'orderNo', visible: true, align: 'center', valign: 'middle'},
            {title: '金额', field: 'score', visible: true, align: 'center', valign: 'middle'},
            {title: '备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            {title: '有效状态 1有效 0删除', field: 'dataFlag', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'},
            {title: '购物券过期时间', field: 'overdueTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Shopping.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Shopping.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加购物券
 */
Shopping.openAddShopping = function () {
    var index = layer.open({
        type: 2,
        title: '添加购物券',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/shopping/shopping_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看购物券详情
 */
Shopping.openShoppingDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '购物券详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/shopping/shopping_update/' + Shopping.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除购物券
 */
Shopping.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/shopping/delete", function (data) {
            Feng.success("删除成功!");
            Shopping.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("shoppingId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询购物券列表
 */
Shopping.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Shopping.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Shopping.initColumn();
    var table = new BSTable(Shopping.id, "/shopping/list", defaultColunms);
    table.setPaginationType("client");
    Shopping.table = table.init();
});
