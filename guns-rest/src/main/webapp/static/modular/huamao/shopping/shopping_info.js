/**
 * 初始化购物券详情对话框
 */
var ShoppingInfoDlg = {
    shoppingInfoData : {}
};

/**
 * 清除数据
 */
ShoppingInfoDlg.clearData = function() {
    this.shoppingInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ShoppingInfoDlg.set = function(key, val) {
    this.shoppingInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ShoppingInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ShoppingInfoDlg.close = function() {
    parent.layer.close(window.parent.Shopping.layerIndex);
}

/**
 * 收集数据
 */
ShoppingInfoDlg.collectData = function() {
    this
    .set('id')
    .set('type')
    .set('dataId')
    .set('orderNo')
    .set('score')
    .set('remark')
    .set('dataFlag')
    .set('createTime')
    .set('overdueTime');
}

/**
 * 提交添加
 */
ShoppingInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/shopping/add", function(data){
        Feng.success("添加成功!");
        window.parent.Shopping.table.refresh();
        ShoppingInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.shoppingInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ShoppingInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/shopping/update", function(data){
        Feng.success("修改成功!");
        window.parent.Shopping.table.refresh();
        ShoppingInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.shoppingInfoData);
    ajax.start();
}

$(function() {

});
