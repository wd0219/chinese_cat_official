//现金记录分页查询
function cashListByPage(p){
    $('#loading').show();
    var params = {};
    params = ZZHT.getParams('.u-query');
    params.page = p;
//  $.post(ZZHT.U('home/log/cashListByPage'),params,function(data,textStatus){
//      $('#loading').hide();
//      var json = ZZHT.toJson(data);
//      $('.j-order-row').remove();
//      if(json.status==1){
//          json = json.data;
//          var gettpl = document.getElementById('tblist').innerHTML;
//          laytpl(gettpl).render(json.data, function(html){
//              $(html).insertAfter('#loadingBdy');
//              $('.gImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.OSS + ZZHT.conf.GOODS_LOGO});
//          });
//          if(json.last_page>1){
//              laypage({
//                  cont: 'pager',
//                  pages:json.last_page,
//                  curr: json.current_page,
//                  skin: '#ed5d2a',
//                  groups: 3,
//                  jump: function(e, first){
//                      if(!first){
//                          cashListByPage(e.curr);
//                      }
//                  }
//              });
//          }
//      }
//  });
}

//待发现金记录分页查询
function cashListFreezeByPage(p){
    $('#loading').show();
    var params = {};
    params = ZZHT.getParams('.u-query');
    params.page = p;
    $.post(ZZHT.U('home/log/cashListFreezeByPage'),params,function(data,textStatus){
        $('#loading').hide();
        var json = ZZHT.toJson(data);
        $('.j-order-row').remove();
        if(json.status==1){
            json = json.data;
            var gettpl = document.getElementById('tblist').innerHTML;
            laytpl(gettpl).render(json.data, function(html){
                $(html).insertAfter('#loadingBdy');
                $('.gImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.OSS + ZZHT.conf.GOODS_LOGO});
            });
            if(json.last_page>1){
                laypage({
                    cont: 'pager',
                    pages:json.last_page,
                    curr: json.current_page,
                    skin: '#ed5d2a',
                    groups: 3,
                    jump: function(e, first){
                        if(!first){
                            cashListFreezeByPage(e.curr);
                        }
                    }
                });
            }
        }
    });
}

//积分记录分页查询
function scoreListByPage(p){
    $('#loading').show();
    var params = {};
    params = ZZHT.getParams('.u-query');
    params.page = p;
    $.post(ZZHT.U('home/log/scoreListByPage'),params,function(data,textStatus){
        $('#loading').hide();
        var json = ZZHT.toJson(data);
        $('.j-order-row').remove();
        if(json.status==1){
            json = json.data;
            var gettpl = document.getElementById('tblist').innerHTML;
            laytpl(gettpl).render(json.data, function(html){
                $(html).insertAfter('#loadingBdy');
                $('.gImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.OSS + ZZHT.conf.GOODS_LOGO});
            });
            if(json.last_page>1){
                laypage({
                    cont: 'pager',
                    pages:json.last_page,
                    curr: json.current_page,
                    skin: '#ed5d2a',
                    groups: 3,
                    jump: function(e, first){
                        if(!first){
                            scoreListByPage(e.curr);
                        }
                    }
                });
            }
        }
    });
}

//待发积分记录分页查询
function scoreFreezeListByPage(p){
    $('#loading').show();
    var params = {};
    params = ZZHT.getParams('.u-query');
    params.page = p;
    $.post(ZZHT.U('home/log/scoreFreezeListByPage'),params,function(data,textStatus){
        $('#loading').hide();
        var json = ZZHT.toJson(data);
        $('.j-order-row').remove();
        if(json.status==1){
            json = json.data;
            var gettpl = document.getElementById('tblist').innerHTML;
            laytpl(gettpl).render(json.data, function(html){
                $(html).insertAfter('#loadingBdy');
                $('.gImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.OSS + ZZHT.conf.GOODS_LOGO});
            });
            if(json.last_page>1){
                laypage({
                    cont: 'pager',
                    pages:json.last_page,
                    curr: json.current_page,
                    skin: '#ed5d2a',
                    groups: 3,
                    jump: function(e, first){
                        if(!first){
                            scoreFreezeListByPage(e.curr);
                        }
                    }
                });
            }
        }
    });
}

//分红记录分页查询
function abonusListByPage(p){
    $('#loading').show();
    var params = {};
    params = ZZHT.getParams('.u-query');
    params.page = p;
    $.post(ZZHT.U('home/log/abonusListByPage'),params,function(data,textStatus){
        $('#loading').hide();
        var json = ZZHT.toJson(data);
        $('.j-order-row').remove();
        if(json.status==1){
            json = json.data;
            var gettpl = document.getElementById('tblist').innerHTML;
            laytpl(gettpl).render(json.data, function(html){
                $(html).insertAfter('#loadingBdy');
                $('.gImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.OSS + ZZHT.conf.GOODS_LOGO});
            });
            if(json.last_page>1){
                laypage({
                    cont: 'pager',
                    pages:json.last_page,
                    curr: json.current_page,
                    skin: '#ed5d2a',
                    groups: 3,
                    jump: function(e, first){
                        if(!first){
                            abonusListByPage(e.curr);
                        }
                    }
                });
            }
        }
    });
}

//特别积分记录分页查询
function scoreSpecialListByPage(p){
    $('#loading').show();
    var params = {};
    params = ZZHT.getParams('.u-query');
    params.page = p;
    $.post(ZZHT.U('home/log/scoreSpecialListByPage'),params,function(data,textStatus){
        $('#loading').hide();
        var json = ZZHT.toJson(data);
        $('.j-order-row').remove();
        if(json.status==1){
            json = json.data;
            var gettpl = document.getElementById('tblist').innerHTML;
            laytpl(gettpl).render(json.data, function(html){
                $(html).insertAfter('#loadingBdy');
                $('.gImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.OSS + ZZHT.conf.GOODS_LOGO});
            });
            if(json.last_page>1){
                laypage({
                    cont: 'pager',
                    pages:json.last_page,
                    curr: json.current_page,
                    skin: '#ed5d2a',
                    groups: 3,
                    jump: function(e, first){
                        if(!first){
                            scoreSpecialListByPage(e.curr);
                        }
                    }
                });
            }
        }
    });
}

//华宝记录分页查询
function kaiyuanListByPage(p){
    $('#loading').show();
    var params = {};
    params = ZZHT.getParams('.u-query');
    params.page = p;
    $.post(ZZHT.U('home/log/kaiyuanListByPage'),params,function(data,textStatus){
        $('#loading').hide();
        var json = ZZHT.toJson(data);
        $('.j-order-row').remove();
        if(json.status==1){
            json = json.data;
            var gettpl = document.getElementById('tblist').innerHTML;
            laytpl(gettpl).render(json.data, function(html){
                $(html).insertAfter('#loadingBdy');
                $('.gImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.OSS + ZZHT.conf.GOODS_LOGO});
            });
            if(json.last_page>1){
                laypage({
                    cont: 'pager',
                    pages:json.last_page,
                    curr: json.current_page,
                    skin: '#ed5d2a',
                    groups: 3,
                    jump: function(e, first){
                        if(!first){
                            kaiyuanListByPage(e.curr);
                        }
                    }
                });
            }
        }
    });
}