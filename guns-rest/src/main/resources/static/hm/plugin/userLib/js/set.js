$(function() {
	"use strict";
	//时间选择
	//	var start = {
	//		elem: '#startTime',
	//		format: 'YYYY-MM-DD',
	//		min: '2014-01-01', //设定最小日期为当前日期
	//		max: laydate.now(),
	//		istoday: false,
	//		choose: function(datas) {
	//			end.min = datas; //开始日选好后，重置结束日的最小日期
	//			end.start = datas; //将结束日的初始值设定为开始日
	//		}
	//	}
	//	var end = {
	//		elem: '#endTime',
	//		format: 'YYYY-MM-DD',
	//		min: '2014-01-01',
	//		max: laydate.now(), //设定最小日期为当前日期
	//		istoday: false,
	//		choose: function(datas) {
	//			start.max = datas; //结束日选好后，重置开始日的最大日期
	//		}
	//	};
	//	laydate(start);
	//	laydate(end);
	$('#start').on('change', function() {
		console.log('aaaaaaaa');
		$('#startTime').val($('#start').val());
	});
	$('#end').on('change', function() {
		$('#endTime').val($('#end').val());
	});

	$(".ui-checkbox").click(function(e) {
		var chk = $(this).children("input");
		var check = chk.attr("checked");
		if(check) return;
		$(".ui-checkbox").children("input").attr("checked", false);
		chk.attr("checked", check ? false : true);
	});
	$(".ui-checkbox2").click(function(e) {
		var chk = $(this).children("input");
		var check = chk.attr("checked");
		if(check) return;
		$(".ui-checkbox2").children("input").attr("checked", false);
		chk.attr("checked", check ? false : true);
	});
	$('img').lazyload({
		effect: "fadeIn",
		failurelimit: 10,
		skip_invisible: false,
		threshold: 100
	});
});
//店铺设置
//店铺修改
function editStore() {
	$('#startTime').val($('#start').val());
	$('#endTime').val($('#end').val());
	//保存
	$('#setform').validator({
		rules: {
			checkMobile: function(element) {
				return /^1[34578]\d{9}$/.test(element.value) || '联系电话输入有误';
			},
			checkRange: function(element) {
				return /^([1-4][0-9]{0,2}|500|[1-9])$/.test(element.value) || '积分赠送比例为0到500';
			}
		},
		fields: {
			'shopName': '简称:required;',
			'linkman': '联系人:required;',
			'telephone': '联系电话:required;checkMobile;',
			'address': '经营地址:required;',
			'lng': '经纬度:required;',
			'lat': '经纬度:required;',
			'scoreRatio': '积分赠送比例:required;checkRange;'
		},
		// 表单验证通过后，ajax提交
		valid: function(form) {
			if($('#selectType option:checked').val() == 0) {
				ZZHT.msg('请选择行业类型', {
					icon: 2
				});
				return false;
			}
			if(!$('#storeImg1').val() || !$('#storeImg2').val() || !$('#storeImg3').val()) {
				ZZHT.msg('必须上传三张展示图片', {
					icon: 2
				});
				return false;
			}
			var ll = ZZHT.msg('数据处理中，请稍候...');
			var params = getParams('.ipt');
			params['shopId'] = shopId;
			
			console.log(params);
			
			$.post(SURL + 'sktShops/updateShops', params, function(data) {
				layer.close(ll);
				console.log(data)
				if(data.messageCode==200){
					ZZHT.msg('提交成功',{icon:6})
				}
			});
		}
	});

	//图片上传
	for(var i = 0; i < 4; i++) {
		ZZHT.upload({
			pick: '#adFilePicker' + i,
			formData: {
				dir: 'users'
			},
			accept: {
				extensions: 'gif,jpg,jpeg,bmp,png',
				mimeTypes: 'image/*'
			},
			callback: function(f) {
				var json = f;
				var id = this.pick;
				//将上传的图片路径赋给全局变量
				$(id).next('input').val(json.entity);
				$(id).parent('div').prev('div').html('<img src="' + json.entity + '"  width="364" height="100" />');
			}
		});
	}
}