$(function() {
	"use strict";
	//关闭
	$('#J_closed').on('click', function() {
		$(this).parent('.advertising').remove();
	});
	//复制
	$('#J_copy').on('click',function(){
		var url = document.getElementById("url"),
			urlVale = $('input[id="url"]').val();
		if(!!urlVale) {
			url.select();
			document.execCommand("Copy");
			alert("已复制好，可贴粘。");
		}else {
			ZZHT.msg('请输入url',{icon:2});
        	return false;
		}
	});
});

function shareInformation(){
    $.ajax({
        url: ZZHT.U('home/share/shareInformation'),
        type: 'post',
        data: {},
        dataType: 'json',
        success: function(data) {
            var json = ZZHT.toJson(data);
            if (json.status == 1) {
                document.getElementById("url").value = json.data.qrCode;
                document.getElementById("message").innerHTML = json.data.write;
                //二维码
                //参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
                var a = qrcode(10, 'M');
                var url = json.data.qrCode;
                a.addData(url);
                a.make();
                $('#payQr').html(a.createImgTag());
                //ZZHT.msg("操作成功", {icon: 1});
                //location.href = ZZHT.U('cards/index');
            } else {
               // ZZHT.msg(json.msg, {icon: 2});
            }
        }
    });
}

//分享记录分页查询
function shareListByPage(p){
    $('#loading').show();
    var params = {};
    params = ZZHT.getParams('.u-query');
    params.page = p;
    $.post(ZZHT.U('home/share/shareListByPage'),params,function(data,textStatus){
        $('#loading').hide();
        var json = ZZHT.toJson(data);
        $('.j-order-row').remove();
        if(json.status==1){
            json = json.data;
            var gettpl = document.getElementById('tblist').innerHTML;
            laytpl(gettpl).render(json.data, function(html){
                $(html).insertAfter('#loadingBdy');
                $('.gImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.OSS + ZZHT.conf.GOODS_LOGO});
            });
            if(json.last_page>1){
                laypage({
                    cont: 'pager',
                    pages:json.last_page,
                    curr: json.current_page,
                    skin: '#ed5d2a',
                    groups: 3,
                    jump: function(e, first){
                        if(!first){
                            shareListByPage(e.curr);
                        }
                    }
                });
            }
        }
    });
}