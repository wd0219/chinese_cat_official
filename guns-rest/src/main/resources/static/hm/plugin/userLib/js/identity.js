function initGrid() {
	for(var i = 1; i < 4; i++) {
		ZZHT.upload({
			pick: '#adFilePicker' + i,
			formData: {
				dir: 'users'
			},
			accept: {
				extensions: 'gif,jpg,jpeg,bmp,png',
				mimeTypes: 'image/*'
			},
			callback: function(f) {
				var json = ZZHT.toJson(f);
				if(json.status == 1) {
					var id = this.pick;
					$('#uploadMsg').empty().hide();
					//将上传的图片路径赋给全局变量
					$(id).next('input').val(json.savePath + json.thumb);
					$(id).parent('div').prev('div').html('<img src="' + ZZHT.conf.OSS + '/' + json.savePath + json.thumb + '"  height="152" />');
				} else {
					ZZHT.msg(json.msg, {
						icon: 2
					});
				}
			}
		});
	}
}

//处理快速认证数据
function veShimingForm() {

	$('#shimingForm').validator({
		rules: {
			checkID: function(element) {
				return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(element.value) || '身份证号输入有误';
			},
			checkMobile: function(element) {
				return /^1[34578]\d{9}$/.test(element.value) || '手机号输入有误';
			},
			checkBank: function(element) {
				return /^([1-9]{1})(\d{15}|\d{18})$/.test(element.value) || '银行卡号输入有误';
			},
		},
		fields: {
			'trueName': '姓名:required',
			'cardID': '身份证号:required;checkID',
			'phone': '预留手机号:required;checkMobile',
			'accNo': '银行卡号:required;checkBank',
			'areaId1': '所属地区:required;'
		},
		valid: function(form) {
			var ll = ZZHT.msg('数据处理中，请稍候...');
			var params = ZZHT.getParams('.j-ipt');
			params['cityId'] = $('select[level="1"]').val();
			params['areaId'] = $('select[level="2"]').val();
			if(params['areaId1'] == -1 || !params['cityId'] || !params['areaId']) {
				ZZHT.msg('请选择所属地区', {
					icon: 2
				});
				return;
			}
			if(params['bankId'] == -1) {
				ZZHT.msg('请选择开户银行', {
					icon: 2
				});
				return;
			}
			params.userId = localStorage.userId;
			params.provinceId = params.areaId1;
			delete params['areaId1_'+params.provinceId];
			delete params['areaId1_'+params.provinceId+'_'+params.cityId];
			delete params.areaId1;
			console.log(params);
			$.get(SURL+'usersBankcards/quickApproveBrank', params, function(data) {
				layer.close(ll);
				console.log(data);
//				if(json.status > 0) {
//					ZZHT.msg(json.msg, {
//						icon: 1
//					});
//					var backUrl = $('#backUrl').val();
//					if(backUrl != "") {
//						location.href = backUrl;
//					} else {
//						location.href = ZZHT.U('home/account/index');
//					}
//				} else {
//					ZZHT.msg(json.msg, {
//						icon: 2
//					});
//				}
			});
		}
	});
}
//处理人工审核认证数据
function vePersonForm() {
	for(var i = 1; i < 4; i++) {
		ZZHT.upload({
			pick: '#adFilePicker' + i,
			formData: {
				dir: 'users'
			},
			accept: {
				extensions: 'gif,jpg,jpeg,bmp,png',
				mimeTypes: 'image/*'
			},
			callback: function(f) {
				var json = f;
				console.log(f);
				if(json.entity) {
					var id = this.pick;
					$('#uploadMsg').empty().hide();
					//将上传的图片路径赋给全局变量
					$(id).next('input').val(json.entity);
					$(id).parent('div').prev('div').html('<img src="'+json.entity+'"  height="152" />');
				} else {
					ZZHT.msg(json.message, {
						icon: 2
					});
				}
			}
		});
	}
	$('#personForm').validator({//提交认证
		rules: {
			checkID: function(element) {
				return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(element.value) || '身份证号输入有误';
			}
		},
		fields: {
			'cardID': '身份证号:required;checkID',
		},
		valid: function(form) {
			if(!$('#cardUrl').val()){
				layer.msg('请上传身份证正面图片');
				return;
			}
			if(!$('#cardBackUrl').val()){
				layer.msg('请上传身份证反面图片');
				return;
			}
			if(!$('#handCardUrl').val()){
				layer.msg('请上传手持身份证图片');
				return;
			}
			
			var ll = ZZHT.msg('数据处理中，请稍候...', {
			        time: 60000
			    });
			var params = ZZHT.getParams('.ipt');
			params.userId = localStorage.userId;
			console.log(params);
			$.get(SURL+'usersRealname/addUserRealName', params, function(data) {
				layer.close(ll);
				console.log(data);
				layer.msg(data.message);
				if(data.messageCode==200){
					window.location.href="5.html";
				}
			});
		}
	});
}