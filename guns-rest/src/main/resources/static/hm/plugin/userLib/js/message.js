
function batchDel() {//批量删除
	var ids = ZZHT.getChks('.chk');
	if(ids == '') {
		ZZHT.msg('请选择要删除的消息!', {
			icon: 5
		});
		return;
	}
	ZZHT.confirm({
		content: "您确定要删除该消息吗？",
		yes: function(tips) {
			var load = ZZHT.load({
				msg: '请稍后...'
			});
			remMsg(ids,function(){
				window.location.reload();
			})
		}
	});
}

function batchRead() {//批量设为已读
	var ids = ZZHT.getChks('.chk');
	if(ids == '') {
		ZZHT.msg('请选择处理的消息!', {
			icon: 5
		});
		return;
	}
	ZZHT.confirm({
		content: "您确定要将这些消息标记为已读吗？",
		yes: function(tips) {
			setRead(ids,function(){
				window.location.reload();
			})
		}
	});
}