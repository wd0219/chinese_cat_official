$(function(){
	"use strict";
	$(".ui-checkbox1").click(function(e) {
		var chk = $(this).children("input");
		var check = chk.attr("checked");
        if(check) return;
		$(".ui-checkbox1").children("input").attr("checked", false);
		chk.attr("checked", check ? false : true);
	});
	$(".ui-checkbox2").click(function(e) {
		var chk = $(this).children("input");
		var check = chk.attr("checked");
        if(check) return;
		$(".ui-checkbox2").children("input").attr("checked", false);
		chk.attr("checked", check ? false : true);
	});
});
function depositForm(commonCashDraw,taxRatio,kaiyuanYingyeTX,cash,kaiyuan,kaiyuanTurn) {
	var conf = {
		'commonCashDraw' : commonCashDraw,
		'taxRatio' : taxRatio,
		"kaiyuanYingyeTX" : kaiyuanYingyeTX,
		'kaiyuan' : kaiyuan,
		'cash' : cash,
		'kaiyuanTurn' : kaiyuanTurn
	};
	$('#deposit').validator({
    	rules: {
			checkPrice: function(element) {
				return /^\d+(\.\d{1,2})?$/.test(element.value) || '价格小数后长度必须在1-2位之内'
			},
			checkPwd: function(element) {
				return /^[0-9]{6}$/.test(element.value) || '密码必须为6位数字'
			}
    	},
        fields: {
        	'money1':'本次赎回金额:required;checkPrice',
            'password': '交易密码:required;length[6];checkPwd'
        },
        valid: function(form) {
        	var me = this;
        	var url = SURL+'usersDraws/applyRedeem';//确认赎回借口地址
        	var data = {};
        	data.payPwd = $('input[name=password]').val();
        	data.bankCardId = $('#J_score option:selected').val();
        	data.money = $("input[name=money1]").val();
        	data.taxRatio = conf.taxRatio;
        	
            if(!data.bankCardId) {
                ZZHT.msg('请添加银行卡',{icon:2});
                return;
            }
        	switch($('form').data('name')) {
        		case 'deposit' :       //现金赎回
        			if(parseInt(data.money) > parseInt(conf.cash)) {
	        			ZZHT.msg('现金余额不足',{icon:2});
		        		return;
		        	}
//      			data.expensesrate = conf.commonCashDraw;
        			data.fee = format2Decimal($('input[name=money1]').val() * conf.commonCashDraw * 0.01);
        			data.type = 2;
        			data.getTime = 1;
        			data.ratio = (conf.commonCashDraw-conf.taxRatio).toFixed(2);
        			break;
        		case 'depositKaiyuan' :  //华宝赎回
	        		if(parseInt(data.money) * 100 > parseInt(conf.kaiyuan)) {
	        			ZZHT.msg('华宝余额不足',{icon:2});
		        		return;
		        	}
        			data.ratio = $("input[name=expensesrate][checked=checked]").val();
        			data.fee = parseFloat($('#poundage').html())/100;
        			data.type = 1;
        			var name = $("input[name=expensesrate][checked=checked]").attr('class');
        			var num = name.substr(name.length-1,name.length);
        			if(num ==0){
        				num = 1;
        			}
        			data.getTime = num;
        			break;
        		case 'depositKaiyuanTurn' : //华宝货款赎回
        			if(data.money * 100 > conf.kaiyuanTurn) {
	        			ZZHT.msg('华宝货款余额不足',{icon:2});
		        		return;
		        	}
        			data.ratio = conf.kaiyuanYingyeTX;
        			data.fee = parseFloat($('#poundage').html())/100;
        			data.getTime = 1;
        			data.type = 3;
        			break;		
        	}
        	me.holdSubmit();
        	data.userId = localStorage.userId;
        	console.log(data);
        	$.post(url,data,function(data){
        		me.holdSubmit(false);
        		console.log(data);
        		layer.msg(data.entity.msg);
        		if(data.entity.status==1){
        			ZZHT.msg('申请已提交，等待到账。。。。。。',{icon:2},function(){
        				location.href="01.html";
        			})
        		}
	        })
        }
	});
}

function rechargeForm() {//现金充值
	$('#recharge').validator({
    	rules: {
			checkPrice: function(element) {
				return /^\d+(\.\d{1,2})?$/.test(element.value) || '价格小数后长度必须在1-2位之内'
			}
    	},
        fields: {
        	'pay_money':'充值金额:required;checkPrice'
        },
        valid: function(form) {
        	var me = this;
			// ajax提交表单之前，先禁用submit
			me.holdSubmit();
			var url = ZZHT.U('home/account/rechargeOrder');
	        var money = $('input[name=pay_money]').val();
	        var data = {'cash':money};
	        $.post(url,data,function(data){
	          var json = ZZHT.toJson(data);
	          if(json.status==1){
	          	ZZHT.msg(json.msg,{icon:1},function() {
	          		location.href = json.data.url;
	          	});            
	          } else {
	          	me.holdSubmit(false);
	            ZZHT.msg(json.msg,{icon:2});
	          }
	        })
        }
	});
}
function format2Decimal(num) {
	return Math.ceil(num * 100) / 100;
}