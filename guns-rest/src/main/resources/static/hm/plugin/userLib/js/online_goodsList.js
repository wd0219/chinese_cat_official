//获取商城分类
function getShopCat() {
	$.ajax({
		type: "get",
		url: SURL + "sktShopCats/selectList",
		data: {
			"shopId": shopId
		},
		async:false,
		contentType: 'application/json;charset=UTF-8',
		success: function(res) {
			vm.menu = res.entity;
		}
	});
}

//批量删除
function delList(type) {
	console.log(vm.checkArr);
	if(vm.checkArr.length > 0) {
		layer.confirm('确认删除？', function(index) {
			$.get(SURL + 'goods/deleteBathGoods', {
				goodsId: vm.checkArr
			}, function(data, textStatus) { //获取店铺id
				if(data.messageCode == '200') {
					location.reload();
				} else {
					layer.msg(data.msg);
				}
			});
			layer.close(index);
		})
	} else {
		layer.msg('请先选中商品');
	}
};

//批量设置
function changList(isWhat,type) {
	console.log(vm.checkArr);
	if(vm.checkArr.length > 0) {
		var obj = {};
		obj.goodsId = vm.checkArr;
		if(isWhat == 'isSale') { //批量下架
			obj[isWhat] = type;
		} else {
			obj[isWhat] = 1;
		}
		$.ajax({
			type: "get",
			url: SURL + "goods/update",
			data: obj,
			//contentType:'application/json',
			dataType: 'json',
			success: function(res) {
				location.reload();
			}
		});
	} else {
		layer.msg('请先选中商品');
	}
};

//初始化
function init() {
	$.get(SURL + 'sktShops/findByUserId', {
		userId: localStorage.userId
	}, function(data, textStatus) { //获取店铺id
		shopId = data.entity[0].shopId;
		getList();
		getShopCat();
		initPage();
	});
}

//获取数据列表
function getListByUrl(url,param){
	param.shopId = shopId;
	param.pageNum = pageNum;
	console.log(param);
	$.ajax({ 
		type: "get",
		url: SURL + url,
		async: false,
		data: param,
		timeout: 5000,
		dataType: 'json',
		success: function(res) {
			console.log(res);
			pages = res.page;
			if(res.messageCode == 200) {
				vm.tab = res.entity;
			} else {
				layer.msg(res.message);
			}
		},
		error: function(textStatus) {
			console.error(textStatus);
		},
		complete: function(XMLHttpRequest, status) {
			if(status == 'timeout') {
				layer.msg("网络超时，请刷新", function() {
					location.reload();
				})
			}
		}
	});
}

//全选
function checkAll(_this){
	var all = document.getElementById('all');
	if(all.checked) { //实现反选
		_this.checkArr = [];
		_this.tab.forEach(function(item) {
			_this.checkArr.push(item.goodsId);
		});
	} else { //实现全选
		_this.checkArr = [];
	}
}

//删除
function del(i,id,that){
	var obj = {};
	obj.goodsId = [];
	obj.goodsId.push(id);
	layer.confirm('确认删除？', function(index) {
		$.get(SURL + 'goods/deleteBathGoods', obj, function(data, textStatus) {
			if(data.messageCode == '200') {
				that.tab.splice(i, 1);
			} else {
				layer.msg(data.msg);
			}
		});
		layer.close(index);
	})
}

// 双击设置 
function changSaleStatusa(isWhat, i, id ,that){
	var obj = {};
	obj.goodsId = [];
	obj.goodsId.push(id);
	if(that.tab[i][isWhat] == 0) {
		that.tab[i][isWhat] = 1;
		obj[isWhat] = 1;
	} else {
		that.tab[i][isWhat] = 0;
		obj[isWhat] = 0;
	}
	$.ajax({
		type: "get",
		url: SURL + "goods/update",
		data: obj,
		dataType: 'json',
		success: function(res) {
			layer.msg(res.message);
		}
	});
}

//搜索
function goodsByPage(){
	search_data = getParams('.s-query');
	getList();
	initPage();
}
