
function specSave() { //保存规格
	var data = {};
	var that = vm;
	data.goodsId = goodsId;
//	back_spec(function(res) {
//		var res = res;
//		$.each(res, function(i, v) {
//			//						delete res[i]["itemDesc"];
//			res[i].shopId = shopId;
//		})
//		data.specItemss = JSON.stringify(res);
//	});

	get_tab();
	Vue.nextTick(function(){
		var goodsSpecss = that.spec.goodsSpecss;
		
		var goodsAttributess = that.spec.goodsAttributess;
		
		$.each(goodsSpecss, function(i, v) {
			goodsSpecss[i].shopId = shopId;
		})
		
		$.each(goodsAttributess, function(i, v) {
			goodsAttributess[i].shopId = shopId;
		})
		
		data.goodsSpecss = JSON.stringify(goodsSpecss);
		data.goodsAttributess = JSON.stringify(goodsAttributess);
		console.log(data);
		$.ajax({
			type: "get",
			data: data,
			url: SURL + "goods/addAttrCat_two",
			success: function(data) {
				console.log(data);
				layer.msg(data.message);
			}
		});
	})
	
};

function batchChange(type) {
	$('.' + type).val($('.' + type + 'h').val());
};

function get_tab() {
	var tab = [];
	$.each($('.tab_tr'), function(i, v) {
		var tr = {};
		tr.shopId = shopId;
		var specIds = [];
		$.each(vm.spec.listArr[i], function(s, k) {
			specIds.push(k.itemId);
		});
		tr.specIds = specIds.join(':');
		tr.productNo = ($(this).children('.productNo_td').children('input').val());
		tr.marketPrice = ($(this).children('.marketPrice_td').children('input').val());
		tr.specPrice = ($(this).children('.specPrice_td').children('input').val());
		tr.specStock = ($(this).children('.specStock_td').children('input').val());
		tr.warnStock = ($(this).children('.warnStock_td').children('input').val());
		tr.saleNum = ($(this).children('.saleNum_td').children('input').val()) //	选填	销量
		tab.push(tr);
	})
	vm.spec.goodsSpecss = tab;
};

function back_tab(arr) {
	var tab = arr;
	$.each(tab, function(i, v) {
		var tr = $('.tab_tr').eq(i);
		tr.children('.productNo_td').children('input').val(v.productNo);
		tr.children('.specPrice_td').children('input').val(v.specPrice);
		tr.children('.specStock_td').children('input').val(v.specStock);
		tr.children('.warnStock_td').children('input').val(v.warnStock);
		tr.children('.marketPrice_td').children('input').val(v.marketPrice);
		tr.children('.saleNum_td').children('input').val(v.saleNum);
	})
};

/**删除批量上传的图片**/
function delBatchUploadImg(obj) {
	var c = ZZHT.confirm({
		content: '您确定要删除商品图片吗?',
		yes: function() {
			$(obj).parent().remove("li");
			layer.close(c);
		}
	});
}

function lastGoodsCatCallback(opts) {
	if(opts.isLast) {
		getSpecAttrs(opts.val);
	} else {
		$('#specsAttrBox').empty();
	}
}

function getBrand(cid) {
	$.ajax({ //根据分类id,获取品牌列表
		type: "get",
		url: SURL + "goods/getCatBrands",
		data: {
			"catId": cid
		},
		async: false,
		success: function(data) {
			vm.brand = data.entity;
		}
	});
}

function getList() { //获取店铺分类列表
	$.ajax({
		type: "get",
		url: SURL + "sktShopCats/selectList",
		data: {
			"shopId": shopId
		},
		success: function(res) {
			var data = res.entity;
			vm.shopf1 = [];
			vm.shopf2 = [];
			$.each(data, function(i, v) {
				if(v.parentId == '0') {
					vm.shopf1.push(v);
				} else {
					vm.shopf2.push(v);
				}
			});
			Vue.nextTick(function() {
				init();
			})
		}
	});
}

function getProductNo() { //获取新的商品货号
	$.get(SURL + 'goods/generateNo', {}, function(data, textStatus) {
		vm.goods.productNo = data.entity;
		vm.goods.goodsSn = data.entity;
	});
}

function init() {
	if(gup('goodsId')) {
		$('.aORe').html('编辑商品');
		goodsId = gup('goodsId');
		src = 'edit';
		$.ajax({//获取商品详情
			type: "get",
			url: SURL + "goods/selelctgGoodsById",
			data: {
				goodsId: goodsId,
			},
			dataType: 'json',
			success: function(data) {

				var obj = data.entity[0];
				var brandId = data.entity[0].brandId;
				var goodsDesc = obj.goodsDesc;
				oldPath = obj.goodsCatIdPath;
				editor1.txt.html(goodsDesc);
				if(obj.gallery) {
					obj.gallery = JSON.parse(obj.gallery);
					vm.goods = obj;
				} else {
					obj.gallery = [];
					vm.goods = obj;
				}

				$('#goodsImg').val(obj.goodsImg);
				$('#preview').attr('src', obj.goodsImg);

				vm.goodsf1_now = obj.goodsCatIdPath.split('_')[0];
				Vue.nextTick(function() {
					vm.goods.brandId = brandId;

					vm.goodsf2_now = obj.goodsCatIdPath.split('_')[1];
					Vue.nextTick(function() {
						vm.goodsf3_now = obj.goodsCatIdPath.split('_')[2];
					});
				});
			}
		});
	} else { //新增商品
		src = 'add';
		getProductNo();
		$('.aORe').html('新增商品');
	}
}

function add(data) {
	$.ajax({ //根据分类id,获取品牌列表
		type: "get",
		url: SURL + "goods/add",
		data: data,
		success: function(data) {
			location.href = "goods2.html?goodsId=" + data.entity;
			layer.close(loading);
			layer.msg(data.message);
		}
	});
};

function edit(data) {
	if(oldPath!=data.goodsCatIdPath){//改变了商品分类以后清空商品规格
		$.ajax({//清空商品规格
			type: "get",
			async:false,
			url: SURL + "goods/deleteAttrCat_two",
			data: {"goodsId":goodsId},
			success: function(res) {
				console.log(res);
			}
		});
	};
	$.ajax({
		type: "get",
		url: SURL + "goods/updateGoods",
		async:false,
		data: data,
		success: function(data) {
			location.href = "goods2.html?goodsId=" + gup('goodsId');
			layer.close(loading);
			layer.msg(data.message);
		}
	});
	
};

function getSpec(goodsCatId) { //根据分类id,获取规格
	$.ajax({ //获取当前商品的规格
		type: "get",
		url: SURL + "goods/selectSpecShow",
		data: {
			'goodsId': goodsId
		},
		success: function(data) {
			console.log(data);
			if(data.entity.length > 0) { //有规格 修改
				console.log('有规格');
				specType = 'edit';
				$.each(data.entity, function(i, v) {
					data.entity[i].isShow = 1;
				})
				
				to_spec(data.entity);
				
				changeSpec();
				
				$.ajax({ //获取该商品的价格
					type: "get",
					url: SURL + "goods/selectCatShow",
					data: {
						'goodsId': goodsId,
						'shopId': shopId
					},
					success: function(data) {
						back_tab(data.entity);
						vm.spec.goodsSpecss = data.entity;
					}
				});

			} else { //没有规格 需要新增

				specType = 'add';
				
				//获取初始规格
				$.ajax({ 
					type: "get",
					url: SURL + "goods/selectGoodsCat",
					data: {
						'goodsCatId': goodsCatId
					},
					success: function(data) {
						console.log(data);
						
						to_spec(data.entity);
						
						var that  = vm;
						var tabArr = [];
						$.each(that.spec.specItemss, function(i, v) {
							var arr = [];
							$.each(v.itemDesc, function(ind, item) {
								if(v.itemDesc[ind].isShow == 1 || v.itemDesc[ind].isShow == true) {
									var obj = {
										catName: that.spec.specItemss[i].catName,
										catId: that.spec.specItemss[i].catId,
										val: item.val,
										itemId:item.itemId,
										dataFlag:false,
									};
									arr.push(obj);
								}
							});
							
							if(arr.length > 0) {
								tabArr.push(arr[0]);
							}
						});
						console.log(tabArr);
						getItemId(tabArr,function(tabArr){
							vm.spec.tabArr = tabArr;
						})
					}
				});
				
				//获取初始分类对应的属性
				$.ajax({ 
					type: "get",
					url: SURL + "goods/selectGoodsAttr",
					data: {
						'goodsCatId': goodsCatId
					},
					success: function(data) {
//						console.log(data);
						//vm.spec.goodsAttributess = data.entity;
					}
				});

			}

		}
	});
};

/**初始化**/
function initEdit() {
	$('#tab').TabPanel({
		tab: 0,
		callback: function(no) {
			if(no == 1) { //商品规格
				if(gup('goodsId')) {
					$('.j-specImg').children().each(function() {
						if(!$(this).hasClass('webuploader-pick')) $(this).css({
							width: '80px',
							height: '25px'
						});
					});
					var goodsCatId = vm.goodsf3_now || vm.goodsf2_now || vm.goodsf1_now;
					if(goodsCatId) {
						getSpec(goodsCatId);
					}
				} else {
					layer.msg('请先保存商品详情信息');
				}
			}
			if(!initBatchUpload && no == 2) { //批量上传图片
				initBatchUpload = true;
				var uploader = batchUpload({
					uploadPicker: '#batchUpload',
					uploadServer: SURL + 'upload',
					formData: {
						dir: 'goods',
						isWatermark: 1,
						isThumb: 1
					},
					uploadSuccess: function(file, response) {
						var json = response;
						if(json.messageCode == 200) {
							$li = $('#' + file.id);
							$li.append('<input type="hidden" class="j-gallery-img" iv="' + json.entity + '" v="' + json.entity + '"/>');
							var delBtn = $('<span class="btn-del">删除</span>');
							$li.append(delBtn);
							delBtn.on('click', function() {
								delBatchUploadImg($(this), function() {
									uploader.removeFile(file);
									uploader.refresh();
								});
							});
							$('.filelist li').css('border', '1px solid rgb(59, 114, 165)');
						} else {
							ZZHT.msg(json.msg, {
								icon: 2
							});
						}
					}
				});
			}
			$('.btn-del').click(function() {
				delBatchUploadImg($(this), function() {
					$(this).parent().remove();
				});
			})
		}
	});
	for(var i = 1; i < 8; i++) {
		ZZHT.upload({
			pick: '#goodsImgPicker' + i,
			formData: {
				dir: 'goods',
				isWatermark: 1,
				isThumb: 1
			},
			accept: {
				extensions: 'gif,jpg,jpeg,bmp,png',
				mimeTypes: 'image/*'
			},
			callback: function(f) {
				var json = f;
				if(json.messageCode == '200') {
					id = this.pick;
					$('#uploadMsg').empty().hide();
					$(id).prev('div').children('#preview').attr('src', json.entity);
					$(id).nextAll().siblings('input[data-target="#msg_goodsImg"]').val(json.entity);
					$('#msg_goodsImg').hide();
				}
			},
			progress: function(rate) {
				$('#uploadMsg').show().html('已上传' + rate + "%");
			}
		});
	};

	//富文本编辑器
	var E = window.wangEditor;
	
	editor1 = new E('#editor');
	var url = SURL + "aliyunUpload"; //接口
	editor1.customConfig.uploadImgServer = url;

	editor1.customConfig.uploadFileName = 'file';

	editor1.create();
	
	//富文本编辑器————end
}

function getItemId(arr,back){//获取规格itemId //新增规格Id 修改规格
	$.each(arr,function(i,v){
		if(arr[i].dataFlag){
			arr[i].dataFlag = 1;
		}else{
			arr[i].dataFlag = -1;
		}
	})
	var data = {
		goodsId:goodsId,
		shopId:shopId,
		specItems:JSON.stringify(arr),
	};
	$.ajax({ //获取商城第一级分类
		type: "get",
		url: SURL + "goods/addGoodsCat",
		async:false,
		data:data,
		success: function(data) {
			console.log(data);
			var tabArr = [];
			$.each(data.entity,function(i,v){
				v.dataFlag = v.dataFlag == 1 ? true : false;
				var arr = [];
				arr.push(v);
				tabArr.push(arr);
			})
			console.log(tabArr);
			return back && back(tabArr);
		}
	});
}

function changeSpec(){
	var that  = vm;
	var tabArr = [];
	$.each(that.spec.specItemss, function(i, v) {
		var arr = [];
		$.each(v.itemDesc, function(ind, item) {
			if(v.itemDesc[ind].dataFlag == 1 || v.itemDesc[ind].dataFlag == true) {
				var obj = {
					'catName': that.spec.specItemss[i].catName,
					'catId': that.spec.specItemss[i].catId,
					'val': item.val,
					'itemId':item.itemId,
					'dataFlag':that.spec.specItemss[i].dataFlag,
				};
				arr.push(obj);
			}
		});
		
		if(arr.length > 0) {
			tabArr.push(arr);
		}
	});
	
	vm.spec.tabArr = tabArr;
	console.log(tabArr);
	to_tab(tabArr);
}

