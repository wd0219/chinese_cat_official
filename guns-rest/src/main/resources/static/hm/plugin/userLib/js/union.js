//处理申请联盟商家
function initGrid() {
    var cardId1 = $("#IDnumber1").val(); //法人隐藏位数的身份证号
    var cardId = $("#IDnumber").val(); //法人身份证号
    var legalPerson = $("#legalPerson").val();//法人姓名
    for (var i = 1; i < 7; i++) {
        ZZHT.upload({
            pick: '#adFilePicker' + i,
            formData: {dir: 'users'},
            accept: {extensions: 'gif,jpg,jpeg,bmp,png', mimeTypes: 'image/*'},
            callback: function (f) {
                var json = f;
                if (json.messageCode == 200) {
                    var id = this.pick;
                    $('#uploadMsg').empty().hide();
                    //将上传的图片路径赋给全局变量
                    $(id).next('input').val(json.entity);
                    $(id).parent('div').prev('div').html('<img src="'+json.entity+'"  height="152" />');
                    $(id).nextAll('span').remove();
                } else {
                    ZZHT.msg(json.message, {icon: 2});
                }
            }
        });
    }
    //为否时填写身份证号添加事件
    $("#IDnumber1").on('blur', function () {
        $("#IDnumber").val($(this).val());
    });
    //是否法人切换
    $(".ui-checkbox1").click(function (e) {
        var chk = $(this).children("input");
        var check = chk.attr("checked");
        if (check) return;
        $(".ui-checkbox1").children("input").attr("checked", false);
        chk.attr("checked", check ? false : true);
        if (chk.val() == 1) {//选择是的时候
            //去掉否时的验证信息msg-box
            $("#legalPerson").next(".msg-box").remove();
            $("#IDnumber1").next(".msg-box").remove();
            //重新设置值
            $("#legalPerson").val(legalPerson);
            $("#IDnumber1").val(cardId1);
            $("#IDnumber").val(cardId);
            //设为不可选
            $("#legalPerson").attr('disabled', true);
            $("#IDnumber1").attr('disabled', true);
            //身份证去掉校验
            $("#IDnumber1").attr('data-rule', '身份证号: required;');
            //授权书图片去掉校验并隐藏
            $("#authCertificate").css('display', 'none');
            $("#authorizationImg").removeAttr('data-rule');

        } else {//选择否的时候
            //设置可用
            $("#legalPerson").attr('disabled', false);
            $("#IDnumber1").attr('disabled', false);
            //清空值
            $("#legalPerson").val('');
            $("#IDnumber1").val('');
            //身份正加上校验
            $("#IDnumber1").attr('placeholder', '请输入身份证号');
            $("#IDnumber1").attr('data-rule', '身份证号: required;IDcard;');
            /*$("#legalPerson").attr('placeholder','请输入法人姓名');
             $("#legalPerson").attr('data-rule','法人姓名: required;');*/
            //授权书图片加上校验并显示
            $("#authCertificate").css('display', 'block');
            $("#authorizationImg").attr('data-rule', '上传授权证书: required;');
        }
    });
    //个体，公司切换
    $(".uis-checkbox").click(function (e) {
        var chk = $(this).children("input");
        var check = chk.attr("checked");
        if (check) return;
        $(".uis-checkbox").children("input").attr("checked", false);
        chk.attr("checked", check ? false : true);
    });
    //协议切换
    $(".ui-checkbox3").click(function (e) {
        var a = $(this).attr('data-id');
        var chk = $('.ui-checkbox3 .chks');
        if(a == 1){
            chk.removeAttr("checked");
            $(this).attr('data-id',0)
        }else{
            chk.attr("checked","checked");
            $(this).attr('data-id',1)
        }
        
    });
    //三证合一切换
    $(".ui-checkbox5").click(function (e) {
        var chk = $(this).children("input");
        var check = chk.attr("checked");
        if (check) return;
        $(".ui-checkbox5").children("input").attr("checked", false);
        chk.attr("checked", check ? false : true);
        var name1 = chk.val();
        if (parseInt(name1) === 0) {//不是三合一
            //显示上传图片并增加校验
            $('#codeCertificate').css('display', 'block');
            $('#taxCertificate').css('display', 'block');
            $("#taxImg").attr('data-rule', '综合服务费务登记证: required;');
            $("#codeImg").attr('data-rule', '组织机构代码证: required;');
        } else {//三合一
            //隐藏上传图片并去掉校验
            $('#codeCertificate').css('display', 'none');
            $('#taxCertificate').css('display', 'none');
            $("#taxImg").removeAttr('data-rule');
            $("#codeImg").removeAttr('data-rule');
        }
    });
    $('#form_union').validator({
        rules: {
            checkID: function (element) {
                return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(element.value) || '身份证号输入有误';
            }
        },
        fields: {
            'IDnumber': 'checkID'
        },
        valid: function (form) {
            var ischecked = $(".chks").attr("checked");
            if (ischecked == undefined){
                ZZHT.msg("请勾选商家联盟商家服务协议",{icon:5});
                return;
            }
            var ll = ZZHT.msg('数据处理中，请稍候...');
            var params = ZZHT.getParams('.ipt');
            var cityId = $('select[level="1"]').val();
            var areaId = $('select[level="2"]').val();
            params['cityId'] = cityId ? cityId : 0;
            params['areaId'] = areaId ? areaId : 0;
            params['isLegal'] = $('input[name=isLegal][checked=checked]').val();
            params['companyType'] = $('input[name=companyType][checked=checked]').val();
            params['licenseMerge'] = $('input[name=licenseMerge][checked=checked]').val();
            params['userId'] = localStorage.userId;
            console.log(params);
            //申请线下联盟商家
            $.get(SURL+'sktShopApprove/addShopApprove', params, function (data) {
                layer.close(ll);
                if(data.messageCode==200){
                	ZZHT.msg('申请成功',function(){
                		location.href="5.html";
                	})
                }else{
                	ZZHT.msg(data.message);
                }
            });
			
        }
    });
    //
    $(".xieyi").click(function(){
        layer.open({
            type: 2,
            title: '用户注册协议',
            shadeClose: true,
            shade: 0.8,
            area: ['1000px', ($(window).height() - 50) + 'px'],
            content: '../view/layer/union_layer.html',
            btn: ['同意并注册'],
            yes: function(index, layero) {
                layer.close(index);
            }
        });
    });

}