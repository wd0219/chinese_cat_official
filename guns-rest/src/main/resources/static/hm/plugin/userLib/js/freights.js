var shopId;
$(function(){
	areasByList();
});
//运费列表
function areasByList(){
	var loading = ZZHT.msg('正在加载运费数据，请稍后...', {icon: 16,time:60000});
	$.get(SURL + 'sktShops/findByUserId', {
		userId: localStorage.userId
	}, function(data, textStatus) { //获取店铺id
		shopId = data.entity[0].shopId;

		//先获取店铺运费信息渲染页面
		$.ajax({
			type: "get",
			url: SURL + "sktShops/findByYunFei",
			data: {
				"shopId": shopId,
			},
			success: function(res) {
				console.log(res);
				layer.close(loading);

				$.each(res.entity,function(i,v){
//					if($('.text_v.areaId2')){
//						$('.text_v.areaId2').val(v.freight);
//					}
					$('#'+v.areaId2).val(v.freight);
				})
				
			}
		});

	});
	
	
}

function treeOpen(obj,id){
    if( $(obj).attr('class').indexOf('active') > -1 ){
    	$(obj).removeClass('active');
        $(obj).html('<img class="ZZHT-lfloat" src="../plugin/userLib/img/seller_icon_zk.png">');
        $('.text_'+id).show();
        $('.tree_'+id).show();
    }else{
    	$(obj).addClass('active');
        $(obj).html('<img class="ZZHT-lfloat" src="../plugin/userLib/img/seller_icon_sq.png">');
        $('.text_'+id).hide();
        $('.tree_'+id).hide();
    }
}

function freightOnblur(obj,id,v){
	$postage = $(obj).val();
	if(v == 0){
		$('.possort').val($postage);
	}else{
		$('.price_'+id).val($postage);
	}
}

function freightSubmit(){
    var params = ZZHT.getParams('.ipt');
    var arr = [];
    $.each(params,function(i,v){
    	var obj={
    		shopId:shopId,
    		freight:v,
    		areaId2:i
    	};
    	arr.push(obj);
    })
    var loading = ZZHT.msg('正在提交数据，请稍后...', {icon: 16,time:60000});
    arr = JSON.stringify(arr);
//	console.log(arr);
	$.ajax({
		type: "post",
		url: SURL + "sktShops/updateByYunFei",
		data: {'sktShops2':arr},
		success: function(res) {
			layer.close(loading);
			layer.msg(res.message);
		}
	});
}