
$(function(){
	"use strict";
	$('#scoreRatio').on('change',function(){
        var totalScore = "";
        var payMoney = $('#payMoney').val();
        var scoreRatio = $('#scoreRatio').val();
        //切换输入文本框
        if($('#scoreRatio option:checked').val()==0) {
            $('#input_score').show();
        }else {
            $('#scoreRatio2').val("");
            $('#input_score').hide();
        }
        //计算奖励积分
        if (payMoney == "" || scoreRatio==0 ) {
            $('#totalScore').html(0);
            return false;
        } else {
            totalScore = payMoney * scoreRatio;
        }
        totalScore = totalScore.toFixed(3);
        $('#totalScore').html(totalScore);
	});
});

//输入消费金额计算奖励积分
$("#totalMoney").on('input',function(){
    var totalScore = "";
    var payMoney = $('#totalMoney').val();
    var scoreRatio = $('#scoreRatio').val();
    var scoreRatio2 = $('#scoreRatio2').val();
    if (payMoney == "" && (scoreRatio == "" || scoreRatio2 == "") ) {
        $('#totalScore').html(0);
        return false;
    } else {
        if (scoreRatio == 0 && scoreRatio2 != "" && scoreRatio2 != undefined) {
            totalScore = payMoney * scoreRatio2;
        } else {
            totalScore = payMoney * scoreRatio;
        }
    }
    totalScore = totalScore.toFixed(3);
    $('#totalScore').html(totalScore);
})

//输入积分计算奖励积分
$("#scoreRatio2").on('input',function(){
    var totalScore = "";
    var payMoney = $('#payMoney').val();
    var scoreRatio2 = $('#scoreRatio2').val();
    if (scoreRatio2 == "" || payMoney=="" ) {
        $('#totalScore').html(0);
        return false;
    } else {
        totalScore = payMoney * scoreRatio2;
    }
    totalScore = totalScore.toFixed(3);
    $('#totalScore').html(totalScore);
})


//赠送积分
function handOut() {
	$('#handOut').validator({
        rules: {
            checkPrice: function(element) {
				return /^\d+(\.\d{1,2})?$/.test(element.value) || '价格小数后长度必须在1-2位之内'
			},
            checkNumber: function(element) {
				return /^\+?[1-5]\d{0,3}(\.\d*)?$/.test(element.value) || '奖励积分必须在1-500之间'
			},
			checkPwd: function(element) {
				return /^[0-9]{6}$/.test(element.value) || '密码必须为6位数字'
			}
        },
        fields: {
        	'checkData':'买家:required;',
            'totalMoney':'消费金额:required;checkPrice',
            'payPwd': '交易密码:required;checkPwd'
        },
        valid: function(form) {
            var params = ZZHT.getParams('.ipt');
            var scoreRatio = $('#scoreRatio').val();
            var scoreRatio2 = $('#scoreRatio2').val();
            if (scoreRatio == 0 && scoreRatio2 == "") {
                ZZHT.msg("请选择或输入奖励比例", {
                    icon: 5,
                });
                return false;
            }
            var standardShareMan = $('#standardShareMan').text();
            if (standardShareMan == '--') {
                ZZHT.msg("请输入正确的买家信息", {
                    icon: 5,
                });
                return false;
            }
            if (scoreRatio == 0) {
                params['scoreRatio'] = scoreRatio2;
            } else {
                params['scoreRatio'] = scoreRatio;
            }
            if (!/^[1-9]\d*$/.test(params['scoreRatio'])) {
                ZZHT.msg("积分比例只能为正整数", {
                    icon: 5,
                });
                return false;
            }
            params['userId'] = inviteId;
            params['shopId'] = shopId;
            params['score'] = $('#totalScore').html();
            $("#subMitButton").html('正在提交...').attr('disabled', true);
            console.log(params);
            $.get(SURL+'webAccount/sendScore', params, function(data, textStatus) {
            	layer.msg(data.entity.msg);
                $("#subMitButton").html('确认').attr('disabled', false);
                if(data.entity.code=="01"){
                	layer.msg(data.entity.msg,function(){
                		window.location.reload();
                	})
                }
            });
        }
	});
}

// 检查分享人信息
$("#checkData").blur(function(){
    var params = new Object();
    var phoneOrLoginName = $('#checkData').val();
    params['phoneOrLoginName'] = phoneOrLoginName;
    if (phoneOrLoginName == "") {
        $('#standardShareMan').html("--");
        inviteId = '';
        return false;
    } else {
        $.post(SURL+'users/checkUserInfo', params, function (data, textStatus) {
            var json = data;
            console.log(json);
            if (json.messageCode == 200) {
            	if(phoneOrLoginName==json.entity.userPhone){//如果用户输入的是手机号就返回登录名
            		$('#standardShareMan').html(json.entity.loginName);
            	}else{
            		$('#standardShareMan').html(json.entity.userPhone);
            	}
            	inviteId = json.entity.userId;
            } else {
                ZZHT.msg(json.message, {
                    icon: 2,
                }, function () {
                    $('#standardShareMan').html("--");
                    inviteId = '';
                });
            }
        });
    }
})

//输入购买金额计算奖励积分
$("#buyMoney").on('input',function(){
    var totalScore = "";
    var payMoney = $('#buyMoney').val();
    if (payMoney == "" ) {
        $('#sumScore').html(0);
        return false;
    } else {
        totalScore = payMoney * 10000 / num;
        totalScore = totalScore.toFixed(3);
    }
    $('#sumScore').html(totalScore);
})