/**
 * @author caody
 * 调用页面初始化方法
 */
$(function() {
	initSystem();
});
/**
 * 初始化方法的，调用sktOrderShow对象的加载方法
 */
var initSystem = function(){
    sktOrderShow.init();
};

/**
 * 创建sktOrderShow页面的对象，
 * 订单数据加载方法
 * 商品数据加载方法
 * 页面关闭方法
 */
var sktOrderShow = {
		
		/**
		 * sktOrderShow页面初始化
		 */
		init : function(){
			this.sktOrderLoad();
			this.sktOrderGoodsLoad();
		},
		/**
		 * 订单数据加载
		 * ajax回调方法中不能用this.xxx调用其他函数，需要用对象引用例如sktOrderShow.xxxx
		 */
		sktOrderLoad:function(){
			var sktOrdersId = gup('orderId');
			$.post(SURL+"/sktOrders/sktOrders_load",{
				sktOrdersId:sktOrdersId
			},function(data){
				console.log(data);
				
				var skt_order = data;
				/**
				 * 订单流程（日志信息）加载
				 */
				sktOrderShow.sktOrderFlowLoad(skt_order);
				/**
				 * 订单信息加载
				 */
				sktOrderShow.sktOrderInfoLoad(skt_order);
				/**
				 * 发票信息加载
				 */
				sktOrderShow.sktInvoiceInfoLoad(skt_order);
				/**
				 * 收货人信息加载
				 */
				sktOrderShow.sktConsigneeInfoLoad(skt_order);
				/**
				 * 商品信息中的订单积分信息加载
				 */
				sktOrderShow.sktOrderScoreInfoLoad(skt_order);
			})
		},
		/**
		 * 商品信息中的订单积分信息加载
		 */
		sktOrderScoreInfoLoad:function(skt_order){
			
			$("#goodsSummary").html("");
			$("#goodsSummary").append("<div class='summary' id='goodsMoney'></div>");
			$("#goodsMoney").append("商品总金额：¥<span>"+skt_order.goodsMoney+"</span>");
			
			$("#goodsSummary").append("<div class='summary' id='deliverMoney'></div>");
			$("#deliverMoney").append("运费：¥<span>"+skt_order.deliverMoney+"</span>");
			
			$("#goodsSummary").append("<div class='summary' id='totalMoney'></div>");
			$("#totalMoney").append("应付总金额：¥<span>"+skt_order.totalMoney+"</span>");
			
			$("#goodsSummary").append("<div class='summary' id='realMoney'></div>");
			$("#realMoney").append("实付总金额：¥<span>"+skt_order.realMoney+"</span>");
			
			$("#goodsSummary").append("<div class='summary' id='score'></div>");
			//$("#score").append("可获得积分：<span class='orderScore'>"+skt_order.score+"</span>");
			$("#score").append("可获得积分：<span class='orderScore'>"+skt_order.goodsMoney * 100+".000</span>个");
		},
		
		/**
		 * 收货人信息加载
		 */
		sktConsigneeInfoLoad:function(skt_order){
			
			$("#consigneeInfo").append("<div class='form-group' id='userName'></div>");
			$("#userName").append("<label for='name'>收货人:</label>"+skt_order.userName+"");
			//$("#userName").append("<label for='name'>"+skt_order.userName+"</label>");
			
			$("#consigneeInfo").append("<div class='form-group' id='userAddress'></div>");
			$("#userAddress").append("<label for='name'>收货地址:</label>"+skt_order.userAddress+"");
			//$("#userAddress").append("<label for='name'>"+skt_order.userAddress+"</label>");
			
			$("#consigneeInfo").append("<div class='form-group' id='userPhone'></div>");
			$("#userPhone").append("<label for='name'>联系方式:</label>"+skt_order.userPhone+"");
			//$("#userPhone").append("<label for='name'>"+skt_order.userPhone+"</label>");
			
		},
		
		/**
		 * 发票信息加载
		 */
		sktInvoiceInfoLoad:function(skt_order){
			
			var invoiceDescription = "";
			if(skt_order.isInvoice == 0){
				invoiceDescription = "不需要";
			}else{
				invoiceDescription = "需要";
			}
			
			$("#invoiceInfo").append("<div class='form-group' id='isInvoice'></div>");
			$("#isInvoice").append("<label for='name'>是否需要发票:</label>"+invoiceDescription+"");
			//$("#isInvoice").append("<label for='name'>"+invoiceDescription+"</label>");
			
			$("#invoiceInfo").append("<div class='form-group' id='invoiceClient'></div>");
			$("#invoiceClient").append("<label for='name'>发票抬头:</label>"+skt_order.invoiceClient+"");
			//$("#invoiceClient").append("<label for='name'>"+skt_order.invoiceClient+"</label>");
		},
		
		/**
		 * 订单信息加载
		 */
		sktOrderInfoLoad:function(skt_order){
			
			var payName = "";
			
			switch(skt_order.payType){
			case 1:
				payName = "线下现金支付"
				break;
			case 2:
				payName = "现金账户支付"
					break;
			case 3:
				payName = "华宝支付"
					break;
			case 4:
				payName = "第三方支付"
					break;
			case 5:
				payName = "混合支付"
					break;
			case 6:
				payName = "华宝营业额"
					break;
			}
			
			$("#orderInfo").append("<div class='form-group' id='orderNo'></div>");
			$("#orderNo").append("<label for='name'>订单编号:</label>"+skt_order.orderNo+"");
			//$("#orderNo").append("<label for='name'>"+skt_order.orderNo+"</label>");
			
			$("#orderInfo").append("<div class='form-group' id='createTime'></div>");
			$("#createTime").append("<label for='name'>下单时间:</label>"+skt_order.createTime+"");
			//$("#createTime").append("<label for='name'>"+skt_order.createTime+"</label>");
			
			$("#orderInfo").append("<div class='form-group' id='payType'></div>");
			$("#payType").append("<label for='name'>支付方式:</label>"+payName+"");
			//$("#payType").append("<label for='name'>"+payName+"</label>");
			
			$("#orderInfo").append("<div class='form-group' id='buyerMessage'></div>");
			$("#buyerMessage").append("<label for='name'>买家留言:</label>暂无");
			//$("#buyerMessage").append("<label for='name'>暂无</label>");
			
		},
		
		/**
		 * 订单流程加载
		 */
		sktOrderFlowLoad:function(skt_order){
			/**
			 * 清空订单流程内容
			 */
			switch(skt_order.orderStatus){
			
			/**
			 *退款给状态 
			 */
			case -3:
				this.logInformation(skt_order);
				break;
			
			/**
			 * 用户正在下单时
			 * 未支付，商家未发货，未确认收货，未双方获评
			 */
			case -2:
				$("#flow").append("<div class='icon'><span class='icons icon12 icon13'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon21'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon31'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons  icon41'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon51 '></span></div>");
				
				$("#explain").append("<div class='path' id='path'></div>");
				$("#path").append("<span>下单成功，等待用户支付<br>" + skt_order.createTime  + "</span>");
				$("#explain").append("<p>下单 等待支付</p>");
				$("#explain").append("<p>已支付</p>");
				$("#explain").append("<p>商家发货</p>");
				$("#explain").append("<p>确认收货</p>");
				$("#explain").append("<p>订单结束<br>双方互评</p>");
				break;
			/**
			 * 用户取消
			 */
			case -1:
				this.logInformation(skt_order);
				break;
			/**
			 * 用户正在支付时
			 * 已下单，商家未发货，未确认收货，未双方获评
			 */
			case 0:
				$("#flow").append("<div class='icon'><span class='icons icon12'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon22 icon23'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon31'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons  icon41'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon51 '></span></div>");
				
				$("#explain").append("<div class='path' id='path'></div>");
				$("#path").append("<span>下单成功，等待用户支付<br>" + skt_order.createTime  + "</span>");
				$("#path").append('<span style="">订单已支付，下单成功付<br>' + skt_order.paymentTime  + '</span>');
				$("#explain").append("<p>下单 等待支付</p>");
				$("#explain").append("<p>已支付</p>");
				$("#explain").append("<p>商家发货</p>");
				$("#explain").append("<p style=''>确认收货</p>");
				$("#explain").append("<p style=''>订单结束<br>双方互评</p>");
				break;
			/**
			 * 商家发货
			 * 已下单，用户已支付，未确认收货，未双方获评
			 */
			case 1:
				$("#flow").append("<div class='icon'><span class='icons icon12'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon22'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon32 icon33'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons  icon41'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon51 '></span></div>");
				
				$("#explain").append("<div class='path' id='path'></div>");
				$("#path").append("<span>下单成功，等待用户支付<br>" + skt_order.createTime  + "</span>");
				$("#path").append("<span>订单已支付，下单成功付<br>" + skt_order.deliveryTime  + "</span>");
				$("#path").append("<span>商家已发货<br>" + skt_order.paymentTime  + "</span>");
				$("#explain").append("<p>下单 等待支付</p>");
				$("#explain").append("<p>已支付</p>");
				$("#explain").append("<p>商家发货</p>");
				$("#explain").append("<p>确认收货</p>");
				$("#explain").append("<p>订单结束<br>双方互评</p>");
				break;
			/**
			 * 用户确认收货
			 */
			case 2:
				this.logInformation(skt_order);
				break;
			
			/**
			 * 确认收货
			 * 已下单，用户已支付，商家已发货，未双方获评
			 */
			case 3:
				$("#flow").append("<div class='icon'><span class='icons icon12'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon22'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon32'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons  icon42 icon43'></span></div>");
				$("#flow").append("<div class='arrow'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon51 '></span></div>");
				
				$("#explain").append("<div class='path' id='path'></div>");
				$("#path").append("<span>下单成功，等待用户支付<br>" + skt_order.createTime  + "</span>");
				$("#path").append("<span>订单已支付，下单成功付<br>" + skt_order.deliveryTime  + "</span>");
				$("#path").append("<span>商家已发货<br>" + skt_order.paymentTime  + "</span>");
				$("#path").append("<span>用户已收货<br>" + skt_order.receiveTime  + "</span>");
				$("#explain").append("<p>下单 等待支付</p>");
				$("#explain").append("<p>已支付</p>");
				$("#explain").append("<p>商家发货</p>");
				$("#explain").append("<p>确认收货</p>");
				$("#explain").append("<p>订单结束<br>双方互评</p>");
				break;
			/**
			 * 评价结束
			 * 已下单，用户已支付，商家已发货，未双方获评
			 */
			case 4:
				$("#flow").append("<div class='icon'><span class='icons icon12'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon22'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon32'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons  icon42'></span></div>");
				$("#flow").append("<div class='arrow1'>··················&gt;</div>");
				$("#flow").append("<div class='icon'><span class='icons icon52 icon53'></span></div>");
				
				$("#explain").append("<div class='path' id='path'></div>");
				$("#path").append("<span>下单成功，等待用户支付<br>" + skt_order.createTime  + "</span>");
				$("#path").append("<span>订单已支付，下单成功付<br>" + skt_order.deliveryTime  + "</span>");
				$("#path").append("<span>商家已发货<br>" + skt_order.paymentTime  + "</span>");
				$("#path").append("<span>用户已收货<br>" + skt_order.receiveTime  + "</span>");
				$("#path").append("<span>订单结束<br>双方互评</span>");
				$("#explain").append("<p>下单 等待支付</p>");
				$("#explain").append("<p>已支付</p>");
				$("#explain").append("<p>商家发货</p>");
				$("#explain").append("<p>确认收货</p>");
				$("#explain").append("<p>订单结束<br>双方互评</p>");
				break;
				
			} 
		},
		/**
		 * 非流程下的日志信息，用from表单来展示
		 */
		logInformation:function(skt_order){
			/**
			 * 	清空容器中的样式
			 */
			$("#logInformation").html("");
			
			var html = [];
			if(skt_order.createTime != "")
				html.push("<div><label>下单成功,等待用户支付:</label>"+skt_order.createTime+"</div>");
			if(skt_order.paymentTime != "")
				html.push("<div><label>订单已支付,下单成功:</label>"+skt_order.paymentTime+"</div>");
			if(skt_order.deliveryTime != "")
				html.push("<div>"+skt_order.deliveryTime+"<label>商家已发货,快递号为</label>"+skt_order.expressNo+"</div>");
			if(skt_order.refundTime != undefined)
				html.push("<div>"+skt_order.refundTime+"<label> 用户退款原因</label>"+skt_order.refundOtherReson+"</div>");
			html.push("</div>");
			$('#logInformation').html(html.join(''));
			
		},
		/**
		 * 商品数据加载
		 */
		sktOrderGoodsLoad:function(){
			var sktOrdersId = gup('orderId');
			$.post(SURL+"/sktOrders/sktOrdersGoods_load",{
				sktOrdersId:sktOrdersId
			},function(result){
				$.each(result, function(i,val){
					var goodsId = val.goodsId;
					$("#goodsHead").after("<div class='goods-item' id='goodsItem"+i+"'></div>");
					$("#goodsItem"+i+"").append("<div class='shop'>"+val.shopName+"</div>");
					$("#goodsItem"+i+"").append("<div class='goods-list' id='goodsList"+i+"' style='overflow: hidden;'>");
					$("#goodsList"+i+"").append("<div class='item' id='item"+i+"'></div>");
					
					$("#item"+i+"").append("<div class='goods' id='goods"+i+"'></div>");
					$("#item"+i+"").append("<div class='price'>¥"+val.goodsPrice+"</div>");
					$("#item"+i+"").append("<div class='num'>"+val.goodsNum+"</div>");
					$("#item"+i+"").append("<div class='t-price'>¥"+val.goodsPrice * val.goodsNum+"</div>");
					$("#item"+i+"").append("<div class='f-clear'></div>");
					
					$("#goods"+i+"").append("<div class='img' id='img"+i+"'></div>");
					$("#goods"+i+"").append("<div class='name'>"+val.goodsName+"</div>");
					
					
					
					
					
					if(decodeURI(gup('orderStatus'))=='待评价'){
						var href = 'evaluate.html?goodsId='+goodsId+'&shopId='+val.shopId+'&orderId='+val.orderId;
						$("#goods"+i+"").append("<div class='to_eva'><a href="+href+">去评价</a></div>");
					}
					
					//$("#goods"+i+"").append("<div class='spec'>内存：2G<br>颜色：铂金色</div>");
					
					var img = val.goodsImg;
					if(img.substr(0,4)!='http'){
						img = 'http://zzhtwl.oss-cn-qingdao.aliyuncs.com/'+img;
					}
					
					$("#img"+i+"").append("<a href='../view/detail.html?goodsId="+val.goodsId+"' target='_blank'>" +
							"<img src='"+img+"' width='80' height='80' title='"+val.goodsName+"'> </a>");
					
					sktOrderShow.sktOrderGoodsParameterLoad(goodsId,i);
					
				});
			})
		},
		/**
		 * 商品参数加载
		 */
		sktOrderGoodsParameterLoad:function(goodsId,i){
			$.post(SURL+"/sktOrders/sktOrdersGoodsParameter_load",{
				goodsId:goodsId
			},function(result){
				console.log(result);
				var temp="";
				$.each(result,function(i,data){
					$.each(data,function(ind,val){
						temp = temp + ind +":"+val+"<br>";
					})
					
				});
				$("#goods"+i+"").append("<div class='spec'>"+temp+"</div>")
			})
		},
		/**
		 * 关闭当前页面
		 */
		close:function(){
			parent.layer.close(window.parent.SktOrders.layerIndex);
		},
		
};




