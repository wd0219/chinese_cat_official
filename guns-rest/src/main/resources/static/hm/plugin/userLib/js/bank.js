//添加银行卡提交
$('.confirm_union').click(function(){
    var params = getParams('.ipt');
        params['provinceId'] = $('select[level="0"]').val();
        params['cityId'] = $('select[level="1"]').val() ? $('select[level="1"]').val() : "";
        params['areaId'] = $('select[level="2"]').val() ? $('select[level="2"]').val() : "";
	    params['type']=1;
	    params['userId']=localStorage.userId;
	    console.log(params);
	    $.get(SURL+'usersBankcards/add',params, function(data, textStatus) {
	        var json = data;
	        console.log(json);
	        if(json.messageCode == 200) {
	            ZZHT.msg(json.message, {
	                icon: 6
	            }, function() {
	                location.href = '6.html';
	            });
	        } else {
	            ZZHT.msg(json.message, {
	                icon: 5
	            });
	        }
	    });
})

//删除银行卡
$('.del_bank').click(function(){
    var params = new Object();
        params['bankCardId'] = $(this).attr("data-id");
    $.post(ZZHT.U('home/banks/delBank'), params, function(data, textStatus) {
        var json = ZZHT.toJson(data);
        if(json.status == 1) {
            location.href = ZZHT.U('home/banks/bankIndex');
        } else {
            ZZHT.msg(json.msg, {
                icon: 5
            });
        }
    });
})

//选择默认银行卡
$(".ui-radio").click(function(e) {
    var chk = $(this).children("input");
    var check = chk.attr("checked");
    if(check) return;
    $(".ui-radio").children("input").attr("checked", false);
    chk.attr("checked", check ? false : true);
    var params = new Object();
    params['bankCardId'] = $(this).attr("data-id");
    $.post(ZZHT.U('home/banks/defaultBank'), params, function(data, textStatus) {
        var json = ZZHT.toJson(data);
        if(json.status == 1) {
            location.href = ZZHT.U('home/banks/bankIndex');
        } else {
            ZZHT.msg(json.msg, {
                icon: 5
            });
        }
    });
});