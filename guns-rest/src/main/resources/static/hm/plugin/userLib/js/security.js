var time = 0;
var isSend = false;
$(function() {
	//复选框
	$(".ui-checkbox1").click(function(e) {
		var chk = $(this).children("input");
		var check = chk.attr("checked");
        if(check) return;
		$(".ui-checkbox1").children("input").attr("checked", false);
		chk.attr("checked", check ? false : true);
	});
	$(".uis-checkbox").click(function(e) {
		var chk = $(this).children("input");
		var check = chk.attr("checked");
        if(check) return;
		$(".uis-checkbox").children("input").attr("checked", false);
		chk.attr("checked", check ? false : true);
	});
	$(".ui-checkbox3").click(function(e) {
		var chk = $(this).children("input");
		var check = chk.attr("checked");
        if(check) return;
		$(".ui-checkbox3").children("input").attr("checked", false);
		chk.attr("checked", check ? false : true);
	});
	// $('#phoneVerify').validator({
	//    valid: function(form){
	//  	  var n=$('#VerifyId').val();
	//  	  getPhoneVerifys(n);
	//    }
	// });
	
	$('.del_bank').click(function() {
		layer.open({
			type: 0,
			title: '银行卡管理',
			area: ['300px', '180px'],
			btn: ['确定', '取消'],
			content: '确定删除此银行卡？'
		});
	});
});

function vePayForm() {
	//修改密码
	myorm = $('#payform').validator({
		valid: function(form) {
			var params = ZZHT.getParams('.ipt');
            	params['function'] = "findPayPwd";
			var loading = ZZHT.msg('正在提交数据，请稍后...', {
				icon: 16,
				time: 60000
			});
			$.post(ZZHT.U('home/users/payPassEdit'), params, function(data, textStatus) {
				layer.close(loading);
				var json = ZZHT.toJson(data);
				if(json.status == '1') {
					ZZHT.msg(json.msg, {
						icon: 1,
						time: 2000
					}, function() {
						location.href = ZZHT.U('home/account/index');
					});
				} else {
					ZZHT.msg(json.msg, {
						icon: 2
					});
				}
			});
		}
	})
}

function veemailForm() {
	//绑定邮箱
	emailForm = $('#emailForm').validator({
		rules: {
			remote: function(element) {
				return $.post(ZZHT.U('home/users/checkEmail'), {
					"loginName": element.value
				}, function(data, textStatus) {});
			}
		},
		fields: {
			userEmail: {
				rule: "required;email;remote;",
				msg: {
					required: "请输入邮箱",
					email: "请输入有效的邮箱"
				},
				tip: "请输入邮箱",
			},
			verifyCode: {
				rule: "required",
				msg: {
					required: "请输入验证码"
				},
				tip: "请输入验证码",
				target: "#verify"
			}
		},

		valid: function(form) {
			var params = ZZHT.getParams('.ipt');
			var loading = ZZHT.msg('正在提交数据，请稍后...', {
				icon: 16,
				time: 60000
			});
			$.post(ZZHT.U('home/users/getEmailVerify'), params, function(data, textStatus) {
				layer.close(loading);
				var json = ZZHT.toJson(data);
				if(json.status == '1') {
					ZZHT.msg('邮箱已发送，请注册查收');
					setTimeout(function() {
						$('#emailForm').hide();
						$('#inemail').html($('#userEmail').val());
						$('#prompt').show();
					}, 1000);
				} else {
					ZZHT.msg(json.msg, {
						icon: 2
					});
					ZZHT.getVerify('#verifyImg');
				}
			});
		}
	});
}

function vephoneForm() {
	//绑定手机号
	phoneForm = $('#phoneForm').validator({
		valid: function(form) {
			var me = this;
			// ajax提交表单之前，先禁用submit
			me.holdSubmit();
			var params = ZZHT.getParams('.ipt');
			var loading = ZZHT.msg('正在提交数据，请稍后...', {
				icon: 16,
				time: 60000
			});
			$.post(ZZHT.U('home/users/phoneEdito'), params, function(data, textStatus) {
				layer.close(loading);
				var json = ZZHT.toJson(data);
				if(json.status == '1') {
					location.href = ZZHT.U('home/users/editPhoneSu', 'pr=' + json.process);
				} else {
					ZZHT.msg(json.msg, {
						icon: 2
					});
					ZZHT.getVerify('#verifyImg');
				}
			});
		}
	});
}

function vegetemailForm() {
	if(isSend) return;
	isSend = true;
	//修改邮箱
	getemailForm = $('#getemailForm').validator({
		valid: function(form) {
			var params = ZZHT.getParams('.ipt');
			var loading = ZZHT.msg('正在提交数据，请稍后...', {
				icon: 16,
				time: 60000
			});
			$.post(ZZHT.U('home/users/getEmailVerifyt'), params, function(data, textStatus) {
				layer.close(loading);
				var json = ZZHT.toJson(data);
				if(json.status == '1') {
					ZZHT.msg('邮箱已发送，请注册查收');
					time = 120;
					$('#timeSend1').attr('disabled', 'disabled').css('background', '#e8e6e6');
					$('#timeSend1').html('发送验证邮件(120)');
					var task = setInterval(function() {
						time--;
						$('#timeSend1').html('发送验证邮件(' + time + ")");
						if(time == 0) {
							isSend = false;
							clearInterval(task);
							$('#timeSend1').html("重新发送验证邮件");
							$('#timeSend1').removeAttr('disabled').css('background', '#ed5d2a');
						}
					}, 1000);
				} else {
					ZZHT.msg(json.msg, {
						icon: 2
					});
					ZZHT.getVerify('#verifyImg');
				}
			});
		}
	});
}

function vegetphoneForm() {
	//修改手机号
	getphoneForm = $('#getphoneForm').validator({
		valid: function(form) {
			var params = ZZHT.getParams('.ipt');
			var loading = ZZHT.msg('正在提交数据，请稍后...', {
				icon: 16,
				time: 60000
			});
			$.post(ZZHT.U('home/users/phoneEditt'), params, function(data, textStatus) {
				layer.close(loading);
				var json = ZZHT.toJson(data);
				if(json.status == '1') {
					location.href = ZZHT.U('home/users/editPhoneSut');
				} else {
					ZZHT.msg(json.msg, {
						icon: 2
					});
					ZZHT.getVerify('#verifyImg');
				}
			});
		}
	});
}
//修改交易密码获取手机短信验证码
function getPhoneCode() {
	// $('.phoneCode').css('background', '#999');
	// $('.phoneCode').text('重新发送验证码');
}

//发送手机验证码
function getPhoneVerify(code) {
	if (code == "" || code.length != 4 ) {
        ZZHT.msg("请输入图形验证码或图形验证码长度为4", {
            icon: 5
        });
	} else {
        getPhoneVerifys(code);
	}
}

function getPhoneVerifys(code) {
	ZZHT.msg('正在发送短信，请稍后...', {
		time: 600000
	});
	var time = 0;
	var isSend = false;
	var params = new Object();
    	params['function'] = "findPayPwd";
		params['code'] = code;
    	params['type'] = 2;
	$.post(ZZHT.U('home/users/checkImgeCodePc'), params, function(data, textStatus) {
		var json = ZZHT.toJson(data);
		if(isSend) return;
		isSend = true;
		if(json.status != 1) {
			ZZHT.msg(json.msg, {
				icon: 5
			});
			ZZHT.getVerify('#verifyImg');
			time = 0;
			isSend = false;
		}
		if(json.status == 1) {
			ZZHT.msg('短信已发送，请注册查收');
			layer.closeAll('page');
			time = 120;
			$('#timeObtain').attr('disabled', 'disabled').css('background', '#e8e6e6');
			$('#timeObtain').html('获取手机验证码(120)').css('width', '130px');
			var task = setInterval(function() {
				time--;
				$('#timeObtain').html('获取手机验证码(' + time + ")");
				if(time == 0) {
					isSend = false;
					clearInterval(task);
					$('#timeObtain').html("重新获取验证码").css('width', '100px');
					$('#timeObtain').removeAttr('disabled').css('background', '#ed5d2a');
				}
			}, 1000);
		}
	});
}
//申请代理表单提交
function identityAgencyForm(userType){
$('#identityAgencyForm').validator({
	valid: function(form) {
	    var params = getParams('.ipt');
	    params['province'] = $('select[level="0"]').val();
	    params['citys'] = $('select[level="1"]').val()?$('select[level="1"]').val():'';
	    params['county'] = $('select[level="2"]').val()?$('select[level="2"]').val():'';
	    params['userId'] = localStorage.userId;
	    console.log(params);
	    console.log(userType);
	    if(!params['province']){
	    	layer.msg('请选择区域')
	    }else{
	    	if(userType=='0'){//普通用户
		    	layer.msg('请先升级为主管或经理');
		    }else{
		    	$.get(SURL+'agentsApplys/addAgentsApplys', params, function(data, textStatus) {
			        var json = data;
			        console.log(json);
			        if(json.messageCode == 200) {
			            layer.msg('申请成功', {icon: 1,time: 1000});
			            location.href = '5.html';
			        } else {
			            layer.msg(json.message, {icon: 2});
			        }
			    });
		    }
	    }
	}
});

}
//判断升级跳转
$('.grade').click(function(){
    $.post(ZZHT.U('home/Identity/judgeReal'), "", function(data, textStatus) {
        var json = ZZHT.toJson(data);
        if(json.status == 1) {
            location.href = ZZHT.U('home/Identity/grade');
        } else {
            ZZHT.msg(json.msg, {icon: 5});
        }
    });
});
//判断升级经理跳转
$('.graded').click(function(){
    $.post(ZZHT.U('home/Identity/judgeReal'), "", function(data, textStatus) {
        var json = ZZHT.toJson(data);
        if(json.status == 1) {
            location.href = ZZHT.U('home/Identity/upgrade');
        } else {
            ZZHT.msg(json.msg, {icon: 5});
        }
    });
});
//判断商家联盟申请跳转
$('.union').click(function(){
    $.post(ZZHT.U('home/Identity/judgeReal'), "", function(data, textStatus) {
        var json = ZZHT.toJson(data);
        if(json.status == 1) {
            location.href = ZZHT.U('home/Identity/union');
        } else {
            ZZHT.msg(json.msg, {icon: 5});
        }
    });
});
//判断申请线上商家跳转
$('.online').click(function(){
    $.post(ZZHT.U('home/Identity/onlineVerify'), "", function(data, textStatus) {
        var json = ZZHT.toJson(data);
        if(json.status == 1) {
            location.href = ZZHT.U('home/Identity/online');
        } else {
            ZZHT.msg(json.msg, {icon: 5});
        }
    });
});
//判断申请代理跳转
$('.agent').click(function(){
    $.post(ZZHT.U('home/Identity/agentVerify'), "", function(data, textStatus) {
        var json = ZZHT.toJson(data);
        if(json.status == 1) {
            location.href = ZZHT.U('home/Identity/agent');
        } else {
            ZZHT.msg(json.msg, {icon: 5});
        }
    });
});