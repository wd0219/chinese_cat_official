function dropDown(obj, id) {
	if($(obj).attr('class').indexOf('js-shop-plus') > -1) {
		$(obj).removeClass('js-shop-plus').addClass('js-shop-redu');
		$('.tree_' + id).slideUp();
	} else {
		$(obj).removeClass('js-shop-redu').addClass('js-shop-plus');
		$('.tree_' + id).slideDown();
	}
}

function init() {

	//获取店铺评分和信息
	$.ajax({
		type: "get",
		url: SURL + "goodsCats/selectShopsCores",
		data: {
			"shopId": shopId,
		},
		timeout: 5000,
		dataType: 'json',
		success: function(res) {
			vm.shopInfo = res.entity;
			Vue.nextTick(function(){
				ishas(1,shopId);
			})
		}
	});

	//查询店铺热卖商品
	$.ajax({
		type: "get",
		url: SURL + "goods/selectGoodsHots",
		data: {
			"shopId": shopId,
		},
		success: function(data) {
			if(data.entity) {
				vm.shop_hot = data.entity;
				Vue.nextTick(function() {
					imgError();
					if(data.entity.length > 0) {
						$('.ZZHT-shop-contl .ZZHT-align-center').hide();
					}
				})
			}
		}
	});

	//获取店铺的分类
	$.ajax({
		type: "get",
		url: SURL + "sktShopCats/selectList",
		data: {
			"shopId": shopId
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(res) {
			vm.menu = res.entity;
		}
	});

	//先获取店铺轮播图
	$.ajax({
		type: "get",
		url: SURL + "sktShops/findByXSShopsImg",
		data: {
			"shopId": shopId,
		},
		success: function(res) {
			if(res.entity) {
				$.each(res.entity[0], function(i, v) {
					vm.carousel.push(v);
				})
			}
		}
	});

	getList();
	initPage();
}

function getList() { //获取商品列表
	var data = $.extend(true, {}, search_data);
	data.shopId = shopId;
	data.goodsName = goodsName;
	if(shopCatId) {
		data.shopCatId = shopCatId;
	}
	//排序
	if(type) {
		data[type] = statu;
	}
	//分页
	data.pageNum = pageNum;
	//是否只显示新品
	data.isNew = isNew;
	//是否只显示有货
	data.goodsStock = goodsStock;
	console.log(data);
	$.ajax({
		type: "get",
		data: data,
		async: false,
		url: SURLS + "goodsCats/selectShopCatsGoods",
		success: function(data) {
			console.log(data);
			pages = data.page;
			vm.main = data.entity;
		}
	});
}

//分页
function initPage() { //初始化分页
	laypage({
		cont: 'pager',
		pages: pages, //总页数
		skip: false, //是否开启跳页
		skin: '#ed5d2a',
		groups: 3, //连续显示分页数
		curr: function() { //通过url获取当前页，也可以同上（pages）方式获取
			var page = location.search.match(/page=(\d+)/);
			return page ? page[1] : 1;
		}(),
		jump: function(e, first) { //触发分页后的回调
			if(!first) { //一定要加此判断，否则初始时会无限刷新
				pageNum = e.curr;
				getList();
			}
		}
	});
}
var total = 2;

function page(t) { //点击上一页 下一页
	var page = location.search.match(/page=(\d+)/);
	if(page && page.length > 1) { //说明当前url上有page参数
		curr = page[1]; // 当前页
	}
	if(t == 'prev') { // 上一页
		if(curr <= 1) return;
		curr = parseInt(curr) - 1;
	} else { // 下一页
		if(curr >= total) return;
		curr = parseInt(curr) + 1;
	}
	pageNum = curr;
	getList();
}
//分页__end

function searchShopsGoods(obj) {
	statu = $('#mdesc').val();
	if($('#msort').val() != obj) statu = 0;
	type = obj;
	getList();
}