//header
var vue_header = `<div class="ZZHT-header">
			<div class="ZZHT-nav">
				<ul class="headrf fl">
					<li class="drop-infos" style="background-color:#fff;">
						<div class="ZZHT-fred">
							<a href="../view/index.html">华猫商城</a>
						</div>
					</li>
					<li class="j-dorpdown">
						<div class="drop-down">
							<img src="../images/index/huamao_03.png" alt="" class="dp_left top_phone"/>
							<a href="#" target="_blank">手机华猫</a>
							<img src="../images/index/huamao_05.png" alt="" class="dp_right"/>
						</div>
						<div class='j-dorpdown-layer sweep-list'>
							<div class="qrcodea clearfix">
								<div id='qrcodea' class="qrcodeal"></div>
								<div class="qrcodear">
									<p>扫描二维码</p><span>下载手机客户端</span>
									<a>Android</a>
									<a>iPhone</a>
								</div>
								<div class="ZZHT-clear"></div>
							</div>
						</div>
					</li>
				</ul>
				<ul class="headrf fr">
					<li class="drop-infos go_login">
						<div class="ZZHT-fred">
							<a href="index.html">登录</a>
						</div>
					</li>
					<li class="drop-infos go_register">
						<div class="ZZHT-fred">
							<a href="index.html">注册</a>
						</div>
					</li>
					<li class="spacer"></li>
					<li class="j-dorpdown">
						<div class="drop-down pdl5">
							<a href="login.html">我的华猫</a>
							<img src="../images/index/huamao_05.png" alt="" class="dp_right"/>
						</div>
						<div class='j-dorpdown-layer foucs-list'>
							<div>
								<a href="login-1.html">线下商家</a>
							</div>
							<div>
								<a href="login-2.html">商城商家</a>
							</div>
							<div>
								<a href="login-3.html">代理公司</a>
							</div>
						</div>
					</li>
					<li class="j-dorpdown">
						<div class="drop-down pdl5">
							<a href='#'>快捷方式</a>
							<img src="../images/index/huamao_05.png" alt="" class="dp_right"/>
						</div>
						<div class='j-dorpdown-layer foucs-list'>
							<div>
								<a href="login.html">实名认证</a>
							</div>
							<div>
								<a href="login.html">用户升级</a>
							</div>
							<div>
								<a href="login.html">申请联盟</a>
							</div>
							<div>
								<a href="login.html">申请商家</a>
							</div>
							<div>
								<a href="login.html">申请代理</a>
							</div>
						</div>
					</li>
					</li>
				</ul>
				<div class="ZZHT-clear"></div>
			</div>
		</div>`;
//	var vue_header = "<div>aaaaa</div>";
Vue.component('vue_header', {
	data: function() {
		return {
			arr: [],
		}
	},
	template: vue_header
});

//search
var search_container = `<div class='ZZHT-search-container clearfix'>
				<div class='ZZHT-logo fl'>
				<a href="../view/index.html" tppabs="http://izzht.com/" title="华猫商城">
					<img src="../images/index/logo.png" title="华猫商城" alt="华猫商城" height="60">
				</a>
			</div>
			<div class="ZZHT-search-box">
				<div class='ZZHT-search'>
					<input type="text" id='search-ipt' class='search-ipt' placeholder='' value='' />
					<div id='search-btn' class="search-btn" onclick='javascript:Hsearch(this.value)'>搜索</div>
				</div>
			</div>
			<div class="ZZHT-cart-box">
				<a href="login.html" tppabs="http://izzht.com/home/carts/index.html" target="_blank"><span class="word j-word"><i class="icon-cart"></i>我的购物车(<span class="goodsNum">0</span>)</span>
				</a>
				<div class="ZZHT-cart-boxs">
					<div id="list-carts" class="clearfix"></div>
					<div id="list-carts2" class="clearfix"></div>
					<div id="list-carts3" class="clearfix"></div>
					<div class="ZZHT-clear"></div>
				</div>
			</div>
			</div>
			<div class="ZZHT-clear"></div>`;
Vue.component('search_container', {
	data: function() {
		return {
			arr: [],
		}
	},
	template: search_container
});

var nav_menu = `<div class="ZZHT-nav-menus">
				<div class="nav-w" style="position: relative;">
					<div class="w-spacer"></div>
					<div class="dorpdown j-index" id="ZZHT-categorys">
						<div class="dt j-cate-dt">
							<a href="lists.html" target="_blank"><i class="icon-menu"></i>全部商品分类</a>
						</div>
						<div class="dd j-cate-dd ">
							<div class="dd-inner">
								<div v-for="(v,i) in menuArr" :id="'cat-icon-'+i" class="item fore1">
									<h3>
				                      <a :href="i+'.html'" target="_blank">{{v}}</a>
				                    </h3>
				                    
								</div>
							</div>
							<div style="display: none;" class="dorpdown-layer">
								<div class="item-sub" i="1">
									<div class="item-brands" style="display: none;">
										<div class="brands-inner">
										</div>
									</div>
									<div class="subitems">
										<dl class="fore2">
											<dt>
			                                  <a target="_blank" href="9.html">当季流行<i>&gt;</i></a>
			                                </dt>
											<dd>
												<a target="_blank" href="10.html" >冬季新品</a>
												<a target="_blank" href="11.html">商场同款</a>
												<a target="_blank" href="12.html">设计师潮牌</a>
												<a target="_blank" href="13.html">潮流家居服</a>
												<a target="_blank" href="14.html">百搭船袜</a>
											</dd>
										</dl>
										<dl class="fore2">
											<dt>
			                                  <a target="_blank" href="15.html">女装<i>&gt;</i></a>
			                                </dt>
											<dd>
												<a target="_blank" href="16.html">新品推荐</a>
												<a target="_blank" href="17.html">当季热卖</a>
												<a target="_blank" href="18.html">连衣裙</a>
											</dd>
										</dl>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="ZZHT-nav-items">
					<ul>
						<li class="fore1">
							<a href="">首页</a>
						</li>
						<li class="fore1">
							<a href="" target="_blank">品牌街</a>
						</li>
						<li class="fore1">
							<a href="">店铺街</a>
						</li>
						<li class="fore1">
							<a href="index.htm#">华猫自营</a>
						</li>
					</ul>
				</div>
			</div>`;
Vue.component('nav_menu', {
	data: function() {
		return {
			menuArr: ['女装/男装/内衣内裤','母婴/童装/玩具','美妆/洗护','鞋靴/箱包/钟表','手机/电脑','食品/酒水/生鲜','家电/数码/办公','乐器/眼镜/珠宝','房产/装修/建材','家具/家装/家纺','机票/酒店/旅游','礼品鲜花/农资绿植']
		}
	},
	template: nav_menu
});


new Vue({
	el: '#vue_header',
});

$('.j-dorpdown').hover(function() {
	$(this).children('.j-dorpdown-layer').show();
}, function() {
	$(this).children('.j-dorpdown-layer').hide();
});

$('.j-cate-dd').hover(function() {
	$(this).children('.dorpdown-layer').show();
}, function() {
	$(this).children('.dorpdown-layer').hide();
});

$(function() {
	//二维码
	//参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
	var a = qrcode(8, 'M');
	var url = 'http://www.51huamall.com/' + 'home/users/download/?request=1';
	a.addData(url);
	a.make();
	$('#qrcodea').html(a.createImgTag());
});

//header_end

//footer 友情连接+帮助中心
var vue_footer_ck1 = `<div class="ZZHT-footer">
			<div class="friendlink">
				<span>友情链接：</span>
			</div>
			<div class="ZZHT-footer-hp-ck1">
				<div class="ZZHT-desc">
					<dl>
						<dt>帮助中心</dt>
						<dd class="uinn-l">
							<a target="_blank" href="">帮助中心</a>
						</dd>
					</dl>
					<dl>
						<dt>关于我们</dt>
						<dd class="uinn-l">
							<a target="_blank" href="">关于我们</a>
						</dd>
						<dd class="uinn-l">
							<a target="_blank" href="">用户注册</a>
						</dd>
					</dl>
					<dl>
						<dt>购物指南</dt>
						<dd class="uinn-l">
							<a target="_blank" href="">支付方式</a>
						</dd>
						<dd class="uinn-l">
							<a target="_blank" href="">如何购买</a>
						</dd>
						<dd class="uinn-l">
							<a target="_blank" href="">购物流程</a>
						</dd>
					</dl>
					<dl>
						<dt>售后服务</dt>
						<dd class="uinn-l">
							<a target="_blank" href="">售后服务</a>
						</dd>
						<dd class="uinn-l">
							<a target="_blank" href="">帮助中心</a>
						</dd>
					</dl>
					<dl>
						<dt>商务合作</dt>
						<dd class="uinn-l">
							<a target="_blank" href="">商务合作</a>
						</dd>
						<dd class="uinn-l">
							<a target="_blank" href="">商家入驻</a>
						</dd>
					</dl>
				</div>
				<div class="ZZHT-contact">
					<ul>
						<li style="height:85px;">
							<div class="qr-code" style="position:relative;">
								<span id="footerQrcodea"></span>
							</div>
						</li>
					</ul>
				</div>
				<div class="ZZHT-clear"></div>
			</div>
		</div>`;
Vue.component('vue_footer_ck1', {
	data: function() {
		return {
			arr: []
		}
	},
	template: vue_footer_ck1
});
//footer 
var vue_footer_ck3 = `<div class="ZZHT-footer-hp-ck3">
			<div class="ZZHT-footer">
				<div class="links">
					<a href="javascript:if(confirm(%27http://izzht.com/home/Helpcenter/view/id/62.html  \n\nThis file was not retrieved by Teleport Ultra, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://izzht.com/home/Helpcenter/view/id/62.html%27" tppabs="http://izzht.com/home/Helpcenter/view/id/62.html">关于我们</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="index.htm#" tppabs="http://izzht.com/#">知识讲堂</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="javascript:if(confirm(%27http://www.qq.com/  \n\nThis file was not retrieved by Teleport Ultra, because it is addressed on a domain or path outside the boundaries set for its Starting Address.  \n\nDo you want to open it from the server?%27))window.location=%27http://www.qq.com/%27" tppabs="http://www.qq.com/">帮助中心</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="index.htm#" tppabs="http://izzht.com/#">交易条款</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="index.htm#" tppabs="http://izzht.com/#">诚征英才</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="index.htm#" tppabs="http://izzht.com/#">网站地图</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="javascript:if(confirm(%27http://izzht.com/shop/  \n\nThis file was not retrieved by Teleport Ultra, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://izzht.com/shop/%27" tppabs="http://izzht.com/shop/">店铺管理</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<a href="javascript:if(confirm(%27http://izzht.com/home/News/nList/catId/11.html  \n\nThis file was not retrieved by Teleport Ultra, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://izzht.com/home/News/nList/catId/11.html%27" tppabs="http://izzht.com/home/News/nList/catId/11.html">平台公告</a>
				</div>
				<div class="copyright">
					<div id="ZZHT-mallLicense" data='1'>Copyright©2018 Powered By
						<a target="_blank" href="index.htm" tppabs="http://izzht.com/">中证华太</a>&nbsp;&nbsp;
						<a target="_blank" href="javascript:if(confirm(%27http://www.miibeian.gov.cn/  \n\nThis file was not retrieved by Teleport Ultra, because it is addressed on a domain or path outside the boundaries set for its Starting Address.  \n\nDo you want to open it from the server?%27))window.location=%27http://www.miibeian.gov.cn/%27" tppabs="http://www.miibeian.gov.cn/">宁ICP备17002373号-1</a>
					</div>
				</div>
			</div>
		</div>`;
Vue.component('vue_footer_ck3', {
	data: function() {
		return {
			arr: []
		}
	},
	template: vue_footer_ck3
});
new Vue({
	el: '#vue_footer'
});

$(function() {
	//二维码
	//参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
	var a = qrcode(8, 'M');
	var url = 'http://www.51huamall.com/' + 'home/users/download/?request=1';
	a.addData(url);
	a.make();
	$('#footerQrcodea').html(a.createImgTag());
});
//footer _end

