//$(function() {
//	$('.goodsImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 100,placeholder:window.conf.OSS+window.conf.GOODS_LOGO});//商品默认图片
//	$('.shopsImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 100,placeholder:window.conf.OSS+window.conf.SHOP_LOGO});//店铺默认头像
//	$('.usersImg').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 100,placeholder:window.conf.OSS+window.conf.USER_LOGO});//会员默认头像
//});
ZZHT.initVisitor = function(){
	ZZHT.dropDownLayer(".j-dorpdown",".j-dorpdown-layer");
	ZZHT.dropDownLayer(".drop-info",".ZZHT-tag");
	ZZHT.dropDownLayerCart(".ZZHT-cart-box",".ZZHT-cart-boxs");
	ZZHT.searchIpt();
	ZZHT.showCategoryNavs();
	ZZHT.Sidebar();
    $("#search-ipt").keydown(function (e) {
        if (e.keyCode == 13) {
            ZZHT.search(this.value);
        }
    });
	$('.ZZHT-lite-cart').hover(function(){
		$('.ZZHT-lite-carts').show();
	},function(){
		$('.ZZHT-lite-carts').hide();
	});
};
ZZHT.searchIpt = function(){
	$('.j-search-box').hover(function(){
		$(".j-type-list").show();
		$(this).find('i').removeClass('arrow').addClass('over');
		$(this).css({"border-left":"2px solid #e23c3d"});
	},function(){
		$(".j-type-list").hide();
		$(this).css({"border-left":"2px solid #e23c3d"});
		$(this).find('i').removeClass('over').addClass('arrow');
	});

	$('j-type-list').hover(function(){
		$(".j-type-list").show();
		$(this).find('i').removeClass('arrow').addClass('over');
		$(this).css({"border-left":"2px solid #e23c3d"});
	});

	$(".j-type-list div").click(function(){
		$("#search-type").val($(this).attr("data"));
		$(".j-search-type span").html($(this).html());
		if($(this).attr("data")==1){
			$(this).attr("data",0);
			$(this).html('商品');
			$('#search-ipt').attr('placeholder',$('#adsShopWordsSearch').val());
		}else{
			$(this).attr("data",1);
			$(this).html('店铺');
			$('#search-ipt').attr('placeholder',$('#adsGoodsWordsSearch').val());
		}
		$(".j-type-list").hide();
		$(".j-search-type").find('i').removeClass('over').addClass('arrow');
	});
}
ZZHT.search = function(){
	if($("#search-type").val()==1){
		ZZHT.shopSearch($.trim($('#search-ipt').val()));
	}else{
		ZZHT.goodsSearch($.trim($('#search-ipt').val()));
	}
}
ZZHT.shopSearch = function(v){
	location.href = ZZHT.U('home/shops/shopstreet','keyword='+v);
}
ZZHT.goodsSearch = function(v){
	location.href = ZZHT.U('home/goods/search','keyword='+v);
}
ZZHT.showCategoryNavs = function(){
	if($('.ZZHT-filters')[0]){
		$(".drop-down").hover(function(){
			$(this).addClass("hover");
		},function(){
			$(this).removeClass("hover");
		});
		$(".dorp-down-layer").hover(function(){
			$(this).prev().addClass("hover");
		},function(){
			$(this).prev().removeClass("hover");
		});
	}
}
ZZHT.Sidebar = function(){
	if(!$('#ZZHT-categorys')[0])return;
	if(!$('#ZZHT-categorys').hasClass('j-index')){
	   ZZHT.dropDownLayer("#ZZHT-categorys",".j-cate-dd");
	}
	$(".dd-inner").children(".item").hover(function() { //一级导航悬浮
		$(this).parent().find('.over-cat').show();

        $(this).addClass("hover").siblings(".item").removeClass("hover");
        var index = $(this).index();
        $(".dorpdown-layer").children(".item-sub").hide();
        $(".dorpdown-layer").children(".item-sub").eq(index).show();

        var start = $('.j-cate-dt').offset().top;
        var obj = $('#index_menus_sub');
        var sh = document.documentElement.scrollTop || document.body.scrollTop; // 滚动条距离顶部高度
        if(sh>start+50){
	            var start = sh-start;
        }else{
        	var start = 50;
        }
        //obj.stop().animate({ "top": start });
        obj.css('top',start);


    },function(){
    	$(this).parent().find('.over-cat').hide();
    });



    $('.over-cat-icon').parent().mouseover(function(){
    	 $(this).find('.over-cat-icon').addClass('over-cat-icon-hover');
    });
     $('.over-cat-icon').parent().mouseout(function(){
    	$(this).find('.over-cat-icon').removeClass('over-cat-icon-hover');
    });

    $(".dd-inner").children(".item").mouseover(function() {

    	$('.dd-inner').find('.over-cat').show();

        var iCon = $(this).attr('id');
        $('.'+iCon).addClass(iCon+'-hover');
    });
    $(".dd-inner").children(".item").mouseout(function() {

    	$('.dd-inner').find('.over-cat').hide();

        var iCon = $(this).attr('id');
        $('.'+iCon).removeClass(iCon+'-hover');
    });

    $("#index_menus_sub").hover(function(){
    	$('.dd-inner').find('.over-cat').show();
    	$(this).show();
    },function(){
    	$(this).hide();
    	$('.dd-inner').find('.over-cat').hide();
    });
    $(".dd-inner").hover(function() { //整个导航菜单悬浮，是否显示二级导航到出厂
        $("#index_menus_sub").show();

    }, function() {
        $("#index_menus_sub").hide();
        $('.item').removeClass("hover");
    })
    $("#index_menus_sub").children(".item-sub").hover(function() { //二级导航悬浮
        var index = $(this).index();
        $(".dd-inner").children(".item").eq(index).addClass("hover");
        $("#index_menus_sub").show();
        var i = index+1;
        $('.cat-icon-'+i).addClass('cat-icon-'+i+'-hover');
    }, function() {
        $("#index_menus_sub").hide();
        $(".dd-inner").children(".item").removeClass("hover");
        var index = $(this).index();
        var i = index+1;
        $('.cat-icon-'+i).removeClass('cat-icon-'+i+'-hover');

    });
    
    $('.fore2').hover(function(){
	$(this).children('dt').css('background-color','#f5b272');
	},function(){
		$(this).children('dt').css('background-color','');
	});
}
ZZHT.dropDownLayer = function(dropdown,layer){
	$(dropdown).hover(function () {
        $(this).find(layer).show();
    }, function () {
    	$(this).find(layer).hide();
    });
	$(layer).hover(function () {
		$(this).find(layer).show();
    }, function () {
    	$(this).find(layer).hide();
    });
}

ZZHT.tips = function(content, selector, options){
	var opts = {};
	opts = $.extend(opts, {tips:1, time:2000, maxWidth: 260}, options);
	return layer.tips(content, selector, opts);
}
ZZHT.open = function(options){
	var opts = {};
	opts = $.extend(opts, {offset:'100px'}, options);
	return layer.open(opts);
}
ZZHT.confirm = function(options){
	var opts = {};
	opts = $.extend(opts, {title:'系统提示',offset:'200px'}, options);
	return layer.confirm(opts.content,{icon: 3, title:opts.title,offset:opts.offset},options.yes,options.cancel);
}
ZZHT.load = function(options){
	var opts = {};
	opts = $.extend(opts,{time:2000,shade: [0.4, '#000000'],offset: '200px'},options);
	return layer.msg(opts.msg, opts);
}
ZZHT.msg = function(msg, options, func){
	var opts = {};
	//有抖動的效果,第二位是函數
	if(typeof(options)!='function'){
		opts = $.extend(opts,{time:1000,shade: [0.4, '#000000'],offset: '200px'},options);
		return layer.msg(msg, opts, func);
	}else{
		return layer.msg(msg, options);
	}
}
ZZHT.toJson = function(str){
	var json = {};
	try{
		if(typeof(str )=="object"){
			json = str;
		}else{
			json = eval("("+str+")");
		}
		if(json.status && json.status=='-999'){
			ZZHT.msg('对不起，您已经退出系统！请重新登录',{icon:5},function(){
				if(window.parent){
					window.parent.location.reload();
				}else{
					location.reload();
				}
			});
		}else if(json.status && json.status=='-998'){
			ZZHT.msg('对不起，您没有操作权限，请与管理员联系');
			return;
		}
	}catch(e){
		ZZHT.msg("系统发生错误:"+e.getMessage,{icon:5});
		json = {};
	}
	return json;
}

//用户退出
ZZHT.logout = function(){
	$.post(ZZHT.U('home/users/logout'),{},function(data,textStatus){
		location.href=ZZHT.U('home/index/index');
	});
}

//代理公司退出
ZZHT.agentlogout = function(){
    $.post(ZZHT.U('agent/index/logout'),{},function(data,textStatus){
        location.href=ZZHT.U('agent/index/login');
    });
}

/**
* 上传图片
*/
ZZHT.upload = function(opts){
	var _opts = {};
	_opts = $.extend(_opts,{auto: true,swf: '../plugins/webuploader/Uploader.swf',server:SURL+'imageUpload'},opts);
	var uploader = WebUploader.create(_opts);
	uploader.on('uploadSuccess', function( file,response ) {
	    var json = ZZHT.toJson(response._raw);
	    if(_opts.callback)_opts.callback(json,file);
	});
	uploader.on('uploadError', function( file ) {
		if(_opts.uploadError)_opts.uploadError();
	});
	uploader.on( 'uploadProgress', function( file, percentage ) {
		percentage = percentage.toFixed(2)*100;
		if(_opts.progress)_opts.progress(percentage);
	});
    return uploader;
}

ZZHT.goTo = function(obj){
	location.href = $(obj).attr('data');
}
ZZHT.getVerify = function(id){
//  $(id).attr('src',ZZHT.U('home/index/getVerify','rnd='+Math.random()));
    $(id).attr('src','http://www.51huamall.com/home/index/getVerify.html?rnd='+Math.random());
}
ZZHT.loginWindow = function(){
	ZZHT.open({type:2,area:['550px','320px'],offset:'auto',title:'用户登录',content:[ZZHT.U('home/users/toLoginBox'),'no']});
}
/********************* 选项卡切换隐藏 **********************/
$.fn.TabPanel = function(options){
	var defaults = {tab: 0}; 
	var opts = $.extend(defaults, options);
	var t = this;
	$(t).find('.ZZHT-tab-nav li').click(function(){
		$(this).addClass("on").siblings().removeClass();
		var index = $(this).index();
		$(t).find('.ZZHT-tab-content .ZZHT-tab-item').eq(index).show().siblings().hide();
		if(opts.callback)opts.callback(index);
	});
	$(t).find('.ZZHT-tab-nav li').eq(opts.tab).click();
}
/**
 * 去除url中指定的参数(用于分页)
 */
ZZHT.splitURL = function(spchar){
	var url = location.href;
	var urlist = url.split("?");
	var furl = new Array();
	var fparams = new Array();
		furl.push(urlist[0]);
	if(urlist.length>1){
		var urlparam = urlist[1];
			params = urlparam.split("&");
		for(var i=0; i<params.length; i++){
			var vparam = params[i];
			var param = vparam.split("=");
			if(param[0]!=spchar){
				fparams.push(vparam);
			}
		}
		if(fparams.length>0){
			furl.push(fparams.join("&"));
		}
		
	}
	if(furl.length>1){
		return furl.join("?");
	}else{
		return furl.join("");
	}
}
ZZHT.addCart = function(goodsId){
	if(window.conf.IS_LOGIN==0){
		ZZHT.loginWindow();
		return;
	}
	$.post(ZZHT.U('home/carts/addCart'),{goodsId:goodsId,buyNum:1},function(data,textStatus){
	     var json = ZZHT.toJson(data);
	     if(json.status==1){
	    	 ZZHT.msg(json.msg,{icon:1,time:600,shade:false});
	     }else{
	    	 ZZHT.msg(json.msg,{icon:2});
	     }
	});
}

ZZHT.delCart = function(id){
	ZZHT.confirm({content:'您确定要删除该商品吗？',yes:function(index){
		$.post(ZZHT.U('home/carts/delCart'),{id:id,rnd:Math.random()},function(data,textStatus){
		     var json = ZZHT.toJson(data);
		     if(json.status==1){
		    	 ZZHT.msg(json.msg,{icon:1});
		         location.href=ZZHT.U('home/carts/index');
		     }else{
		    	 ZZHT.msg(json.msg,{icon:2});
		     }
		});
	}});
}
ZZHT.changeCartGoods = function(id,buyNum,isCheck){
//	$.post(ZZHT.U('home/carts/changeCartGoods'),{id:id,isCheck:isCheck,buyNum:buyNum,rnd:Math.random()},function(data,textStatus){
//	     var json = ZZHT.toJson(data);
//	     if(json.status!=1){
//	    	 ZZHT.msg(json.msg,{icon:2});
//	     }
//	});
}
ZZHT.dropDownLayerCart = function(dropdown,layer){
	$(dropdown).hover(function () {
        $(this).find(layer).show();
        ZZHT.checkCart();
    }, function () {
    	$(this).find(layer).hide();
    });
	$(layer).hover(function () {
		$(this).find(layer).show();
    }, function () {
    	$(this).find(layer).hide();
    });
	/*$('.j-word').mouseout(function () {
		ZZHT.checkCart();
	});*/
}
ZZHT.delCheckCart = function(id,func){
	$.post(ZZHT.U('home/carts/delCart'),{id:id,rnd:Math.random()},function(data,textStatus){
	     var json = ZZHT.toJson(data);
	     if(json.status==1){
	    	 ZZHT.msg(json.msg,{icon:1});
	    	 ZZHT.checkCart();
	     }else{
	    	 ZZHT.msg(json.msg,{icon:2});
	     }
	});
}
ZZHT.checkCart = function(){
	$('#list-carts2').html('');
	$('#list-carts3').html('');
	$('#list-carts').html('<div style="padding:32px 0px 77px 112px;"><img src="../img/loading.gif" style="vertical-align: middle;">正在加载数据...</div>');
	if(window.conf.IS_LOGIN ==1){
        $.get(SURL+'sktCarts/selectCartsByuserId',{userId:localStorage.userId},function(data) {
            var json = data;
            if(json.messageCode==200){
                if(json.entity.length>0){
                    var gettpl = document.getElementById('list-cart').innerHTML;
                    laytpl(gettpl).render(json, function(html){
                        $('#list-carts').html(html);
                    });
                    if(json.goodsTotalScore > 0) {
                        var html = '<div class="comm clearfix" id="list-comm">&nbsp;&nbsp;共<span>'+json.goodsTotalNum+'</span>件商品<span class="span2">￥'+json.goodsTotalMoney+'<span class="fr" style="font-weight:normal;color:#999;font-size:12px;">+'+json.goodsTotalScore+'积分</span>'+'</span></div>';
                    } else {
                        var html = '<div class="comm clearfix" id="list-comm">&nbsp;&nbsp;共<span>'+json.goodsTotalNum+'</span>件商品<span class="span2">￥'+json.goodsTotalMoney+'</span></div>';
                    }
                    $('#list-carts2').html(html);
                    $('#list-carts3').html('<a href="../carts/carts.html" class="btn btn-3">去购物车结算</a>');
                    $('.goodsImgc').lazyload({ effect: "fadeIn",failurelimit : 10,skip_invisible : false,threshold: 200,placeholder:window.conf.ROOT+'/'+window.conf.GOODS_LOGO});//商品默认图片
                    if(json.list.length>5){
                        $('#list-carts').css('overflow-y','scroll').css('height','416');
                    }
                }else{
                    $('#list-carts').html('<p class="carts">购物车中空空如也，赶紧去选购吧～</p>');
                }
                $('#goodsTotalNum').html(json.goodsTotalNum);
                if(json.goodsTotalNum != 0) {
                    $('#cart-count').html(json.goodsTotalNum).show();
                } else {
                    $('#cart-count').html('').hide();
                }
            }
        });
	}else {
        $('#list-carts').html('<p class="carts">购物车中空空如也，赶紧去选购吧～</p>');
	}
}
ZZHT.changeIptNum = function(diffNum,iptId,btnId,id,func){
	var suffix = (id)?"_"+id:"";
	var iptElem = $(iptId+suffix);
	var minVal = parseInt(iptElem.attr('data-min'),10);
	var maxVal = parseInt(iptElem.attr('data-max'),10);
	var tmp = 0;
	if(maxVal<minVal){
		tmp = maxVal;
		maxVal = minVal;
		minVal = tmp;
	}
	var num = parseInt(iptElem.val(),10);
	num = num?num:1;
	num = num + diffNum;
	btnId = btnId.split(',');
	$(btnId[0]+suffix).css('color','#666');
	$(btnId[1]+suffix).css('color','#666');
	if(minVal>=num){
		num=minVal;
		$(btnId[0]+suffix).css('color','#ccc');
	}
	if(maxVal<=num){
		num=maxVal;
		$(btnId[1]+suffix).css('color','#ccc');
	}
	iptElem.val(num);
	if(suffix!='')ZZHT.changeCartGoods(id,num,-1);
	if(func){
		var fn = window[func];
		fn();
	}
}
ZZHT.shopQQ = function(val){
	if(ZZHT.blank(val) !=''){
      return [
              '<a href="tencent://message/?uin='+val+'&Site=QQ交谈&Menu=yes">',
		      '<img border="0" src="http://wpa.qq.com/pa?p=1:'+val+':7" alt="QQ交谈" width="71" height="24" />',
		      '</a>'
		      ].join('');
	}else{
		return '';
	}
}
ZZHT.shopWangWang = function(val){
	if(ZZHT.blank(val) !=''){
		return [
	           '<a target="_blank" href="http://www.taobao.com/webww/ww.php?ver=3&touid=%27+val+%27&siteid=cntaobao&status=1&charset=utf-8">',
		       '<img border="0" src="http://amos.alicdn.com/realonline.aw?v=2&uid='+val+'&site=cntaobao&s=1&charset=utf-8" alt="和我联系" />',
	           '</a>'
		       ].join('');
	}else{
		return '';
	}
}
ZZHT.cancelFavorite = function(obj,type,id,fId){
	if(window.conf.IS_LOGIN==0){
		ZZHT.loginWindow();
		return;
	}
	var param = {},str = '商品';
	param.id = fId;
	param.type = type;
	str = (type==1)?'店铺':'商品';
	$.post(ZZHT.U('home/favorites/cancel'),param,function(data,textStatus){
	    var json = ZZHT.toJson(data);
	    if(json.status=='1'){
	       ZZHT.msg(json.msg,{icon:1});
	       $(obj).removeClass('j-fav').addClass('j-fav2');
	       $(obj).html('关注'+str)[0].onclick = function(){
	    	   ZZHT.addFavorite(obj,type,id,fId);
	       };
	    }else{
	       ZZHT.msg(json.msg,{icon:5});
	    }
    });
}
ZZHT.addFavorite = function(obj,type,id,fId){
	if(window.conf.IS_LOGIN==0){
		ZZHT.loginWindow();
		return;
	}
	$.post(ZZHT.U('home/favorites/add'),{type:type,id:id},function(data,textStatus){
	     var json = ZZHT.toJson(data);
	     if(json.status==1){
	    	 ZZHT.msg(json.msg,{icon:1});
	    	 $(obj).removeClass('j-fav2').addClass('j-fav');
             $(obj).html('已收藏')[0].onclick = function () {
	    		 ZZHT.cancelFavorite(obj,type,id,json.data.fId);
	    	 };
	     }else{
	    	 ZZHT.msg(json.msg,{icon:2});
	     }
	});
}
/**
 * 循环调用及设置商品分类
 * @param id           当前分类ID
 * @param val          当前分类值
 * @param childIds     分类路径值【数组】
 * @param isRequire    是否要求必填
 * @param className    样式，方便将来获取值
 * @param beforeFunc   运行前回调函数
 * @param afterFunc    运行后回调函数
 */
ZZHT.ITSetGoodsCats = function(opts){
	var obj = $('#'+opts.id);
	obj.attr('lastgoodscat',1);
	var level = $('#'+opts.id).attr('level')?(parseInt($('#'+opts.id).attr('level'),10)+1):1;
	if(opts.childIds.length>0){
		opts.childIds.shift();
		if(opts.beforeFunc){
			if(typeof(opts.beforeFunc)=='function'){
				opts.beforeFunc({id:opts.id,val:opts.val});
			}else{
			   var fn = window[opts.beforeFunc];
			   fn({id:opts.id,val:opts.val});
			}
		}
		$.post(ZZHT.U('home/goodscats/listQuery'),{parentId:opts.val},function(data,textStatus){
		     var json = ZZHT.toJson(data);
		     if(json.data && json.data.length>0){
		    	 opts.isLast = false;
			     json = json.data;
		         var html = [];
		         var tid = opts.id+"_"+opts.val;
		         html.push("<select id='"+tid+"' level='"+level+"' class='"+opts.className+"' "+(opts.isRequire?" data-rule='required;' ":"")+">");
			     html.push("<option value=''>-请选择-</option>");
			     for(var i=0;i<json.length;i++){
			       	 var cat = json[i];
			       	 html.push("<option value='"+cat.catId+"' "+((opts.childIds[0]==cat.catId)?"selected":"")+">"+cat.catName+"</option>");
			     }
			     html.push('</select>');
			     $(html.join('')).insertAfter(obj);
			     var tidObj = $('#'+tid);
			     if(tidObj.val()!=''){
			    	obj.removeAttr('lastgoodscat');
			    	tidObj.attr('lastgoodscat',1);
				    opts.id = tid;
				    opts.val = tidObj.val();
				    ZZHT.ITSetGoodsCats(opts);
				 }
			     tidObj.change(function(){
				    opts.id = tid;
				    opts.val = $(this).val();
				    ZZHT.ITGoodsCats(opts);
				 })
		     }else{
		    	 opts.isLast = true;
		    	 opts.lastVal = opts.val;
		     }
		     if(opts.afterFunc){
		    	 if(typeof(opts.afterFunc)=='function'){
		    		 opts.afterFunc(opts);
		    	 }else{
		    	     var fn = window[opts.afterFunc];
		    	     fn(opts);
		    	 }
		     }
		});
	}
}

/**
 * 循环创建商品分类
 * @param id            当前分类ID
 * @param val           当前分类值
 * @param className     样式，方便将来获取值
 * @param isRequire     是否要求必填
 * @param beforeFunc    运行前回调函数
 * @param afterFunc     运行后回调函数
 */
ZZHT.ITGoodsCats = function(opts){
	opts.className = opts.className?opts.className:"j-goodsCats";
	var obj = $('#'+opts.id);
	obj.attr('lastgoodscat',1);
	var level = parseInt(obj.attr('level'),10)+1;
	$("select[id^='"+opts.id+"_']").remove();
	if(opts.isRequire)$('.msg-box[for^="'+opts.id+'_"]').remove();
	if(opts.beforeFunc){
		if(typeof(opts.beforeFunc)=='function'){
			opts.beforeFunc({id:opts.id,val:opts.val});
		}else{
		   var fn = window[opts.beforeFunc];
		   fn({id:opts.id,val:opts.val});
		}
	}
	opts.lastVal = opts.val;
	if(opts.val==''){
		obj.removeAttr('lastgoodscat');
		var lastId = 0,level = 0,tmpLevel = 0,lasObjId;
		$('.'+opts.className).each(function(){
			tmpLevel = parseInt($(this).attr('level'),10);
			if(level <= tmpLevel && $(this).val()!=''){
				level = tmpLevel;
				lastId = $(this).val();
				lasObjId = $(this).attr('id');
			}
		})
		$('#'+lasObjId).attr('lastgoodscat',1);
		opts.id = lasObjId;
    	opts.val = $('#'+lasObjId).val();
	    opts.isLast = true;
	    opts.lastVal = opts.val;
		if(opts.afterFunc){
			if(typeof(opts.afterFunc)=='function'){
				opts.afterFunc(opts);
			}else{
	    	    var fn = window[opts.afterFunc];
	    	    fn(opts);
			}
	    }
		return;
	}
	$.post(SURL+'areas/getAreas',{parentId:opts.val},function(data,textStatus){
	     var json = ZZHT.toJson(data);
	     if(json.data && json.data.length>0){
	    	 opts.isLast = false;
	    	 json = json.data;
	         var html = [];
	         var tid = opts.id+"_"+opts.val;
	         html.push("<select id='"+tid+"' level='"+level+"' class='"+opts.className+"' "+(opts.isRequire?" data-rule='required;' ":"")+">");
		     html.push("<option value='' >-请选择-</option>");
		     for(var i=0;i<json.length;i++){
		       	 var cat = json[i];
		       	 html.push("<option value='"+cat.catId+"'>"+cat.catName+"</option>");
		     }
		     html.push('</select>');
		     $(html.join('')).insertAfter(obj);
		     $("#"+tid).change(function(){
		    	opts.id = tid;
		    	opts.val = $(this).val();
		    	if(opts.val!=''){
		    		obj.removeAttr('lastgoodscat');
		    	}
		    	ZZHT.ITGoodsCats(opts);
		     })
	     }else{
	    	 opts.isLast = true;
	    	 opts.lastVal = opts.val;
	     }
	     if(opts.afterFunc){
	    	 if(typeof(opts.afterFunc)=='function'){
	    		 opts.afterFunc(opts);
	    	 }else{
	    	     var fn = window[opts.afterFunc];
	    	     fn(opts);
	    	 }
	     }
	});
}
/**
 * 获取最后已选分类的id
 */
ZZHT.ITGetAllGoodsCatVals = function(srcObj,className){
	var goodsCatId = '';
	$('.'+className).each(function(){
		if($(this).attr('lastgoodscat')=='1')goodsCatId = $(this).attr('id')+'_'+$(this).val();
	});
	goodsCatId = goodsCatId.replace(srcObj+'_','');
	return goodsCatId.split('_');
}
/**
 * 获取最后分类值
 */
ZZHT.ITGetGoodsCatVal = function(className){
	var goodsCatId = '';
	$('.'+className).each(function(){
		if($(this).attr('lastgoodscat')=='1')goodsCatId = $(this).val();
	});
	return goodsCatId;
}
/**
 * 循环创建地区
 * @param id            当前分类ID
 * @param val           当前分类值
 * @param className     样式，方便将来获取值
 * @param isRequire     是否要求必填
 * @param beforeFunc    运行前回调函数
 * @param afterFunc     运行后回调函数
 */
ZZHT.ITAreas = function(opts){
	opts.className = opts.className?opts.className:"j-areas";
	var obj = $('#'+opts.id);
	obj.attr('lastarea',1);
	var level = parseInt(obj.attr('level'),10)+1;
	$("select[id^='"+opts.id+"_']").remove();
	if(opts.isRequire)$('.msg-box[for^="'+opts.id+'_"]').remove();
	if(opts.beforeFunc){
		if(typeof(opts.beforeFunc)=='function'){
			opts.beforeFunc({id:opts.id,val:opts.val});
		}else{
		   var fn = window[opts.beforeFunc];
		   fn({id:opts.id,val:opts.val});
		}
	}
	opts.lastVal = opts.val;
	if(opts.val==''){
		obj.removeAttr('lastarea');
		var lastId = 0,level = 0,tmpLevel = 0,lasObjId;
		$('.'+opts.className).each(function(){
			tmpLevel = parseInt($(this).attr('level'),10);
			if(level <= tmpLevel && $(this).val()!=''){
				level = tmpLevel;
				lastId = $(this).val();
				lasObjId = $(this).attr('id');
			}
		})
		$('#'+lasObjId).attr('lastarea',1);
		opts.id = lasObjId;
    	opts.val = $('#'+lasObjId).val();
	    opts.isLast = true;
	    opts.lastVal = opts.val;
		if(opts.afterFunc){
			if(typeof(opts.afterFunc)=='function'){
				opts.afterFunc(opts);
			}else{
	    	    var fn = window[opts.afterFunc];
	    	    fn(opts);
			}
	    }
		return;
	}
//	$.post(ZZHT.U('home/areas/listQuery'),{parentId:opts.val},function(data,textStatus){
	$.post(SURL+'areas/getAreas',{parentId:opts.val},function(data,textStatus){
//	     var json = ZZHT.toJson(data);
	     var json = data;
	     console.log(json);
	     if(json.entity && json.entity.length>0){
	    	 json = json.entity;
	         var html = [],tmp;
	         var tid = opts.id+"_"+opts.val;
	         html.push("<select id='"+tid+"' level='"+level+"' class='"+opts.className+"' "+(opts.isRequire?" data-rule='required;' ":"")+">");
		     html.push("<option value='' >-请选择-</option>");
		     for(var i=0;i<json.length;i++){
		    	 tmp = json[i];
		       	 html.push("<option value='"+tmp.areaId+"'>"+tmp.areaName+"</option>");
		     }
		     html.push('</select>');
		     $(html.join('')).insertAfter(obj);
		     $("#"+tid).change(function(){
		    	opts.id = tid;
		    	opts.val = $(this).val();
		    	if(opts.val!=''){
		    		obj.removeAttr('lastarea');
		    	}
		    	ZZHT.ITAreas(opts);
		     })
	     }else{
	    	 opts.isLast = true;
	    	 opts.lastVal = opts.val;
	     }
	     if(opts.afterFunc){
	    	 if(typeof(opts.afterFunc)=='function'){
	    		 opts.afterFunc(opts);
	    	 }else{
	    	     var fn = window[opts.afterFunc];
	    	     fn(opts);
	    	 }
	     }
	});
}
/**
 * 循环调用及设置地区
 * @param id           当前地区ID
 * @param val          当前地区值
 * @param childIds     地区路径值【数组】
 * @param isRequire    是否要求必填
 * @param className    样式，方便将来获取值
 * @param beforeFunc   运行前回调函数
 * @param afterFunc    运行后回调函数
 */
ZZHT.ITSetAreas = function(opts){
	var obj = $('#'+opts.id);
	obj.attr('lastarea',1);
	var level = $('#'+opts.id).attr('level')?(parseInt($('#'+opts.id).attr('level'),10)+1):1;
	if(opts.childIds.length>0){
		opts.childIds.shift();
		if(opts.beforeFunc){
			if(typeof(opts.beforeFunc)=='function'){
				opts.beforeFunc({id:opts.id,val:opts.val});
			}else{
			   var fn = window[opts.beforeFunc];
			   fn({id:opts.id,val:opts.val});
			}
		}
		$.post(SURL+'areas/getAreas',{parentId:opts.val},function(data,textStatus){
//		     var json = ZZHT.toJson(data);
		     var json = data;
		     if(json.entity && json.entity.length>0){
		    	 json = json.entity;
		         var html = [],tmp;
		         var tid = opts.id+"_"+opts.val;
		         html.push("<select id='"+tid+"' level='"+level+"' class='"+opts.className+"' "+(opts.isRequire?" data-rule='required;' ":"")+">");
			     html.push("<option value=''>-请选择-</option>");
			     for(var i=0;i<json.length;i++){
			    	 tmp = json[i];
			       	 html.push("<option value='"+tmp.areaId+"' "+((opts.childIds[0]==tmp.areaId)?"selected":"")+">"+tmp.areaName+"</option>");
			     }
			     html.push('</select>');
			     $(html.join('')).insertAfter(obj);
			     var tidObj = $('#'+tid);
			     if(tidObj.val()!=''){
			    	obj.removeAttr('lastarea');
			    	tidObj.attr('lastarea',1);
				    opts.id = tid;
				    opts.val = tidObj.val();
				    ZZHT.ITSetAreas(opts);
				 }
			     tidObj.change(function(){
				    opts.id = tid;
				    opts.val = $(this).val();
				    ZZHT.ITAreas(opts);
				 })
		     }else{
		    	 opts.isLast = true;
		    	 opts.lastVal = opts.val;
		     }
		     if(opts.afterFunc){
		    	 if(typeof(opts.afterFunc)=='function'){
		    		 opts.afterFunc(opts);
		    	 }else{
		    	     var fn = window[opts.afterFunc];
		    	     fn(opts);
		    	 }
		     }
		});
	}
}
/**
 * 获取最后地区的值
 */
ZZHT.ITGetAreaVal = function(className){
	var areaId = '';
	$('.'+className).each(function(){
		if($(this).attr('lastarea')=='1')areaId = $(this).val();
	});
	return areaId;
}
/**
 * 获取最后已选分类的id
 */
ZZHT.ITGetAllAreaVals = function(srcObj,className){
	var areaId = '';
	$('.'+className).each(function(){
		if($(this).attr('lastarea')=='1')areaId = $(this).attr('id')+'_'+$(this).val();
	});
	areaId = areaId.replace(srcObj+'_','');
	return areaId.split('_');
}
///**记录广告点击**/
//ZZHT.recordClick = function(adId){
//	$.post(ZZHT.U('home/ads/recordClick'),{id:adId},function(data){});
//}
//ZZHT.position = function(mid,mtype){
//	$.post(ZZHT.U('home/index/position'),{menuId:mid,menuType:mtype},function(data){});
//}
//关闭顶部广告
//ZZHT.closeAds = function(t){
//$(t).parent().remove();
//$.cookie('ads_cookie', '1', { expires: 1 }); 
//}
//ZZHT.closeIframe = function(){
//	var index = parent.layer.getFrameIndex(window.name);
//	parent.layer.close(index);
//}

