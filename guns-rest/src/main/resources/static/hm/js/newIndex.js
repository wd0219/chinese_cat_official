$(function () {
    // vue所有日志的显示
    Vue.config.silent = true;
    // vue开始到结束曲线是否显示
    Vue.config.performance = false;
    // vue开发环境信息是否显示
    Vue.config.productionTip = false;
    // vue错误信息钩子
    Vue.config.errorHandler = function (err, vm1, info) {
        console.log(err+"< - >"+vm1+"< - >"+info);
    };
    
//  $('.hmNavItem').show();
    
    if(localStorage.loginName){
    	$('.userName').html('Hello,'+localStorage.loginName);
    	
    	$('.login').hide();
    	$('.register').hide();
    	$('.logout').show();
    	$('.hmLogin>img').show();
    	
    	getUserPhoto();
    	
    	$('.hmNoticeCont').click(function(){
    		location.href = '../Usercenter/msgList.html';
    	})
    };

    // 一个vue实例
    var vm = new Vue({
        el: '#app',
        data: {
            //图片基础路径 在ljf_base.js 里 所有页面应该都有配置
            imgUrlS: imgUrl,
            //今日推荐的商品数组
            bestGoods: [],
            //今日推荐的品牌数组
            brandGoods: [],
            //轮播图数组
            adimg: [],
            //轮播图链接数组
            adurl: [],
            //主体内容1
            content1: {
                c1Host: [],
                c1List: [],
                cNavId: []
            },
            //主体内容2
            content2: {
                c2Host: [],
                c2List: [],
                cNavId: []
            },
            //主体内容3
            content3: {
                c3Host: [],
                c3List: [],
                cNavId: []
            },
            //主体内容4
            content4: {
                c4Host: [],
                c4List: [],
                cNavId: []
            }
        },
        methods: {
            go_detail: function (id) {//方法
                location.href = "../view/detail.html?goodsId=" + id;
            },
            go_brand: function (id) {//点击跳转到品牌页
                location.href = "../second/second.html?brandId=" + id;
            },
            go_more: function (catName) {
                location.href = "../second/second.html?catName=" + catName;
            }
        },
        updated: function () {
            this.$nextTick(function () {
                var mySwiper = new Swiper('.swiper-container', {
                    autoplay: 2500,//可选选项，自动滑动
                    effect : 'fade'
                })
            })
        }
    });

    // 今日推荐数据
    $.ajax({
        url: SURL + "goods/todayHotSaleGoods",
        success: function (data) {
            for (var i = 0; i < 3; i++) {
                vm.bestGoods.push(data.entity[i]);
            }
        }
    });

    // 首页轮播图;
    $.ajax({
        url: SURL + "sktShops/findWebLunbo",
        success: function (data) {
            for (var i=0; i<4; i++){
                vm.adimg.push(data.entity[i])
            }
        },
    });

    // 大类一（手机、电脑）
    $.ajax({
        url: SURL + "goods/selectByCatName",
        data: {
            param: "电脑",
            param1: "手机"
        },
        success: function (data) {
        	console.log(data);
            vm.content1.c1Host = data.entity[0];
            for (var i=1; i<=8; i++) {
                vm.content1.c1List.push(data.entity[i])
            }
            // console.log(data);
        }
    });

    // 大类二（食品/酒水/生鲜）
    $.ajax({
        url: SURL + "goods/selectByCatName",
        data: {
            param: "女装",
            param1: "男装",
            param2: "童装"
        },
        success: function (data) {
            vm.content2.c2Host = data.entity[0];
            for (var i=1; i<=8; i++) {
                vm.content2.c2List.push(data.entity[i])
            }
        }
    });

    // 大类三（西洋/民族/配件）
    $.ajax({
        url: SURL + "goods/selectByCatName",
        data: {
            param: "西洋",
            param1: "民族",
            param2: "配件"
        },
        success: function (data) {
            vm.content3.c3Host = data.entity[0];
            for (var i=1; i<=8; i++) {
                vm.content3.c3List.push(data.entity[i])
            }
        }
    });

    // 大类四（食品/酒水/生鲜）
    $.ajax({
        url: SURL + "goods/selectByCatName",
        data: {
            param: "食品",
            param1: "酒水",
            param2: "生鲜"
        },
        success: function (data) {
            vm.content4.c4Host = data.entity[0];
            for (var i=1; i<=8; i++) {
                vm.content4.c4List.push(data.entity[i])
            }
        }
    });

    // 获取大类
    $.ajax({
        url: SURL + "goodsCats/selectFirstCat",
        success: function (data) {
            $.each(data.entity, function (i) {
                if (data.entity[i].catName == "手机/电脑") {
                    var contNav1 = data.entity[i].catId;

                    // 获取大类子类
                    $.ajax({
                        url: SURL + "goodsCats/selectByGoodsInfo/" + contNav1,
                        success: function (data) {
                            var arrc = data.entity;
                            vm.content1.cNavId.push(data.entity[1],data.entity[0]);
                        }
                    })
                }
                if (data.entity[i].catName == "女装/男装/内衣内裤") {
                    var contNav2 = data.entity[i].catId;

                    // 获取大类子类
                    $.ajax({
                        url: SURL + "goodsCats/selectByGoodsInfo/" + contNav2,
                        success: function (data) {
                            vm.content2.cNavId.push(data.entity[3],data.entity[2],data.entity[1]);
//                          console.log(data);
                        }
                    })
                }
                if (data.entity[i].catName == "食品/酒水/生鲜") {
                    var contNav3 = data.entity[i].catId;

                    // 获取大类子类
                    $.ajax({
                        url: SURL + "goodsCats/selectByGoodsInfo/" + contNav3,
                        success: function (data) {
                            vm.content4.cNavId.push(data.entity[2],data.entity[1],data.entity[0]);
                        }
                    })
                }
            });
        }
    });
});

function go_msgList(){	
	console.log('aaa');
	if(localStorage.loginName){
		location.href = '../Usercenter/msgList.html';
	}
};

function go_userCenter(){
	location.href='../Usercenter/01.html';
}
