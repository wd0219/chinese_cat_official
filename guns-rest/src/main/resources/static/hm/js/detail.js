function init() {
	$('#tab').TabPanel({
		tab: 0,
		callback: function(no) {
			//						if(no == 1) { //商品属性
			//
			//						}
			//						if(no == 2) { //商品评价
			//
			//						}
			$('.btn-del').click(function() {
				delBatchUploadImg($(this), function() {
					$(this).parent().remove();
				});
			})
		}
	});

	$.ajax({ //根据商品id 查询商品所属商城分类
		type: "get",
		url: SURL + "goods/selectCatsLevelByGoodsid",
		data: {
			"goodsId": goodsId,
		},
		success: function(data) {
			console.log(data);
			$.each(data.entity, function(i, v) {
				vm.hm_class.push(v);
			})
		}
	});

	$.ajax({ //根据商品id 查询商品所属店铺分类
		type: "get",
		url: SURL + "goods/selectGoodsShopCat",
		data: {
			"goodsId": goodsId,
		},
		success: function(data) {
			$.each(data.entity, function(i, v) {
				vm.shop_class.push(v);
			})
		}
	});

	$.ajax({ //根据商品id 查询商品评分
		type: "get",
		url: SURL + "goods/selectGoodsScores",
		data: {
			"goodsId": goodsId,
		},
		success: function(data) {
			if(data.entity) {
				vm.scores = data.entity;
			}
		}
	});

	$.ajax({ //查看商品评论
		type: "get",
		url: SURL + "goods/selectGoodsAppraises",
		data: {
			"goodsId": goodsId,
		},
		success: function(data) {
			console.log(data);

			$.each(data.entity, function(i, v) {
				if(data.entity[i].images) {
					data.entity[i].images = v.images.split(',');
				} else {
					data.entity[i].images = [];
				}
			})

			vm.reviews = data.entity;
		}
	});

	$.ajax({ //获取商品信息
		type: "get",
		url: SURLS + "sktShops/selectGoods",
		data: {
			goodsId: goodsId,
		},
		success: function(data) {
			console.log(data.entity);
			var isHttp = "";
			if(data.entity[0].goodsImg.split(':')[0] == "http") {
				isHttp = "";
			} else {
				isHttp = imgUrlS;
			}
			var gallery = data.entity[0].gallery;
			if(gallery == '') {
				data.entity[0].gallery = [];
			} else {
				if(gallery.substr(0, 1) == '[') {
					data.entity[0].gallery = [];
					$.each(JSON.parse(gallery), function(i, v) {
						var obj = {
							"src": v,
							"cloudzoom": "useZoom: '.cloudzoom', image:'" + v + "', zoomImage:'" + v + "'",
						}
						data.entity[0].gallery.push(obj);
					});
				} else {
					gallery = gallery.split(',');
					data.entity[0].gallery = [];
					$.each(gallery, function(i, v) {
						var obj = {
							"src": imgUrlS + v,
							"cloudzoom": "useZoom: '.cloudzoom', image:'" + imgUrlS + v + "', zoomImage:'" + imgUrlS + v + "'",
						}
						data.entity[0].gallery.push(obj);
					});
				}
			};

			var firstPng = {
				"src": isHttp + data.entity[0].goodsImg,
				"cloudzoom": "useZoom: '.cloudzoom', image:'" + isHttp + data.entity[0].goodsImg + "', zoomImage:'" + isHttp + data.entity[0].goodsImg + "'",
			}
			data.entity[0].gallery.unshift(firstPng);
			vm.goods.push(data.entity[0]);
			vm.src = isHttp + data.entity[0].goodsImg;
			vm.cloudzoom = "";
			var shopId = data.entity[0].shopId;
			goodsInfo.shopId = shopId;
			localStorage.shopId = shopId;
			vm.shopId = shopId;
			goodsInfo.cartId = data.entity[0].cartId;

			Vue.nextTick(function() {
				CloudZoom.quickStart();
				imagesMove({id:'.goods-pics',items:'.items'});
				ishas(1, shopId);
				ishas(0, parseInt(goodsId));
				imgError();
			});
			goodsInfo.id = goodsId;
			goodsInfo.goodsPrice = data.entity[0].shopPrice;
			goodsScore = data.entity[0].goodsScore;

			$.ajax({ //查询店铺热卖商品
				type: "get",
				url: SURL + "goods/selectGoodsHots",
				data: {
					"shopId": shopId,
				},
				success: function(data) {
					if(data.entity) {
						vm.shop_hot = data.entity;
					}
					Vue.nextTick(function() {
						imgError();
					})
				}
			});

			$.ajax({ //获取商品规格
				type: "get",
				url: SURL + "goods/selectSpecShow",
				data: {
					"goodsId": goodsId,
				},
				success: function(data) {
					if(data.entity.length > 0) { //有规格
						goodsInfo.isSpec = 1;
						$.each(data.entity, function(i, v) {
							data.entity[i].isShow = 1;
						});
						to_spec(data.entity);

						Vue.nextTick(function() {
							var tabArr = [];
							$.each(vm.spec.specItemss, function(i, v) {
								var arr = [];
								$.each(v.itemDesc, function(ind, item) {
									if(v.itemDesc[ind].isShow == 1 || v.itemDesc[ind].isShow == true) {
										var obj = {
											catName: vm.spec.specItemss[i].catName,
											catId: vm.spec.specItemss[i].catId,
											val: item.val,
											itemId: item.itemId
										};
										arr.push(obj);
									}
								});
								if(arr.length > 0) {
									tabArr.push(arr);
								}
							});
							to_tab(tabArr);
						})

						$.ajax({ //获取该不同规格商品的价格
							type: "get",
							url: SURL + "goods/selectCatShow",
							data: {
								'goodsId': goodsId,
							},
							success: function(data) {
								console.log(data);
								vm.spec.goodsSpecss = data.entity;
								Vue.nextTick(function() {
									changeItem();
								})
							}
						});
					} else {
						goodsInfo.isSpec = 0;
					}
				}
			});
		}
	});

}

function changeItem() { //选择不同的规格
	var newlist = [];
	$.each($('.ul1 .active'), function(i, v) {
		var id = v.id.split('_')[1];
		newlist.push(id);
	})
	var listArr = vm.spec.listArr;
	var that = vm;
	
	var active_specIds = newlist.join(':');
	var priceList = [];
	priceList.push.apply(priceList,that.spec.goodsSpecss);//价格列表
	that.goods[0].goodsStock = 0;
	that.goods[0].shopPrice = 0;
	that.goods[0].marketPrice = 0;
	$.each(priceList,function(i,v){
		if(v.specIds==active_specIds){
			that.goods[0].goodsStock = v.specStock;
			that.goods[0].shopPrice = v.specPrice;
			that.goods[0].marketPrice = v.marketPrice;
			return
		}
	})
}

function addCart(type, iptId) { //加入购物车 立即购买
	if(!localStorage.userId) {
		layer.msg('请先登录');

		location.href = 'login.html';
	}

	var types = type;
	var goodsSpecId = '';
	if(goodsInfo.isSpec == 1) {
		var newlist = [];
		$.each($('.ul1 .active'), function(i, v) {
			var id = v.id.split('_')[1];
			newlist.push(id);
		})
		goodsSpecId = newlist.join(':');
	}
	var buyNum = $(iptId)[0] ? $(iptId).val() : 1;
	$.get(SURL + 'sktCarts/add', {
		userId: localStorage.userId,
		goodsId: goodsInfo.id,
		specIds: goodsSpecId,
		cartNum: buyNum,
		isDirect: types,
		isCheck: 1,
	}, function(data, textStatus) {
		var json = data;
		var cartId = json.entity;
		console.log(cartId);
		if(json.messageCode == 200) {
			if(types == 0) { //加入购物车
				ZZHT.msg('加入购物车成功', {
					icon: 1
				});
				$("#header").load("../plugin/header.html", function() {});
			}
			if(types == 1) { //直接购买
				var money = buyNum * vm.goods[0].shopPrice;
				money = returnFloat(money);
				var param = [{
					shopId: goodsInfo.shopId,
					goodsId: goodsInfo.id,
					cartNum: buyNum,
					cartId: cartId,
					isCheck: 1,
				}];
				param = JSON.stringify(param);
				$.ajax({
					type: "get",
					url: SURL + "sktCarts/cartsSettlement",
					data: {
						userId: localStorage.userId,
						goodsMoney: money,
						param: param,
					},
					success: function(data) {
						if(data.messageCode == 200) {
							console.log(data);
							var orderId = data.entity.sktOrdersListId[0];
							location.href = "../order/order.html?sktOrdersId=" + orderId;
						} else {
							layer.msg(data.message);
						}
					}
				});
			}
		} else {
			ZZHT.msg(json.message, {
				icon: 2
			});
		}
	});
	changeCart();
};

function imagesMove(opts){
	var tempLength = 0; //临时变量,当前移动的长度
	var viewNum = 5; //设置每次显示图片的个数量
	var moveNum = 2; //每次移动的数量
	var moveTime = 300; //移动速度,毫秒
	var scrollDiv = $(opts.id+" "+opts.items+" ul"); //进行移动动画的容器
	var scrollItems = $(opts.id+" "+opts.items+" ul li"); //移动容器里的集合
	var moveLength = scrollItems.eq(0).width() * moveNum; //计算每次移动的长度
	var countLength = (scrollItems.length - viewNum) * scrollItems.eq(0).width(); //计算总长度,总个数*单个长度
	  
	//下一张
	$(opts.id+" .next").bind("click",function(){
		if(tempLength < countLength){
			if((countLength - tempLength) > moveLength){
				scrollDiv.animate({left:"-=" + moveLength + "px"}, moveTime);
				tempLength += moveLength;
			}else{
				scrollDiv.animate({left:"-=" + (countLength - tempLength) + "px"}, moveTime);
				tempLength += (countLength - tempLength);
			}
		}
	});
	//上一张
	$(opts.id+" .prev").bind("click",function(){
		if(tempLength > 0){
			if(tempLength > moveLength){
				scrollDiv.animate({left: "+=" + moveLength + "px"}, moveTime);
				tempLength -= moveLength;
			}else{
				scrollDiv.animate({left: "+=" + tempLength + "px"}, moveTime);
				tempLength = 0;
			}
		}
	});
}