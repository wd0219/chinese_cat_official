//切换登录方式
var inviteId = '';//分享人id;
function switchLogin() {
    $('#qrcode_code').on('click', function () {
        $('#qrcode_box_pc').hide();
        $('#qrcode_box_code').show();
    });
    $('#qrcode_pc').on('click', function () {
        $('#qrcode_box_code').hide();
        $('#qrcode_box_pc').show();
    });

    function qrcode() {
//  	$.post(ZZHT.U('home/qr/checkLogin'), function(data, textStatus) {
//  		var json = ZZHT.toJson(data);
//  		if(json.status == 1) {
//  			location.href = ZZHT.U('home/account/index');
//  		}
//  	});
    }
    setInterval(qrcode,1000);
}

var h = $('.ZZHT-header').height();
var m = $('.ZZHT-lite-container').height();
var f = $('.ZZHT-footer-help').height();
var clientH = $(window).height();
var logoH = clientH - h - m - 126;
var logo_imgH = $('.login-box').height();
$('#login_box').height(logo_imgH);
var logoHs = $('.ZZHT-login_r').height();
var hs = logo_imgH - logoHs - 70
//计算登录div top值
var tH = hs / 2 + 'px';
var w = document.documentElement.clientWidth || document.body.clientWidth;
var ws = w - 1200;
//计算登录div right值
var rH = ws / 2 + 'px';
//分享人信息核查
$("#checkData").blur(function () {
    var params = new Object();
    var phoneOrLoginName = $('#checkData').val();
    params['phoneOrLoginName'] = phoneOrLoginName;
    if (phoneOrLoginName == "") {
        $('#standardShareMan').val("");
        inviteId = '';
        return false;
    } else {
        $.post(SURL+'users/checkUserInfo', params, function (data, textStatus) {
            var json = data;
            if (json.messageCode == 200) {
            	if(phoneOrLoginName==json.entity.userPhone){//如果用户输入的是手机号就返回登录名
            		$('#standardShareMan').val(json.entity.loginName);
            	}else{
            		$('#standardShareMan').val(json.entity.userPhone);
            	}
            	inviteId = json.entity.userId;
            } else {
                ZZHT.msg(json.message, {
                    icon: 2,
                }, function () {
                    $('#standardShareMan').val("");
                    inviteId = '';
                });
            }
        });
    }
})

//复选框
$(".ui-radio").click(function (e) {
    var chk = $(this).children("input");
    var check = chk.attr("checked");
    $(".ui-radio").children("input").attr("checked", false);
    chk.attr("checked", check ? false : true);
});
$('.error_tips').hide();

function showProtocol() {
    layer.open({
        type: 2,
        title: '用户注册协议',
        shadeClose: true,
        shade: 0.8,
        area: ['1000px', ($(window).height() - 50) + 'px'],
        content: 'layer/protocol.html',
        btn: ['同意并注册'],
        yes: function (index, layero) {
            layer.close(index);
        }
    });
}

//var time = 0;
//var isSend = false;
//var isUse = false;

//function getPhoneVerifyCode(params) {
//  ZZHT.msg('正在发送短信，请稍后...', {
//      time: 600000
//  });
//  $.post('url', params, function (data, textStatus) {//发送短信
//      var json = ZZHT.toJson(data);
//      if (json.status != 1) {
//          ZZHT.msg(json.msg, {
//              icon: 5
//          });
//          time = 0;
//          isSend = false;
//      }
//      if (json.status == 1) {
//          ZZHT.msg('短信已发送，请注册查收');
//          time = 120;
//          $('#timeTips').css('width', '100px');
//          $('#timeTips').html('获取验证码(120)s');
//          $('#mobileCode').val(json.phoneVerifyCode);
//          var task = setInterval(function () {
//              time--;
//              $('#timeTips').html('获取验证码(' + time + ")s");
//              if (time == 0) {
//                  isSend = false;
//                  clearInterval(task);
//                  $('#timeTips').html("重新获取验证码");
//              }
//          }, 1000);
//      }
//  });
//}

/*
 *	注册提交
 *	add by zjf 2017/11/28
 */
function initRegist() {
    $('#reg_form').validator({
        rules: {
        	checkUser: function (element) {
                return /^[\w\u0391-\uFFE5]{3,20}$/.test(element.value) || '请填写3-20位汉字或字母或数字';
            },
            checkMobile: function (element) {
                return /^1[34578]\d{9}$/.test(element.value) || '手机号输入有误';
            },
            checkPwd: function (element) {
                return /^[0-9a-zA-Z_]{6,20}$/.test(element.value) || '密码必须为6-20位数字、字母或下划线'
            },
            checkConfirmPwd: function (element) {
                return /^[0-9a-zA-Z_]{6,20}$/.test(element.value) || '确认密码必须为6-20位数字、字母或下划线'
            }
        },
        fields: {
            'loginName': '用户名:required;checkUser;',
            'userPhone': '手机号:required;checkMobile',
            'loginPwd': '密码:required;checkPwd',
            'password_confirm': '确认密码:required; match(loginPwd);checkConfirmPwd',
            'verifyCode': '验证码:required;',
            'code': '短信验证码:required;',
            'checkData':'user;'
        },
        // 表单验证通过后，ajax提交
        valid: function (form) {
            var params = ZZHT.getParams('.ipt');
            var isChecked = $('#protocol').attr('checked');
            if (isChecked == undefined) {
                ZZHT.msg("请勾选用户协议", {
                    icon: 5
                });
                return;
            }
            
            var userCode = $('#code').val();
            if(userCode!=msgcode){
            	layer.msg('短信验证码不正确');
            	return;
            }
            
            var res = verifyCode.validate(document.getElementById("verifyCode").value);

            //验证码正确
			if(res){
				var me = this;
	            // ajax提交表单之前，先禁用submit
	            me.holdSubmit();
				
//				params['regTerminal'] = 1;
//	            params['function'] = "register";
	            params['regTerminal'] = '1';
	            if(inviteId){//如果有分享人
	            	params['inviteId']=inviteId;
	            }
	            $("#reg_butt").val('正在提交...').attr('disabled', true);
	            console.log(params);
	            $.get(SURL+'users/addUser', params, function (data, textStatus) {
	                var json = data;
	                console.log(json);
	                if (json.messageCode==200) {
	                	if(json.entity==''){
	                		layer.msg('注册成功，正在跳转登录!', {
		                        icon: 6
		                    }, function () {
		                        location.href = '../view/login.html';
		                    });
	                	}
	                	else if(json.entity=='REPEAT'){
	                		layer.msg('用户名或手机号重复');
	                		$("#reg_butt").val('立即注册').attr('disabled', false);
	                		me.holdSubmit(false);
	                	}
	                    
	                } else {
	                    $("#reg_butt").val('立即注册').attr('disabled', false);
	                    me.holdSubmit(false);
	                    ZZHT.getVerify('#verifyImg');
	                    layer.msg(json.message, {
	                        icon: 5
	                    });
	                }
	            });
			}else{
				layer.msg('验证码错误');
				return;
			}
        }
    });
}

/*
 *	H5注册提交
 *	add by zjf 2017/11/28
 */
function h5Regist() {
    var isChecked = $('#protocol').attr('checked')
    if (isChecked == undefined) {
        ZZHT.msg("请勾选用户协议", {
            icon: 5
        });
        return;
    }
    var params = ZZHT.getParams('.ipt');
    params['regTerminal'] = 2;
    params['function'] = "register";
    $.post(ZZHT.U('home/users/registerSubmit'), params, function (data, textStatus) {
        var json = ZZHT.toJson(data);
        if (json.status > 0) {
            ZZHT.msg('注册成功，正在跳转登录!', {
                icon: 6
            }, function () {
                location.href = ZZHT.U('home/users/download');
            });
        } else {
            ZZHT.getVerify('#verifyImg');
            ZZHT.msg(json.msg, {
                icon: 5
            });
        }
    });
}

/*
 *图片验证码
 */
var verifyCode = new GVerify("v_container");

/*
 *注册发送手机短信验证码(1)
 *add by zjf 2017/11/28
 */
function registerPhoneVerify() {//发送短信之前的验证
    var param = new Object();
    param['userPhone'] = $('#userPhone').val();
    param['code'] = $('#verifyCode').val();
    if (param['userPhone'] == '') {
        ZZHT.msg('请输入手机号码!', {
            icon: 5
        });
        return;
    }
    if (param['code'] == "" || param['code'].length != 4) {
        ZZHT.msg("请输入图形验证码或图形验证码长度为4", {
            icon: 5
        });
        return;
    }    
    else {
        if ($('#timeTips').attr('disabled')) {
            return;
        }
        var res = verifyCode.validate(document.getElementById("verifyCode").value);
		if(res){
			getRegisterPhoneVerifys($('#userPhone').val());
		}else{
			ZZHT.msg("验证码不正确", {
	            icon: 5
	        });
	        return;
		}
    }
}

var msgcode = '';

/*
 *发送手机短信验证码(2)
 *add by zjf 2017/11/28
 */
function getRegisterPhoneVerifys(phone) {
    ZZHT.msg('正在发送短信，请稍后...', {
        time: 600000
    });
    var time = 0;
    var isSend = false;
    $.post( SURL+'sendMessage', {phone:phone}, function (data, textStatus) {
        var json = data;
        if (isSend) return;
        isSend = true;

        if (json.type != 1) {
            ZZHT.msg(json.message, {
                icon: 5
            });
            time = 0;

            isSend = false;
        }
        if (json.type == 1) {
            ZZHT.msg('短信已发送，请注意查收');
            layer.closeAll('page');

            //获取短信验证码
            msgcode = data.entity;
            console.log(data.entity);

            //获取短信截流
            time = 120;
            $('#timeTips').attr('disabled', 'disabled').css('background', '#ccc');
            $('#timeTips').html('获取手机验证码(120s)');
            $('#mobileCodeTips').css('right', '-160px');
            var task = setInterval(function () {
                time--;
                $('#timeTips').html('获取手机验证码(' + time + "s)");
                if (time == 0) {
                    isSend = false;
                    clearInterval(task);
                    $('#timeTips').html("重新获取验证码");
                    $('#timeTips').removeAttr('disabled').css('background', '#ed5d2a');
                    $('#mobileCodeTips').css('right', '-130px');
                }
            }, 1000);
        }
    });
}

//登录回车键登录事件
$(document).keyup(function (event) {
    if (event.keyCode == 13) {
        $(".ZZHT-login-but").trigger("click");
    }
});
/*css兼容ie8*/
function supportCss3(style) {
    var prefix = ['webkit', 'Moz', 'ms', 'o'],
    i,
    humpString = [],
    htmlStyle = document.documentElement.style,
    _toHumb = function (string) {
    return string.replace(/-(\w)/g, function ($0, $1) {
    return $1.toUpperCase();
    });
    };


    for (i in prefix)
    humpString.push(_toHumb(prefix[i] + '-' + style));


    humpString.push(_toHumb(style));


    for (i in humpString)
    if (humpString[i] in htmlStyle) return true;


    return false;
}
if(!supportCss3('animation-play-state')){ /*是否支持 不支持就条用里面的代码*/
   if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE8.0"){
        $(".ZZHT-login_r").css({
            top: '27px'
        });
    }else if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion .split(";")[1].replace(/[ ]/g,"")=="MSIE9.0"){
        $(".ZZHT-login_r").css({
            top: '50%'
        });
    }
}

