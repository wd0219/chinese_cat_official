/*
*AUTHOR:XYY
* DATE:20180530
* E-MAIL:18210854820@163.COM
* */
// Vue.component('no-cart',{template:'#noPro'})
var vm =new Vue({
    el: '.ZZHT-container',//作用域 父及的id 我这里放到了整个内容区
    data: {//数据源
       // imgUrlS:imgUrlS,//图片基础路径 在ljf_base.js 里 所有页面应该都有配置
        cart:[],
        name:'xyy',
        checkFlag:1,
        yesSelect:false,
        allFlag:false,//全选功能
        total:0,
        isCart:true,

    },
    components:{
        'no-cart':{
            template:'#noPro'
        }
    },
    mounted:function(){
        this.$nextTick(function(){
            this.cartView();
           // this.deleteProduct();
           // this.totalMoney();
        })
    },
    filters:{
        formatMoney:function (val) {
            return '￥'+val.toString()
        }
    },
    computed:{
        // orPrice:function(item){
        //     price=item.specPrice ||item.shopPrice
        //     return price
        // },
    },
    methods:{
        orPrice:function(item){
            price=item.specPrice ||item.shopPrice;
            return price
        },
        cartView:function () {
            var _this=this;
           // console.log(localStorage.getItem('userId'))
            this.$http.get(SURL+'sktCarts/selectCartsByuserId',{params :{userId: localStorage.userId,isDirect:0}}).then(function (res) {
             //   console.log(res.body.entity);
                _this.cart=res.body.entity;

            _this.isShowNoCart()

                _this.totalMoney()
                _this.autoSelectAll()
            })
        },

        //是否显示空购物车
        isShowNoCart:function(){
            if(this.cart.length==0){
                this.isCart=true;
            }else{
                this.isCart=false;

            }

        },
        //部分选
       changeBtn:function(v){
            this.yesSelect=!this.yesSelect;
            if(v.isCheck==1){
                // console.log(this.yesSelect)
                v.isCheck=0;
               this.yesSelect=!this.yesSelect;
            }else{
                v.isCheck=1;
                this.yesSelect=!this.yesSelect;
            }
            this.totalMoney()
            this.autoSelectAll()
        },

        clickCheck:function (v) {
            // alert('hahah')
           var _this=this;
           for (var  key in v){
                key.isCheck=!key.isCheck;
            }

        },
        //当所有商品选中时，全选默认选中
        autoSelectAll:function(){
            var n=0,_this=this;
            _this.cart.forEach(function (val,index) {
                if(val.isCheck){
                    n+=1
                }
                if(n>=_this.cart.length){
                    _this.allFlag=true
                }else{
                    _this.allFlag=false
                }
            })
        },
        //全选功能
        checkSelectAll:function () {
            this.allFlag=!this.allFlag;
            var _this=this;
            this.cart.forEach(function (val,i) {
                if(_this.allFlag){
                    // this.yesSelect=
                    val.isCheck=1
                }else{
                    val.isCheck=0
                }
            });
            this.totalMoney()
            console.log(_this.noSelect());
        },
        //删除功能
        deleteProduct:function (val,id) {
            var index=this.cart.indexOf(val),_this=this;
            layer.confirm('你确定要删除？', {
                btn: ['确定', '取消'] //按钮i
            }, function(i) {
                _this.$http.get( SURL + "sktCarts/delete",{params:{sktCartsId: id}}).then(function (res) {
                    // console.log(res)
                    if(res.body.messageCode==200){
                        _this.cart.splice(index,1)
                        _this.totalMoney();
                        _this.isShowNoCart();
                    }
                    layer.close(i);
                }).catch(function (err) {
                    // console.log(err)
                })
            }, function(res) {
                layer.alert('已取消')
            });

        },

        //总价
        totalMoney:function () {
            this.total=0;var _this=this;
            // alert(0)
           _this.cart.forEach(function (val,index) {
               if(val.isCheck){
                   // _this.orPrice(val)
                  return  _this.total+=_this.orPrice(val)*val.cartNum
               }
            })
        },

        //判断是否全部没有选中
        //有选中的就为1 否则0
        noSelect:function(){
            var _this=this;
         return  (function () {
             var n=0;
                _this.cart.forEach(function (v,i) {
                    if(v.isCheck){
                        n+=1
                    }else{
                        n+=0
                    }
                })
             return n
            })()

        },
        //跳到购物车
        goOrder:function(){
            //商品信息列表（shopId，cartId，goodsId，cartNum，isCheck）
            var param=[], shopIdArr=[];
            this.cart.forEach(function (val,i) {
                var obj={};
                obj['shopId']=val.shopId;
               obj['cartId']=val.cartId;
                obj['goodsId']=val.goodsId;
                obj['cartNum']=val.cartNum;
                obj['isCheck']=val.isCheck;

                //Array.prototype.push.call
                //Array.prototype.push.call(param,obj)
                shopIdArr.push(obj['shopId']=val.shopId);
                param.push(obj)
            });
            // console.log(param)
            var _this=this;
            // param=JSON.stringify(param);



            if(_this.noSelect()) {
                _this.$http.get(SURL + "sktCarts/cartsSettlement", {
                    params: {
                        userId: localStorage.userId,
                        goodsMoney: returnFloat(_this.total),
                        param: JSON.stringify(param)
                    }
                }).then(function (res) {
                    //   console.log(res)
                    layer.alert(res.body.message);
                    if (res.body.messageCode == '200') {

                        location.href = '../order/order.html';
                        localStorage.setItem('orderId', JSON.stringify(res.body.entity.sktOrdersListId))
                        localStorage.setItem('shopIdArr', JSON.stringify(shopIdArr))


                    }
                }).catch(function (req) {
                    // console.log(req)
                    alert('数据发生错误')
                })
            } else{
                layer.alert('请选择商品')
            }

            // return false
        },
        //调到首页
        goIndex:function () {
           //e.preventDefault()
            location.href='../view/index.html';
           // alert(11)
            return false;
        }

    }

    
});
