package com.stylefeng.guns;

import com.stylefeng.guns.modular.huamao.service.ISktCartsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class cartsSettlemenTest {
    @Autowired
    private ISktCartsService sktCartsService;

    @Test
    public   void  Test(){
        List<Map<String,Object>> mapList = new ArrayList<Map<String, Object>>();
        Map<String,Object> map = new HashMap<String, Object>();
        Map<String,Object> map1 = new HashMap<String, Object>();
        map.put("shopId",2);
        map.put("cartId",382);
        map.put("goodsId",3);
        map.put("cartNum",2);
        map.put("isCheck",1);


        map1.put("shopId",2);
        map1.put("cartId",383);
        map1.put("goodsId",3);
        map1.put("cartNum",2);
        map1.put("isCheck",1);
        mapList.add(map);
        mapList.add(map1);

        BigDecimal loanAmount = new BigDecimal("0");
        Object object = sktCartsService.cartsSettlement(5247,loanAmount,mapList);
        System.out.print(object);

    }
}
