package com.stylefeng.guns;

import com.alibaba.fastjson.JSONObject;
import com.stylefeng.guns.modular.huamao.controller.GoodsCatsController;
import com.stylefeng.guns.modular.huamao.controller.WebAccountController;
import com.stylefeng.guns.modular.huamao.model.*;
import com.stylefeng.guns.modular.huamao.dao.SktAgentsStockholderMapper;
import com.stylefeng.guns.modular.huamao.dao.UsersMapper;
import com.stylefeng.guns.modular.huamao.service.IAccountService;
import com.stylefeng.guns.modular.huamao.service.IMessagesService;
import com.stylefeng.guns.modular.huamao.service.IUsersDrawsService;
import com.stylefeng.guns.modular.huamao.util.CacheUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GunsRestApplicationTests {

	@Autowired
	private IAccountService accountService;
	@Autowired
	CacheUtil cacheUtil;
	@Autowired
	private SktAgentsStockholderMapper agentsStockholderMapper;
	@Autowired
	private UsersMapper usersMapper;

	@Autowired
	private WebAccountController wc;

	@Autowired
	private GoodsCatsController gcc;
	@Autowired
	private IMessagesService messagesService;
    @Autowired
    private IUsersDrawsService usersDrawsService;
	@Test
	public void contextLoads() {
        cacheUtil.set("1",2);
        System.out.println(12);
	}
    @Test
    public void getCache(){
        Object o = cacheUtil.get("1");
        System.out.println(o);
    }
	/**
	 * 用户升级测试
	 */
//	@Test
//	public void updateAfter(){
//		Users shareMan = new Users();
//		shareMan.setUserId(6218);
//		shareMan.setUserType(1);
//		BigDecimal cash = new BigDecimal(9997);
//		accountService.upAfterAdd(6217, true, shareMan, 2000, cash.multiply(new BigDecimal("100")), 7,
//				"用户升级奖励");
//	}
     @Test
	public void testToPayGoods(){
//        boolean b = accountService.toPayGoods(1);
//        System.out.println(b);
    }


	/**
	 * 充值生成订单测试
	 */
	@Test
	public void recharge(){
		JSONObject json = accountService.recharge(147, 4, new BigDecimal(100), "现金充值", 0);
		System.out.println(json);
//		UsersRecharge rechare = accountService.recharge(4, 4, new BigDecimal(400), "现金充值", 0);
//		System.out.println(rechare);
	}

	//申请库存积分前操作测试
	@Test
	public void applyStockBefore(){
		String buyStockScorePrice = accountService.applyStockBefore();
		System.out.println(buyStockScorePrice);
	}

	//查询是否在购买时间段内测试
	@Test
	public void isBuyTime(){

		JSONObject json = accountService.isBuyTime(265, new BigDecimal(42));
    }

	//充值后，现金账户添加现金
	@Test
	public void rechargeAfter(){
		UsersRecharge recharge = new UsersRecharge();
		recharge.setId(9013);
//		accountService.rechargeAfter(recharge);
	}

	//积分转换 普通用户
//	@Test
//	public void scoreTransform(){
//		accountService.scoreTransform(null,new BigDecimal(6.001),6208,1);
//	}
	//积分转换 代理公司
//	@Test
//	public void scoreTransform2(){
//		accountService.scoreTransform(1,new BigDecimal(6.001),null,2);
//	}
	@Test
	public  void doUpAgent(){
		String s = accountService.doUpAgent(1,1);
		System.out.println(s);
	}
	@Test
	public  void test(){
//		Users users = usersMapper.selectById(1);
//		System.out.println(users.getLoginName());
//		gcc.getGoodscatsByGoodsCatId(314);
	//	wc.getUseUpDate(1);
//		List<Integer> usersId=new ArrayList<Integer>();
//		usersId.add(2);
//		messagesService.addMessages(1,1,usersId,"发个消息试试",null);

	}

	@Test
	//充值线下支付测试Integer id,String remark,String orderImg
	public void underLinePay(){
		UsersRecharge recharge = new UsersRecharge();
		recharge.setId(9013);
		recharge.setRemark("充值测试");
		recharge.setOrderImg("图片");
		accountService.underLinePay(recharge);
	}

	//股东分红
//	@Test
//	public void shareBonus(){
//		accountService.shareBonus(10001);
//	}

	@Test
	public void testSendScore(){
		BigDecimal score=new BigDecimal(10000);
		BigDecimal totalMoney=new BigDecimal(100);
		wc.sendScore(10,68,score,"123456",totalMoney,100,"线下消费赠送积分");
	}
	@Test
	public void  testBuyplaque (){
		SktBuyplaque sktBuyplaque=new SktBuyplaque();
		sktBuyplaque.setUserId(1);
		sktBuyplaque.setCash(new BigDecimal(1000));
		sktBuyplaque.setPayType(2);
		accountService.buyplaque(sktBuyplaque);
	}
	@Test
	public void  testUserSale (){
		List<Integer> orderIdList=new ArrayList<Integer>();
		orderIdList.add(1);
	//	accountService.userSale(orderIdList);
		accountService.toPayGoods(orderIdList);
	}
//	public void  testApplyRedeem(){
//
//
//    }


	//点击线下支付按钮操作测试
	@Test
	public void clickUnderLinePay(){
		UnderLinePay underLinePay = accountService.clickUnderLinePay();
		System.out.println(underLinePay);
	}
}
