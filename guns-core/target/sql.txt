SELECT
	u.userPhone,u.userType,a.score,a.freezeScore,a.totalScore,a.kaiyuan,
	a.totalKaiyuan,a.cash,a.freezeCash,a.totalCash,a.onlineExpenditure,
	a.offlineExpenditure,a.inviteNum,a.one,a.two,a.three,a.four,
	a.five
FROM 
	`skt_account` AS a
LEFT JOIN
	`skt_users` AS u
ON
	a.userid = u.userId